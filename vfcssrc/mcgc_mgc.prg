* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: mcgc_mgc                                                        *
*              Manutenzione Carrelli                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-07-30                                                      *
* Last revis.: 2016-08-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tmcgc_mgc"))

* --- Class definition
define class tmcgc_mgc as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 572
  Height = 451+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-08-03"
  HelpContextID=30040675
  max_rt_seq=35

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  MANM_CAR_IDX = 0
  MAND_CAR_IDX = 0
  ANAGRAFE_IDX = 0
  INTERVENTI_IDX = 0
  TIPOINTERVENTO_IDX = 0
  cFile = "MANM_CAR"
  cFileDetail = "MAND_CAR"
  cKeySelect = "MCSERIALE"
  cKeyWhere  = "MCSERIALE=this.w_MCSERIALE"
  cKeyDetail  = "MCSERIALE=this.w_MCSERIALE and CPROWNUM=this.w_CPROWNUM"
  cKeyWhereODBC = '"MCSERIALE="+cp_ToStrODBC(this.w_MCSERIALE)';

  cKeyDetailWhereODBC = '"MCSERIALE="+cp_ToStrODBC(this.w_MCSERIALE)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"MAND_CAR.MCSERIALE="+cp_ToStrODBC(this.w_MCSERIALE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'MAND_CAR.CPROWORD '
  cPrg = "mcgc_mgc"
  cComment = "Manutenzione Carrelli"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MCSERIALE = space(10)
  w_OBSOTEST = ctod('  /  /  ')
  w_MCCODICE = space(10)
  w_MCMOTORE = space(1)
  o_MCMOTORE = space(1)
  w_MCORE = 0
  o_MCORE = 0
  w_MCDATA = ctod('  /  /  ')
  w_MCTOTORE = 0
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_MCDURORE = 0
  w_MCTINT = space(1)
  w_MCCODINT = space(10)
  w_MCDESINT = space(40)
  w_MCTIPO = space(1)
  w_Descri = space(100)
  w_OREANAG = 0
  o_OREANAG = 0
  w_DATAANAG = ctod('  /  /  ')
  o_DATAANAG = ctod('  /  /  ')
  w_ALIMENTAZIONE = space(1)
  w_CHKERR = .F.
  w_MCDTANAG = ctod('  /  /  ')
  w_MCOREANAG = 0
  w_TESTD = space(1)
  w_TESTE = space(1)
  w_CHKM = space(1)
  w_TIPO = space(1)
  w_STAMPA = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_SAVE = .F.
  w_OK = .F.
  w_MCFLTAG = space(1)
  o_MCFLTAG = space(1)
  w_MCOPRX = 0
  w_TAGORE = 0
  o_TAGORE = 0

  * --- Autonumbered Variables
  op_MCSERIALE = this.W_MCSERIALE
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MANM_CAR','mcgc_mgc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tmcgc_mgcPag1","mcgc_mgc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Manutenzione")
      .Pages(1).HelpContextID = 77710890
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMCSERIALE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='ANAGRAFE'
    this.cWorkTables[2]='INTERVENTI'
    this.cWorkTables[3]='TIPOINTERVENTO'
    this.cWorkTables[4]='MANM_CAR'
    this.cWorkTables[5]='MAND_CAR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MANM_CAR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MANM_CAR_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_MCSERIALE = NVL(MCSERIALE,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_3_joined
    link_1_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from MANM_CAR where MCSERIALE=KeySet.MCSERIALE
    *
    i_nConn = i_TableProp[this.MANM_CAR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MANM_CAR_IDX,2],this.bLoadRecFilter,this.MANM_CAR_IDX,"mcgc_mgc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MANM_CAR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MANM_CAR.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"MAND_CAR.","MANM_CAR.")
      i_cTable = i_cTable+' MANM_CAR '
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MCSERIALE',this.w_MCSERIALE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_OBSOTEST = i_datsys
        .w_Descri = space(100)
        .w_OREANAG = 0
        .w_DATAANAG = ctod("  /  /  ")
        .w_ALIMENTAZIONE = 'S'
        .w_TIPO = 'C'
        .w_STAMPA = '1'
        .w_SAVE = .F.
        .w_OK = .T.
        .w_TAGORE = 0
        .w_MCSERIALE = NVL(MCSERIALE,space(10))
        .op_MCSERIALE = .w_MCSERIALE
        .w_MCCODICE = NVL(MCCODICE,space(10))
          if link_1_3_joined
            this.w_MCCODICE = NVL(ANCODICE103,NVL(this.w_MCCODICE,space(10)))
            this.w_MCMOTORE = NVL(ANALIM103,space(1))
            this.w_Descri = NVL(ANDESCRI103,space(100))
            this.w_OREANAG = NVL(ANORE103,0)
            this.w_DATAANAG = NVL(cp_ToDate(ANULIN103),ctod("  /  /  "))
            this.w_TAGORE = NVL(ANOREMAN103,0)
          else
          .link_1_3('Load')
          endif
        .w_MCMOTORE = NVL(MCMOTORE,space(1))
        .w_MCORE = NVL(MCORE,0)
        .w_MCDATA = NVL(cp_ToDate(MCDATA),ctod("  /  /  "))
        .w_MCTOTORE = NVL(MCTOTORE,0)
        .w_MCTIPO = NVL(MCTIPO,space(1))
        .w_CHKERR = iif(.w_MCORE<>0,iif(.w_MCORE<.w_MCOREANAG,.T.,.F.),.F.)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .w_MCDTANAG = NVL(cp_ToDate(MCDTANAG),ctod("  /  /  "))
        .w_MCOREANAG = NVL(MCOREANAG,0)
        .w_TESTD = iif(.w_MCMOTORE='D','D','')
        .w_TESTE = iif(.w_MCMOTORE='E','E','')
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .w_MCFLTAG = NVL(MCFLTAG,space(1))
        .w_MCOPRX = NVL(MCOPRX,0)
        cp_LoadRecExtFlds(this,'MANM_CAR')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from MAND_CAR where MCSERIALE=KeySet.MCSERIALE
      *                            and CPROWNUM=KeySet.CPROWNUM
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.MAND_CAR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAND_CAR_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('MAND_CAR')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "MAND_CAR.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" MAND_CAR"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'MCSERIALE',this.w_MCSERIALE  )
        select * from (i_cTable) MAND_CAR where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_MCTOTORE = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_CPROWNUM = NVL(CPROWNUM,0)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MCDURORE = NVL(MCDURORE,0)
          .w_MCTINT = NVL(MCTINT,space(1))
          * evitabile
          *.link_2_4('Load')
          .w_MCCODINT = NVL(MCCODINT,space(10))
          * evitabile
          *.link_2_5('Load')
          .w_MCDESINT = NVL(MCDESINT,space(40))
        .w_CHKM = iif(.w_MCMOTORE='D','E','D')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_MCTOTORE = .w_MCTOTORE+.w_MCDURORE
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_CHKERR = iif(.w_MCORE<>0,iif(.w_MCORE<.w_MCOREANAG,.T.,.F.),.F.)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .w_TESTD = iif(.w_MCMOTORE='D','D','')
        .w_TESTE = iif(.w_MCMOTORE='E','E','')
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_39.enabled = .oPgFrm.Page1.oPag.oBtn_1_39.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_MCSERIALE=space(10)
      .w_OBSOTEST=ctod("  /  /  ")
      .w_MCCODICE=space(10)
      .w_MCMOTORE=space(1)
      .w_MCORE=0
      .w_MCDATA=ctod("  /  /  ")
      .w_MCTOTORE=0
      .w_CPROWNUM=0
      .w_CPROWORD=10
      .w_MCDURORE=0
      .w_MCTINT=space(1)
      .w_MCCODINT=space(10)
      .w_MCDESINT=space(40)
      .w_MCTIPO=space(1)
      .w_Descri=space(100)
      .w_OREANAG=0
      .w_DATAANAG=ctod("  /  /  ")
      .w_ALIMENTAZIONE=space(1)
      .w_CHKERR=.f.
      .w_MCDTANAG=ctod("  /  /  ")
      .w_MCOREANAG=0
      .w_TESTD=space(1)
      .w_TESTE=space(1)
      .w_CHKM=space(1)
      .w_TIPO=space(1)
      .w_STAMPA=space(1)
      .w_UTCC=0
      .w_UTCV=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_SAVE=.f.
      .w_OK=.f.
      .w_MCFLTAG=space(1)
      .w_MCOPRX=0
      .w_TAGORE=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_OBSOTEST = i_datsys
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_MCCODICE))
         .link_1_3('Full')
        endif
        .DoRTCalc(4,5,.f.)
        .w_MCDATA = i_datsys
        .DoRTCalc(7,10,.f.)
        .w_MCTINT = 'C'
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_MCTINT))
         .link_2_4('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_MCCODINT))
         .link_2_5('Full')
        endif
        .DoRTCalc(13,13,.f.)
        .w_MCTIPO = 'C'
        .DoRTCalc(15,17,.f.)
        .w_ALIMENTAZIONE = 'S'
        .w_CHKERR = iif(.w_MCORE<>0,iif(.w_MCORE<.w_MCOREANAG,.T.,.F.),.F.)
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .w_MCDTANAG = iif(UPPER(this.cfunction)='LOAD',.w_DATAANAG,.w_MCDTANAG)
        .w_MCOREANAG = iif(UPPER(this.cfunction)='LOAD',.w_OREANAG,.w_MCOREANAG)
        .w_TESTD = iif(.w_MCMOTORE='D','D','')
        .w_TESTE = iif(.w_MCMOTORE='E','E','')
        .w_CHKM = iif(.w_MCMOTORE='D','E','D')
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .w_TIPO = 'C'
        .w_STAMPA = '1'
        .DoRTCalc(27,30,.f.)
        .w_SAVE = .F.
        .w_OK = .T.
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .w_MCFLTAG = 'N'
        .w_MCOPRX = iif(.w_MCFLTAG='S',.w_MCORE+.w_TAGORE,0)
      endif
    endwith
    cp_BlankRecExtFlds(this,'MANM_CAR')
    this.DoRTCalc(35,35,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMCSERIALE_1_1.enabled = !i_bVal
      .Page1.oPag.oMCCODICE_1_3.enabled = i_bVal
      .Page1.oPag.oMCORE_1_6.enabled = i_bVal
      .Page1.oPag.oMCDATA_1_8.enabled = i_bVal
      .Page1.oPag.oMCTOTORE_3_1.enabled = i_bVal
      .Page1.oPag.oMCFLTAG_1_41.enabled = i_bVal
      .Page1.oPag.oBtn_1_39.enabled = .Page1.oPag.oBtn_1_39.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oMCORE_1_6.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MANM_CAR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MANM_CAR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MANM_CAR_IDX,2])
    cp_AskTableProg(this,i_nConn,"mancar","w_MCSERIALE")
    with this
      .op_MCSERIALE = .w_MCSERIALE
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MANM_CAR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCSERIALE,"MCSERIALE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCODICE,"MCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCMOTORE,"MCMOTORE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCORE,"MCORE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCDATA,"MCDATA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCTOTORE,"MCTOTORE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCTIPO,"MCTIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCDTANAG,"MCDTANAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCOREANAG,"MCOREANAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCFLTAG,"MCFLTAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCOPRX,"MCOPRX",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MANM_CAR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MANM_CAR_IDX,2])
    i_lTable = "MANM_CAR"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MANM_CAR_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        GCSC_BSM(this,"C")
      endwith
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_MCDURORE N(5);
      ,t_MCTINT C(1);
      ,t_MCCODINT C(10);
      ,t_MCDESINT C(40);
      ,CPROWNUM N(10);
      ,t_CPROWNUM N(4);
      ,t_CHKM C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tmcgc_mgcbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCDURORE_2_3.controlsource=this.cTrsName+'.t_MCDURORE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCTINT_2_4.controlsource=this.cTrsName+'.t_MCTINT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCCODINT_2_5.controlsource=this.cTrsName+'.t_MCCODINT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCDESINT_2_6.controlsource=this.cTrsName+'.t_MCDESINT'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(70)
    this.AddVLine(128)
    this.AddVLine(161)
    this.AddVLine(254)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MANM_CAR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MANM_CAR_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"mancar","w_MCSERIALE")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MANM_CAR
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MANM_CAR')
        i_extval=cp_InsertValODBCExtFlds(this,'MANM_CAR')
        local i_cFld
        i_cFld=" "+;
                  "(MCSERIALE,MCCODICE,MCMOTORE,MCORE,MCDATA"+;
                  ",MCTOTORE,MCTIPO,MCDTANAG,MCOREANAG,UTCC"+;
                  ",UTCV,UTDC,UTDV,MCFLTAG,MCOPRX"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_MCSERIALE)+;
                    ","+cp_ToStrODBCNull(this.w_MCCODICE)+;
                    ","+cp_ToStrODBC(this.w_MCMOTORE)+;
                    ","+cp_ToStrODBC(this.w_MCORE)+;
                    ","+cp_ToStrODBC(this.w_MCDATA)+;
                    ","+cp_ToStrODBC(this.w_MCTOTORE)+;
                    ","+cp_ToStrODBC(this.w_MCTIPO)+;
                    ","+cp_ToStrODBC(this.w_MCDTANAG)+;
                    ","+cp_ToStrODBC(this.w_MCOREANAG)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_MCFLTAG)+;
                    ","+cp_ToStrODBC(this.w_MCOPRX)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MANM_CAR')
        i_extval=cp_InsertValVFPExtFlds(this,'MANM_CAR')
        cp_CheckDeletedKey(i_cTable,0,'MCSERIALE',this.w_MCSERIALE)
        INSERT INTO (i_cTable);
              (MCSERIALE,MCCODICE,MCMOTORE,MCORE,MCDATA,MCTOTORE,MCTIPO,MCDTANAG,MCOREANAG,UTCC,UTCV,UTDC,UTDV,MCFLTAG,MCOPRX &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_MCSERIALE;
                  ,this.w_MCCODICE;
                  ,this.w_MCMOTORE;
                  ,this.w_MCORE;
                  ,this.w_MCDATA;
                  ,this.w_MCTOTORE;
                  ,this.w_MCTIPO;
                  ,this.w_MCDTANAG;
                  ,this.w_MCOREANAG;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_MCFLTAG;
                  ,this.w_MCOPRX;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MAND_CAR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAND_CAR_IDX,2])
      *
      * insert into MAND_CAR
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(MCSERIALE,CPROWORD,MCDURORE,MCTINT,MCCODINT"+;
                  ",MCDESINT,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MCSERIALE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_MCDURORE)+","+cp_ToStrODBCNull(this.w_MCTINT)+","+cp_ToStrODBCNull(this.w_MCCODINT)+;
             ","+cp_ToStrODBC(this.w_MCDESINT)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MCSERIALE',this.w_MCSERIALE,'CPROWNUM',this.w_CPROWNUM)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_MCSERIALE,this.w_CPROWORD,this.w_MCDURORE,this.w_MCTINT,this.w_MCCODINT"+;
                ",this.w_MCDESINT,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.MANM_CAR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MANM_CAR_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update MANM_CAR
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'MANM_CAR')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " MCCODICE="+cp_ToStrODBCNull(this.w_MCCODICE)+;
             ",MCMOTORE="+cp_ToStrODBC(this.w_MCMOTORE)+;
             ",MCORE="+cp_ToStrODBC(this.w_MCORE)+;
             ",MCDATA="+cp_ToStrODBC(this.w_MCDATA)+;
             ",MCTOTORE="+cp_ToStrODBC(this.w_MCTOTORE)+;
             ",MCTIPO="+cp_ToStrODBC(this.w_MCTIPO)+;
             ",MCDTANAG="+cp_ToStrODBC(this.w_MCDTANAG)+;
             ",MCOREANAG="+cp_ToStrODBC(this.w_MCOREANAG)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",MCFLTAG="+cp_ToStrODBC(this.w_MCFLTAG)+;
             ",MCOPRX="+cp_ToStrODBC(this.w_MCOPRX)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'MANM_CAR')
          i_cWhere = cp_PKFox(i_cTable  ,'MCSERIALE',this.w_MCSERIALE  )
          UPDATE (i_cTable) SET;
              MCCODICE=this.w_MCCODICE;
             ,MCMOTORE=this.w_MCMOTORE;
             ,MCORE=this.w_MCORE;
             ,MCDATA=this.w_MCDATA;
             ,MCTOTORE=this.w_MCTOTORE;
             ,MCTIPO=this.w_MCTIPO;
             ,MCDTANAG=this.w_MCDTANAG;
             ,MCOREANAG=this.w_MCOREANAG;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,MCFLTAG=this.w_MCFLTAG;
             ,MCOPRX=this.w_MCOPRX;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_MCDESINT))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.MAND_CAR_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.MAND_CAR_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from MAND_CAR
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MAND_CAR
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MCDURORE="+cp_ToStrODBC(this.w_MCDURORE)+;
                     ",MCTINT="+cp_ToStrODBCNull(this.w_MCTINT)+;
                     ",MCCODINT="+cp_ToStrODBCNull(this.w_MCCODINT)+;
                     ",MCDESINT="+cp_ToStrODBC(this.w_MCDESINT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,MCDURORE=this.w_MCDURORE;
                     ,MCTINT=this.w_MCTINT;
                     ,MCCODINT=this.w_MCCODINT;
                     ,MCDESINT=this.w_MCDESINT;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_MCDESINT))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.MAND_CAR_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MAND_CAR_IDX,2])
        *
        * delete MAND_CAR
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.MANM_CAR_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MANM_CAR_IDX,2])
        *
        * delete MANM_CAR
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_MCDESINT))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- mcgc_mgc
    this.NotifyEvent('Ripristino')
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MANM_CAR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MANM_CAR_IDX,2])
    if i_bUpd
      this.bHeaderUpdated=.t. && Totalized fields
      with this
        .DoRTCalc(1,18,.t.)
        if .o_MCORE<>.w_MCORE.or. .o_OREANAG<>.w_OREANAG
          .w_CHKERR = iif(.w_MCORE<>0,iif(.w_MCORE<.w_MCOREANAG,.T.,.F.),.F.)
        endif
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        if .o_DATAANAG<>.w_DATAANAG
          .w_MCDTANAG = iif(UPPER(this.cfunction)='LOAD',.w_DATAANAG,.w_MCDTANAG)
        endif
        if .o_OREANAG<>.w_OREANAG
          .w_MCOREANAG = iif(UPPER(this.cfunction)='LOAD',.w_OREANAG,.w_MCOREANAG)
        endif
          .w_TESTD = iif(.w_MCMOTORE='D','D','')
          .w_TESTE = iif(.w_MCMOTORE='E','E','')
        if .o_MCMOTORE<>.w_MCMOTORE
          .w_CHKM = iif(.w_MCMOTORE='D','E','D')
        endif
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .DoRTCalc(25,33,.t.)
        if .o_MCORE<>.w_MCORE.or. .o_TAGORE<>.w_TAGORE.or. .o_MCFLTAG<>.w_MCFLTAG
          .w_MCOPRX = iif(.w_MCFLTAG='S',.w_MCORE+.w_TAGORE,0)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(35,35,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CPROWNUM with this.w_CPROWNUM
      replace t_CHKM with this.w_CHKM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.mEnableControlsFixed()
    this.mHideControls()
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oMCFLTAG_1_41.visible=!this.oPgFrm.Page1.oPag.oMCFLTAG_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oMCOPRX_1_43.visible=!this.oPgFrm.Page1.oPag.oMCOPRX_1_43.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MCCODICE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
    i_lTable = "ANAGRAFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2], .t., this.ANAGRAFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('ANME_GEME',True,'ANAGRAFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MCCODICE)+"%");
                   +" and ANTIPO="+cp_ToStrODBC(this.w_MCTIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPO,ANCODICE,ANALIM,ANDESCRI,ANORE,ANULIN,ANOREMAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPO,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPO',this.w_MCTIPO;
                     ,'ANCODICE',trim(this.w_MCCODICE))
          select ANTIPO,ANCODICE,ANALIM,ANDESCRI,ANORE,ANULIN,ANOREMAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPO,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCCODICE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCCODICE) and !this.bDontReportError
            deferred_cp_zoom('ANAGRAFE','*','ANTIPO,ANCODICE',cp_AbsName(oSource.parent,'oMCCODICE_1_3'),i_cWhere,'ANME_GEME',"Seleziona Numero Carrello",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MCTIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPO,ANCODICE,ANALIM,ANDESCRI,ANORE,ANULIN,ANOREMAN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPO,ANCODICE,ANALIM,ANDESCRI,ANORE,ANULIN,ANOREMAN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPO,ANCODICE,ANALIM,ANDESCRI,ANORE,ANULIN,ANOREMAN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPO="+cp_ToStrODBC(this.w_MCTIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPO',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPO,ANCODICE,ANALIM,ANDESCRI,ANORE,ANULIN,ANOREMAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPO,ANCODICE,ANALIM,ANDESCRI,ANORE,ANULIN,ANOREMAN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MCCODICE);
                   +" and ANTIPO="+cp_ToStrODBC(this.w_MCTIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPO',this.w_MCTIPO;
                       ,'ANCODICE',this.w_MCCODICE)
            select ANTIPO,ANCODICE,ANALIM,ANDESCRI,ANORE,ANULIN,ANOREMAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCODICE = NVL(_Link_.ANCODICE,space(10))
      this.w_MCMOTORE = NVL(_Link_.ANALIM,space(1))
      this.w_Descri = NVL(_Link_.ANDESCRI,space(100))
      this.w_OREANAG = NVL(_Link_.ANORE,0)
      this.w_DATAANAG = NVL(cp_ToDate(_Link_.ANULIN),ctod("  /  /  "))
      this.w_TAGORE = NVL(_Link_.ANOREMAN,0)
    else
      if i_cCtrl<>'Load'
        this.w_MCCODICE = space(10)
      endif
      this.w_MCMOTORE = space(1)
      this.w_Descri = space(100)
      this.w_OREANAG = 0
      this.w_DATAANAG = ctod("  /  /  ")
      this.w_TAGORE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPO,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.ANAGRAFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ANAGRAFE_IDX,3] and i_nFlds+6<200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.ANCODICE as ANCODICE103"+ ",link_1_3.ANALIM as ANALIM103"+ ",link_1_3.ANDESCRI as ANDESCRI103"+ ",link_1_3.ANORE as ANORE103"+ ",link_1_3.ANULIN as ANULIN103"+ ",link_1_3.ANOREMAN as ANOREMAN103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on MANM_CAR.MCCODICE=link_1_3.ANCODICE"+" and MANM_CAR.MCTIPO=link_1_3.ANTIPO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and MANM_CAR.MCCODICE=link_1_3.ANCODICE(+)"'+'+" and MANM_CAR.MCTIPO=link_1_3.ANTIPO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MCTINT
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPOINTERVENTO_IDX,3]
    i_lTable = "TIPOINTERVENTO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPOINTERVENTO_IDX,2], .t., this.TIPOINTERVENTO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPOINTERVENTO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCTINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('MICG_MMI',True,'TIPOINTERVENTO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MICODICE like "+cp_ToStrODBC(trim(this.w_MCTINT)+"%");

          i_ret=cp_SQL(i_nConn,"select MICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MICODICE',trim(this.w_MCTINT))
          select MICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCTINT)==trim(_Link_.MICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCTINT) and !this.bDontReportError
            deferred_cp_zoom('TIPOINTERVENTO','*','MICODICE',cp_AbsName(oSource.parent,'oMCTINT_2_4'),i_cWhere,'MICG_MMI',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where MICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MICODICE',oSource.xKey(1))
            select MICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCTINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where MICODICE="+cp_ToStrODBC(this.w_MCTINT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MICODICE',this.w_MCTINT)
            select MICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCTINT = NVL(_Link_.MICODICE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MCTINT = space(1)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPOINTERVENTO_IDX,2])+'\'+cp_ToStr(_Link_.MICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPOINTERVENTO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCTINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MCCODINT
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INTERVENTI_IDX,3]
    i_lTable = "INTERVENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2], .t., this.INTERVENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCODINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('zoomint',True,'INTERVENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_MCCODINT)+"%");
                   +" and TIMEZZO="+cp_ToStrODBC(this.w_MCTIPO);
                   +" and TITIPO="+cp_ToStrODBC(this.w_MCTINT);
                   +" and TIMOTORE="+cp_ToStrODBC(this.w_CHKM);

          i_ret=cp_SQL(i_nConn,"select TIMEZZO,TITIPO,TIMOTORE,TICODICE,TIDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TIMEZZO,TITIPO,TIMOTORE,TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TIMEZZO',this.w_MCTIPO;
                     ,'TITIPO',this.w_MCTINT;
                     ,'TIMOTORE',this.w_CHKM;
                     ,'TICODICE',trim(this.w_MCCODINT))
          select TIMEZZO,TITIPO,TIMOTORE,TICODICE,TIDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TIMEZZO,TITIPO,TIMOTORE,TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCCODINT)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCCODINT) and !this.bDontReportError
            deferred_cp_zoom('INTERVENTI','*','TIMEZZO,TITIPO,TIMOTORE,TICODICE',cp_AbsName(oSource.parent,'oMCCODINT_2_5'),i_cWhere,'zoomint',"Seleziona Tipo Intervento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MCTIPO<>oSource.xKey(1);
           .or. this.w_MCTINT<>oSource.xKey(2);
           .or. this.w_CHKM<>oSource.xKey(3);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TIMEZZO,TITIPO,TIMOTORE,TICODICE,TIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TIMEZZO,TITIPO,TIMOTORE,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TIMEZZO,TITIPO,TIMOTORE,TICODICE,TIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(4));
                     +" and TIMEZZO="+cp_ToStrODBC(this.w_MCTIPO);
                     +" and TITIPO="+cp_ToStrODBC(this.w_MCTINT);
                     +" and TIMOTORE="+cp_ToStrODBC(this.w_CHKM);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TIMEZZO',oSource.xKey(1);
                       ,'TITIPO',oSource.xKey(2);
                       ,'TIMOTORE',oSource.xKey(3);
                       ,'TICODICE',oSource.xKey(4))
            select TIMEZZO,TITIPO,TIMOTORE,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCODINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TIMEZZO,TITIPO,TIMOTORE,TICODICE,TIDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_MCCODINT);
                   +" and TIMEZZO="+cp_ToStrODBC(this.w_MCTIPO);
                   +" and TITIPO="+cp_ToStrODBC(this.w_MCTINT);
                   +" and TIMOTORE="+cp_ToStrODBC(this.w_CHKM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TIMEZZO',this.w_MCTIPO;
                       ,'TITIPO',this.w_MCTINT;
                       ,'TIMOTORE',this.w_CHKM;
                       ,'TICODICE',this.w_MCCODINT)
            select TIMEZZO,TITIPO,TIMOTORE,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCODINT = NVL(_Link_.TICODICE,space(10))
      this.w_MCDESINT = NVL(_Link_.TIDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MCCODINT = space(10)
      endif
      this.w_MCDESINT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2])+'\'+cp_ToStr(_Link_.TIMEZZO,1)+'\'+cp_ToStr(_Link_.TITIPO,1)+'\'+cp_ToStr(_Link_.TIMOTORE,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.INTERVENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCODINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMCSERIALE_1_1.value==this.w_MCSERIALE)
      this.oPgFrm.Page1.oPag.oMCSERIALE_1_1.value=this.w_MCSERIALE
    endif
    if not(this.oPgFrm.Page1.oPag.oMCCODICE_1_3.value==this.w_MCCODICE)
      this.oPgFrm.Page1.oPag.oMCCODICE_1_3.value=this.w_MCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMCORE_1_6.value==this.w_MCORE)
      this.oPgFrm.Page1.oPag.oMCORE_1_6.value=this.w_MCORE
    endif
    if not(this.oPgFrm.Page1.oPag.oMCDATA_1_8.value==this.w_MCDATA)
      this.oPgFrm.Page1.oPag.oMCDATA_1_8.value=this.w_MCDATA
    endif
    if not(this.oPgFrm.Page1.oPag.oMCTOTORE_3_1.value==this.w_MCTOTORE)
      this.oPgFrm.Page1.oPag.oMCTOTORE_3_1.value=this.w_MCTOTORE
    endif
    if not(this.oPgFrm.Page1.oPag.oDescri_1_16.value==this.w_Descri)
      this.oPgFrm.Page1.oPag.oDescri_1_16.value=this.w_Descri
    endif
    if not(this.oPgFrm.Page1.oPag.oOREANAG_1_17.value==this.w_OREANAG)
      this.oPgFrm.Page1.oPag.oOREANAG_1_17.value=this.w_OREANAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAANAG_1_19.value==this.w_DATAANAG)
      this.oPgFrm.Page1.oPag.oDATAANAG_1_19.value=this.w_DATAANAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMCDTANAG_1_24.value==this.w_MCDTANAG)
      this.oPgFrm.Page1.oPag.oMCDTANAG_1_24.value=this.w_MCDTANAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMCOREANAG_1_25.value==this.w_MCOREANAG)
      this.oPgFrm.Page1.oPag.oMCOREANAG_1_25.value=this.w_MCOREANAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMCFLTAG_1_41.RadioValue()==this.w_MCFLTAG)
      this.oPgFrm.Page1.oPag.oMCFLTAG_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMCOPRX_1_43.value==this.w_MCOPRX)
      this.oPgFrm.Page1.oPag.oMCOPRX_1_43.value=this.w_MCOPRX
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCDURORE_2_3.value==this.w_MCDURORE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCDURORE_2_3.value=this.w_MCDURORE
      replace t_MCDURORE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCDURORE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCTINT_2_4.value==this.w_MCTINT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCTINT_2_4.value=this.w_MCTINT
      replace t_MCTINT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCTINT_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCODINT_2_5.value==this.w_MCCODINT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCODINT_2_5.value=this.w_MCCODINT
      replace t_MCCODINT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCODINT_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCDESINT_2_6.value==this.w_MCDESINT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCDESINT_2_6.value=this.w_MCDESINT
      replace t_MCDESINT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCDESINT_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'MANM_CAR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MCCODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMCCODICE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_MCCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MCORE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMCORE_1_6.SetFocus()
            i_bnoObbl = !empty(.w_MCORE)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- mcgc_mgc
      i_bRes=.T.
      IF this.cFunction='Load'
          this.w_UTCC=i_codute
          this.w_UTDC=datetime()
      ENDIF
      IF this.cFunction='Edit'
          this.w_UTCV=i_codute
          this.w_UTDV=datetime()
      ENDIF
      this.NotifyEvent('Archivio')
      IF this.w_OK = .F.
          i_bRes=.F.
      ENDIF
      this.w_OK = .T.
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_MCDESINT))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MCMOTORE = this.w_MCMOTORE
    this.o_MCORE = this.w_MCORE
    this.o_OREANAG = this.w_OREANAG
    this.o_DATAANAG = this.w_DATAANAG
    this.o_MCFLTAG = this.w_MCFLTAG
    this.o_TAGORE = this.w_TAGORE
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_MCDESINT)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_MCDURORE=0
      .w_MCTINT=space(1)
      .w_MCCODINT=space(10)
      .w_MCDESINT=space(40)
      .w_CHKM=space(1)
      .DoRTCalc(1,10,.f.)
        .w_MCTINT = 'C'
      .DoRTCalc(11,11,.f.)
      if not(empty(.w_MCTINT))
        .link_2_4('Full')
      endif
      .DoRTCalc(12,12,.f.)
      if not(empty(.w_MCCODINT))
        .link_2_5('Full')
      endif
      .DoRTCalc(13,23,.f.)
        .w_CHKM = iif(.w_MCMOTORE='D','E','D')
    endwith
    this.DoRTCalc(25,35,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWNUM = t_CPROWNUM
    this.w_CPROWORD = t_CPROWORD
    this.w_MCDURORE = t_MCDURORE
    this.w_MCTINT = t_MCTINT
    this.w_MCCODINT = t_MCCODINT
    this.w_MCDESINT = t_MCDESINT
    this.w_CHKM = t_CHKM
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWNUM with this.w_CPROWNUM
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MCDURORE with this.w_MCDURORE
    replace t_MCTINT with this.w_MCTINT
    replace t_MCCODINT with this.w_MCCODINT
    replace t_MCDESINT with this.w_MCDESINT
    replace t_CHKM with this.w_CHKM
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_MCTOTORE = .w_MCTOTORE-.w_mcdurore
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tmcgc_mgcPag1 as StdContainer
  Width  = 568
  height = 451
  stdWidth  = 568
  stdheight = 451
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMCSERIALE_1_1 as StdField with uid="RHBZKWAHEF",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MCSERIALE", cQueryName = "MCSERIALE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 56616344,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=60, Top=9, InputMask=replicate('X',10)

  add object oMCCODICE_1_3 as StdField with uid="TEIFZEXZGC",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MCCODICE", cQueryName = "MCCODICE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 197727761,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=60, Top=33, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="ANAGRAFE", cZoomOnZoom="ANME_GEME", oKey_1_1="ANTIPO", oKey_1_2="this.w_MCTIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_MCCODICE"

  func oMCCODICE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCCODICE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMCCODICE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ANAGRAFE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPO="+cp_ToStrODBC(this.Parent.oContained.w_MCTIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPO="+cp_ToStr(this.Parent.oContained.w_MCTIPO)
    endif
    do cp_zoom with 'ANAGRAFE','*','ANTIPO,ANCODICE',cp_AbsName(this.parent,'oMCCODICE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'ANME_GEME',"Seleziona Numero Carrello",'',this.parent.oContained
  endproc
  proc oMCCODICE_1_3.mZoomOnZoom
    local i_obj
    i_obj=ANME_GEME()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPO=w_MCTIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_MCCODICE
    i_obj.ecpSave()
  endproc

  add object oMCORE_1_6 as StdField with uid="TMZZIZOYWT",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MCORE", cQueryName = "MCORE",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 220408372,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=40, Top=141, cSayPict='"999999999"', cGetPict='"999999999"'

  add object oMCDATA_1_8 as StdField with uid="BFFUIXAMWI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MCDATA", cQueryName = "MCDATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 189061684,;
   bGlobalFont=.t.,;
    Height=21, Width=89, Left=228, Top=141

  add object oDescri_1_16 as StdField with uid="BBMUBRNBHZ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_Descri", cQueryName = "Descri",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 247478076,;
   bGlobalFont=.t.,;
    Height=21, Width=391, Left=166, Top=33, InputMask=replicate('X',100)

  add object oOREANAG_1_17 as StdField with uid="GLUQLCHCVG",rtseq=16,rtrep=.f.,;
    cFormVar = "w_OREANAG", cQueryName = "OREANAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 73090284,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=295, Top=58

  add object oDATAANAG_1_19 as StdField with uid="NQLXCFFQIB",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DATAANAG", cQueryName = "DATAANAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 9184131,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=373, Top=58


  add object oObj_1_23 as cp_runprogram with uid="TXZIDGFNAS",left=16, top=500, width=50,height=50,;
    caption='Object',;
    prg="MCGM_BGM('A')",;
    cEvent = "Archivio",;
    nPag=1;
    , HelpContextID = 147956972;
  , bGlobalFont=.t.


  add object oMCDTANAG_1_24 as StdField with uid="YKACDGSUVC",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MCDTANAG", cQueryName = "MCDTANAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 10364435,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=373, Top=86

  add object oMCOREANAG_1_25 as StdField with uid="XFFCXKMSWP",rtseq=21,rtrep=.f.,;
    cFormVar = "w_MCOREANAG", cQueryName = "MCOREANAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 203629955,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=295, Top=86


  add object oObj_1_29 as cp_runprogram with uid="SCZZXXOYCH",left=87, top=500, width=50,height=50,;
    caption='Object',;
    prg="MCGM_BGM('E')",;
    cEvent = "Ripristino",;
    nPag=1;
    , HelpContextID = 147956972;
  , bGlobalFont=.t.



  add object oBtn_1_39 as StdButton with uid="RIGYLUATQM",left=424, top=409, width=116,height=25,;
    caption="Salva / Stampa", nPag=1;
    , ToolTipText = "Cliccare per Salvare e Stampare";
    , HelpContextID = 221662588;
  , bGlobalFont=.t.

    proc oBtn_1_39.Click()
      with this.Parent.oContained
        GCSC_BSM(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_39.mCond()
    with this.Parent.oContained
      return (.not.empty(.w_MCCODINT) or .w_SAVE=.T.)
    endwith
  endfunc


  add object oObj_1_40 as cp_runprogram with uid="HXQBIZKGCM",left=156, top=500, width=50,height=50,;
    caption='Object',;
    prg="MCGM_BGM('G')",;
    cEvent = "Insert row end,Init Row",;
    nPag=1;
    , HelpContextID = 147956972;
  , bGlobalFont=.t.


  add object oMCFLTAG_1_41 as StdCheck with uid="MOKJIZIYFR",rtseq=33,rtrep=.f.,left=424, top=139, caption="",;
    HelpContextID = 80102860,;
    cFormVar="w_MCFLTAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMCFLTAG_1_41.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MCFLTAG,&i_cF..t_MCFLTAG),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oMCFLTAG_1_41.GetRadio()
    this.Parent.oContained.w_MCFLTAG = this.RadioValue()
    return .t.
  endfunc

  func oMCFLTAG_1_41.ToRadio()
    this.Parent.oContained.w_MCFLTAG=trim(this.Parent.oContained.w_MCFLTAG)
    return(;
      iif(this.Parent.oContained.w_MCFLTAG=='S',1,;
      0))
  endfunc

  func oMCFLTAG_1_41.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oMCFLTAG_1_41.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MCMOTORE<>'D')
    endwith
    endif
  endfunc

  add object oMCOPRX_1_43 as StdField with uid="JCGNNGXKBS",rtseq=34,rtrep=.f.,;
    cFormVar = "w_MCOPRX", cQueryName = "MCOPRX",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 72690228,;
   bGlobalFont=.t.,;
    Height=21, Width=89, Left=444, Top=140

  func oMCOPRX_1_43.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MCMOTORE<>'D')
    endwith
    endif
  endfunc

  add object oStr_1_4 as StdString with uid="ABAZTHHJJZ",Visible=.t., Left=7, Top=37,;
    Alignment=1, Width=52, Height=18,;
    Caption="Carrello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="IHUQIMNFDL",Visible=.t., Left=16, Top=145,;
    Alignment=1, Width=23, Height=18,;
    Caption="Ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="LLSGSCSGFZ",Visible=.t., Left=119, Top=145,;
    Alignment=1, Width=108, Height=18,;
    Caption="Data Manutenzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="QEPNZJMMYH",Visible=.t., Left=20, Top=173,;
    Alignment=0, Width=49, Height=18,;
    Caption="Interventi"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="GUIQDXMEEN",Visible=.t., Left=73, Top=173,;
    Alignment=0, Width=38, Height=18,;
    Caption="Durata"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="BCLBPPDQBV",Visible=.t., Left=163, Top=173,;
    Alignment=0, Width=47, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="LBEBPGXIGV",Visible=.t., Left=255, Top=173,;
    Alignment=0, Width=67, Height=18,;
    Caption="Descrizione"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="KUDTMVGLWF",Visible=.t., Left=129, Top=173,;
    Alignment=0, Width=27, Height=18,;
    Caption="Tipo"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="PPYSADOYSW",Visible=.t., Left=39, Top=62,;
    Alignment=1, Width=254, Height=18,;
    Caption="Ore/Data Carrello Ultimo Intervento Anagrafica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="VJSUTITKOM",Visible=.t., Left=42, Top=116,;
    Alignment=0, Width=384, Height=19,;
    Caption="ATTENZIONE VERIFICARE LE ORE DEL CARRELLO INSERITE"  ;
    , FontName = "Times New Roman", FontSize = 10, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_CHKERR=.F.)
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="UISIECUOFL",Visible=.t., Left=25, Top=90,;
    Alignment=1, Width=268, Height=18,;
    Caption="Ore/Data Carrello Ultimo Intervento Caricamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="GRSYKXTWZD",Visible=.t., Left=17, Top=13,;
    Alignment=1, Width=42, Height=18,;
    Caption="Seriale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="ADZGMIRZOT",Visible=.t., Left=357, Top=144,;
    Alignment=1, Width=63, Height=18,;
    Caption="Tagliando:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (.w_MCMOTORE<>'D')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=10,top=195,;
    width=533+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=11,top=196,width=532+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TIPOINTERVENTO|INTERVENTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TIPOINTERVENTO'
        oDropInto=this.oBodyCol.oRow.oMCTINT_2_4
      case cFile='INTERVENTI'
        oDropInto=this.oBodyCol.oRow.oMCCODINT_2_5
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oMCTOTORE_3_1 as StdField with uid="NHHAYCVJMT",rtseq=7,rtrep=.f.,;
    cFormVar="w_MCTOTORE",value=0,;
    HelpContextID = 46802449,;
    cQueryName = "MCTOTORE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=115, Top=412, cSayPict=["99999"], cGetPict=["99999"]

  add object oStr_3_2 as StdString with uid="SCEMOMJNKR",Visible=.t., Left=38, Top=416,;
    Alignment=1, Width=76, Height=18,;
    Caption="Durata Totale:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tmcgc_mgcBodyRow as container
  Width=523
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_2 as StdTrsField with uid="KLVERRRSDT",rtseq=9,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 49943152,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oMCDURORE_2_3 as StdTrsField with uid="PBYDSLPBZU",rtseq=10,rtrep=.t.,;
    cFormVar="w_MCDURORE",value=0,;
    HelpContextID = 45032977,;
    cTotal = "this.Parent.oContained.w_mctotore", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=54, Left=51, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oMCTINT_2_4 as StdTrsField with uid="SEBULMXMDA",rtseq=11,rtrep=.t.,;
    cFormVar="w_MCTINT",value=space(1),;
    HelpContextID = 144431668,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=30, Left=108, Top=0, InputMask=replicate('X',1), bHasZoom = .t. , cLinkFile="TIPOINTERVENTO", cZoomOnZoom="MICG_MMI", oKey_1_1="MICODICE", oKey_1_2="this.w_MCTINT"

  func oMCTINT_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
      if .not. empty(.w_MCCODINT)
        bRes2=.link_2_5('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMCTINT_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMCTINT_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPOINTERVENTO','*','MICODICE',cp_AbsName(this.parent,'oMCTINT_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'MICG_MMI',"",'',this.parent.oContained
  endproc
  proc oMCTINT_2_4.mZoomOnZoom
    local i_obj
    i_obj=MICG_MMI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MICODICE=this.parent.oContained.w_MCTINT
    i_obj.ecpSave()
  endproc

  add object oMCCODINT_2_5 as StdTrsField with uid="XFRIDIMIIT",rtseq=12,rtrep=.t.,;
    cFormVar="w_MCCODINT",value=space(10),;
    HelpContextID = 70707680,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=90, Left=141, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="INTERVENTI", cZoomOnZoom="zoomint", oKey_1_1="TIMEZZO", oKey_1_2="this.w_MCTIPO", oKey_2_1="TITIPO", oKey_2_2="this.w_MCTINT", oKey_3_1="TIMOTORE", oKey_3_2="this.w_CHKM", oKey_4_1="TICODICE", oKey_4_2="this.w_MCCODINT"

  func oMCCODINT_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCCODINT_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMCCODINT_2_5.mZoom
    do TIZI_KTI with this.Parent.oContained
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oMCCODINT_2_5.mZoomOnZoom
    local i_obj
    i_obj=zoomint()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TIMEZZO=w_MCTIPO
    i_obj.TITIPO=w_MCTINT
    i_obj.TIMOTORE=w_CHKM
     i_obj.w_TICODICE=this.parent.oContained.w_MCCODINT
    i_obj.ecpSave()
  endproc

  add object oMCDESINT_2_6 as StdTrsField with uid="CROVOFVIED",rtseq=13,rtrep=.t.,;
    cFormVar="w_MCDESINT",value=space(40),;
    HelpContextID = 55630304,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=285, Left=233, Top=0, cSayPict=["!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"], cGetPict=["!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"], InputMask=replicate('X',40)
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_2.When()
    return(.t.)
  proc oCPROWORD_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('mcgc_mgc','MANM_CAR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MCSERIALE=MANM_CAR.MCSERIALE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
