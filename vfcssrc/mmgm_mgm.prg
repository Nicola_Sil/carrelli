* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: mmgm_mgm                                                        *
*              Manutenzione Mezzi / Veicoli                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-07-30                                                      *
* Last revis.: 2016-03-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tmmgm_mgm"))

* --- Class definition
define class tmmgm_mgm as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 593
  Height = 424+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-03-31"
  HelpContextID=238435901
  max_rt_seq=29

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  MANM_MEZZ_IDX = 0
  MAND_MEZZ_IDX = 0
  ANAGRAFE_IDX = 0
  INTERVENTI_IDX = 0
  TIPOINTERVENTO_IDX = 0
  cFile = "MANM_MEZZ"
  cFileDetail = "MAND_MEZZ"
  cKeySelect = "MMSERIALE"
  cKeyWhere  = "MMSERIALE=this.w_MMSERIALE"
  cKeyDetail  = "MMSERIALE=this.w_MMSERIALE and CPROWNUM=this.w_CPROWNUM"
  cKeyWhereODBC = '"MMSERIALE="+cp_ToStrODBC(this.w_MMSERIALE)';

  cKeyDetailWhereODBC = '"MMSERIALE="+cp_ToStrODBC(this.w_MMSERIALE)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"MAND_MEZZ.MMSERIALE="+cp_ToStrODBC(this.w_MMSERIALE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'MAND_MEZZ.CPROWORD '
  cPrg = "mmgm_mgm"
  cComment = "Manutenzione Mezzi / Veicoli"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MMSERIALE = space(10)
  w_OBSOTEST = ctod('  /  /  ')
  w_MMCODICE = space(10)
  w_MMMOTORE = space(1)
  w_MMORE = 0
  o_MMORE = 0
  w_MMDATA = ctod('  /  /  ')
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_MMDURORE = 0
  w_MMTINT = space(1)
  w_MMCODINT = space(10)
  w_MMDESINT = space(40)
  w_Descri = space(100)
  w_OREANAG = 0
  o_OREANAG = 0
  w_DATAANAG = ctod('  /  /  ')
  o_DATAANAG = ctod('  /  /  ')
  w_MMDTANAG = ctod('  /  /  ')
  w_MMOREANAG = 0
  w_MMTOTORE = 0
  w_MMTIPO = space(1)
  w_CHKERR = .F.
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_SAVE = .F.
  w_OK = .F.
  w_MMFLTAG = space(1)
  o_MMFLTAG = space(1)
  w_MMOPRX = 0
  w_MTAGORE = 0
  o_MTAGORE = 0

  * --- Autonumbered Variables
  op_MMSERIALE = this.W_MMSERIALE
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MANM_MEZZ','mmgm_mgm')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tmmgm_mgmPag1","mmgm_mgm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Manutenzione")
      .Pages(1).HelpContextID = 77669770
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMMSERIALE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='ANAGRAFE'
    this.cWorkTables[2]='INTERVENTI'
    this.cWorkTables[3]='TIPOINTERVENTO'
    this.cWorkTables[4]='MANM_MEZZ'
    this.cWorkTables[5]='MAND_MEZZ'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MANM_MEZZ_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MANM_MEZZ_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_MMSERIALE = NVL(MMSERIALE,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_3_joined
    link_1_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from MANM_MEZZ where MMSERIALE=KeySet.MMSERIALE
    *
    i_nConn = i_TableProp[this.MANM_MEZZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MANM_MEZZ_IDX,2],this.bLoadRecFilter,this.MANM_MEZZ_IDX,"mmgm_mgm")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MANM_MEZZ')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MANM_MEZZ.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"MAND_MEZZ.","MANM_MEZZ.")
      i_cTable = i_cTable+' MANM_MEZZ '
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MMSERIALE',this.w_MMSERIALE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_OBSOTEST = i_datsys
        .w_Descri = space(100)
        .w_OREANAG = 0
        .w_DATAANAG = ctod("  /  /  ")
        .w_SAVE = .F.
        .w_OK = .T.
        .w_MTAGORE = 0
        .w_MMSERIALE = NVL(MMSERIALE,space(10))
        .op_MMSERIALE = .w_MMSERIALE
        .w_MMCODICE = NVL(MMCODICE,space(10))
          if link_1_3_joined
            this.w_MMCODICE = NVL(ANCODICE103,NVL(this.w_MMCODICE,space(10)))
            this.w_Descri = NVL(ANDESCRI103,space(100))
            this.w_OREANAG = NVL(ANORE103,0)
            this.w_DATAANAG = NVL(cp_ToDate(ANULIN103),ctod("  /  /  "))
            this.w_MMMOTORE = NVL(ANALIM103,space(1))
            this.w_MTAGORE = NVL(ANOREMAN103,0)
          else
          .link_1_3('Load')
          endif
        .w_MMMOTORE = NVL(MMMOTORE,space(1))
        .w_MMORE = NVL(MMORE,0)
        .w_MMDATA = NVL(cp_ToDate(MMDATA),ctod("  /  /  "))
        .w_MMDTANAG = NVL(cp_ToDate(MMDTANAG),ctod("  /  /  "))
        .w_MMOREANAG = NVL(MMOREANAG,0)
        .w_MMTOTORE = NVL(MMTOTORE,0)
        .w_MMTIPO = NVL(MMTIPO,space(1))
        .w_CHKERR = iif(.w_MMORE<>0,iif(.w_MMORE<.w_MMOREANAG,.T.,.F.),.F.)
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .w_MMFLTAG = NVL(MMFLTAG,space(1))
        .w_MMOPRX = NVL(MMOPRX,0)
        cp_LoadRecExtFlds(this,'MANM_MEZZ')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from MAND_MEZZ where MMSERIALE=KeySet.MMSERIALE
      *                            and CPROWNUM=KeySet.CPROWNUM
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.MAND_MEZZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAND_MEZZ_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('MAND_MEZZ')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "MAND_MEZZ.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" MAND_MEZZ"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'MMSERIALE',this.w_MMSERIALE  )
        select * from (i_cTable) MAND_MEZZ where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_CPROWNUM = NVL(CPROWNUM,0)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MMDURORE = NVL(MMDURORE,0)
          .w_MMTINT = NVL(MMTINT,space(1))
          * evitabile
          *.link_2_4('Load')
          .w_MMCODINT = NVL(MMCODINT,space(10))
          * evitabile
          *.link_2_5('Load')
          .w_MMDESINT = NVL(MMDESINT,space(40))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_CHKERR = iif(.w_MMORE<>0,iif(.w_MMORE<.w_MMOREANAG,.T.,.F.),.F.)
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_32.enabled = .oPgFrm.Page1.oPag.oBtn_1_32.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_MMSERIALE=space(10)
      .w_OBSOTEST=ctod("  /  /  ")
      .w_MMCODICE=space(10)
      .w_MMMOTORE=space(1)
      .w_MMORE=0
      .w_MMDATA=ctod("  /  /  ")
      .w_CPROWNUM=0
      .w_CPROWORD=10
      .w_MMDURORE=0
      .w_MMTINT=space(1)
      .w_MMCODINT=space(10)
      .w_MMDESINT=space(40)
      .w_Descri=space(100)
      .w_OREANAG=0
      .w_DATAANAG=ctod("  /  /  ")
      .w_MMDTANAG=ctod("  /  /  ")
      .w_MMOREANAG=0
      .w_MMTOTORE=0
      .w_MMTIPO=space(1)
      .w_CHKERR=.f.
      .w_UTCC=0
      .w_UTCV=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_SAVE=.f.
      .w_OK=.f.
      .w_MMFLTAG=space(1)
      .w_MMOPRX=0
      .w_MTAGORE=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_OBSOTEST = i_datsys
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_MMCODICE))
         .link_1_3('Full')
        endif
        .DoRTCalc(4,5,.f.)
        .w_MMDATA = I_DATSYS
        .DoRTCalc(7,9,.f.)
        .w_MMTINT = 'S'
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_MMTINT))
         .link_2_4('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_MMCODINT))
         .link_2_5('Full')
        endif
        .DoRTCalc(12,15,.f.)
        .w_MMDTANAG = iif(UPPER(this.cfunction)='LOAD',.w_DATAANAG,.w_MMDTANAG)
        .w_MMOREANAG = iif(UPPER(this.cfunction)='LOAD',.w_OREANAG,.w_MMOREANAG)
        .w_MMTOTORE = .w_MMDURORE
        .w_MMTIPO = 'M'
        .w_CHKERR = iif(.w_MMORE<>0,iif(.w_MMORE<.w_MMOREANAG,.T.,.F.),.F.)
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .DoRTCalc(21,24,.f.)
        .w_SAVE = .F.
        .w_OK = .T.
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .w_MMFLTAG = 'N'
        .w_MMOPRX = iif(.w_MMFLTAG='S',.w_MMORE+.w_MTAGORE,0)
      endif
    endwith
    cp_BlankRecExtFlds(this,'MANM_MEZZ')
    this.DoRTCalc(29,29,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMMSERIALE_1_1.enabled = !i_bVal
      .Page1.oPag.oMMCODICE_1_3.enabled = i_bVal
      .Page1.oPag.oMMORE_1_6.enabled = i_bVal
      .Page1.oPag.oMMDATA_1_7.enabled = i_bVal
      .Page1.oPag.oMMFLTAG_1_36.enabled = i_bVal
      .Page1.oPag.oBtn_1_32.enabled = .Page1.oPag.oBtn_1_32.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'MANM_MEZZ',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MANM_MEZZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MANM_MEZZ_IDX,2])
    cp_AskTableProg(this,i_nConn,"manmez","w_MMSERIALE")
    with this
      .op_MMSERIALE = .w_MMSERIALE
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MANM_MEZZ_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMSERIALE,"MMSERIALE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMCODICE,"MMCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMMOTORE,"MMMOTORE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMORE,"MMORE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMDATA,"MMDATA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMDTANAG,"MMDTANAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMOREANAG,"MMOREANAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMTOTORE,"MMTOTORE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMTIPO,"MMTIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMFLTAG,"MMFLTAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMOPRX,"MMOPRX",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MANM_MEZZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MANM_MEZZ_IDX,2])
    i_lTable = "MANM_MEZZ"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MANM_MEZZ_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        GCSC_BSM(this,"M")
      endwith
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_MMDURORE N(5);
      ,t_MMTINT C(1);
      ,t_MMCODINT C(10);
      ,t_MMDESINT C(40);
      ,CPROWNUM N(10);
      ,t_CPROWNUM N(4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tmmgm_mgmbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMMDURORE_2_3.controlsource=this.cTrsName+'.t_MMDURORE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMMTINT_2_4.controlsource=this.cTrsName+'.t_MMTINT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODINT_2_5.controlsource=this.cTrsName+'.t_MMCODINT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMMDESINT_2_6.controlsource=this.cTrsName+'.t_MMDESINT'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(59)
    this.AddVLine(110)
    this.AddVLine(144)
    this.AddVLine(240)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MANM_MEZZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MANM_MEZZ_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"manmez","w_MMSERIALE")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MANM_MEZZ
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MANM_MEZZ')
        i_extval=cp_InsertValODBCExtFlds(this,'MANM_MEZZ')
        local i_cFld
        i_cFld=" "+;
                  "(MMSERIALE,MMCODICE,MMMOTORE,MMORE,MMDATA"+;
                  ",MMDTANAG,MMOREANAG,MMTOTORE,MMTIPO,UTCC"+;
                  ",UTCV,UTDC,UTDV,MMFLTAG,MMOPRX"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_MMSERIALE)+;
                    ","+cp_ToStrODBCNull(this.w_MMCODICE)+;
                    ","+cp_ToStrODBC(this.w_MMMOTORE)+;
                    ","+cp_ToStrODBC(this.w_MMORE)+;
                    ","+cp_ToStrODBC(this.w_MMDATA)+;
                    ","+cp_ToStrODBC(this.w_MMDTANAG)+;
                    ","+cp_ToStrODBC(this.w_MMOREANAG)+;
                    ","+cp_ToStrODBC(this.w_MMTOTORE)+;
                    ","+cp_ToStrODBC(this.w_MMTIPO)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_MMFLTAG)+;
                    ","+cp_ToStrODBC(this.w_MMOPRX)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MANM_MEZZ')
        i_extval=cp_InsertValVFPExtFlds(this,'MANM_MEZZ')
        cp_CheckDeletedKey(i_cTable,0,'MMSERIALE',this.w_MMSERIALE)
        INSERT INTO (i_cTable);
              (MMSERIALE,MMCODICE,MMMOTORE,MMORE,MMDATA,MMDTANAG,MMOREANAG,MMTOTORE,MMTIPO,UTCC,UTCV,UTDC,UTDV,MMFLTAG,MMOPRX &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_MMSERIALE;
                  ,this.w_MMCODICE;
                  ,this.w_MMMOTORE;
                  ,this.w_MMORE;
                  ,this.w_MMDATA;
                  ,this.w_MMDTANAG;
                  ,this.w_MMOREANAG;
                  ,this.w_MMTOTORE;
                  ,this.w_MMTIPO;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_MMFLTAG;
                  ,this.w_MMOPRX;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MAND_MEZZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAND_MEZZ_IDX,2])
      *
      * insert into MAND_MEZZ
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(MMSERIALE,CPROWORD,MMDURORE,MMTINT,MMCODINT"+;
                  ",MMDESINT,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MMSERIALE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_MMDURORE)+","+cp_ToStrODBCNull(this.w_MMTINT)+","+cp_ToStrODBCNull(this.w_MMCODINT)+;
             ","+cp_ToStrODBC(this.w_MMDESINT)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MMSERIALE',this.w_MMSERIALE,'CPROWNUM',this.w_CPROWNUM)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_MMSERIALE,this.w_CPROWORD,this.w_MMDURORE,this.w_MMTINT,this.w_MMCODINT"+;
                ",this.w_MMDESINT,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.MANM_MEZZ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MANM_MEZZ_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update MANM_MEZZ
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'MANM_MEZZ')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " MMCODICE="+cp_ToStrODBCNull(this.w_MMCODICE)+;
             ",MMMOTORE="+cp_ToStrODBC(this.w_MMMOTORE)+;
             ",MMORE="+cp_ToStrODBC(this.w_MMORE)+;
             ",MMDATA="+cp_ToStrODBC(this.w_MMDATA)+;
             ",MMDTANAG="+cp_ToStrODBC(this.w_MMDTANAG)+;
             ",MMOREANAG="+cp_ToStrODBC(this.w_MMOREANAG)+;
             ",MMTOTORE="+cp_ToStrODBC(this.w_MMTOTORE)+;
             ",MMTIPO="+cp_ToStrODBC(this.w_MMTIPO)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",MMFLTAG="+cp_ToStrODBC(this.w_MMFLTAG)+;
             ",MMOPRX="+cp_ToStrODBC(this.w_MMOPRX)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'MANM_MEZZ')
          i_cWhere = cp_PKFox(i_cTable  ,'MMSERIALE',this.w_MMSERIALE  )
          UPDATE (i_cTable) SET;
              MMCODICE=this.w_MMCODICE;
             ,MMMOTORE=this.w_MMMOTORE;
             ,MMORE=this.w_MMORE;
             ,MMDATA=this.w_MMDATA;
             ,MMDTANAG=this.w_MMDTANAG;
             ,MMOREANAG=this.w_MMOREANAG;
             ,MMTOTORE=this.w_MMTOTORE;
             ,MMTIPO=this.w_MMTIPO;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,MMFLTAG=this.w_MMFLTAG;
             ,MMOPRX=this.w_MMOPRX;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_MMDESINT))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.MAND_MEZZ_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.MAND_MEZZ_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from MAND_MEZZ
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MAND_MEZZ
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MMDURORE="+cp_ToStrODBC(this.w_MMDURORE)+;
                     ",MMTINT="+cp_ToStrODBCNull(this.w_MMTINT)+;
                     ",MMCODINT="+cp_ToStrODBCNull(this.w_MMCODINT)+;
                     ",MMDESINT="+cp_ToStrODBC(this.w_MMDESINT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,MMDURORE=this.w_MMDURORE;
                     ,MMTINT=this.w_MMTINT;
                     ,MMCODINT=this.w_MMCODINT;
                     ,MMDESINT=this.w_MMDESINT;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_MMDESINT))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.MAND_MEZZ_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MAND_MEZZ_IDX,2])
        *
        * delete MAND_MEZZ
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.MANM_MEZZ_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MANM_MEZZ_IDX,2])
        *
        * delete MANM_MEZZ
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_MMDESINT))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- mmgm_mgm
    this.NotifyEvent('Ripristino')
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MANM_MEZZ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MANM_MEZZ_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,15,.t.)
        if .o_DATAANAG<>.w_DATAANAG
          .w_MMDTANAG = iif(UPPER(this.cfunction)='LOAD',.w_DATAANAG,.w_MMDTANAG)
        endif
        if .o_OREANAG<>.w_OREANAG
          .w_MMOREANAG = iif(UPPER(this.cfunction)='LOAD',.w_OREANAG,.w_MMOREANAG)
        endif
          .w_MMTOTORE = .w_MMDURORE
        .DoRTCalc(19,19,.t.)
        if .o_OREANAG<>.w_OREANAG.or. .o_MMORE<>.w_MMORE
          .w_CHKERR = iif(.w_MMORE<>0,iif(.w_MMORE<.w_MMOREANAG,.T.,.F.),.F.)
        endif
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .DoRTCalc(21,27,.t.)
        if .o_MMORE<>.w_MMORE.or. .o_MMFLTAG<>.w_MMFLTAG.or. .o_MTAGORE<>.w_MTAGORE
          .w_MMOPRX = iif(.w_MMFLTAG='S',.w_MMORE+.w_MTAGORE,0)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(29,29,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CPROWNUM with this.w_CPROWNUM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mEnableControlsFixed()
    this.mHideControls()
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MMCODICE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
    i_lTable = "ANAGRAFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2], .t., this.ANAGRAFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('ANME_GEME',True,'ANAGRAFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MMCODICE)+"%");
                   +" and ANTIPO="+cp_ToStrODBC(this.w_MMTIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPO,ANCODICE,ANDESCRI,ANORE,ANULIN,ANALIM,ANOREMAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPO,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPO',this.w_MMTIPO;
                     ,'ANCODICE',trim(this.w_MMCODICE))
          select ANTIPO,ANCODICE,ANDESCRI,ANORE,ANULIN,ANALIM,ANOREMAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPO,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MMCODICE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MMCODICE) and !this.bDontReportError
            deferred_cp_zoom('ANAGRAFE','*','ANTIPO,ANCODICE',cp_AbsName(oSource.parent,'oMMCODICE_1_3'),i_cWhere,'ANME_GEME',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MMTIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPO,ANCODICE,ANDESCRI,ANORE,ANULIN,ANALIM,ANOREMAN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPO,ANCODICE,ANDESCRI,ANORE,ANULIN,ANALIM,ANOREMAN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPO,ANCODICE,ANDESCRI,ANORE,ANULIN,ANALIM,ANOREMAN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPO="+cp_ToStrODBC(this.w_MMTIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPO',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPO,ANCODICE,ANDESCRI,ANORE,ANULIN,ANALIM,ANOREMAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPO,ANCODICE,ANDESCRI,ANORE,ANULIN,ANALIM,ANOREMAN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MMCODICE);
                   +" and ANTIPO="+cp_ToStrODBC(this.w_MMTIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPO',this.w_MMTIPO;
                       ,'ANCODICE',this.w_MMCODICE)
            select ANTIPO,ANCODICE,ANDESCRI,ANORE,ANULIN,ANALIM,ANOREMAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMCODICE = NVL(_Link_.ANCODICE,space(10))
      this.w_Descri = NVL(_Link_.ANDESCRI,space(100))
      this.w_OREANAG = NVL(_Link_.ANORE,0)
      this.w_DATAANAG = NVL(cp_ToDate(_Link_.ANULIN),ctod("  /  /  "))
      this.w_MMMOTORE = NVL(_Link_.ANALIM,space(1))
      this.w_MTAGORE = NVL(_Link_.ANOREMAN,0)
    else
      if i_cCtrl<>'Load'
        this.w_MMCODICE = space(10)
      endif
      this.w_Descri = space(100)
      this.w_OREANAG = 0
      this.w_DATAANAG = ctod("  /  /  ")
      this.w_MMMOTORE = space(1)
      this.w_MTAGORE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPO,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.ANAGRAFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ANAGRAFE_IDX,3] and i_nFlds+6<200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.ANCODICE as ANCODICE103"+ ",link_1_3.ANDESCRI as ANDESCRI103"+ ",link_1_3.ANORE as ANORE103"+ ",link_1_3.ANULIN as ANULIN103"+ ",link_1_3.ANALIM as ANALIM103"+ ",link_1_3.ANOREMAN as ANOREMAN103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on MANM_MEZZ.MMCODICE=link_1_3.ANCODICE"+" and MANM_MEZZ.MMTIPO=link_1_3.ANTIPO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and MANM_MEZZ.MMCODICE=link_1_3.ANCODICE(+)"'+'+" and MANM_MEZZ.MMTIPO=link_1_3.ANTIPO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MMTINT
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPOINTERVENTO_IDX,3]
    i_lTable = "TIPOINTERVENTO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPOINTERVENTO_IDX,2], .t., this.TIPOINTERVENTO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPOINTERVENTO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMTINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('MICG_MMI',True,'TIPOINTERVENTO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MICODICE like "+cp_ToStrODBC(trim(this.w_MMTINT)+"%");

          i_ret=cp_SQL(i_nConn,"select MICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MICODICE',trim(this.w_MMTINT))
          select MICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MMTINT)==trim(_Link_.MICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MMTINT) and !this.bDontReportError
            deferred_cp_zoom('TIPOINTERVENTO','*','MICODICE',cp_AbsName(oSource.parent,'oMMTINT_2_4'),i_cWhere,'MICG_MMI',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where MICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MICODICE',oSource.xKey(1))
            select MICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMTINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where MICODICE="+cp_ToStrODBC(this.w_MMTINT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MICODICE',this.w_MMTINT)
            select MICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMTINT = NVL(_Link_.MICODICE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MMTINT = space(1)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPOINTERVENTO_IDX,2])+'\'+cp_ToStr(_Link_.MICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPOINTERVENTO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMTINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MMCODINT
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INTERVENTI_IDX,3]
    i_lTable = "INTERVENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2], .t., this.INTERVENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCODINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('TIGC_MTI',True,'INTERVENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_MMCODINT)+"%");
                   +" and TIMEZZO="+cp_ToStrODBC(this.w_MMTIPO);
                   +" and TITIPO="+cp_ToStrODBC(this.w_MMTINT);

          i_ret=cp_SQL(i_nConn,"select TIMEZZO,TITIPO,TICODICE,TIDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TIMEZZO,TITIPO,TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TIMEZZO',this.w_MMTIPO;
                     ,'TITIPO',this.w_MMTINT;
                     ,'TICODICE',trim(this.w_MMCODINT))
          select TIMEZZO,TITIPO,TICODICE,TIDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TIMEZZO,TITIPO,TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MMCODINT)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MMCODINT) and !this.bDontReportError
            deferred_cp_zoom('INTERVENTI','*','TIMEZZO,TITIPO,TICODICE',cp_AbsName(oSource.parent,'oMMCODINT_2_5'),i_cWhere,'TIGC_MTI',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MMTIPO<>oSource.xKey(1);
           .or. this.w_MMTINT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TIMEZZO,TITIPO,TICODICE,TIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TIMEZZO,TITIPO,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TIMEZZO,TITIPO,TICODICE,TIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(3));
                     +" and TIMEZZO="+cp_ToStrODBC(this.w_MMTIPO);
                     +" and TITIPO="+cp_ToStrODBC(this.w_MMTINT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TIMEZZO',oSource.xKey(1);
                       ,'TITIPO',oSource.xKey(2);
                       ,'TICODICE',oSource.xKey(3))
            select TIMEZZO,TITIPO,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCODINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TIMEZZO,TITIPO,TICODICE,TIDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_MMCODINT);
                   +" and TIMEZZO="+cp_ToStrODBC(this.w_MMTIPO);
                   +" and TITIPO="+cp_ToStrODBC(this.w_MMTINT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TIMEZZO',this.w_MMTIPO;
                       ,'TITIPO',this.w_MMTINT;
                       ,'TICODICE',this.w_MMCODINT)
            select TIMEZZO,TITIPO,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMCODINT = NVL(_Link_.TICODICE,space(10))
      this.w_MMDESINT = NVL(_Link_.TIDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MMCODINT = space(10)
      endif
      this.w_MMDESINT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2])+'\'+cp_ToStr(_Link_.TIMEZZO,1)+'\'+cp_ToStr(_Link_.TITIPO,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.INTERVENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCODINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMMSERIALE_1_1.value==this.w_MMSERIALE)
      this.oPgFrm.Page1.oPag.oMMSERIALE_1_1.value=this.w_MMSERIALE
    endif
    if not(this.oPgFrm.Page1.oPag.oMMCODICE_1_3.value==this.w_MMCODICE)
      this.oPgFrm.Page1.oPag.oMMCODICE_1_3.value=this.w_MMCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMMORE_1_6.value==this.w_MMORE)
      this.oPgFrm.Page1.oPag.oMMORE_1_6.value=this.w_MMORE
    endif
    if not(this.oPgFrm.Page1.oPag.oMMDATA_1_7.value==this.w_MMDATA)
      this.oPgFrm.Page1.oPag.oMMDATA_1_7.value=this.w_MMDATA
    endif
    if not(this.oPgFrm.Page1.oPag.oDescri_1_14.value==this.w_Descri)
      this.oPgFrm.Page1.oPag.oDescri_1_14.value=this.w_Descri
    endif
    if not(this.oPgFrm.Page1.oPag.oOREANAG_1_15.value==this.w_OREANAG)
      this.oPgFrm.Page1.oPag.oOREANAG_1_15.value=this.w_OREANAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAANAG_1_17.value==this.w_DATAANAG)
      this.oPgFrm.Page1.oPag.oDATAANAG_1_17.value=this.w_DATAANAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMMDTANAG_1_18.value==this.w_MMDTANAG)
      this.oPgFrm.Page1.oPag.oMMDTANAG_1_18.value=this.w_MMDTANAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMMOREANAG_1_19.value==this.w_MMOREANAG)
      this.oPgFrm.Page1.oPag.oMMOREANAG_1_19.value=this.w_MMOREANAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMMTOTORE_3_1.value==this.w_MMTOTORE)
      this.oPgFrm.Page1.oPag.oMMTOTORE_3_1.value=this.w_MMTOTORE
    endif
    if not(this.oPgFrm.Page1.oPag.oMMFLTAG_1_36.RadioValue()==this.w_MMFLTAG)
      this.oPgFrm.Page1.oPag.oMMFLTAG_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMMOPRX_1_38.value==this.w_MMOPRX)
      this.oPgFrm.Page1.oPag.oMMOPRX_1_38.value=this.w_MMOPRX
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMDURORE_2_3.value==this.w_MMDURORE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMDURORE_2_3.value=this.w_MMDURORE
      replace t_MMDURORE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMDURORE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMTINT_2_4.value==this.w_MMTINT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMTINT_2_4.value=this.w_MMTINT
      replace t_MMTINT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMTINT_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODINT_2_5.value==this.w_MMCODINT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODINT_2_5.value=this.w_MMCODINT
      replace t_MMCODINT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODINT_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMDESINT_2_6.value==this.w_MMDESINT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMDESINT_2_6.value=this.w_MMDESINT
      replace t_MMDESINT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMDESINT_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'MANM_MEZZ')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- mmgm_mgm
      i_bRes=.T.
      IF this.cFunction='Load'
          this.w_UTCC=i_codute
          this.w_UTDC=datetime()
      ENDIF
      IF this.cFunction='Edit'
          this.w_UTCV=i_codute
          this.w_UTDV=datetime()
      ENDIF
      this.NotifyEvent('Archivio')
      IF this.w_OK = .F.
          i_bRes=.F.
      ENDIF
      this.w_OK = .T.
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_MMCODINT) and (not(Empty(.w_MMDESINT)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODINT_2_5
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_MMDESINT))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MMORE = this.w_MMORE
    this.o_OREANAG = this.w_OREANAG
    this.o_DATAANAG = this.w_DATAANAG
    this.o_MMFLTAG = this.w_MMFLTAG
    this.o_MTAGORE = this.w_MTAGORE
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_MMDESINT)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_MMDURORE=0
      .w_MMTINT=space(1)
      .w_MMCODINT=space(10)
      .w_MMDESINT=space(40)
      .DoRTCalc(1,9,.f.)
        .w_MMTINT = 'S'
      .DoRTCalc(10,10,.f.)
      if not(empty(.w_MMTINT))
        .link_2_4('Full')
      endif
      .DoRTCalc(11,11,.f.)
      if not(empty(.w_MMCODINT))
        .link_2_5('Full')
      endif
    endwith
    this.DoRTCalc(12,29,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWNUM = t_CPROWNUM
    this.w_CPROWORD = t_CPROWORD
    this.w_MMDURORE = t_MMDURORE
    this.w_MMTINT = t_MMTINT
    this.w_MMCODINT = t_MMCODINT
    this.w_MMDESINT = t_MMDESINT
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWNUM with this.w_CPROWNUM
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MMDURORE with this.w_MMDURORE
    replace t_MMTINT with this.w_MMTINT
    replace t_MMCODINT with this.w_MMCODINT
    replace t_MMDESINT with this.w_MMDESINT
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tmmgm_mgmPag1 as StdContainer
  Width  = 589
  height = 424
  stdWidth  = 589
  stdheight = 424
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMMSERIALE_1_1 as StdField with uid="LPJTFFTYUP",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MMSERIALE", cQueryName = "MMSERIALE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 56572664,;
   bGlobalFont=.t.,;
    Height=21, Width=94, Left=49, Top=6, InputMask=replicate('X',10)

  add object oMMCODICE_1_3 as StdField with uid="UYOYRXKENM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MMCODICE", cQueryName = "MMCODICE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 70664015,;
   bGlobalFont=.t.,;
    Height=21, Width=94, Left=49, Top=30, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="ANAGRAFE", cZoomOnZoom="ANME_GEME", oKey_1_1="ANTIPO", oKey_1_2="this.w_MMTIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_MMCODICE"

  func oMMCODICE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMMCODICE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMMCODICE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ANAGRAFE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPO="+cp_ToStrODBC(this.Parent.oContained.w_MMTIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPO="+cp_ToStr(this.Parent.oContained.w_MMTIPO)
    endif
    do cp_zoom with 'ANAGRAFE','*','ANTIPO,ANCODICE',cp_AbsName(this.parent,'oMMCODICE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'ANME_GEME',"",'',this.parent.oContained
  endproc
  proc oMMCODICE_1_3.mZoomOnZoom
    local i_obj
    i_obj=ANME_GEME()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPO=w_MMTIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_MMCODICE
    i_obj.ecpSave()
  endproc

  add object oMMORE_1_6 as StdField with uid="FIIEFFWDSL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MMORE", cQueryName = "MMORE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 48070764,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=39, Top=132, cSayPict='"999999999"', cGetPict='"999999999"'

  add object oMMDATA_1_7 as StdField with uid="NVEPHXDQFO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MMDATA", cQueryName = "MMDATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 189018004,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=244, Top=134

  add object oDescri_1_14 as StdField with uid="AGVMDRBURB",rtseq=13,rtrep=.f.,;
    cFormVar = "w_Descri", cQueryName = "Descri",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 20916260,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=144, Top=30, InputMask=replicate('X',100)

  add object oOREANAG_1_15 as StdField with uid="GLUQLCHCVG",rtseq=14,rtrep=.f.,;
    cFormVar = "w_OREANAG", cQueryName = "OREANAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 195304052,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=285, Top=56

  add object oDATAANAG_1_17 as StdField with uid="NQLXCFFQIB",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DATAANAG", cQueryName = "DATAANAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 259210205,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=363, Top=56

  add object oMMDTANAG_1_18 as StdField with uid="YKACDGSUVC",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MMDTANAG", cQueryName = "MMDTANAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 258027341,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=363, Top=84

  add object oMMOREANAG_1_19 as StdField with uid="XFFCXKMSWP",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MMOREANAG", cQueryName = "MMOREANAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 64849181,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=285, Top=84


  add object oObj_1_25 as cp_runprogram with uid="TXZIDGFNAS",left=15, top=447, width=50,height=50,;
    caption='Object',;
    prg="MCGM_BGM('C')",;
    cEvent = "Archivio",;
    nPag=1;
    , HelpContextID = 120437364;
  , bGlobalFont=.t.



  add object oObj_1_26 as cp_runprogram with uid="SCZZXXOYCH",left=86, top=447, width=50,height=50,;
    caption='Object',;
    prg="MCGM_BGM('F')",;
    cEvent = "Ripristino",;
    nPag=1;
    , HelpContextID = 120437364;
  , bGlobalFont=.t.



  add object oBtn_1_32 as StdButton with uid="RIGYLUATQM",left=448, top=389, width=116,height=25,;
    caption="Salva / Stampa", nPag=1;
    , ToolTipText = "Cliccare per Salvare e Stampare";
    , HelpContextID = 46731748;
  , bGlobalFont=.t.

    proc oBtn_1_32.Click()
      with this.Parent.oContained
        GCSC_BSM(this.Parent.oContained,"M")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    with this.Parent.oContained
      return (.not.empty(.w_MMCODINT) or .w_SAVE=.T.)
    endwith
  endfunc


  add object oObj_1_35 as cp_runprogram with uid="HXQBIZKGCM",left=146, top=448, width=50,height=50,;
    caption='Object',;
    prg="MCGM_BGM('G')",;
    cEvent = "Insert row end,Init Row",;
    nPag=1;
    , HelpContextID = 120437364;
  , bGlobalFont=.t.


  add object oMMFLTAG_1_36 as StdCheck with uid="MOKJIZIYFR",rtseq=27,rtrep=.f.,left=449, top=132, caption="",;
    HelpContextID = 188288916,;
    cFormVar="w_MMFLTAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMMFLTAG_1_36.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MMFLTAG,&i_cF..t_MMFLTAG),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oMMFLTAG_1_36.GetRadio()
    this.Parent.oContained.w_MMFLTAG = this.RadioValue()
    return .t.
  endfunc

  func oMMFLTAG_1_36.ToRadio()
    this.Parent.oContained.w_MMFLTAG=trim(this.Parent.oContained.w_MMFLTAG)
    return(;
      iif(this.Parent.oContained.w_MMFLTAG=='S',1,;
      0))
  endfunc

  func oMMFLTAG_1_36.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMMOPRX_1_38 as StdField with uid="JCGNNGXKBS",rtseq=28,rtrep=.f.,;
    cFormVar = "w_MMOPRX", cQueryName = "MMOPRX",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 72646548,;
   bGlobalFont=.t.,;
    Height=21, Width=89, Left=469, Top=133

  add object oStr_1_4 as StdString with uid="AICLGWZPSX",Visible=.t., Left=8, Top=34,;
    Alignment=1, Width=40, Height=18,;
    Caption="Mezzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="WGNWDXYAYT",Visible=.t., Left=15, Top=136,;
    Alignment=1, Width=23, Height=18,;
    Caption="Ore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="LCXGBKCNQN",Visible=.t., Left=10, Top=164,;
    Alignment=0, Width=53, Height=18,;
    Caption="Intervento"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="QXSTVIWRED",Visible=.t., Left=65, Top=164,;
    Alignment=0, Width=38, Height=18,;
    Caption="Durata"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="CPISSQWNSM",Visible=.t., Left=145, Top=164,;
    Alignment=0, Width=44, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="NBBTLLDNQG",Visible=.t., Left=242, Top=164,;
    Alignment=0, Width=121, Height=18,;
    Caption="Descrizione"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="RJRRYRXHLP",Visible=.t., Left=111, Top=164,;
    Alignment=0, Width=30, Height=18,;
    Caption="Tipo"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="PPYSADOYSW",Visible=.t., Left=29, Top=60,;
    Alignment=1, Width=254, Height=18,;
    Caption="Ore/Data Carrello Ultimo Intervento Anagrafica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="UISIECUOFL",Visible=.t., Left=15, Top=88,;
    Alignment=1, Width=268, Height=18,;
    Caption="Ore/Data Carrello Ultimo Intervento Caricamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="ECTMFYJGWE",Visible=.t., Left=136, Top=136,;
    Alignment=1, Width=108, Height=18,;
    Caption="Data Manutenzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="VJSUTITKOM",Visible=.t., Left=35, Top=110,;
    Alignment=0, Width=384, Height=19,;
    Caption="ATTENZIONE VERIFICARE LE ORE INSERITE"  ;
    , FontName = "Times New Roman", FontSize = 10, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_CHKERR=.F.)
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="CUXCHYUZEH",Visible=.t., Left=6, Top=10,;
    Alignment=1, Width=42, Height=18,;
    Caption="Seriale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="ADZGMIRZOT",Visible=.t., Left=382, Top=137,;
    Alignment=1, Width=63, Height=18,;
    Caption="Tagliando:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=186,;
    width=567+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=187,width=566+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TIPOINTERVENTO|INTERVENTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TIPOINTERVENTO'
        oDropInto=this.oBodyCol.oRow.oMMTINT_2_4
      case cFile='INTERVENTI'
        oDropInto=this.oBodyCol.oRow.oMMCODINT_2_5
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oMMTOTORE_3_1 as StdField with uid="HJLVKIFIVJ",rtseq=18,rtrep=.f.,;
    cFormVar="w_MMTOTORE",value=0,enabled=.f.,;
    HelpContextID = 221589327,;
    cQueryName = "MMTOTORE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=85, Top=384

  add object oStr_3_2 as StdString with uid="AVADCJUWGC",Visible=.t., Left=9, Top=388,;
    Alignment=1, Width=76, Height=18,;
    Caption="Durata Totale:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tmmgm_mgmBodyRow as container
  Width=557
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_2 as StdTrsField with uid="JREFERHYAY",rtseq=8,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 218451184,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oMMDURORE_2_3 as StdTrsField with uid="AHQIGHAABF",rtseq=9,rtrep=.t.,;
    cFormVar="w_MMDURORE",value=0,;
    HelpContextID = 223358799,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=49, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oMMTINT_2_4 as StdTrsField with uid="ICOGYNXRDE",rtseq=10,rtrep=.t.,;
    cFormVar="w_MMTINT",value=space(1),;
    HelpContextID = 144387988,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=33, Left=99, Top=0, InputMask=replicate('X',1), bHasZoom = .t. , cLinkFile="TIPOINTERVENTO", cZoomOnZoom="MICG_MMI", oKey_1_1="MICODICE", oKey_1_2="this.w_MMTINT"

  func oMMTINT_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
      if .not. empty(.w_MMCODINT)
        bRes2=.link_2_5('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMMTINT_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMMTINT_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPOINTERVENTO','*','MICODICE',cp_AbsName(this.parent,'oMMTINT_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'MICG_MMI',"",'',this.parent.oContained
  endproc
  proc oMMTINT_2_4.mZoomOnZoom
    local i_obj
    i_obj=MICG_MMI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MICODICE=this.parent.oContained.w_MMTINT
    i_obj.ecpSave()
  endproc

  add object oMMCODINT_2_5 as StdTrsField with uid="YUBWWEEJAE",rtseq=11,rtrep=.t.,;
    cFormVar="w_MMCODINT",value=space(10),;
    HelpContextID = 197771456,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=94, Left=133, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="INTERVENTI", cZoomOnZoom="TIGC_MTI", oKey_1_1="TIMEZZO", oKey_1_2="this.w_MMTIPO", oKey_2_1="TITIPO", oKey_2_2="this.w_MMTINT", oKey_3_1="TICODICE", oKey_3_2="this.w_MMCODINT"

  func oMMCODINT_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oMMCODINT_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMMCODINT_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INTERVENTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TIMEZZO="+cp_ToStrODBC(this.Parent.oContained.w_MMTIPO)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TITIPO="+cp_ToStrODBC(this.Parent.oContained.w_MMTINT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TIMEZZO="+cp_ToStr(this.Parent.oContained.w_MMTIPO)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TITIPO="+cp_ToStr(this.Parent.oContained.w_MMTINT)
    endif
    do cp_zoom with 'INTERVENTI','*','TIMEZZO,TITIPO,TICODICE',cp_AbsName(this.parent,'oMMCODINT_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'TIGC_MTI',"",'',this.parent.oContained
  endproc
  proc oMMCODINT_2_5.mZoomOnZoom
    local i_obj
    i_obj=TIGC_MTI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TIMEZZO=w_MMTIPO
    i_obj.TITIPO=w_MMTINT
     i_obj.w_TICODICE=this.parent.oContained.w_MMCODINT
    i_obj.ecpSave()
  endproc

  add object oMMDESINT_2_6 as StdTrsField with uid="YHXNBBPCNG",rtseq=12,rtrep=.t.,;
    cFormVar="w_MMDESINT",value=space(40),;
    HelpContextID = 212848832,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=322, Left=230, Top=0, cSayPict=["!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"], cGetPict=["!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"], InputMask=replicate('X',40)
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_2.When()
    return(.t.)
  proc oCPROWORD_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('mmgm_mgm','MANM_MEZZ','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MMSERIALE=MANM_MEZZ.MMSERIALE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
