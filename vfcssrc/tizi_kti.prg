* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: tizi_kti                                                        *
*              Zoom Tipo Intervento                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-07-31                                                      *
* Last revis.: 2016-08-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("ttizi_kti",oParentObject))

* --- Class definition
define class ttizi_kti as StdForm
  Top    = 27
  Left   = 57

  * --- Standard Properties
  Width  = 436
  Height = 465
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-08-04"
  HelpContextID=185995524
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  cPrg = "tizi_kti"
  cComment = "Zoom Tipo Intervento"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_MCTINT = space(1)
  w_CHKM = space(1)
  w_MCTIPO = space(1)
  w_SELEZ = 0
  w_intzoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","ttizi_ktiPag1","tizi_kti",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_intzoom = this.oPgFrm.Pages(1).oPag.intzoom
    DoDefault()
    proc Destroy()
      this.w_intzoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MCTINT=space(1)
      .w_CHKM=space(1)
      .w_MCTIPO=space(1)
      .w_SELEZ=0
      .w_MCTINT=oParentObject.w_MCTINT
      .w_CHKM=oParentObject.w_CHKM
      .w_MCTIPO=oParentObject.w_MCTIPO
      .oPgFrm.Page1.oPag.intzoom.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
    this.DoRTCalc(1,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MCTINT=.w_MCTINT
      .oParentObject.w_CHKM=.w_CHKM
      .oParentObject.w_MCTIPO=.w_MCTIPO
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.intzoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.intzoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.intzoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_3.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMCTINT_1_2.value==this.w_MCTINT)
      this.oPgFrm.Page1.oPag.oMCTINT_1_2.value=this.w_MCTINT
    endif
    if not(this.oPgFrm.Page1.oPag.oCHKM_1_4.value==this.w_CHKM)
      this.oPgFrm.Page1.oPag.oCHKM_1_4.value=this.w_CHKM
    endif
    if not(this.oPgFrm.Page1.oPag.oMCTIPO_1_5.value==this.w_MCTIPO)
      this.oPgFrm.Page1.oPag.oMCTIPO_1_5.value=this.w_MCTIPO
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZ_1_9.value==this.w_SELEZ)
      this.oPgFrm.Page1.oPag.oSELEZ_1_9.value=this.w_SELEZ
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class ttizi_ktiPag1 as StdContainer
  Width  = 681
  height = 465
  stdWidth  = 681
  stdheight = 465
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object intzoom as cp_szoombox with uid="CLBYLLIINQ",left=10, top=6, width=410,height=404,;
    caption='Object',;
    bNoZoomGridShape=.f.,cZoomOnZoom="",cMenuFile="",cTable="INTERVENTI",cZoomFile="TIZI_ZTI",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.t.,bQueryOnLoad=.t.,bReadOnly=.f.,;
    nPag=1;
    , HelpContextID = 172877741;
  , bGlobalFont=.t.


  add object oMCTINT_1_2 as StdField with uid="NNOZTVBOYF",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MCTINT", cQueryName = "MCTINT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 71604531,;
   bGlobalFont=.t.,;
    Height=21, Width=67, Left=543, Top=47, InputMask=replicate('X',1)


  add object oObj_1_3 as cp_runprogram with uid="DGYFRMUVYA",left=12, top=496, width=50,height=50,;
    caption='Object',;
    prg="MCGM_BGM('B')",;
    cEvent = "w_intzoom selected",;
    nPag=1;
    , HelpContextID = 172877741;
  , bGlobalFont=.t.


  add object oCHKM_1_4 as StdField with uid="OZAOADKTLM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CHKM", cQueryName = "CHKM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 191368595,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=543, Top=28, InputMask=replicate('X',1)

  add object oMCTIPO_1_5 as StdField with uid="HJLSTBSKJF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MCTIPO", cQueryName = "MCTIPO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 258251059,;
   bGlobalFont=.t.,;
    Height=21, Width=136, Left=545, Top=6, InputMask=replicate('X',1)


  add object oBtn_1_6 as StdButton with uid="MYGFRXPCOX",left=329, top=429, width=87,height=25,;
    caption="Conferma", nPag=1;
    , HelpContextID = 64185588;
  , bGlobalFont=.t.

    proc oBtn_1_6.Click()
      with this.Parent.oContained
        MCGM_BGM(this.Parent.oContained,"B")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_7 as cp_runprogram with uid="IZTWGSKMCQ",left=108, top=500, width=272,height=29,;
    caption='MCGM_BGM',;
    prg="MCGM_BGM('+')",;
    cEvent = "w_intzoom row checked",;
    nPag=1;
    , HelpContextID = 212350592;
  , bGlobalFont=.t.



  add object oObj_1_8 as cp_runprogram with uid="OCIVZSUOKE",left=105, top=536, width=272,height=29,;
    caption='MCGM_BGM',;
    prg="MCGM_BGM('+')",;
    cEvent = "w_intzoom row unchecked",;
    nPag=1;
    , HelpContextID = 212350592;
  , bGlobalFont=.t.


  add object oSELEZ_1_9 as StdField with uid="HSGURLHTJX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SELEZ", cQueryName = "SELEZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 16784275,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=120, Top=429

  add object oStr_1_10 as StdString with uid="JYNHZRDEQI",Visible=.t., Left=15, Top=433,;
    Alignment=1, Width=103, Height=18,;
    Caption="Righe Selezionate:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('tizi_kti','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
