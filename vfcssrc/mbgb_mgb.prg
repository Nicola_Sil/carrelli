* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: mbgb_mgb                                                        *
*              Manutenzione Batterie                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-07-30                                                      *
* Last revis.: 2016-03-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tmbgb_mgb"))

* --- Class definition
define class tmbgb_mgb as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 593
  Height = 421+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-03-31"
  HelpContextID=30044787
  max_rt_seq=20

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  MANM_BATT_IDX = 0
  MAND_BATT_IDX = 0
  ANAGRAFE_IDX = 0
  INTERVENTI_IDX = 0
  TIPOINTERVENTO_IDX = 0
  cFile = "MANM_BATT"
  cFileDetail = "MAND_BATT"
  cKeySelect = "MBSERIALE"
  cKeyWhere  = "MBSERIALE=this.w_MBSERIALE"
  cKeyDetail  = "MBSERIALE=this.w_MBSERIALE and CPROWNUM=this.w_CPROWNUM"
  cKeyWhereODBC = '"MBSERIALE="+cp_ToStrODBC(this.w_MBSERIALE)';

  cKeyDetailWhereODBC = '"MBSERIALE="+cp_ToStrODBC(this.w_MBSERIALE)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"MAND_BATT.MBSERIALE="+cp_ToStrODBC(this.w_MBSERIALE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'MAND_BATT.CPROWORD '
  cPrg = "mbgb_mgb"
  cComment = "Manutenzione Batterie"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MBSERIALE = space(10)
  w_OBSOTEST = ctod('  /  /  ')
  w_MBCODICE = space(10)
  w_CPROWORD = 0
  w_MBDURORE = 0
  w_MBTINT = space(1)
  w_MBCODINT = space(10)
  w_MBDESINT = space(40)
  w_CPROWNUM = 0
  w_Descri = space(100)
  w_MBDTANAG = ctod('  /  /  ')
  w_MBDATA = ctod('  /  /  ')
  w_MBTOTORE = 0
  w_MBTIPO = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_SAVE = .F.
  w_OK = .F.

  * --- Autonumbered Variables
  op_MBSERIALE = this.W_MBSERIALE
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MANM_BATT','mbgb_mgb')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tmbgb_mgbPag1","mbgb_mgb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Manutenzione")
      .Pages(1).HelpContextID = 77715002
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMBSERIALE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='ANAGRAFE'
    this.cWorkTables[2]='INTERVENTI'
    this.cWorkTables[3]='TIPOINTERVENTO'
    this.cWorkTables[4]='MANM_BATT'
    this.cWorkTables[5]='MAND_BATT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MANM_BATT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MANM_BATT_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_MBSERIALE = NVL(MBSERIALE,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_3_joined
    link_1_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from MANM_BATT where MBSERIALE=KeySet.MBSERIALE
    *
    i_nConn = i_TableProp[this.MANM_BATT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MANM_BATT_IDX,2],this.bLoadRecFilter,this.MANM_BATT_IDX,"mbgb_mgb")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MANM_BATT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MANM_BATT.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"MAND_BATT.","MANM_BATT.")
      i_cTable = i_cTable+' MANM_BATT '
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MBSERIALE',this.w_MBSERIALE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_OBSOTEST = i_datsys
        .w_Descri = space(100)
        .w_SAVE = .F.
        .w_OK = .T.
        .w_MBSERIALE = NVL(MBSERIALE,space(10))
        .op_MBSERIALE = .w_MBSERIALE
        .w_MBCODICE = NVL(MBCODICE,space(10))
          if link_1_3_joined
            this.w_MBCODICE = NVL(ANCODICE103,NVL(this.w_MBCODICE,space(10)))
            this.w_Descri = NVL(ANDESCRI103,space(100))
            this.w_MBDTANAG = NVL(cp_ToDate(ANULIN103),ctod("  /  /  "))
          else
          .link_1_3('Load')
          endif
        .w_MBDTANAG = NVL(cp_ToDate(MBDTANAG),ctod("  /  /  "))
        .w_MBDATA = NVL(cp_ToDate(MBDATA),ctod("  /  /  "))
        .w_MBTOTORE = NVL(MBTOTORE,0)
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate()
        .w_MBTIPO = NVL(MBTIPO,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        cp_LoadRecExtFlds(this,'MANM_BATT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from MAND_BATT where MBSERIALE=KeySet.MBSERIALE
      *                            and CPROWNUM=KeySet.CPROWNUM
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.MAND_BATT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAND_BATT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('MAND_BATT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "MAND_BATT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" MAND_BATT"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'MBSERIALE',this.w_MBSERIALE  )
        select * from (i_cTable) MAND_BATT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_MBTOTORE = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MBDURORE = NVL(MBDURORE,0)
          .w_MBTINT = NVL(MBTINT,space(1))
          * evitabile
          *.link_2_3('Load')
          .w_MBCODINT = NVL(MBCODINT,space(10))
          * evitabile
          *.link_2_4('Load')
          .w_MBDESINT = NVL(MBDESINT,space(40))
          .w_CPROWNUM = NVL(CPROWNUM,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_MBTOTORE = .w_MBTOTORE+.w_MBDURORE
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_21.enabled = .oPgFrm.Page1.oPag.oBtn_1_21.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_MBSERIALE=space(10)
      .w_OBSOTEST=ctod("  /  /  ")
      .w_MBCODICE=space(10)
      .w_CPROWORD=10
      .w_MBDURORE=0
      .w_MBTINT=space(1)
      .w_MBCODINT=space(10)
      .w_MBDESINT=space(40)
      .w_CPROWNUM=0
      .w_Descri=space(100)
      .w_MBDTANAG=ctod("  /  /  ")
      .w_MBDATA=ctod("  /  /  ")
      .w_MBTOTORE=0
      .w_MBTIPO=space(1)
      .w_UTCC=0
      .w_UTCV=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_SAVE=.f.
      .w_OK=.f.
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_OBSOTEST = i_datsys
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_MBCODICE))
         .link_1_3('Full')
        endif
        .DoRTCalc(4,6,.f.)
        if not(empty(.w_MBTINT))
         .link_2_3('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_MBCODINT))
         .link_2_4('Full')
        endif
        .DoRTCalc(8,11,.f.)
        .w_MBDATA = i_datsys
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate()
        .DoRTCalc(13,13,.f.)
        .w_MBTIPO = 'B'
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .DoRTCalc(15,18,.f.)
        .w_SAVE = .F.
        .w_OK = .T.
      endif
    endwith
    cp_BlankRecExtFlds(this,'MANM_BATT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMBSERIALE_1_1.enabled = !i_bVal
      .Page1.oPag.oMBCODICE_1_3.enabled = i_bVal
      .Page1.oPag.oMBDATA_1_13.enabled = i_bVal
      .Page1.oPag.oMBTOTORE_3_1.enabled = i_bVal
      .Page1.oPag.oBtn_1_21.enabled = .Page1.oPag.oBtn_1_21.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'MANM_BATT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MANM_BATT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MANM_BATT_IDX,2])
    cp_AskTableProg(this,i_nConn,"manbatt","w_MBSERIALE")
    with this
      .op_MBSERIALE = .w_MBSERIALE
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MANM_BATT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MBSERIALE,"MBSERIALE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MBCODICE,"MBCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MBDTANAG,"MBDTANAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MBDATA,"MBDATA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MBTOTORE,"MBTOTORE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MBTIPO,"MBTIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MANM_BATT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MANM_BATT_IDX,2])
    i_lTable = "MANM_BATT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MANM_BATT_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        GCSC_BSM(this,"B")
      endwith
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_MBDURORE N(5);
      ,t_MBTINT C(1);
      ,t_MBCODINT C(10);
      ,t_MBDESINT C(40);
      ,CPROWNUM N(10);
      ,t_CPROWNUM N(4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tmbgb_mgbbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMBDURORE_2_2.controlsource=this.cTrsName+'.t_MBDURORE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMBTINT_2_3.controlsource=this.cTrsName+'.t_MBTINT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMBCODINT_2_4.controlsource=this.cTrsName+'.t_MBCODINT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMBDESINT_2_5.controlsource=this.cTrsName+'.t_MBDESINT'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(62)
    this.AddVLine(113)
    this.AddVLine(144)
    this.AddVLine(238)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MANM_BATT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MANM_BATT_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"manbatt","w_MBSERIALE")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MANM_BATT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MANM_BATT')
        i_extval=cp_InsertValODBCExtFlds(this,'MANM_BATT')
        local i_cFld
        i_cFld=" "+;
                  "(MBSERIALE,MBCODICE,MBDTANAG,MBDATA,MBTOTORE"+;
                  ",MBTIPO,UTCC,UTCV,UTDC,UTDV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_MBSERIALE)+;
                    ","+cp_ToStrODBCNull(this.w_MBCODICE)+;
                    ","+cp_ToStrODBC(this.w_MBDTANAG)+;
                    ","+cp_ToStrODBC(this.w_MBDATA)+;
                    ","+cp_ToStrODBC(this.w_MBTOTORE)+;
                    ","+cp_ToStrODBC(this.w_MBTIPO)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MANM_BATT')
        i_extval=cp_InsertValVFPExtFlds(this,'MANM_BATT')
        cp_CheckDeletedKey(i_cTable,0,'MBSERIALE',this.w_MBSERIALE)
        INSERT INTO (i_cTable);
              (MBSERIALE,MBCODICE,MBDTANAG,MBDATA,MBTOTORE,MBTIPO,UTCC,UTCV,UTDC,UTDV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_MBSERIALE;
                  ,this.w_MBCODICE;
                  ,this.w_MBDTANAG;
                  ,this.w_MBDATA;
                  ,this.w_MBTOTORE;
                  ,this.w_MBTIPO;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MAND_BATT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MAND_BATT_IDX,2])
      *
      * insert into MAND_BATT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(MBSERIALE,CPROWORD,MBDURORE,MBTINT,MBCODINT"+;
                  ",MBDESINT,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MBSERIALE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_MBDURORE)+","+cp_ToStrODBCNull(this.w_MBTINT)+","+cp_ToStrODBCNull(this.w_MBCODINT)+;
             ","+cp_ToStrODBC(this.w_MBDESINT)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MBSERIALE',this.w_MBSERIALE,'CPROWNUM',this.w_CPROWNUM)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_MBSERIALE,this.w_CPROWORD,this.w_MBDURORE,this.w_MBTINT,this.w_MBCODINT"+;
                ",this.w_MBDESINT,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.MANM_BATT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MANM_BATT_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update MANM_BATT
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'MANM_BATT')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " MBCODICE="+cp_ToStrODBCNull(this.w_MBCODICE)+;
             ",MBDTANAG="+cp_ToStrODBC(this.w_MBDTANAG)+;
             ",MBDATA="+cp_ToStrODBC(this.w_MBDATA)+;
             ",MBTOTORE="+cp_ToStrODBC(this.w_MBTOTORE)+;
             ",MBTIPO="+cp_ToStrODBC(this.w_MBTIPO)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'MANM_BATT')
          i_cWhere = cp_PKFox(i_cTable  ,'MBSERIALE',this.w_MBSERIALE  )
          UPDATE (i_cTable) SET;
              MBCODICE=this.w_MBCODICE;
             ,MBDTANAG=this.w_MBDTANAG;
             ,MBDATA=this.w_MBDATA;
             ,MBTOTORE=this.w_MBTOTORE;
             ,MBTIPO=this.w_MBTIPO;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_MBDESINT))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.MAND_BATT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.MAND_BATT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from MAND_BATT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MAND_BATT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MBDURORE="+cp_ToStrODBC(this.w_MBDURORE)+;
                     ",MBTINT="+cp_ToStrODBCNull(this.w_MBTINT)+;
                     ",MBCODINT="+cp_ToStrODBCNull(this.w_MBCODINT)+;
                     ",MBDESINT="+cp_ToStrODBC(this.w_MBDESINT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,MBDURORE=this.w_MBDURORE;
                     ,MBTINT=this.w_MBTINT;
                     ,MBCODINT=this.w_MBCODINT;
                     ,MBDESINT=this.w_MBDESINT;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_MBDESINT))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.MAND_BATT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MAND_BATT_IDX,2])
        *
        * delete MAND_BATT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.MANM_BATT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MANM_BATT_IDX,2])
        *
        * delete MANM_BATT
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_MBDESINT))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MANM_BATT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MANM_BATT_IDX,2])
    if i_bUpd
      this.bHeaderUpdated=.t. && Totalized fields
      with this
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CPROWNUM with this.w_CPROWNUM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_3_3.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mEnableControlsFixed()
    this.mHideControls()
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_3_3.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MBCODICE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
    i_lTable = "ANAGRAFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2], .t., this.ANAGRAFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MBCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('ANME_GEME',True,'ANAGRAFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MBCODICE)+"%");
                   +" and ANTIPO="+cp_ToStrODBC(this.w_MBTIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPO,ANCODICE,ANDESCRI,ANULIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPO,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPO',this.w_MBTIPO;
                     ,'ANCODICE',trim(this.w_MBCODICE))
          select ANTIPO,ANCODICE,ANDESCRI,ANULIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPO,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MBCODICE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MBCODICE) and !this.bDontReportError
            deferred_cp_zoom('ANAGRAFE','*','ANTIPO,ANCODICE',cp_AbsName(oSource.parent,'oMBCODICE_1_3'),i_cWhere,'ANME_GEME',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MBTIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPO,ANCODICE,ANDESCRI,ANULIN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPO,ANCODICE,ANDESCRI,ANULIN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPO,ANCODICE,ANDESCRI,ANULIN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPO="+cp_ToStrODBC(this.w_MBTIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPO',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPO,ANCODICE,ANDESCRI,ANULIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MBCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPO,ANCODICE,ANDESCRI,ANULIN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MBCODICE);
                   +" and ANTIPO="+cp_ToStrODBC(this.w_MBTIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPO',this.w_MBTIPO;
                       ,'ANCODICE',this.w_MBCODICE)
            select ANTIPO,ANCODICE,ANDESCRI,ANULIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MBCODICE = NVL(_Link_.ANCODICE,space(10))
      this.w_Descri = NVL(_Link_.ANDESCRI,space(100))
      this.w_MBDTANAG = NVL(cp_ToDate(_Link_.ANULIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MBCODICE = space(10)
      endif
      this.w_Descri = space(100)
      this.w_MBDTANAG = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPO,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.ANAGRAFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MBCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ANAGRAFE_IDX,3] and i_nFlds+3<200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.ANCODICE as ANCODICE103"+ ",link_1_3.ANDESCRI as ANDESCRI103"+ ",link_1_3.ANULIN as ANULIN103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on MANM_BATT.MBCODICE=link_1_3.ANCODICE"+" and MANM_BATT.MBTIPO=link_1_3.ANTIPO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and MANM_BATT.MBCODICE=link_1_3.ANCODICE(+)"'+'+" and MANM_BATT.MBTIPO=link_1_3.ANTIPO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MBTINT
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPOINTERVENTO_IDX,3]
    i_lTable = "TIPOINTERVENTO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPOINTERVENTO_IDX,2], .t., this.TIPOINTERVENTO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPOINTERVENTO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MBTINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('MICG_MMI',True,'TIPOINTERVENTO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MICODICE like "+cp_ToStrODBC(trim(this.w_MBTINT)+"%");

          i_ret=cp_SQL(i_nConn,"select MICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MICODICE',trim(this.w_MBTINT))
          select MICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MBTINT)==trim(_Link_.MICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MBTINT) and !this.bDontReportError
            deferred_cp_zoom('TIPOINTERVENTO','*','MICODICE',cp_AbsName(oSource.parent,'oMBTINT_2_3'),i_cWhere,'MICG_MMI',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where MICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MICODICE',oSource.xKey(1))
            select MICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MBTINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where MICODICE="+cp_ToStrODBC(this.w_MBTINT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MICODICE',this.w_MBTINT)
            select MICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MBTINT = NVL(_Link_.MICODICE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MBTINT = space(1)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPOINTERVENTO_IDX,2])+'\'+cp_ToStr(_Link_.MICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPOINTERVENTO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MBTINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MBCODINT
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INTERVENTI_IDX,3]
    i_lTable = "INTERVENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2], .t., this.INTERVENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MBCODINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('TIGC_MTI',True,'INTERVENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_MBCODINT)+"%");
                   +" and TIMEZZO="+cp_ToStrODBC(this.w_MBTIPO);
                   +" and TITIPO="+cp_ToStrODBC(this.w_MBTINT);

          i_ret=cp_SQL(i_nConn,"select TIMEZZO,TITIPO,TICODICE,TIDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TIMEZZO,TITIPO,TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TIMEZZO',this.w_MBTIPO;
                     ,'TITIPO',this.w_MBTINT;
                     ,'TICODICE',trim(this.w_MBCODINT))
          select TIMEZZO,TITIPO,TICODICE,TIDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TIMEZZO,TITIPO,TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MBCODINT)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MBCODINT) and !this.bDontReportError
            deferred_cp_zoom('INTERVENTI','*','TIMEZZO,TITIPO,TICODICE',cp_AbsName(oSource.parent,'oMBCODINT_2_4'),i_cWhere,'TIGC_MTI',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MBTIPO<>oSource.xKey(1);
           .or. this.w_MBTINT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TIMEZZO,TITIPO,TICODICE,TIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TIMEZZO,TITIPO,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TIMEZZO,TITIPO,TICODICE,TIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(3));
                     +" and TIMEZZO="+cp_ToStrODBC(this.w_MBTIPO);
                     +" and TITIPO="+cp_ToStrODBC(this.w_MBTINT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TIMEZZO',oSource.xKey(1);
                       ,'TITIPO',oSource.xKey(2);
                       ,'TICODICE',oSource.xKey(3))
            select TIMEZZO,TITIPO,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MBCODINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TIMEZZO,TITIPO,TICODICE,TIDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_MBCODINT);
                   +" and TIMEZZO="+cp_ToStrODBC(this.w_MBTIPO);
                   +" and TITIPO="+cp_ToStrODBC(this.w_MBTINT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TIMEZZO',this.w_MBTIPO;
                       ,'TITIPO',this.w_MBTINT;
                       ,'TICODICE',this.w_MBCODINT)
            select TIMEZZO,TITIPO,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MBCODINT = NVL(_Link_.TICODICE,space(10))
      this.w_MBDESINT = NVL(_Link_.TIDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MBCODINT = space(10)
      endif
      this.w_MBDESINT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2])+'\'+cp_ToStr(_Link_.TIMEZZO,1)+'\'+cp_ToStr(_Link_.TITIPO,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.INTERVENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MBCODINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMBSERIALE_1_1.value==this.w_MBSERIALE)
      this.oPgFrm.Page1.oPag.oMBSERIALE_1_1.value=this.w_MBSERIALE
    endif
    if not(this.oPgFrm.Page1.oPag.oMBCODICE_1_3.value==this.w_MBCODICE)
      this.oPgFrm.Page1.oPag.oMBCODICE_1_3.value=this.w_MBCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDescri_1_10.value==this.w_Descri)
      this.oPgFrm.Page1.oPag.oDescri_1_10.value=this.w_Descri
    endif
    if not(this.oPgFrm.Page1.oPag.oMBDTANAG_1_11.value==this.w_MBDTANAG)
      this.oPgFrm.Page1.oPag.oMBDTANAG_1_11.value=this.w_MBDTANAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMBDATA_1_13.value==this.w_MBDATA)
      this.oPgFrm.Page1.oPag.oMBDATA_1_13.value=this.w_MBDATA
    endif
    if not(this.oPgFrm.Page1.oPag.oMBTOTORE_3_1.value==this.w_MBTOTORE)
      this.oPgFrm.Page1.oPag.oMBTOTORE_3_1.value=this.w_MBTOTORE
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMBDURORE_2_2.value==this.w_MBDURORE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMBDURORE_2_2.value=this.w_MBDURORE
      replace t_MBDURORE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMBDURORE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMBTINT_2_3.value==this.w_MBTINT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMBTINT_2_3.value=this.w_MBTINT
      replace t_MBTINT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMBTINT_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMBCODINT_2_4.value==this.w_MBCODINT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMBCODINT_2_4.value=this.w_MBCODINT
      replace t_MBCODINT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMBCODINT_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMBDESINT_2_5.value==this.w_MBDESINT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMBDESINT_2_5.value=this.w_MBDESINT
      replace t_MBDESINT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMBDESINT_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'MANM_BATT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- mbgb_mgb
      i_bRes=.T.
      IF this.cFunction='Load'
          this.w_UTCC=i_codute
          this.w_UTDC=datetime()
      ENDIF
      IF this.cFunction='Edit'
          this.w_UTCV=i_codute
          this.w_UTDV=datetime()
      ENDIF
      this.NotifyEvent('Archivio')
      IF this.w_OK = .F.
          i_bRes=.F.
      ENDIF
      this.w_OK = .T.
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_MBDESINT))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_MBDESINT)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_MBDURORE=0
      .w_MBTINT=space(1)
      .w_MBCODINT=space(10)
      .w_MBDESINT=space(40)
      .DoRTCalc(1,6,.f.)
      if not(empty(.w_MBTINT))
        .link_2_3('Full')
      endif
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_MBCODINT))
        .link_2_4('Full')
      endif
    endwith
    this.DoRTCalc(8,20,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_MBDURORE = t_MBDURORE
    this.w_MBTINT = t_MBTINT
    this.w_MBCODINT = t_MBCODINT
    this.w_MBDESINT = t_MBDESINT
    this.w_CPROWNUM = t_CPROWNUM
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MBDURORE with this.w_MBDURORE
    replace t_MBTINT with this.w_MBTINT
    replace t_MBCODINT with this.w_MBCODINT
    replace t_MBDESINT with this.w_MBDESINT
    replace t_CPROWNUM with this.w_CPROWNUM
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_MBTOTORE = .w_MBTOTORE-.w_mbdurore
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tmbgb_mgbPag1 as StdContainer
  Width  = 589
  height = 421
  stdWidth  = 589
  stdheight = 421
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMBSERIALE_1_1 as StdField with uid="XUXXCXWSNB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MBSERIALE", cQueryName = "MBSERIALE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 56620712,;
   bGlobalFont=.t.,;
    Height=21, Width=96, Left=98, Top=9, InputMask=replicate('X',10)

  add object oMBCODICE_1_3 as StdField with uid="VBERVNXCGQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MBCODICE", cQueryName = "MBCODICE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 197723393,;
   bGlobalFont=.t.,;
    Height=21, Width=96, Left=98, Top=34, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="ANAGRAFE", cZoomOnZoom="ANME_GEME", oKey_1_1="ANTIPO", oKey_1_2="this.w_MBTIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_MBCODICE"

  func oMBCODICE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMBCODICE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMBCODICE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ANAGRAFE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPO="+cp_ToStrODBC(this.Parent.oContained.w_MBTIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPO="+cp_ToStr(this.Parent.oContained.w_MBTIPO)
    endif
    do cp_zoom with 'ANAGRAFE','*','ANTIPO,ANCODICE',cp_AbsName(this.parent,'oMBCODICE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'ANME_GEME',"",'',this.parent.oContained
  endproc
  proc oMBCODICE_1_3.mZoomOnZoom
    local i_obj
    i_obj=ANME_GEME()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPO=w_MBTIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_MBCODICE
    i_obj.ecpSave()
  endproc

  add object oDescri_1_10 as StdField with uid="KRWICHSSEM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_Descri", cQueryName = "Descri",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 20961492,;
   bGlobalFont=.t.,;
    Height=21, Width=384, Left=192, Top=34, InputMask=replicate('X',100)

  add object oMBDTANAG_1_11 as StdField with uid="UCWSKOZVHM",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MBDTANAG", cQueryName = "MBDTANAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 258075389,;
   bGlobalFont=.t.,;
    Height=21, Width=99, Left=195, Top=67

  add object oMBDATA_1_13 as StdField with uid="VUZOMMCGMZ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MBDATA", cQueryName = "MBDATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 189066052,;
   bGlobalFont=.t.,;
    Height=21, Width=99, Left=195, Top=95


  add object oBtn_1_21 as StdButton with uid="RIGYLUATQM",left=453, top=383, width=116,height=25,;
    caption="Salva / Stampa", nPag=1;
    , ToolTipText = "Cliccare per Salvare e Stampare";
    , HelpContextID = 221658476;
  , bGlobalFont=.t.

    proc oBtn_1_21.Click()
      with this.Parent.oContained
        GCSC_BSM(this.Parent.oContained,"B")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    with this.Parent.oContained
      return (.not.empty(.w_MBCODINT) or .w_SAVE=.T.)
    endwith
  endfunc


  add object oObj_1_22 as cp_runprogram with uid="HXQBIZKGCM",left=121, top=484, width=50,height=50,;
    caption='Object',;
    prg="MCGM_BGM('G')",;
    cEvent = "Insert row end,Init Row",;
    nPag=1;
    , HelpContextID = 147952860;
  , bGlobalFont=.t.


  add object oStr_1_4 as StdString with uid="MKLKXPRKLI",Visible=.t., Left=10, Top=38,;
    Alignment=1, Width=87, Height=18,;
    Caption="Codice Batteria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="CCOVSZGMVI",Visible=.t., Left=13, Top=156,;
    Alignment=0, Width=53, Height=18,;
    Caption="Intervento"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="NJCSTGLUCS",Visible=.t., Left=67, Top=156,;
    Alignment=0, Width=43, Height=18,;
    Caption="Durata"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="WTOOVORQLA",Visible=.t., Left=146, Top=156,;
    Alignment=0, Width=49, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="EGPSAVKAHL",Visible=.t., Left=239, Top=156,;
    Alignment=0, Width=67, Height=18,;
    Caption="Descrizione"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="RIRCSALPIN",Visible=.t., Left=115, Top=156,;
    Alignment=0, Width=26, Height=18,;
    Caption="Tipo"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="BDAIRKQNFS",Visible=.t., Left=7, Top=71,;
    Alignment=1, Width=184, Height=18,;
    Caption="Data Ultimo intervento Anagrafica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="BPOLYZUHZM",Visible=.t., Left=83, Top=99,;
    Alignment=1, Width=108, Height=18,;
    Caption="Data Manutenzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="IRFFLTAZWH",Visible=.t., Left=55, Top=13,;
    Alignment=1, Width=42, Height=18,;
    Caption="Seriale:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=3,top=178,;
    width=569+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=4,top=179,width=568+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TIPOINTERVENTO|INTERVENTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TIPOINTERVENTO'
        oDropInto=this.oBodyCol.oRow.oMBTINT_2_3
      case cFile='INTERVENTI'
        oDropInto=this.oBodyCol.oRow.oMBCODINT_2_4
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oMBTOTORE_3_1 as StdField with uid="AZGJZHOSRB",rtseq=13,rtrep=.f.,;
    cFormVar="w_MBTOTORE",value=0,;
    HelpContextID = 46798081,;
    cQueryName = "MBTOTORE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=95, Top=384, cSayPict=["99999"], cGetPict=["99999"]

  add object oObj_3_3 as cp_runprogram with uid="TXZIDGFNAS",width=50,height=50,;
   left=60, top=484,;
    caption='Object',;
    prg="MCGM_BGM('D')",;
    cEvent = "Archivio",;
    nPag=3;
    , HelpContextID = 147952860;
  , bGlobalFont=.t.


  add object oStr_3_2 as StdString with uid="SVJBRLOJYO",Visible=.t., Left=18, Top=388,;
    Alignment=1, Width=76, Height=18,;
    Caption="Durata Totale:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tmbgb_mgbBodyRow as container
  Width=559
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="ECYWHAMPAZ",rtseq=4,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 49939040,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oMBDURORE_2_2 as StdTrsField with uid="DFZBBWDQUI",rtseq=5,rtrep=.t.,;
    cFormVar="w_MBDURORE",value=0,;
    HelpContextID = 45028609,;
    cTotal = "this.Parent.oContained.w_mbtotore", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=49, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oMBTINT_2_3 as StdTrsField with uid="UCWRIZSBSW",rtseq=6,rtrep=.t.,;
    cFormVar="w_MBTINT",value=space(1),;
    HelpContextID = 144436036,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=28, Left=100, Top=0, InputMask=replicate('X',1), bHasZoom = .t. , cLinkFile="TIPOINTERVENTO", cZoomOnZoom="MICG_MMI", oKey_1_1="MICODICE", oKey_1_2="this.w_MBTINT"

  func oMBTINT_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
      if .not. empty(.w_MBCODINT)
        bRes2=.link_2_4('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMBTINT_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMBTINT_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPOINTERVENTO','*','MICODICE',cp_AbsName(this.parent,'oMBTINT_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'MICG_MMI',"",'',this.parent.oContained
  endproc
  proc oMBTINT_2_3.mZoomOnZoom
    local i_obj
    i_obj=MICG_MMI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MICODICE=this.parent.oContained.w_MBTINT
    i_obj.ecpSave()
  endproc

  add object oMBCODINT_2_4 as StdTrsField with uid="DSZRXDVNFU",rtseq=7,rtrep=.t.,;
    cFormVar="w_MBCODINT",value=space(10),;
    HelpContextID = 70712048,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=92, Left=131, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="INTERVENTI", cZoomOnZoom="TIGC_MTI", oKey_1_1="TIMEZZO", oKey_1_2="this.w_MBTIPO", oKey_2_1="TITIPO", oKey_2_2="this.w_MBTINT", oKey_3_1="TICODICE", oKey_3_2="this.w_MBCODINT"

  func oMBCODINT_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oMBCODINT_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMBCODINT_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INTERVENTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TIMEZZO="+cp_ToStrODBC(this.Parent.oContained.w_MBTIPO)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TITIPO="+cp_ToStrODBC(this.Parent.oContained.w_MBTINT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TIMEZZO="+cp_ToStr(this.Parent.oContained.w_MBTIPO)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TITIPO="+cp_ToStr(this.Parent.oContained.w_MBTINT)
    endif
    do cp_zoom with 'INTERVENTI','*','TIMEZZO,TITIPO,TICODICE',cp_AbsName(this.parent,'oMBCODINT_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'TIGC_MTI',"",'',this.parent.oContained
  endproc
  proc oMBCODINT_2_4.mZoomOnZoom
    local i_obj
    i_obj=TIGC_MTI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TIMEZZO=w_MBTIPO
    i_obj.TITIPO=w_MBTINT
     i_obj.w_TICODICE=this.parent.oContained.w_MBCODINT
    i_obj.ecpSave()
  endproc

  add object oMBDESINT_2_5 as StdTrsField with uid="BMVJBCUBSN",rtseq=8,rtrep=.t.,;
    cFormVar="w_MBDESINT",value=space(40),;
    HelpContextID = 55634672,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=330, Left=224, Top=0, cSayPict=["!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"], cGetPict=["!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"], InputMask=replicate('X',40)
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('mbgb_mgb','MANM_BATT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MBSERIALE=MANM_BATT.MBSERIALE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
