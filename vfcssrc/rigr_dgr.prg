* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: rigr_dgr                                                        *
*              Richieste                                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-10-16                                                      *
* Last revis.: 2016-08-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("trigr_dgr"))

* --- Class definition
define class trigr_dgr as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 596
  Height = 431+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-08-30"
  HelpContextID=39416318
  max_rt_seq=18

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  RICHIESTE_IDX = 0
  ANAGRAFE_IDX = 0
  INTERVENTI_IDX = 0
  Gomme_IDX = 0
  cFile = "RICHIESTE"
  cKeySelect = "RISERIALE"
  cKeyWhere  = "RISERIALE=this.w_RISERIALE"
  cKeyDetail  = "RISERIALE=this.w_RISERIALE"
  cKeyWhereODBC = '"RISERIALE="+cp_ToStrODBC(this.w_RISERIALE)';

  cKeyDetailWhereODBC = '"RISERIALE="+cp_ToStrODBC(this.w_RISERIALE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"RICHIESTE.RISERIALE="+cp_ToStrODBC(this.w_RISERIALE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'RICHIESTE.CPROWORD '
  cPrg = "rigr_dgr"
  cComment = "Richieste"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RISERIALE = space(10)
  w_RIDATA = ctod('  /  /  ')
  w_RITIPO = space(1)
  w_RICODICE = space(10)
  w_RIMEZMATRI = space(15)
  w_CPROWORD = 0
  w_RICODIN = space(10)
  w_RINOTE = space(100)
  w_RIDESCRI = space(40)
  w_RIQUANTI = 0
  w_RIMATRI = space(10)
  w_DESCRI = space(100)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_OBSOTEST = ctod('  /  /  ')
  w_TOTRIG = 0

  * --- Autonumbered Variables
  op_RISERIALE = this.W_RISERIALE
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'RICHIESTE','rigr_dgr')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","trigr_dgrPag1","rigr_dgr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Richieste")
      .Pages(1).HelpContextID = 161821509
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRISERIALE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='ANAGRAFE'
    this.cWorkTables[2]='INTERVENTI'
    this.cWorkTables[3]='Gomme'
    this.cWorkTables[4]='RICHIESTE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RICHIESTE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RICHIESTE_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_RISERIALE = NVL(RISERIALE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_7_joined
    link_1_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from RICHIESTE where RISERIALE=KeySet.RISERIALE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.RICHIESTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RICHIESTE_IDX,2],this.bLoadRecFilter,this.RICHIESTE_IDX,"rigr_dgr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RICHIESTE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RICHIESTE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RICHIESTE '
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RISERIALE',this.w_RISERIALE  )
      select * from (i_cTable) RICHIESTE where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESCRI = space(100)
        .w_OBSOTEST = I_DATSYS
        .w_RISERIALE = NVL(RISERIALE,space(10))
        .op_RISERIALE = .w_RISERIALE
        .w_RIDATA = NVL(cp_ToDate(RIDATA),ctod("  /  /  "))
        .w_RITIPO = NVL(RITIPO,space(1))
        .w_RICODICE = NVL(RICODICE,space(10))
          if link_1_7_joined
            this.w_RICODICE = NVL(ANCODICE107,NVL(this.w_RICODICE,space(10)))
            this.w_DESCRI = NVL(ANDESCRI107,space(100))
            this.w_RIMEZMATRI = NVL(ANMATRI107,space(15))
          else
          .link_1_7('Load')
          endif
        .w_RIMEZMATRI = NVL(RIMEZMATRI,space(15))
        .w_RINOTE = NVL(RINOTE,space(100))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_TOTRIG = .numrow()
        cp_LoadRecExtFlds(this,'RICHIESTE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_RICODIN = NVL(RICODIN,space(10))
          * evitabile
          *.link_2_2('Load')
          .w_RIDESCRI = NVL(RIDESCRI,space(40))
          .w_RIQUANTI = NVL(RIQUANTI,0)
          .w_RIMATRI = NVL(RIMATRI,space(10))
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_6.Calculate()
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_TOTRIG = .numrow()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_RISERIALE=space(10)
      .w_RIDATA=ctod("  /  /  ")
      .w_RITIPO=space(1)
      .w_RICODICE=space(10)
      .w_RIMEZMATRI=space(15)
      .w_CPROWORD=10
      .w_RICODIN=space(10)
      .w_RINOTE=space(100)
      .w_RIDESCRI=space(40)
      .w_RIQUANTI=0
      .w_RIMATRI=space(10)
      .w_DESCRI=space(100)
      .w_UTCC=0
      .w_UTCV=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_OBSOTEST=ctod("  /  /  ")
      .w_TOTRIG=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_RIDATA = i_datsys
        .w_RITIPO = 'C'
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_RICODICE))
         .link_1_7('Full')
        endif
        .DoRTCalc(5,7,.f.)
        if not(empty(.w_RICODIN))
         .link_2_2('Full')
        endif
        .DoRTCalc(8,9,.f.)
        .w_RIQUANTI = 1
        .DoRTCalc(11,16,.f.)
        .w_OBSOTEST = I_DATSYS
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_6.Calculate()
        .w_TOTRIG = .numrow()
      endif
    endwith
    cp_BlankRecExtFlds(this,'RICHIESTE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRISERIALE_1_1.enabled = i_bVal
      .Page1.oPag.oRIDATA_1_3.enabled = i_bVal
      .Page1.oPag.oRITIPO_1_5.enabled = i_bVal
      .Page1.oPag.oRICODICE_1_7.enabled = i_bVal
      .Page1.oPag.oRIMEZMATRI_1_8.enabled = i_bVal
      .Page1.oPag.oRINOTE_1_10.enabled = i_bVal
      .Page1.oPag.oBtn_1_22.enabled = i_bVal
      .Page1.oPag.oBtn_1_24.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oRISERIALE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oRISERIALE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'RICHIESTE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RICHIESTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RICHIESTE_IDX,2])
    cp_AskTableProg(this,i_nConn,"RICHIESTE","w_RISERIALE")
    with this
      .op_RISERIALE = .w_RISERIALE
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RICHIESTE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RISERIALE,"RISERIALE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIDATA,"RIDATA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RITIPO,"RITIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RICODICE,"RICODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RIMEZMATRI,"RIMEZMATRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RINOTE,"RINOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RICHIESTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RICHIESTE_IDX,2])
    i_lTable = "RICHIESTE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.RICHIESTE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        RISR_BSR(this,"S")
      endwith
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_RICODIN C(10);
      ,t_RIDESCRI C(40);
      ,t_RIQUANTI N(3);
      ,t_RIMATRI C(10);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','trigr_dgrbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRICODIN_2_2.controlsource=this.cTrsName+'.t_RICODIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRIDESCRI_2_3.controlsource=this.cTrsName+'.t_RIDESCRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRIQUANTI_2_4.controlsource=this.cTrsName+'.t_RIQUANTI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRIMATRI_2_5.controlsource=this.cTrsName+'.t_RIMATRI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(45)
    this.AddVLine(139)
    this.AddVLine(430)
    this.AddVLine(465)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RICHIESTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RICHIESTE_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"RICHIESTE","w_RISERIALE")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RICHIESTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RICHIESTE_IDX,2])
      *
      * insert into RICHIESTE
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RICHIESTE')
        i_extval=cp_InsertValODBCExtFlds(this,'RICHIESTE')
        i_cFldBody=" "+;
                  "(RISERIALE,RIDATA,RITIPO,RICODICE,RIMEZMATRI"+;
                  ",CPROWORD,RICODIN,RINOTE,RIDESCRI,RIQUANTI"+;
                  ",RIMATRI,UTCC,UTCV,UTDC,UTDV,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_RISERIALE)+","+cp_ToStrODBC(this.w_RIDATA)+","+cp_ToStrODBC(this.w_RITIPO)+","+cp_ToStrODBCNull(this.w_RICODICE)+","+cp_ToStrODBC(this.w_RIMEZMATRI)+;
             ","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_RICODIN)+","+cp_ToStrODBC(this.w_RINOTE)+","+cp_ToStrODBC(this.w_RIDESCRI)+","+cp_ToStrODBC(this.w_RIQUANTI)+;
             ","+cp_ToStrODBC(this.w_RIMATRI)+","+cp_ToStrODBC(this.w_UTCC)+","+cp_ToStrODBC(this.w_UTCV)+","+cp_ToStrODBC(this.w_UTDC)+","+cp_ToStrODBC(this.w_UTDV)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RICHIESTE')
        i_extval=cp_InsertValVFPExtFlds(this,'RICHIESTE')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'RISERIALE',this.w_RISERIALE)
        INSERT INTO (i_cTable) (;
                   RISERIALE;
                  ,RIDATA;
                  ,RITIPO;
                  ,RICODICE;
                  ,RIMEZMATRI;
                  ,CPROWORD;
                  ,RICODIN;
                  ,RINOTE;
                  ,RIDESCRI;
                  ,RIQUANTI;
                  ,RIMATRI;
                  ,UTCC;
                  ,UTCV;
                  ,UTDC;
                  ,UTDV;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_RISERIALE;
                  ,this.w_RIDATA;
                  ,this.w_RITIPO;
                  ,this.w_RICODICE;
                  ,this.w_RIMEZMATRI;
                  ,this.w_CPROWORD;
                  ,this.w_RICODIN;
                  ,this.w_RINOTE;
                  ,this.w_RIDESCRI;
                  ,this.w_RIQUANTI;
                  ,this.w_RIMATRI;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.RICHIESTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RICHIESTE_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_RICODIN))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'RICHIESTE')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " RIDATA="+cp_ToStrODBC(this.w_RIDATA)+;
                 ",RITIPO="+cp_ToStrODBC(this.w_RITIPO)+;
                 ",RICODICE="+cp_ToStrODBCNull(this.w_RICODICE)+;
                 ",RIMEZMATRI="+cp_ToStrODBC(this.w_RIMEZMATRI)+;
                 ",RINOTE="+cp_ToStrODBC(this.w_RINOTE)+;
                 ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
                 ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
                 ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
                 ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'RICHIESTE')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  RIDATA=this.w_RIDATA;
                 ,RITIPO=this.w_RITIPO;
                 ,RICODICE=this.w_RICODICE;
                 ,RIMEZMATRI=this.w_RIMEZMATRI;
                 ,RINOTE=this.w_RINOTE;
                 ,UTCC=this.w_UTCC;
                 ,UTCV=this.w_UTCV;
                 ,UTDC=this.w_UTDC;
                 ,UTDV=this.w_UTDV;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_RICODIN))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update RICHIESTE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'RICHIESTE')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " RIDATA="+cp_ToStrODBC(this.w_RIDATA)+;
                     ",RITIPO="+cp_ToStrODBC(this.w_RITIPO)+;
                     ",RICODICE="+cp_ToStrODBCNull(this.w_RICODICE)+;
                     ",RIMEZMATRI="+cp_ToStrODBC(this.w_RIMEZMATRI)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",RICODIN="+cp_ToStrODBCNull(this.w_RICODIN)+;
                     ",RINOTE="+cp_ToStrODBC(this.w_RINOTE)+;
                     ",RIDESCRI="+cp_ToStrODBC(this.w_RIDESCRI)+;
                     ",RIQUANTI="+cp_ToStrODBC(this.w_RIQUANTI)+;
                     ",RIMATRI="+cp_ToStrODBC(this.w_RIMATRI)+;
                     ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
                     ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
                     ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
                     ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'RICHIESTE')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      RIDATA=this.w_RIDATA;
                     ,RITIPO=this.w_RITIPO;
                     ,RICODICE=this.w_RICODICE;
                     ,RIMEZMATRI=this.w_RIMEZMATRI;
                     ,CPROWORD=this.w_CPROWORD;
                     ,RICODIN=this.w_RICODIN;
                     ,RINOTE=this.w_RINOTE;
                     ,RIDESCRI=this.w_RIDESCRI;
                     ,RIQUANTI=this.w_RIQUANTI;
                     ,RIMATRI=this.w_RIMATRI;
                     ,UTCC=this.w_UTCC;
                     ,UTCV=this.w_UTCV;
                     ,UTDC=this.w_UTDC;
                     ,UTDV=this.w_UTDV;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RICHIESTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RICHIESTE_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_RICODIN))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete RICHIESTE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_RICODIN))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RICHIESTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RICHIESTE_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_6.Calculate()
        .DoRTCalc(1,17,.t.)
          .w_TOTRIG = .numrow()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_6.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_6.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mEnableControlsFixed()
    this.mHideControls()
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_6.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RICODICE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
    i_lTable = "ANAGRAFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2], .t., this.ANAGRAFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RICODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('ANME_GEME',True,'ANAGRAFE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_RICODICE)+"%");
                   +" and ANTIPO="+cp_ToStrODBC(this.w_RITIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPO,ANCODICE,ANDESCRI,ANMATRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPO,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPO',this.w_RITIPO;
                     ,'ANCODICE',trim(this.w_RICODICE))
          select ANTIPO,ANCODICE,ANDESCRI,ANMATRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPO,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RICODICE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RICODICE) and !this.bDontReportError
            deferred_cp_zoom('ANAGRAFE','*','ANTIPO,ANCODICE',cp_AbsName(oSource.parent,'oRICODICE_1_7'),i_cWhere,'ANME_GEME',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_RITIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPO,ANCODICE,ANDESCRI,ANMATRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPO,ANCODICE,ANDESCRI,ANMATRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPO,ANCODICE,ANDESCRI,ANMATRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPO="+cp_ToStrODBC(this.w_RITIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPO',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPO,ANCODICE,ANDESCRI,ANMATRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RICODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPO,ANCODICE,ANDESCRI,ANMATRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_RICODICE);
                   +" and ANTIPO="+cp_ToStrODBC(this.w_RITIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPO',this.w_RITIPO;
                       ,'ANCODICE',this.w_RICODICE)
            select ANTIPO,ANCODICE,ANDESCRI,ANMATRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RICODICE = NVL(_Link_.ANCODICE,space(10))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(100))
      this.w_RIMEZMATRI = NVL(_Link_.ANMATRI,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_RICODICE = space(10)
      endif
      this.w_DESCRI = space(100)
      this.w_RIMEZMATRI = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPO,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.ANAGRAFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RICODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ANAGRAFE_IDX,3] and i_nFlds+3<200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.ANCODICE as ANCODICE107"+ ",link_1_7.ANDESCRI as ANDESCRI107"+ ",link_1_7.ANMATRI as ANMATRI107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on RICHIESTE.RICODICE=link_1_7.ANCODICE"+" and RICHIESTE.RITIPO=link_1_7.ANTIPO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and RICHIESTE.RICODICE=link_1_7.ANCODICE(+)"'+'+" and RICHIESTE.RITIPO=link_1_7.ANTIPO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=RICODIN
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INTERVENTI_IDX,3]
    i_lTable = "INTERVENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2], .t., this.INTERVENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RICODIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('TIGC_MTI',True,'INTERVENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_RICODIN)+"%");
                   +" and TITIPO="+cp_ToStrODBC('S');

          i_ret=cp_SQL(i_nConn,"select TITIPO,TICODICE,TIDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TITIPO,TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TITIPO','S';
                     ,'TICODICE',trim(this.w_RICODIN))
          select TITIPO,TICODICE,TIDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TITIPO,TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RICODIN)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RICODIN) and !this.bDontReportError
            deferred_cp_zoom('INTERVENTI','*','TITIPO,TICODICE',cp_AbsName(oSource.parent,'oRICODIN_2_2'),i_cWhere,'TIGC_MTI',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if 'S'<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TITIPO,TICODICE,TIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TITIPO,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TITIPO,TICODICE,TIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TITIPO="+cp_ToStrODBC('S');
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TITIPO',oSource.xKey(1);
                       ,'TICODICE',oSource.xKey(2))
            select TITIPO,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RICODIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TITIPO,TICODICE,TIDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_RICODIN);
                   +" and TITIPO="+cp_ToStrODBC('S');
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TITIPO','S';
                       ,'TICODICE',this.w_RICODIN)
            select TITIPO,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RICODIN = NVL(_Link_.TICODICE,space(10))
      this.w_RIDESCRI = NVL(_Link_.TIDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RICODIN = space(10)
      endif
      this.w_RIDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2])+'\'+cp_ToStr(_Link_.TITIPO,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.INTERVENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RICODIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oRISERIALE_1_1.value==this.w_RISERIALE)
      this.oPgFrm.Page1.oPag.oRISERIALE_1_1.value=this.w_RISERIALE
    endif
    if not(this.oPgFrm.Page1.oPag.oRIDATA_1_3.value==this.w_RIDATA)
      this.oPgFrm.Page1.oPag.oRIDATA_1_3.value=this.w_RIDATA
    endif
    if not(this.oPgFrm.Page1.oPag.oRITIPO_1_5.RadioValue()==this.w_RITIPO)
      this.oPgFrm.Page1.oPag.oRITIPO_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRICODICE_1_7.value==this.w_RICODICE)
      this.oPgFrm.Page1.oPag.oRICODICE_1_7.value=this.w_RICODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oRIMEZMATRI_1_8.value==this.w_RIMEZMATRI)
      this.oPgFrm.Page1.oPag.oRIMEZMATRI_1_8.value=this.w_RIMEZMATRI
    endif
    if not(this.oPgFrm.Page1.oPag.oRINOTE_1_10.value==this.w_RINOTE)
      this.oPgFrm.Page1.oPag.oRINOTE_1_10.value=this.w_RINOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_14.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_14.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTRIG_1_25.value==this.w_TOTRIG)
      this.oPgFrm.Page1.oPag.oTOTRIG_1_25.value=this.w_TOTRIG
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRICODIN_2_2.value==this.w_RICODIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRICODIN_2_2.value=this.w_RICODIN
      replace t_RICODIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRICODIN_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIDESCRI_2_3.value==this.w_RIDESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIDESCRI_2_3.value=this.w_RIDESCRI
      replace t_RIDESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIDESCRI_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIQUANTI_2_4.value==this.w_RIQUANTI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIQUANTI_2_4.value=this.w_RIQUANTI
      replace t_RIQUANTI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIQUANTI_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIMATRI_2_5.value==this.w_RIMATRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIMATRI_2_5.value=this.w_RIMATRI
      replace t_RIMATRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRIMATRI_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'RICHIESTE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- rigr_dgr
      IF this.cFunction='Load'
          this.w_UTCC=i_codute
          this.w_UTDC=i_datsys
      ENDIF
      IF this.cFunction='Edit'
          this.w_UTCV=i_codute
          this.w_UTDV=i_datsys
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_RICODIN)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_RICODIN) and (not(Empty(.w_RICODIN)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRICODIN_2_2
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_RICODIN))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_RICODIN)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_RICODIN=space(10)
      .w_RIDESCRI=space(40)
      .w_RIQUANTI=0
      .w_RIMATRI=space(10)
      .DoRTCalc(1,7,.f.)
      if not(empty(.w_RICODIN))
        .link_2_2('Full')
      endif
      .DoRTCalc(8,9,.f.)
        .w_RIQUANTI = 1
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_6.Calculate()
    endwith
    this.DoRTCalc(11,18,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_RICODIN = t_RICODIN
    this.w_RIDESCRI = t_RIDESCRI
    this.w_RIQUANTI = t_RIQUANTI
    this.w_RIMATRI = t_RIMATRI
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_RICODIN with this.w_RICODIN
    replace t_RIDESCRI with this.w_RIDESCRI
    replace t_RIQUANTI with this.w_RIQUANTI
    replace t_RIMATRI with this.w_RIMATRI
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class trigr_dgrPag1 as StdContainer
  Width  = 592
  height = 431
  stdWidth  = 592
  stdheight = 431
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRISERIALE_1_1 as StdField with uid="SVEOQMNZXV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_RISERIALE", cQueryName = "RISERIALE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 65990371,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=61, Top=11, InputMask=replicate('X',10)

  add object oRIDATA_1_3 as StdField with uid="MXNWAKMITV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RIDATA", cQueryName = "RIDATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 198435711,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=254, Top=10


  add object oRITIPO_1_5 as StdCombo with uid="IGJTYVWVSM",rtseq=3,rtrep=.f.,left=13,top=63,width=88,height=22;
    , HelpContextID = 235594623;
    , cFormVar="w_RITIPO",RowSource=""+"Batterie,"+"Carrelli,"+"Mezzi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRITIPO_1_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RITIPO,&i_cF..t_RITIPO),this.value)
    return(iif(xVal =1,'B',;
    iif(xVal =2,'C',;
    iif(xVal =3,'M',;
    space(1)))))
  endfunc
  func oRITIPO_1_5.GetRadio()
    this.Parent.oContained.w_RITIPO = this.RadioValue()
    return .t.
  endfunc

  func oRITIPO_1_5.ToRadio()
    this.Parent.oContained.w_RITIPO=trim(this.Parent.oContained.w_RITIPO)
    return(;
      iif(this.Parent.oContained.w_RITIPO=='B',1,;
      iif(this.Parent.oContained.w_RITIPO=='C',2,;
      iif(this.Parent.oContained.w_RITIPO=='M',3,;
      0))))
  endfunc

  func oRITIPO_1_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oRITIPO_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_RICODICE)
        bRes2=.link_1_7('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oRICODICE_1_7 as StdField with uid="YZINPFURDD",rtseq=4,rtrep=.f.,;
    cFormVar = "w_RICODICE", cQueryName = "RICODICE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 188353734,;
   bGlobalFont=.t.,;
    Height=21, Width=102, Left=105, Top=62, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="ANAGRAFE", cZoomOnZoom="ANME_GEME", oKey_1_1="ANTIPO", oKey_1_2="this.w_RITIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_RICODICE"

  func oRICODICE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oRICODICE_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRICODICE_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ANAGRAFE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPO="+cp_ToStrODBC(this.Parent.oContained.w_RITIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPO="+cp_ToStr(this.Parent.oContained.w_RITIPO)
    endif
    do cp_zoom with 'ANAGRAFE','*','ANTIPO,ANCODICE',cp_AbsName(this.parent,'oRICODICE_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'ANME_GEME',"",'',this.parent.oContained
  endproc
  proc oRICODICE_1_7.mZoomOnZoom
    local i_obj
    i_obj=ANME_GEME()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPO=w_RITIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_RICODICE
    i_obj.ecpSave()
  endproc

  add object oRIMEZMATRI_1_8 as StdField with uid="RULVLBOZMO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_RIMEZMATRI", cQueryName = "RIMEZMATRI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 258934027,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=72, Top=89, InputMask=replicate('X',15)

  add object oRINOTE_1_10 as StdField with uid="KVYHBEKGDS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_RINOTE", cQueryName = "RINOTE",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 130368383,;
   bGlobalFont=.t.,;
    Height=21, Width=536, Left=47, Top=126, cSayPict="'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'", cGetPict="'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'", InputMask=replicate('X',100)

  add object oDESCRI_1_14 as StdField with uid="HPBWYYPMFO",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 66123871,;
   bGlobalFont=.t.,;
    Height=21, Width=379, Left=204, Top=62, InputMask=replicate('X',100)


  add object oBtn_1_22 as StdButton with uid="SCZTGDPNNI",left=464, top=391, width=114,height=25,;
    caption="Salva/Stampa", nPag=1;
    , ToolTipText = "Cliccare per Salvare e Stampare";
    , HelpContextID = 207869931;
  , bGlobalFont=.t.

    proc oBtn_1_22.Click()
      with this.Parent.oContained
        RISR_BSR(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    with this.Parent.oContained
      return (.w_TOTRIG>0)
    endwith
  endfunc


  add object oBtn_1_24 as StdButton with uid="FJEUDWHUGT",left=11, top=391, width=95,height=25,;
    caption="RUOTE", nPag=1;
    , HelpContextID = 229648255;
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      do TGZG_KZG with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    with this.Parent.oContained
      return (.cFunction<>"Query" AND NOT EMPTY(.w_RICODICE) )
    endwith
  endfunc

  add object oTOTRIG_1_25 as StdField with uid="KSOWRVJGCO",rtseq=18,rtrep=.f.,;
    cFormVar = "w_TOTRIG", cQueryName = "TOTRIG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale righe inserite",;
    HelpContextID = 108125535,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=378, Top=394

  add object oStr_1_2 as StdString with uid="MFFFSMNGCI",Visible=.t., Left=13, Top=15,;
    Alignment=1, Width=47, Height=18,;
    Caption="Seriale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="RQLDHAQLBB",Visible=.t., Left=169, Top=14,;
    Alignment=1, Width=84, Height=18,;
    Caption="Data Richiesta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="LZYBSQKGCI",Visible=.t., Left=9, Top=40,;
    Alignment=1, Width=42, Height=18,;
    Caption="Mezzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="EYWXNQIRID",Visible=.t., Left=14, Top=93,;
    Alignment=1, Width=57, Height=18,;
    Caption="Matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="MUMTFZMXTH",Visible=.t., Left=40, Top=163,;
    Alignment=0, Width=47, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="TYESTRLPIC",Visible=.t., Left=140, Top=163,;
    Alignment=0, Width=68, Height=18,;
    Caption="Descrizione"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="CQVGIOHYTS",Visible=.t., Left=466, Top=163,;
    Alignment=0, Width=71, Height=18,;
    Caption="Particolare"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="AMOTEMHJJM",Visible=.t., Left=12, Top=128,;
    Alignment=1, Width=29, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="GJTAGBFMCP",Visible=.t., Left=431, Top=163,;
    Alignment=0, Width=28, Height=18,;
    Caption="Pz"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="SIXWPWNHBF",Visible=.t., Left=293, Top=398,;
    Alignment=1, Width=85, Height=18,;
    Caption="Righe Caricate:"  ;
  , bGlobalFont=.t.

  add object oBox_1_15 as StdBox with uid="WZQFTXXVWP",left=8, top=57, width=578,height=63

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=1,top=183,;
    width=572+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=2,top=184,width=571+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='INTERVENTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='INTERVENTI'
        oDropInto=this.oBodyCol.oRow.oRICODIN_2_2
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class trigr_dgrBodyRow as container
  Width=562
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="REEBOPCXYL",rtseq=6,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 40567509,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=32, Left=-2, Top=0

  add object oRICODIN_2_2 as StdTrsField with uid="TTUZCHNCLS",rtseq=7,rtrep=.t.,;
    cFormVar="w_RICODIN",value=space(10),;
    HelpContextID = 80081791,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=91, Left=34, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="INTERVENTI", cZoomOnZoom="TIGC_MTI", oKey_1_1="TITIPO", oKey_1_2="'S'", oKey_2_1="TICODICE", oKey_2_2="this.w_RICODIN"

  func oRICODIN_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oRICODIN_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oRICODIN_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INTERVENTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TITIPO="+cp_ToStrODBC('S')
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TITIPO="+cp_ToStr('S')
    endif
    do cp_zoom with 'INTERVENTI','*','TITIPO,TICODICE',cp_AbsName(this.parent,'oRICODIN_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'TIGC_MTI',"",'',this.parent.oContained
  endproc
  proc oRICODIN_2_2.mZoomOnZoom
    local i_obj
    i_obj=TIGC_MTI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TITIPO='S'
     i_obj.w_TICODICE=this.parent.oContained.w_RICODIN
    i_obj.ecpSave()
  endproc

  add object oRIDESCRI_2_3 as StdTrsField with uid="KDVUXSPDOC",rtseq=9,rtrep=.t.,;
    cFormVar="w_RIDESCRI",value=space(40),;
    HelpContextID = 165667638,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=289, Left=127, Top=0, cSayPict=['!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'], cGetPict=['!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'], InputMask=replicate('X',40)

  add object oRIQUANTI_2_4 as StdTrsField with uid="YOWGHTKOBI",rtseq=10,rtrep=.t.,;
    cFormVar="w_RIQUANTI",value=0,;
    HelpContextID = 1109194,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=418, Top=0

  add object oRIMATRI_2_5 as StdTrsField with uid="XLFIFTAHWV",rtseq=11,rtrep=.t.,;
    cFormVar="w_RIMATRI",value=space(10),;
    HelpContextID = 86813825,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=104, Left=453, Top=0, InputMask=replicate('X',10)

  add object oObj_2_6 as cp_runprogram with uid="NLRODZGVOY",width=50,height=50,;
   left=33, top=325,;
    caption='Object',;
    prg="RISR_BSR('A')",;
    nPag=2;
    , HelpContextID = 138581329;
  , bGlobalFont=.t.

  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('rigr_dgr','RICHIESTE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RISERIALE=RICHIESTE.RISERIALE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
