* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: gcsc_bmc                                                        *
*              GESTIONE STAMPE                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-09-29                                                      *
* Last revis.: 2016-03-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgcsc_bmc",oParentObject)
return(i_retval)

define class tgcsc_bmc as StdBatch
  * --- Local variables
  Print = .f.
  * --- WorkFile variables
  MANM_CAR_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    L_DADATA=this.oParentObject.w_DADATA
    L_ADATA=this.oParentObject.W_ADATA
    this.Print = .F.
    do case
      case this.oParentObject.w_Tipo="C"
        do case
          case this.oParentObject.w_STAMPA="1"
            vq_exec("query\GCSC_QMC.VQR",this,"__TMP__")
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.print=.T.
              CP_CHPRN("QUERY\GCSC_QMC.FRX")
            endif
          case this.oParentObject.w_STAMPA="2"
            vq_exec("query\ANSA_QSA.VQR",this,"__TMP__")
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.print=.T.
              CP_CHPRN("QUERY\ANSA_QSA.FRX")
            endif
          case this.oParentObject.w_STAMPA="3"
            vq_exec("query\ANSA_QST.VQR",this,"__TMP__")
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.print=.T.
              CP_CHPRN("QUERY\ANSA_QST.FRX")
            endif
          otherwise
        endcase
      case this.oParentObject.w_Tipo="B"
        do case
          case this.oParentObject.w_STAMPA="1" 
 
            vq_exec("query\GCSB_QMB.VQR",this,"__TMP__")
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.print=.T.
              CP_CHPRN("QUERY\GCSB_QMB.FRX")
            endif
          case this.oParentObject.w_STAMPA="2" 
 
            vq_exec("query\ANSA_QSA.VQR",this,"__TMP__")
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.print=.T.
              CP_CHPRN("QUERY\ANSA_QSA.FRX")
            endif
          otherwise
        endcase
      case this.oParentObject.w_Tipo="M"
        do case
          case this.oParentObject.w_STAMPA="1" 
 
            vq_exec("query\GCSC_QMC.VQR",this,"__TMP__")
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.print=.T.
              CP_CHPRN("QUERY\GCSC_QMC.FRX")
            endif
          case this.oParentObject.w_STAMPA="2" 
 
            vq_exec("query\ANSA_QSA.VQR",this,"__TMP__")
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.print=.T.
              CP_CHPRN("QUERY\ANSA_QSA.FRX")
            endif
          case this.oParentObject.w_STAMPA="3" 
 
            vq_exec("query\ANSA_QST.VQR",this,"__TMP__")
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.print=.T.
              CP_CHPRN("QUERY\ANSA_QST.FRX")
            endif
          otherwise
        endcase
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    select "__tmp__"
    if reccount()<>0
      this.Print = .T.
    else
      if cp_YesNo("Non ci sono dati da stampare, continuare?","!",,,"ATTENZIONE")
        this.Print = .T.
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MANM_CAR'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
