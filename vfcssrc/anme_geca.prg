* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: anme_geca                                                       *
*              Archivio Carrelli                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-07-24                                                      *
* Last revis.: 2016-07-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tanme_geca"))

* --- Class definition
define class tanme_geca as StdForm
  Top    = 15
  Left   = 30

  * --- Standard Properties
  Width  = 633
  Height = 256+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-07-28"
  HelpContextID=69876574
  max_rt_seq=39

  * --- Constant Properties
  ANAGRAFE_IDX = 0
  REPARTI_IDX = 0
  Gomme_IDX = 0
  cFile = "ANAGRAFE"
  cKeySelect = "ANTIPO,ANCODICE,ANTIPO,ANTIPO"
  cKeyWhere  = "ANTIPO=this.w_ANTIPO and ANCODICE=this.w_ANCODICE and ANTIPO=this.w_ANTIPO and ANTIPO=this.w_ANTIPO"
  cKeyWhereODBC = '"ANTIPO="+cp_ToStrODBC(this.w_ANTIPO)';
      +'+" and ANCODICE="+cp_ToStrODBC(this.w_ANCODICE)';
      +'+" and ANTIPO="+cp_ToStrODBC(this.w_ANTIPO)';
      +'+" and ANTIPO="+cp_ToStrODBC(this.w_ANTIPO)';

  cKeyWhereODBCqualified = '"ANAGRAFE.ANTIPO="+cp_ToStrODBC(this.w_ANTIPO)';
      +'+" and ANAGRAFE.ANCODICE="+cp_ToStrODBC(this.w_ANCODICE)';
      +'+" and ANAGRAFE.ANTIPO="+cp_ToStrODBC(this.w_ANTIPO)';
      +'+" and ANAGRAFE.ANTIPO="+cp_ToStrODBC(this.w_ANTIPO)';

  cPrg = "anme_geca"
  cComment = "Archivio Carrelli"
  icon = "anag.ico"
  cAutoZoom = 'CARRELLI'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ANTIPO = space(1)
  w_ANCODICE = space(10)
  w_ANFLDESCRI = space(1)
  w_ANNUM = 0
  w_ANGPS = space(1)
  w_ANALIM = space(1)
  w_ANVOLT = 0
  w_ANMATRI = space(15)
  w_ANMARCA = space(15)
  w_ANMODE = space(15)
  w_ANREPARTO = space(5)
  w_ANANNO = 0
  w_ANDESCRI = space(100)
  w_ANDESCRI2 = space(40)
  w_DESREP = space(40)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_ANTIPO = space(1)
  w_CODI = space(10)
  w_ANORE = 0
  w_ANPRIN = ctod('  /  /  ')
  w_ANULIN = ctod('  /  /  ')
  w_ANEND = ctod('  /  /  ')
  w_ANPRORE = 0
  w_ANOREMAN = 0
  w_ANTIPO = space(1)
  w_CODI = space(10)
  w_ANGOANT = space(10)
  w_ANNRANT = 0
  w_ANGOPOST = space(10)
  w_ANNRPOS = 0
  w_ANTDESCRI = space(10)
  w_POSDESCRI = space(10)
  w_ANTFORI = 0
  w_POSFORI = 0
  w_DESCRI = space(40)
  w_DESCRI = space(40)

  * --- Autonumbered Variables
  op_ANCODICE = this.W_ANCODICE
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ANAGRAFE','anme_geca')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tanme_gecaPag1","anme_geca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati Carrello")
      .Pages(1).HelpContextID = 152234003
      .Pages(2).addobject("oPag","tanme_gecaPag2","anme_geca",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parti")
      .Pages(2).HelpContextID = 210643602
      .Pages(3).addobject("oPag","tanme_gecaPag3","anme_geca",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Interventi")
      .Pages(3).HelpContextID = 78118615
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='REPARTI'
    this.cWorkTables[2]='Gomme'
    this.cWorkTables[3]='ANAGRAFE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ANAGRAFE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ANAGRAFE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ANTIPO = NVL(ANTIPO,space(1))
      .w_ANCODICE = NVL(ANCODICE,space(10))
      .w_ANTIPO = NVL(ANTIPO,space(1))
      .w_ANTIPO = NVL(ANTIPO,space(1))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_11_joined
    link_1_11_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ANAGRAFE where ANTIPO=KeySet.ANTIPO
    *                            and ANCODICE=KeySet.ANCODICE
    *                            and ANTIPO=KeySet.ANTIPO
    *                            and ANTIPO=KeySet.ANTIPO
    *
    i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ANAGRAFE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ANAGRAFE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ANAGRAFE '
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ANTIPO',this.w_ANTIPO  ,'ANCODICE',this.w_ANCODICE  ,'ANTIPO',this.w_ANTIPO  ,'ANTIPO',this.w_ANTIPO  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESREP = space(40)
        .w_ANTDESCRI = space(10)
        .w_POSDESCRI = space(10)
        .w_ANTFORI = 0
        .w_POSFORI = 0
        .w_ANTIPO = NVL(ANTIPO,space(1))
        .w_ANCODICE = NVL(ANCODICE,space(10))
        .op_ANCODICE = .w_ANCODICE
        .w_ANFLDESCRI = NVL(ANFLDESCRI,space(1))
        .w_ANNUM = NVL(ANNUM,0)
        .w_ANGPS = NVL(ANGPS,space(1))
        .w_ANALIM = NVL(ANALIM,space(1))
        .w_ANVOLT = NVL(ANVOLT,0)
        .w_ANMATRI = NVL(ANMATRI,space(15))
        .w_ANMARCA = NVL(ANMARCA,space(15))
        .w_ANMODE = NVL(ANMODE,space(15))
        .w_ANREPARTO = NVL(ANREPARTO,space(5))
          if link_1_11_joined
            this.w_ANREPARTO = NVL(RECODICE111,NVL(this.w_ANREPARTO,space(5)))
            this.w_DESREP = NVL(REDESCRI111,space(40))
          else
          .link_1_11('Load')
          endif
        .w_ANANNO = NVL(ANANNO,0)
        .w_ANDESCRI = NVL(ANDESCRI,space(100))
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .w_ANDESCRI2 = NVL(ANDESCRI2,space(40))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_ANTIPO = NVL(ANTIPO,space(1))
        .w_CODI = .w_ANCODICE
        .w_ANORE = NVL(ANORE,0)
        .w_ANPRIN = NVL(cp_ToDate(ANPRIN),ctod("  /  /  "))
        .w_ANULIN = NVL(cp_ToDate(ANULIN),ctod("  /  /  "))
        .w_ANEND = NVL(cp_ToDate(ANEND),ctod("  /  /  "))
        .w_ANPRORE = NVL(ANPRORE,0)
        .w_ANOREMAN = NVL(ANOREMAN,0)
        .w_ANTIPO = NVL(ANTIPO,space(1))
        .w_CODI = .w_ANCODICE
        .w_ANGOANT = NVL(ANGOANT,space(10))
          if link_2_3_joined
            this.w_ANGOANT = NVL(SERIALANT203,NVL(this.w_ANGOANT,space(10)))
            this.w_ANTDESCRI = NVL(GOMISURE203,space(10))
            this.w_ANTFORI = NVL(GOFORI203,0)
          else
          .link_2_3('Load')
          endif
        .w_ANNRANT = NVL(ANNRANT,0)
        .w_ANGOPOST = NVL(ANGOPOST,space(10))
          .link_2_5('Load')
        .w_ANNRPOS = NVL(ANNRPOS,0)
        .w_DESCRI = .w_ANDESCRI
        .w_DESCRI = .w_ANDESCRI
        cp_LoadRecExtFlds(this,'ANAGRAFE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ANTIPO = space(1)
      .w_ANCODICE = space(10)
      .w_ANFLDESCRI = space(1)
      .w_ANNUM = 0
      .w_ANGPS = space(1)
      .w_ANALIM = space(1)
      .w_ANVOLT = 0
      .w_ANMATRI = space(15)
      .w_ANMARCA = space(15)
      .w_ANMODE = space(15)
      .w_ANREPARTO = space(5)
      .w_ANANNO = 0
      .w_ANDESCRI = space(100)
      .w_ANDESCRI2 = space(40)
      .w_DESREP = space(40)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_ANTIPO = space(1)
      .w_CODI = space(10)
      .w_ANORE = 0
      .w_ANPRIN = ctod("  /  /  ")
      .w_ANULIN = ctod("  /  /  ")
      .w_ANEND = ctod("  /  /  ")
      .w_ANPRORE = 0
      .w_ANOREMAN = 0
      .w_ANTIPO = space(1)
      .w_CODI = space(10)
      .w_ANGOANT = space(10)
      .w_ANNRANT = 0
      .w_ANGOPOST = space(10)
      .w_ANNRPOS = 0
      .w_ANTDESCRI = space(10)
      .w_POSDESCRI = space(10)
      .w_ANTFORI = 0
      .w_POSFORI = 0
      .w_DESCRI = space(40)
      .w_DESCRI = space(40)
      if .cFunction<>"Filter"
        .w_ANTIPO = 'C'
          .DoRTCalc(2,2,.f.)
        .w_ANFLDESCRI = 'N'
          .DoRTCalc(4,4,.f.)
        .w_ANGPS = 'N'
        .DoRTCalc(6,11,.f.)
          if not(empty(.w_ANREPARTO))
          .link_1_11('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
          .DoRTCalc(12,19,.f.)
        .w_ANTIPO = 'C'
        .w_CODI = .w_ANCODICE
          .DoRTCalc(22,26,.f.)
        .w_ANOREMAN = 250
        .w_ANTIPO = 'C'
        .w_CODI = .w_ANCODICE
        .DoRTCalc(30,30,.f.)
          if not(empty(.w_ANGOANT))
          .link_2_3('Full')
          endif
        .DoRTCalc(31,32,.f.)
          if not(empty(.w_ANGOPOST))
          .link_2_5('Full')
          endif
          .DoRTCalc(33,37,.f.)
        .w_DESCRI = .w_ANDESCRI
        .w_DESCRI = .w_ANDESCRI
      endif
    endwith
    cp_BlankRecExtFlds(this,'ANAGRAFE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
    cp_AskTableProg(this,i_nConn,"numcar","w_ANCODICE")
    with this
      .op_ANCODICE = .w_ANCODICE
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oANCODICE_1_2.enabled = i_bVal
      .Page1.oPag.oANFLDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oANNUM_1_4.enabled = i_bVal
      .Page1.oPag.oANGPS_1_5.enabled = i_bVal
      .Page1.oPag.oANALIM_1_6.enabled = i_bVal
      .Page1.oPag.oANVOLT_1_7.enabled = i_bVal
      .Page1.oPag.oANMATRI_1_8.enabled = i_bVal
      .Page1.oPag.oANMARCA_1_9.enabled = i_bVal
      .Page1.oPag.oANMODE_1_10.enabled = i_bVal
      .Page1.oPag.oANREPARTO_1_11.enabled = i_bVal
      .Page1.oPag.oANANNO_1_12.enabled = i_bVal
      .Page1.oPag.oANDESCRI_1_15.enabled = i_bVal
      .Page1.oPag.oANDESCRI2_1_26.enabled = i_bVal
      .Page3.oPag.oANORE_3_3.enabled = i_bVal
      .Page3.oPag.oANPRIN_3_4.enabled = i_bVal
      .Page3.oPag.oANULIN_3_5.enabled = i_bVal
      .Page3.oPag.oANEND_3_6.enabled = i_bVal
      .Page3.oPag.oANPRORE_3_11.enabled = i_bVal
      .Page3.oPag.oANOREMAN_3_12.enabled = i_bVal
      .Page2.oPag.oANGOANT_2_3.enabled = i_bVal
      .Page2.oPag.oANNRANT_2_4.enabled = i_bVal
      .Page2.oPag.oANGOPOST_2_5.enabled = i_bVal
      .Page2.oPag.oANNRPOS_2_6.enabled = i_bVal
      .Page1.oPag.oBtn_1_24.enabled = .Page1.oPag.oBtn_1_24.mCond()
      if i_cOp = "Edit"
        .Page1.oPag.oANCODICE_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oANCODICE_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ANAGRAFE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPO,"ANTIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODICE,"ANCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLDESCRI,"ANFLDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANNUM,"ANNUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANGPS,"ANGPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANALIM,"ANALIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANVOLT,"ANVOLT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANMATRI,"ANMATRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANMARCA,"ANMARCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANMODE,"ANMODE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANREPARTO,"ANREPARTO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANANNO,"ANANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESCRI,"ANDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESCRI2,"ANDESCRI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPO,"ANTIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANORE,"ANORE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPRIN,"ANPRIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANULIN,"ANULIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANEND,"ANEND",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPRORE,"ANPRORE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANOREMAN,"ANOREMAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPO,"ANTIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANGOANT,"ANGOANT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANNRANT,"ANNRANT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANGOPOST,"ANGOPOST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANNRPOS,"ANNRPOS",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- anme_geca
    * --- aggiunge alla Chiave ulteriore filtro su Tipo Conto
    IF NOT EMPTY(i_cWhere)
          i_cWhere=i_cWhere+" and ANTIPO='C'"
    ENDIF
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
    i_lTable = "ANAGRAFE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ANAGRAFE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ANAGRAFE_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"numcar","w_ANCODICE")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ANAGRAFE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ANAGRAFE')
        i_extval=cp_InsertValODBCExtFlds(this,'ANAGRAFE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ANTIPO,ANCODICE,ANFLDESCRI,ANNUM,ANGPS"+;
                  ",ANALIM,ANVOLT,ANMATRI,ANMARCA,ANMODE"+;
                  ",ANREPARTO,ANANNO,ANDESCRI,ANDESCRI2,UTCC"+;
                  ",UTCV,UTDC,UTDV,ANORE,ANPRIN"+;
                  ",ANULIN,ANEND,ANPRORE,ANOREMAN,ANGOANT"+;
                  ",ANNRANT,ANGOPOST,ANNRPOS "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ANTIPO)+;
                  ","+cp_ToStrODBC(this.w_ANCODICE)+;
                  ","+cp_ToStrODBC(this.w_ANFLDESCRI)+;
                  ","+cp_ToStrODBC(this.w_ANNUM)+;
                  ","+cp_ToStrODBC(this.w_ANGPS)+;
                  ","+cp_ToStrODBC(this.w_ANALIM)+;
                  ","+cp_ToStrODBC(this.w_ANVOLT)+;
                  ","+cp_ToStrODBC(this.w_ANMATRI)+;
                  ","+cp_ToStrODBC(this.w_ANMARCA)+;
                  ","+cp_ToStrODBC(this.w_ANMODE)+;
                  ","+cp_ToStrODBCNull(this.w_ANREPARTO)+;
                  ","+cp_ToStrODBC(this.w_ANANNO)+;
                  ","+cp_ToStrODBC(this.w_ANDESCRI)+;
                  ","+cp_ToStrODBC(this.w_ANDESCRI2)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_ANORE)+;
                  ","+cp_ToStrODBC(this.w_ANPRIN)+;
                  ","+cp_ToStrODBC(this.w_ANULIN)+;
                  ","+cp_ToStrODBC(this.w_ANEND)+;
                  ","+cp_ToStrODBC(this.w_ANPRORE)+;
                  ","+cp_ToStrODBC(this.w_ANOREMAN)+;
                  ","+cp_ToStrODBCNull(this.w_ANGOANT)+;
                  ","+cp_ToStrODBC(this.w_ANNRANT)+;
                  ","+cp_ToStrODBCNull(this.w_ANGOPOST)+;
                  ","+cp_ToStrODBC(this.w_ANNRPOS)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ANAGRAFE')
        i_extval=cp_InsertValVFPExtFlds(this,'ANAGRAFE')
        cp_CheckDeletedKey(i_cTable,0,'ANTIPO',this.w_ANTIPO,'ANCODICE',this.w_ANCODICE,'ANTIPO',this.w_ANTIPO,'ANTIPO',this.w_ANTIPO)
        INSERT INTO (i_cTable);
              (ANTIPO,ANCODICE,ANFLDESCRI,ANNUM,ANGPS,ANALIM,ANVOLT,ANMATRI,ANMARCA,ANMODE,ANREPARTO,ANANNO,ANDESCRI,ANDESCRI2,UTCC,UTCV,UTDC,UTDV,ANORE,ANPRIN,ANULIN,ANEND,ANPRORE,ANOREMAN,ANGOANT,ANNRANT,ANGOPOST,ANNRPOS  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ANTIPO;
                  ,this.w_ANCODICE;
                  ,this.w_ANFLDESCRI;
                  ,this.w_ANNUM;
                  ,this.w_ANGPS;
                  ,this.w_ANALIM;
                  ,this.w_ANVOLT;
                  ,this.w_ANMATRI;
                  ,this.w_ANMARCA;
                  ,this.w_ANMODE;
                  ,this.w_ANREPARTO;
                  ,this.w_ANANNO;
                  ,this.w_ANDESCRI;
                  ,this.w_ANDESCRI2;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_ANORE;
                  ,this.w_ANPRIN;
                  ,this.w_ANULIN;
                  ,this.w_ANEND;
                  ,this.w_ANPRORE;
                  ,this.w_ANOREMAN;
                  ,this.w_ANGOANT;
                  ,this.w_ANNRANT;
                  ,this.w_ANGOPOST;
                  ,this.w_ANNRPOS;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ANAGRAFE_IDX,i_nConn)
      *
      * update ANAGRAFE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ANAGRAFE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ANFLDESCRI="+cp_ToStrODBC(this.w_ANFLDESCRI)+;
             ",ANNUM="+cp_ToStrODBC(this.w_ANNUM)+;
             ",ANGPS="+cp_ToStrODBC(this.w_ANGPS)+;
             ",ANALIM="+cp_ToStrODBC(this.w_ANALIM)+;
             ",ANVOLT="+cp_ToStrODBC(this.w_ANVOLT)+;
             ",ANMATRI="+cp_ToStrODBC(this.w_ANMATRI)+;
             ",ANMARCA="+cp_ToStrODBC(this.w_ANMARCA)+;
             ",ANMODE="+cp_ToStrODBC(this.w_ANMODE)+;
             ",ANREPARTO="+cp_ToStrODBCNull(this.w_ANREPARTO)+;
             ",ANANNO="+cp_ToStrODBC(this.w_ANANNO)+;
             ",ANDESCRI="+cp_ToStrODBC(this.w_ANDESCRI)+;
             ",ANDESCRI2="+cp_ToStrODBC(this.w_ANDESCRI2)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",ANORE="+cp_ToStrODBC(this.w_ANORE)+;
             ",ANPRIN="+cp_ToStrODBC(this.w_ANPRIN)+;
             ",ANULIN="+cp_ToStrODBC(this.w_ANULIN)+;
             ",ANEND="+cp_ToStrODBC(this.w_ANEND)+;
             ",ANPRORE="+cp_ToStrODBC(this.w_ANPRORE)+;
             ",ANOREMAN="+cp_ToStrODBC(this.w_ANOREMAN)+;
             ",ANGOANT="+cp_ToStrODBCNull(this.w_ANGOANT)+;
             ",ANNRANT="+cp_ToStrODBC(this.w_ANNRANT)+;
             ",ANGOPOST="+cp_ToStrODBCNull(this.w_ANGOPOST)+;
             ",ANNRPOS="+cp_ToStrODBC(this.w_ANNRPOS)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ANAGRAFE')
        i_cWhere = cp_PKFox(i_cTable  ,'ANTIPO',this.w_ANTIPO  ,'ANCODICE',this.w_ANCODICE  ,'ANTIPO',this.w_ANTIPO  ,'ANTIPO',this.w_ANTIPO  )
        UPDATE (i_cTable) SET;
              ANFLDESCRI=this.w_ANFLDESCRI;
             ,ANNUM=this.w_ANNUM;
             ,ANGPS=this.w_ANGPS;
             ,ANALIM=this.w_ANALIM;
             ,ANVOLT=this.w_ANVOLT;
             ,ANMATRI=this.w_ANMATRI;
             ,ANMARCA=this.w_ANMARCA;
             ,ANMODE=this.w_ANMODE;
             ,ANREPARTO=this.w_ANREPARTO;
             ,ANANNO=this.w_ANANNO;
             ,ANDESCRI=this.w_ANDESCRI;
             ,ANDESCRI2=this.w_ANDESCRI2;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,ANORE=this.w_ANORE;
             ,ANPRIN=this.w_ANPRIN;
             ,ANULIN=this.w_ANULIN;
             ,ANEND=this.w_ANEND;
             ,ANPRORE=this.w_ANPRORE;
             ,ANOREMAN=this.w_ANOREMAN;
             ,ANGOANT=this.w_ANGOANT;
             ,ANNRANT=this.w_ANNRANT;
             ,ANGOPOST=this.w_ANGOPOST;
             ,ANNRPOS=this.w_ANNRPOS;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ANAGRAFE_IDX,i_nConn)
      *
      * delete ANAGRAFE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ANTIPO',this.w_ANTIPO  ,'ANCODICE',this.w_ANCODICE  ,'ANTIPO',this.w_ANTIPO  ,'ANTIPO',this.w_ANTIPO  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .DoRTCalc(1,20,.t.)
            .w_CODI = .w_ANCODICE
        .DoRTCalc(22,28,.t.)
            .w_CODI = .w_ANCODICE
        .DoRTCalc(30,37,.t.)
            .w_DESCRI = .w_ANDESCRI
            .w_DESCRI = .w_ANDESCRI
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oANVOLT_1_7.enabled = this.oPgFrm.Page1.oPag.oANVOLT_1_7.mCond()
    this.oPgFrm.Page1.oPag.oANDESCRI_1_15.enabled = this.oPgFrm.Page1.oPag.oANDESCRI_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_24.visible=!this.oPgFrm.Page1.oPag.oBtn_1_24.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ANREPARTO
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REPARTI_IDX,3]
    i_lTable = "REPARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REPARTI_IDX,2], .t., this.REPARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REPARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANREPARTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('REPA_MRE',True,'REPARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RECODICE like "+cp_ToStrODBC(trim(this.w_ANREPARTO)+"%");

          i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RECODICE',trim(this.w_ANREPARTO))
          select RECODICE,REDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANREPARTO)==trim(_Link_.RECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANREPARTO) and !this.bDontReportError
            deferred_cp_zoom('REPARTI','*','RECODICE',cp_AbsName(oSource.parent,'oANREPARTO_1_11'),i_cWhere,'REPA_MRE',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODICE',oSource.xKey(1))
            select RECODICE,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANREPARTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RECODICE="+cp_ToStrODBC(this.w_ANREPARTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODICE',this.w_ANREPARTO)
            select RECODICE,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANREPARTO = NVL(_Link_.RECODICE,space(5))
      this.w_DESREP = NVL(_Link_.REDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ANREPARTO = space(5)
      endif
      this.w_DESREP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REPARTI_IDX,2])+'\'+cp_ToStr(_Link_.RECODICE,1)
      cp_ShowWarn(i_cKey,this.REPARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANREPARTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.REPARTI_IDX,3] and i_nFlds+2<200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.REPARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.RECODICE as RECODICE111"+ ",link_1_11.REDESCRI as REDESCRI111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on ANAGRAFE.ANREPARTO=link_1_11.RECODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and ANAGRAFE.ANREPARTO=link_1_11.RECODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANGOANT
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.Gomme_IDX,3]
    i_lTable = "Gomme"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.Gomme_IDX,2], .t., this.Gomme_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.Gomme_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANGOANT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('ANAG_MAG',True,'Gomme')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SERIALANT like "+cp_ToStrODBC(trim(this.w_ANGOANT)+"%");

          i_ret=cp_SQL(i_nConn,"select SERIALANT,GOMISURE,GOFORI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SERIALANT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SERIALANT',trim(this.w_ANGOANT))
          select SERIALANT,GOMISURE,GOFORI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SERIALANT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANGOANT)==trim(_Link_.SERIALANT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANGOANT) and !this.bDontReportError
            deferred_cp_zoom('Gomme','*','SERIALANT',cp_AbsName(oSource.parent,'oANGOANT_2_3'),i_cWhere,'ANAG_MAG',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SERIALANT,GOMISURE,GOFORI";
                     +" from "+i_cTable+" "+i_lTable+" where SERIALANT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SERIALANT',oSource.xKey(1))
            select SERIALANT,GOMISURE,GOFORI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANGOANT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SERIALANT,GOMISURE,GOFORI";
                   +" from "+i_cTable+" "+i_lTable+" where SERIALANT="+cp_ToStrODBC(this.w_ANGOANT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SERIALANT',this.w_ANGOANT)
            select SERIALANT,GOMISURE,GOFORI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANGOANT = NVL(_Link_.SERIALANT,space(10))
      this.w_ANTDESCRI = NVL(_Link_.GOMISURE,space(10))
      this.w_ANTFORI = NVL(_Link_.GOFORI,0)
    else
      if i_cCtrl<>'Load'
        this.w_ANGOANT = space(10)
      endif
      this.w_ANTDESCRI = space(10)
      this.w_ANTFORI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.Gomme_IDX,2])+'\'+cp_ToStr(_Link_.SERIALANT,1)
      cp_ShowWarn(i_cKey,this.Gomme_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANGOANT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.Gomme_IDX,3] and i_nFlds+3<200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.Gomme_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.SERIALANT as SERIALANT203"+ ",link_2_3.GOMISURE as GOMISURE203"+ ",link_2_3.GOFORI as GOFORI203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on ANAGRAFE.ANGOANT=link_2_3.SERIALANT"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and ANAGRAFE.ANGOANT=link_2_3.SERIALANT(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANGOPOST
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.Gomme_IDX,3]
    i_lTable = "Gomme"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.Gomme_IDX,2], .t., this.Gomme_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.Gomme_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANGOPOST) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('ANAG_MAG',True,'Gomme')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SERIALPOST like "+cp_ToStrODBC(trim(this.w_ANGOPOST)+"%");

          i_ret=cp_SQL(i_nConn,"select SERIALPOST,GOMISURE,GOFORI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SERIALPOST","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SERIALPOST',trim(this.w_ANGOPOST))
          select SERIALPOST,GOMISURE,GOFORI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SERIALPOST into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANGOPOST)==trim(_Link_.SERIALPOST) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANGOPOST) and !this.bDontReportError
            deferred_cp_zoom('Gomme','*','SERIALPOST',cp_AbsName(oSource.parent,'oANGOPOST_2_5'),i_cWhere,'ANAG_MAG',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SERIALPOST,GOMISURE,GOFORI";
                     +" from "+i_cTable+" "+i_lTable+" where SERIALPOST="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SERIALPOST',oSource.xKey(1))
            select SERIALPOST,GOMISURE,GOFORI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANGOPOST)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SERIALPOST,GOMISURE,GOFORI";
                   +" from "+i_cTable+" "+i_lTable+" where SERIALPOST="+cp_ToStrODBC(this.w_ANGOPOST);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SERIALPOST',this.w_ANGOPOST)
            select SERIALPOST,GOMISURE,GOFORI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANGOPOST = NVL(_Link_.SERIALPOST,space(10))
      this.w_POSDESCRI = NVL(_Link_.GOMISURE,space(10))
      this.w_POSFORI = NVL(_Link_.GOFORI,0)
    else
      if i_cCtrl<>'Load'
        this.w_ANGOPOST = space(10)
      endif
      this.w_POSDESCRI = space(10)
      this.w_POSFORI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.Gomme_IDX,2])+'\'+cp_ToStr(_Link_.SERIALPOST,1)
      cp_ShowWarn(i_cKey,this.Gomme_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANGOPOST Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANCODICE_1_2.value==this.w_ANCODICE)
      this.oPgFrm.Page1.oPag.oANCODICE_1_2.value=this.w_ANCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oANFLDESCRI_1_3.RadioValue()==this.w_ANFLDESCRI)
      this.oPgFrm.Page1.oPag.oANFLDESCRI_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANNUM_1_4.value==this.w_ANNUM)
      this.oPgFrm.Page1.oPag.oANNUM_1_4.value=this.w_ANNUM
    endif
    if not(this.oPgFrm.Page1.oPag.oANGPS_1_5.RadioValue()==this.w_ANGPS)
      this.oPgFrm.Page1.oPag.oANGPS_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANALIM_1_6.RadioValue()==this.w_ANALIM)
      this.oPgFrm.Page1.oPag.oANALIM_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANVOLT_1_7.value==this.w_ANVOLT)
      this.oPgFrm.Page1.oPag.oANVOLT_1_7.value=this.w_ANVOLT
    endif
    if not(this.oPgFrm.Page1.oPag.oANMATRI_1_8.value==this.w_ANMATRI)
      this.oPgFrm.Page1.oPag.oANMATRI_1_8.value=this.w_ANMATRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANMARCA_1_9.value==this.w_ANMARCA)
      this.oPgFrm.Page1.oPag.oANMARCA_1_9.value=this.w_ANMARCA
    endif
    if not(this.oPgFrm.Page1.oPag.oANMODE_1_10.value==this.w_ANMODE)
      this.oPgFrm.Page1.oPag.oANMODE_1_10.value=this.w_ANMODE
    endif
    if not(this.oPgFrm.Page1.oPag.oANREPARTO_1_11.value==this.w_ANREPARTO)
      this.oPgFrm.Page1.oPag.oANREPARTO_1_11.value=this.w_ANREPARTO
    endif
    if not(this.oPgFrm.Page1.oPag.oANANNO_1_12.value==this.w_ANANNO)
      this.oPgFrm.Page1.oPag.oANANNO_1_12.value=this.w_ANANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_15.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_15.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI2_1_26.value==this.w_ANDESCRI2)
      this.oPgFrm.Page1.oPag.oANDESCRI2_1_26.value=this.w_ANDESCRI2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESREP_1_27.value==this.w_DESREP)
      this.oPgFrm.Page1.oPag.oDESREP_1_27.value=this.w_DESREP
    endif
    if not(this.oPgFrm.Page3.oPag.oCODI_3_2.value==this.w_CODI)
      this.oPgFrm.Page3.oPag.oCODI_3_2.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page3.oPag.oANORE_3_3.value==this.w_ANORE)
      this.oPgFrm.Page3.oPag.oANORE_3_3.value=this.w_ANORE
    endif
    if not(this.oPgFrm.Page3.oPag.oANPRIN_3_4.value==this.w_ANPRIN)
      this.oPgFrm.Page3.oPag.oANPRIN_3_4.value=this.w_ANPRIN
    endif
    if not(this.oPgFrm.Page3.oPag.oANULIN_3_5.value==this.w_ANULIN)
      this.oPgFrm.Page3.oPag.oANULIN_3_5.value=this.w_ANULIN
    endif
    if not(this.oPgFrm.Page3.oPag.oANEND_3_6.value==this.w_ANEND)
      this.oPgFrm.Page3.oPag.oANEND_3_6.value=this.w_ANEND
    endif
    if not(this.oPgFrm.Page3.oPag.oANPRORE_3_11.value==this.w_ANPRORE)
      this.oPgFrm.Page3.oPag.oANPRORE_3_11.value=this.w_ANPRORE
    endif
    if not(this.oPgFrm.Page3.oPag.oANOREMAN_3_12.value==this.w_ANOREMAN)
      this.oPgFrm.Page3.oPag.oANOREMAN_3_12.value=this.w_ANOREMAN
    endif
    if not(this.oPgFrm.Page2.oPag.oCODI_2_2.value==this.w_CODI)
      this.oPgFrm.Page2.oPag.oCODI_2_2.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page2.oPag.oANGOANT_2_3.value==this.w_ANGOANT)
      this.oPgFrm.Page2.oPag.oANGOANT_2_3.value=this.w_ANGOANT
    endif
    if not(this.oPgFrm.Page2.oPag.oANNRANT_2_4.value==this.w_ANNRANT)
      this.oPgFrm.Page2.oPag.oANNRANT_2_4.value=this.w_ANNRANT
    endif
    if not(this.oPgFrm.Page2.oPag.oANGOPOST_2_5.value==this.w_ANGOPOST)
      this.oPgFrm.Page2.oPag.oANGOPOST_2_5.value=this.w_ANGOPOST
    endif
    if not(this.oPgFrm.Page2.oPag.oANNRPOS_2_6.value==this.w_ANNRPOS)
      this.oPgFrm.Page2.oPag.oANNRPOS_2_6.value=this.w_ANNRPOS
    endif
    if not(this.oPgFrm.Page2.oPag.oANTDESCRI_2_7.value==this.w_ANTDESCRI)
      this.oPgFrm.Page2.oPag.oANTDESCRI_2_7.value=this.w_ANTDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oPOSDESCRI_2_8.value==this.w_POSDESCRI)
      this.oPgFrm.Page2.oPag.oPOSDESCRI_2_8.value=this.w_POSDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oANTFORI_2_15.value==this.w_ANTFORI)
      this.oPgFrm.Page2.oPag.oANTFORI_2_15.value=this.w_ANTFORI
    endif
    if not(this.oPgFrm.Page2.oPag.oPOSFORI_2_16.value==this.w_POSFORI)
      this.oPgFrm.Page2.oPag.oPOSFORI_2_16.value=this.w_POSFORI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCRI_2_17.value==this.w_DESCRI)
      this.oPgFrm.Page2.oPag.oDESCRI_2_17.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCRI_3_14.value==this.w_DESCRI)
      this.oPgFrm.Page3.oPag.oDESCRI_3_14.value=this.w_DESCRI
    endif
    cp_SetControlsValueExtFlds(this,'ANAGRAFE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- anme_geca
      IF this.cFunction='Load'
          this.w_UTCC=i_codute
          this.w_UTDC=datetime()
      ENDIF
      IF this.cFunction='Edit'
          this.w_UTCV=i_codute
          this.w_UTDV=datetime()
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tanme_gecaPag1 as StdContainer
  Width  = 629
  height = 256
  stdWidth  = 629
  stdheight = 256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANCODICE_1_2 as StdField with uid="SAPJOGSTDD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ANCODICE", cQueryName = "ANTIPO,ANCODICE", tabstop=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 84799525,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=109, Top=9, InputMask=replicate('X',10)

  add object oANFLDESCRI_1_3 as StdCheck with uid="MSDYFYHRWB",rtseq=3,rtrep=.f.,left=585, top=82, caption="",;
    HelpContextID = 82170389,;
    cFormVar="w_ANFLDESCRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANFLDESCRI_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLDESCRI_1_3.GetRadio()
    this.Parent.oContained.w_ANFLDESCRI = this.RadioValue()
    return .t.
  endfunc

  func oANFLDESCRI_1_3.SetRadio()
    this.Parent.oContained.w_ANFLDESCRI=trim(this.Parent.oContained.w_ANFLDESCRI)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLDESCRI=='S',1,;
      0)
  endfunc

  add object oANNUM_1_4 as StdField with uid="UGORGXVYIA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ANNUM", cQueryName = "ANNUM",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25629806,;
   bGlobalFont=.t.,;
    Height=21, Width=66, Left=109, Top=85, cSayPict='"99999"', cGetPict='"99999"'

  add object oANGPS_1_5 as StdCheck with uid="HSNURGBAGY",rtseq=5,rtrep=.f.,left=585, top=111, caption="",;
    HelpContextID = 199103598,;
    cFormVar="w_ANGPS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANGPS_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oANGPS_1_5.GetRadio()
    this.Parent.oContained.w_ANGPS = this.RadioValue()
    return .t.
  endfunc

  func oANGPS_1_5.SetRadio()
    this.Parent.oContained.w_ANGPS=trim(this.Parent.oContained.w_ANGPS)
    this.value = ;
      iif(this.Parent.oContained.w_ANGPS=='S',1,;
      0)
  endfunc


  add object oANALIM_1_6 as StdCombo with uid="XHWWWKHHIM",rtseq=6,rtrep=.f.,left=109,top=112,width=114,height=22;
    , HelpContextID = 103027822;
    , cFormVar="w_ANALIM",RowSource=""+"Diesel,"+"Elettrico,"+"Benzina", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oANALIM_1_6.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'E',;
    iif(this.value =3,'B',;
    space(1)))))
  endfunc
  func oANALIM_1_6.GetRadio()
    this.Parent.oContained.w_ANALIM = this.RadioValue()
    return .t.
  endfunc

  func oANALIM_1_6.SetRadio()
    this.Parent.oContained.w_ANALIM=trim(this.Parent.oContained.w_ANALIM)
    this.value = ;
      iif(this.Parent.oContained.w_ANALIM=='D',1,;
      iif(this.Parent.oContained.w_ANALIM=='E',2,;
      iif(this.Parent.oContained.w_ANALIM=='B',3,;
      0)))
  endfunc

  add object oANVOLT_1_7 as StdField with uid="QLMRHZADZZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ANVOLT", cQueryName = "ANVOLT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 220261266,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=373, Top=112

  func oANVOLT_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANALIM='E')
    endwith
   endif
  endfunc

  add object oANMATRI_1_8 as StdField with uid="ISBKGLAXFE",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ANMATRI", cQueryName = "ANMATRI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 70773723,;
   bGlobalFont=.t.,;
    Height=21, Width=159, Left=109, Top=140, cSayPict='"!!!!!!!!!!!!!!!"', cGetPict='"!!!!!!!!!!!!!!!"', InputMask=replicate('X',15)

  add object oANMARCA_1_9 as StdField with uid="PNPSPZJUXA",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ANMARCA", cQueryName = "ANMARCA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 37219283,;
   bGlobalFont=.t.,;
    Height=21, Width=158, Left=109, Top=168, cSayPict='"!!!!!!!!!!!!!!!"', cGetPict='"!!!!!!!!!!!!!!!"', InputMask=replicate('X',15)

  add object oANMODE_1_10 as StdField with uid="QBBTKQUDDQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ANMODE", cQueryName = "ANMODE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 85453714,;
   bGlobalFont=.t.,;
    Height=21, Width=151, Left=329, Top=168, cSayPict='"!!!!!!!!!!!!!!!"', cGetPict='"!!!!!!!!!!!!!!!"', InputMask=replicate('X',15)

  add object oANREPARTO_1_11 as StdField with uid="QMWZEJDTXT",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ANREPARTO", cQueryName = "ANREPARTO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 8208420,;
   bGlobalFont=.t.,;
    Height=21, Width=67, Left=109, Top=196, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="REPARTI", cZoomOnZoom="REPA_MRE", oKey_1_1="RECODICE", oKey_1_2="this.w_ANREPARTO"

  func oANREPARTO_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oANREPARTO_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANREPARTO_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REPARTI','*','RECODICE',cp_AbsName(this.parent,'oANREPARTO_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'REPA_MRE',"",'',this.parent.oContained
  endproc
  proc oANREPARTO_1_11.mZoomOnZoom
    local i_obj
    i_obj=REPA_MRE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RECODICE=this.parent.oContained.w_ANREPARTO
     i_obj.ecpSave()
  endproc

  add object oANANNO_1_12 as StdField with uid="ITWFBZTYWL",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ANANNO", cQueryName = "ANANNO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 17044590,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=109, Top=224, cSayPict='"9999"', cGetPict='"9999"'

  add object oANDESCRI_1_15 as StdField with uid="RLIEXIOBZO",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "La descrizione si aggiorna in base ai dati inseriti",;
    HelpContextID = 57602164,;
   bGlobalFont=.t.,;
    Height=21, Width=505, Left=109, Top=35, InputMask=replicate('X',100)

  func oANDESCRI_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLDESCRI='S')
    endwith
   endif
  endfunc


  add object oObj_1_22 as cp_runprogram with uid="LAMYOYYRNL",left=49, top=287, width=50,height=50,;
    caption='Object',;
    prg="ANGE_BGD('A')",;
    cEvent = "w_ANNUM Changed, w_ANMATRI Changed, w_ANMARCA Changed,  w_ANMODE Changed,Record Inserted,Record Updated,Done",;
    nPag=1;
    , HelpContextID = 93731218;
  , bGlobalFont=.t.



  add object oBtn_1_24 as StdButton with uid="EDNTDBOWYR",left=526, top=221, width=88,height=25,;
    caption="Interventi", nPag=1,tabstop=.f.;
    , HelpContextID = 78118615;
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      do mcli_kli with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not.empty(.w_ANDESCRI) )
      endwith
    endif
  endfunc

  func oBtn_1_24.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction ='Load')
     endwith
    endif
  endfunc

  add object oANDESCRI2_1_26 as StdField with uid="RFQPIMIUYG",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ANDESCRI2", cQueryName = "ANDESCRI2",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 57614964,;
   bGlobalFont=.t.,;
    Height=21, Width=257, Left=109, Top=59, cSayPict='"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"', cGetPict='"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"', InputMask=replicate('X',40)

  add object oDESREP_1_27 as StdField with uid="CIYDTJSDBW",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESREP", cQueryName = "DESREP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 162701678,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=176, Top=196, InputMask=replicate('X',40)

  add object oStr_1_13 as StdString with uid="SKJECVYYLJ",Visible=.t., Left=50, Top=144,;
    Alignment=1, Width=52, Height=18,;
    Caption="Matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="OTTKMEYXBZ",Visible=.t., Left=66, Top=172,;
    Alignment=1, Width=36, Height=18,;
    Caption="Marca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="BXRVEJXHMH",Visible=.t., Left=34, Top=39,;
    Alignment=1, Width=68, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="EMYEQOEOHF",Visible=.t., Left=54, Top=88,;
    Alignment=1, Width=48, Height=18,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="IHDYHWVHEG",Visible=.t., Left=6, Top=228,;
    Alignment=1, Width=96, Height=18,;
    Caption="Anno Produzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="XRVWVPBUQZ",Visible=.t., Left=268, Top=116,;
    Alignment=1, Width=98, Height=18,;
    Caption="Voltaggio Batterie:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="LWCXUHKHJK",Visible=.t., Left=22, Top=116,;
    Alignment=1, Width=80, Height=18,;
    Caption="Alimentazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="EDZWKIPRTF",Visible=.t., Left=279, Top=172,;
    Alignment=1, Width=46, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="LZHIXVMZLP",Visible=.t., Left=461, Top=88,;
    Alignment=1, Width=118, Height=18,;
    Caption="Descrizione Manuale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="VRXKSCYGZJ",Visible=.t., Left=51, Top=200,;
    Alignment=1, Width=51, Height=18,;
    Caption="Reparto :"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="GMARRFTITZ",Visible=.t., Left=551, Top=115,;
    Alignment=1, Width=28, Height=18,;
    Caption="GPS:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="MJRAZYDQRF",Visible=.t., Left=20, Top=63,;
    Alignment=1, Width=82, Height=18,;
    Caption="Dati Aggiuntivi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="EXIWSPYGTD",Visible=.t., Left=60, Top=13,;
    Alignment=1, Width=42, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.
enddefine
define class tanme_gecaPag2 as StdContainer
  Width  = 629
  height = 256
  stdWidth  = 629
  stdheight = 256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_2_2 as StdField with uid="TEJOCBQLQB",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 256967278,;
   bGlobalFont=.t.,;
    Height=21, Width=119, Left=91, Top=15, InputMask=replicate('X',10)

  add object oANGOANT_2_3 as StdField with uid="ZSVJPSFDCS",rtseq=30,rtrep=.f.,;
    cFormVar = "w_ANGOANT", cQueryName = "ANGOANT",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 233706522,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=91, Top=64, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="Gomme", cZoomOnZoom="ANAG_MAG", oKey_1_1="SERIALANT", oKey_1_2="this.w_ANGOANT"

  func oANGOANT_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oANGOANT_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANGOANT_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'Gomme','*','SERIALANT',cp_AbsName(this.parent,'oANGOANT_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'ANAG_MAG',"",'',this.parent.oContained
  endproc
  proc oANGOANT_2_3.mZoomOnZoom
    local i_obj
    i_obj=ANAG_MAG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SERIALANT=this.parent.oContained.w_ANGOANT
     i_obj.ecpSave()
  endproc

  add object oANNRANT_2_4 as StdField with uid="SPBUHTEDKY",rtseq=31,rtrep=.f.,;
    cFormVar = "w_ANNRANT", cQueryName = "ANNRANT",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 230102042,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=523, Top=64

  add object oANGOPOST_2_5 as StdField with uid="AQAZJVIIBY",rtseq=32,rtrep=.f.,;
    cFormVar = "w_ANGOPOST", cQueryName = "ANGOPOST",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 250482395,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=91, Top=94, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="Gomme", cZoomOnZoom="ANAG_MAG", oKey_1_1="SERIALPOST", oKey_1_2="this.w_ANGOPOST"

  func oANGOPOST_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oANGOPOST_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANGOPOST_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'Gomme','*','SERIALPOST',cp_AbsName(this.parent,'oANGOPOST_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'ANAG_MAG',"",'',this.parent.oContained
  endproc
  proc oANGOPOST_2_5.mZoomOnZoom
    local i_obj
    i_obj=ANAG_MAG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SERIALPOST=this.parent.oContained.w_ANGOPOST
     i_obj.ecpSave()
  endproc

  add object oANNRPOS_2_6 as StdField with uid="LJCQYHAULT",rtseq=33,rtrep=.f.,;
    cFormVar = "w_ANNRPOS", cQueryName = "ANNRPOS",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 246879259,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=523, Top=94

  add object oANTDESCRI_2_7 as StdField with uid="COXAGJOVXU",rtseq=34,rtrep=.f.,;
    cFormVar = "w_ANTDESCRI", cQueryName = "ANTDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 91175413,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=215, Top=64, InputMask=replicate('X',10)

  add object oPOSDESCRI_2_8 as StdField with uid="WUOOHFPZHT",rtseq=35,rtrep=.f.,;
    cFormVar = "w_POSDESCRI", cQueryName = "POSDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 91117813,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=215, Top=94, InputMask=replicate('X',10)

  add object oANTFORI_2_15 as StdField with uid="ULKJIHBVFY",rtseq=36,rtrep=.f.,;
    cFormVar = "w_ANTFORI", cQueryName = "ANTFORI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 261024731,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=396, Top=64

  add object oPOSFORI_2_16 as StdField with uid="WGRQIMDUMM",rtseq=37,rtrep=.f.,;
    cFormVar = "w_POSFORI", cQueryName = "POSFORI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 260967131,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=396, Top=94

  add object oDESCRI_2_17 as StdField with uid="YNSTUNSBYE",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 228761966,;
   bGlobalFont=.t.,;
    Height=21, Width=379, Left=209, Top=15, InputMask=replicate('X',40)

  add object oStr_2_9 as StdString with uid="RKOWHNYWLR",Visible=.t., Left=30, Top=68,;
    Alignment=1, Width=60, Height=18,;
    Caption="Anteriori:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="FHEMOZKAOQ",Visible=.t., Left=22, Top=98,;
    Alignment=1, Width=69, Height=18,;
    Caption="Posteriori:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="AELBDXIUGF",Visible=.t., Left=494, Top=68,;
    Alignment=1, Width=19, Height=18,;
    Caption="Qt�"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="ZJAZKDAYHV",Visible=.t., Left=494, Top=98,;
    Alignment=1, Width=19, Height=18,;
    Caption="Qt�"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="HERTXRSHQY",Visible=.t., Left=15, Top=40,;
    Alignment=1, Width=60, Height=18,;
    Caption="Gomme"  ;
  , bGlobalFont=.t.

  add object oBox_2_13 as StdBox with uid="ESGYCDOKUW",left=15, top=58, width=573,height=74
enddefine
define class tanme_gecaPag3 as StdContainer
  Width  = 629
  height = 256
  stdWidth  = 629
  stdheight = 256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_3_2 as StdField with uid="OHFWTJYVKI",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 256967278,;
   bGlobalFont=.t.,;
    Height=21, Width=119, Left=84, Top=18, InputMask=replicate('X',10)

  add object oANORE_3_3 as StdField with uid="SOJVSVDHJD",rtseq=22,rtrep=.f.,;
    cFormVar = "w_ANORE", cQueryName = "ANORE",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 162927726,;
   bGlobalFont=.t.,;
    Height=21, Width=99, Left=218, Top=58

  add object oANPRIN_3_4 as StdField with uid="QRJKEBINQL",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ANPRIN", cQueryName = "ANPRIN",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 95753326,;
   bGlobalFont=.t.,;
    Height=21, Width=99, Left=218, Top=86

  add object oANULIN_3_5 as StdField with uid="GRTXDFQHDT",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ANULIN", cQueryName = "ANULIN",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 101717102,;
   bGlobalFont=.t.,;
    Height=21, Width=99, Left=218, Top=114

  add object oANEND_3_6 as StdField with uid="GJZDNXKKJK",rtseq=25,rtrep=.f.,;
    cFormVar = "w_ANEND", cQueryName = "ANEND",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 184554606,;
   bGlobalFont=.t.,;
    Height=21, Width=116, Left=488, Top=184

  add object oANPRORE_3_11 as StdField with uid="WHDAJPBMQL",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ANPRORE", cQueryName = "ANPRORE",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 4910039,;
   bGlobalFont=.t.,;
    Height=21, Width=88, Left=322, Top=87

  add object oANOREMAN_3_12 as StdField with uid="DPRTWEXOIZ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ANOREMAN", cQueryName = "ANOREMAN",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 162926413,;
   bGlobalFont=.t.,;
    Height=21, Width=99, Left=218, Top=142

  add object oDESCRI_3_14 as StdField with uid="WXDUHWZAMR",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 228761966,;
   bGlobalFont=.t.,;
    Height=21, Width=379, Left=202, Top=18, InputMask=replicate('X',40)

  add object oStr_3_7 as StdString with uid="MRYMZZSDOD",Visible=.t., Left=31, Top=90,;
    Alignment=1, Width=187, Height=18,;
    Caption="Data/Ore Intervento Programmato:"  ;
  , bGlobalFont=.t.

  add object oStr_3_8 as StdString with uid="WSSEIZSCAO",Visible=.t., Left=94, Top=118,;
    Alignment=1, Width=124, Height=18,;
    Caption="Data Ultimo Intervento:"  ;
  , bGlobalFont=.t.

  add object oStr_3_9 as StdString with uid="VOVCQPOIUF",Visible=.t., Left=390, Top=188,;
    Alignment=1, Width=98, Height=18,;
    Caption="Data Cessazione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_10 as StdString with uid="ZMFTMUOHEO",Visible=.t., Left=145, Top=62,;
    Alignment=1, Width=73, Height=18,;
    Caption="Ore Lavorate:"  ;
  , bGlobalFont=.t.

  add object oStr_3_13 as StdString with uid="XWFGEBXOCX",Visible=.t., Left=138, Top=146,;
    Alignment=1, Width=80, Height=18,;
    Caption="Ore Tagliando:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('anme_geca','ANAGRAFE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ANTIPO=ANAGRAFE.ANTIPO";
  +" and "+i_cAliasName2+".ANCODICE=ANAGRAFE.ANCODICE";
  +" and "+i_cAliasName2+".ANTIPO=ANAGRAFE.ANTIPO";
  +" and "+i_cAliasName2+".ANTIPO=ANAGRAFE.ANTIPO";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
