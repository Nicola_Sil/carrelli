* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: ange_bgd                                                        *
*              Gestione Anagrafica                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-07-29                                                      *
* Last revis.: 2015-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAram
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tange_bgd",oParentObject,m.pAram)
return(i_retval)

define class tange_bgd as StdBatch
  * --- Local variables
  pAram = space(1)
  DESCRI2 = space(40)
  MATRI = space(15)
  MARCA = space(15)
  MODEL = space(15)
  NUM = space(5)
  VOLT = space(3)
  w_PROG = .NULL.
  w_MESS = space(10)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pAram="A"
        this.DESCRI2 = iif(empty(LRTrim(this.oParentObject.w_ANDESCRI2)),"",LRTrim(this.oParentObject.w_ANDESCRI2))
        this.MATRI = iif(empty(LRTrim(this.oParentObject.w_ANMATRI)),""," Matr.N:� "+LRTrim(this.oParentObject.w_ANMATRI))
        this.MARCA = iif(empty(LRTrim(this.oParentObject.w_ANMARCA)),"",LRTrim(this.oParentObject.w_ANMARCA))
        this.MODEL = iif(empty(LRTrim(this.oParentObject.w_ANMODE)),"",LRTrim(this.oParentObject.w_ANMODE))
        this.VOLT = iif(this.oParentObject.w_ANVOLT=0,"",LRTrim(Str(this.oParentObject.w_ANVOLT))+"V ")
        if this.oParentObject.w_ANFLDESCRI="N"
          do case
            case this.oParentObject.w_ANTIPO="C"
              this.NUM = iif(this.oParentObject.w_ANNUM=0,"","Carrello N.� "+LRTrim(Str(this.oParentObject.w_ANNUM)))
              this.oParentObject.w_ANDESCRI = this.NUM+" "+this.MARCA+" "+this.MODEL+this.MATRI
            case this.oParentObject.w_ANTIPO="B"
              this.NUM = iif(this.oParentObject.w_ANNUM=0,"","Batteria N.� "+LRTrim(Str(this.oParentObject.w_ANNUM))+this.DESCRI2+" - ")
              if this.oParentObject.w_ANNUM=0
                this.oParentObject.w_ANDESCRI = this.MODEL+" - "+this.VOLT+this.MARCA+this.MATRI
              else
                this.oParentObject.w_ANDESCRI = this.NUM+this.VOLT+this.MARCA+this.MODEL+this.MATRI
              endif
            case this.oParentObject.w_ANTIPO="M"
              this.NUM = iif(this.oParentObject.w_ANNUM=0,""," N.� "+LRTrim(Str(this.oParentObject.w_ANNUM)))
              this.oParentObject.w_ANDESCRI = this.DESCRI2+" "+this.NUM+" "+this.MARCA+" "+this.MODEL+this.MATRI
          endcase
        endif
      case this.pAram="B"
        ND=THIS.OPARENTOBJECT.w_DETTAGLIO.CCURSOR
        SELECT (ND)
        do case
          case this.oParentObject.w_ANTIPO="B"
            pSerial=MBSERIALE
          case this.oParentObject.w_ANTIPO="C"
            pSerial=MCSERIALE
          case this.oParentObject.w_ANTIPO="M"
            pSerial=MMSERIALE
        endcase
        if EMPTY(pSerial) 
          do CP_ERRORMSG WITH "Selezionare una Riga!",,"."
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
        do case
          case this.oParentObject.w_ANTIPO="B"
            this.w_PROG = MBGB_MGB()
          case this.oParentObject.w_ANTIPO="C"
            this.w_PROG = MCGC_MGC()
          case this.oParentObject.w_ANTIPO="M"
            this.w_PROG = MMGM_MGM()
        endcase
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.ecpFilter()     
        do case
          case this.oParentObject.w_ANTIPO="B"
            this.w_PROG.w_MBSERIALE = pSERIAL
          case this.oParentObject.w_ANTIPO="C"
            this.w_PROG.w_MCSERIALE = pSERIAL
          case this.oParentObject.w_ANTIPO="M"
            this.w_PROG.w_MMSERIALE = pSERIAL
        endcase
        this.w_PROG.ecpSave()     
      case this.pAram="C"
    endcase
  endproc


  proc Init(oParentObject,pAram)
    this.pAram=pAram
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAram"
endproc
