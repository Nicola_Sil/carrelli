* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: tigc_mti                                                        *
*              Interventi                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-07-30                                                      *
* Last revis.: 2015-08-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("ttigc_mti"))

* --- Class definition
define class ttigc_mti as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 461
  Height = 170+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-08-25"
  HelpContextID=188063236
  max_rt_seq=7

  * --- Constant Properties
  INTERVENTI_IDX = 0
  TIPOINTERVENTO_IDX = 0
  MAND_CAR_IDX = 0
  cFile = "INTERVENTI"
  cKeySelect = "TICODICE"
  cKeyWhere  = "TICODICE=this.w_TICODICE"
  cKeyWhereODBC = '"TICODICE="+cp_ToStrODBC(this.w_TICODICE)';

  cKeyWhereODBCqualified = '"INTERVENTI.TICODICE="+cp_ToStrODBC(this.w_TICODICE)';

  cPrg = "tigc_mti"
  cComment = "Interventi"
  icon = "anag.ico"
  cAutoZoom = 'INTERV'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TICODICE = space(10)
  w_TIMEZZO = space(1)
  w_TIDESCRI = space(40)
  w_TITIPO = space(1)
  w_TIMOTORE = space(1)
  o_TIMOTORE = space(1)
  w_DESCRI = space(40)
  w_CHKT = space(3)

  * --- Autonumbered Variables
  op_TICODICE = this.W_TICODICE
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'INTERVENTI','tigc_mti')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","ttigc_mtiPag1","tigc_mti",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Archivo Interventi")
      .Pages(1).HelpContextID = 96561105
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTICODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='TIPOINTERVENTO'
    this.cWorkTables[2]='MAND_CAR'
    this.cWorkTables[3]='INTERVENTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INTERVENTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INTERVENTI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TICODICE = NVL(TICODICE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from INTERVENTI where TICODICE=KeySet.TICODICE
    *
    i_nConn = i_TableProp[this.INTERVENTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INTERVENTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INTERVENTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' INTERVENTI '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TICODICE',this.w_TICODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESCRI = space(40)
        .w_TICODICE = NVL(TICODICE,space(10))
        .op_TICODICE = .w_TICODICE
        .w_TIMEZZO = NVL(TIMEZZO,space(1))
        .w_TIDESCRI = NVL(TIDESCRI,space(40))
        .w_TITIPO = NVL(TITIPO,space(1))
          if link_1_4_joined
            this.w_TITIPO = NVL(MICODICE104,NVL(this.w_TITIPO,space(1)))
            this.w_DESCRI = NVL(MIDESCRI104,space(40))
          else
          .link_1_4('Load')
          endif
        .w_TIMOTORE = NVL(TIMOTORE,space(1))
        .w_CHKT = NVL(CHKT,space(3))
        cp_LoadRecExtFlds(this,'INTERVENTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TICODICE = space(10)
      .w_TIMEZZO = space(1)
      .w_TIDESCRI = space(40)
      .w_TITIPO = space(1)
      .w_TIMOTORE = space(1)
      .w_DESCRI = space(40)
      .w_CHKT = space(3)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
          if not(empty(.w_TITIPO))
          .link_1_4('Full')
          endif
          .DoRTCalc(5,6,.f.)
        .w_CHKT = iif(.w_TIMOTORE='T','DET',.w_TIMOTORE+'T')
      endif
    endwith
    cp_BlankRecExtFlds(this,'INTERVENTI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INTERVENTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2])
    cp_AskTableProg(this,i_nConn,"interv","w_TICODICE")
    with this
      .op_TICODICE = .w_TICODICE
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTICODICE_1_1.enabled = !i_bVal
      .Page1.oPag.oTIMEZZO_1_2.enabled = i_bVal
      .Page1.oPag.oTIDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oTITIPO_1_4.enabled = i_bVal
      .Page1.oPag.oTIMOTORE_1_5.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'INTERVENTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INTERVENTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TICODICE,"TICODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TIMEZZO,"TIMEZZO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TIDESCRI,"TIDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TITIPO,"TITIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TIMOTORE,"TIMOTORE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CHKT,"CHKT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INTERVENTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2])
    i_lTable = "INTERVENTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.INTERVENTI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INTERVENTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.INTERVENTI_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"interv","w_TICODICE")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into INTERVENTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INTERVENTI')
        i_extval=cp_InsertValODBCExtFlds(this,'INTERVENTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TICODICE,TIMEZZO,TIDESCRI,TITIPO,TIMOTORE"+;
                  ",CHKT "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TICODICE)+;
                  ","+cp_ToStrODBC(this.w_TIMEZZO)+;
                  ","+cp_ToStrODBC(this.w_TIDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_TITIPO)+;
                  ","+cp_ToStrODBC(this.w_TIMOTORE)+;
                  ","+cp_ToStrODBC(this.w_CHKT)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INTERVENTI')
        i_extval=cp_InsertValVFPExtFlds(this,'INTERVENTI')
        cp_CheckDeletedKey(i_cTable,0,'TICODICE',this.w_TICODICE)
        INSERT INTO (i_cTable);
              (TICODICE,TIMEZZO,TIDESCRI,TITIPO,TIMOTORE,CHKT  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TICODICE;
                  ,this.w_TIMEZZO;
                  ,this.w_TIDESCRI;
                  ,this.w_TITIPO;
                  ,this.w_TIMOTORE;
                  ,this.w_CHKT;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.INTERVENTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.INTERVENTI_IDX,i_nConn)
      *
      * update INTERVENTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'INTERVENTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TIMEZZO="+cp_ToStrODBC(this.w_TIMEZZO)+;
             ",TIDESCRI="+cp_ToStrODBC(this.w_TIDESCRI)+;
             ",TITIPO="+cp_ToStrODBCNull(this.w_TITIPO)+;
             ",TIMOTORE="+cp_ToStrODBC(this.w_TIMOTORE)+;
             ",CHKT="+cp_ToStrODBC(this.w_CHKT)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'INTERVENTI')
        i_cWhere = cp_PKFox(i_cTable  ,'TICODICE',this.w_TICODICE  )
        UPDATE (i_cTable) SET;
              TIMEZZO=this.w_TIMEZZO;
             ,TIDESCRI=this.w_TIDESCRI;
             ,TITIPO=this.w_TITIPO;
             ,TIMOTORE=this.w_TIMOTORE;
             ,CHKT=this.w_CHKT;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.INTERVENTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.INTERVENTI_IDX,i_nConn)
      *
      * delete INTERVENTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TICODICE',this.w_TICODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INTERVENTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INTERVENTI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_TIMOTORE<>.w_TIMOTORE
            .w_CHKT = iif(.w_TIMOTORE='T','DET',.w_TIMOTORE+'T')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIMOTORE_1_5.visible=!this.oPgFrm.Page1.oPag.oTIMOTORE_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TITIPO
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPOINTERVENTO_IDX,3]
    i_lTable = "TIPOINTERVENTO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPOINTERVENTO_IDX,2], .t., this.TIPOINTERVENTO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPOINTERVENTO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TITIPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('MICG_MMI',True,'TIPOINTERVENTO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MICODICE like "+cp_ToStrODBC(trim(this.w_TITIPO)+"%");

          i_ret=cp_SQL(i_nConn,"select MICODICE,MIDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MICODICE',trim(this.w_TITIPO))
          select MICODICE,MIDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TITIPO)==trim(_Link_.MICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TITIPO) and !this.bDontReportError
            deferred_cp_zoom('TIPOINTERVENTO','*','MICODICE',cp_AbsName(oSource.parent,'oTITIPO_1_4'),i_cWhere,'MICG_MMI',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MICODICE,MIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MICODICE',oSource.xKey(1))
            select MICODICE,MIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TITIPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MICODICE,MIDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MICODICE="+cp_ToStrODBC(this.w_TITIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MICODICE',this.w_TITIPO)
            select MICODICE,MIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TITIPO = NVL(_Link_.MICODICE,space(1))
      this.w_DESCRI = NVL(_Link_.MIDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_TITIPO = space(1)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPOINTERVENTO_IDX,2])+'\'+cp_ToStr(_Link_.MICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPOINTERVENTO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TITIPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIPOINTERVENTO_IDX,3] and i_nFlds+2<200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIPOINTERVENTO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.MICODICE as MICODICE104"+ ",link_1_4.MIDESCRI as MIDESCRI104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on INTERVENTI.TITIPO=link_1_4.MICODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and INTERVENTI.TITIPO=link_1_4.MICODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTICODICE_1_1.value==this.w_TICODICE)
      this.oPgFrm.Page1.oPag.oTICODICE_1_1.value=this.w_TICODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTIMEZZO_1_2.RadioValue()==this.w_TIMEZZO)
      this.oPgFrm.Page1.oPag.oTIMEZZO_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIDESCRI_1_3.value==this.w_TIDESCRI)
      this.oPgFrm.Page1.oPag.oTIDESCRI_1_3.value=this.w_TIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTITIPO_1_4.value==this.w_TITIPO)
      this.oPgFrm.Page1.oPag.oTITIPO_1_4.value=this.w_TITIPO
    endif
    if not(this.oPgFrm.Page1.oPag.oTIMOTORE_1_5.RadioValue()==this.w_TIMOTORE)
      this.oPgFrm.Page1.oPag.oTIMOTORE_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_10.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_10.value=this.w_DESCRI
    endif
    cp_SetControlsValueExtFlds(this,'INTERVENTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TIMEZZO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIMEZZO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_TIMEZZO)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIMOTORE = this.w_TIMOTORE
    return

enddefine

* --- Define pages as container
define class ttigc_mtiPag1 as StdContainer
  Width  = 457
  height = 170
  stdWidth  = 457
  stdheight = 170
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTICODICE_1_1 as StdField with uid="CSGYBCAWFI",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TICODICE", cQueryName = "TICODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 121037592,;
   bGlobalFont=.t.,;
    Height=21, Width=96, Left=131, Top=19, InputMask=replicate('X',10)


  add object oTIMEZZO_1_2 as StdCombo with uid="DDYSXKAZWO",rtseq=2,rtrep=.f.,left=131,top=42,width=96,height=22;
    , HelpContextID = 186629283;
    , cFormVar="w_TIMEZZO",RowSource=""+"Batterie,"+"Carrelli,"+"Mezzi/Veicoli", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oTIMEZZO_1_2.RadioValue()
    return(iif(this.value =1,'B',;
    iif(this.value =2,'C',;
    iif(this.value =3,'M',;
    space(1)))))
  endfunc
  func oTIMEZZO_1_2.GetRadio()
    this.Parent.oContained.w_TIMEZZO = this.RadioValue()
    return .t.
  endfunc

  func oTIMEZZO_1_2.SetRadio()
    this.Parent.oContained.w_TIMEZZO=trim(this.Parent.oContained.w_TIMEZZO)
    this.value = ;
      iif(this.Parent.oContained.w_TIMEZZO=='B',1,;
      iif(this.Parent.oContained.w_TIMEZZO=='C',2,;
      iif(this.Parent.oContained.w_TIMEZZO=='M',3,;
      0)))
  endfunc

  add object oTIDESCRI_1_3 as StdField with uid="CGEIVJDBXF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TIDESCRI", cQueryName = "TIDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 61811948,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=131, Top=65, cSayPict='"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"', cGetPict='"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"', InputMask=replicate('X',40)

  add object oTITIPO_1_4 as StdField with uid="BQXBMYLKSY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TITIPO", cQueryName = "TITIPO",;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 260320419,;
   bGlobalFont=.t.,;
    Height=21, Width=33, Left=131, Top=88, InputMask=replicate('X',1), bHasZoom = .t. , cLinkFile="TIPOINTERVENTO", cZoomOnZoom="MICG_MMI", oKey_1_1="MICODICE", oKey_1_2="this.w_TITIPO"

  func oTITIPO_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTITIPO_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTITIPO_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPOINTERVENTO','*','MICODICE',cp_AbsName(this.parent,'oTITIPO_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'MICG_MMI',"",'',this.parent.oContained
  endproc
  proc oTITIPO_1_4.mZoomOnZoom
    local i_obj
    i_obj=MICG_MMI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MICODICE=this.parent.oContained.w_TITIPO
     i_obj.ecpSave()
  endproc


  add object oTIMOTORE_1_5 as StdCombo with uid="HSPSLKIIAR",rtseq=5,rtrep=.f.,left=131,top=111,width=96,height=22;
    , HelpContextID = 264879336;
    , cFormVar="w_TIMOTORE",RowSource=""+"Elettrico,"+"Diesel,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIMOTORE_1_5.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'D',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIMOTORE_1_5.GetRadio()
    this.Parent.oContained.w_TIMOTORE = this.RadioValue()
    return .t.
  endfunc

  func oTIMOTORE_1_5.SetRadio()
    this.Parent.oContained.w_TIMOTORE=trim(this.Parent.oContained.w_TIMOTORE)
    this.value = ;
      iif(this.Parent.oContained.w_TIMOTORE=='E',1,;
      iif(this.Parent.oContained.w_TIMOTORE=='D',2,;
      iif(this.Parent.oContained.w_TIMOTORE=='T',3,;
      0)))
  endfunc

  func oTIMOTORE_1_5.mHide()
    with this.Parent.oContained
      return (.w_TIMEZZO='B')
    endwith
  endfunc

  add object oDESCRI_1_10 as StdField with uid="HJOAOKOFNS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 161355683,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=165, Top=88, InputMask=replicate('X',40)

  add object oStr_1_6 as StdString with uid="WNLKIUQXGH",Visible=.t., Left=30, Top=23,;
    Alignment=1, Width=98, Height=18,;
    Caption="Codice Intervento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="OMDDGHDTKO",Visible=.t., Left=4, Top=69,;
    Alignment=1, Width=124, Height=18,;
    Caption="Descrizione Intervento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="LHNPJGHMKV",Visible=.t., Left=45, Top=92,;
    Alignment=1, Width=83, Height=18,;
    Caption="Tipo Intervento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="KZFBNOTNOX",Visible=.t., Left=58, Top=115,;
    Alignment=1, Width=70, Height=18,;
    Caption="Tipo Motore :"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_TIMEZZO='B')
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="IYVCELZEFG",Visible=.t., Left=65, Top=46,;
    Alignment=1, Width=63, Height=18,;
    Caption="Tipo Mezzo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('tigc_mti','INTERVENTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TICODICE=INTERVENTI.TICODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
