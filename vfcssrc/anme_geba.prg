* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: anme_geba                                                       *
*              Archivio Batterie                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-07-24                                                      *
* Last revis.: 2018-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tanme_geba"))

* --- Class definition
define class tanme_geba as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 630
  Height = 237+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-10-29"
  HelpContextID=69876574
  max_rt_seq=24

  * --- Constant Properties
  ANAGRAFE_IDX = 0
  REPARTI_IDX = 0
  cFile = "ANAGRAFE"
  cKeySelect = "ANTIPO,ANCODICE,ANCODICE"
  cKeyWhere  = "ANTIPO=this.w_ANTIPO and ANCODICE=this.w_ANCODICE and ANCODICE=this.w_ANCODICE"
  cKeyWhereODBC = '"ANTIPO="+cp_ToStrODBC(this.w_ANTIPO)';
      +'+" and ANCODICE="+cp_ToStrODBC(this.w_ANCODICE)';
      +'+" and ANCODICE="+cp_ToStrODBC(this.w_ANCODICE)';

  cKeyWhereODBCqualified = '"ANAGRAFE.ANTIPO="+cp_ToStrODBC(this.w_ANTIPO)';
      +'+" and ANAGRAFE.ANCODICE="+cp_ToStrODBC(this.w_ANCODICE)';
      +'+" and ANAGRAFE.ANCODICE="+cp_ToStrODBC(this.w_ANCODICE)';

  cPrg = "anme_geba"
  cComment = "Archivio Batterie"
  icon = "anag.ico"
  cAutoZoom = 'BATTERIE'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ANTIPO = space(1)
  w_ANCODICE = space(10)
  w_ANNUM = 0
  w_ANDESCRI2 = space(40)
  w_ANVOLT = 0
  w_ANMATRI = space(15)
  w_ANMODE = space(15)
  w_ANMARCA = space(15)
  w_ANCODGAR = space(15)
  w_ANANNO = 0
  w_ANDTACQ = ctod('  /  /  ')
  w_ANDESCRI = space(100)
  w_ANCODICE = space(10)
  w_ANPRIN = ctod('  /  /  ')
  w_ANULIN = ctod('  /  /  ')
  w_ANEND = ctod('  /  /  ')
  w_ANORE = 0
  w_ANFLDESCRI = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_ANREPARTO = space(5)
  w_DEREPARTO = space(40)

  * --- Autonumbered Variables
  op_ANCODICE = this.W_ANCODICE
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ANAGRAFE','anme_geba')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tanme_gebaPag1","anme_geba",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati Batteria")
      .Pages(1).HelpContextID = 183036467
      .Pages(2).addobject("oPag","tanme_gebaPag2","anme_geba",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Interventi")
      .Pages(2).HelpContextID = 78118615
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='REPARTI'
    this.cWorkTables[2]='ANAGRAFE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ANAGRAFE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ANAGRAFE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ANTIPO = NVL(ANTIPO,space(1))
      .w_ANCODICE = NVL(ANCODICE,space(10))
      .w_ANCODICE = NVL(ANCODICE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_31_joined
    link_1_31_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ANAGRAFE where ANTIPO=KeySet.ANTIPO
    *                            and ANCODICE=KeySet.ANCODICE
    *                            and ANCODICE=KeySet.ANCODICE
    *
    i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ANAGRAFE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ANAGRAFE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ANAGRAFE '
      link_1_31_joined=this.AddJoinedLink_1_31(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ANTIPO',this.w_ANTIPO  ,'ANCODICE',this.w_ANCODICE  ,'ANCODICE',this.w_ANCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DEREPARTO = space(40)
        .w_ANTIPO = NVL(ANTIPO,space(1))
        .w_ANCODICE = NVL(ANCODICE,space(10))
        .op_ANCODICE = .w_ANCODICE
        .w_ANNUM = NVL(ANNUM,0)
        .w_ANDESCRI2 = NVL(ANDESCRI2,space(40))
        .w_ANVOLT = NVL(ANVOLT,0)
        .w_ANMATRI = NVL(ANMATRI,space(15))
        .w_ANMODE = NVL(ANMODE,space(15))
        .w_ANMARCA = NVL(ANMARCA,space(15))
        .w_ANCODGAR = NVL(ANCODGAR,space(15))
        .w_ANANNO = NVL(ANANNO,0)
        .w_ANDTACQ = NVL(cp_ToDate(ANDTACQ),ctod("  /  /  "))
        .w_ANDESCRI = NVL(ANDESCRI,space(100))
        .w_ANCODICE = NVL(ANCODICE,space(10))
        .op_ANCODICE = .w_ANCODICE
        .w_ANPRIN = NVL(cp_ToDate(ANPRIN),ctod("  /  /  "))
        .w_ANULIN = NVL(cp_ToDate(ANULIN),ctod("  /  /  "))
        .w_ANEND = NVL(cp_ToDate(ANEND),ctod("  /  /  "))
        .w_ANORE = NVL(ANORE,0)
        .w_ANFLDESCRI = NVL(ANFLDESCRI,space(1))
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_ANREPARTO = NVL(ANREPARTO,space(5))
          if link_1_31_joined
            this.w_ANREPARTO = NVL(RECODICE131,NVL(this.w_ANREPARTO,space(5)))
            this.w_DEREPARTO = NVL(REDESCRI131,space(40))
          else
          .link_1_31('Load')
          endif
        cp_LoadRecExtFlds(this,'ANAGRAFE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ANTIPO = space(1)
      .w_ANCODICE = space(10)
      .w_ANNUM = 0
      .w_ANDESCRI2 = space(40)
      .w_ANVOLT = 0
      .w_ANMATRI = space(15)
      .w_ANMODE = space(15)
      .w_ANMARCA = space(15)
      .w_ANCODGAR = space(15)
      .w_ANANNO = 0
      .w_ANDTACQ = ctod("  /  /  ")
      .w_ANDESCRI = space(100)
      .w_ANCODICE = space(10)
      .w_ANPRIN = ctod("  /  /  ")
      .w_ANULIN = ctod("  /  /  ")
      .w_ANEND = ctod("  /  /  ")
      .w_ANORE = 0
      .w_ANFLDESCRI = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_ANREPARTO = space(5)
      .w_DEREPARTO = space(40)
      if .cFunction<>"Filter"
        .w_ANTIPO = 'B'
          .DoRTCalc(2,17,.f.)
        .w_ANFLDESCRI = 'N'
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .DoRTCalc(19,23,.f.)
          if not(empty(.w_ANREPARTO))
          .link_1_31('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'ANAGRAFE')
    this.DoRTCalc(24,24,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
    cp_AskTableProg(this,i_nConn,"NUMBATT","w_ANCODICE")
    with this
      .op_ANCODICE = .w_ANCODICE
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oANCODICE_1_2.enabled = i_bVal
      .Page1.oPag.oANNUM_1_3.enabled = i_bVal
      .Page1.oPag.oANDESCRI2_1_4.enabled = i_bVal
      .Page1.oPag.oANVOLT_1_5.enabled = i_bVal
      .Page1.oPag.oANMATRI_1_6.enabled = i_bVal
      .Page1.oPag.oANMODE_1_7.enabled = i_bVal
      .Page1.oPag.oANMARCA_1_8.enabled = i_bVal
      .Page1.oPag.oANCODGAR_1_9.enabled = i_bVal
      .Page1.oPag.oANANNO_1_10.enabled = i_bVal
      .Page1.oPag.oANDTACQ_1_11.enabled = i_bVal
      .Page1.oPag.oANDESCRI_1_14.enabled = i_bVal
      .Page2.oPag.oANPRIN_2_2.enabled = i_bVal
      .Page2.oPag.oANULIN_2_4.enabled = i_bVal
      .Page2.oPag.oANEND_2_6.enabled = i_bVal
      .Page2.oPag.oANORE_2_8.enabled = i_bVal
      .Page1.oPag.oANFLDESCRI_1_19.enabled = i_bVal
      .Page1.oPag.oANREPARTO_1_31.enabled = i_bVal
      .Page1.oPag.oBtn_1_23.enabled = .Page1.oPag.oBtn_1_23.mCond()
      if i_cOp = "Edit"
        .Page1.oPag.oANCODICE_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oANCODICE_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ANAGRAFE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPO,"ANTIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODICE,"ANCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANNUM,"ANNUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESCRI2,"ANDESCRI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANVOLT,"ANVOLT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANMATRI,"ANMATRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANMODE,"ANMODE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANMARCA,"ANMARCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODGAR,"ANCODGAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANANNO,"ANANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDTACQ,"ANDTACQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESCRI,"ANDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODICE,"ANCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPRIN,"ANPRIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANULIN,"ANULIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANEND,"ANEND",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANORE,"ANORE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLDESCRI,"ANFLDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANREPARTO,"ANREPARTO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
    i_lTable = "ANAGRAFE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ANAGRAFE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ANAGRAFE_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"NUMBATT","w_ANCODICE")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ANAGRAFE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ANAGRAFE')
        i_extval=cp_InsertValODBCExtFlds(this,'ANAGRAFE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ANTIPO,ANCODICE,ANNUM,ANDESCRI2,ANVOLT"+;
                  ",ANMATRI,ANMODE,ANMARCA,ANCODGAR,ANANNO"+;
                  ",ANDTACQ,ANDESCRI,ANPRIN,ANULIN,ANEND"+;
                  ",ANORE,ANFLDESCRI,UTCC,UTCV,UTDC"+;
                  ",UTDV,ANREPARTO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ANTIPO)+;
                  ","+cp_ToStrODBC(this.w_ANCODICE)+;
                  ","+cp_ToStrODBC(this.w_ANNUM)+;
                  ","+cp_ToStrODBC(this.w_ANDESCRI2)+;
                  ","+cp_ToStrODBC(this.w_ANVOLT)+;
                  ","+cp_ToStrODBC(this.w_ANMATRI)+;
                  ","+cp_ToStrODBC(this.w_ANMODE)+;
                  ","+cp_ToStrODBC(this.w_ANMARCA)+;
                  ","+cp_ToStrODBC(this.w_ANCODGAR)+;
                  ","+cp_ToStrODBC(this.w_ANANNO)+;
                  ","+cp_ToStrODBC(this.w_ANDTACQ)+;
                  ","+cp_ToStrODBC(this.w_ANDESCRI)+;
                  ","+cp_ToStrODBC(this.w_ANPRIN)+;
                  ","+cp_ToStrODBC(this.w_ANULIN)+;
                  ","+cp_ToStrODBC(this.w_ANEND)+;
                  ","+cp_ToStrODBC(this.w_ANORE)+;
                  ","+cp_ToStrODBC(this.w_ANFLDESCRI)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBCNull(this.w_ANREPARTO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ANAGRAFE')
        i_extval=cp_InsertValVFPExtFlds(this,'ANAGRAFE')
        cp_CheckDeletedKey(i_cTable,0,'ANTIPO',this.w_ANTIPO,'ANCODICE',this.w_ANCODICE,'ANCODICE',this.w_ANCODICE)
        INSERT INTO (i_cTable);
              (ANTIPO,ANCODICE,ANNUM,ANDESCRI2,ANVOLT,ANMATRI,ANMODE,ANMARCA,ANCODGAR,ANANNO,ANDTACQ,ANDESCRI,ANPRIN,ANULIN,ANEND,ANORE,ANFLDESCRI,UTCC,UTCV,UTDC,UTDV,ANREPARTO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ANTIPO;
                  ,this.w_ANCODICE;
                  ,this.w_ANNUM;
                  ,this.w_ANDESCRI2;
                  ,this.w_ANVOLT;
                  ,this.w_ANMATRI;
                  ,this.w_ANMODE;
                  ,this.w_ANMARCA;
                  ,this.w_ANCODGAR;
                  ,this.w_ANANNO;
                  ,this.w_ANDTACQ;
                  ,this.w_ANDESCRI;
                  ,this.w_ANPRIN;
                  ,this.w_ANULIN;
                  ,this.w_ANEND;
                  ,this.w_ANORE;
                  ,this.w_ANFLDESCRI;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_ANREPARTO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ANAGRAFE_IDX,i_nConn)
      *
      * update ANAGRAFE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ANAGRAFE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ANNUM="+cp_ToStrODBC(this.w_ANNUM)+;
             ",ANDESCRI2="+cp_ToStrODBC(this.w_ANDESCRI2)+;
             ",ANVOLT="+cp_ToStrODBC(this.w_ANVOLT)+;
             ",ANMATRI="+cp_ToStrODBC(this.w_ANMATRI)+;
             ",ANMODE="+cp_ToStrODBC(this.w_ANMODE)+;
             ",ANMARCA="+cp_ToStrODBC(this.w_ANMARCA)+;
             ",ANCODGAR="+cp_ToStrODBC(this.w_ANCODGAR)+;
             ",ANANNO="+cp_ToStrODBC(this.w_ANANNO)+;
             ",ANDTACQ="+cp_ToStrODBC(this.w_ANDTACQ)+;
             ",ANDESCRI="+cp_ToStrODBC(this.w_ANDESCRI)+;
             ",ANPRIN="+cp_ToStrODBC(this.w_ANPRIN)+;
             ",ANULIN="+cp_ToStrODBC(this.w_ANULIN)+;
             ",ANEND="+cp_ToStrODBC(this.w_ANEND)+;
             ",ANORE="+cp_ToStrODBC(this.w_ANORE)+;
             ",ANFLDESCRI="+cp_ToStrODBC(this.w_ANFLDESCRI)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",ANREPARTO="+cp_ToStrODBCNull(this.w_ANREPARTO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ANAGRAFE')
        i_cWhere = cp_PKFox(i_cTable  ,'ANTIPO',this.w_ANTIPO  ,'ANCODICE',this.w_ANCODICE  ,'ANCODICE',this.w_ANCODICE  )
        UPDATE (i_cTable) SET;
              ANNUM=this.w_ANNUM;
             ,ANDESCRI2=this.w_ANDESCRI2;
             ,ANVOLT=this.w_ANVOLT;
             ,ANMATRI=this.w_ANMATRI;
             ,ANMODE=this.w_ANMODE;
             ,ANMARCA=this.w_ANMARCA;
             ,ANCODGAR=this.w_ANCODGAR;
             ,ANANNO=this.w_ANANNO;
             ,ANDTACQ=this.w_ANDTACQ;
             ,ANDESCRI=this.w_ANDESCRI;
             ,ANPRIN=this.w_ANPRIN;
             ,ANULIN=this.w_ANULIN;
             ,ANEND=this.w_ANEND;
             ,ANORE=this.w_ANORE;
             ,ANFLDESCRI=this.w_ANFLDESCRI;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,ANREPARTO=this.w_ANREPARTO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ANAGRAFE_IDX,i_nConn)
      *
      * delete ANAGRAFE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ANTIPO',this.w_ANTIPO  ,'ANCODICE',this.w_ANCODICE  ,'ANCODICE',this.w_ANCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ANAGRAFE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ANAGRAFE_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,24,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oANDESCRI2_1_4.enabled = this.oPgFrm.Page1.oPag.oANDESCRI2_1_4.mCond()
    this.oPgFrm.Page1.oPag.oANDESCRI_1_14.enabled = this.oPgFrm.Page1.oPag.oANDESCRI_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_23.visible=!this.oPgFrm.Page1.oPag.oBtn_1_23.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ANREPARTO
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REPARTI_IDX,3]
    i_lTable = "REPARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REPARTI_IDX,2], .t., this.REPARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REPARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANREPARTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('REPA_MRE',True,'REPARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RECODICE like "+cp_ToStrODBC(trim(this.w_ANREPARTO)+"%");

          i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RECODICE',trim(this.w_ANREPARTO))
          select RECODICE,REDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANREPARTO)==trim(_Link_.RECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANREPARTO) and !this.bDontReportError
            deferred_cp_zoom('REPARTI','*','RECODICE',cp_AbsName(oSource.parent,'oANREPARTO_1_31'),i_cWhere,'REPA_MRE',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODICE',oSource.xKey(1))
            select RECODICE,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANREPARTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODICE,REDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RECODICE="+cp_ToStrODBC(this.w_ANREPARTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODICE',this.w_ANREPARTO)
            select RECODICE,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANREPARTO = NVL(_Link_.RECODICE,space(5))
      this.w_DEREPARTO = NVL(_Link_.REDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ANREPARTO = space(5)
      endif
      this.w_DEREPARTO = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REPARTI_IDX,2])+'\'+cp_ToStr(_Link_.RECODICE,1)
      cp_ShowWarn(i_cKey,this.REPARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANREPARTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_31(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.REPARTI_IDX,3] and i_nFlds+2<200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.REPARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_31.RECODICE as RECODICE131"+ ",link_1_31.REDESCRI as REDESCRI131"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_31 on ANAGRAFE.ANREPARTO=link_1_31.RECODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_31"
          i_cKey=i_cKey+'+" and ANAGRAFE.ANREPARTO=link_1_31.RECODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANCODICE_1_2.value==this.w_ANCODICE)
      this.oPgFrm.Page1.oPag.oANCODICE_1_2.value=this.w_ANCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oANNUM_1_3.value==this.w_ANNUM)
      this.oPgFrm.Page1.oPag.oANNUM_1_3.value=this.w_ANNUM
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI2_1_4.value==this.w_ANDESCRI2)
      this.oPgFrm.Page1.oPag.oANDESCRI2_1_4.value=this.w_ANDESCRI2
    endif
    if not(this.oPgFrm.Page1.oPag.oANVOLT_1_5.value==this.w_ANVOLT)
      this.oPgFrm.Page1.oPag.oANVOLT_1_5.value=this.w_ANVOLT
    endif
    if not(this.oPgFrm.Page1.oPag.oANMATRI_1_6.value==this.w_ANMATRI)
      this.oPgFrm.Page1.oPag.oANMATRI_1_6.value=this.w_ANMATRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANMODE_1_7.value==this.w_ANMODE)
      this.oPgFrm.Page1.oPag.oANMODE_1_7.value=this.w_ANMODE
    endif
    if not(this.oPgFrm.Page1.oPag.oANMARCA_1_8.value==this.w_ANMARCA)
      this.oPgFrm.Page1.oPag.oANMARCA_1_8.value=this.w_ANMARCA
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODGAR_1_9.value==this.w_ANCODGAR)
      this.oPgFrm.Page1.oPag.oANCODGAR_1_9.value=this.w_ANCODGAR
    endif
    if not(this.oPgFrm.Page1.oPag.oANANNO_1_10.value==this.w_ANANNO)
      this.oPgFrm.Page1.oPag.oANANNO_1_10.value=this.w_ANANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oANDTACQ_1_11.value==this.w_ANDTACQ)
      this.oPgFrm.Page1.oPag.oANDTACQ_1_11.value=this.w_ANDTACQ
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_14.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_14.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oANPRIN_2_2.value==this.w_ANPRIN)
      this.oPgFrm.Page2.oPag.oANPRIN_2_2.value=this.w_ANPRIN
    endif
    if not(this.oPgFrm.Page2.oPag.oANULIN_2_4.value==this.w_ANULIN)
      this.oPgFrm.Page2.oPag.oANULIN_2_4.value=this.w_ANULIN
    endif
    if not(this.oPgFrm.Page2.oPag.oANEND_2_6.value==this.w_ANEND)
      this.oPgFrm.Page2.oPag.oANEND_2_6.value=this.w_ANEND
    endif
    if not(this.oPgFrm.Page2.oPag.oANORE_2_8.value==this.w_ANORE)
      this.oPgFrm.Page2.oPag.oANORE_2_8.value=this.w_ANORE
    endif
    if not(this.oPgFrm.Page1.oPag.oANFLDESCRI_1_19.RadioValue()==this.w_ANFLDESCRI)
      this.oPgFrm.Page1.oPag.oANFLDESCRI_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANREPARTO_1_31.value==this.w_ANREPARTO)
      this.oPgFrm.Page1.oPag.oANREPARTO_1_31.value=this.w_ANREPARTO
    endif
    if not(this.oPgFrm.Page1.oPag.oDEREPARTO_1_33.value==this.w_DEREPARTO)
      this.oPgFrm.Page1.oPag.oDEREPARTO_1_33.value=this.w_DEREPARTO
    endif
    cp_SetControlsValueExtFlds(this,'ANAGRAFE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- anme_geba
      IF this.cFunction='Load'
          this.w_UTCC=i_codute
          this.w_UTDC=datetime()
      ENDIF
      IF this.cFunction='Edit'
          this.w_UTCV=i_codute
          this.w_UTDV=datetime()
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tanme_gebaPag1 as StdContainer
  Width  = 626
  height = 237
  stdWidth  = 626
  stdheight = 237
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANCODICE_1_2 as StdField with uid="SAPJOGSTDD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ANCODICE", cQueryName = "ANTIPO,ANCODICE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 84799525,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=109, Top=9, InputMask=replicate('X',10)

  add object oANNUM_1_3 as StdField with uid="XUEEJJFNOB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ANNUM", cQueryName = "ANNUM",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25629806,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=109, Top=63, cSayPict='"99999"', cGetPict='"99999"'

  add object oANDESCRI2_1_4 as StdField with uid="HVUSJUYLYQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ANDESCRI2", cQueryName = "ANDESCRI2",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 57614964,;
   bGlobalFont=.t.,;
    Height=21, Width=80, Left=226, Top=63, cSayPict='"!!!!!"', cGetPict='"!!!!!"', InputMask=replicate('X',40)

  func oANDESCRI2_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANNUM<>0)
    endwith
   endif
  endfunc

  add object oANVOLT_1_5 as StdField with uid="QLMRHZADZZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ANVOLT", cQueryName = "ANVOLT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 220261266,;
   bGlobalFont=.t.,;
    Height=21, Width=117, Left=109, Top=91

  add object oANMATRI_1_6 as StdField with uid="ISBKGLAXFE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ANMATRI", cQueryName = "ANMATRI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 70773723,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=109, Top=119, cSayPict='"!!!!!!!!!!!!!!!"', cGetPict='"!!!!!!!!!!!!!!!"', InputMask=replicate('X',15)

  add object oANMODE_1_7 as StdField with uid="BYUZZJOZGG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ANMODE", cQueryName = "ANMODE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 85453714,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=328, Top=88, InputMask=replicate('X',15)

  add object oANMARCA_1_8 as StdField with uid="PNPSPZJUXA",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ANMARCA", cQueryName = "ANMARCA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 37219283,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=109, Top=147, cSayPict='"!!!!!!!!!!!!!!!"', cGetPict='"!!!!!!!!!!!!!!!"', InputMask=replicate('X',15)

  add object oANCODGAR_1_9 as StdField with uid="CDGNHWLBDS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ANCODGAR", cQueryName = "ANCODGAR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 84799731,;
   bGlobalFont=.t.,;
    Height=21, Width=119, Left=328, Top=116, InputMask=replicate('X',15)

  add object oANANNO_1_10 as StdField with uid="ITWFBZTYWL",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ANANNO", cQueryName = "ANANNO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 17044590,;
   bGlobalFont=.t.,;
    Height=21, Width=117, Left=109, Top=173, cSayPict='"9999"', cGetPict='"9999"'

  add object oANDTACQ_1_11 as StdField with uid="DJPUBXONFP",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ANDTACQ", cQueryName = "ANDTACQ",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 39775203,;
   bGlobalFont=.t.,;
    Height=21, Width=82, Left=109, Top=197

  add object oANDESCRI_1_14 as StdField with uid="RLIEXIOBZO",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 57602164,;
   bGlobalFont=.t.,;
    Height=21, Width=501, Left=109, Top=35, InputMask=replicate('X',100)

  func oANDESCRI_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLDESCRI='S')
    endwith
   endif
  endfunc

  add object oANFLDESCRI_1_19 as StdCheck with uid="MSDYFYHRWB",rtseq=18,rtrep=.f.,left=592, top=57, caption="", tabstop=.f.,;
    HelpContextID = 82170389,;
    cFormVar="w_ANFLDESCRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANFLDESCRI_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLDESCRI_1_19.GetRadio()
    this.Parent.oContained.w_ANFLDESCRI = this.RadioValue()
    return .t.
  endfunc

  func oANFLDESCRI_1_19.SetRadio()
    this.Parent.oContained.w_ANFLDESCRI=trim(this.Parent.oContained.w_ANFLDESCRI)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLDESCRI=='S',1,;
      0)
  endfunc


  add object oObj_1_21 as cp_runprogram with uid="LAMYOYYRNL",left=94, top=281, width=50,height=50,;
    caption='Object',;
    prg="ANGE_BGD('A')",;
    cEvent = "w_ANMODE Changed, w_ANDESCRI2 Changed, w_ANNUM Changed, w_ANMATRI Changed, w_ANMARCA Changed,  w_ANVOLT Changed,Record Inserted,Record Updated,Done",;
    nPag=1;
    , HelpContextID = 93731218;
  , bGlobalFont=.t.



  add object oBtn_1_23 as StdButton with uid="EDNTDBOWYR",left=522, top=205, width=88,height=25,;
    caption="Interventi", nPag=1,tabstop=.f.;
    , HelpContextID = 78118615;
  , bGlobalFont=.t.

    proc oBtn_1_23.Click()
      do mbli_kli with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not.empty(.w_ANDESCRI) )
      endwith
    endif
  endfunc

  func oBtn_1_23.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction ='Load')
     endwith
    endif
  endfunc

  add object oANREPARTO_1_31 as StdField with uid="FRBGPQRIFW",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ANREPARTO", cQueryName = "ANREPARTO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 260227036,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=328, Top=140, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="REPARTI", cZoomOnZoom="REPA_MRE", oKey_1_1="RECODICE", oKey_1_2="this.w_ANREPARTO"

  func oANREPARTO_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oANREPARTO_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANREPARTO_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REPARTI','*','RECODICE',cp_AbsName(this.parent,'oANREPARTO_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'REPA_MRE',"",'',this.parent.oContained
  endproc
  proc oANREPARTO_1_31.mZoomOnZoom
    local i_obj
    i_obj=REPA_MRE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RECODICE=this.parent.oContained.w_ANREPARTO
     i_obj.ecpSave()
  endproc

  add object oDEREPARTO_1_33 as StdField with uid="RMOCTVHXCY",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DEREPARTO", cQueryName = "DEREPARTO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 260263132,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=328, Top=162, InputMask=replicate('X',40)

  add object oStr_1_12 as StdString with uid="SKJECVYYLJ",Visible=.t., Left=50, Top=123,;
    Alignment=1, Width=52, Height=18,;
    Caption="Matricola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="OTTKMEYXBZ",Visible=.t., Left=66, Top=151,;
    Alignment=1, Width=36, Height=18,;
    Caption="Marca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="BXRVEJXHMH",Visible=.t., Left=34, Top=39,;
    Alignment=1, Width=68, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="EMYEQOEOHF",Visible=.t., Left=54, Top=67,;
    Alignment=1, Width=48, Height=18,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="IHDYHWVHEG",Visible=.t., Left=6, Top=177,;
    Alignment=1, Width=96, Height=18,;
    Caption="Anno Produzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="XRVWVPBUQZ",Visible=.t., Left=4, Top=95,;
    Alignment=1, Width=98, Height=18,;
    Caption="Voltaggio Batterie:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="LZHIXVMZLP",Visible=.t., Left=470, Top=61,;
    Alignment=1, Width=118, Height=20,;
    Caption="Descrizione Manuale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="NBZPAGMDKL",Visible=.t., Left=280, Top=92,;
    Alignment=1, Width=46, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="QMISTKZPTB",Visible=.t., Left=60, Top=13,;
    Alignment=1, Width=42, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="VPBLHVYFET",Visible=.t., Left=24, Top=201,;
    Alignment=1, Width=78, Height=18,;
    Caption="Data Acquisto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="GZHJGOPSTP",Visible=.t., Left=232, Top=120,;
    Alignment=1, Width=94, Height=18,;
    Caption="Codice Garanzia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="IUHNVYJOCR",Visible=.t., Left=235, Top=144,;
    Alignment=1, Width=92, Height=18,;
    Caption="Codice Reparto :"  ;
  , bGlobalFont=.t.
enddefine
define class tanme_gebaPag2 as StdContainer
  Width  = 626
  height = 237
  stdWidth  = 626
  stdheight = 237
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANPRIN_2_2 as StdField with uid="QRJKEBINQL",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ANPRIN", cQueryName = "ANPRIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 95753326,;
   bGlobalFont=.t.,;
    Height=21, Width=101, Left=186, Top=64

  add object oANULIN_2_4 as StdField with uid="GRTXDFQHDT",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ANULIN", cQueryName = "ANULIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 101717102,;
   bGlobalFont=.t.,;
    Height=21, Width=101, Left=186, Top=96

  add object oANEND_2_6 as StdField with uid="GJZDNXKKJK",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ANEND", cQueryName = "ANEND",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 184554606,;
   bGlobalFont=.t.,;
    Height=21, Width=116, Left=474, Top=192

  add object oANORE_2_8 as StdField with uid="SOJVSVDHJD",rtseq=17,rtrep=.f.,;
    cFormVar = "w_ANORE", cQueryName = "ANORE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 162927726,;
   bGlobalFont=.t.,;
    Height=21, Width=101, Left=186, Top=36

  add object oStr_2_3 as StdString with uid="MRYMZZSDOD",Visible=.t., Left=22, Top=68,;
    Alignment=1, Width=164, Height=18,;
    Caption="Data Intervento Programmato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="WSSEIZSCAO",Visible=.t., Left=62, Top=100,;
    Alignment=1, Width=124, Height=18,;
    Caption="Data Ultimo Intervento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="VOVCQPOIUF",Visible=.t., Left=376, Top=196,;
    Alignment=1, Width=98, Height=18,;
    Caption="Data Cessazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="ZMFTMUOHEO",Visible=.t., Left=113, Top=40,;
    Alignment=1, Width=73, Height=18,;
    Caption="Ore Lavorate:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('anme_geba','ANAGRAFE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ANTIPO=ANAGRAFE.ANTIPO";
  +" and "+i_cAliasName2+".ANCODICE=ANAGRAFE.ANCODICE";
  +" and "+i_cAliasName2+".ANCODICE=ANAGRAFE.ANCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
