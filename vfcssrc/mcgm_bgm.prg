* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: mcgm_bgm                                                        *
*              Gestione Interventi                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-07-31                                                      *
* Last revis.: 2016-08-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAram
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tmcgm_bgm",oParentObject,m.pAram)
return(i_retval)

define class tmcgm_bgm as StdBatch
  * --- Local variables
  pAram = space(1)
  CODICE = space(10)
  TINT = space(1)
  DESINT = space(40)
  w_MESSAGGIO = space(10)
  WRITE = .f.
  ROWPOS = 0
  LSELEZ = 0
  PROGRE = 0
  CPROGRE = 0
  w_CFUNC = space(10)
  w_PADRE = .NULL.
  w_MESS = space(200)
  w_TRIG = 0
  w_FIGLIO = .NULL.
  w_ZOOM = .NULL.
  w_PADRE = .NULL.
  nr = 0
  SECONDA = .f.
  * --- WorkFile variables
  ANAGRAFE_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Carrelli Inizio---
    * --- --Batterie Inizio---
    * --- --Mezzi Inizio---
    this.WRITE = .T.
    do case
      case this.pAram="A"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.WRITE=.T.
          * --- Write into ANAGRAFE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ANAGRAFE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ANAGRAFE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ANAGRAFE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANORE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCORE),'ANAGRAFE','ANORE');
            +",ANULIN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCDATA),'ANAGRAFE','ANULIN');
            +",ANPRORE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCOPRX),'ANAGRAFE','ANPRORE');
                +i_ccchkf ;
            +" where ";
                +"ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MCCODICE);
                +" and ANTIPO = "+cp_ToStrODBC(this.oParentObject.w_MCTIPO);
                   )
          else
            update (i_cTable) set;
                ANORE = this.oParentObject.w_MCORE;
                ,ANULIN = this.oParentObject.w_MCDATA;
                ,ANPRORE = this.oParentObject.w_MCOPRX;
                &i_ccchkf. ;
             where;
                ANCODICE = this.oParentObject.w_MCCODICE;
                and ANTIPO = this.oParentObject.w_MCTIPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.pAram="B"
        ND=THIS.OPARENTOBJECT.w_INTZOOM.CCURSOR
        if this.oParentObject.w_Selez>1
          this.w_PADRE = this.oParentObject.oParentObject.ctrsname
          this.w_FIGLIO = this.oparentobject.oparentobject
          this.w_ZOOM = this.oparentobject
          this.TINT = this.w_ZOOM.w_MCTINT
          select (this.w_PADRE)
          this.PROGRE = RECNO()
          SELECT * from (ND) into cursor SELEZIONATI where xchk=1
          SELECT SELEZIONATI
          GO TOP
          SCAN
          this.CODICE = TICODICE
          this.DESINT = TIDESCRI
          this.w_FIGLIO.AddRow()     
          UPDATE (this.w_PADRE) SET T_MCTINT=this.TINT WHERE I_RECNO=this.PROGRE
          UPDATE (this.w_PADRE) SET T_MCCODINT=this.CODICE WHERE I_RECNO=this.PROGRE
          UPDATE (this.w_PADRE) SET T_MCDESINT=this.DESINT WHERE I_RECNO=this.PROGRE
          this.PROGRE = this.PROGRE + 1
          ENDSCAN
          this.w_FIGLIO.AddRow()     
          this.w_FIGLIO.trsfromwork()
          this.w_FIGLIO.REFRESH()     
          this.w_ZOOM.ecpquit()     
        else
          this.w_PADRE = this.oParentObject.oParentObject
          this.w_FIGLIO = this.oparentobject.oparentobject
          this.w_ZOOM = this.oparentobject
          SELECT (ND)
          this.CODICE = TICODICE
          this.DESINT = TIDESCRI
          this.w_PADRE.MarkPos()     
          this.oParentObject.oparentobject.w_MCCODINT = this.CODICE
          this.oParentObject.oparentobject.w_MCDESINT = this.DESINT
          SELECT (this.w_PADRE.cTrsName)
          this.W_padre.trsfromwork()
          this.w_FIGLIO.AddRow()     
          this.w_FIGLIO.trsfromwork()
          this.w_FIGLIO.REFRESH()     
          this.w_ZOOM.ecpquit()     
        endif
      case this.pAram="+"
        this.oParentObject.w_SELEZ = 0
        this.LSELEZ = 0
        this.ROWPOS = recno()
        KT=THIS.OPARENTOBJECT.w_INTZOOM.CCURSOR
        SELECT (KT)
        go top
        scan
        if xchk=1
          this.LSELEZ = this.LSELEZ+1
        endif
        endscan
        GO this.ROWPOS
        this.oParentObject.w_SELEZ=this.LSELEZ
      case this.pAram="-"
      case this.pAram="C"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.WRITE=.T.
          * --- Write into ANAGRAFE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ANAGRAFE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ANAGRAFE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ANAGRAFE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANORE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMORE),'ANAGRAFE','ANORE');
            +",ANULIN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMDATA),'ANAGRAFE','ANULIN');
            +",ANPRORE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMOPRX),'ANAGRAFE','ANPRORE');
                +i_ccchkf ;
            +" where ";
                +"ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MMCODICE);
                +" and ANTIPO = "+cp_ToStrODBC(this.oParentObject.w_MMTIPO);
                   )
          else
            update (i_cTable) set;
                ANORE = this.oParentObject.w_MMORE;
                ,ANULIN = this.oParentObject.w_MMDATA;
                ,ANPRORE = this.oParentObject.w_MMOPRX;
                &i_ccchkf. ;
             where;
                ANCODICE = this.oParentObject.w_MMCODICE;
                and ANTIPO = this.oParentObject.w_MMTIPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.pAram="D"
        if this.oParentObject.w_MBDTANAG>this.oParentObject.w_MBDATA
          this.WRITE = .F.
          this.w_Messaggio = "ATTENZIONE"+ chr(13) + chr(10) +"La Data di Manutenzione � inferiore alla data dell'ultimo Intervento"+ chr(13) + chr(13) +chr(10) +"Aggiornare comunque la data inserita?"
          if cp_YesNo(this.w_Messaggio)
            this.WRITE = .T.
          endif
        endif
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.WRITE=.T.
          * --- Write into ANAGRAFE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ANAGRAFE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ANAGRAFE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ANAGRAFE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANULIN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MBDATA),'ANAGRAFE','ANULIN');
                +i_ccchkf ;
            +" where ";
                +"ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MBCODICE);
                +" and ANTIPO = "+cp_ToStrODBC(this.oParentObject.w_MBTIPO);
                   )
          else
            update (i_cTable) set;
                ANULIN = this.oParentObject.w_MBDATA;
                &i_ccchkf. ;
             where;
                ANCODICE = this.oParentObject.w_MBCODICE;
                and ANTIPO = this.oParentObject.w_MBTIPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.pAram="E"
        if this.oParentObject.w_OREANAG=this.oParentObject.w_MCORE
          * --- Write into ANAGRAFE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ANAGRAFE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ANAGRAFE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ANAGRAFE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANORE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCOREANAG),'ANAGRAFE','ANORE');
            +",ANULIN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCDTANAG),'ANAGRAFE','ANULIN');
                +i_ccchkf ;
            +" where ";
                +"ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MCCODICE);
                +" and ANTIPO = "+cp_ToStrODBC(this.oParentObject.w_MCTIPO);
                   )
          else
            update (i_cTable) set;
                ANORE = this.oParentObject.w_MCOREANAG;
                ,ANULIN = this.oParentObject.w_MCDTANAG;
                &i_ccchkf. ;
             where;
                ANCODICE = this.oParentObject.w_MCCODICE;
                and ANTIPO = this.oParentObject.w_MCTIPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.pAram="F"
        if this.oParentObject.w_OREANAG=this.oParentObject.w_MMORE
          * --- Write into ANAGRAFE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ANAGRAFE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ANAGRAFE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ANAGRAFE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANORE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMOREANAG),'ANAGRAFE','ANORE');
            +",ANULIN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMDTANAG),'ANAGRAFE','ANULIN');
                +i_ccchkf ;
            +" where ";
                +"ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MMCODICE);
                +" and ANTIPO = "+cp_ToStrODBC(this.oParentObject.w_MMTIPO);
                   )
          else
            update (i_cTable) set;
                ANORE = this.oParentObject.w_MMOREANAG;
                ,ANULIN = this.oParentObject.w_MMDTANAG;
                &i_ccchkf. ;
             where;
                ANCODICE = this.oParentObject.w_MMCODICE;
                and ANTIPO = this.oParentObject.w_MMTIPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.pAram="G"
        this.w_PADRE = This.oParentObject
        this.w_CFUNC = this.w_PADRE.cFunction
        this.w_TRIG = this.w_PADRE.NUMROW()
        if this.w_TRIG=0 
          this.oParentObject.w_SAVE = .F.
        else
          this.oParentObject.w_SAVE = .T.
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PADRE = This.oParentObject
    this.w_CFUNC = this.w_PADRE.cFunction
    this.w_PADRE.MarkPos()     
    this.w_TRIG = this.w_PADRE.NUMROW()
    if this.w_TRIG=0 AND this.w_CFUNC<>"Query"
      this.w_MESS = "Registrazione senza righe di dettaglio"
      cp_ErrorMsg(this.w_MESS,"Stop","Attenzione")
      this.oParentObject.w_OK = .F.
      this.w_PADRE.RePos()     
      this.WRITE = .F.
    else
      this.WRITE = .T.
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pAram)
    this.pAram=pAram
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ANAGRAFE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAram"
endproc
