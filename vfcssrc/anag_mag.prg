* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: anag_mag                                                        *
*              Gomme                                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2016-07-26                                                      *
* Last revis.: 2016-07-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tanag_mag"))

* --- Class definition
define class tanag_mag as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 439
  Height = 173+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-07-26"
  HelpContextID=130688959
  max_rt_seq=8

  * --- Constant Properties
  Gomme_IDX = 0
  cFile = "Gomme"
  cKeySelect = "SERIALANT"
  cKeyWhere  = "SERIALANT=this.w_SERIALANT"
  cKeyWhereODBC = '"SERIALANT="+cp_ToStrODBC(this.w_SERIALANT)';

  cKeyWhereODBCqualified = '"Gomme.SERIALANT="+cp_ToStrODBC(this.w_SERIALANT)';

  cPrg = "anag_mag"
  cComment = "Gomme"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SERIALANT = space(10)
  o_SERIALANT = space(10)
  w_GOMISURE = space(10)
  w_GOFORI = 0
  w_SERIALPOST = space(10)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Autonumbered Variables
  op_SERIALANT = this.W_SERIALANT
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'Gomme','anag_mag')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tanag_magPag1","anag_mag",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Gomme")
      .Pages(1).HelpContextID = 17163248
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSERIALANT_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='Gomme'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.Gomme_IDX,5],7]
    this.nPostItConn=i_TableProp[this.Gomme_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SERIALANT = NVL(SERIALANT,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from Gomme where SERIALANT=KeySet.SERIALANT
    *
    i_nConn = i_TableProp[this.Gomme_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.Gomme_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('Gomme')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "Gomme.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' Gomme '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SERIALANT',this.w_SERIALANT  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_SERIALANT = NVL(SERIALANT,space(10))
        .op_SERIALANT = .w_SERIALANT
        .w_GOMISURE = NVL(GOMISURE,space(10))
        .w_GOFORI = NVL(GOFORI,0)
        .w_SERIALPOST = NVL(SERIALPOST,space(10))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        cp_LoadRecExtFlds(this,'Gomme')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SERIALANT = space(10)
      .w_GOMISURE = space(10)
      .w_GOFORI = 0
      .w_SERIALPOST = space(10)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
          .DoRTCalc(1,3,.f.)
        .w_SERIALPOST = .w_SERIALANT
      endif
    endwith
    cp_BlankRecExtFlds(this,'Gomme')
    this.DoRTCalc(5,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.Gomme_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.Gomme_IDX,2])
    cp_AskTableProg(this,i_nConn,"SERGOMME","w_SERIALANT")
    with this
      .op_SERIALANT = .w_SERIALANT
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSERIALANT_1_1.enabled = i_bVal
      .Page1.oPag.oGOMISURE_1_2.enabled = i_bVal
      .Page1.oPag.oGOFORI_1_3.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSERIALANT_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSERIALANT_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'Gomme',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.Gomme_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SERIALANT,"SERIALANT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GOMISURE,"GOMISURE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GOFORI,"GOFORI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SERIALPOST,"SERIALPOST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.Gomme_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.Gomme_IDX,2])
    i_lTable = "Gomme"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.Gomme_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.Gomme_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.Gomme_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.Gomme_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SERGOMME","w_SERIALANT")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into Gomme
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'Gomme')
        i_extval=cp_InsertValODBCExtFlds(this,'Gomme')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SERIALANT,GOMISURE,GOFORI,SERIALPOST,UTCC"+;
                  ",UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_SERIALANT)+;
                  ","+cp_ToStrODBC(this.w_GOMISURE)+;
                  ","+cp_ToStrODBC(this.w_GOFORI)+;
                  ","+cp_ToStrODBC(this.w_SERIALPOST)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'Gomme')
        i_extval=cp_InsertValVFPExtFlds(this,'Gomme')
        cp_CheckDeletedKey(i_cTable,0,'SERIALANT',this.w_SERIALANT)
        INSERT INTO (i_cTable);
              (SERIALANT,GOMISURE,GOFORI,SERIALPOST,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SERIALANT;
                  ,this.w_GOMISURE;
                  ,this.w_GOFORI;
                  ,this.w_SERIALPOST;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.Gomme_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.Gomme_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.Gomme_IDX,i_nConn)
      *
      * update Gomme
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'Gomme')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " GOMISURE="+cp_ToStrODBC(this.w_GOMISURE)+;
             ",GOFORI="+cp_ToStrODBC(this.w_GOFORI)+;
             ",SERIALPOST="+cp_ToStrODBC(this.w_SERIALPOST)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'Gomme')
        i_cWhere = cp_PKFox(i_cTable  ,'SERIALANT',this.w_SERIALANT  )
        UPDATE (i_cTable) SET;
              GOMISURE=this.w_GOMISURE;
             ,GOFORI=this.w_GOFORI;
             ,SERIALPOST=this.w_SERIALPOST;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.Gomme_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.Gomme_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.Gomme_IDX,i_nConn)
      *
      * delete Gomme
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SERIALANT',this.w_SERIALANT  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.Gomme_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.Gomme_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_SERIALANT<>.w_SERIALANT
            .w_SERIALPOST = .w_SERIALANT
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSERIALANT_1_1.value==this.w_SERIALANT)
      this.oPgFrm.Page1.oPag.oSERIALANT_1_1.value=this.w_SERIALANT
    endif
    if not(this.oPgFrm.Page1.oPag.oGOMISURE_1_2.value==this.w_GOMISURE)
      this.oPgFrm.Page1.oPag.oGOMISURE_1_2.value=this.w_GOMISURE
    endif
    if not(this.oPgFrm.Page1.oPag.oGOFORI_1_3.value==this.w_GOFORI)
      this.oPgFrm.Page1.oPag.oGOFORI_1_3.value=this.w_GOFORI
    endif
    cp_SetControlsValueExtFlds(this,'Gomme')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- anag_mag
      IF this.cFunction='Load'
          this.w_UTCC=i_codute
          this.w_UTDC=datetime()
          this.w_serialpost=this.w_serialant
      ENDIF
      IF this.cFunction='Edit'
          this.w_UTCV=i_codute
          this.w_UTDV=datetime()
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SERIALANT = this.w_SERIALANT
    return

enddefine

* --- Define pages as container
define class tanag_magPag1 as StdContainer
  Width  = 435
  height = 173
  stdWidth  = 435
  stdheight = 173
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSERIALANT_1_1 as StdField with uid="MCYOAWPPXF",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SERIALANT", cQueryName = "SERIALANT",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 143935582,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=153, Top=25, InputMask=replicate('X',10)

  add object oGOMISURE_1_2 as StdField with uid="OMASKLCAZQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GOMISURE", cQueryName = "GOMISURE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 45349973,;
   bGlobalFont=.t.,;
    Height=21, Width=274, Left=153, Top=52, InputMask=replicate('X',10)

  add object oGOFORI_1_3 as StdField with uid="PPOFPRFLIR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_GOFORI", cQueryName = "GOFORI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 111774736,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=153, Top=75, cSayPict='"99"', cGetPict='"99"'

  add object oStr_1_4 as StdString with uid="AHUWSAIUJF",Visible=.t., Left=110, Top=29,;
    Alignment=1, Width=42, Height=18,;
    Caption="Seriale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="SAWUSMCFRP",Visible=.t., Left=20, Top=79,;
    Alignment=1, Width=132, Height=18,;
    Caption="Numero Fori Cerchione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="XDCOLJAGSN",Visible=.t., Left=64, Top=56,;
    Alignment=1, Width=88, Height=18,;
    Caption="Misure Gomma:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('anag_mag','Gomme','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SERIALANT=Gomme.SERIALANT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
