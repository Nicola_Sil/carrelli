private dy,dx,bmp
dy = iif(i_vidgrf,3,0)
dx = iif(i_vidgrf,wcols()-80,0)
bmp= "prog.bmp"
if i_vidgrf .and. file(bmp)
  *activate screen
  *@ 0,0 say (bmp) bitmap size wrows(),wcols() center
  _screen.icon=i_cStdIcon
  _screen.caption = i_msgtitle

  _screen.AddObject("oImgLogo","screen_image")
  _screen.oImgLogo.picture = bmp

  _screen.oImgLogo.top = _screen.height - (_screen.oImgLogo.Height) - 100
  _screen.oImgLogo.left = (_screen.width - _screen.oImgLogo.width)/2

  _screen.oImgLogo.Visible = .T.
else
  @ 4+dy,32+dx say " Carrelli" font "Courier New",42 style "B"
  @ 10+dy,32+dx say "                        "
  @ 12+dy,32+dx say " Author  : Nicola Esti"
  @ 13+dy,32+dx say " Client  : SIL"
  @ 14+dy,32+dx say " Version : 1.0 - 2016-08-03"
  @ 2+dy,31+dx to 15+dy,79+dx double
  @ 11+dy,32+dx to 11+dy,78+dx
endif
if type("_screen")="O"
  _screen.caption="Carrelli"
endif
