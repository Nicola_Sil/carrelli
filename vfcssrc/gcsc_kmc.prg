* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: gcsc_kmc                                                        *
*              Stampe                                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-09-29                                                      *
* Last revis.: 2016-03-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgcsc_kmc",oParentObject))

* --- Class definition
define class tgcsc_kmc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 495
  Height = 171
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-03-31"
  HelpContextID=199906921
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "gcsc_kmc"
  cComment = "Stampe"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPO = space(1)
  w_MOTORE = space(10)
  w_DADATA = ctod('  /  /  ')
  w_ADATA = ctod('  /  /  ')
  w_STAMPA = space(2)
  w_OBSOTEST = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgcsc_kmcPag1","gcsc_kmc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPO=space(1)
      .w_MOTORE=space(10)
      .w_DADATA=ctod("  /  /  ")
      .w_ADATA=ctod("  /  /  ")
      .w_STAMPA=space(2)
      .w_OBSOTEST=ctod("  /  /  ")
        .w_TIPO = 'C'
        .w_MOTORE = ''
        .w_DADATA = i_datsys
        .w_ADATA = i_datsys
        .w_STAMPA = '2'
        .w_OBSOTEST = i_datsys
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMOTORE_1_2.enabled = this.oPgFrm.Page1.oPag.oMOTORE_1_2.mCond()
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMOTORE_1_2.visible=!this.oPgFrm.Page1.oPag.oMOTORE_1_2.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPO_1_1.RadioValue()==this.w_TIPO)
      this.oPgFrm.Page1.oPag.oTIPO_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOTORE_1_2.RadioValue()==this.w_MOTORE)
      this.oPgFrm.Page1.oPag.oMOTORE_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDADATA_1_3.value==this.w_DADATA)
      this.oPgFrm.Page1.oPag.oDADATA_1_3.value=this.w_DADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oADATA_1_4.value==this.w_ADATA)
      this.oPgFrm.Page1.oPag.oADATA_1_4.value=this.w_ADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAMPA_1_5.RadioValue()==this.w_STAMPA)
      this.oPgFrm.Page1.oPag.oSTAMPA_1_5.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgcsc_kmcPag1 as StdContainer
  Width  = 491
  height = 171
  stdWidth  = 491
  stdheight = 171
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPO_1_1 as StdCombo with uid="ALJCFDXFHI",rtseq=1,rtrep=.f.,left=82,top=17,width=100,height=22;
    , HelpContextID = 194381770;
    , cFormVar="w_TIPO",RowSource=""+"Carrelli,"+"Batterie,"+"Mezzi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPO_1_1.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'B',;
    iif(this.value =3,'M',;
    space(1)))))
  endfunc
  func oTIPO_1_1.GetRadio()
    this.Parent.oContained.w_TIPO = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_1_1.SetRadio()
    this.Parent.oContained.w_TIPO=trim(this.Parent.oContained.w_TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO=='C',1,;
      iif(this.Parent.oContained.w_TIPO=='B',2,;
      iif(this.Parent.oContained.w_TIPO=='M',3,;
      0)))
  endfunc


  add object oMOTORE_1_2 as StdCombo with uid="PYDQQWYSOB",value=3,rtseq=2,rtrep=.f.,left=278,top=17,width=100,height=22;
    , HelpContextID = 24494650;
    , cFormVar="w_MOTORE",RowSource=""+"Diesel,"+"Elettrico,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOTORE_1_2.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'E',;
    iif(this.value =3,'',;
    space(10)))))
  endfunc
  func oMOTORE_1_2.GetRadio()
    this.Parent.oContained.w_MOTORE = this.RadioValue()
    return .t.
  endfunc

  func oMOTORE_1_2.SetRadio()
    this.Parent.oContained.w_MOTORE=trim(this.Parent.oContained.w_MOTORE)
    this.value = ;
      iif(this.Parent.oContained.w_MOTORE=='D',1,;
      iif(this.Parent.oContained.w_MOTORE=='E',2,;
      iif(this.Parent.oContained.w_MOTORE=='',3,;
      0)))
  endfunc

  func oMOTORE_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPO='C')
    endwith
   endif
  endfunc

  func oMOTORE_1_2.mHide()
    with this.Parent.oContained
      return (.w_TIPO<>'C')
    endwith
  endfunc

  add object oDADATA_1_3 as StdField with uid="UDTVIUCISI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DADATA", cQueryName = "DADATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 90493130,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=82, Top=54

  add object oADATA_1_4 as StdField with uid="RURQYUHZRQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ADATA", cQueryName = "ADATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 125959674,;
   bGlobalFont=.t.,;
    Height=21, Width=108, Left=278, Top=54


  add object oSTAMPA_1_5 as StdCombo with uid="APIMCOVHLW",rtseq=5,rtrep=.f.,left=12,top=100,width=460,height=22;
    , HelpContextID = 93908186;
    , cFormVar="w_STAMPA",RowSource=""+"RIEPILOGO MANUTENZIONI,"+"STAMPA ANAGRAFICHE,"+"STAMPA TAGLIANDI", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTAMPA_1_5.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    space(2)))))
  endfunc
  func oSTAMPA_1_5.GetRadio()
    this.Parent.oContained.w_STAMPA = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPA_1_5.SetRadio()
    this.Parent.oContained.w_STAMPA=trim(this.Parent.oContained.w_STAMPA)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPA=='1',1,;
      iif(this.Parent.oContained.w_STAMPA=='2',2,;
      iif(this.Parent.oContained.w_STAMPA=='3',3,;
      0)))
  endfunc


  add object oBtn_1_6 as StdButton with uid="ZFEJEXWVBQ",left=303, top=132, width=81,height=25,;
    caption="Stampa", nPag=1;
    , HelpContextID = 210318118;
  , bGlobalFont=.t.

    proc oBtn_1_6.Click()
      with this.Parent.oContained
        do GCSC_BMC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_7 as StdButton with uid="GZHXKPASUD",left=388, top=132, width=81,height=25,;
    caption="Annulla", nPag=1;
    , ToolTipText = "Premere per annullare la stampa";
    , HelpContextID = 122813446;
    , caption='\<Annulla';
  , bGlobalFont=.t.

    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_8 as StdString with uid="PQGWPKVMRS",Visible=.t., Left=29, Top=58,;
    Alignment=1, Width=48, Height=18,;
    Caption="Da Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="WTYLZPYYDS",Visible=.t., Left=226, Top=58,;
    Alignment=1, Width=48, Height=18,;
    Caption="Da Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="MSQIIPLUWM",Visible=.t., Left=206, Top=21,;
    Alignment=1, Width=67, Height=18,;
    Caption="Tipo Motore:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_TIPO<>'C')
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="ALNKTCLWAX",Visible=.t., Left=14, Top=21,;
    Alignment=1, Width=63, Height=18,;
    Caption="Tipo Mezzo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gcsc_kmc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
