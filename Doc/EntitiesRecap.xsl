<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output method="html" version="4.0" encoding="ISO-8859-1"/>

<xsl:param name="default_extension" select="'.xml'"/>  <!-- usato nei link del tag Plan -->

<xsl:include href="common.xsl"/>


<xsl:template match="/">
  <xsl:apply-templates select="Plan"/>
</xsl:template>


<!-- Nodo principale -->

<xsl:template match="Plan">
  <HTML>
  <HEAD>
    <STYLE>
      BODY {font-family:Times New Roman; color:black; background:white; margin:20px}
      TABLE {font-family:Times New Roman; color:black; background:white}
      A:hover {color:red}
    </STYLE>
  </HEAD>
  <BODY>
  <P ALIGN="CENTER"><FONT COLOR="#000080" SIZE="6"><B>Entities summary</B></FONT></P>
  <P ALIGN="CENTER">
    <TABLE BORDER="1" CELLPADDING="4" CELLSPACING="0">
      <TR BGCOLOR="#6495ED">
        <TD><FONT COLOR="WHITE"><B>Entity</B></FONT></TD>
        <TD><FONT COLOR="WHITE"><B>Class</B></FONT></TD>
        <TD><FONT COLOR="WHITE"><B>Program</B></FONT></TD>
        <TD><FONT COLOR="WHITE"><B>Created</B></FONT></TD>
        <TD><FONT COLOR="WHITE"><B>Group</B></FONT></TD>
      </TR>
      <xsl:for-each select="PlanItemlist/PlanItem[ItemObj!='String']">
        <xsl:sort select="ItemGroup"/>
        <xsl:sort select="ItemObj"/>
        <xsl:sort select="ItemName"/>
        <xsl:variable name="link">
          <xsl:choose>
            <xsl:when test="ItemObj='Group'">Diagrams</xsl:when>
            <xsl:otherwise>Entities</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:variable name="group">
          <xsl:choose>
            <xsl:when test="ItemGroup=''">TOP-LEVEL</xsl:when>
            <xsl:otherwise><xsl:value-of select="ItemGroup"/></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <TR BGCOLOR="#F4FBFF">
          <TD>
            <A HREF="{/Plan/PlanName}_{$link}{$default_extension}#{ItemName}">
              <xsl:value-of select="ItemName"/>
            </A>
          </TD>
          <TD><xsl:value-of select="ItemObj"/></TD>
          <TD><xsl:apply-templates select="ItemProcname"/></TD>
          <TD><xsl:apply-templates select="ItemDatecreate"/></TD>
          <TD>
            <A HREF="{/Plan/PlanName}_Diagrams{$default_extension}#{$group}">
              <xsl:value-of select="$group"/>
            </A>
          </TD>
        </TR>
      </xsl:for-each>
    </TABLE>
  </P>
  <TABLE BORDER="0" WIDTH="100%" CELLSPACING="0">
    <TR>
      <TD ALIGN="CENTER" WIDTH="50%">
        <A HREF="{PlanName}_Main{$default_extension}">Project overview</A>
      </TD>
      <TD ALIGN="CENTER" WIDTH="50%">
        <A HREF="{PlanName}_Diagrams{$default_extension}">Diagrams</A>
      </TD>
    </TR>
  </TABLE>
  </BODY>
  </HTML>
</xsl:template>


<!-- Gestione celle vuote -->

<xsl:template match="ItemProcname | ItemDatecreate">
  <xsl:call-template name="ValueOrNotAvailable">
    <xsl:with-param name="NA" select="'&#160;'"/>
  </xsl:call-template>
</xsl:template>


</xsl:stylesheet>
