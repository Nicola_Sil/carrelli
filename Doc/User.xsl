<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output method="html" version="4.0" encoding="ISO-8859-1"/>

<xsl:param name="default_extension" select="'.xml'"/>  <!-- usato nel tag File -->

<xsl:include href="common.xsl"/>


<xsl:template match="/">
  <xsl:apply-templates select="Codify"/>
</xsl:template>


<!-- Nodo principale -->

<xsl:template match="Codify">
  <HTML>
  <HEAD><STYLE>
    BODY  {font-family:Times New Roman; color:black; background:white; margin:20px}
    TABLE {font-family:Times New Roman; color:black; background:white}
    A:hover {color:red;}
  </STYLE></HEAD>
  <BODY BGCOLOR="#FFFFFF">
  <A NAME="top-of-file"/>
  <P ALIGN="CENTER">
    <FONT COLOR="#000080" SIZE="6"><B><xsl:value-of select="Title"/></B></FONT>
  </P>
  <TABLE ALIGN="CENTER" BORDER="0" CELLPADDING="4" CELLSPACING="0">
    <xsl:if test="Note!=''">
      <TR><TD COLSPAN="8" xsl:use-attribute-sets="global-notes">
        <xsl:apply-templates select="Note"/>
      </TD></TR>
    </xsl:if>
    <TR><TD COLSPAN="8">
      <DL>
        <xsl:apply-templates select="Include/File"/>
      </DL>
    </TD></TR>
  </TABLE>

  <TABLE BORDER="0" ALIGN="CENTER" WIDTH="100%" CELLPADDING="4" CELLSPACING="0">
    <TR><TD COLSPAN="8" HEIGHT="30"></TD></TR>   <!-- RIGA SPAZIATRICE -->
    <xsl:for-each select="Pag">
      <xsl:if test="position() mod 3 = 1">
        <TR>
          <xsl:call-template name="PageIndex">
            <xsl:with-param name="page" select="."/>
            <xsl:with-param name="next" select="count(following-sibling::Pag[position() &lt;= 2])"/>
          </xsl:call-template>
        </TR>
      </xsl:if>
    </xsl:for-each>
  </TABLE>
  <xsl:apply-templates select="Pag"/>
  </BODY>
  </HTML>
</xsl:template>


<!-- Gestione del tag "File": link a un file XML o HTML -->

<xsl:template match="File">
  <xsl:variable name="link" select="concat(substring-before(@src,'.xml'),$default_extension)"/>
  <DT STYLE="margin-top:10px">
  <A HREF="{$link}">
     <xsl:choose>
       <xsl:when test="Title!=''"><xsl:value-of select="Title"/></xsl:when>
       <xsl:otherwise><xsl:value-of select="$link"/></xsl:otherwise>
     </xsl:choose>
  </A>
  <xsl:if test="Note!=''">
    <DD><xsl:apply-templates select="Note"/></DD>
  </xsl:if>
  </DT>
</xsl:template>


<!-- Gestione dell'immagine delle maschere e delle aree linkate ai vari oggetti contenuti -->

<xsl:template match="Pag">
  <P>
  <TABLE BORDER="0" WIDTH="100%" ALIGN="CENTER">
    <TR><TD BGCOLOR="#B0C4DE" ALIGN="CENTER">
      <FONT COLOR="#000080" SIZE="4">
        <A NAME="{Title/@LinkID}">
          <xsl:call-template name="PageTitle">
            <xsl:with-param name="page" select="."/>
          </xsl:call-template>
        </A>
      </FONT>
    </TD></TR>
    <TR><TD>&#160;</TD></TR>

    <xsl:if test="Note!=''">
      <TR><TD>
        <TABLE ALIGN="CENTER" WIDTH="80%" BORDER="0" CELLPADDING="0" CELLSPACING="0">
          <TR><TD xsl:use-attribute-sets="page-notes">
            <xsl:apply-templates select="Note"/>
          </TD></TR>
        </TABLE>
      </TD></TR>
      <TR><TD>&#160;</TD></TR>
    </xsl:if>

    <TR><TD>
      <TABLE BORDER="0" ALIGN="CENTER">
        <TR><TD ALIGN="CENTER">
          <xsl:variable name="pag_num" select="@number"/>
          <xsl:variable name="x" select="JPG/@x"/>
          <xsl:variable name="y" select="JPG/@y"/>
          <MAP NAME="Pag{$pag_num}">
            <xsl:for-each select="Item">
              <AREA HREF="#{Name/@LinkID}" SHAPE="RECT" TITLE="Go to '{Name}'"
                    COORDS="{@x1 - $x},{@y1 - $y},{@x2 - $x},{@y2 - $y}"/>
            </xsl:for-each>
          </MAP>
          <IMG BORDER="0" SRC="{JPG}.jpg" ALIGN="CENTER" VALIGN="MIDDLE" USEMAP="#Pag{$pag_num}"/>
        </TD></TR>
        <TR><TD>&#160;</TD></TR>
        <TR><TD>
          <xsl:apply-templates select="Item"/>
        </TD></TR>
      </TABLE>
    </TD></TR>
    <TR><TD ALIGN="RIGHT">
      <A HREF="#top-of-file">Go to top</A>
    </TD></TR>
  </TABLE>
  </P>
</xsl:template>


<!-- ************ Gestione di tutti gli oggetti nell'ordine in cui si presentano nel file XML ****************  -->

<xsl:template match="Item">
  <TABLE BORDER="0">
    <TR><TD>
      <TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0">
        <THEAD><TR><TD ALIGN="LEFT"><TABLE><TR>
          <TD><A NAME="{Name/@LinkID}"><B><FONT COLOR="BLACK">
            <xsl:choose>
              <xsl:when test="Comment!=''"><xsl:value-of select="Comment"/></xsl:when>
              <xsl:when test="Name!=''"><xsl:value-of select="Name"/></xsl:when>
              <xsl:when test="@type='stringa'">Empty string with Hyperlink property</xsl:when>
              <xsl:when test="@type='espressione'">Empty expression with Hyperlink property</xsl:when>
            </xsl:choose>
          </FONT></B></A></TD>
          <xsl:if test="Key='key1'">
            <TD VALIGN="MIDDLE" WIDTH="30"><IMG SRC="Key.gif" TITLE="Primary key"/></TD>
          </xsl:if>
          <xsl:if test="Zoom">
            <TD VALIGN="MIDDLE" WIDTH="30"><IMG SRC="Zoom.gif" TITLE="{Zoom} zoom"/></TD>
          </xsl:if>
          <xsl:if test="Obligatory='TRUE'">
            <TD VALIGN="MIDDLE" WIDTH="30"><IMG SRC="Stop.gif" TITLE="Obligatory"/></TD>
          </xsl:if>
        </TR></TABLE></TD></TR></THEAD>
        <TR>
          <TD xsl:use-attribute-sets="first-top-table"><FONT COLOR="WHITE"><B>Notes</B></FONT></TD>
          <xsl:if test="Class!=''">
            <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE"><B>Class</B></FONT></TD>
          </xsl:if>
          <xsl:if test="Bitmap!=''">
            <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE"><B>Bitmap</B></FONT></TD>
          </xsl:if>
          <xsl:if test="Help!=''">
            <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE"><B>Tooltip</B></FONT></TD>
          </xsl:if>
          <xsl:if test="Hyperlink!=''">
            <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE"><B>Hyperlink</B></FONT></TD>
          </xsl:if>
        </TR>
        <TR>
          <TD xsl:use-attribute-sets="first-bottom-table">
            <xsl:apply-templates select="Note"/>
          </TD>
          <xsl:if test="Class!=''">
            <TD xsl:use-attribute-sets="bottom-table">
              <xsl:value-of select="Class"/>
            </TD>
          </xsl:if>
          <xsl:if test="Bitmap!=''">
            <TD xsl:use-attribute-sets="bottom-table">
              <xsl:value-of select="Bitmap"/>
            </TD>
          </xsl:if>
          <xsl:if test="Help!=''">
            <TD xsl:use-attribute-sets="bottom-table">
              <xsl:value-of select="Help"/>
            </TD>
          </xsl:if>
          <xsl:if test="Hyperlink!=''">
            <TD xsl:use-attribute-sets="bottom-table">
              <xsl:value-of select="Hyperlink"/>
            </TD>
          </xsl:if>
        </TR>
      </TABLE>
    </TD></TR>
    <TR><TD ROWSPAN="2" ALIGN="LEFT" VALIGN="MIDDLE">
      <A HREF="#{../Title/@LinkID}">Go to <xsl:call-template name="PageTitle"/></A>
    </TD></TR>
  </TABLE>
</xsl:template>


<!-- Gestione celle vuote -->

<xsl:template match="Item/Note">
  <xsl:call-template name="ValueOrNotAvailable"/>
</xsl:template>


</xsl:stylesheet>
