<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output method="html" version="4.0" encoding="ISO-8859-1"/>

<xsl:include href="common.xsl"/>


<xsl:template match="/">
  <xsl:apply-templates select="Codify"/>
</xsl:template>


<!-- Nodo principale -->

<xsl:template match="Codify">
  <HTML>
  <BODY BGCOLOR="#FFFFFF">
  <A NAME="top-of-file"/>
  <P ALIGN="CENTER">
    <FONT COLOR="#000080" SIZE="6"><B><xsl:value-of select="Title"/></B></FONT>
  </P>
  <TABLE ALIGN="CENTER" BORDER="0" CELLPADDING="4" CELLSPACING="0">
    <xsl:if test="Note!=''">
      <TR><TD COLSPAN="8" xsl:use-attribute-sets="global-notes">
        <xsl:apply-templates select="Note"/>
      </TD></TR>
    </xsl:if>
    <TR><TD COLSPAN="8" HEIGHT="30"></TD></TR>               <!-- RIGA SPAZIATRICE -->
    <TR>
      <TD xsl:use-attribute-sets="first-top-table"><FONT COLOR="WHITE" SIZE="3"><B>Name</B></FONT></TD>
      <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Title</B></FONT></TD>
      <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Template</B></FONT></TD>
      <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Author</B></FONT></TD>
      <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>User</B></FONT></TD>
      <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Version</B></FONT></TD>
      <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Created</B></FONT></TD>
      <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Revised</B></FONT></TD>
    </TR>
    <TR>
      <TD xsl:use-attribute-sets="first-bottom-table">
        <xsl:apply-templates select="Name"/>
      </TD>
      <TD xsl:use-attribute-sets="bottom-table">
        <xsl:apply-templates select="Title"/>
      </TD>
      <TD xsl:use-attribute-sets="bottom-table">
        <xsl:apply-templates select="Template"/>
      </TD>
      <TD xsl:use-attribute-sets="bottom-table">
        <xsl:apply-templates select="Author"/>
      </TD>
      <TD xsl:use-attribute-sets="bottom-table">
        <xsl:apply-templates select="User"/>
      </TD>
      <TD xsl:use-attribute-sets="bottom-table">
        <xsl:apply-templates select="Version"/>
      </TD>
      <TD xsl:use-attribute-sets="bottom-table">
        <xsl:apply-templates select="Created"/>
      </TD>
      <TD xsl:use-attribute-sets="bottom-table">
        <xsl:apply-templates select="Lastrevision"/>
      </TD>
    </TR>

    <xsl:if test="AtEndAction/EntityName!=''">
      <TR><TD COLSPAN="8" HEIGHT="10"></TD></TR>   <!-- RIGA SPAZIATRICE -->
      <TR><TD ALIGN="LEFT" COLSPAN="8">
        <FONT COLOR="DARKBLUE" SIZE="4"><B>&quot;At end&quot; Action</B></FONT>
      </TD></TR>
      <TR>
        <TD xsl:use-attribute-sets="first-top-table"><FONT COLOR="WHITE" SIZE="3"><B>Entity</B></FONT></TD>
        <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Type</B></FONT></TD>
        <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Open On</B></FONT></TD>
        <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Open Mode</B></FONT></TD>
        <xsl:if test="AtEndAction/EntityParam!=''">
          <TD xsl:use-attribute-sets="top-table" COLSPAN="4"><FONT COLOR="WHITE" SIZE="3"><B>Parameters</B></FONT></TD>
        </xsl:if>
      </TR>
      <TR>
        <TD xsl:use-attribute-sets="first-bottom-table">
          <FONT SIZE="2"><xsl:value-of select="AtEndAction/EntityName"/></FONT>
        </TD>
        <TD xsl:use-attribute-sets="bottom-table">
          <FONT SIZE="2"><xsl:value-of select="AtEndAction/EntityType"/></FONT>
        </TD>
        <TD xsl:use-attribute-sets="bottom-table">
          <FONT SIZE="2"><xsl:value-of select="AtEndAction/EntityOpenOn"/></FONT>
        </TD>
        <TD xsl:use-attribute-sets="bottom-table">
          <FONT SIZE="2"><xsl:value-of select="AtEndAction/EntityOpenMode"/></FONT>
        </TD>
        <xsl:if test="AtEndAction/EntityParam!=''">
          <TD xsl:use-attribute-sets="bottom-table" COLSPAN="4"><FONT SIZE="2">
            <xsl:for-each select="AtEndAction/EntityParam">
              <xsl:value-of select="."/>
              <xsl:if test="position() &lt; last()">; </xsl:if>
            </xsl:for-each>
          </FONT></TD>
        </xsl:if>
      </TR>
    </xsl:if>
  </TABLE>

  <TABLE BORDER="0" ALIGN="CENTER" WIDTH="100%" CELLPADDING="4" CELLSPACING="0">
    <TR><TD COLSPAN="8" HEIGHT="30"></TD></TR>   <!-- RIGA SPAZIATRICE -->
    <xsl:for-each select="Pag">
      <xsl:if test="position() mod 3 = 1">
        <TR>
          <xsl:call-template name="PageIndex">
            <xsl:with-param name="page" select="."/>
            <xsl:with-param name="next" select="count(following-sibling::Pag[position() &lt;= 2])"/>
          </xsl:call-template>
        </TR>
      </xsl:if>
    </xsl:for-each>
  </TABLE>
  <xsl:apply-templates select="Pag"/>
  </BODY>
  </HTML>
</xsl:template>


<!-- Gestione dell'immagine delle maschere e delle aree linkate ai vari oggetti contenuti -->

<xsl:template match="Pag">
  <P>
  <TABLE BORDER="0" WIDTH="100%" ALIGN="CENTER">
    <TR><TD BGCOLOR="#B0C4DE" ALIGN="CENTER">
      <FONT COLOR="#000080" SIZE="4">
        <A NAME="{Title/@LinkID}">
          <xsl:call-template name="PageTitle">
            <xsl:with-param name="page" select="."/>
          </xsl:call-template>
        </A>
      </FONT>
    </TD></TR>
    <TR><TD>&#160;</TD></TR>

    <xsl:if test="Note!=''">
      <TR><TD>
        <TABLE ALIGN="CENTER" WIDTH="80%" BORDER="0" CELLPADDING="0" CELLSPACING="0">
          <TR><TD xsl:use-attribute-sets="page-notes">
            <xsl:apply-templates select="Note"/>
          </TD></TR>
        </TABLE>
      </TD></TR>
      <TR><TD>&#160;</TD></TR>
    </xsl:if>

    <TR><TD>
      <TABLE BORDER="0" ALIGN="CENTER">
        <TR><TD ALIGN="CENTER">
          <xsl:variable name="pag_num" select="@number"/>
          <xsl:variable name="x" select="JPG/@x"/>
          <xsl:variable name="y" select="JPG/@y"/>
          <MAP NAME="Pag{$pag_num}">
            <xsl:for-each select="Item">
              <AREA HREF="#{Name/@LinkID}" SHAPE="RECT" TITLE="Go to '{Name}'"
                    COORDS="{@x1 - $x},{@y1 - $y},{@x2 - $x},{@y2 - $y}"/>
            </xsl:for-each>
          </MAP>
          <IMG BORDER="0" SRC="{JPG}.jpg" ALIGN="CENTER" VALIGN="MIDDLE" USEMAP="#Pag{$pag_num}"/>
        </TD></TR>
        <TR><TD>&#160;</TD></TR>
        <TR><TD>
          <xsl:apply-templates select="Item"/>
        </TD></TR>
      </TABLE>
    </TD></TR>
    <TR><TD ALIGN="RIGHT">
      <A HREF="#top-of-file">Go to top</A>
    </TD></TR>
  </TABLE>
  </P>
</xsl:template>


<!-- Gestione degli oggetti di tipo 'campo' o 'variabile' -->

<xsl:template match="Item[@type='campo' or @type='variabile']">
  <TABLE BORDER="0">
    <TR><TD>
      <TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0">
        <THEAD><TR><TD ALIGN="LEFT" COLSPAN="5">
          <A NAME="{Name/@LinkID}"><B><xsl:value-of select="Name"/></B></A>
        </TD></TR></THEAD>
        <TR>
          <TD xsl:use-attribute-sets="first-top-table"><FONT COLOR="WHITE" SIZE="3"><B>Type</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Comment</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Notes</B></FONT></TD>
          <xsl:if test="Hyperlink!=''">
            <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Hyperlink</B></FONT></TD>
          </xsl:if>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Help</B></FONT></TD>
        </TR>
        <TR>
          <TD xsl:use-attribute-sets="first-bottom-table">
            <FONT COLOR="RED" SIZE="2">
              <xsl:choose>
                <xsl:when test="@type='variabile'">Variable</xsl:when>
                <xsl:when test="@type='campo'">Field</xsl:when>
              </xsl:choose>
            </FONT>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table">
            <xsl:apply-templates select="Comment"/>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table" ALIGN="LEFT">
            <xsl:apply-templates select="Note"/>
          </TD>
          <!--** Solo i Field Writer hanno la possibilita di Hyperlink (Update Row Action) **-->
          <xsl:if test="Hyperlink!=''">
            <TD xsl:use-attribute-sets="bottom-table">
              <FONT SIZE="2"><xsl:value-of select="Hyperlink"/></FONT>
            </TD>
          </xsl:if>
          <TD xsl:use-attribute-sets="bottom-table">
            <xsl:apply-templates select="Help"/>
          </TD>
        </TR>
      </TABLE>
    </TD></TR>
    <TR><TD ROWSPAN="2" ALIGN="LEFT" VALIGN="MIDDLE">
      <A HREF="#{../Title/@LinkID}">
        <FONT SIZE="2">Go to <xsl:call-template name="PageTitle"/></FONT>
      </A>
    </TD></TR>
  </TABLE>
</xsl:template>


<!-- Gestione degli oggetti di tipo 'stringa' o 'espressione' -->

<xsl:template match="Item[@type='stringa' or @type='espressione']">
  <TABLE BORDER="0">
    <TR><TD>
      <TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0">
        <THEAD><TR><TD ALIGN="LEFT" COLSPAN="3">
          <A NAME="{Name/@LinkID}"><B>
            <xsl:choose>
              <xsl:when test="Name!=''"><xsl:value-of select="Name"/></xsl:when>
              <xsl:when test="@type='stringa'">Empty string with Hyperlink property</xsl:when>
              <xsl:when test="@type='espressione'">Empty expression with Hyperlink property</xsl:when>
            </xsl:choose>
          </B></A>
        </TD></TR></THEAD>
        <TR>
          <TD xsl:use-attribute-sets="first-top-table"><FONT COLOR="WHITE" SIZE="3"><B>Type</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Hyperlink</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Open On</B></FONT></TD>
        </TR>
        <TR>
          <TD xsl:use-attribute-sets="first-bottom-table">
            <FONT COLOR="RED" SIZE="2">
              <xsl:choose>
              <xsl:when test="@type='stringa'">String</xsl:when>
              <xsl:when test="@type='espressione'">Expression</xsl:when>
              </xsl:choose>
            </FONT>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table">
            <xsl:apply-templates select="Hyperlink"/>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table">
            <FONT SIZE="2">
              <xsl:value-of select="HyperlinkOpenOn"/>
            </FONT>
          </TD>
        </TR>
      </TABLE>
    </TD></TR>
    <TR><TD ROWSPAN="2" ALIGN="LEFT" VALIGN="MIDDLE">
      <A HREF="#{../Title/@LinkID}">
        <FONT SIZE="2">Go to <xsl:call-template name="PageTitle"/></FONT>
      </A>
    </TD></TR>
  </TABLE>
</xsl:template>


<!-- Gestione degli oggetti di tipo 'oggetto' -->

<xsl:template match="Item[@type='oggetto']">
  <TABLE BORDER="0">
    <TR><TD>
      <TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0">
        <THEAD><TR><TD ALIGN="LEFT" COLSPAN="7">
          <A NAME="{Name/@LinkID}"><B><xsl:value-of select="Name"/></B></A>
        </TD></TR></THEAD>
        <TR>
          <TD xsl:use-attribute-sets="first-top-table"><FONT COLOR="WHITE" SIZE="3"><B>Class</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Comment</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Bitmap</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Properties</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Ref</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Notes</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Help</B></FONT></TD>
        </TR>
        <TR>
          <TD xsl:use-attribute-sets="first-bottom-table">
            <xsl:apply-templates select="Class"/>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table">
            <xsl:apply-templates select="Comment"/>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table">
            <xsl:apply-templates select="Bitmap"/>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table">
            <xsl:apply-templates select="Property"/>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table">
            <xsl:apply-templates select="Ref"/>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table" ALIGN="LEFT">
            <xsl:apply-templates select="Note"/>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table">
            <xsl:apply-templates select="Help"/>
          </TD>
        </TR>
      </TABLE>
    </TD></TR>
    <TR><TD ROWSPAN="2" ALIGN="LEFT" VALIGN="MIDDLE">
      <A HREF="#{../Title/@LinkID}">
        <FONT SIZE="2">Go to <xsl:call-template name="PageTitle"/></FONT>
      </A>
    </TD></TR>
  </TABLE>
</xsl:template>


<!-- Gestione degli oggetti di tipo 'Link_pc' -->

<xsl:template match="Item[@type='link_pc']">
  <TABLE BORDER="0">
    <TR><TD>
      <TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0">
        <THEAD><TR><TD ALIGN="LEFT" COLSPAN="4">
          <A NAME="{Name/@LinkID}"><B><xsl:value-of select="Name"/></B></A>
        </TD></TR></THEAD>
        <TR>
          <TD xsl:use-attribute-sets="first-top-table"><FONT COLOR="WHITE" SIZE="3"><B>Edit</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Program</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Notes</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Help</B></FONT></TD>
        </TR>
        <TR>
          <TD xsl:use-attribute-sets="first-bottom-table">
            <xsl:choose>
              <xsl:when test="Edit='E'">Edit</xsl:when>
              <xsl:when test="Edit='P'">Paint</xsl:when>
              <xsl:when test="Edit='N'">Hide/No</xsl:when>
            </xsl:choose>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table">
            <xsl:apply-templates select="Program"/>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table" ALIGN="LEFT">
            <xsl:apply-templates select="Note"/>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table">
            <xsl:apply-templates select="Help"/>
          </TD>
        </TR>
      </TABLE>
    </TD></TR>
    <TR><TD ROWSPAN="2" ALIGN="LEFT" VALIGN="MIDDLE">
      <A HREF="#{../Title/@LinkID}">
        <FONT SIZE="2">Go to <xsl:call-template name="PageTitle"/></FONT>
      </A>
    </TD></TR>
  </TABLE>
</xsl:template>


<!-- Gestione degli oggetti di tipo 'calcolo' -->

<xsl:template match="Item[@type='calcolo']">
  <TABLE BORDER="0">
    <TR><TD>
      <TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0">
        <THEAD><TR><TD ALIGN="LEFT" COLSPAN="3">
          <A NAME="{Name/@LinkID}"><B><xsl:value-of select="Name"/></B></A>
        </TD></TR></THEAD>
        <TR>
          <TD xsl:use-attribute-sets="first-top-table"><FONT COLOR="WHITE" SIZE="3"><B>Execute</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Depends On</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Notes</B></FONT></TD>
        </TR>
        <TR>
          <TD xsl:use-attribute-sets="first-bottom-table">
            <xsl:choose>
              <xsl:when test="Calc_routine | Calc_var">
              <TABLE>
              <xsl:for-each select="Calc_routine">
                <TR><TD NOWRAP="NOWRAP"><FONT SIZE="2">
                  <xsl:value-of select="./Name"/>(<xsl:text/>
                  <xsl:for-each select="./Param">
                    <xsl:value-of select="."/>
                    <xsl:if test="position() &lt; last()">; </xsl:if>
                  </xsl:for-each>)<xsl:text/>
                </FONT></TD></TR>
              </xsl:for-each>
              <xsl:for-each select="Calc_var">
                <TR><TD><FONT SIZE="2">
                  <xsl:value-of select="."/>
                </FONT></TD></TR>
              </xsl:for-each>
              </TABLE>
              </xsl:when>
              <xsl:otherwise>Nothing</xsl:otherwise>
            </xsl:choose>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table">
            <xsl:choose>
              <xsl:when test="DependsOnVar | DependsOnEvent | DependsOnMB">
              <TABLE>
              <xsl:for-each select="DependsOnVar | DependsOnEvent | DependsOnMB">
                <TR><TD NOWRAP="NOWRAP"><FONT SIZE="2">
                  <xsl:value-of select="."/>
                </FONT></TD></TR>
              </xsl:for-each>
              </TABLE>
              </xsl:when>
              <xsl:otherwise>Nothing</xsl:otherwise>
            </xsl:choose>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table" ALIGN="LEFT">
            <xsl:apply-templates select="Note"/>
          </TD>
        </TR>
      </TABLE>
    </TD></TR>
    <TR><TD ROWSPAN="2" ALIGN="LEFT" VALIGN="MIDDLE">
      <A HREF="#{../Title/@LinkID}">
        <FONT SIZE="2">Go to <xsl:call-template name="PageTitle"/></FONT>
      </A>
    </TD></TR>
  </TABLE>
</xsl:template>


<!-- Gestione degli oggetti di tipo 'bottone' -->

<xsl:template match="Item[@type='bottone']">
  <TABLE BORDER="0">
    <TR><TD>
      <TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0">
        <THEAD><TR><TD ALIGN="LEFT" COLSPAN="4">
          <A NAME="{Name/@LinkID}"><B><xsl:value-of select="Name"/></B></A>
        </TD></TR></THEAD>
        <TR>
          <TD xsl:use-attribute-sets="first-top-table"><FONT COLOR="WHITE" SIZE="3"><B>Bitmap</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Execute</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Notes</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Help</B></FONT></TD>
        </TR>
        <TR>
          <TD xsl:use-attribute-sets="first-bottom-table">
            <xsl:apply-templates select="Bitmap"/>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table">
            <xsl:apply-templates select="Execute"/>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table" ALIGN="LEFT">
            <xsl:apply-templates select="Note"/>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table">
            <xsl:apply-templates select="Help"/>
          </TD>
        </TR>
      </TABLE>
    </TD></TR>
    <TR><TD ROWSPAN="2" ALIGN="LEFT" VALIGN="MIDDLE">
      <A HREF="#{../Title/@LinkID}">
        <FONT SIZE="2">Go to <xsl:call-template name="PageTitle"/></FONT>
      </A>
    </TD></TR>
  </TABLE>
</xsl:template>


<!-- Gestione degli oggetti di tipo 'immagine' -->

<xsl:template match="Item[@type='immagine']">
  <TABLE BORDER="0">
    <TR><TD>
      <TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0">
        <THEAD><TR><TD ALIGN="LEFT" COLSPAN="4">
          <A NAME="{Name/@LinkID}"><B>Image</B></A>
        </TD></TR></THEAD>
        <TR>
          <TD xsl:use-attribute-sets="first-top-table"><FONT COLOR="WHITE" SIZE="3"><B>Name</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Hyperlink</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Open On</B></FONT></TD>
          <TD xsl:use-attribute-sets="top-table"><FONT COLOR="WHITE" SIZE="3"><B>Help</B></FONT></TD>
        </TR>
        <TR>
          <TD xsl:use-attribute-sets="first-bottom-table">
            <xsl:apply-templates select="Name"/>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table">
            <xsl:apply-templates select="Hyperlink"/>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table">
            <FONT SIZE="2">
              <xsl:value-of select="HyperlinkOpenOn"/>
            </FONT>
          </TD>
          <TD xsl:use-attribute-sets="bottom-table">
            <xsl:apply-templates select="Help"/>
          </TD>
        </TR>
      </TABLE>
    </TD></TR>
    <TR><TD ROWSPAN="2" ALIGN="LEFT" VALIGN="MIDDLE">
      <A HREF="#{../Title/@LinkID}">
        <FONT SIZE="2">Go to <xsl:call-template name="PageTitle"/></FONT>
      </A>
    </TD></TR>
  </TABLE>
</xsl:template>


<!-- Gestione celle vuote -->

<xsl:template match="Name | Title | Template | Author | User |
                     Version | Created | Lastrevised |
                     Comment | Note | Help | Hyperlink |
                     Class | Bitmap | Property | Ref |
                     Program | Execute">
  <xsl:call-template name="ValueOrNotAvailable"/>
</xsl:template>


</xsl:stylesheet>

