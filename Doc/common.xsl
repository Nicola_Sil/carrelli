<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">


<xsl:attribute-set name="global-notes">
    <xsl:attribute name="ALIGN">LEFT</xsl:attribute>
    <xsl:attribute name="BGCOLOR">#F5F5F5</xsl:attribute>
    <xsl:attribute name="STYLE">border: 1px #000000 solid;</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="page-notes" use-attribute-sets="global-notes">
    <xsl:attribute name="BGCOLOR">#DCDCDC</xsl:attribute>
</xsl:attribute-set>


<xsl:attribute-set name="top-table">
    <xsl:attribute name="ALIGN">CENTER</xsl:attribute>
    <xsl:attribute name="BGCOLOR">#6495ED</xsl:attribute>
    <xsl:attribute name="STYLE">border: 1px #000000 solid;
                                border-left: none;</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="first-top-table" use-attribute-sets="top-table">
    <xsl:attribute name="STYLE">border: 1px #000000 solid;</xsl:attribute>
</xsl:attribute-set>


<xsl:attribute-set name="bottom-table">
    <xsl:attribute name="ALIGN">CENTER</xsl:attribute>
    <xsl:attribute name="BGCOLOR">#F5F5F5</xsl:attribute>
    <xsl:attribute name="STYLE">border-bottom: 1px #000000 solid;
                                border-right: 1px #000000 solid;</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="first-bottom-table" use-attribute-sets="bottom-table">
    <xsl:attribute name="STYLE">border: 1px #000000 solid;
                                border-top: none;</xsl:attribute>
</xsl:attribute-set>


<xsl:template name="PageTitle">
    <xsl:param name="page" select=".."/>
    <xsl:param name="name" select="'Page'"/>
    <B>
    <xsl:choose>
        <xsl:when test="$page/Title!=''">
            <xsl:value-of select="$page/Title"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$name"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="$page/@number"/>
        </xsl:otherwise>
    </xsl:choose>
    </B>
</xsl:template>


<xsl:template name="PageIndex">
    <xsl:param name="name" select="'Page'"/>
    <xsl:param name="page"/>
    <xsl:param name="next"/>
    <TD WIDTH="20%" VALIGN="TOP" ALIGN="CENTER">
        <TABLE>
            <TR><TD ALIGN="CENTER"><FONT SIZE="2">
                <A HREF="#{$page/Title/@LinkID}">
                    <xsl:value-of select="$name"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$page/@number"/>
                </A>
            </FONT></TD></TR>
            <TR><TD ALIGN="CENTER"><FONT SIZE="2">
                <B><xsl:value-of select="$page/Title"/></B>
            </FONT></TD></TR>
        </TABLE>
    </TD>
    <xsl:if test="$next > 0">
        <xsl:for-each select="following-sibling::Pag[1]">
            <xsl:call-template name="PageIndex">
                <xsl:with-param name="name" select="$name"/>
                <xsl:with-param name="page" select="."/>
                <xsl:with-param name="next" select="number($next)-1"/>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:if>
</xsl:template>


<xsl:template name="ValueOrNotAvailable">
    <xsl:param name="NA" select="'N/A'"/>
    <xsl:choose>
        <xsl:when test=".=../@* and .!=''"><xsl:value-of select="."/></xsl:when>
        <xsl:when test="text()!=''"><xsl:apply-templates/></xsl:when>
        <xsl:otherwise><xsl:value-of select="$NA"/></xsl:otherwise>
    </xsl:choose>
</xsl:template>


<xsl:template name="LogicalCheckBox">
    <xsl:choose>
        <xsl:when test=".='FALSE'"><IMG SRC="No.gif"/></xsl:when>
        <xsl:when test=".='TRUE'"><IMG SRC="Yes.gif"/></xsl:when>
        <xsl:otherwise><B>?</B></xsl:otherwise>
    </xsl:choose>
</xsl:template>


<!-- User documentation tags -->

<xsl:template match="Book">
    <DIV ALIGN="CENTER">
        <H1><xsl:value-of select="@title"/></H1>
        <IMG SRC="{@bookcover}"/>
        <xsl:apply-templates/>
    </DIV>
</xsl:template>


<xsl:template match="Chapter">
    <H1><A NAME="{@anchor}"><xsl:value-of select="@title"/></A></H1>
    <xsl:apply-templates/>
</xsl:template>


<xsl:template match="Sect">
    <H2><A NAME="{@anchor}"><xsl:value-of select="@title"/></A></H2>
    <xsl:apply-templates/>
</xsl:template>


<xsl:template match="SubSect">
    <H3><A NAME="{@anchor}"><xsl:value-of select="@title"/></A></H3>
    <xsl:apply-templates/>
</xsl:template>


<xsl:template match="Paragraph">
    <P><H4><A NAME="{@anchor}"><xsl:value-of select="@title"/></A></H4></P>
    <xsl:apply-templates/>
</xsl:template>


<xsl:template match="SubParagraph">
    <P><H5><A NAME="{@anchor}"><xsl:value-of select="@title"/></A></H5></P>
    <xsl:apply-templates/>
</xsl:template>


<xsl:template match="H6">
    <P><H6><A NAME="{@anchor}"><xsl:value-of select="@title"/></A></H6></P>
    <xsl:apply-templates/>
</xsl:template>


<xsl:template match="Code">
    <P/>
    <TABLE WIDTH="100%">
        <TR><TD STYLE="font-size:14pt; color:white; background-color:#888888">
            <xsl:value-of select="@title"/>
        </TD></TR>
        <TR><TD>&#160;</TD></TR>
        <TR><TD><PRE>
            <xsl:apply-templates/>
        </PRE></TD></TR>
        <TR><TD STYLE="border-bottom: 2pt SOLID #888888;">&#160;</TD></TR>
    </TABLE>
</xsl:template>


<xsl:template match="LineCode">
    <P><SPAN STYLE="font-family: Courier New">
        <xsl:value-of select='.'/>
    </SPAN></P>
</xsl:template>


<xsl:template match="Example">
    <P/>
    <TABLE WIDTH="100%">
        <TR><TD STYLE="font-size:14pt; color:red; font-style:italic; font-weight:bold;">
            <xsl:choose>
                <xsl:when test="@title"><xsl:value-of select="@title"/></xsl:when>
                <xsl:otherwise>Esempio</xsl:otherwise>
            </xsl:choose>
        </TD></TR>
        <TR><TD>&#160;</TD></TR>
        <TR><TD>
            <xsl:apply-templates/>
        </TD></TR>
        <TR><TD STYLE="border-bottom: 2pt SOLID red;">&#160;</TD></TR>
    </TABLE>
</xsl:template>


<xsl:template match="Notes">
    <P/>
    <TABLE WIDTH="100%" BORDER="2" STYLE="Border-color:yellow">
        <TR><TD STYLE="font-size:14pt; background-color:yellow;">
            <xsl:choose>
                <xsl:when test="@title"><xsl:value-of select="@title"/></xsl:when>
                <xsl:otherwise>Nota bene</xsl:otherwise>
            </xsl:choose>
        </TD></TR>
        <TR><TD>
            <xsl:apply-templates/>
        </TD></TR>
    </TABLE>
</xsl:template>


<xsl:template match="Image">
    <P/>
    <IMG BORDER="0" SRC="{@file}" ALIGN="CENTER"/>
    <P><SPAN STYLE="font-size:8pt; text-align:center;">
        <xsl:value-of select="@caption"/>
    </SPAN></P>
</xsl:template>


<xsl:template match="@*|node()">
    <xsl:copy>
        <xsl:copy-of select="@*"/>
        <xsl:apply-templates/>
    </xsl:copy>
</xsl:template>


</xsl:stylesheet>
