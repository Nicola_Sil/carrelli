* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP3START
* Ver      : 2.1.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 24/03/95
* Aggiornato il : 01/09/97
* Translated    : 03/06/2000  (Max Vizzini)
* #%&%#Build:  59
* ----------------------------------------------------------------------------
* Starting program.
*
#include "cp_app_lang.inc"

* CP3START is the starting program for applications generated with
* CodePainter Revolution. This program initializes the environment and
* the system variables. Moreover, it opens the database files.
*
*  Variables:
*    CP_DBTYPE : database type (Oracle,SQLServer,Access,DB2,VFP)
*    CP_ODBCCONN : name of the ODBC connection to use
* ----------------------------------------------------------------------------

Lparameters i_xParm1,i_xParm2,i_xParm3,i_xParm4,i_xParm5

* --- STARTING PROGRAM FOR CODEPAINTER REVOLUTION APPLICATIONS GENERATED IN VISUAL FOXPRO
#Define cPathCS "C:\painterr\Build57\vfcsim"
Set Talk Off

* --- variable for error administration
Public bTrsErr
bTrsErr = .F.
* --- global variables
i_codute = 1
i_GROUPROLE=0
i_codazi = "sil"
i_datsys = Date()
i_curform = .Null.                       && Current Form
i_vidgrf = .T.
i_cBmpPath = ''                          && Path for cursors and BMP
i_cStdIcon = 'logosil.ico'               && standard icon
i_bDisablePostIn=.F.
i_ab_btnpostin=.T.                   && abilita/disabilita bottoni post-in su application bar
i_ab_btnuser=.T.                     && abilita/disabilita bottone gestione utenti su application bar
i_bDisableBackgroundImage=.F.
i_bDisableAsyncConn=.T.
i_nZoomMaxRows=200
i_bCheckEmptyRows=.T.                    && Se messo a .f. disabilita il controllo di riga piena
i_CpDic = "plan"
i_cModules = "vfcsim"
i_cLanguage = ''
i_cLanguageData = ''
i_bSecurityRecord = .F. && Se attivo abilita la sicurezza a livello di dato.
i_MsgTitle=''
i_cAppDir = 'Zucchetti' &&Nome della sottocartella per la Application Data, pu� essere sostituita con il nome del prodotto nel cnf (Es: C:\Documents and Settings\[user]\Dati applicazioni\Zucchetti\)

Public cHomeDir
cHomeDir = Addbs(Sys(5)+Curdir())  && use whatever method you prefer

Public i_VisualTheme
#If Version(5)<900
    i_VisualTheme = -1 && -1 Vista Classica, 5 Office 2007 Blue
#Else
    i_VisualTheme = 5 && -1 Vista Classica, 5 Office 2007 Blue
#Endif

g_PROJECTLANGUAGE='' && lingua di progetto

* --------- Parametrizzare interfaccia
* Sceglie quale data deve essere utilizzata come filtro per le date di validit� dei postin
* i_DatSysPostInFilterDate = .t. usa i_DatSys, altrimenti usa la funzione date().
Public i_DatSysPostInFilterDate
i_DatSysPostInFilterDate = .T.
Public i_dPostInFilterDate
i_dPostInFilterDate = i_datsys

Public g_MNODEBMP, g_MLEAFBMP

* ---- Nomi bitmap di default se bitmap vuoti...
g_MLEAFBMP='DIR_DESC.BMP' && Bitmap per le foglie
g_MNODEBMP='DIRCL.BMP'    && Bitmap per i nodi

*--- Gestione riconnessione automatica
Public i_cFileCNF
*--- Gestione riconnessione automatica

*--- Interfaccia - Font
Public i_cProjectFontName, i_nProjectFontSize
i_cProjectFontName='Arial'
i_nProjectFontSize=9

* --------- Parametrizzare interfaccia End
* --- setta l' ambiente
CP_PATH=''        && program paths
CP_DBTYPE=''      && database type:VFP,Access,SQLServer,Oracle,DB2,Informix,Interbase,SAPDB,MySQL,PostgreSQL
CP_ODBCCONN=''    && ODBC connection name

* --- legge i parametri della procedura
ReadAppParam(i_xParm1,i_xParm2,i_xParm3,i_xParm4,i_xParm5)

SetEnv()

* --- The following variables cannot be changed from user setup
i_cSuperPwd = 'codepainter'   && password for superuser rights
i_demolimits = ''             && demo version limits
* --- Managing servers and files
Public i_TableProp, i_nTables
i_nTables = 1
Dimension i_TableProp[1,5]
Public i_ServerConn, i_nServers, i_ServerConnBis
i_nServers = 1
*--- Gestione riconnessione
Dimension i_ServerConn[1,8]
Dimension i_ServerConnBis[1,2]
*--- Gestione riconnessione
* --- "Local" connection definition
i_ServerConn[1,1]='local'
i_ServerConn[1,4]=1
i_ServerConn[1,5]=0
i_ServerConn[1,7]=! i_bDisablePostIn
i_ServerConn[1,2]=Iif(Empty(CP_DBTYPE) Or CP_DBTYPE='VFP',0,-1) && 0 for foxpro database, -1 for ODBC connection
i_ServerConn[1,6]=CP_DBTYPE
i_ServerConn[1,3]=CP_ODBCCONN
*--- Gestione riconnessione
i_ServerConn[1,8]=""
i_ServerConnBis[1,1]=-2
i_ServerConnBis[1,2]=CP_ODBCCONN
*--- Gestione riconnessione

* --- Open connection with the database
If cp_OpenDatabase()
    *--- leggo le impostazioni generali
    *--- Deve andare dopo la ricostruzione database (legge tabelle create appositamente per la gestione)
    *--- interfaccia (macro per evitare che il batch vada a finire nell'eseguibile)
    l_sPrg="cp_LoadGUI"
    Do (l_sPrg) With .Null. , 'S'
    *--- Leggo e setto le impostazioni della riconessione automatica
    l_sPrg="cp_LoadAutoRiconnect"
    Do (l_sPrg) With .Null.

    cp_AskUser()
    If i_codute<>0
        AddModuleToPath()
        * --- Build application menu
        cp_backgroundmask()
        cp_menu()
        cp_desk()
        Read Events
        * --- Program end
        *--- Blocco l'interfaccia
        i_ThemesManager.LockVfp(.T.)
        *--- Distruggo qui la toolbar perch� ho ancora bisogno delle connessione al db
        If Vartype(odesktopbar) = 'O'
            odesktopbar.Destroy()
            odesktopbar = .Null.
        Endif
        Release opostittimer
        * --- Release all forms. This is needed to save Post-INs before
        *     the main connection is closed
        i=1
        Do While i<=_Screen.FormCount
            If _Screen.Forms(i).BaseClass='Form'
                _Screen.Forms(i).Release()
            Else
                i=i+1
            Endif
        Enddo
    Else
        CP_MSG(CP_TRANSLATE(MSG_PROCEDURE_EXIT),.F.)
    Endif
    cp_CloseDatabase()
Else
    CP_MSG(CP_TRANSLATE(MSG_CANNOT_OPEN_DATABASE) ,.F.)
Endif
ResetEnv()
Return

Proc SetEnv()
    Set Talk Off
    Set Safety Off
    Set Deleted On
    Set Confirm On
    Set Escape On
    Set Date Italian
    Set Hours To 24
    Set Exclusive Off
    Set Multilock On
    Set Decimal To 6
    Set Status Bar Off
    #If Version(5)>=800
        * settata a on per avere i tooltiptext attivi anche sui detail in basso
        * sulla barra di stato
        Set Status Off
        *set notify off
        Set Notify Cursor Off
    #Endif
    *--- Disabilito il controllo data compilazione fxp/prg
    If _vfp.StartMode<>0
        Set Development Off
    Endif
    Set Cpdialog Off
    Set NullDisplay To ''
    Set Escape Off
    Set Reprocess To Automatic
    *set sysmenu to _msm_view
    *release pad _mview of _msysmenu
    On Shutdown Clear Events
    * --- Release standard toolbar
    Deactivate Window "Color Palette"
    Deactivate Window "Database Designer"
    Deactivate Window "Form Controls"
    Deactivate Window "Form Designer"
    Deactivate Window "Layout"
    Deactivate Window "Print Preview"
    Deactivate Window "Query Designer"
    Deactivate Window "Report Controls"
    Deactivate Window "Report Designer"
    Deactivate Window "Standard"
    Deactivate Window "View Designer"
    * --- Function Keys
    Push Key
    On Key Label F1    Do cp_help
    i_help = ''
    On Key Label F2    Do cp_DoAction With "ecpPrint"
    On Key Label F3    Do cp_DoAction With "ecpEdit"
    On Key Label F4    Do cp_DoAction With "ecpLoad"
    On Key Label F5    Do cp_DoAction With "ecpDelete"
    On Key Label F6    Do cp_DoAction With "ecpF6"
    On Key Label F7    Do cp_DoAction With "ecpPrior"
    On Key Label F8    Do cp_DoAction With "ecpNext"
    On Key Label F9    Do cp_DoAction With "ecpZoom"
    On Key Label Ctrl+F9    Do cp_DoAction With "ecpZoomOnZoom"
    On Key Label Alt+F9  Do vrt_addfield
    On Key Label F10   Do cp_DoAction With "ecpSave"
    On Key Label F12   Do cp_DoAction With "ecpFilter"
    On Key Label Alt+F12   Do cp_DoAction With "ecpSecurity"
    On Key Label Ctrl+F12   Do vrt_build
    On Key Label Alt+F11   Do cp_DoAction With "ecpInfo"
    On Key Label PGUP  Do cp_DoAction With "ecpPgUp"
    On Key Label PGDN  Do cp_DoAction With "ecpPgDn"
    On Key Label ESC   Do cp_DoAction With "ecpQuit"
    On Key Label Ctrl+D Do cp_NavBar With .T.

    * --- Configurazione
    GetConfigFile(1)

    * --- Path and procedures
    cPath = cPathCS
    cPath=Iif(!Empty(CP_PATH),CP_PATH,cPath)
    i_cBmpPath = cPath+'\'
    * --- cerca l' eventuale IM locale da aggiungere in path
    Set Path To && altrimenti la funzione 'file' risponde .t. per tutti i file in path i file
    PUBLIC cPathVfcsim
    Do Case
        Case Directory('vfcsim\')
            * --- aggiunge vfcsim alla path (applicazione normale, sottodirectory contenente VFCSIM)
            impath='.\vfcsim;'
                                cPathVfcsim=cHomeDir+"\vfcsim"
                                i_cBmpPath = '.\vfcsim\'
        Case Directory('..\vfcsim\')
            * --- aggiunge ..\vfcsim alla path (applicazione "large", sottodirectory contenente VFCSIM nella directory superiore)
            impath='..\vfcsim;'
                                cPathVfcsim=cHomeDir+"..\vfcsim"
                                i_cBmpPath = '..\vfcsim\'
        Otherwise
            * --- non ha trovato IM locale
            impath=''
            cPathVfcsim=cPathCS
    Endcase
    *
    Set Path To ..\vfcssrc; &impath &cPath
    Set Procedure To cp_lib,cp_tbar,cp_forms,cp_ctrls,cp_zoom,cp_desk,cp_sec,cp_ppx,cp_dbadm,cp_dcx,cp_sqlx2,cp_class,cp_func,cp_expdb, vm_exec Additive
    Set Procedure To cp_ThemesManager, cp_NavBar, cp_DockWnd, cp_activitylogger, cp_gridex Additive && Zucchetti Aulla inizio aggiunto cp_monlib
    *--- Aggiungo cp_func della vfcsim
    If Not Empty(impath)
        Set Procedure To i_cBmpPath+"cp_func" Additive
    Endif
    Set Classlib To stdz Additive
    Set Classlib To FoxCharts Additive  && FoxChart
    If !(Upper(Set("Classlib"))$"DATEPICKER")
        Set Classlib To DatePicker Additive
    Endif
    Do "System.App"
    Do declare_dlls  &&ZoomHeader
    &&Declare API
    Declare Integer GetMenuItemCount In user32 Integer hMenu
    Declare Integer GetSystemMetrics In user32 Integer nIndex
    Declare Integer DestroyMenu In user32 Integer hMenu    
    Declare Integer CreatePopupMenu In user32    
    * --- Path di memorizzazione delle bitmap ottenute dall'esplosione delle icone
    * --- chiamata funzione per appdata
    * --- cp_lib cp_getappdata

    i_cTmpImg =cp_AdvancedDir(Addbs(Cp_getAppData())+Addbs(i_cAppDir),'IMG',Sys(2003))

    * --- Background
    Clear
    Do cp__logo
    * --- Gestione Logo Dinamico
    If Type("_screen.oImgLogo")='O'
        Bindevent(_Screen,"Resize",_Screen.oImgLogo,"screen_resize")
    Endif
    * --- fine Gestione Logo Dinamico
    i_MsgTitle= _Screen.Caption

    *--- gestore temi
    If Not Vartype(_Screen.cp_ThemesManager)=="O"
        _Screen.Newobject("cp_ThemesManager","cp_ThemesManager")
        Public i_ThemesManager
        i_ThemesManager = _Screen.cp_ThemesManager
        *--- Disattivo il menu
        If i_VisualTheme <> -1
            Set Sysmenu Off
        Endif
        *--- OutLook Navigation Panel
    Endif

    *--- Activity logger, setta variabili pubbliche
    oActivityLoggerSetup = Createobject("ActivityLoggerSetup")

    * --- Configurazione
    GetConfigFile(2)
    * controlli per sicurezza al livello di record
    * --- visualizzo lo zoom
    i_bSecurityRecord  = i_bSecurityRecord  And Not(Empty(CP_DBTYPE) Or CP_DBTYPE='VFP') && se database VFP disabilito sicurezzza a livello di record
    If i_bSecurityRecord
        On Key Label Ctrl+F7 Do cp_recseczoom With i_curform
        * --- costruisco l'anagrafica dal  business object di riferimento
        On Key Label Alt+F7 Do cp_DoAction With "ecpSecurityRecord"
    Endif
    *--- Toolbar
    Public oCpToolBar
    oCpToolBar=Createobject('CPToolBar', i_VisualTheme<>-1 And Vartype(_Screen.cp_ThemesManager)=="O")
    oCpToolBar.Dock(0)
    oCpToolBar.Show()
    * --- Screen
    _Screen.Icon=i_cBmpPath+i_cStdIcon
    _Screen.Caption= i_MsgTitle
    If File('cp_fhelp.hlp')
        Set Help To cp_fhelp.hlp
    Endif
Endproc

Proc GetConfigFile(nType)
    If nType=1
        *--- Eseguo macro dei parametri cnf prima la set procedure e creo i_cConfigCnf per le successive macro
        Local h,l,F
        F=''
        If File(i_CpDic+'.cnf')
            F=i_CpDic
        Else
            If File('cp3start.cnf')
                F='cp3start'
            Endif
        Endif
        If !Empty(F+'.cnf')
            Public i_cConfigCnf
            i_cConfigCnf = ''
            h=Fopen(F+'.cnf')
            l_bMacro=.F.
            Do While !Feof(h)
                l=Fgets(h)
                If Upper(l)='*--- BEFORE SET PROCEDURE START'
                    l_bMacro=.T.
                Endif
                If Upper(l)='*--- BEFORE SET PROCEDURE END'
                    l_bMacro=.F.
                Endif
                If l_bMacro
                    &l
                Else
                    i_cConfigCnf = i_cConfigCnf + Iif(Upper(l)<>'*--- BEFORE SET PROCEDURE END',l+Chr(13),'')
                Endif
            Enddo
            Fclose(h)
        Endif
        *--- Gestione riconnessione automatica
        i_cFileCNF=F+'.cnf'
        *--- Gestione riconnessione automatica
    Else
        *--- Eseguo macro dei parametri cnf dopo la set procedure
        If Vartype(i_cConfigCnf)='C'
            Local l_num, i, l
            l_num = Alines(aCnf, i_cConfigCnf)
            For i=1 To l_num
                l=aCnf[i]
                &l
            Endfor
            Release i_cConfigCnf
        Endif
    Endif
Endproc

Proc ResetEnv()
    On Error =.T.
    Set Sysmenu To Defa
    Clear Events
    If Type('_screen.cnt')='O'
        * --- VFP6 can generate an error when clear all is executed if we do not remove
        * the background dialog window.
        _Screen.RemoveObject('cnt')
    Endif
    If Vartype(_Screen.navBar)=="O"
        _Screen.RemoveObject("NavBar")
    Endif
    If Vartype(_Screen.ImgBackground)=="O"
        _Screen.RemoveObject("ImgBackground")
    Endif
    If Vartype(_Screen.TBMDI)=="O"
        _Screen.RemoveObject("TBMDI")
    Endif
    If Vartype(_Screen.cp_AlertManager)=='O'
        _Screen.RemoveObject("cp_AlertManager")
        Wait Clear
    Endif
    *--- Distruggo commandbar
    oCpToolBar.Destroy()
    oCpToolBar = .Null.
    i_MenuToolbar.Destroy()
    i_MenuToolbar = .Null.
    If Vartype(_Screen.cp_ThemesManager)=="O"
        _Screen.RemoveObject("cp_ThemesManager")
    Endif
    * rilasciamo toolbar
    If Type('odesktopbar')='O'
        Release odesktopbar
    Endif
    If Type('OCPTOOLBAR')='O'
        Release oCpToolBar
    Endif

    Clear All
    Release All
    Close All
    Set Help To
    Pop Key
    On Shutdown
    _Screen.Icon=''
    _Screen.Caption='Microsoft Visual FoxPro'
    if ("CP_DOCKWND"$Upper(Set("Library")))
     release library ..\vfcsim\cp_dockwnd.fll
    endif
    On Error
Endproc

Proc AddModuleToPath()
    If Type('i_cModules')='C' And Not(Empty(i_cModules))
        Local i_p,i_m, l_module
        i_m=i_cModules
        i_p=At(',',i_m)
        Do While i_p<>0
            l_module = Left(i_m,i_p-1)
            If Upper(l_module)<>"VFCSIM"
                AddModuleToPath1(cp_FindModule(m.l_module))
            Endif
            i_m=Substr(i_m,i_p+1)
            i_p=At(',',i_m)
        Enddo
        If Upper(i_m)<>"VFCSIM"
            AddModuleToPath1(cp_FindModule(i_m))
        Endif
    Endif
Endproc

Proc AddModuleToPath1(i_mp)
    Local i_r
    i_r=Rat('\',i_mp)
    If i_r<>0
        i_mp=Left(i_mp,i_r-1)
        If Lower(Right(i_mp,4))='\exe'
            Set Path To Set('path')+';'+Left(i_mp,Len(i_mp)-4)+'\VFCSSRC'
            If File(Left(i_mp,Len(i_mp)-4)+'\VFCSSRC\CP_FUNC.PRG')
                Compile (Left(i_mp,Len(i_mp)-4)+'\VFCSSRC\CP_FUNC.PRG')
            Endif
            Set Proc To (Left(i_mp,Len(i_mp)-4)+'\VFCSSRC\CP_FUNC.FXP') Additive
        Else
            Set Path To Set('path')+';'+i_mp
            If File(i_mp+'\CP_FUNC.PRG')
                Compile (i_mp+'\CP_FUNC.PRG')
            Endif
            Set Proc To (i_mp+'\CP_FUNC.FXP') Additive
        Endif
    Endif
Endproc

Procedure ReadAppParam(i_xParm1,i_xParm2,i_xParm3,i_xParm4,i_xParm5)
    Local i_n,i_i
    For i_i=1 To 5
        i_n='i_xParm'+Alltrim(Str(i_i))
        If Type(i_n)='C' And Left(&i_n,5)='-sso='
            Public i_cSsoID,i_xSsoCredentials
            i_cSsoID=Substr(&i_n,6)
        Endif
        If Type(i_n)='C' And Left(&i_n,6)='-ssoc='
            Public i_xSsoCredentials
            i_xSsoCredentials=Substr(&i_n,7)
        Endif
    Next
    Return



