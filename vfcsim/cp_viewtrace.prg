* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_viewtrace                                                    *
*              Activity logger viewer                                          *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-23                                                      *
* Last revis.: 2011-04-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject
* --- Area Manuale = Header
* --- cp_viewtrace
If i_ServerConn[1,2]=0
    cp_ErrorMsg(cp_translate("Activity Logger non disponibile per database Visual FoxPro"))
    Return
Endif
* --- Fine Area Manuale
Return(Createobject("tcp_viewtrace",oParentObject))

* --- Class definition
Define Class tcp_viewtrace As StdForm
    Top    = 4
    Left   = 3

    * --- Standard Properties
    Width  = 795
    Height = 539+35
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2011-04-18"
    HelpContextID=37257243
    max_rt_seq=31

    * --- Constant Properties
    _IDX = 0
    cPrg = "cp_viewtrace"
    cComment = "Activity logger viewer"
    oParentObject = .Null.
    Icon = "mask.ico"
    *closable = .f.

    * --- Local Variables
    w_CARTELLALOG = Space(250)
    w_ALCMDSQL = Space(0)
    w_NOMECURSORE = Space(10)
    w_DATI = Space(10)
    w_NOMECURSOREDATI = Space(10)
    w_FILEDILOG = Space(250)
    w_FILT_DBR = Space(1)
    w_FILT_DBW = Space(1)
    w_FILT_DBI = Space(1)
    w_FILT_DBD = Space(1)
    w_RIGHE = 0
    w_MILLISECONDI = 0
    w_FILT_VQR = Space(1)
    w_FILT_GSO = Space(1)
    w_FILT_GSD = Space(1)
    w_FILT_BTO = Space(1)
    w_FILT_BTD = Space(1)
    w_FILT_DBM = Space(1)
    w_FILT_DBT = Space(1)
    w_FILT_DBX = Space(1)
    w_FILT_TBT = Space(1)
    w_FILT_TCT = Space(1)
    w_FILT_TRT = Space(1)
    w_FILT_ASI = Space(1)
    w_FILT_RIC = Space(1)
    w_FILT_DEA = Space(1)
    w_CONTENUTOINFRASESQL = Space(250)
    w_TROVASUCCESSIVO = .F.
    w_CARTELLALOG = Space(250)
    w_FILT_TASTIFUN = Space(1)
    w_FILT_EVENT = Space(1)
    w_ELENCO = .Null.
    * --- Area Manuale = Declare Variables
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=2, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tcp_viewtracePag1","cp_viewtrace",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_translate("Traccia")
            .Pages(2).AddObject("oPag","tcp_viewtracePag2","cp_viewtrace",2)
            .Pages(2).oPag.Visible=.T.
            .Pages(2).Caption=cp_translate("Selezioni")
            .Pages(2).oPag.oContained=Thisform
        Endwith
        This.Parent.oFirstControl = .Null.
        This.Parent.cComment=cp_translate(This.Parent.cComment)
        * --- Area Manuale = Init Page Frame
        * --- Fine Area Manuale
    Endproc
    Proc Init(oParentObject)
        If Vartype(m.oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        This.w_ELENCO = This.oPgFrm.Pages(1).oPag.ELENCO
        DoDefault()
    Proc Destroy()
        This.w_ELENCO = .Null.
        DoDefault()

        * --- Open tables
    Function OpenWorkTables()
        Return(This.OpenAllTables(0))

    Procedure SetPostItConn()
        Return


        * --- Read record and initialize Form variables
    Procedure LoadRec()
    Endproc

    * --- ecpQuery
    Procedure ecpQuery()
        This.BlankRec()
        This.cFunction="Edit"
        This.SetStatus()
        If This.cFunction="Edit"
            This.mEnableControls()
        Endif
    Endproc
    * --- Esc
    Proc ecpQuit()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            This.cFunction = "Edit"
            This.oFirstControl.SetFocus()
            Return
        Endif
        * ---
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Endproc
    Proc QueryUnload()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            Nodefault
            Return
        Endif
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Proc ecpSave()
        If This.TerminateEdit() And This.CheckForm()
            This.LockScreen=.T.
            This.mReplace(.T.)
            This.LockScreen=.F.
            This.NotifyEvent("Done")
            This.Release()
        Endif
    Endproc
    Func OkToQuit()
        Return .T.
    Endfunc

    * --- Blank Form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        With This
            .w_CARTELLALOG=Space(250)
            .w_ALCMDSQL=Space(0)
            .w_NOMECURSORE=Space(10)
            .w_DATI=Space(10)
            .w_NOMECURSOREDATI=Space(10)
            .w_FILEDILOG=Space(250)
            .w_FILT_DBR=Space(1)
            .w_FILT_DBW=Space(1)
            .w_FILT_DBI=Space(1)
            .w_FILT_DBD=Space(1)
            .w_RIGHE=0
            .w_MILLISECONDI=0
            .w_FILT_VQR=Space(1)
            .w_FILT_GSO=Space(1)
            .w_FILT_GSD=Space(1)
            .w_FILT_BTO=Space(1)
            .w_FILT_BTD=Space(1)
            .w_FILT_DBM=Space(1)
            .w_FILT_DBT=Space(1)
            .w_FILT_DBX=Space(1)
            .w_FILT_TBT=Space(1)
            .w_FILT_TCT=Space(1)
            .w_FILT_TRT=Space(1)
            .w_FILT_ASI=Space(1)
            .w_FILT_RIC=Space(1)
            .w_FILT_DEA=Space(1)
            .w_CONTENUTOINFRASESQL=Space(250)
            .w_TROVASUCCESSIVO=.F.
            .w_CARTELLALOG=Space(250)
            .w_FILT_TASTIFUN=Space(1)
            .w_FILT_EVENT=Space(1)
            .DoRTCalc(1,1,.F.)
            .w_ALCMDSQL = .w_ELENCO.GETVAR("ALCMDSQL")
            .oPgFrm.Page1.oPag.ELENCO.Calculate()
            .DoRTCalc(3,3,.F.)
            .w_DATI = .w_ELENCO.GETVAR("ALCRDATI")
            .w_NOMECURSOREDATI = Justpath( .w_FILEDILOG ) + "\" + Nvl( .w_DATI , "")
            .DoRTCalc(6,6,.F.)
            .w_FILT_DBR = 'S'
            .w_FILT_DBW = 'S'
            .w_FILT_DBI = 'S'
            .w_FILT_DBD = 'S'
            .DoRTCalc(11,12,.F.)
            .w_FILT_VQR = 'S'
            .w_FILT_GSO = 'S'
            .w_FILT_GSD = 'S'
            .w_FILT_BTO = 'S'
            .w_FILT_BTD = 'S'
            .w_FILT_DBM = 'S'
            .w_FILT_DBT = 'S'
            .w_FILT_DBX = 'S'
            .w_FILT_TBT = 'S'
            .w_FILT_TCT = 'S'
            .w_FILT_TRT = 'S'
            .w_FILT_ASI = 'S'
            .w_FILT_RIC = 'S'
            .w_FILT_DEA = 'S'
            .DoRTCalc(27,27,.F.)
            .w_TROVASUCCESSIVO = .F.
            .DoRTCalc(29,29,.F.)
            .w_FILT_TASTIFUN = 'S'
            .w_FILT_EVENT = 'S'
        Endwith
        This.SaveDependsOn()
        This.SetControlsValue()
        This.mHideControls()
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- cp_viewtrace
        This.w_ELENCO.bNoMenuFunction = .T.
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = Setting operation
    *     Allowed Parameters: Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        * --- Deactivating List page when <> da Query
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt
        i_cFlt =""
        Return (i_cFlt)
    Endfunc

    Proc QueryKeySet(i_cWhere,i_cOrderBy)
    Endproc

    Proc SelectCursor
    Proc GetXKey

        * --- Update Database
    Function mReplace(i_bEditing)
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        This.NotifyEvent('Update start')
        With This
        Endwith
        This.NotifyEvent('Update end')
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(.T.)

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        If i_bUpd
            With This
                .DoRTCalc(1,1,.T.)
                .w_ALCMDSQL = .w_ELENCO.GETVAR("ALCMDSQL")
                .oPgFrm.Page1.oPag.ELENCO.Calculate()
                .DoRTCalc(3,3,.T.)
                .w_DATI = .w_ELENCO.GETVAR("ALCRDATI")
                .w_NOMECURSOREDATI = Justpath( .w_FILEDILOG ) + "\" + Nvl( .w_DATI , "")
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
            Endwith
            This.DoRTCalc(6,31,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
            .oPgFrm.Page1.oPag.ELENCO.Calculate()
        Endwith
        Return


        * --- Enable controls under condition
    Procedure mEnableControls()
        This.oPgFrm.Page1.oPag.oBtn_1_7.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_11.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        DoDefault()
        Return

        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            .oPgFrm.Page1.oPag.ELENCO.Event(cEvent)
        Endwith
        * --- Area Manuale = Notify Event End
        * --- cp_viewtrace
        *visualizza la pagina 1
        If cEvent='PopolaZoom'
            This.oPgFrm.ActivePage=1
        Endif
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

    Function SetControlsValue()
        If Not(This.oPgFrm.Page1.oPag.oALCMDSQL_1_2.Value==This.w_ALCMDSQL)
            This.oPgFrm.Page1.oPag.oALCMDSQL_1_2.Value=This.w_ALCMDSQL
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILEDILOG_2_2.Value==This.w_FILEDILOG)
            This.oPgFrm.Page2.oPag.oFILEDILOG_2_2.Value=This.w_FILEDILOG
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_DBR_2_3.RadioValue()==This.w_FILT_DBR)
            This.oPgFrm.Page2.oPag.oFILT_DBR_2_3.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_DBW_2_4.RadioValue()==This.w_FILT_DBW)
            This.oPgFrm.Page2.oPag.oFILT_DBW_2_4.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_DBI_2_5.RadioValue()==This.w_FILT_DBI)
            This.oPgFrm.Page2.oPag.oFILT_DBI_2_5.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_DBD_2_6.RadioValue()==This.w_FILT_DBD)
            This.oPgFrm.Page2.oPag.oFILT_DBD_2_6.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oRIGHE_2_7.Value==This.w_RIGHE)
            This.oPgFrm.Page2.oPag.oRIGHE_2_7.Value=This.w_RIGHE
        Endif
        If Not(This.oPgFrm.Page2.oPag.oMILLISECONDI_2_8.Value==This.w_MILLISECONDI)
            This.oPgFrm.Page2.oPag.oMILLISECONDI_2_8.Value=This.w_MILLISECONDI
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_VQR_2_9.RadioValue()==This.w_FILT_VQR)
            This.oPgFrm.Page2.oPag.oFILT_VQR_2_9.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_GSO_2_10.RadioValue()==This.w_FILT_GSO)
            This.oPgFrm.Page2.oPag.oFILT_GSO_2_10.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_GSD_2_11.RadioValue()==This.w_FILT_GSD)
            This.oPgFrm.Page2.oPag.oFILT_GSD_2_11.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_BTO_2_12.RadioValue()==This.w_FILT_BTO)
            This.oPgFrm.Page2.oPag.oFILT_BTO_2_12.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_BTD_2_13.RadioValue()==This.w_FILT_BTD)
            This.oPgFrm.Page2.oPag.oFILT_BTD_2_13.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_DBM_2_14.RadioValue()==This.w_FILT_DBM)
            This.oPgFrm.Page2.oPag.oFILT_DBM_2_14.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_DBT_2_15.RadioValue()==This.w_FILT_DBT)
            This.oPgFrm.Page2.oPag.oFILT_DBT_2_15.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_DBX_2_16.RadioValue()==This.w_FILT_DBX)
            This.oPgFrm.Page2.oPag.oFILT_DBX_2_16.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_TBT_2_18.RadioValue()==This.w_FILT_TBT)
            This.oPgFrm.Page2.oPag.oFILT_TBT_2_18.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_TCT_2_19.RadioValue()==This.w_FILT_TCT)
            This.oPgFrm.Page2.oPag.oFILT_TCT_2_19.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_TRT_2_20.RadioValue()==This.w_FILT_TRT)
            This.oPgFrm.Page2.oPag.oFILT_TRT_2_20.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_ASI_2_21.RadioValue()==This.w_FILT_ASI)
            This.oPgFrm.Page2.oPag.oFILT_ASI_2_21.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_RIC_2_22.RadioValue()==This.w_FILT_RIC)
            This.oPgFrm.Page2.oPag.oFILT_RIC_2_22.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_DEA_2_23.RadioValue()==This.w_FILT_DEA)
            This.oPgFrm.Page2.oPag.oFILT_DEA_2_23.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oCONTENUTOINFRASESQL_2_31.Value==This.w_CONTENUTOINFRASESQL)
            This.oPgFrm.Page2.oPag.oCONTENUTOINFRASESQL_2_31.Value=This.w_CONTENUTOINFRASESQL
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_TASTIFUN_2_35.RadioValue()==This.w_FILT_TASTIFUN)
            This.oPgFrm.Page2.oPag.oFILT_TASTIFUN_2_35.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oFILT_EVENT_2_36.RadioValue()==This.w_FILT_EVENT)
            This.oPgFrm.Page2.oPag.oFILT_EVENT_2_36.SetRadio()
        Endif
        cp_SetControlsValueExtFlds(This,'')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
            Else
                If Not(i_bnoChk)
                    cp_ErrorMsg(i_cErrorMsg)
                Endif
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

Enddefine

* --- Define pages as container
Define Class tcp_viewtracePag1 As StdContainer
    Width  = 791
    Height = 541
    stdWidth  = 791
    stdheight = 541
    resizeXpos=344
    resizeYpos=231
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.

    Add Object oALCMDSQL_1_2 As StdMemo With uid="ZWWLMXPBDG",rtseq=2,rtrep=.F.,;
        cFormVar = "w_ALCMDSQL", cQueryName = "ALCMDSQL",Enabled=.F.,;
        bObbl = .F. , nPag = 1, Value=Space(0), bMultilanguage =  .F.,;
        HelpContextID = 243787634,;
        bGlobalFont=.T.,;
        Height=75, Width=631, Left=1, Top=462, ReadOnly=.T., Enabled=.T.


    Add Object ELENCO As cp_zoombox_log With uid="OPFFMBUHES",Left=-3, Top=10, Width=799,Height=431,;
        caption='Object',;
        bOptions=.F.,bAdvOptions=.F.,bReadOnly=.F.,bQueryOnLoad=.T.,cMenuFile="",cZoomOnZoom="",cTable="TMP_DBFLOG",bRetriveAllRows=.F.,cZoomFile="CP_VIEWTRACE",;
        cEvent = "PopolaZoom",;
        nPag=1;
        , HelpContextID = 183043462;
        , bGlobalFont=.T.



    Add Object oBtn_1_7 As StdButton With uid="PLRGXQKEQF",Left=637, Top=446, Width=48,Height=45,;
        Picture="VISUALIZ.bmp", Caption="", nPag=1;
        , ToolTipText = "Mostra il risultato dell'istruzione eseguita";
        , HelpContextID = 57049166;
        , Caption='\<Dati';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_7.Click()
        With This.Parent.oContained
            cp_viewtrace_b(This.Parent.oContained,"D")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_7.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (Not Empty( .w_DATI ))
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_8 As StdButton With uid="HKYUBHHNJZ",Left=637, Top=496, Width=48,Height=45,;
        Picture="new_interrogT.bmp", Caption="", nPag=1;
        , ToolTipText = "Azzera il log";
        , HelpContextID = 63510422;
        , Caption='\<Azzera';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_8.Click()
        With This.Parent.oContained
            cp_viewtrace_b(This.Parent.oContained,"Z")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_1_10 As StdButton With uid="VDPQDABGNJ",Left=690, Top=446, Width=48,Height=45,;
        Picture="REQUERY.bmp", Caption="", nPag=1;
        , ToolTipText = "Cerca la riga con la frase sql o il messaggio di errore contenente l'espressione richiesta a video dopo la pressione del pulsante";
        , HelpContextID = 206371434;
        , Caption='\<Ricerca';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_10.Click()
        With This.Parent.oContained
            cp_viewtrace_b(This.Parent.oContained,"R")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_1_11 As StdButton With uid="JWUPQWSTHH",Left=690, Top=496, Width=48,Height=45,;
        Picture="REQUERY.bmp", Caption="", nPag=1;
        , ToolTipText = "Cerca la riga successiva con la frase sql o il messaggio di errore contenente l'espressione specificata";
        , HelpContextID = 14210664;
        , Caption='\<Successivo';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_11.Click()
        With This.Parent.oContained
            cp_viewtrace_b(This.Parent.oContained,"S")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_11.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (.w_TROVASUCCESSIVO)
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_13 As StdButton With uid="OLNMHXADLC",Left=742, Top=446, Width=48,Height=45,;
        Picture="REQUERY.bmp", Caption="", nPag=1;
        , ToolTipText = "Ripete l'interrogazione";
        , HelpContextID = 236355557;
        , Caption='\<Interroga';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_13.Click()
        With This.Parent.oContained
            .NotifyEvent("PopolaZoom")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_1_14 As StdButton With uid="PTAFAFGHOT",Left=742, Top=496, Width=48,Height=45,;
        Picture="ESC.BMP", Caption="", nPag=1;
        , ToolTipText = "Chiude la maschera";
        , HelpContextID = 91652174;
        , Caption='\<Esci';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_14.Click()
        =cp_StandardFunction(This,"Quit")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object oStr_1_9 As StdString With uid="EWTEYNTLSQ",Visible=.T., Left=2, Top=443,;
        Alignment=0, Width=133, Height=18,;
        Caption="Comando SQL"  ;
        , bGlobalFont=.T.
Enddefine
Define Class tcp_viewtracePag2 As StdContainer
    Width  = 791
    Height = 541
    stdWidth  = 791
    stdheight = 541
    resizeXpos=712
    resizeYpos=321
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.

    Add Object oFILEDILOG_2_2 As StdField With uid="PPVUSBZQTE",rtseq=6,rtrep=.F.,;
        cFormVar = "w_FILEDILOG", cQueryName = "FILEDILOG",;
        bObbl = .F. , nPag = 2, Value=Space(250), bMultilanguage =  .F.,;
        HelpContextID = 263549590,;
        bGlobalFont=.T.,;
        Height=21, Width=363, Left=136, Top=10, InputMask=Replicate('X',250), bHasZoom = .T.

    Proc oFILEDILOG_2_2.mZoom
        With This.Parent.oContained
            cp_viewtrace_b(This.Parent.oContained, "F" )
        Endwith
        If !Isnull(This.Parent.oContained)
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object oFILT_DBR_2_3 As StdCheck With uid="CPFRTCCADS",rtseq=7,rtrep=.F.,Left=29, Top=84, Caption="Select",;
        ToolTipText = "Visualizza frasi select SQL",;
        HelpContextID = 69408215,;
        cFormVar="w_FILT_DBR", bObbl = .F. , nPag = 2;
        , BackColor=4145087;
        , bGlobalFont=.T.


    Func oFILT_DBR_2_3.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_DBR_2_3.GetRadio()
        This.Parent.oContained.w_FILT_DBR = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_DBR_2_3.SetRadio()
        This.Parent.oContained.w_FILT_DBR=Trim(This.Parent.oContained.w_FILT_DBR)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_DBR=='S',1,;
            0)
    Endfunc

    Add Object oFILT_DBW_2_4 As StdCheck With uid="BTMMWBCGQO",rtseq=8,rtrep=.F.,Left=29, Top=110, Caption="Update",;
        ToolTipText = "Visualizza frasi update SQL",;
        HelpContextID = 69080535,;
        cFormVar="w_FILT_DBW", bObbl = .F. , nPag = 2;
        , BackColor=4145087;
        , bGlobalFont=.T.


    Func oFILT_DBW_2_4.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_DBW_2_4.GetRadio()
        This.Parent.oContained.w_FILT_DBW = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_DBW_2_4.SetRadio()
        This.Parent.oContained.w_FILT_DBW=Trim(This.Parent.oContained.w_FILT_DBW)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_DBW=='S',1,;
            0)
    Endfunc

    Add Object oFILT_DBI_2_5 As StdCheck With uid="SNHRIQSKCP",rtseq=9,rtrep=.F.,Left=29, Top=136, Caption="Insert",;
        ToolTipText = "Visualizza frasi insert SQL",;
        HelpContextID = 69998039,;
        cFormVar="w_FILT_DBI", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_DBI_2_5.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_DBI_2_5.GetRadio()
        This.Parent.oContained.w_FILT_DBI = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_DBI_2_5.SetRadio()
        This.Parent.oContained.w_FILT_DBI=Trim(This.Parent.oContained.w_FILT_DBI)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_DBI=='S',1,;
            0)
    Endfunc

    Add Object oFILT_DBD_2_6 As StdCheck With uid="LKEHYFCFRE",rtseq=10,rtrep=.F.,Left=29, Top=162, Caption="Delete",;
        ToolTipText = "Visualizza frasi delete SQL",;
        HelpContextID = 70325719,;
        cFormVar="w_FILT_DBD", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_DBD_2_6.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_DBD_2_6.GetRadio()
        This.Parent.oContained.w_FILT_DBD = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_DBD_2_6.SetRadio()
        This.Parent.oContained.w_FILT_DBD=Trim(This.Parent.oContained.w_FILT_DBD)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_DBD=='S',1,;
            0)
    Endfunc

    Add Object oRIGHE_2_7 As StdField With uid="ZYQSVUKXYD",rtseq=11,rtrep=.F.,;
        cFormVar = "w_RIGHE", cQueryName = "RIGHE",;
        bObbl = .F. , nPag = 2, Value=0, bMultilanguage =  .F.,;
        ToolTipText = "Numero minimo di righe interessate per cui l'istruzione SQLviene tracciata",;
        HelpContextID = 205948029,;
        bGlobalFont=.T.,;
        Height=21, Width=131, Left=169, Top=453, cSayPict='"999999999999999999"', cGetPict='"999999999999999999"'

    Add Object oMILLISECONDI_2_8 As StdField With uid="FAHYKCYVRA",rtseq=12,rtrep=.F.,;
        cFormVar = "w_MILLISECONDI", cQueryName = "MILLISECONDI",;
        bObbl = .F. , nPag = 2, Value=0, bMultilanguage =  .F.,;
        ToolTipText = "Durata minima in millisecondi per cui l'istruzione SQLviene tracciata",;
        HelpContextID = 254683146,;
        bGlobalFont=.T.,;
        Height=21, Width=131, Left=169, Top=479, cSayPict='"999999999999999999"', cGetPict='"999999999999999999"'

    Add Object oFILT_VQR_2_9 As StdCheck With uid="EOIEVJSAQD",rtseq=13,rtrep=.F.,Left=29, Top=188, Caption="VQR",;
        ToolTipText = "Visualizza esecuzioni di visual query",;
        HelpContextID = 69342167,;
        cFormVar="w_FILT_VQR", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_VQR_2_9.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_VQR_2_9.GetRadio()
        This.Parent.oContained.w_FILT_VQR = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_VQR_2_9.SetRadio()
        This.Parent.oContained.w_FILT_VQR=Trim(This.Parent.oContained.w_FILT_VQR)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_VQR=='S',1,;
            0)
    Endfunc

    Add Object oFILT_GSO_2_10 As StdCheck With uid="SDOQPCEGOO",rtseq=14,rtrep=.F.,Left=227, Top=84, Caption="Apertura gestione",;
        ToolTipText = "Visualizza l'apertura a video dei form",;
        HelpContextID = 69534423,;
        cFormVar="w_FILT_GSO", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_GSO_2_10.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_GSO_2_10.GetRadio()
        This.Parent.oContained.w_FILT_GSO = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_GSO_2_10.SetRadio()
        This.Parent.oContained.w_FILT_GSO=Trim(This.Parent.oContained.w_FILT_GSO)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_GSO=='S',1,;
            0)
    Endfunc

    Add Object oFILT_GSD_2_11 As StdCheck With uid="NNIOVSGXJL",rtseq=15,rtrep=.F.,Left=227, Top=110, Caption="Chiusura gestione",;
        ToolTipText = "Visualizza la chiusura dei form",;
        HelpContextID = 70255319,;
        cFormVar="w_FILT_GSD", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_GSD_2_11.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_GSD_2_11.GetRadio()
        This.Parent.oContained.w_FILT_GSD = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_GSD_2_11.SetRadio()
        This.Parent.oContained.w_FILT_GSD=Trim(This.Parent.oContained.w_FILT_GSD)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_GSD=='S',1,;
            0)
    Endfunc

    Add Object oFILT_BTO_2_12 As StdCheck With uid="AUZKUQFIIH",rtseq=16,rtrep=.F.,Left=227, Top=136, Caption="Lancio routine",;
        ToolTipText = "Visualizza l'avvio delle routine",;
        HelpContextID = 69531607,;
        cFormVar="w_FILT_BTO", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_BTO_2_12.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_BTO_2_12.GetRadio()
        This.Parent.oContained.w_FILT_BTO = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_BTO_2_12.SetRadio()
        This.Parent.oContained.w_FILT_BTO=Trim(This.Parent.oContained.w_FILT_BTO)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_BTO=='S',1,;
            0)
    Endfunc

    Add Object oFILT_BTD_2_13 As StdCheck With uid="HEVDXZKNQR",rtseq=17,rtrep=.F.,Left=227, Top=162, Caption="Chiusura routine",;
        ToolTipText = "Visualizza la chiusura delle routine",;
        HelpContextID = 70252503,;
        cFormVar="w_FILT_BTD", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_BTD_2_13.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_BTD_2_13.GetRadio()
        This.Parent.oContained.w_FILT_BTD = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_BTD_2_13.SetRadio()
        This.Parent.oContained.w_FILT_BTD=Trim(This.Parent.oContained.w_FILT_BTD)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_BTD=='S',1,;
            0)
    Endfunc

    Add Object oFILT_DBM_2_14 As StdCheck With uid="YHTAORWGMF",rtseq=18,rtrep=.F.,Left=498, Top=84, Caption="Modifica DB",;
        ToolTipText = "Visualizza frasi di ricostruzione database",;
        HelpContextID = 69735895,;
        cFormVar="w_FILT_DBM", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_DBM_2_14.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_DBM_2_14.GetRadio()
        This.Parent.oContained.w_FILT_DBM = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_DBM_2_14.SetRadio()
        This.Parent.oContained.w_FILT_DBM=Trim(This.Parent.oContained.w_FILT_DBM)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_DBM=='S',1,;
            0)
    Endfunc

    Add Object oFILT_DBT_2_15 As StdCheck With uid="XTULRTLHBE",rtseq=19,rtrep=.F.,Left=498, Top=110, Caption="Creazione temporaneo",;
        ToolTipText = "Visualizza frasi di creazione cursore lato server",;
        HelpContextID = 69277143,;
        cFormVar="w_FILT_DBT", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_DBT_2_15.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_DBT_2_15.GetRadio()
        This.Parent.oContained.w_FILT_DBT = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_DBT_2_15.SetRadio()
        This.Parent.oContained.w_FILT_DBT=Trim(This.Parent.oContained.w_FILT_DBT)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_DBT=='S',1,;
            0)
    Endfunc

    Add Object oFILT_DBX_2_16 As StdCheck With uid="NFVDKRBHJZ",rtseq=20,rtrep=.F.,Left=498, Top=136, Caption="Altre istruzioni sul DB",;
        ToolTipText = "Visualizza altre frasi eseguite dal database",;
        HelpContextID = 69014999,;
        cFormVar="w_FILT_DBX", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_DBX_2_16.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_DBX_2_16.GetRadio()
        This.Parent.oContained.w_FILT_DBX = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_DBX_2_16.SetRadio()
        This.Parent.oContained.w_FILT_DBX=Trim(This.Parent.oContained.w_FILT_DBX)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_DBX=='S',1,;
            0)
    Endfunc

    Add Object oFILT_TBT_2_18 As StdCheck With uid="VDDLEILTIF",rtseq=21,rtrep=.F.,Left=498, Top=162, Caption="Begin transaction",;
        ToolTipText = "Visualizza le istruzioni di begin transaction",;
        HelpContextID = 69273047,;
        cFormVar="w_FILT_TBT", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_TBT_2_18.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_TBT_2_18.GetRadio()
        This.Parent.oContained.w_FILT_TBT = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_TBT_2_18.SetRadio()
        This.Parent.oContained.w_FILT_TBT=Trim(This.Parent.oContained.w_FILT_TBT)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_TBT=='S',1,;
            0)
    Endfunc

    Add Object oFILT_TCT_2_19 As StdCheck With uid="XTZKHKVBWE",rtseq=22,rtrep=.F.,Left=498, Top=188, Caption="Commit",;
        ToolTipText = "Visualizza le commit",;
        HelpContextID = 69268951,;
        cFormVar="w_FILT_TCT", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_TCT_2_19.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_TCT_2_19.GetRadio()
        This.Parent.oContained.w_FILT_TCT = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_TCT_2_19.SetRadio()
        This.Parent.oContained.w_FILT_TCT=Trim(This.Parent.oContained.w_FILT_TCT)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_TCT=='S',1,;
            0)
    Endfunc

    Add Object oFILT_TRT_2_20 As StdCheck With uid="SHUBUUQZWL",rtseq=23,rtrep=.F.,Left=498, Top=214, Caption="Rollback",;
        ToolTipText = "Visualizza le rollback",;
        HelpContextID = 69207511,;
        cFormVar="w_FILT_TRT", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_TRT_2_20.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_TRT_2_20.GetRadio()
        This.Parent.oContained.w_FILT_TRT = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_TRT_2_20.SetRadio()
        This.Parent.oContained.w_FILT_TRT=Trim(This.Parent.oContained.w_FILT_TRT)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_TRT=='S',1,;
            0)
    Endfunc

    Add Object oFILT_ASI_2_21 As StdCheck With uid="WQCXVHKVNW",rtseq=24,rtrep=.F.,Left=498, Top=240, Caption="Query asincrona",;
        ToolTipText = "Visualizza query asincrone (elenchi, zoom)",;
        HelpContextID = 69929175,;
        cFormVar="w_FILT_ASI", bObbl = .F. , nPag = 2;
        , BackColor=4145087;
        , bGlobalFont=.T.


    Func oFILT_ASI_2_21.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_ASI_2_21.GetRadio()
        This.Parent.oContained.w_FILT_ASI = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_ASI_2_21.SetRadio()
        This.Parent.oContained.w_FILT_ASI=Trim(This.Parent.oContained.w_FILT_ASI)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_ASI=='S',1,;
            0)
    Endfunc

    Add Object oFILT_RIC_2_22 As StdCheck With uid="YBPEWVJVRZ",rtseq=25,rtrep=.F.,Left=498, Top=292, Caption="Riconnessione",;
        ToolTipText = "Visualizza le riconnessioni in seguito a cadute di connessione",;
        HelpContextID = 70358999,;
        cFormVar="w_FILT_RIC", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_RIC_2_22.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_RIC_2_22.GetRadio()
        This.Parent.oContained.w_FILT_RIC = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_RIC_2_22.SetRadio()
        This.Parent.oContained.w_FILT_RIC=Trim(This.Parent.oContained.w_FILT_RIC)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_RIC=='S',1,;
            0)
    Endfunc

    Add Object oFILT_DEA_2_23 As StdCheck With uid="KYXPJVHKIS",rtseq=26,rtrep=.F.,Left=498, Top=266, Caption="Deadlock",;
        ToolTipText = "Visualizza i deadlock",;
        HelpContextID = 70510039,;
        cFormVar="w_FILT_DEA", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_DEA_2_23.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_DEA_2_23.GetRadio()
        This.Parent.oContained.w_FILT_DEA = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_DEA_2_23.SetRadio()
        This.Parent.oContained.w_FILT_DEA=Trim(This.Parent.oContained.w_FILT_DEA)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_DEA=='S',1,;
            0)
    Endfunc


    Add Object oBtn_2_24 As StdButton With uid="ELWERNFDNW",Left=740, Top=445, Width=48,Height=45,;
        Picture="REQUERY.bmp", Caption="", nPag=2;
        , ToolTipText = "Ripete l'interrogazione";
        , HelpContextID = 236355557;
        , Caption='\<Interroga';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_2_24.Click()
        With This.Parent.oContained
            .NotifyEvent("PopolaZoom")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_2_25 As StdButton With uid="WUITYAGFGU",Left=740, Top=349, Width=48,Height=45,;
        Picture="REQUERY.bmp", Caption="", nPag=2;
        , ToolTipText = "Cerca la riga con la frase sql o il messaggio di errore contenente l'espressione specificata";
        , HelpContextID = 206371434;
        , Caption='\<Ricerca';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_2_25.Click()
        With This.Parent.oContained
            cp_viewtrace_b(This.Parent.oContained,"Q")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_2_27 As StdButton With uid="OZWDZVHQXJ",Left=740, Top=494, Width=48,Height=45,;
        Picture="ESC.BMP", Caption="", nPag=2;
        , ToolTipText = "Chiude la maschera";
        , HelpContextID = 91652174;
        , Caption='\<Esci';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_2_27.Click()
        =cp_StandardFunction(This,"Quit")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object oCONTENUTOINFRASESQL_2_31 As StdField With uid="ZWYULJVRRW",rtseq=27,rtrep=.F.,;
        cFormVar = "w_CONTENUTOINFRASESQL", cQueryName = "CONTENUTOINFRASESQL",;
        bObbl = .F. , nPag = 2, Value=Space(250), bMultilanguage =  .F.,;
        ToolTipText = "Si posiziona sulla frase SQL contenente l'espressione",;
        HelpContextID = 102406383,;
        bGlobalFont=.T.,;
        Height=21, Width=363, Left=136, Top=360, InputMask=Replicate('X',250)

    Add Object oFILT_TASTIFUN_2_35 As StdCheck With uid="YEGZHGGUDW",rtseq=30,rtrep=.F.,Left=227, Top=188, Caption="Tasti funzione",;
        ToolTipText = "Tasti funzione",;
        HelpContextID = 169733982,;
        cFormVar="w_FILT_TASTIFUN", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_TASTIFUN_2_35.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_TASTIFUN_2_35.GetRadio()
        This.Parent.oContained.w_FILT_TASTIFUN = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_TASTIFUN_2_35.SetRadio()
        This.Parent.oContained.w_FILT_TASTIFUN=Trim(This.Parent.oContained.w_FILT_TASTIFUN)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_TASTIFUN=='S',1,;
            0)
    Endfunc

    Add Object oFILT_EVENT_2_36 As StdCheck With uid="XVWDLGRVSD",rtseq=31,rtrep=.F.,Left=227, Top=214, Caption="Eventi",;
        ToolTipText = "Visualizza gli eventi",;
        HelpContextID = 78719785,;
        cFormVar="w_FILT_EVENT", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oFILT_EVENT_2_36.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oFILT_EVENT_2_36.GetRadio()
        This.Parent.oContained.w_FILT_EVENT = This.RadioValue()
        Return .T.
    Endfunc

    Func oFILT_EVENT_2_36.SetRadio()
        This.Parent.oContained.w_FILT_EVENT=Trim(This.Parent.oContained.w_FILT_EVENT)
        This.Value = ;
            iif(This.Parent.oContained.w_FILT_EVENT=='S',1,;
            0)
    Endfunc

    Add Object oStr_2_1 As StdString With uid="MLJYOZTCIX",Visible=.T., Left=14, Top=12,;
        Alignment=1, Width=118, Height=18,;
        Caption="File di log:"  ;
        , bGlobalFont=.T.

    Add Object oStr_2_17 As StdString With uid="ZPHILCBLSZ",Visible=.T., Left=18, Top=52,;
        Alignment=0, Width=75, Height=19,;
        Caption="Filtri"  ;
        , FontName = "Arial", FontSize = 10, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_2_28 As StdString With uid="OEIPNCMTOD",Visible=.T., Left=19, Top=326,;
        Alignment=0, Width=65, Height=19,;
        Caption="Ricerca"  ;
        , FontName = "Arial", FontSize = 10, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_2_30 As StdString With uid="SZCYOQGWPL",Visible=.T., Left=13, Top=362,;
        Alignment=1, Width=119, Height=18,;
        Caption="Cerca in frase SQL:"  ;
        , bGlobalFont=.T.

    Add Object oStr_2_32 As StdString With uid="WGBEEEDINI",Visible=.T., Left=27, Top=454,;
        Alignment=1, Width=136, Height=18,;
        Caption="Nro minimo righe:"  ;
        , bGlobalFont=.T.

    Add Object oStr_2_33 As StdString With uid="WCWIWEYKKB",Visible=.T., Left=26, Top=480,;
        Alignment=1, Width=138, Height=18,;
        Caption="Durata minima:"  ;
        , bGlobalFont=.T.

    Add Object oBox_2_26 As StdBox With uid="ABRXYWOJLS",Left=16, Top=71, Width=755,Height=2

    Add Object oBox_2_29 As StdBox With uid="VNCPNIFARW",Left=16, Top=345, Width=755,Height=2
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_viewtrace','','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Dialog Window"
Endproc

* --- Area Manuale = Functions & Procedures
* --- cp_viewtrace
Define Class cp_zoombox_log As cp_zoombox

    * Ridefinisce la procedura Query, popolando cCursor da DBF invece che da __sqlx__.SQLExec(this.nConn,s,this.cCursor,this.cDatabaseType)

    Proc Query(initsel,bSameTable,i_bNoFilter,i_bDontFill,i_bCreateHeader,i_bNotRemoveObject)
        * --- esegue la query sull' archivio principale, creando il cursore su cui operera'
        Local fld,ord,whe,i,j,k,del,op,Key,aFlds,i_timedmsg,s,i_tmpcursor,whe_t,bReCreateHeader, bThisFormLockScreen
        Dimension aFlds[1]
        This.o_initsel=initsel
        If Empty(This.cSymFile)
            Return
        Endif
        bThisFormLockScreen=Thisform.LockScreen
        Thisform.LockScreen=.T.
        This.grd.Enabled=.F.
        If Not i_bNotRemoveObject
            Local cObjManyHeaderName
            cObjManyHeaderName="oManyHeader_"+This.Name
            If Vartype(This.oRefHeaderObject)='O' And (Vartype(i_bNoreMoveDblClick)<>'L' Or Not i_bNoreMoveDblClick)
                This.oRefHeaderObject.Parent.RemoveObject(cObjManyHeaderName)
                This.oRefHeaderObject=Null
                bReCreateHeader=.T.
            Else
                bReCreateHeader=.F.
            Endif
        Endif
        * --- se e' sulla stessa tabella, crea un cursore fasullo che ottimizza la riscrittura della griglia
        If bSameTable And Used(This.cCursor)
            Select (This.cCursor)
            =Afields(aFlds)
            i_tmpcursor=Sys(2015)
            Create Cursor (i_tmpcursor) From Array aFlds
            This.grd.RecordSource=i_tmpcursor
            Select (This.cCursor)
        Else
            bSameTable=.F.
        Endif
        * --- i campi da selezionare
        If This.bFillFields
            fld=This.cStartFields && '*'
        Else
            fld=''
            *--- Rileggo XDC
            Cp_ReadXdc()
            For i=1 To This.nFields
                If Isnull(This.oCpQuery) And i_dcx.IsFieldNameTranslate(This.cSymFile,This.cFields(i))
                    fld=fld+Iif(i=1,'',',')+cp_TransLinkFldName(This.cFields(i))+' as '+This.cFields(i)
                Else
                    fld=fld+Iif(i=1,'',',')+This.cFields(i)
                Endif
            Next
            If Empty(fld)
                fld='*'
            Endif
        Endif
        * -- l' ordine da utilizzare
        ord=''
        For i=1 To This.nOrderBy
            j=0
            * e' preferibile utilizzare il numero del campo piuttosto che il nome:
            * meno problemi in query complesse con campi qualificati o con union e piu' veloce
            If Isnull(This.oCpQuery)
                For k=1 To This.nFields
                    If Lower(This.cOrderBy[i])==Lower(This.cFields[k])
                        j=k
                    Endif
                Next
            Else
                * --- se ho la query vado a ricerca l'alias....
                For k=1 To This.oCpQuery.nFields
                    If Lower(This.cOrderBy[i])==Lower(This.oCpQuery.i_Fields[k,2])
                        j=k
                    Endif
                Next
            Endif
            ord=ord+Iif(i=1,'',',')+Iif(j=0,This.cOrderBy[i],Alltrim(Str(j)))
            If This.bOrderDesc[i]
                ord=ord+' desc'
            Endif
        Next
        *--- mancava il controllo della variabile w_ORERBY che modifica l'ordinamento
        If Not Isnull(This.oCpQuery) And Not Empty(This.oCpQuery.cOrderByTmp)
            k=This.oCpQuery.cOrderByTmp
            i=At(' desc',Lower(k))
            If i<>0
                k=Left(k,i-1)
            Endif
            If At(' '+k+' ',ord)=0
                ord = ord+Iif(Empty(ord),'',',')+This.oCpQuery.cOrderByTmp
            Endif
        Endif
        * --- costruisce la where
        whe=''
        op=''
        For i=1 To This.nWhere
            whe_t=This.cWhere(i)
            If Right(Trim(whe_t),2)=']]'
                whe_t=Left(whe_t,At('[[',whe_t)-1)
            Endif
            If !(whe_t=='OR')
                If This.nConn<>0
                    whe=whe+op+Trim(This.toodbc(whe_t))
                Else
                    whe=whe+op+Trim(whe_t)
                Endif
                op=' and '
            Else
                op=Iif(!Empty(op),' or ',op)  && la or non puo' essere al primo posto
            Endif
        Next
        * ---
        If Isnull(This.oCpQuery)
            If Vartype(initsel)='C' And !Empty(initsel)
                * --- caso di query inizializzata dall' esterno (zoom standard su selezioni aperte)
                whe=Iif(Empty(whe),initsel,'('+whe+') and ('+initsel+')')
            Endif
            * --- costruisce la frase SQL (query interna)
            If !Empty(whe)
                whe=' where '+whe
            Endif
            If !Empty(ord)
                ord=' order by '+ord
            Endif
            * --- crea effettivamente la frase SQL
            If This.bModSQL And !Empty(This.cSQL)
                * --- Zucchetti Aulla inizio - i_Codazi non trimmato
                s=Strtran(Strtran(This.cSQL,'xxx',Alltrim(i_codazi)),Chr(13)+Chr(10),' ')
                If !Empty(whe)
                    If ' where '$s
                        s=Strtran(s,' where ',whe+' and ')
                    Else
                        s=s+whe
                    Endif
                Endif
                If !Empty(ord)
                    If ' order by '$s
                        s=Strtran(s,' order by ',ord+',')
                    Else
                        s=s+ord
                    Endif
                Endif
            Else
                s="select "+fld+" from "+This.cFile+Iif(This.nConn<>0," "," as ")+This.lFile+" "+whe+" "+ord
            Endif
            * --- esegue
            If This.nConn<>0
                *=cp_SQL(this.nConn,"select "+fld+" from "+this.cFile+" "+whe+" "+ord,this.cCursor)
                Local __sqlx__
                __sqlx__=This.GetSqlx()
                If Type("__sqlx__")<>'O'
                    cp_sql(This.nConn,s,This.cCursor)
                Else
                    *__sqlx__.SQLExec(this.nConn,s,this.cCursor,this.cDatabaseType)
                    *Parte sostitutiva, inizio
                    *Ad ogni nuova interrogazione dello zoom le ricerche per contenuto devono
                    *ricominciare, per cui occorre azzerare le variabili w_CONTENUTOINFRASESQL e w_TROVASUCCESSIVO
                    This.Parent.oContained.w_CONTENUTOINFRASESQL = ""
                    This.Parent.oContained.w_TROVASUCCESSIVO = .F.
                    If Empty(This.Parent.oContained.w_FILEDILOG)
                        * Cartella contenente i file di log
                        If Directory(i_cACTIVATEPROFILER_POSIZIONELOG)
                            This.Parent.oContained.w_CARTELLALOG=Addbs(i_cACTIVATEPROFILER_POSIZIONELOG)
                        Else
                            This.Parent.oContained.w_CARTELLALOG=Addbs(Addbs(Sys(2023))+'Logger')
                        Endif
                        * Nome fisico del DBF da mostrare
                        If File(Alltrim(This.Parent.oContained.w_CARTELLALOG)+Alltrim(i_cACTIVATEPROFILER_NOMELOG)+".DBF")
                            This.Parent.oContained.w_FILEDILOG=Alltrim(This.Parent.oContained.w_CARTELLALOG)+Alltrim(i_cACTIVATEPROFILER_NOMELOG)+".DBF"
                        Else
                            If File(This.Parent.oContained.w_CARTELLALOG+"ACTIVITYLOGGER.DBF")
                                This.Parent.oContained.w_FILEDILOG=This.Parent.oContained.w_CARTELLALOG+"ACTIVITYLOGGER.DBF"
                            Else
                                This.Parent.oContained.w_FILEDILOG=Space( 250 )
                            Endif
                        Endif
                    Endif
                    Local cCursoreDiAppoggio,l_where,l_comandodaeseguire
                    If Not Empty(This.Parent.oContained.w_FILEDILOG) And File(This.Parent.oContained.w_FILEDILOG)
                        cCursoreDiAppoggio = Sys( 2015 )
                        If Used( This.cCursor )
                            * --- copio il contenuto del cursore attuale in un cursore di appoggio, se ho un errore posso sempre ripristinare
                            l_comandodaeseguire = "SELECT * FROM " + This.cCursor + " INTO CURSOR " + cCursoreDiAppoggio
                            &l_comandodaeseguire
                            * --- Chiudo il cursore di partenza dell'elenco
                            If Vartype(This.Parent.Parent.oManyHeader_ELENCO)='O'
                                _Screen.LockScreen=.T.
                                This.Parent.Parent.RemoveObject("oManyHeader_ELENCO")
                            Endif
                            Select( This.cCursor )
                            Use
                        Endif
                        l_where = " where 1=1 "
                        If This.Parent.oContained.w_FILT_DBR = "N"
                            l_where = l_where + " AND NOT (AL__TIPO = 'DBR' AND ALTIPCON<>'A') "
                        Endif
                        If This.Parent.oContained.w_FILT_DBW = "N"
                            l_where = l_where + " AND AL__TIPO <> 'DBW' "
                        Endif
                        If This.Parent.oContained.w_FILT_DBI = "N"
                            l_where = l_where + " AND AL__TIPO <> 'DBI' "
                        Endif
                        If This.Parent.oContained.w_FILT_DBD = "N"
                            l_where = l_where + " AND AL__TIPO <> 'DBD' "
                        Endif
                        If This.Parent.oContained.w_FILT_DBM = "N"
                            l_where = l_where + " AND AL__TIPO <> 'DBM' "
                        Endif
                        If This.Parent.oContained.w_FILT_DBT = "N"
                            l_where = l_where + " AND AL__TIPO <> 'DBT' "
                        Endif
                        If This.Parent.oContained.w_FILT_DBX = "N"
                            l_where = l_where + " AND AL__TIPO <> 'DBX' "
                        Endif
                        If This.Parent.oContained.w_FILT_GSO = "N"
                            l_where = l_where + " AND AL__TIPO <> 'GSO' "
                        Endif
                        If This.Parent.oContained.w_FILT_GSD = "N"
                            l_where = l_where + " AND AL__TIPO <> 'GSD' "
                        Endif
                        If This.Parent.oContained.w_FILT_BTO = "N"
                            l_where = l_where + " AND AL__TIPO <> 'BTO' "
                        Endif
                        If This.Parent.oContained.w_FILT_BTD = "N"
                            l_where = l_where + " AND AL__TIPO <> 'BTD' "
                        Endif
                        If This.Parent.oContained.w_FILT_TBT = "N"
                            l_where = l_where + " AND AL__TIPO <> 'TBT' "
                        Endif
                        If This.Parent.oContained.w_FILT_TCT = "N"
                            l_where = l_where + " AND AL__TIPO <> 'TCT' "
                        Endif
                        If This.Parent.oContained.w_FILT_TRT = "N"
                            l_where = l_where + " AND AL__TIPO <> 'TRT' "
                        Endif
                        If This.Parent.oContained.w_FILT_ASI = "N"
                            l_where = l_where + " AND NOT (AL__TIPO = 'DBR' AND ALTIPCON='A') "
                        Endif
                        If This.Parent.oContained.w_FILT_RIC = "N"
                            l_where = l_where + " AND ALDEARIC <> 'R' "
                        Endif
                        If This.Parent.oContained.w_FILT_DEA = "N"
                            l_where = l_where + " AND ALDEARIC <> 'D' "
                        Endif
                        If This.Parent.oContained.w_FILT_VQR = "N"
                            l_where = l_where + " AND AL__TIPO <> 'VQR' "
                        Endif
                        If This.Parent.oContained.w_FILT_TASTIFUN = "N"
                            * --- i tasti funzione iniziano per U cone gli eventi
                            l_where = l_where + " AND Not(Left(AL__TIPO,1) = 'U' And AL__TIPO<>'UEV')"
                        Endif
                        If This.Parent.oContained.w_FILT_EVENT = "N"
                            l_where = l_where + " AND AL__TIPO <> 'UEV' "
                        Endif
                        If This.Parent.oContained.w_RIGHE > 0
                            l_where = l_where + " AND ALROWAFF >= " + Alltrim(Str(This.Parent.oContained.w_RIGHE)) + " "
                        Endif
                        If This.Parent.oContained.w_MILLISECONDI > 0
                            l_where = l_where + " AND ALTOTTMS >= " + Alltrim(Str(This.Parent.oContained.w_MILLISECONDI)) + " "
                        Endif
                        Local VecchioErrore2,l_comandodaeseguire,cErroreInCaricamento
                        l_comandodaeseguire = "select *, cast(ALCMDSQL as char(254)) As ALSQLCHR, cast(ALERROR as char(254)) As ALERRCHR, LEFT(JUSTFNAME(ALPROCED)+SPACE(50),50) As ALLIBNAM from " + " '"+Alltrim(This.Parent.oContained.w_FILEDILOG) +"' "+ l_where + " into cursor " + This.cCursor
                        VecchioErrore2=On('error')
                        cErroreInCaricamento=.F.
                        On Error cErroreInCaricamento=.T.
                        &l_comandodaeseguire
                        If i_AdvancedHeaderZoom And Vartype(This.Parent.Parent.oManyHeader_ELENCO)<>"O"
                            This.Parent.Parent.AddObject("oManyHeader_ELENCO","ManyHeader")
                            This.oRefHeaderObject=This.Parent.Parent.oManyHeader_ELENCO
                            This.oRefHeaderObject.InitHeader(This.grd,.T.)
                            _Screen.LockScreen=.F.
                        Endif
                        * -- La select apre una nuova sessione per il DBF di traccia, la chiudo
                        If Used( Juststem(Alltrim(This.Parent.oContained.w_FILEDILOG) ))
                            Use In(Juststem(Alltrim(This.Parent.oContained.w_FILEDILOG) ))
                        Endif
                        On Error &VecchioErrore2
                        * --- Ho avuto un errore, ripristino il vecchio cursore come cCursor
                        If cErroreInCaricamento
                            cp_ErrorMsg("Non � stato possibile caricare i dati","!","")
                            l_comandodaeseguire = "SELECT * FROM " + cCursoreDiAppoggio + " INTO CURSOR " + This.cCursor
                            &l_comandodaeseguire
                        Endif
                        * --- rimozione cursore di appoggio...
                        If Used(cCursoreDiAppoggio)
                            Use In (cCursoreDiAppoggio)
                        Endif
                        *Parte sostitutiva, fine
                    Else
                        __sqlx__.SQLExec(This.nConn,Strtran(s,'ALSQLCHR','CAST (ALCMDSQL as char(254)) as ALSQLCHR'),This.cCursor,This.cDatabaseType)
                    Endif
                Endif
            Else
                i_timedmsg=Createobject('timedmsg',cp_translate(MSG_EXECUTING_QUERY_D),3)
                s=s+Iif(' where '$s Or i_bNoFilter Or !This.bReadOnly,' nofilter','')
                &s Into Cursor (This.cCursor)
            Endif
            This.cOldSelect=Left(s,At('FROM',Upper(s)))
        Else
            * --- utilizza la query esterna
            i_timedmsg=.Null.
            This.oCpQuery.mLoadWhereTmp(whe)
            Thisform.LockScreen=bThisFormLockScreen
            * --- Se query con filtro aggiuntivo e non trovo l'alias della tabella all'interno della
            * --- visual query allora applico una ulteriore Sub select sulla query
            * --- perch� se utilizzo alias nei query filter potrebbero essere diversi da quelli specificati nella query
            Local bSub
            bSub=.F.
            If i_bSecurityRecord
                Local nIdx
                If Not Empty( initsel )
                    #If Version(5)>=900
                        nIdx=Ascan(This.oCpQuery.i_Files,This.lFile,1,1,2,15)
                    #Else
                        Local i_i
                        nIdx=0
                        For i_i=1 To Alen(This.oCpQuery.i_Files,1)
                            If  Not Empty( This.oCpQuery.i_Files[i_i,2])
                                If Lower(This.oCpQuery.i_Files[i_i,2])==Lower(This.lFile)
                                    nIdx=i_i
                                    Exit
                                Endif
                            Else
                                nIdx=0
                                Exit
                            Endif
                        Next
                    #Endif
                    bSub= (nIdx=0 )
                Endif
            Endif

            If bSub
                This.oCpQuery.nFiles=Iif(This.oCpQuery.nFiles=0,This.oCpQuery.nFilesOld,This.oCpQuery.nFiles)
                This.oCpQuery.nOrderBy=0 && Una subselect non deve avere ordinamenti...
                s='select * from ('+This.oCpQuery.mDoQuery('nocursor','Get',.Null.)+') '+Iif(This.nConn<>0," "," as ")+This.lFile+' where '+initsel+Iif(Not Empty(ord)," order by "+ord,"")
                If This.nConn<>0
                    Local __sqlx__
                    __sqlx__=This.GetSqlx()
                    If Type("__sqlx__")<>'O'
                        cp_sql(This.nConn,s,This.cCursor)
                    Else
                        __sqlx__.SQLExec(This.nConn,s,This.cCursor,This.cDatabaseType)
                    Endif
                Else
                    i_timedmsg=Createobject('timedmsg',cp_translate(MSG_EXECUTING_QUERY_D),3)
                    s=s+Iif(' where '$s Or i_bNoFilter Or !This.bReadOnly,' nofilter','')
                    &s Into Cursor (This.cCursor)
                Endif
            Else
                If Vartype(initsel)='C' And !Empty(initsel)
                    This.oCpQuery.mLoadWhereTmp(initsel)
                Endif
                This.oCpQuery.mLoadOrderByTmp(ord)
                *--- Zucchetti Aulla Inizio, query sincrona
                *this.oCpQuery.mDoQuery(this.cCursor,'Exec',this.GetSqlx(),i_bNoFilter or !this.bReadOnly)
                This.oCpQuery.mDoQuery(This.cCursor,'Exec',Iif(Type('this.bSinc') = 'L' And This.bSinc, .T.,This.GetSqlx()),i_bNoFilter Or !This.bReadOnly)
                *--- Zucchetti Aulla Fine, query sincrona
            Endif
            Thisform.LockScreen=.T.
        Endif
        If !This.bReadOnly
            Select (This.cCursor)
            Local i_tmpname
            i_tmpname=Sys(2015)
            Use Dbf() Again In 0 Alias (i_tmpname)
            Use
            Select (i_tmpname)
            Use Dbf() Again In 0 Alias (This.cCursor)
            Use
            Select (This.cCursor)
        Endif
        If !i_bDontFill Or This.nColPositions=0
            * --- riempie la lista dei campi
            If This.bFillFields And Used(This.cCursor)
                This.FillFields()
                bSameTable=.F.
                This.nColPositions=0
            Endif
            * --- riempie la griglia
            If bSameTable
                This.grd.fillagain()
            Else
                This.grd.ColumnCount=0
                If Used(This.cCursor)
                    This.grd.Fill()
                Endif
            Endif
        Endif
        If This.nColPositions=0 And Used(This.cCursor)
            This.SaveColumnStatus()
        Endif
        If Not i_bNotRemoveObject And i_AdvancedHeaderZoom
            Do Case
                Case (bReCreateHeader Or i_bCreateHeader) And i_AdvancedHeaderZoom And This.Visible And Vartype(This.oRefHeaderObject)<>"O"
                    This.Parent.AddObject(cObjManyHeaderName,"ManyHeader")
                    This.oRefHeaderObject=This.Parent.&cObjManyHeaderName
                    This.oRefHeaderObject.InitHeader(This.grd,.T.)
                Case Not (Vartype(i_bNoreMoveDblClick)<>'L' Or Not i_bNoreMoveDblClick) And This.Visible And Vartype(This.oRefHeaderObject)="O"
                    This.oRefHeaderObject.HeaderRedraw()
            Endcase
        Endif
        This.grd.Enabled=.T.
        This.bChangedFld=.F.
        If Len(whe)<=127
            This.src.ToolTipText=whe
        Else
            This.src.ToolTipText=Left(whe,124)+'...'
        Endif
        Wait Clear
        If bSameTable
            Select (i_tmpcursor)
            Use
            Select (This.cCursor)
        Endif
        Thisform.LockScreen=bThisFormLockScreen
        * --- gestione dell' avanzamento dello zoom nel caso di risultato troncato
        Local i_nrec
        If Type('i_nZoomMaxRows')='N' And Type('i_bDisableAsyncConn')<>'U' And i_bDisableAsyncConn And i_ServerConn[1,2]<>0
            i_nrec=Reccount(This.cCursor)
            If i_nrec>0 And i_nrec=i_nZoomMaxRows
                This.morerows.Visible=.T.
            Else
                This.morerows.Visible=.F.
            Endif
        Endif
        * selezione della prima riga
        This.grd.Refresh
        * ---
    Endproc

Enddefine
* --- Fine Area Manuale
