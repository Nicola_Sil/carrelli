* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_designmenu                                                   *
*              Men� Visuale                                                    *
*                                                                              *
*      Author: GiorGio Montali                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language: Visual Fox Pro                                                  *
*          OS: Windows                                                         *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-16                                                      *
* Last revis.: 2009-09-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
Return(Createobject("tcp_designmenu",oParentObject))

* --- Class definition
Define Class tcp_designmenu As StdForm
    Top    = 9
    Left   = 64

    * --- Standard Properties
    Width  = 587
    Height = 589
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2009-09-25"
    HelpContextID=250088799
    max_rt_seq=20

    * --- Constant Properties
    _IDX = 0
    cPrg = "cp_designmenu"
    cComment = "Men� Visuale"
    oParentObject = .Null.
    Icon = "mask.ico"
    *closable = .f.

    * --- Local Variables
    w_COND_AGGANCIO = .F.
    w_VOCEMENU = Space(60)
    w_NAMEPROC = Space(250)
    w_INSERTPOINT = Space(254)
    w_POSITION = Space(6)
    w_DELETED = .F.
    w_BITMAP = Space(254)
    w_MODULO = Space(254)
    w_ABILITATO = .F.
    w_MRU = Space(1)
    w_TEAROFF = Space(1)
    w_NOTE = Space(254)
    w_SYSCURSOR = Space(10)
    w_MENUNAME = Space(25)
    o_MENUNAME = Space(25)
    w_INDEX = 0
    w_USRCURSOR = Space(10)
    w_DIRECTORY = 0
    w_MENUUTENTE = Space(25)
    w_CUR_MENU = Space(10)
    w_OLD_MENU = Space(25)
    w_USRTreeView = .Null.
    w_SYSTreeView = .Null.
    * --- Area Manuale = Declare Variables
    * --- cp_designmenu
    * --- Disabilito la possibilit� di chiudere la maschera con la crocetta
    * --- in alto a destra
    Closable=.F.
    AutoCenter=.T.
    bTestMenu=.F.
    * --- Disabilito la Toolbar all'attivarsi della maschera
    Proc SetStatus()
        DoDefault()
        oCpToolBar.Enable(.F.)
    Endproc

    * --- Disabilito la Toolbar all'attivarsi della maschera
    * --- inoltre valorizzo i_curform facendola puntare alla toolbar
    * --- che ha lanciato la maschera
    Proc Activate()
        i_CurForm=This.oParentObject
        This.SelectCursor()
        oCpToolBar.Enable(.F.)
        *--- tasto destro non funziona, lo devo disabilitare
        On Key Label RIGHTMOUSE Wait Window ""
    Endproc

    Procedure editbtn()
        If This.w_COND_AGGANCIO
            * ---- Se sono sulla Root non ho padri, devo quindi testare il tipo della propriet�
            * ---- prima di utilizzarla
            If Type('this.w_USRTreeView.oTree.selectedItem.parent.index')='N' And This.w_USRTreeView.oTree.SelectedItem.Parent.Index=1
                Return(.T.) &&(this.w_DIRECTORY<>4 and this.w_VOCEMENU<>'------------')
            Else
                Return(.F.)
            Endif
        Else
            Return(.F.)
        Endif
    Endproc

    * --- Carica il bitmap, restituisce il path relativo alla EXE..
    Proc loadBit
        This.w_BITMAP = Strtran(Getfile("bmp",cp_Translate(MSG_FILE_NAME+MSG_FS),cp_Translate(MSG_OPEN),0,cp_Translate(MSG_OPEN)),Sys(5)+Sys(2003)+'\')
        This.notifyEvent('w_BITMAP LostFocus')
    Endproc

    Proc loadMn
        Local pathmenu
        pathmenu=Getfile("vmn",cp_Translate(MSG_FILE_NAME+MSG_FS),cp_Translate(MSG_OPEN),0,cp_Translate(MSG_OPEN))
        If Not Empty(pathmenu)
            This.w_MENUNAME=Upper(Alltrim(Left(pathmenu,Len(pathmenu)-4)))
            This.notifyEvent("Ricarica")
        Endif
    Endproc
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=1, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tcp_designmenuPag1","cp_designmenu",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate("Pag.1")
            .Pages(1).oPag.oContained=Thisform
        Endwith
        This.Parent.oFirstControl = This.Page1.oPag.oVOCEMENU_1_8
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
        * --- Area Manuale = Init Page Frame
        * --- cp_designmenu
        * --- Diminuisco dimensione Maschera
        If Type("i_bFox26")='L' And i_bFox26
            This.Parent.Height=415
        Endif

        * --- Translate interface...
        This.Parent.cComment=cp_Translate(MSG_VISUAL_MENU)
        * --- Fine Area Manuale
    Endproc
    Proc Init(oParentObject)
        If Vartype(m.oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        This.w_USRTreeView = This.oPgFrm.Pages(1).oPag.USRTreeView
        This.w_SYSTreeView = This.oPgFrm.Pages(1).oPag.SYSTreeView
        DoDefault()
    Proc Destroy()
        This.w_USRTreeView = .Null.
        This.w_SYSTreeView = .Null.
        DoDefault()

        * --- Open tables
    Function OpenWorkTables()
        Return(This.OpenAllTables(0))

    Procedure SetPostItConn()
        Return


        * --- Read record and initialize Form variables
    Procedure LoadRec()
    Endproc

    * --- ecpQuery
    Procedure ecpQuery()
        This.BlankRec()
        This.cFunction="Edit"
        This.SetStatus()
        If This.cFunction="Edit"
            This.mEnableControls()
        Endif
    Endproc
    * --- Esc
    Proc ecpQuit()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            This.cFunction = "Edit"
            This.oFirstControl.SetFocus()
            Return
        Endif
        * ---
        This.Hide()
        This.notifyEvent("Edit Aborted")
        This.notifyEvent("Done")
        This.Release()
    Endproc
    Proc QueryUnload()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            Nodefault
            Return
        Endif
        This.Hide()
        This.notifyEvent("Edit Aborted")
        This.notifyEvent("Done")
        This.Release()
    Proc ecpSave()
        If This.TerminateEdit() And This.CheckForm()
            This.LockScreen=.T.
            This.mReplace(.T.)
            This.LockScreen=.F.
            This.notifyEvent("Done")
            This.Release()
        Endif
    Endproc
    Func OkToQuit()
        Return .T.
    Endfunc

    * --- Blank Form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- cp_designmenu
        * Disabilito Gruppi voci di Men�
        This.bNoMenuFunction=.T.
        This.bNoMenuAction=.T.
        This.bNoMenuProperty=.T.
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        With This
            .w_COND_AGGANCIO=.F.
            .w_VOCEMENU=Space(60)
            .w_NAMEPROC=Space(250)
            .w_INSERTPOINT=Space(254)
            .w_POSITION=Space(6)
            .w_DELETED=.F.
            .w_BITMAP=Space(254)
            .w_MODULO=Space(254)
            .w_ABILITATO=.F.
            .w_MRU=Space(1)
            .w_TEAROFF=Space(1)
            .w_NOTE=Space(254)
            .w_SYSCURSOR=Space(10)
            .w_MENUNAME=Space(25)
            .w_INDEX=0
            .w_USRCURSOR=Space(10)
            .w_DIRECTORY=0
            .w_MENUUTENTE=Space(25)
            .w_CUR_MENU=Space(10)
            .w_OLD_MENU=Space(25)
            .w_COND_AGGANCIO = At('DEFAULT', .w_MENUNAME)<>0 Or At(Upper( i_CpDic) , .w_MENUNAME)<>0
            .oPgFrm.Page1.oPag.USRTreeView.Calculate()
            .DoRTCalc(2,4,.F.)
            .w_POSITION = 'P'
            .DoRTCalc(6,8,.F.)
            .w_ABILITATO = .T.
            .oPgFrm.Page1.oPag.SYSTreeView.Calculate()
            .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
            .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
            .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
            .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
            .oPgFrm.Page1.oPag.oObj_1_51.Calculate(cp_Translate(MSG_USER_MENU)+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_52.Calculate(cp_Translate(MSG_ENTRY_TITLE))
            .oPgFrm.Page1.oPag.oObj_1_53.Calculate(cp_Translate(MSG_MENU)+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_54.Calculate(cp_Translate(MSG_TITLE)+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_55.Calculate(cp_Translate(MSG_PROCEDURE_NAME))
            .oPgFrm.Page1.oPag.oObj_1_56.Calculate(cp_Translate(MSG_PROC)+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_57.Calculate(cp_Translate(MSG_PATH_LINK))
            .oPgFrm.Page1.oPag.oObj_1_58.Calculate(cp_Translate(MSG_PATH)+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_59.Calculate(cp_Translate(MSG_REL_POSITION))
            .oPgFrm.Page1.oPag.oObj_1_60.Calculate(cp_Translate(MSG_POSITION)+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_61.Calculate(cp_Translate(MSG_DELETED))
            .oPgFrm.Page1.oPag.oObj_1_62.Calculate(cp_Translate(MSG_BITMAP_MENU))
            .oPgFrm.Page1.oPag.oObj_1_63.Calculate(cp_Translate(MSG_BITMAP)+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_64.Calculate(cp_Translate(MSG_COND_ACTIVATION))
            .oPgFrm.Page1.oPag.oObj_1_65.Calculate(cp_Translate(MSG_ACTIVATION)+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_66.Calculate(cp_Translate(MSG_ENABLED_MENU))
            .oPgFrm.Page1.oPag.oObj_1_67.Calculate(cp_Translate(MSG_VISIBILITY_MRU))
            .oPgFrm.Page1.oPag.oObj_1_68.Calculate(cp_Translate(MSG_POPUP_UNBOUND))
            .oPgFrm.Page1.oPag.oObj_1_69.Calculate(cp_Translate(MSG_VISIBILITY_MRU_TOOLTIPTEXT))
            .oPgFrm.Page1.oPag.oObj_1_70.Calculate(cp_Translate(MSG_POPUP_UNBOUND_TOOLTIPTEXT))
            .oPgFrm.Page1.oPag.oObj_1_71.Calculate(cp_Translate(MSG_MENU_NOTE_TOOLTIPTEXT))
        Endwith
        This.DoRTCalc(10,20,.F.)
        This.SaveDependsOn()
        This.SetControlsValue()
        This.oPgFrm.Page1.oPag.oBtn_1_3.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_4.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_5.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_6.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_7.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_11.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_20.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_21.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_22.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_23.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_24.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_25.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_26.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_47.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
        This.mHideControls()
        This.notifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = Setting operation
    *     Allowed Parameters: Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        * --- Deactivating List page when <> da Query
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt
        i_cFlt =""
        Return (i_cFlt)
    Endfunc

    Proc QueryKeySet(i_cWhere,i_cOrderBy)
    Endproc

    Proc SelectCursor
    Proc GetXKey

        * --- Update Database
    Function mReplace(i_bEditing)
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        This.notifyEvent('Update start')
        With This
        Endwith
        This.notifyEvent('Update end')
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(.T.)

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        If i_bUpd
            With This
                If .o_MENUNAME<>.w_MENUNAME
                    .w_COND_AGGANCIO = At('DEFAULT', .w_MENUNAME)<>0 Or At(Upper( i_CpDic) , .w_MENUNAME)<>0
                Endif
                .oPgFrm.Page1.oPag.USRTreeView.Calculate()
                .oPgFrm.Page1.oPag.SYSTreeView.Calculate()
                .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
                .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
                .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
                .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
                .oPgFrm.Page1.oPag.oObj_1_51.Calculate(cp_Translate(MSG_USER_MENU)+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_52.Calculate(cp_Translate(MSG_ENTRY_TITLE))
                .oPgFrm.Page1.oPag.oObj_1_53.Calculate(cp_Translate(MSG_MENU)+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_54.Calculate(cp_Translate(MSG_TITLE)+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_55.Calculate(cp_Translate(MSG_PROCEDURE_NAME))
                .oPgFrm.Page1.oPag.oObj_1_56.Calculate(cp_Translate(MSG_PROC)+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_57.Calculate(cp_Translate(MSG_PATH_LINK))
                .oPgFrm.Page1.oPag.oObj_1_58.Calculate(cp_Translate(MSG_PATH)+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_59.Calculate(cp_Translate(MSG_REL_POSITION))
                .oPgFrm.Page1.oPag.oObj_1_60.Calculate(cp_Translate(MSG_POSITION)+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_61.Calculate(cp_Translate(MSG_DELETED))
                .oPgFrm.Page1.oPag.oObj_1_62.Calculate(cp_Translate(MSG_BITMAP_MENU))
                .oPgFrm.Page1.oPag.oObj_1_63.Calculate(cp_Translate(MSG_BITMAP)+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_64.Calculate(cp_Translate(MSG_COND_ACTIVATION))
                .oPgFrm.Page1.oPag.oObj_1_65.Calculate(cp_Translate(MSG_ACTIVATION)+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_66.Calculate(cp_Translate(MSG_ENABLED_MENU))
                .oPgFrm.Page1.oPag.oObj_1_67.Calculate(cp_Translate(MSG_VISIBILITY_MRU))
                .oPgFrm.Page1.oPag.oObj_1_68.Calculate(cp_Translate(MSG_POPUP_UNBOUND))
                .oPgFrm.Page1.oPag.oObj_1_69.Calculate(cp_Translate(MSG_VISIBILITY_MRU_TOOLTIPTEXT))
                .oPgFrm.Page1.oPag.oObj_1_70.Calculate(cp_Translate(MSG_POPUP_UNBOUND_TOOLTIPTEXT))
                .oPgFrm.Page1.oPag.oObj_1_71.Calculate(cp_Translate(MSG_MENU_NOTE_TOOLTIPTEXT))
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
            Endwith
            This.DoRTCalc(2,20,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
            .oPgFrm.Page1.oPag.USRTreeView.Calculate()
            .oPgFrm.Page1.oPag.SYSTreeView.Calculate()
            .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
            .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
            .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
            .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
            .oPgFrm.Page1.oPag.oObj_1_51.Calculate(cp_Translate(MSG_USER_MENU)+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_52.Calculate(cp_Translate(MSG_ENTRY_TITLE))
            .oPgFrm.Page1.oPag.oObj_1_53.Calculate(cp_Translate(MSG_MENU)+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_54.Calculate(cp_Translate(MSG_TITLE)+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_55.Calculate(cp_Translate(MSG_PROCEDURE_NAME))
            .oPgFrm.Page1.oPag.oObj_1_56.Calculate(cp_Translate(MSG_PROC)+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_57.Calculate(cp_Translate(MSG_PATH_LINK))
            .oPgFrm.Page1.oPag.oObj_1_58.Calculate(cp_Translate(MSG_PATH)+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_59.Calculate(cp_Translate(MSG_REL_POSITION))
            .oPgFrm.Page1.oPag.oObj_1_60.Calculate(cp_Translate(MSG_POSITION)+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_61.Calculate(cp_Translate(MSG_DELETED))
            .oPgFrm.Page1.oPag.oObj_1_62.Calculate(cp_Translate(MSG_BITMAP_MENU))
            .oPgFrm.Page1.oPag.oObj_1_63.Calculate(cp_Translate(MSG_BITMAP)+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_64.Calculate(cp_Translate(MSG_COND_ACTIVATION))
            .oPgFrm.Page1.oPag.oObj_1_65.Calculate(cp_Translate(MSG_ACTIVATION)+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_66.Calculate(cp_Translate(MSG_ENABLED_MENU))
            .oPgFrm.Page1.oPag.oObj_1_67.Calculate(cp_Translate(MSG_VISIBILITY_MRU))
            .oPgFrm.Page1.oPag.oObj_1_68.Calculate(cp_Translate(MSG_POPUP_UNBOUND))
            .oPgFrm.Page1.oPag.oObj_1_69.Calculate(cp_Translate(MSG_VISIBILITY_MRU_TOOLTIPTEXT))
            .oPgFrm.Page1.oPag.oObj_1_70.Calculate(cp_Translate(MSG_POPUP_UNBOUND_TOOLTIPTEXT))
            .oPgFrm.Page1.oPag.oObj_1_71.Calculate(cp_Translate(MSG_MENU_NOTE_TOOLTIPTEXT))
        Endwith
        Return

    Proc Calculate_JWIZBYPABW()
        With This
            * --- Legge le informazioni del nodo selezionato
            .w_INDEX = .w_USRTreeView.oTree.SelectedItem.Index
            cp_SuppMenu(This;
                ,'USRSEL';
                )
        Endwith
    Endproc
    Proc Calculate_KUVMTVSARB()
        With This
            * --- Carica Men� (avvio e da bottone in alto a destra)
            cp_SuppMenu(This;
                ,'INITMENU';
                )
        Endwith
    Endproc

    * --- Enable controls under condition
    Procedure mEnableControls()
        This.oPgFrm.Page1.oPag.oVOCEMENU_1_8.Enabled = This.oPgFrm.Page1.oPag.oVOCEMENU_1_8.mCond()
        This.oPgFrm.Page1.oPag.oNAMEPROC_1_9.Enabled = This.oPgFrm.Page1.oPag.oNAMEPROC_1_9.mCond()
        This.oPgFrm.Page1.oPag.oINSERTPOINT_1_10.Enabled = This.oPgFrm.Page1.oPag.oINSERTPOINT_1_10.mCond()
        This.oPgFrm.Page1.oPag.oPOSITION_1_12.Enabled = This.oPgFrm.Page1.oPag.oPOSITION_1_12.mCond()
        This.oPgFrm.Page1.oPag.oDELETED_1_13.Enabled = This.oPgFrm.Page1.oPag.oDELETED_1_13.mCond()
        This.oPgFrm.Page1.oPag.oBITMAP_1_14.Enabled = This.oPgFrm.Page1.oPag.oBITMAP_1_14.mCond()
        This.oPgFrm.Page1.oPag.oMODULO_1_15.Enabled = This.oPgFrm.Page1.oPag.oMODULO_1_15.mCond()
        This.oPgFrm.Page1.oPag.oABILITATO_1_16.Enabled = This.oPgFrm.Page1.oPag.oABILITATO_1_16.mCond()
        This.oPgFrm.Page1.oPag.oMRU_1_17.Enabled = This.oPgFrm.Page1.oPag.oMRU_1_17.mCond()
        This.oPgFrm.Page1.oPag.oTEAROFF_1_18.Enabled = This.oPgFrm.Page1.oPag.oTEAROFF_1_18.mCond()
        This.oPgFrm.Page1.oPag.oNOTE_1_19.Enabled = This.oPgFrm.Page1.oPag.oNOTE_1_19.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_3.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_4.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_5.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_6.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_7.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_11.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_20.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_21.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_22.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_23.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_24.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_25.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_47.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        This.oPgFrm.Page1.oPag.oINSERTPOINT_1_10.Visible=!This.oPgFrm.Page1.oPag.oINSERTPOINT_1_10.mHide()
        This.oPgFrm.Page1.oPag.oBtn_1_11.Visible=!This.oPgFrm.Page1.oPag.oBtn_1_11.mHide()
        This.oPgFrm.Page1.oPag.oPOSITION_1_12.Visible=!This.oPgFrm.Page1.oPag.oPOSITION_1_12.mHide()
        This.oPgFrm.Page1.oPag.oDELETED_1_13.Visible=!This.oPgFrm.Page1.oPag.oDELETED_1_13.mHide()
        This.oPgFrm.Page1.oPag.oBITMAP_1_14.Visible=!This.oPgFrm.Page1.oPag.oBITMAP_1_14.mHide()
        This.oPgFrm.Page1.oPag.oMODULO_1_15.Visible=!This.oPgFrm.Page1.oPag.oMODULO_1_15.mHide()
        This.oPgFrm.Page1.oPag.oABILITATO_1_16.Visible=!This.oPgFrm.Page1.oPag.oABILITATO_1_16.mHide()
        This.oPgFrm.Page1.oPag.oMRU_1_17.Visible=!This.oPgFrm.Page1.oPag.oMRU_1_17.mHide()
        This.oPgFrm.Page1.oPag.oTEAROFF_1_18.Visible=!This.oPgFrm.Page1.oPag.oTEAROFF_1_18.mHide()
        This.oPgFrm.Page1.oPag.oNOTE_1_19.Visible=!This.oPgFrm.Page1.oPag.oNOTE_1_19.mHide()
        This.oPgFrm.Page1.oPag.oBtn_1_20.Visible=!This.oPgFrm.Page1.oPag.oBtn_1_20.mHide()
        This.oPgFrm.Page1.oPag.oBtn_1_21.Visible=!This.oPgFrm.Page1.oPag.oBtn_1_21.mHide()
        This.oPgFrm.Page1.oPag.oBtn_1_22.Visible=!This.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
        This.oPgFrm.Page1.oPag.oBtn_1_23.Visible=!This.oPgFrm.Page1.oPag.oBtn_1_23.mHide()
        This.oPgFrm.Page1.oPag.oBtn_1_24.Visible=!This.oPgFrm.Page1.oPag.oBtn_1_24.mHide()
        This.oPgFrm.Page1.oPag.oBtn_1_25.Visible=!This.oPgFrm.Page1.oPag.oBtn_1_25.mHide()
        This.oPgFrm.Page1.oPag.oStr_1_33.Visible=!This.oPgFrm.Page1.oPag.oStr_1_33.mHide()
        This.oPgFrm.Page1.oPag.oStr_1_34.Visible=!This.oPgFrm.Page1.oPag.oStr_1_34.mHide()
        This.oPgFrm.Page1.oPag.oStr_1_37.Visible=!This.oPgFrm.Page1.oPag.oStr_1_37.mHide()
        This.oPgFrm.Page1.oPag.oStr_1_38.Visible=!This.oPgFrm.Page1.oPag.oStr_1_38.mHide()
        This.oPgFrm.Page1.oPag.oBtn_1_47.Visible=!This.oPgFrm.Page1.oPag.oBtn_1_47.mHide()
        DoDefault()
        Return

        * --- NotifyEvent
    Function notifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            .oPgFrm.Page1.oPag.USRTreeView.Event(cEvent)
            .oPgFrm.Page1.oPag.SYSTreeView.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
            If Lower(cEvent)==Lower("w_usrtreeview NodeSelected")
                .Calculate_JWIZBYPABW()
                bRefresh=.T.
            Endif
            If Lower(cEvent)==Lower("Avvio") Or Lower(cEvent)==Lower("Ricarica")
                .Calculate_KUVMTVSARB()
                bRefresh=.T.
            Endif
            .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_53.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_54.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_55.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_58.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_59.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_60.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_61.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_62.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_63.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_64.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_65.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_66.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_67.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_68.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_69.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_70.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_71.Event(cEvent)
        Endwith
        * --- Area Manuale = Notify Event End
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

    Function SetControlsValue()
        If Not(This.oPgFrm.Page1.oPag.oVOCEMENU_1_8.Value==This.w_VOCEMENU)
            This.oPgFrm.Page1.oPag.oVOCEMENU_1_8.Value=This.w_VOCEMENU
        Endif
        If Not(This.oPgFrm.Page1.oPag.oNAMEPROC_1_9.Value==This.w_NAMEPROC)
            This.oPgFrm.Page1.oPag.oNAMEPROC_1_9.Value=This.w_NAMEPROC
        Endif
        If Not(This.oPgFrm.Page1.oPag.oINSERTPOINT_1_10.Value==This.w_INSERTPOINT)
            This.oPgFrm.Page1.oPag.oINSERTPOINT_1_10.Value=This.w_INSERTPOINT
        Endif
        If Not(This.oPgFrm.Page1.oPag.oPOSITION_1_12.RadioValue()==This.w_POSITION)
            This.oPgFrm.Page1.oPag.oPOSITION_1_12.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oDELETED_1_13.RadioValue()==This.w_DELETED)
            This.oPgFrm.Page1.oPag.oDELETED_1_13.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oBITMAP_1_14.Value==This.w_BITMAP)
            This.oPgFrm.Page1.oPag.oBITMAP_1_14.Value=This.w_BITMAP
        Endif
        If Not(This.oPgFrm.Page1.oPag.oMODULO_1_15.Value==This.w_MODULO)
            This.oPgFrm.Page1.oPag.oMODULO_1_15.Value=This.w_MODULO
        Endif
        If Not(This.oPgFrm.Page1.oPag.oABILITATO_1_16.RadioValue()==This.w_ABILITATO)
            This.oPgFrm.Page1.oPag.oABILITATO_1_16.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oMRU_1_17.RadioValue()==This.w_MRU)
            This.oPgFrm.Page1.oPag.oMRU_1_17.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTEAROFF_1_18.RadioValue()==This.w_TEAROFF)
            This.oPgFrm.Page1.oPag.oTEAROFF_1_18.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oNOTE_1_19.Value==This.w_NOTE)
            This.oPgFrm.Page1.oPag.oNOTE_1_19.Value=This.w_NOTE
        Endif
        If Not(This.oPgFrm.Page1.oPag.oMENUNAME_1_29.Value==This.w_MENUNAME)
            This.oPgFrm.Page1.oPag.oMENUNAME_1_29.Value=This.w_MENUNAME
        Endif
        If Not(This.oPgFrm.Page1.oPag.oMENUUTENTE_1_45.Value==This.w_MENUUTENTE)
            This.oPgFrm.Page1.oPag.oMENUUTENTE_1_45.Value=This.w_MENUUTENTE
        Endif
        cp_SetControlsValueExtFlds(This,'')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
            Else
                If Not(i_bnoChk)
                    cp_ErrorMsg(i_cErrorMsg)
                Endif
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

    * --- SaveDependsOn
    Proc SaveDependsOn()
        This.o_MENUNAME = This.w_MENUNAME
        Return

Enddefine

* --- Define pages as container
Define Class tcp_designmenuPag1 As StdContainer
    Width  = 583
    Height = 589
    stdWidth  = 583
    stdheight = 589
    resizeXpos=179
    resizeYpos=256
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.


    Add Object USRTreeView As cp_TreeviewDis_Menu With uid="QFWBFQPPXP",Left=13, Top=30, Width=269,Height=304,;
        caption='TreeView',;
        cLeafBmp=g_MLEAFBMP,nIndent=20,cCursor="",cShowFields="VOCEMENU",cNodeShowField="",cLeafShowField="",cNodeBmp=g_MNODEBMP,cLvlSep=".",;
        cEvent = "UsrMenu",;
        nPag=1;
        , HelpContextID = 48488278;
        , bGlobalFont=.T.



    Add Object oBtn_1_3 As StdButton With uid="NVOBHVNQQU",Left=13, Top=338, Width=50,Height=20,;
        caption="Menu", nPag=1;
        , ToolTipText = ""+MSG_ADD_MENU+"";
        , HelpContextID = 200803745;
        , Caption=MSG_MENU_BUTTON;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_3.Click()
        With This.Parent.oContained
            cp_SuppMenu(This.Parent.oContained,"ADD",cp_Translate(MSG_NEW_MENU),1)
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_3.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (Not Empty (.w_INDEX))
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_4 As StdButton With uid="XDQIKLVXWR",Left=65, Top=338, Width=64,Height=20,;
        caption="Opzione", nPag=1;
        , ToolTipText = ""+MSG_ADD_OPT+"";
        , HelpContextID = 108284075;
        , Caption=MSG_OPTION_BUTTON;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_4.Click()
        With This.Parent.oContained
            cp_SuppMenu(This.Parent.oContained,"ADD",cp_Translate(MSG_OPTION),2)
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_4.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (Not Empty(.w_INDEX))
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_5 As StdButton With uid="DJGIJGOPLJ",Left=130, Top=338, Width=48,Height=20,;
        caption="Sep", nPag=1;
        , ToolTipText = ""+MSG_ADD_SEP+"";
        , HelpContextID = 100142319;
        , Caption=MSG_SEP_MENU;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_5.Click()
        With This.Parent.oContained
            cp_SuppMenu(This.Parent.oContained,"ADD","------------",2)
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_5.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (Not Empty (.w_INDEX))
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_6 As StdButton With uid="TUHAANZWGM",Left=181, Top=338, Width=48,Height=20,;
        caption="Seq +", nPag=1;
        , ToolTipText = ""+MSG_MOVEDOWN_MENU+"";
        , HelpContextID = 100130798;
        , Caption=MSG_SEQ_MENU+' \<+';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_6.Click()
        With This.Parent.oContained
            .w_USRTreeView.Move_Nodo(  .w_USRTreeView.oTree.Nodes( .w_INDEX ) , 1)
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_6.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (.w_INDEX>1 And Empty(.w_INSERTPOINT))
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_7 As StdButton With uid="PEQGUJPZDH",Left=230, Top=338, Width=48,Height=20,;
        caption="Seq -", nPag=1;
        , ToolTipText = ""+MSG_MOVEUP_MENU+"";
        , HelpContextID = 100130286;
        , Caption=MSG_SEQ_MENU+' \<-';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_7.Click()
        With This.Parent.oContained
            .w_USRTreeView.Move_Nodo(  .w_USRTreeView.oTree.Nodes( .w_INDEX ) , -1)
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_7.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (.w_INDEX>1 And Empty(.w_INSERTPOINT))
            Endwith
        Endif
    Endfunc

    Add Object oVOCEMENU_1_8 As StdField With uid="FHONHFRZWB",rtseq=2,rtrep=.F.,;
        cFormVar = "w_VOCEMENU", cQueryName = "VOCEMENU",;
        bObbl = .F. , nPag = 1, Value=Space(60), bMultilanguage =  .F.,;
        ToolTipText = "Titolo voce",;
        HelpContextID = 44733492,;
        bGlobalFont=.T.,;
        Height=21, Width=202, Left=79, Top=367, InputMask=Replicate('X',60)

    Func oVOCEMENU_1_8.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_INDEX>1 And .w_DIRECTORY<>4 And .w_VOCEMENU<>'------------')
            Endwith
        Endif
    Endfunc

    Add Object oNAMEPROC_1_9 As StdField With uid="LSRVZPLTRN",rtseq=3,rtrep=.F.,;
        cFormVar = "w_NAMEPROC", cQueryName = "NAMEPROC",;
        bObbl = .F. , nPag = 1, Value=Space(250), bMultilanguage =  .F.,;
        ToolTipText = "Nome procedura",;
        HelpContextID = 160196414,;
        bGlobalFont=.T.,;
        Height=21, Width=213, Left=361, Top=367, InputMask=Replicate('X',250)

    Func oNAMEPROC_1_9.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_INDEX>1 And .w_DIRECTORY=1 And .w_VOCEMENU<>'------------')
            Endwith
        Endif
    Endfunc

    Add Object oINSERTPOINT_1_10 As StdField With uid="SMHZUAJWBD",rtseq=4,rtrep=.F.,;
        cFormVar = "w_INSERTPOINT", cQueryName = "INSERTPOINT",;
        bObbl = .F. , nPag = 1, Value=Space(254), bMultilanguage =  .F.,;
        ToolTipText = "Percorso dal quale svolgere l'operazione di inserimento o di cancellazione",;
        HelpContextID = 239962520,;
        bGlobalFont=.T.,;
        Height=21, Width=429, Left=79, Top=395, InputMask=Replicate('X',254)

    Func oINSERTPOINT_1_10.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.editbtn())
            Endwith
        Endif
    Endfunc

    Func oINSERTPOINT_1_10.mHide()
        With This.Parent.oContained
            Return (Type("i_bFox26")='L' And i_bFox26)
        Endwith
    Endfunc


    Add Object oBtn_1_11 As StdButton With uid="BOQGKTOANU",Left=515, Top=396, Width=59,Height=20,;
        caption="Percorso", nPag=1;
        , ToolTipText = ""+MSG_SET_PATH_ITEM+"";
        , HelpContextID = 26048445;
        , Caption=MSG_PATH_BUTTON;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_11.Click()
        With This.Parent.oContained
            cp_SuppMenu(This.Parent.oContained,"INS")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_11.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (.editbtn())
            Endwith
        Endif
    Endfunc

    Func oBtn_1_11.mHide()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (Type("i_bFox26")='L' And i_bFox26)
            Endwith
        Endif
    Endfunc


    Add Object oPOSITION_1_12 As StdCombo With uid="GUZUTLVZXT",rtseq=5,rtrep=.F.,Left=79,Top=423,Width=98,Height=17;
        , ToolTipText = "Posizione relativa del menu che si vuole inserire";
        , HelpContextID = 205249412;
        , cFormVar="w_POSITION",RowSource=""+""+MSG_BEFORE+","+""+MSG_AFTER+"", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oPOSITION_1_12.RadioValue()
        Return(Iif(This.Value =1,'P',;
            iif(This.Value =2,'D',;
            space(6))))
    Endfunc
    Func oPOSITION_1_12.GetRadio()
        This.Parent.oContained.w_POSITION = This.RadioValue()
        Return .T.
    Endfunc

    Func oPOSITION_1_12.SetRadio()
        This.Parent.oContained.w_POSITION=Trim(This.Parent.oContained.w_POSITION)
        This.Value = ;
            iif(This.Parent.oContained.w_POSITION=='P',1,;
            iif(This.Parent.oContained.w_POSITION=='D',2,;
            0))
    Endfunc

    Func oPOSITION_1_12.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.editbtn() And Not Empty(.w_INSERTPOINT) And Not .w_DELETED)
            Endwith
        Endif
    Endfunc

    Func oPOSITION_1_12.mHide()
        With This.Parent.oContained
            Return (Type("i_bFox26")='L' And i_bFox26)
        Endwith
    Endfunc

    Add Object oDELETED_1_13 As StdCheck With uid="LVRVBVWTCW",rtseq=6,rtrep=.F.,Left=198, Top=419, Caption="Cancellato",;
        ToolTipText = ""+MSG_DELETED_ITEM_MENU+"",;
        HelpContextID = 189831997,;
        cFormVar="w_DELETED", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oDELETED_1_13.RadioValue()
        Return(Iif(This.Value =1,.T.,;
            .F.))
    Endfunc
    Func oDELETED_1_13.GetRadio()
        This.Parent.oContained.w_DELETED = This.RadioValue()
        Return .T.
    Endfunc

    Func oDELETED_1_13.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_DELETED==.T.,1,;
            0)
    Endfunc

    Func oDELETED_1_13.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_DIRECTORY<>4 And .w_VOCEMENU<>'------------' And Not Empty(.w_INSERTPOINT) And .editbtn())
            Endwith
        Endif
    Endfunc

    Func oDELETED_1_13.mHide()
        With This.Parent.oContained
            Return (Type("i_bFox26")='L' And i_bFox26)
        Endwith
    Endfunc

    Add Object oBITMAP_1_14 As StdField With uid="JXDYJOOYSR",rtseq=7,rtrep=.F.,;
        cFormVar = "w_BITMAP", cQueryName = "BITMAP",;
        bObbl = .F. , nPag = 1, Value=Space(254), bMultilanguage =  .F.,;
        ToolTipText = "Bitmap associata al menu",;
        HelpContextID = 116574011,;
        bGlobalFont=.T.,;
        Height=21, Width=193, Left=353, Top=423, InputMask=Replicate('X',254)

    Func oBITMAP_1_14.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_INDEX>1 And .w_DIRECTORY<>4 And .w_VOCEMENU<>'------------')
            Endwith
        Endif
    Endfunc

    Func oBITMAP_1_14.mHide()
        With This.Parent.oContained
            Return (Type("i_bFox26")='L' And i_bFox26)
        Endwith
    Endfunc

    Add Object oMODULO_1_15 As StdField With uid="XOHAMZSSRD",rtseq=8,rtrep=.F.,;
        cFormVar = "w_MODULO", cQueryName = "MODULO",;
        bObbl = .F. , nPag = 1, Value=Space(254), bMultilanguage =  .F.,;
        ToolTipText = "Condizione per l'attivazione",;
        HelpContextID = 67974197,;
        bGlobalFont=.T.,;
        Height=21, Width=399, Left=79, Top=448, InputMask=Replicate('X',254)

    Func oMODULO_1_15.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_INDEX>1 And .w_DIRECTORY<>4 And .w_VOCEMENU<>'------------')
            Endwith
        Endif
    Endfunc

    Func oMODULO_1_15.mHide()
        With This.Parent.oContained
            Return (Type("i_bFox26")='L' And i_bFox26)
        Endwith
    Endfunc

    Add Object oABILITATO_1_16 As StdCheck With uid="ECOLHGTDUG",rtseq=9,rtrep=.F.,Left=487, Top=448, Caption="Abilitato",;
        HelpContextID = 57769814,;
        cFormVar="w_ABILITATO", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oABILITATO_1_16.RadioValue()
        Return(Iif(This.Value =1,.T.,;
            .F.))
    Endfunc
    Func oABILITATO_1_16.GetRadio()
        This.Parent.oContained.w_ABILITATO = This.RadioValue()
        Return .T.
    Endfunc

    Func oABILITATO_1_16.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_ABILITATO==.T.,1,;
            0)
    Endfunc

    Func oABILITATO_1_16.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_INDEX>1 And .w_DIRECTORY<>4 And .w_VOCEMENU<>'------------')
            Endwith
        Endif
    Endfunc

    Func oABILITATO_1_16.mHide()
        With This.Parent.oContained
            Return (Type("i_bFox26")='L' And i_bFox26)
        Endwith
    Endfunc

    Add Object oMRU_1_17 As StdCheck With uid="HVDVAAIMFN",rtseq=10,rtrep=.F.,Left=79, Top=475, Caption="Visibilit� MRU",;
        ToolTipText = "Specifica se la voce di menu � raramente usata e non � visibile perch� � attiva l'espansione del menu",;
        HelpContextID = 67629814,;
        cFormVar="w_MRU", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oMRU_1_17.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func oMRU_1_17.GetRadio()
        This.Parent.oContained.w_MRU = This.RadioValue()
        Return .T.
    Endfunc

    Func oMRU_1_17.SetRadio()
        This.Parent.oContained.w_MRU=Trim(This.Parent.oContained.w_MRU)
        This.Value = ;
            iif(This.Parent.oContained.w_MRU=='S',1,;
            0)
    Endfunc

    Func oMRU_1_17.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_INDEX>1)
            Endwith
        Endif
    Endfunc

    Func oMRU_1_17.mHide()
        With This.Parent.oContained
            Return (.T.)
        Endwith
    Endfunc

    Add Object oTEAROFF_1_18 As StdCheck With uid="QWMADVRTNB",rtseq=11,rtrep=.F.,Left=355, Top=475, Caption="Popup sganciabile",;
        ToolTipText = "Permette al sub-popup di essere sganciato e di essere usato separatamente come una toolbar",;
        HelpContextID = 78469374,;
        cFormVar="w_TEAROFF", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oTEAROFF_1_18.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func oTEAROFF_1_18.GetRadio()
        This.Parent.oContained.w_TEAROFF = This.RadioValue()
        Return .T.
    Endfunc

    Func oTEAROFF_1_18.SetRadio()
        This.Parent.oContained.w_TEAROFF=Trim(This.Parent.oContained.w_TEAROFF)
        This.Value = ;
            iif(This.Parent.oContained.w_TEAROFF=='S',1,;
            0)
    Endfunc

    Func oTEAROFF_1_18.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_INDEX>1 And .w_DIRECTORY=2)
            Endwith
        Endif
    Endfunc

    Func oTEAROFF_1_18.mHide()
        With This.Parent.oContained
            Return (.T.)
        Endwith
    Endfunc

    Add Object oNOTE_1_19 As StdField With uid="KABTHNYAYN",rtseq=12,rtrep=.F.,;
        cFormVar = "w_NOTE", cQueryName = "NOTE",;
        bObbl = .F. , nPag = 1, Value=Space(254), bMultilanguage =  .F.,;
        ToolTipText = "Specifica il testo che viene mostrato nella status bar quando il puntatore del mouse si muove sopra una voce di menu.",;
        HelpContextID = 84408133,;
        bGlobalFont=.T.,;
        Height=79, Width=571, Left=5, Top=501, InputMask=Replicate('X',254)

    Func oNOTE_1_19.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_INDEX>1)
            Endwith
        Endif
    Endfunc

    Func oNOTE_1_19.mHide()
        With This.Parent.oContained
            Return (.T.)
        Endwith
    Endfunc


    Add Object oBtn_1_20 As StdButton With uid="LHYPBMLFXP",Left=554, Top=425, Width=20,Height=19,;
        caption="...", nPag=1;
        , HelpContextID = 184028465;
        , bGlobalFont=.T.

    Proc oBtn_1_20.Click()
        With This.Parent.oContained
            .loadBit()
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_20.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (.w_INDEX>1 And .w_DIRECTORY<>4 And .w_VOCEMENU<>'------------')
            Endwith
        Endif
    Endfunc

    Func oBtn_1_20.mHide()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (Type("i_bFox26")='L' And i_bFox26)
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_21 As StdButton With uid="UESYSCUXNU",Left=298, Top=338, Width=52,Height=20,;
        caption="Menu ^", nPag=1;
        , ToolTipText = ""+MSG_ADD_MENU_BEFORE_ITEM+"";
        , HelpContextID = 200410529;
        , Caption=MSG_ADD_MENU_BEFORE;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_21.Click()
        With This.Parent.oContained
            cp_SuppMenu(This.Parent.oContained,"ADD",cp_Translate(MSG_NEW_MENU),1,.T.,"P")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_21.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (.w_COND_AGGANCIO)
            Endwith
        Endif
    Endfunc

    Func oBtn_1_21.mHide()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (Type("i_bFox26")='L' And i_bFox26)
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_22 As StdButton With uid="HVJUPXMDUP",Left=352, Top=338, Width=54,Height=20,;
        caption="Menu v", nPag=1;
        , ToolTipText = ""+MSG_ADD_MENU_AFTER_ITEM+"";
        , HelpContextID = 200312225;
        , Caption=MSG_ADD_MENU_AFTER;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_22.Click()
        With This.Parent.oContained
            cp_SuppMenu(This.Parent.oContained,"ADD",cp_Translate(MSG_NEW_MENU),1,.T.,"D")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_22.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (.w_COND_AGGANCIO)
            Endwith
        Endif
    Endfunc

    Func oBtn_1_22.mHide()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (Type("i_bFox26")='L' And i_bFox26)
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_23 As StdButton With uid="MQONVJWEXR",Left=408, Top=338, Width=53,Height=20,;
        caption="Opt ^", nPag=1;
        , ToolTipText = ""+MSG_ADD_OPT_BEFORE_ITEM+"";
        , HelpContextID = 101208853;
        , Caption=MSG_ADD_OPT_BEFORE;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_23.Click()
        With This.Parent.oContained
            cp_SuppMenu(This.Parent.oContained,"ADD",cp_Translate(MSG_OPTION),2,.T.,"P")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_23.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (.w_COND_AGGANCIO)
            Endwith
        Endif
    Endfunc

    Func oBtn_1_23.mHide()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (Type("i_bFox26")='L' And i_bFox26)
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_24 As StdButton With uid="TUNBGSYVIU",Left=463, Top=338, Width=53,Height=20,;
        caption="Opt v", nPag=1;
        , ToolTipText = ""+MSG_ADD_OPT_AFTER_ITEM+"";
        , HelpContextID = 101214997;
        , Caption=MSG_ADD_OPT_AFTER;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_24.Click()
        With This.Parent.oContained
            cp_SuppMenu(This.Parent.oContained,"ADD",cp_Translate(MSG_OPTION),2,.T.,"D")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_24.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (.w_COND_AGGANCIO)
            Endwith
        Endif
    Endfunc

    Func oBtn_1_24.mHide()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (Type("i_bFox26")='L' And i_bFox26)
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_25 As StdButton With uid="ZDZUFSYWAE",Left=518, Top=338, Width=54,Height=20,;
        caption="Elimina", nPag=1;
        , ToolTipText = ""+MSG_ERASE_ITEM+"";
        , HelpContextID = 59751718;
        , Caption=MSG_ADD_ERASE_MENU;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_25.Click()
        With This.Parent.oContained
            cp_SuppMenu(This.Parent.oContained,"ADD",cp_Translate(MSG_OPTION),2,.T.,"C")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_25.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (.w_COND_AGGANCIO)
            Endwith
        Endif
    Endfunc

    Func oBtn_1_25.mHide()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (Type("i_bFox26")='L' And i_bFox26)
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_26 As StdButton With uid="ACOIJVSEMS",Left=486, Top=6, Width=20,Height=19,;
        caption="...", nPag=1;
        , ToolTipText = ""+MSG_OPEN+"";
        , HelpContextID = 184028465;
        , bGlobalFont=.T.

    Proc oBtn_1_26.Click()
        With This.Parent.oContained
            .loadMn()
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object SYSTreeView As cp_TreeviewDis_Menu With uid="PNTOTRGEIT",Left=299, Top=30, Width=273,Height=304,;
        caption='TreeView',;
        cLeafBmp=g_MLEAFBMP,nIndent=20,cCursor="",cShowFields="VOCEMENU",cNodeShowField="",cLeafShowField="",cNodeBmp=g_MNODEBMP,cLvlSep='.',;
        cEvent = "SysMenu",;
        nPag=1;
        , HelpContextID = 48488278;
        , bGlobalFont=.T.


    Add Object oMENUNAME_1_29 As StdField With uid="ONKECAAQKI",rtseq=14,rtrep=.F.,;
        cFormVar = "w_MENUNAME", cQueryName = "MENUNAME",;
        bObbl = .F. , nPag = 1, Value=Space(25), bMultilanguage =  .F.,;
        HelpContextID = 123120065,;
        bGlobalFont=.T.,;
        Height=21, Width=118, Left=365, Top=5, InputMask=Replicate('X',25), ReadOnly = .T.,TabStop = .F.


    Add Object oObj_1_39 As cp_runprogram With uid="FHKPWORUAA",Left=268, Top=643, Width=333,Height=22,;
        caption='cp_SuppMenu(USRDEL)',;
        prg="cp_SuppMenu('USRDEL')",;
        cEvent = "w_usrtreeview NodeClick",;
        nPag=1;
        , HelpContextID = 35195570;
        , bGlobalFont=.T.



    Add Object oObj_1_40 As cp_runprogram With uid="BHBJAIUXSK",Left=268, Top=595, Width=333,Height=22,;
        caption='cp_SuppMenu(LOST)',;
        prg="cp_SuppMenu('LOST')",;
        cEvent = "w_VOCEMENU LostFocus,w_INSERTPOINT LostFocus,w_POSITION LostFocus,w_NAMEPROC LostFocus,w_MODULO LostFocus, w_BITMAP LostFocus,w_ABILITATO LostFocus,w_DELETED LostFocus,w_NOTE LostFocus,w_MRU LostFocus,w_TEAROFF LostFocus",;
        nPag=1;
        , HelpContextID = 219698139;
        , bGlobalFont=.T.



    Add Object oObj_1_41 As cp_runprogram With uid="HEZRGXKBCY",Left=268, Top=619, Width=333,Height=22,;
        caption='cp_SuppMenu(SYSCLICK)',;
        prg="cp_SuppMenu('SYSCLICK')",;
        cEvent = "w_systreeview NodeClick",;
        nPag=1;
        , HelpContextID = 81297688;
        , bGlobalFont=.T.


    Add Object oMENUUTENTE_1_45 As StdField With uid="HUCJXUTUDP",rtseq=18,rtrep=.F.,;
        cFormVar = "w_MENUUTENTE", cQueryName = "MENUUTENTE",;
        bObbl = .F. , nPag = 1, Value=Space(25), bMultilanguage =  .F.,;
        HelpContextID = 221416767,;
        bGlobalFont=.T.,;
        Height=21, Width=118, Left=96, Top=5, InputMask=Replicate('X',25), ReadOnly = .T.,TabStop = .F.


    Add Object oBtn_1_47 As StdButton With uid="TJEHKLWKFJ",Left=222, Top=6, Width=59,Height=20,;
        caption="Prova", nPag=1;
        , ToolTipText = ""+MSG_SET_TRY_ITEM+"";
        , HelpContextID = 117988208;
        , Caption=MSG_TRY,TabStop = .F.;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_47.Click()
        With This.Parent.oContained
            cp_SuppMenu(This.Parent.oContained,"TRY")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_47.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (.w_USRTreeView.oTree.Nodes.Count > 1)
            Endwith
        Endif
    Endfunc

    Func oBtn_1_47.mHide()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (Type("i_bFox26")='L' And i_bFox26)
            Endwith
        Endif
    Endfunc


    Add Object oObj_1_50 As cp_runprogram With uid="VDEDXIYXYC",Left=268, Top=667, Width=333,Height=22,;
        caption='cp_SuppMenu(TRY)',;
        prg="cp_SuppMenu('TRY')",;
        cEvent = "Ripristina",;
        nPag=1;
        , ToolTipText = "Esegue ripristina in uscita";
        , HelpContextID = 146938843;
        , bGlobalFont=.T.



    Add Object oObj_1_51 As cp_setCtrlObjprop With uid="WTFHXDANKA",Left=587, Top=149, Width=38,Height=38,;
        caption='Object',;
        cProp="Caption",cObj="Menu utente:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_52 As cp_setobjprop With uid="SKPUZOAYUU",Left=666, Top=201, Width=71,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_VOCEMENU",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_53 As cp_setCtrlObjprop With uid="STMQUKDDXO",Left=587, Top=175, Width=78,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Men�:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_54 As cp_setCtrlObjprop With uid="ILZANBVMJA",Left=587, Top=201, Width=78,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Titolo:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_55 As cp_setobjprop With uid="FHEBSEOCGP",Left=666, Top=227, Width=71,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_NAMEPROC",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_56 As cp_setCtrlObjprop With uid="XYLPWDHILE",Left=587, Top=227, Width=78,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Procedura:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_57 As cp_setobjprop With uid="LDYNYTFXXY",Left=666, Top=252, Width=71,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_INSERTPOINT",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_58 As cp_setCtrlObjprop With uid="CNXPIZVNEX",Left=587, Top=253, Width=78,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Path:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_59 As cp_setobjprop With uid="KWYZDTTRZC",Left=666, Top=278, Width=71,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_POSITION",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_60 As cp_setCtrlObjprop With uid="SHTOZWNFYJ",Left=587, Top=279, Width=78,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Posizione:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_61 As cp_setCtrlObjprop With uid="CNLSINMLZA",Left=587, Top=305, Width=78,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Cancellato",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_62 As cp_setobjprop With uid="JEDGXBHRZN",Left=666, Top=329, Width=71,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_BITMAP",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_63 As cp_setCtrlObjprop With uid="DPZKIOSYYB",Left=587, Top=331, Width=78,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Bitmap:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_64 As cp_setobjprop With uid="VLOELJFJJH",Left=666, Top=355, Width=71,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_MODULO",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_65 As cp_setCtrlObjprop With uid="KAYDNLAKGB",Left=587, Top=357, Width=78,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Attivazione:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_66 As cp_setCtrlObjprop With uid="DJWDWAMSIC",Left=587, Top=383, Width=78,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Abilitato",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_67 As cp_setCtrlObjprop With uid="HCUNAFCVWK",Left=587, Top=409, Width=78,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Visibilit� MRU",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_68 As cp_setCtrlObjprop With uid="YETOGWCJLS",Left=587, Top=435, Width=78,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Popup sganciabile",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_69 As cp_setobjprop With uid="HDTXYZDDKQ",Left=666, Top=407, Width=71,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_MRU",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_70 As cp_setobjprop With uid="QVMERADSUL",Left=666, Top=433, Width=71,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_TEAROFF",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.



    Add Object oObj_1_71 As cp_setobjprop With uid="RRYDKQFRMB",Left=666, Top=459, Width=71,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_NOTE",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 101686363;
        , bGlobalFont=.T.


    Add Object oStr_1_32 As StdString With uid="WAJVPFIJKM",Visible=.T., Left=293, Top=6,;
        Alignment=1, Width=70, Height=18,;
        Caption="Men�:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_33 As StdString With uid="PVIHHYBQQP",Visible=.T., Left=1, Top=395,;
        Alignment=1, Width=72, Height=18,;
        Caption="Path:"  ;
        , bGlobalFont=.T.

    Func oStr_1_33.mHide()
        With This.Parent.oContained
            Return (Type("i_bFox26")='L' And i_bFox26)
        Endwith
    Endfunc

    Add Object oStr_1_34 As StdString With uid="XFASJSVNSA",Visible=.T., Left=2, Top=423,;
        Alignment=1, Width=71, Height=18,;
        Caption="Posizione:"  ;
        , bGlobalFont=.T.

    Func oStr_1_34.mHide()
        With This.Parent.oContained
            Return (Type("i_bFox26")='L' And i_bFox26)
        Endwith
    Endfunc

    Add Object oStr_1_35 As StdString With uid="GGHGAZBYUT",Visible=.T., Left=3, Top=367,;
        Alignment=1, Width=70, Height=18,;
        Caption="Titolo:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_36 As StdString With uid="PMOKJZSZVT",Visible=.T., Left=284, Top=367,;
        Alignment=1, Width=75, Height=18,;
        Caption="Procedura:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_37 As StdString With uid="JQIHGCEWXO",Visible=.T., Left=1, Top=448,;
        Alignment=1, Width=72, Height=18,;
        Caption="Attivazione:"  ;
        , bGlobalFont=.T.

    Func oStr_1_37.mHide()
        With This.Parent.oContained
            Return (Type("i_bFox26")='L' And i_bFox26)
        Endwith
    Endfunc

    Add Object oStr_1_38 As StdString With uid="HSCAQNDISJ",Visible=.T., Left=297, Top=423,;
        Alignment=1, Width=54, Height=18,;
        Caption="Bitmap:"  ;
        , bGlobalFont=.T.

    Func oStr_1_38.mHide()
        With This.Parent.oContained
            Return (Type("i_bFox26")='L' And i_bFox26)
        Endwith
    Endfunc

    Add Object oStr_1_46 As StdString With uid="LYGHWPDYAV",Visible=.T., Left=6, Top=6,;
        Alignment=1, Width=87, Height=18,;
        Caption="Menu utente:"  ;
        , bGlobalFont=.T.
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_designmenu','','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Dialog Window"
Endproc

* --- Area Manuale = Functions & Procedures
* --- cp_designmenu
*** Definizione sottoclasse della cp_treeview per la gestione del menu ***

**Generali: la propriet� tag di ogni nodo � utilizzata per memorizzare tutte le informazioni della voce
**di menu, per quel nodo.
**Le informazioni sono concatenate e separate dal simbolo �...
**L'ordine di concatenazione � (per i nomi vedere cursore):
**NAMEPROC+'�'+ALLTRIM(STR(DIRECTORY))+'�'+IIF(ENABLED,'T','F')+'�'+BITMAP+'�'+MODULO+'�'+INS_POINT+'�'+POSITION+'�'+DELETED
Define Class cp_TreeviewDis_Menu As cp_TreeView


    **Inizializzo un contatore a 1 per il calcolo del livello.
    contatore=1
    rifPadre=-1

    *** Aggiunge un nodo figlio rispetto al nodo sul quale si � posizionati.
    *** NON aggiorna il cursore contenente le voci della TreeView.
    *** @Param cText: il nome testuale del nodo.
    *** @Param relazione: indica la posizione d'inserimento del nodo:
    ***  1 se il nodo deve essere posizionato in fondo a tutti i nodi allo stesso livello della chiave i_key
    ***  2 se il nodo deve essere posizionato dopo i_key
    ***  4 se il nodo deve essere un figlio del nodo i_key
    *** @Param tipo: indica se devo creare un submenu o una voce
    *** se tipo=1 creo un submenu; se uguale a 2 creo una opzione
    *** (serve solamente per determinare la bitmap corretta)
    *** @Param i_key: la chiave del nodo al quale si vuole aggiungere un figlio
    *** @return il numero della chiave inserita
    Procedure addElement(cText,relazione,tipo,i_key)
        If Type('i_Key')=='L'
            keyNode=This.oTree.SelectedItem.Key
        Else
            keyNode=i_key
        Endif
        keyNew=This.generateKey(keyNode,relazione)
        This.oTree.Nodes.Add(keyNode,relazione,keyNew,cp_Translate(cText),tipo)
        Return(keyNew)
    Endproc

    **Ritorna il valore del campo del nodo selezionato
    **@param cField: il nome del campo che si vuole avere.
    **@return: il valore di cField del nodo selezionato.
    **Se non � possibile trovare il valore, ritorna una stringa vuota
    Procedure getVar(cField)
        keyNode=This.oTree.SelectedItem.Key
        cCursor=This.cCursor
        Select (cCursor)
        Seek Alltrim(Substr(keyNode,2))
        If Found()
            Return(&cCursor->&cField)
        Else
            Return('')
        Endif
    Endproc

    **Elimina la voce selezionata dalla TreeView
    **Elimina anche tutte le sottovoci legate alla voce selezionata o passata
    Procedure delElement(delIndex)
        If Type('delIndex')=='L'
            This.oTree.Nodes.Remove(This.oTree.SelectedItem.Index)
        Else
            This.oTree.Nodes.Remove(delIndex)
        Endif
    Endproc

    **Espande la voce di menu selezionata o la voce con indice passato
    Procedure expandElement(expIndice)
        If Type('expIndice')=='L'
            This.oTree.SelectedItem.Expanded=.T.
        Else
            This.oTree.Nodes(expIndice).Expanded=.T.
        Endif
    Endproc

    Proc oTree.DblClick()
        Local cKey,cCursor,cOldErr
        Private err
        err=.F.
        If Not Isnull(This.SelectedItem) And This.SelectedItem.Index > 0
            If This.SelectedItem.Children = 0
                cCursor=This.Parent.cCursor
                cOldErr=On('ERROR')
                On Error err=.T.
                cKey=This.SelectedItem.Tag
                On Error &cOldErr
                If err
                    cKey=This.Nodes.Item(1).Tag
                Endif
                Select (This.Parent.cCursor)
                cOldErr=On('ERROR')
                * --- Nel Tag non ho il Recno()
                * --- ma informazioni strtturate
                * --- relative al nodo
                On Error err=.T.
                Go (cKey)
                On Error &cOldErr
                cOldErr=On('ERROR')
                On Error err=.T.
                i_Value=&cCursor->cpexec
                On Error &cOldErr
                If Not err
                    *---Esiste il campo "cpExec": eseguo la procedura contenuta nel campo
                    Do &i_Value
                    If err
                        cp_ErrorMsg(cp_MsgFormat(MSG_ERROR_WHILE_CALLING_PROCEDURE__,Upper(i_Value)),"!","MSG_ERROR",.F.)
                    Endif
                Endif
            Endif
            If This.Parent.nDblClick=2
                This.Parent.Parent.oContained.notifyEvent("w_"+Lower(This.Parent.Name)+" NodeClick")
                This.Parent.nDblClick=0
            Endif
            This.Parent.bRightMouse=.F.
        Endif
        *endIf
        Return

    Proc FillTree
        Local oNodX,cFathKey,aLev,cLevel,bIsContained,i,nIdx,cCursor,cTmp
        Local nBmpIndex,nPrevIdx,cNodeFields,cLeafFields,cPrevLeafText,bBmp,cOldErr
        Private err
        cFathKey=''
        bIsContained=.F.
        Dimension aLev[1]
        Dimension cNodeFields[1]
        Dimension cLeafFields[1]
        aLev[1]=''
        cNodeFields[1]=''
        cLeafFields[1]=''
        bBmp=.T.
        *---Compongo la lista dei campi da visualizzare nei nodi
        cTmp=This.cNodeShowField+Iif(Empty(This.cNodeShowField) Or Empty(This.cShowFields),'','+')+This.cShowFields+','
        i=0
        Do While At(",",cTmp)<>0
            i=i+1
            Dimension cNodeFields[i]
            cNodeFields[i]=Left(cTmp,At(",",cTmp)-1)
            cTmp=Substr(cTmp,At(",",cTmp)+1)
        Enddo
        *---Compongo la lista dei campi da visualizzare nelle foglie
        cTmp=This.cLeafShowField+Iif(Empty(This.cLeafShowField) Or Empty(This.cShowFields),'','+')+This.cShowFields+','
        i=0
        Do While At(",",cTmp)<>0
            i=i+1
            Dimension cLeafFields[i]
            cLeafFields[i]=Left(cTmp,At(",",cTmp)-1)
            cTmp=Substr(cTmp,At(",",cTmp)+1)
        Enddo
        cCursor=This.cCursor
        *--- Cancella tutti i nodi esistenti
        This.oTree.Nodes.Clear
        Select (cCursor)
        Set Order To LvlKey
        nPrevIdx=1
        cPrevLeafText=''
        *--- Verifica se esiste il campo "CPBmpName" dei bitmap parametrici
        err=.F.
        cOldErr=On('ERROR')
        On Error err=.T.
        cLevel=&cCursor..CPBmpName
        On Error &cOldErr
        If err
            bBmp=.F.
        Endif
        Scan
            cLevel="k"+Trim(&cCursor->LvlKey)
            nIdx=0
            For i=Alen(aLev) To 1 Step -1
                If This.cLvlSep+aLev[i]+This.cLvlSep$This.cLvlSep+cLevel
                    nIdx=i
                    Exit For
                Endif
            Next
            Dimension aLev[nIdx+1]
            aLev[nIdx+1]=cLevel
            *---Conta il nodo per decidere quale campo visualizzare
            i=Len(cLevel)-Len(Strtran(cLevel,This.cLvlSep,''))+1
            If i>Alen(cNodeFields)
                i=1
            Endif
            cTmp=''
            If !bBmp Or Empty(&cCursor..CPBmpName)
                If nIdx<=nPrevIdx And This.oTree.Nodes.Count>0  &&devo aggiornare il precedente perch� non � un nodo con subitems
                    If This.bHasElementBmp And This.ImgList.ListImages.Count>0
                        This.oTree.Nodes(This.oTree.Nodes.Count).Image=This.ImgList.ListImages.Count
                    Endif
                    This.oTree.Nodes(This.oTree.Nodes.Count).Text=cPrevLeafText
                Endif
                If This.ImgList.ListImages.Count>0
                    nBmpIndex=nIdx%This.ListImageCount()+1
                    cTmp=","+Alltrim(Str(nBmpIndex))
                Else
                    nBmpIndex=1
                Endif
            Else
                * --- Bitmap paramentrici
                cTmp=","+Alltrim(Str(This.GetBitmapIndex(&cCursor..CPBmpName)))
            Endif
            If nIdx=0
                oNodX=This.oTree.Nodes.Add(,,aLev[nIdx+1],Iif(Isnull(&cNodeFields[i]),'',cp_Translate(cp_ToStr(&cNodeFields[i],1)))&cTmp)
            Else
                oNodX=This.oTree.Nodes.Add(aLev[nIdx],4,aLev[nIdx+1],Iif(Isnull(&cNodeFields[i]),'',cp_Translate(cp_ToStr(&cNodeFields[i],1)))&cTmp)
            Endif
            * --- Nel Tag vado a mettere
            * --- le informazione del nodo
            oNodX.Tag=Alltrim(NAMEPROC)+'�'+Alltrim(Str(Directory))+'�'+Iif(Enabled,'T','F')+'�'+Alltrim(CPBmpName)+'�'+Alltrim(MODULO)+'�'+Alltrim(INS_POINT)+'�'+Alltrim(POSITION)+'�'+Alltrim(INSERTION)+'�'+Alltrim(Deleted) ;
                +'�'+Alltrim(Note)+'�'+Alltrim(MRU)+'�'+Alltrim(TEAROFF)
            nPrevIdx=nIdx
            cPrevLeafText=Iif(Isnull(&cLeafFields[i]),'',cp_Translate(cp_ToStr(&cLeafFields[i],1)))
            *onodx.ensurevisible
        Endscan
        If This.oTree.Nodes.Count>0  &&devo aggiornare il precedente perch� non � un nodo con subitems
            If This.bHasElementBmp
                This.oTree.Nodes(This.oTree.Nodes.Count).Image=This.ImgList.ListImages.Count
            Endif
            This.oTree.Nodes(This.oTree.Nodes.Count).Text=cPrevLeafText
        Endif
        Return

    Proc visita(nodo,cursore,padre)
        Local w_VOCEMENU,w_NAMEPROC,w_DIRECTORY,w_ENABLED,w_BITMAP,w_MODULO,w_INSERTPOINT,w_POSITION,w_INSERTION,w_DELETED, w_NOTE, w_MRU, w_TEAROFF
        Local k,done
        k=1
        *children=nodo.children
        This.getValues(nodo,@w_VOCEMENU,@w_NAMEPROC,@w_DIRECTORY,@w_ENABLED,@w_BITMAP,@w_MODULO,@w_INSERTPOINT,@w_POSITION,@w_INSERTION,@w_DELETED,@w_NOTE,@w_MRU,@w_TEAROFF)
        **inserisco in un cursore le voci del nodo analizzato
        Insert Into (cursore) (VOCEMENU,Level,NAMEPROC,Directory,rifPadre,ELEMENTI,Id,Enabled,CPBmpName,MODULO,INS_POINT,POSITION,INSERTION,Deleted,Note,MRU,TEAROFF);
            VALUES (cp_Translate(w_VOCEMENU),This.contatore,w_NAMEPROC,w_DIRECTORY,padre,nodo.Children,Sys(2015),w_ENABLED,w_BITMAP,w_MODULO,w_INSERTPOINT,w_POSITION,w_INSERTION,w_DELETED,w_NOTE,w_MRU,w_TEAROFF)
        **se il nodo ha figli...
        This.rifPadre=This.rifPadre+1
        If nodo.Children<>0
            padre=This.rifPadre
            done=.F.
            nodo=nodo.Child
            For k=1 To nodo.Parent.Children
                **calcolo il primo figlio
                If Not done
                    This.contatore=This.contatore+1
                    *this.rifPadre=this.rifPadre+1
                    This.visita(nodo,cursore,padre)
                    done=.T.
                Else
                    *this.rifPadre=this.rifPadre+1
                    nodo=nodo.Next
                    This.visita(nodo,cursore,padre)
                Endif
            Endfor
            This.contatore=This.contatore-1
        Endif
        *WAIT WINDOW 'prima ricorsione '+ ALLTRIM(STR(nodo.parent.children))+ ALLTRIM(STR(k))
        Return

        * --- Calcola il tag date le informazioni e lo associa al nodo..
    Procedure SetTag(oNodo,cNameProc,cDirectory,bAbilitato,cBitmap,cModulo,cInsertPoint,cPosition,cInsertion,cDeleted,cNote,cMRU,cTearOff)
        oNodo.Tag = Alltrim(cNameProc)+"�"+Alltrim(Str(cDirectory))+"�"+Iif(bAbilitato,"T","F")+"�"+Alltrim(cBitmap)+"�"+Alltrim(cModulo)+;
            "�"+Alltrim(cInsertPoint)+"�"+Alltrim(cPosition)+"�"+Alltrim(cInsertion)+"�"+Alltrim(cDeleted)+"�"+Alltrim(cNote)+"�"+Alltrim(cMRU)+"�"+Alltrim(cTearOff)
    Endproc

    * --- Dato un oNodo interpreta il tag valorizzando le singole propriet�
    Function getValues(oNodo,cVoceMenu,cNameProc,cDirectory,bAbilitato,cBitmap,cModulo,cInsertPoint,cPosition,pInsertion,cDeleted,cNote,cMRU,cTearOff)
        Local cTag
        cTag = Alltrim(oNodo.Tag)
        cVoceMenu = cp_Translate(oNodo.Text)
        cNameProc = Alltrim(Left(cTag,At("�",cTag)-1))
        cTag = Strtran(cTag,cNameProc+"�","",1,1)
        cDirectory = Int(Val(Alltrim(Left(cTag,At("�",cTag)-1))))
        cTag = Strtran(cTag,Alltrim(Str(cDirectory))+"�","",1,1)
        bAbilitato = Alltrim(Left(cTag,At("�",cTag)-1))="T"
        cTag = Strtran(cTag,Iif(bAbilitato,"T","F")+"�","",1,1)
        cBitmap = Alltrim(Left(cTag,At("�",cTag)-1))
        cTag = Strtran(cTag,cBitmap+"�","",1,1)
        cModulo = Alltrim(Left(cTag,At("�",cTag)-1))
        cTag = Strtran(cTag,cModulo+"�","",1,1)
        cInsertPoint = Alltrim(Left(cTag,At("�",cTag)-1))
        cTag = Strtran(cTag,cInsertPoint+"�","",1,1)
        cPosition = Alltrim(Left(cTag,At("�",cTag)-1))
        cTag = Strtran(cTag,cPosition+"�","",1,1)
        pInsertion=Alltrim(Left(cTag,At("�",cTag)-1))
        cTag = Strtran(cTag,pInsertion+"�","",1,1)
        cDeleted = Alltrim(Left(cTag,At("�",cTag)-1))
        cTag = Strtran(cTag,cDeleted+"�","",1,1)
        cNote = Alltrim(Left(cTag,At("�",cTag)-1))
        cTag = Strtran(cTag,cNote +"�","",1,1)
        cMRU = Alltrim(Left(cTag,At("�",cTag)-1))
        cTag = Strtran(cTag,cMRU+"�","",1,1)
        cTearOff = cTag
    Endfunc

    * --- Muove oNodo in direzione ascedente o discendente all'interno della
    * --- tree view
    Procedure Move_Nodo(oNodo , nDirection)
        Local oZio, oNode
        * --- Se sono sulla root non posso svolgere nessun spostamento
        If oNodo.Index<>oNodo.Root.Index
            If nDirection=1
                oNode=oNodo.Next
            Else
                oNode=oNodo.Previous
            Endif
            * --- Esiste un fratello successivo al nodo ?
            If Not Isnull(oNode)
                * --- E' espanso oppure non ha figli
                If oNode.Expanded And oNode.Children>0
                    * --- Se espanso lo inserisco come primo figlio del nodo successivo
                    oZio=oNode.Child
                    This.SpostaNodo( oNodo , oZio ,Iif(nDirection=1,3,1)  )
                Else
                    * --- Se non espanso lo sposto dopo il nodo ad esso successivo
                    This.SpostaNodo( oNodo , oNode , Iif(nDirection=1,2,3)   )
                Endif
            Else
                * --- Non esiste un fratello successivo al nodo
                * --- Sposto il nodo dopo suo padre se non � figlio
                * --- della root
                oZio=oNodo.Parent
                * --- l'antenato non � la Root ?
                If oZio.Root.Index<>oZio.Index
                    This.SpostaNodo( oNodo , oZio , Iif(nDirection=1,2,3)   )
                Endif
            Endif
        Endif
    Endproc

    * --- Dato un nodo (riferimento oggetto) lo sposta in base
    * --- alla relazione (1,2,3,4) come
    * --- fratello precedente (2) o successivo (3) o ultimo fratello (1) o primo figlio (4) di oRNodo (Riferimento oggetto)
    Procedure SpostaNodo( oPNodo , oRNodo, nRelazione )
        Local cCurName,cOldLevel,nIndexTemp,oPadreNodo,n_K, cText,cTsg,nBmpIndex,nIndex,bExp
        cCurName=Sys(2015)
        nIndexTemp=oPNodo.Index

        * --- Se non ho figli devo spostare il singolo nodo
        * --- altrimenti devo, tramite una visita, recuperare tutti i nodi suoi eredi
        * --- e spostarli uno per uno
        If oPNodo.Children=0
            cText = oPNodo.Text
            cTag = oPNodo.Tag
            bExp=oPNodo.Expanded
            * --- Recupero il nome del bitmap associato al nodo direttamente
            * --- dalle variabili della maschera (sul nodo selectedimage non � accessibile)
            This.oTree.Nodes.Add( oRNodo.Index , nRelazione ,, cp_Translate(cText) , This.GetBitmapIndex(Iif(Empty(This.Parent.oContained.w_BITMAP),Iif( This.Parent.oContained.w_DIRECTORY=1 Or This.Parent.oContained.w_DIRECTORY=4 ,g_MLEAFBMP,g_MNODEBMP),This.Parent.oContained.w_BITMAP) ))
            This.oTree.Nodes(This.oTree.Nodes.Count()).Tag = cTag
            * --- L'indice va decrementato xcg� al termine elimino un nodo sempre
            nIndex = This.oTree.Nodes.Count()-1
        Else
            #If Version(5)>=700
                Select * From ( This.cCursor ) Where 1=0 Into Cursor &cCurName NoFilter Readwrite
            #Else
                Local w_nome
                w_nome = Sys(2015)
                Select * From ( This.cCursor ) Into Cursor (w_nome) Where 1=0 NoFilter
                Use Dbf() Again In 0 Alias ( cCurName )
                Use
                Select ( cCurName )
            #Endif
            * --- Visito l'albero a partire dal nodo che devo spostare e salvo gli elementi da spostare in un &cCurName..
            *     E' importante, prima della visita, settare a 1 la propriet� contatore della TreeView per il calcolo corretto
            *     del LEVEL del menu.
            This.rifPadre=-1
            This.contatore = 1
            This.visita(oPNodo,cCurName,0)
            Select (cCurName)
            Go Top
            Scan
                * ---- Sono nel primo elemento, questo sar� effettivamente legato, le altre righe
                * ---- saranno semplicemente figli di questo nodo...
                If Recno(cCurName)=1
                    bExp=oPNodo.Expanded
                    This.oTree.Nodes.Add( oRNodo.Index , nRelazione ,, cp_Translate(Alltrim(&cCurName..VOCEMENU)) , This.GetBitmapIndex(Iif(Empty(&cCurName..CPBmpName),Iif( &cCurName..Directory=1 Or &cCurName..Directory=4 ,g_MLEAFBMP,g_MNODEBMP),&cCurName..CPBmpName) ))
                    * --- L'indice va decrementato xch� al termine elimino tanti nodi quanti nel cursore..
                    * --- svolgendo la Remove..
                    nIndex = This.oTree.Nodes.Count()-Reccount(cCurName)
                Else
                    If &cCurName..Level=cOldLevel
                        * --- Se il livello del nodo che devo inserire � uguale al nodo precedente, inserisco il nodo subito dopo
                        This.oTree.Nodes.Add(nIndexTemp,2,,cp_Translate(Alltrim(&cCurName..VOCEMENU)),This.GetBitmapIndex(Iif(Empty(&cCurName..CPBmpName),Iif( &cCurName..Directory=1 Or &cCurName..Directory=4 ,g_MLEAFBMP,g_MNODEBMP),&cCurName..CPBmpName) ))
                    Else
                        If &cCurName..Level>cOldLevel
                            * --- Se il livello del nodo che devo inserire � maggiore del nodo precedente, inserisco il nodo all'interno dello stesso
                            This.oTree.Nodes.Add(nIndexTemp,4,,Alltrim(&cCurName..VOCEMENU),This.GetBitmapIndex(Iif(Empty(&cCurName..CPBmpName),Iif( &cCurName..Directory=1 Or &cCurName..Directory=4 ,g_MLEAFBMP,g_MNODEBMP),&cCurName..CPBmpName) ))
                        Else
                            * --- Calcolo l'index del nodo all'interno del quale devo inserire la voce
                            oPadreNodo = This.oTree.Nodes(nIndexTemp).Parent
                            n_K = cOldLevel-&cCurName..Level
                            Do While n_K>0
                                oPadreNodo = oPadreNodo.Parent
                                n_K = n_K - 1
                            Enddo
                            nIndexTemp = oPadreNodo.Index
                            This.oTree.Nodes.Add(nIndexTemp,4,,cp_Translate(Alltrim(&cCurName..VOCEMENU)),This.GetBitmapIndex(Iif(Empty(&cCurName..CPBmpName),Iif( &cCurName..Directory=1 Or &cCurName..Directory=4 ,g_MLEAFBMP,g_MNODEBMP),&cCurName..CPBmpName) ))
                        Endif
                    Endif
                Endif
                * --- tengo aggiornato oldLevel e la chiave
                cOldLevel = &cCurName..Level
                * --- Portiamo l'indice sul nodo appena creato
                nIndexTemp = This.oTree.Nodes.Count()
                * --- Salviamo il Tag della TreeView
                This.SetTag(This.oTree.Nodes( nIndexTemp ) , &cCurName..NAMEPROC , &cCurName..Directory , &cCurName..Enabled , &cCurName..CPBmpName , &cCurName..MODULO , &cCurName..INS_POINT , &cCurName..POSITION , "" , &cCurName..Deleted, &cCurName..Note, &cCurName..MRU, &cCurName..TEAROFF)
            Endscan
            * --- Elimino il cursore d'appoggio
            Use In Select( cCurName )
        Endif
        * --- Elimino il nodo dalla TreeView
        This.delElement(oPNodo.Index)
        * ---- valorizzo w_INDEX
        This.Parent.oContained.w_INDEX= nIndex
        * --- Il nodo spostato lo espando se lo era
        This.oTree.Nodes(nIndex).Expanded=bExp
    Endproc

    Proc oTree.DragDrop(oSource,nX,nY)
        Local cOldFName,bOk,cFName,cDefPath
        cDefPath=Sys(5)+Sys(2003)+'\CUSTOM\'
        cFName=''
        If Upper(oSource.Name)='STDSGRP'
            * drag iniziato nella lista dei gruppi del tool di gestione utenti
            cFName='CUSTOM_G'+Alltrim(Str(_cpgroups_.Code))+'.VMN'
        Else
            If Upper(oSource.Name)='STDSUSR'
                * drag iniziato nella lista degli utenti del tool di gestione utenti
                cFName='CUSTOM_'+Alltrim(Str(_cpusers_.Code))+'.VMN'
            Endif
        Endif
        If At(cFName,Thisform.oParentObject.cFileName)<>0
            cFName=Thisform.oParentObject.cFileName
        Endif
        If Not Empty(cFName)
            cOldFName=Thisform.oParentObject.cFileName
            Thisform.oParentObject.cFileName=cFName
            bOk=.T.
            If At("\",cFName)<>0
                cDefPath=''
            Else
                Thisform.oParentObject.cFileName=cDefPath+cFName
            Endif
            *--- Zucchetti Aulla Inizio - Monitor Framework
            *--- Se il monitor � attivo verifico i diritti
            Local l_UsrRight
            l_UsrRight = cp_set_get_right()
            Do Case
                Case l_UsrRight = 2   &&Read Only
                    Thisform.oParentObject.cFileName=cOldFName
                    Return
                Case l_UsrRight = 3
                    If Upper(oSource.Name)='STDSUSR'
                        If i_CodUte<>_cpusers_.Code
                            Thisform.oParentObject.cFileName=cOldFName
                            Return
                        Endif
                    Else
                        If Ascan(i_aUsrGrps, _cpgroups_.Code) = 0
                            Thisform.oParentObject.cFileName=cOldFName
                            Return
                        Endif
                    Endif
                    cFName = Forcepath(Upper(cFName), Fullpath(i_usrpath))
                Case l_UsrRight = 4
                    cFName = Forcepath(Upper(cFName), Fullpath(i_usrpath))
                Otherwise
                    cFName = Iif(Not Empty(cDefPath), Forcepath(Upper(cFName), cDefPath), Upper(cFName))
            Endcase
            Thisform.oParentObject.cFileName=cFName
            If cp_fileexist(cFName)
                bOk=Messagebox(cp_MsgFormat(MSG_FILE____ALREADY_EXIST_C_SUBSTITUTE_QP, cFName, ''),4+48,MSG_SAVE)=6
            Else
                bOk=Messagebox(cp_MsgFormat(MSG_SAVE_FILE__QP, cFName),4+32,MSG_SAVE)=6
            Endif
            *--- Zucchetti Aulla Fine - Monitor Framework
            If bOk
                Thisform.oParentObject.SaveDoc()
            Else
                Thisform.oParentObject.bModified=.F.
            Endif
            Thisform.oParentObject.cFileName=cOldFName
        Endif
        Return



Enddefine
* --- Fine Area Manuale
