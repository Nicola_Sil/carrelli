* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_searchdetailr                                                *
*              Routine ctrl mask detail search                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-12-22                                                      *
* Last revis.: 2011-03-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject,pCommandExec
* --- Area Manuale = Header
* --- Fine Area Manuale
Private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
Createobject("tcp_searchdetailr",oParentObject,m.pCommandExec)
Return(i_retval)

Define Class tcp_searchdetailr As StdBatch
    * --- Local variables
    pCommandExec = Space(10)
    w_VALUEFILTERTYPE = Space(1)
    w_POSITION = 0
    w_OLDPOSITION = 0
    w_FilterCond = Space(254)
    w_ZOOM = .Null.
    w_OBJFORM = .Null.
    w_FIRSTREC = .F.
    w_NUMREC = 0
    * --- WorkFile variables
    runtime_filters = 2

    Procedure Pag1
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        * --- Routine per il controllo dela maschera di ricerca e filtro
        Local i_OldCursor
        i_OldCursor=Select ()
        This.w_OBJFORM = This.oParentObject.oParentObject.oObjColumn.Parent.Parent.Parent.Parent.Parent.Parent.Parent
        Do Case
            Case This.pCommandExec="Type"
                With This.oParentObject.oParentObject.oObjColumn
                    Do Case
                        Case Vartype(.Value)="D" Or Vartype(.Value)="T"
                            This.w_VALUEFILTERTYPE = "D"
                            This.oParentObject.w_ZoomValue.Visible = .T.
                            This.oParentObject.w_ZoomValueN.Visible = .F.
                        Case Vartype(.Value)="N"
                            This.w_VALUEFILTERTYPE = "N"
                            This.oParentObject.w_ZoomValue.Visible = .F.
                            This.oParentObject.w_ZoomValueN.Visible = .T.
                        Otherwise
                            This.w_VALUEFILTERTYPE = "C"
                            This.oParentObject.w_ZoomValue.Visible = .T.
                            This.oParentObject.w_ZoomValueN.Visible = .F.
                    Endcase
                Endwith
                Select (i_OldCursor)
                i_retcode = 'stop'
                i_retval = This.w_VALUEFILTERTYPE
                Return
            Case This.pCommandExec="Search"
                This.oParentObject.RunDeactivateRelease = .F.
                This.w_OBJFORM.MarkPos()
                If This.w_OBJFORM.NumRow()>0
                    This.Pag2()
                    If i_retcode='stop' Or !Empty(i_Error)
                        Return
                    Endif
                    If Found()
                        This.w_POSITION = Recno()
                    Else
                        If cp_YesNo("Valore non trovato. Ricomincio ricerca dall'inizio?")
                            This.oParentObject.w_SecondSearch = .F.
                            This.w_POSITION = 1
                            Select (This.w_OBJFORM.cTrsName)
                            Go (This.w_POSITION)
                            This.Pag2()
                            If i_retcode='stop' Or !Empty(i_Error)
                                Return
                            Endif
                            If Found()
                                This.w_POSITION = Recno()
                            Else
                                This.w_POSITION = This.w_OLDPOSITION
                            Endif
                        Else
                            This.w_POSITION = This.w_OLDPOSITION
                        Endif
                    Endif
                    Select (This.w_OBJFORM.cTrsName)
                    Go (This.w_POSITION)
                    This.w_OBJFORM.SetRow(This.w_POSITION)
                    This.w_OBJFORM.Refresh()
                    This.oParentObject.RunDeactivateRelease = .T.
                Else
                    Ah_ErrorMsg("Valore non trovato")
                Endif
                If i_curform.WindowType=1
                    This.oParentObject.EcpQuit()
                Else
                    This.oParentObject.SetFocusOnFirstControl()
                    If This.oParentObject.oPgFrm.Pages[1].oPag.oStr_StringAdvanced.Caption = cp_Translate("Nascondi filtri avanzati")
                        This.oParentObject.oPgFrm.Pages[1].oPag.oStr_StringAdvanced.Click()
                    Endif
                    This.oParentObject.oTimerInform.Enabled=.T.
                Endif
            Case This.pCommandExec="Filter"
                This.w_OBJFORM.MarkPos()
                If This.oParentObject.w_RemPrevFilter="S"
                    This.oParentObject.oParentObject.Parent.BlankFilter()
                    Select (This.w_OBJFORM.cTrsName)
                    Set Filter To
                Endif
                Local w_ColumnSearch, w_Operand, w_SearchValueFilter
                w_ColumnSearch = This.oParentObject.oParentObject.oObjColumn.ControlSource
                w_Operand=Iif(This.oParentObject.w_TypeFilter="=", "==", This.oParentObject.w_TypeFilter)
                Select (This.w_OBJFORM.cTrsName)
                Do Case
                    Case This.oParentObject.w_CTRLTYPE="D"
                        w_SearchValueFilter = This.oParentObject.w_SearchValueD
                        Set Filter To Nvl( &w_ColumnSearch , cp_CharToDate("  -  -    ")) &w_Operand Nvl( w_SearchValueFilter , cp_CharToDate("  -  -    "))
                    Case This.oParentObject.w_CTRLTYPE="N"
                        w_SearchValueFilter = cp_TOSTRODBC(This.oParentObject.w_SearchValueN)
                        Set Filter To &w_ColumnSearch &w_Operand &w_SearchValueFilter
                    Otherwise
                        w_SearchValueFilter = cp_TOSTRODBC(Upper(Alltrim(This.oParentObject.w_SearchValue)))
                        Do Case
                            Case This.oParentObject.w_TypeFilter="Like %"
                                Set Filter To w_SearchValueFilter $ Upper(Alltrim( &w_ColumnSearch ))
                            Case This.oParentObject.w_TypeFilter="Like"
                                Set Filter To &w_SearchValueFilter == Upper(Alltrim( Left( &w_ColumnSearch , Len(&w_SearchValueFilter))))
                            Case This.oParentObject.w_TypeFilter="Not Like %"
                                Set Filter To Not( &w_SearchValueFilter $ Upper(Alltrim( &w_ColumnSearch )))
                            Case This.oParentObject.w_TypeFilter="Not Like"
                                Set Filter To &w_SearchValueFilter <> Upper(Alltrim( Left( &w_ColumnSearch , Len(&w_SearchValueFilter))))
                            Otherwise
                                Set Filter To Alltrim( &w_ColumnSearch ) &w_Operand Upper( &w_SearchValueFilter )
                        Endcase
                Endcase
                This.w_POSITION = Recno()
                Count To This.w_NUMREC
                If This.w_NUMREC=0
                    Set Filter To
                    This.w_OBJFORM.RePos()
                    Ah_ErrorMsg("Non sono presenti righe di dettaglio per il filtro selezionato")
                Else
                    This.w_OBJFORM.SetRow(This.w_POSITION)
                    This.oParentObject.oParentObject.oWhereFilter.Visible=.T.
                Endif
                This.w_OBJFORM.Refresh()
                Select (i_OldCursor)
                This.oParentObject.EcpQuit()
            Case This.pCommandExec="LoadZoom"
                Local w_ColumnSearch
                w_ColumnSearch = This.oParentObject.oParentObject.oObjColumn.ControlSource
                This.w_OBJFORM.MarkPos()
                Do Case
                    Case This.oParentObject.w_CTRLTYPE="D"
                        This.w_ZOOM = This.oParentObject.w_ZoomValue
                        Select Distinct &w_ColumnSearch As VALUEZOOM From (This.w_OBJFORM.cTrsName) Into Cursor elemzoom Where &w_ColumnSearch Is Not Null Order By 1 Desc
                    Case This.oParentObject.w_CTRLTYPE="N"
                        This.w_ZOOM = This.oParentObject.w_ZoomValueN
                        Select Distinct &w_ColumnSearch As VALUEZOOM From (This.w_OBJFORM.cTrsName) Into Cursor elemzoom Order By 1
                    Otherwise
                        This.w_ZOOM = This.oParentObject.w_ZoomValue
                        Select Distinct Alltrim( &w_ColumnSearch ) As VALUEZOOM From (This.w_OBJFORM.cTrsName) Into Cursor elemzoom Where &w_ColumnSearch Is Not Null And Not Empty( &w_ColumnSearch ) Order By 1
                Endcase
                If Used("elemzoom")
                    * --- Cancello il contenuto dello Zoom
                    Select (This.w_ZOOM.cCursor)
                    Zap
                    * --- Scan del cursore di appoggio e copia su cursore zoom
                    Select elemzoom
                    Go Top
                    Scan
                        Scatter Memvar
                        Select (This.w_ZOOM.cCursor)
                        Append Blank
                        Gather Memvar
                    Endscan
                    Use In elemzoom
                    Select (This.w_ZOOM.cCursor)
                    Go Top
                    This.w_ZOOM.grd.Refresh
                    This.w_ZOOM.grd.Refresh()
                Endif
                This.w_OBJFORM.RePos()
            Case This.pCommandExec="Select"
                This.w_OBJFORM.MarkPos()
                If This.oParentObject.w_RemPrevFilter="S"
                    This.oParentObject.oParentObject.Parent.BlankFilter()
                    Select (This.w_OBJFORM.cTrsName)
                    Set Filter To
                Endif
                Local w_ColumnSearch, w_Operand, w_FilterSearch
                w_ColumnSearch = This.oParentObject.oParentObject.oObjColumn.ControlSource
                w_FilterSearch=""
                If This.oParentObject.w_CTRLTYPE="N"
                    This.w_ZOOM = This.oParentObject.w_ZoomValueN
                Else
                    This.w_ZOOM = This.oParentObject.w_ZoomValue
                Endif
                This.w_FIRSTREC = .T.
                Select (This.w_ZOOM.cCursor)
                Scan For xchk=1
                    If This.w_FIRSTREC
                        This.w_FIRSTREC = .F.
                        w_FilterSearch = "("
                    Else
                        w_FilterSearch = w_FilterSearch + " OR "
                    Endif
                    Select (This.w_ZOOM.cCursor)
                    Do Case
                        Case This.oParentObject.w_CTRLTYPE="D"
                            w_FilterSearch = w_FilterSearch + Alltrim(This.oParentObject.oParentObject.oObjColumn.ControlSource) + " = CTOD(" + cp_TOSTRODBC(CP_TODATE(Left(VALUEZOOM,10))) + ")"
                        Case This.oParentObject.w_CTRLTYPE="N"
                            w_FilterSearch = w_FilterSearch + Alltrim(This.oParentObject.oParentObject.oObjColumn.ControlSource) + " = " + cp_TOSTRODBC(VALUEZOOM)
                        Otherwise
                            w_FilterSearch = w_FilterSearch + "ALLTRIM(" + Alltrim(This.oParentObject.oParentObject.oObjColumn.ControlSource) + ") == " + cp_TOSTRODBC(Alltrim(VALUEZOOM))
                    Endcase
                Endscan
                Select (This.w_OBJFORM.cTrsName)
                If Not Empty(w_FilterSearch)
                    w_FilterSearch = w_FilterSearch + ")"
                    Set Filter To &w_FilterSearch
                Endif
                This.w_POSITION = Recno()
                Count To This.w_NUMREC
                If This.w_NUMREC=0
                    Set Filter To
                    This.w_OBJFORM.RePos()
                    Ah_ErrorMsg("Non sono presenti righe di dettaglio per il filtro selezionato")
                Else
                    This.w_OBJFORM.SetRow(This.w_POSITION)
                    This.oParentObject.oParentObject.oWhereFilter.Visible=.T.
                Endif
                This.w_OBJFORM.Refresh()
                Select (i_OldCursor)
                This.oParentObject.EcpQuit()
            Case This.pCommandExec="Checked"
                If This.oParentObject.w_CTRLTYPE="N"
                    This.w_ZOOM = This.oParentObject.w_ZoomValueN
                Else
                    This.w_ZOOM = This.oParentObject.w_ZoomValue
                Endif
                Select (This.w_ZOOM.cCursor)
                This.w_OLDPOSITION = Recno()
                Count For xchk=1 To This.w_POSITION
                This.oParentObject.w_CHECKED = This.w_POSITION>0
                If Not This.oParentObject.w_CHECKED And This.oParentObject.w_CTRLTYPE="C"
                    This.oParentObject.w_TypeFilter = Iif(g_RicPerCont="T", "Like %", "Like")
                Endif
                Select (This.w_ZOOM.cCursor)
                Go This.w_OLDPOSITION
        Endcase
        This.w_OBJFORM = .Null.
        Select (i_OldCursor)
    Endproc


    Procedure Pag2
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        Select (This.w_OBJFORM.cTrsName)
        This.w_OLDPOSITION = Recno()
        Do Case
            Case (This.w_OLDPOSITION>1 Or This.oParentObject.w_SecondSearch) And This.w_OLDPOSITION<This.w_OBJFORM.NumRow()
                Go This.w_OLDPOSITION+1
            Case This.oParentObject.w_SecondSearch And This.w_OLDPOSITION=This.w_OBJFORM.NumRow()
                Go 1
        Endcase
        This.oParentObject.w_SecondSearch = .T.
        Local w_ColumnSearch, w_Operand
        w_ColumnSearch = This.oParentObject.oParentObject.oObjColumn.ControlSource
        w_Operand=Iif(This.oParentObject.w_TypeFilter="=", "==", This.oParentObject.w_TypeFilter)
        Do Case
            Case This.oParentObject.w_CTRLTYPE="D"
                Locate For Nvl( &w_ColumnSearch , cp_CharToDate("  -  -    ")) &w_Operand Nvl( This.oParentObject.w_SearchValueD , cp_CharToDate("  -  -    ")) Rest
            Case This.oParentObject.w_CTRLTYPE="N"
                Locate For &w_ColumnSearch &w_Operand This.oParentObject.w_SearchValueN Rest
            Otherwise
                Do Case
                    Case This.oParentObject.w_TypeFilter="Like %"
                        Locate For Upper(Alltrim(This.oParentObject.w_SearchValue)) $ Upper(Alltrim( &w_ColumnSearch )) Rest
                    Case This.oParentObject.w_TypeFilter="Like"
                        Locate For Upper(Alltrim(This.oParentObject.w_SearchValue)) == Upper(Alltrim( Left( &w_ColumnSearch , Len(Alltrim(This.oParentObject.w_SearchValue))))) Rest
                    Case This.oParentObject.w_TypeFilter="Not Like %"
                        Locate For Not(Upper(Alltrim(This.oParentObject.w_SearchValue)) $ Upper(Alltrim( &w_ColumnSearch ))) Rest
                    Case This.oParentObject.w_TypeFilter="Not Like"
                        Locate For Upper(Alltrim(This.oParentObject.w_SearchValue)) <> Upper(Alltrim( Left( &w_ColumnSearch , Len(Alltrim(This.oParentObject.w_SearchValue))))) Rest
                    Otherwise
                        Locate For Upper(Alltrim( &w_ColumnSearch )) &w_Operand Upper(Alltrim(This.oParentObject.w_SearchValue)) Rest
                Endcase
        Endcase
    Endproc


    Proc Init(oParentObject,pCommandExec)
        This.pCommandExec=pCommandExec
        DoDefault(oParentObject)
        Return
    Function OpenTables()
        Dimension This.cWorkTables[max(1,0)]
        Return(This.OpenAllTables(0))

        * --- Area Manuale = Functions & Procedures
        * --- Fine Area Manuale
Enddefine

Procedure getEntityType(result)
    result="Routine"
Endproc
Procedure getEntityParmList(result)
    result="pCommandExec"
Endproc
