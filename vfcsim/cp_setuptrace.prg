* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_setuptrace                                                   *
*              Activity logger setting                                         *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-23                                                      *
* Last revis.: 2009-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject
* --- Area Manuale = Header
* --- cp_setuptrace
If i_ServerConn[1,2]=0
    cp_ErrorMsg(cp_translate("Activity Logger non disponibile per database Visual FoxPro"))
    Return
Endif
* --- Fine Area Manuale
Return(Createobject("tcp_setuptrace",oParentObject))

* --- Class definition
Define Class tcp_setuptrace As StdForm
    Top    = 17
    Left   = 10

    * --- Standard Properties
    Width  = 732
    Height = 525
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2009-11-05"
    HelpContextID=219688695
    max_rt_seq=29

    * --- Constant Properties
    _IDX = 0
    cPrg = "cp_setuptrace"
    cComment = "Activity logger setting"
    oParentObject = .Null.
    Icon = "mask.ico"
    *closable = .f.

    * --- Local Variables
    w_ATTIVATO = 0
    o_ATTIVATO = 0
    w_POSIZIONELOG = Space(250)
    w_NOMELOG = Space(250)
    w_MULTISESSION = 0
    w_TIPOROLLOVER = Space(1)
    o_TIPOROLLOVER = Space(1)
    w_ROLLOVER = 0
    w_NUMERODIFILEDILOG = 0
    w_CONFIGURAZIONI = Space(1)
    w_TRACCIATURASQLLETTURA = 0
    w_TRACCIATURASQLLETTURA = 0
    w_TRACCIATURASQLSCRITTURA = 0
    w_TRACCIATURASQLINSERIMENTO = 0
    w_TRACCIATURASQLCANCELLAZIONE = 0
    w_TRACCIATURAVQR = 0
    w_TRACCIATURAGESTIONI = 0
    w_TRACCIATURAROUTINE = 0
    w_TRACCIATURATASTIFUN = 0
    w_TRACCIATURAEVENTI = 0
    w_RIGHE = 0
    w_MILLISECONDI = 0
    w_TRACCIATURASQLMODIFICADB = 0
    w_TRACCIATURASQLTEMPORANEO = 0
    w_TRACCIATURASQLALTROSUDB = 0
    w_TRACCIATURATRANSAZIONI = 0
    w_TRACCIATURAQUERYASINCRONA = 0
    w_TRACCIATURADEADLOCK = 0
    w_TRACCIATURARICONNESSIONE = 0
    w_FILECNF = Space(250)
    w_SESSIONNAME = Space(10)
    * --- Area Manuale = Declare Variables
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=1, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tcp_setuptracePag1","cp_setuptrace",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_translate("Pag.1")
            .Pages(1).oPag.oContained=Thisform
        Endwith
        This.Parent.oFirstControl = This.Page1.oPag.oATTIVATO_1_2
        This.Parent.cComment=cp_translate(This.Parent.cComment)
        * --- Area Manuale = Init Page Frame
        * --- Fine Area Manuale
    Endproc
    Proc Init(oParentObject)
        If Vartype(m.oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        DoDefault()

        * --- Open tables
    Function OpenWorkTables()
        Return(This.OpenAllTables(0))

    Procedure SetPostItConn()
        Return


        * --- Read record and initialize Form variables
    Procedure LoadRec()
    Endproc

    * --- ecpQuery
    Procedure ecpQuery()
        This.BlankRec()
        This.cFunction="Edit"
        This.SetStatus()
        If This.cFunction="Edit"
            This.mEnableControls()
        Endif
    Endproc
    * --- Esc
    Proc ecpQuit()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            This.cFunction = "Edit"
            This.oFirstControl.SetFocus()
            Return
        Endif
        * ---
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Endproc
    Proc QueryUnload()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            Nodefault
            Return
        Endif
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Proc ecpSave()
        If This.TerminateEdit() And This.CheckForm()
            This.LockScreen=.T.
            This.mReplace(.T.)
            This.LockScreen=.F.
            This.NotifyEvent("Done")
            This.Release()
        Endif
    Endproc
    Func OkToQuit()
        Return .T.
    Endfunc

    * --- Blank Form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        With This
            .w_ATTIVATO=0
            .w_POSIZIONELOG=Space(250)
            .w_NOMELOG=Space(250)
            .w_MULTISESSION=0
            .w_TIPOROLLOVER=Space(1)
            .w_ROLLOVER=0
            .w_NUMERODIFILEDILOG=0
            .w_CONFIGURAZIONI=Space(1)
            .w_TRACCIATURASQLLETTURA=0
            .w_TRACCIATURASQLLETTURA=0
            .w_TRACCIATURASQLSCRITTURA=0
            .w_TRACCIATURASQLINSERIMENTO=0
            .w_TRACCIATURASQLCANCELLAZIONE=0
            .w_TRACCIATURAVQR=0
            .w_TRACCIATURAGESTIONI=0
            .w_TRACCIATURAROUTINE=0
            .w_TRACCIATURATASTIFUN=0
            .w_TRACCIATURAEVENTI=0
            .w_RIGHE=0
            .w_MILLISECONDI=0
            .w_TRACCIATURASQLMODIFICADB=0
            .w_TRACCIATURASQLTEMPORANEO=0
            .w_TRACCIATURASQLALTROSUDB=0
            .w_TRACCIATURATRANSAZIONI=0
            .w_TRACCIATURAQUERYASINCRONA=0
            .w_TRACCIATURADEADLOCK=0
            .w_TRACCIATURARICONNESSIONE=0
            .w_FILECNF=Space(250)
            .w_SESSIONNAME=Space(10)
            .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
            .DoRTCalc(1,4,.F.)
            .w_TIPOROLLOVER = 'D'
            .w_ROLLOVER = Iif( .w_TIPOROLLOVER='D' , 1900, 10000 )
            .DoRTCalc(7,7,.F.)
            .w_CONFIGURAZIONI = "0"
            .oPgFrm.Page1.oPag.oObj_1_55.Calculate(Iif( .w_TIPOROLLOVER='D' , 'Raggiunta la dimensione in Megabyte specificata  la traccia viene rinominata e ne viene creata una nuova' ,  'Raggiunto il numero di record indicato la traccia viene rinominata e ne viene creata una nuova' ))
            .oPgFrm.Page1.oPag.oObj_1_56.Calculate(Iif( .w_TIPOROLLOVER='D' , 'Dimensione (MB):', 'Numero record per log:' ))
            .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        Endwith
        This.DoRTCalc(9,29,.F.)
        This.SaveDependsOn()
        This.SetControlsValue()
        This.mHideControls()
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = Setting operation
    *     Allowed Parameters: Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        * --- Deactivating List page when <> da Query
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt
        i_cFlt =""
        Return (i_cFlt)
    Endfunc

    Proc QueryKeySet(i_cWhere,i_cOrderBy)
    Endproc

    Proc SelectCursor
    Proc GetXKey

        * --- Update Database
    Function mReplace(i_bEditing)
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        This.NotifyEvent('Update start')
        With This
        Endwith
        This.NotifyEvent('Update end')
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(.T.)

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        If i_bUpd
            With This
                .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
                .DoRTCalc(1,5,.T.)
                If .o_TIPOROLLOVER<>.w_TIPOROLLOVER
                    .w_ROLLOVER = Iif( .w_TIPOROLLOVER='D' , 1900, 10000 )
                Endif
                .oPgFrm.Page1.oPag.oObj_1_55.Calculate(Iif( .w_TIPOROLLOVER='D' , 'Raggiunta la dimensione in Megabyte specificata  la traccia viene rinominata e ne viene creata una nuova' ,  'Raggiunto il numero di record indicato la traccia viene rinominata e ne viene creata una nuova' ))
                .oPgFrm.Page1.oPag.oObj_1_56.Calculate(Iif( .w_TIPOROLLOVER='D' , 'Dimensione (MB):', 'Numero record per log:' ))
                If .o_ATTIVATO<>.w_ATTIVATO
                    .Calculate_MDPKYCXTRM()
                Endif
                .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
            Endwith
            This.DoRTCalc(7,29,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
            .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
            .oPgFrm.Page1.oPag.oObj_1_55.Calculate(Iif( .w_TIPOROLLOVER='D' , 'Raggiunta la dimensione in Megabyte specificata  la traccia viene rinominata e ne viene creata una nuova' ,  'Raggiunto il numero di record indicato la traccia viene rinominata e ne viene creata una nuova' ))
            .oPgFrm.Page1.oPag.oObj_1_56.Calculate(Iif( .w_TIPOROLLOVER='D' , 'Dimensione (MB):', 'Numero record per log:' ))
            .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        Endwith
        Return

    Proc Calculate_WEJHQQFRIN()
        With This
            * --- Configura il log
            .w_TRACCIATURAGESTIONI = Iif( .w_CONFIGURAZIONI $ "T-E" , 1 , 0 )
            .w_TRACCIATURAROUTINE = Iif( .w_CONFIGURAZIONI $ "T-E" , 1 , 0 )
            .w_TRACCIATURASQLLETTURA = Iif( .w_CONFIGURAZIONI $ "T-S-Q" , Iif( .w_ATTIVATO=2, 2,1) , 0 )
            .w_TRACCIATURASQLSCRITTURA = Iif( .w_CONFIGURAZIONI $ "T-Q" , 1 , 0 )
            .w_TRACCIATURASQLINSERIMENTO = Iif( .w_CONFIGURAZIONI $ "T-Q" , 1 , 0 )
            .w_TRACCIATURASQLCANCELLAZIONE = Iif( .w_CONFIGURAZIONI $ "T-Q" , 1 , 0 )
            .w_TRACCIATURASQLMODIFICADB = Iif( .w_CONFIGURAZIONI $ "T" , 1 , 0 )
            .w_TRACCIATURASQLTEMPORANEO = Iif( .w_CONFIGURAZIONI $ "T-Q" , 1 , 0 )
            .w_TRACCIATURASQLALTROSUDB = Iif( .w_CONFIGURAZIONI $ "T" , 1 , 0 )
            .w_TRACCIATURATRANSAZIONI = Iif( .w_CONFIGURAZIONI $ "T" , 1 , 0 )
            .w_TRACCIATURAQUERYASINCRONA = Iif( .w_CONFIGURAZIONI $ "T-S-Q" ,  2 , 0 )
            .w_TRACCIATURADEADLOCK = Iif( .w_CONFIGURAZIONI $ "T-D" , 1 , 0 )
            .w_TRACCIATURARICONNESSIONE = Iif( .w_CONFIGURAZIONI $ "T-D" , 1 , 0 )
            .w_TRACCIATURAVQR = Iif( .w_CONFIGURAZIONI $ "T" , 1 , 0 )
            .w_TRACCIATURATASTIFUN = Iif( .w_CONFIGURAZIONI $ "T-E" , 1 , 0 )
            .w_TRACCIATURAEVENTI = Iif( .w_CONFIGURAZIONI $ "T-E" , 1 , 0 )
        Endwith
    Endproc
    Proc Calculate_MDPKYCXTRM()
        With This
            * --- Aggiorna SQL lettura e Query asincrona
            .w_TRACCIATURASQLLETTURA = Iif (.w_ATTIVATO=2 ,  Iif (.w_TRACCIATURASQLLETTURA>0,2,0)  , .w_TRACCIATURASQLLETTURA)
            .w_TRACCIATURAQUERYASINCRONA = Iif (.w_ATTIVATO=2 ,  Iif (.w_TRACCIATURAQUERYASINCRONA>0,2,0)  , .w_TRACCIATURAQUERYASINCRONA)
            .w_POSIZIONELOG = Iif (.w_ATTIVATO=0 Or .w_ATTIVATO=2,'',.w_POSIZIONELOG)
            .w_NOMELOG = Iif (.w_ATTIVATO=0 Or .w_ATTIVATO=2,'',.w_NOMELOG)
        Endwith
    Endproc

    * --- Enable controls under condition
    Procedure mEnableControls()
        This.oPgFrm.Page1.oPag.oPOSIZIONELOG_1_5.Enabled = This.oPgFrm.Page1.oPag.oPOSIZIONELOG_1_5.mCond()
        This.oPgFrm.Page1.oPag.oNOMELOG_1_7.Enabled = This.oPgFrm.Page1.oPag.oNOMELOG_1_7.mCond()
        This.oPgFrm.Page1.oPag.oMULTISESSION_1_8.Enabled = This.oPgFrm.Page1.oPag.oMULTISESSION_1_8.mCond()
        This.oPgFrm.Page1.oPag.oTIPOROLLOVER_1_9.Enabled = This.oPgFrm.Page1.oPag.oTIPOROLLOVER_1_9.mCond()
        This.oPgFrm.Page1.oPag.oROLLOVER_1_10.Enabled = This.oPgFrm.Page1.oPag.oROLLOVER_1_10.mCond()
        This.oPgFrm.Page1.oPag.oNUMERODIFILEDILOG_1_12.Enabled = This.oPgFrm.Page1.oPag.oNUMERODIFILEDILOG_1_12.mCond()
        This.oPgFrm.Page1.oPag.oCONFIGURAZIONI_1_15.Enabled = This.oPgFrm.Page1.oPag.oCONFIGURAZIONI_1_15.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_16.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_16.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_18.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_18.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURASQLSCRITTURA_1_20.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURASQLSCRITTURA_1_20.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURASQLINSERIMENTO_1_22.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURASQLINSERIMENTO_1_22.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURASQLCANCELLAZIONE_1_24.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURASQLCANCELLAZIONE_1_24.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURAVQR_1_25.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURAVQR_1_25.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURAGESTIONI_1_27.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURAGESTIONI_1_27.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURAROUTINE_1_28.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURAROUTINE_1_28.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURATASTIFUN_1_30.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURATASTIFUN_1_30.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURAEVENTI_1_31.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURAEVENTI_1_31.mCond()
        This.oPgFrm.Page1.oPag.oRIGHE_1_32.Enabled = This.oPgFrm.Page1.oPag.oRIGHE_1_32.mCond()
        This.oPgFrm.Page1.oPag.oMILLISECONDI_1_34.Enabled = This.oPgFrm.Page1.oPag.oMILLISECONDI_1_34.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURASQLMODIFICADB_1_35.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURASQLMODIFICADB_1_35.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURASQLTEMPORANEO_1_36.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURASQLTEMPORANEO_1_36.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURASQLALTROSUDB_1_37.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURASQLALTROSUDB_1_37.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURATRANSAZIONI_1_39.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURATRANSAZIONI_1_39.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURAQUERYASINCRONA_1_40.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURAQUERYASINCRONA_1_40.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURADEADLOCK_1_41.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURADEADLOCK_1_41.mCond()
        This.oPgFrm.Page1.oPag.oTRACCIATURARICONNESSIONE_1_42.Enabled = This.oPgFrm.Page1.oPag.oTRACCIATURARICONNESSIONE_1_42.mCond()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        This.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_16.Visible=!This.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_16.mHide()
        This.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_18.Visible=!This.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_18.mHide()
        This.oPgFrm.Page1.oPag.oStr_1_59.Visible=!This.oPgFrm.Page1.oPag.oStr_1_59.mHide()
        DoDefault()
        Return

        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            .oPgFrm.Page1.oPag.oObj_1_3.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_55.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
            If Lower(cEvent)==Lower("w_CONFIGURAZIONI Changed")
                .Calculate_WEJHQQFRIN()
                bRefresh=.T.
            Endif
            .oPgFrm.Page1.oPag.oObj_1_62.Event(cEvent)
        Endwith
        * --- Area Manuale = Notify Event End
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

    Function SetControlsValue()
        If Not(This.oPgFrm.Page1.oPag.oATTIVATO_1_2.RadioValue()==This.w_ATTIVATO)
            This.oPgFrm.Page1.oPag.oATTIVATO_1_2.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oPOSIZIONELOG_1_5.Value==This.w_POSIZIONELOG)
            This.oPgFrm.Page1.oPag.oPOSIZIONELOG_1_5.Value=This.w_POSIZIONELOG
        Endif
        If Not(This.oPgFrm.Page1.oPag.oNOMELOG_1_7.Value==This.w_NOMELOG)
            This.oPgFrm.Page1.oPag.oNOMELOG_1_7.Value=This.w_NOMELOG
        Endif
        If Not(This.oPgFrm.Page1.oPag.oMULTISESSION_1_8.RadioValue()==This.w_MULTISESSION)
            This.oPgFrm.Page1.oPag.oMULTISESSION_1_8.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTIPOROLLOVER_1_9.RadioValue()==This.w_TIPOROLLOVER)
            This.oPgFrm.Page1.oPag.oTIPOROLLOVER_1_9.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oROLLOVER_1_10.Value==This.w_ROLLOVER)
            This.oPgFrm.Page1.oPag.oROLLOVER_1_10.Value=This.w_ROLLOVER
        Endif
        If Not(This.oPgFrm.Page1.oPag.oNUMERODIFILEDILOG_1_12.Value==This.w_NUMERODIFILEDILOG)
            This.oPgFrm.Page1.oPag.oNUMERODIFILEDILOG_1_12.Value=This.w_NUMERODIFILEDILOG
        Endif
        If Not(This.oPgFrm.Page1.oPag.oCONFIGURAZIONI_1_15.RadioValue()==This.w_CONFIGURAZIONI)
            This.oPgFrm.Page1.oPag.oCONFIGURAZIONI_1_15.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_16.RadioValue()==This.w_TRACCIATURASQLLETTURA)
            This.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_16.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_18.RadioValue()==This.w_TRACCIATURASQLLETTURA)
            This.oPgFrm.Page1.oPag.oTRACCIATURASQLLETTURA_1_18.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURASQLSCRITTURA_1_20.RadioValue()==This.w_TRACCIATURASQLSCRITTURA)
            This.oPgFrm.Page1.oPag.oTRACCIATURASQLSCRITTURA_1_20.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURASQLINSERIMENTO_1_22.RadioValue()==This.w_TRACCIATURASQLINSERIMENTO)
            This.oPgFrm.Page1.oPag.oTRACCIATURASQLINSERIMENTO_1_22.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURASQLCANCELLAZIONE_1_24.RadioValue()==This.w_TRACCIATURASQLCANCELLAZIONE)
            This.oPgFrm.Page1.oPag.oTRACCIATURASQLCANCELLAZIONE_1_24.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURAVQR_1_25.RadioValue()==This.w_TRACCIATURAVQR)
            This.oPgFrm.Page1.oPag.oTRACCIATURAVQR_1_25.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURAGESTIONI_1_27.RadioValue()==This.w_TRACCIATURAGESTIONI)
            This.oPgFrm.Page1.oPag.oTRACCIATURAGESTIONI_1_27.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURAROUTINE_1_28.RadioValue()==This.w_TRACCIATURAROUTINE)
            This.oPgFrm.Page1.oPag.oTRACCIATURAROUTINE_1_28.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURATASTIFUN_1_30.RadioValue()==This.w_TRACCIATURATASTIFUN)
            This.oPgFrm.Page1.oPag.oTRACCIATURATASTIFUN_1_30.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURAEVENTI_1_31.RadioValue()==This.w_TRACCIATURAEVENTI)
            This.oPgFrm.Page1.oPag.oTRACCIATURAEVENTI_1_31.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oRIGHE_1_32.Value==This.w_RIGHE)
            This.oPgFrm.Page1.oPag.oRIGHE_1_32.Value=This.w_RIGHE
        Endif
        If Not(This.oPgFrm.Page1.oPag.oMILLISECONDI_1_34.Value==This.w_MILLISECONDI)
            This.oPgFrm.Page1.oPag.oMILLISECONDI_1_34.Value=This.w_MILLISECONDI
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURASQLMODIFICADB_1_35.RadioValue()==This.w_TRACCIATURASQLMODIFICADB)
            This.oPgFrm.Page1.oPag.oTRACCIATURASQLMODIFICADB_1_35.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURASQLTEMPORANEO_1_36.RadioValue()==This.w_TRACCIATURASQLTEMPORANEO)
            This.oPgFrm.Page1.oPag.oTRACCIATURASQLTEMPORANEO_1_36.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURASQLALTROSUDB_1_37.RadioValue()==This.w_TRACCIATURASQLALTROSUDB)
            This.oPgFrm.Page1.oPag.oTRACCIATURASQLALTROSUDB_1_37.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURATRANSAZIONI_1_39.RadioValue()==This.w_TRACCIATURATRANSAZIONI)
            This.oPgFrm.Page1.oPag.oTRACCIATURATRANSAZIONI_1_39.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURAQUERYASINCRONA_1_40.RadioValue()==This.w_TRACCIATURAQUERYASINCRONA)
            This.oPgFrm.Page1.oPag.oTRACCIATURAQUERYASINCRONA_1_40.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURADEADLOCK_1_41.RadioValue()==This.w_TRACCIATURADEADLOCK)
            This.oPgFrm.Page1.oPag.oTRACCIATURADEADLOCK_1_41.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTRACCIATURARICONNESSIONE_1_42.RadioValue()==This.w_TRACCIATURARICONNESSIONE)
            This.oPgFrm.Page1.oPag.oTRACCIATURARICONNESSIONE_1_42.SetRadio()
        Endif
        cp_SetControlsValueExtFlds(This,'')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            If .bUpdated
                Do Case
                    Case   Not(Not Empty( Justdrive( .w_POSIZIONELOG ) ))  And (.w_ATTIVATO=1  Or  .w_ATTIVATO=3)
                        .oPgFrm.ActivePage = 1
                        .oPgFrm.Page1.oPag.oPOSIZIONELOG_1_5.SetFocus()
                        i_bnoChk = .F.
                        i_bRes = .F.
                        i_cErrorMsg = Thisform.msgFmt("Controllare se il percorso selezionato fa uso di una unit� disco")
                Endcase
            Endif
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
            Else
                If Not(i_bnoChk)
                    cp_ErrorMsg(i_cErrorMsg)
                Endif
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

    * --- SaveDependsOn
    Proc SaveDependsOn()
        This.o_ATTIVATO = This.w_ATTIVATO
        This.o_TIPOROLLOVER = This.w_TIPOROLLOVER
        Return

Enddefine

* --- Define pages as container
Define Class tcp_setuptracePag1 As StdContainer
    Width  = 728
    Height = 525
    stdWidth  = 728
    stdheight = 525
    resizeXpos=355
    resizeYpos=168
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.


    Add Object oATTIVATO_1_2 As StdCombo With uid="JWOMUWHYQF",Value=1,rtseq=1,rtrep=.F.,Left=176,Top=15,Width=165,Height=21;
        , ToolTipText = "Attiva o disattiva il log";
        , HelpContextID = 156277211;
        , cFormVar="w_ATTIVATO",RowSource=""+"No,"+"Su DBF,"+"Su schermo,"+"Su schermo e su DBF", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oATTIVATO_1_2.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            iif(This.Value =3,2,;
            iif(This.Value =4,3,;
            0)))))
    Endfunc
    Func oATTIVATO_1_2.GetRadio()
        This.Parent.oContained.w_ATTIVATO = This.RadioValue()
        Return .T.
    Endfunc

    Func oATTIVATO_1_2.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_ATTIVATO==0,1,;
            iif(This.Parent.oContained.w_ATTIVATO==1,2,;
            iif(This.Parent.oContained.w_ATTIVATO==2,3,;
            iif(This.Parent.oContained.w_ATTIVATO==3,4,;
            0))))
    Endfunc


    Add Object oObj_1_3 As cp_runprogram With uid="RAHAXRFAOG",Left=745, Top=56, Width=192,Height=50,;
        caption='cp_setuptrace_b I',;
        prg="cp_setuptrace_b('I')",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 51562729;
        , bGlobalFont=.T.


    Add Object oPOSIZIONELOG_1_5 As StdField With uid="AZIHLBPRBL",rtseq=2,rtrep=.F.,;
        cFormVar = "w_POSIZIONELOG", cQueryName = "POSIZIONELOG",;
        bObbl = .F. , nPag = 1, Value=Space(250), bMultilanguage =  .F.,;
        sErrorMsg = "Controllare se il percorso selezionato fa uso di una unit� disco",;
        ToolTipText = "Cartella in cui vengono posti i file di log",;
        HelpContextID = 46390631,;
        bGlobalFont=.T.,;
        Height=21, Width=433, Left=176, Top=43, InputMask=Replicate('X',250), bHasZoom = .T.

    Func oPOSIZIONELOG_1_5.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO=1  Or  .w_ATTIVATO=3)
            Endwith
        Endif
    Endfunc

    Func oPOSIZIONELOG_1_5.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            bRes = (Not Empty( Justdrive( .w_POSIZIONELOG ) ))
        Endwith
        Return bRes
    Endfunc

    Proc oPOSIZIONELOG_1_5.mZoom
        cp_setuptrace_b(This.Parent.oContained,"G")
        If !Isnull(This.Parent.oContained)
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object oNOMELOG_1_7 As StdField With uid="UTYWSTQQBT",rtseq=3,rtrep=.F.,;
        cFormVar = "w_NOMELOG", cQueryName = "NOMELOG",;
        bObbl = .F. , nPag = 1, Value=Space(250), bMultilanguage =  .F.,;
        ToolTipText = "Nome assegnato al file di log",;
        HelpContextID = 246124652,;
        bGlobalFont=.T.,;
        Height=21, Width=433, Left=176, Top=73, InputMask=Replicate('X',250)

    Func oNOMELOG_1_7.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO=1  Or  .w_ATTIVATO=3)
            Endwith
        Endif
    Endfunc

    Add Object oMULTISESSION_1_8 As StdCheck With uid="CHLBCDOCVO",rtseq=4,rtrep=.F.,Left=614, Top=74, Caption="Multisessione",;
        ToolTipText = "Aggiunge al nome del log un identificatore diverso per ogni sessione",;
        HelpContextID = 142783154,;
        cFormVar="w_MULTISESSION", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oMULTISESSION_1_8.RadioValue()
        Return(Iif(This.Value =1,1,;
            0))
    Endfunc
    Func oMULTISESSION_1_8.GetRadio()
        This.Parent.oContained.w_MULTISESSION = This.RadioValue()
        Return .T.
    Endfunc

    Func oMULTISESSION_1_8.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_MULTISESSION==1,1,;
            0)
    Endfunc

    Func oMULTISESSION_1_8.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO=1  Or  .w_ATTIVATO=3)
            Endwith
        Endif
    Endfunc


    Add Object oTIPOROLLOVER_1_9 As StdCombo With uid="YEFWGKZIKD",rtseq=5,rtrep=.F.,Left=176,Top=102,Width=165,Height=21;
        , ToolTipText = "Per default il rollover viene attivato al raggiungimento di 2Gb";
        , HelpContextID = 186219420;
        , cFormVar="w_TIPOROLLOVER",RowSource=""+"Dimensione della traccia,"+"Numero di record", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTIPOROLLOVER_1_9.RadioValue()
        Return(Iif(This.Value =1,'D',;
            iif(This.Value =2,'R',;
            space(1))))
    Endfunc
    Func oTIPOROLLOVER_1_9.GetRadio()
        This.Parent.oContained.w_TIPOROLLOVER = This.RadioValue()
        Return .T.
    Endfunc

    Func oTIPOROLLOVER_1_9.SetRadio()
        This.Parent.oContained.w_TIPOROLLOVER=Trim(This.Parent.oContained.w_TIPOROLLOVER)
        This.Value = ;
            iif(This.Parent.oContained.w_TIPOROLLOVER=='D',1,;
            iif(This.Parent.oContained.w_TIPOROLLOVER=='R',2,;
            0))
    Endfunc

    Func oTIPOROLLOVER_1_9.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO=1  Or  .w_ATTIVATO=3)
            Endwith
        Endif
    Endfunc

    Add Object oROLLOVER_1_10 As StdField With uid="WMRRZSXSDW",rtseq=6,rtrep=.F.,;
        cFormVar = "w_ROLLOVER", cQueryName = "ROLLOVER",;
        bObbl = .F. , nPag = 1, Value=0, bMultilanguage =  .F.,;
        ToolTipText = "Raggiunto il numero di record indicato o la dimensione, la traccia viene rinominata e ne viene creata una nuova",;
        HelpContextID = 93134077,;
        bGlobalFont=.T.,;
        Height=21, Width=104, Left=505, Top=102, cSayPict='"999999999999999999"', cGetPict='"999999999999999999"'

    Func oROLLOVER_1_10.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO=1  Or  .w_ATTIVATO=3)
            Endwith
        Endif
    Endfunc

    Add Object oNUMERODIFILEDILOG_1_12 As StdField With uid="JXANHGTAJG",rtseq=7,rtrep=.F.,;
        cFormVar = "w_NUMERODIFILEDILOG", cQueryName = "NUMERODIFILEDILOG",;
        bObbl = .F. , nPag = 1, Value=0, bMultilanguage =  .F.,;
        ToolTipText = "Cancella le tracce pi� vecchie mantenendo le pi� recenti",;
        HelpContextID = 136465968,;
        bGlobalFont=.T.,;
        Height=20, Width=165, Left=176, Top=131, cSayPict='"999999999999999999"', cGetPict='"999999999999999999"'

    Func oNUMERODIFILEDILOG_1_12.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO=1  Or  .w_ATTIVATO=3)
            Endwith
        Endif
    Endfunc


    Add Object oCONFIGURAZIONI_1_15 As StdCombo With uid="GJWYOEGPUA",rtseq=8,rtrep=.F.,Left=182,Top=174,Width=165,Height=21;
        , ToolTipText = "Imposta i filtri";
        , HelpContextID = 58273826;
        , cFormVar="w_CONFIGURAZIONI",RowSource=""+"Seleziona tutto,"+"Esecuzione programmi,"+"Solo select SQL,"+"Solo istruzioni SQL,"+"Deadlock o riconnessioni,"+"Deseleziona tutto,"+"Selezione configurazione", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oCONFIGURAZIONI_1_15.RadioValue()
        Return(Iif(This.Value =1,'T',;
            iif(This.Value =2,'E',;
            iif(This.Value =3,'S',;
            iif(This.Value =4,'Q',;
            iif(This.Value =5,'D',;
            iif(This.Value =6,'N',;
            iif(This.Value =7,'0',;
            space(1)))))))))
    Endfunc
    Func oCONFIGURAZIONI_1_15.GetRadio()
        This.Parent.oContained.w_CONFIGURAZIONI = This.RadioValue()
        Return .T.
    Endfunc

    Func oCONFIGURAZIONI_1_15.SetRadio()
        This.Parent.oContained.w_CONFIGURAZIONI=Trim(This.Parent.oContained.w_CONFIGURAZIONI)
        This.Value = ;
            iif(This.Parent.oContained.w_CONFIGURAZIONI=='T',1,;
            iif(This.Parent.oContained.w_CONFIGURAZIONI=='E',2,;
            iif(This.Parent.oContained.w_CONFIGURAZIONI=='S',3,;
            iif(This.Parent.oContained.w_CONFIGURAZIONI=='Q',4,;
            iif(This.Parent.oContained.w_CONFIGURAZIONI=='D',5,;
            iif(This.Parent.oContained.w_CONFIGURAZIONI=='N',6,;
            iif(This.Parent.oContained.w_CONFIGURAZIONI=='0',7,;
            0)))))))
    Endfunc

    Func oCONFIGURAZIONI_1_15.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc


    Add Object oTRACCIATURASQLLETTURA_1_16 As StdCombo With uid="MFWYCHOFQW",Value=1,rtseq=9,rtrep=.F.,Left=182,Top=208,Width=165,Height=21;
        , ToolTipText = "Traccia le istruzioni select";
        , HelpContextID = 170877774;
        , cFormVar="w_TRACCIATURASQLLETTURA",RowSource=""+"No,"+"S�", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURASQLLETTURA_1_16.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,2,;
            0)))
    Endfunc
    Func oTRACCIATURASQLLETTURA_1_16.GetRadio()
        This.Parent.oContained.w_TRACCIATURASQLLETTURA = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURASQLLETTURA_1_16.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURASQLLETTURA==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURASQLLETTURA==2,2,;
            0))
    Endfunc

    Func oTRACCIATURASQLLETTURA_1_16.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc

    Func oTRACCIATURASQLLETTURA_1_16.mHide()
        With This.Parent.oContained
            Return (.w_ATTIVATO<>2)
        Endwith
    Endfunc


    Add Object oTRACCIATURASQLLETTURA_1_18 As StdCombo With uid="XVTJZYNMAO",Value=1,rtseq=10,rtrep=.F.,Left=182,Top=208,Width=165,Height=21;
        , ToolTipText = "Istruzioni select";
        , HelpContextID = 170877774;
        , cFormVar="w_TRACCIATURASQLLETTURA",RowSource=""+"No,"+"S� con DBF del risultato,"+"S� senza DBF del risultato", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURASQLLETTURA_1_18.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            iif(This.Value =3,2,;
            0))))
    Endfunc
    Func oTRACCIATURASQLLETTURA_1_18.GetRadio()
        This.Parent.oContained.w_TRACCIATURASQLLETTURA = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURASQLLETTURA_1_18.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURASQLLETTURA==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURASQLLETTURA==1,2,;
            iif(This.Parent.oContained.w_TRACCIATURASQLLETTURA==2,3,;
            0)))
    Endfunc

    Func oTRACCIATURASQLLETTURA_1_18.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc

    Func oTRACCIATURASQLLETTURA_1_18.mHide()
        With This.Parent.oContained
            Return (.w_ATTIVATO = 2)
        Endwith
    Endfunc


    Add Object oTRACCIATURASQLSCRITTURA_1_20 As StdCombo With uid="QNSXGAXUZP",Value=1,rtseq=11,rtrep=.F.,Left=182,Top=236,Width=165,Height=21;
        , ToolTipText = "Traccia le istruzioni update";
        , HelpContextID = 140285805;
        , cFormVar="w_TRACCIATURASQLSCRITTURA",RowSource=""+"No,"+"S�", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURASQLSCRITTURA_1_20.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            0)))
    Endfunc
    Func oTRACCIATURASQLSCRITTURA_1_20.GetRadio()
        This.Parent.oContained.w_TRACCIATURASQLSCRITTURA = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURASQLSCRITTURA_1_20.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURASQLSCRITTURA==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURASQLSCRITTURA==1,2,;
            0))
    Endfunc

    Func oTRACCIATURASQLSCRITTURA_1_20.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc


    Add Object oTRACCIATURASQLINSERIMENTO_1_22 As StdCombo With uid="SEEIRHXYQR",Value=1,rtseq=12,rtrep=.F.,Left=182,Top=264,Width=165,Height=21;
        , ToolTipText = "Traccia le istruzioni insert";
        , HelpContextID = 240041659;
        , cFormVar="w_TRACCIATURASQLINSERIMENTO",RowSource=""+"No,"+"S�", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURASQLINSERIMENTO_1_22.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            0)))
    Endfunc
    Func oTRACCIATURASQLINSERIMENTO_1_22.GetRadio()
        This.Parent.oContained.w_TRACCIATURASQLINSERIMENTO = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURASQLINSERIMENTO_1_22.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURASQLINSERIMENTO==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURASQLINSERIMENTO==1,2,;
            0))
    Endfunc

    Func oTRACCIATURASQLINSERIMENTO_1_22.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc


    Add Object oTRACCIATURASQLCANCELLAZIONE_1_24 As StdCombo With uid="NHRNQINKTD",Value=1,rtseq=13,rtrep=.F.,Left=182,Top=292,Width=165,Height=21;
        , ToolTipText = "Traccia le  istruzioni delete";
        , HelpContextID = 131366435;
        , cFormVar="w_TRACCIATURASQLCANCELLAZIONE",RowSource=""+"No,"+"S�", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURASQLCANCELLAZIONE_1_24.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            0)))
    Endfunc
    Func oTRACCIATURASQLCANCELLAZIONE_1_24.GetRadio()
        This.Parent.oContained.w_TRACCIATURASQLCANCELLAZIONE = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURASQLCANCELLAZIONE_1_24.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURASQLCANCELLAZIONE==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURASQLCANCELLAZIONE==1,2,;
            0))
    Endfunc

    Func oTRACCIATURASQLCANCELLAZIONE_1_24.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc


    Add Object oTRACCIATURAVQR_1_25 As StdCombo With uid="HARCTXOMJN",Value=1,rtseq=14,rtrep=.F.,Left=182,Top=320,Width=165,Height=21;
        , ToolTipText = "Traccia le chiamate di visual query";
        , HelpContextID = 26443017;
        , cFormVar="w_TRACCIATURAVQR",RowSource=""+"No,"+"S�", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURAVQR_1_25.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            0)))
    Endfunc
    Func oTRACCIATURAVQR_1_25.GetRadio()
        This.Parent.oContained.w_TRACCIATURAVQR = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURAVQR_1_25.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURAVQR==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURAVQR==1,2,;
            0))
    Endfunc

    Func oTRACCIATURAVQR_1_25.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc


    Add Object oTRACCIATURAGESTIONI_1_27 As StdCombo With uid="GPZIRJZKDJ",Value=1,rtseq=15,rtrep=.F.,Left=182,Top=348,Width=165,Height=21;
        , ToolTipText = "Traccia apertura e chiusura di form a video";
        , HelpContextID = 176717726;
        , cFormVar="w_TRACCIATURAGESTIONI",RowSource=""+"No,"+"S�", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURAGESTIONI_1_27.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            0)))
    Endfunc
    Func oTRACCIATURAGESTIONI_1_27.GetRadio()
        This.Parent.oContained.w_TRACCIATURAGESTIONI = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURAGESTIONI_1_27.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURAGESTIONI==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURAGESTIONI==1,2,;
            0))
    Endfunc

    Func oTRACCIATURAGESTIONI_1_27.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc


    Add Object oTRACCIATURAROUTINE_1_28 As StdCombo With uid="GDLUQCNJQY",Value=1,rtseq=16,rtrep=.F.,Left=182,Top=376,Width=165,Height=21;
        , ToolTipText = "Traccia avvio e chiusura delle routine";
        , HelpContextID = 74951369;
        , cFormVar="w_TRACCIATURAROUTINE",RowSource=""+"No,"+"S�", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURAROUTINE_1_28.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            0)))
    Endfunc
    Func oTRACCIATURAROUTINE_1_28.GetRadio()
        This.Parent.oContained.w_TRACCIATURAROUTINE = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURAROUTINE_1_28.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURAROUTINE==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURAROUTINE==1,2,;
            0))
    Endfunc

    Func oTRACCIATURAROUTINE_1_28.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc


    Add Object oTRACCIATURATASTIFUN_1_30 As StdCombo With uid="TERNEMWZCZ",Value=1,rtseq=17,rtrep=.F.,Left=182,Top=404,Width=165,Height=21;
        , ToolTipText = "Traccia tasti funzioni F3,F4,F5,F6,F10,F12 ed ESC";
        , HelpContextID = 209157431;
        , cFormVar="w_TRACCIATURATASTIFUN",RowSource=""+"No,"+"S�", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURATASTIFUN_1_30.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            0)))
    Endfunc
    Func oTRACCIATURATASTIFUN_1_30.GetRadio()
        This.Parent.oContained.w_TRACCIATURATASTIFUN = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURATASTIFUN_1_30.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURATASTIFUN==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURATASTIFUN==1,2,;
            0))
    Endfunc

    Func oTRACCIATURATASTIFUN_1_30.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc


    Add Object oTRACCIATURAEVENTI_1_31 As StdCombo With uid="YRPSPERZEO",Value=1,rtseq=18,rtrep=.F.,Left=182,Top=432,Width=165,Height=21;
        , ToolTipText = "Traccia la notifica degli eventi";
        , HelpContextID = 266292999;
        , cFormVar="w_TRACCIATURAEVENTI",RowSource=""+"No,"+"S�", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURAEVENTI_1_31.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            0)))
    Endfunc
    Func oTRACCIATURAEVENTI_1_31.GetRadio()
        This.Parent.oContained.w_TRACCIATURAEVENTI = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURAEVENTI_1_31.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURAEVENTI==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURAEVENTI==1,2,;
            0))
    Endfunc

    Func oTRACCIATURAEVENTI_1_31.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc

    Add Object oRIGHE_1_32 As StdField With uid="ZYQSVUKXYD",rtseq=19,rtrep=.F.,;
        cFormVar = "w_RIGHE", cQueryName = "RIGHE",;
        bObbl = .F. , nPag = 1, Value=0, bMultilanguage =  .F.,;
        ToolTipText = "Numero minimo di righe interessate per cui l'istruzione SQLviene tracciata",;
        HelpContextID = 84441278,;
        bGlobalFont=.T.,;
        Height=21, Width=165, Left=182, Top=462, cSayPict='"999999999999999999"', cGetPict='"999999999999999999"'

    Func oRIGHE_1_32.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO<>0)
            Endwith
        Endif
    Endfunc

    Add Object oMILLISECONDI_1_34 As StdField With uid="FAHYKCYVRA",rtseq=20,rtrep=.F.,;
        cFormVar = "w_MILLISECONDI", cQueryName = "MILLISECONDI",;
        bObbl = .F. , nPag = 1, Value=0, bMultilanguage =  .F.,;
        ToolTipText = "Durata minima in millisecondi per cui l'istruzione SQLviene tracciata",;
        HelpContextID = 209538601,;
        bGlobalFont=.T.,;
        Height=21, Width=165, Left=182, Top=489, cSayPict='"999999999999999999"', cGetPict='"999999999999999999"'

    Func oMILLISECONDI_1_34.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO<>0)
            Endwith
        Endif
    Endfunc


    Add Object oTRACCIATURASQLMODIFICADB_1_35 As StdCombo With uid="OZVKJLRYBM",Value=1,rtseq=21,rtrep=.F.,Left=553,Top=208,Width=165,Height=21;
        , ToolTipText = "Ricostruzione database";
        , HelpContextID = 255358127;
        , cFormVar="w_TRACCIATURASQLMODIFICADB",RowSource=""+"No,"+"S�", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURASQLMODIFICADB_1_35.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            0)))
    Endfunc
    Func oTRACCIATURASQLMODIFICADB_1_35.GetRadio()
        This.Parent.oContained.w_TRACCIATURASQLMODIFICADB = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURASQLMODIFICADB_1_35.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURASQLMODIFICADB==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURASQLMODIFICADB==1,2,;
            0))
    Endfunc

    Func oTRACCIATURASQLMODIFICADB_1_35.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc


    Add Object oTRACCIATURASQLTEMPORANEO_1_36 As StdCombo With uid="KOWHCWYDNN",Value=1,rtseq=22,rtrep=.F.,Left=553,Top=236,Width=165,Height=21;
        , ToolTipText = "Creazioni temporanei su server";
        , HelpContextID = 126794936;
        , cFormVar="w_TRACCIATURASQLTEMPORANEO",RowSource=""+"No,"+"S�", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURASQLTEMPORANEO_1_36.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            0)))
    Endfunc
    Func oTRACCIATURASQLTEMPORANEO_1_36.GetRadio()
        This.Parent.oContained.w_TRACCIATURASQLTEMPORANEO = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURASQLTEMPORANEO_1_36.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURASQLTEMPORANEO==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURASQLTEMPORANEO==1,2,;
            0))
    Endfunc

    Func oTRACCIATURASQLTEMPORANEO_1_36.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc


    Add Object oTRACCIATURASQLALTROSUDB_1_37 As StdCombo With uid="LBIHXUKAPM",Value=1,rtseq=23,rtrep=.F.,Left=553,Top=264,Width=165,Height=21;
        , ToolTipText = "Altre istruzioni su database";
        , HelpContextID = 86329512;
        , cFormVar="w_TRACCIATURASQLALTROSUDB",RowSource=""+"No,"+"S�", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURASQLALTROSUDB_1_37.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            0)))
    Endfunc
    Func oTRACCIATURASQLALTROSUDB_1_37.GetRadio()
        This.Parent.oContained.w_TRACCIATURASQLALTROSUDB = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURASQLALTROSUDB_1_37.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURASQLALTROSUDB==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURASQLALTROSUDB==1,2,;
            0))
    Endfunc

    Func oTRACCIATURASQLALTROSUDB_1_37.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc


    Add Object oTRACCIATURATRANSAZIONI_1_39 As StdCombo With uid="KLQBURDHVZ",Value=1,rtseq=24,rtrep=.F.,Left=553,Top=292,Width=165,Height=21;
        , ToolTipText = "Begin transaction - commit - rollback";
        , HelpContextID = 135614754;
        , cFormVar="w_TRACCIATURATRANSAZIONI",RowSource=""+"No,"+"S�", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURATRANSAZIONI_1_39.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            0)))
    Endfunc
    Func oTRACCIATURATRANSAZIONI_1_39.GetRadio()
        This.Parent.oContained.w_TRACCIATURATRANSAZIONI = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURATRANSAZIONI_1_39.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURATRANSAZIONI==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURATRANSAZIONI==1,2,;
            0))
    Endfunc

    Func oTRACCIATURATRANSAZIONI_1_39.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc


    Add Object oTRACCIATURAQUERYASINCRONA_1_40 As StdCombo With uid="WNNERTPZLR",Value=1,rtseq=25,rtrep=.F.,Left=553,Top=320,Width=165,Height=21;
        , ToolTipText = "Traccia query asincrone";
        , HelpContextID = 245962978;
        , cFormVar="w_TRACCIATURAQUERYASINCRONA",RowSource=""+"No,"+"S�", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURAQUERYASINCRONA_1_40.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,2,;
            0)))
    Endfunc
    Func oTRACCIATURAQUERYASINCRONA_1_40.GetRadio()
        This.Parent.oContained.w_TRACCIATURAQUERYASINCRONA = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURAQUERYASINCRONA_1_40.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURAQUERYASINCRONA==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURAQUERYASINCRONA==2,2,;
            0))
    Endfunc

    Func oTRACCIATURAQUERYASINCRONA_1_40.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc


    Add Object oTRACCIATURADEADLOCK_1_41 As StdCombo With uid="TPTSJDDDWM",Value=1,rtseq=26,rtrep=.F.,Left=553,Top=376,Width=165,Height=21;
        , ToolTipText = "Traccia i deadlock";
        , HelpContextID = 174694348;
        , cFormVar="w_TRACCIATURADEADLOCK",RowSource=""+"No,"+"S�", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURADEADLOCK_1_41.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            0)))
    Endfunc
    Func oTRACCIATURADEADLOCK_1_41.GetRadio()
        This.Parent.oContained.w_TRACCIATURADEADLOCK = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURADEADLOCK_1_41.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURADEADLOCK==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURADEADLOCK==1,2,;
            0))
    Endfunc

    Func oTRACCIATURADEADLOCK_1_41.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc


    Add Object oTRACCIATURARICONNESSIONE_1_42 As StdCombo With uid="APMEYUEVOV",Value=1,rtseq=27,rtrep=.F.,Left=553,Top=404,Width=165,Height=21;
        , ToolTipText = "Traccia le riconnessioni in seguito a caduta di connessione";
        , HelpContextID = 157598540;
        , cFormVar="w_TRACCIATURARICONNESSIONE",RowSource=""+"No,"+"S�", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oTRACCIATURARICONNESSIONE_1_42.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            0)))
    Endfunc
    Func oTRACCIATURARICONNESSIONE_1_42.GetRadio()
        This.Parent.oContained.w_TRACCIATURARICONNESSIONE = This.RadioValue()
        Return .T.
    Endfunc

    Func oTRACCIATURARICONNESSIONE_1_42.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_TRACCIATURARICONNESSIONE==0,1,;
            iif(This.Parent.oContained.w_TRACCIATURARICONNESSIONE==1,2,;
            0))
    Endfunc

    Func oTRACCIATURARICONNESSIONE_1_42.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_ATTIVATO>0)
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_45 As StdButton With uid="TPPZXKGHKR",Left=508, Top=474, Width=48,Height=45,;
        Picture="NewOffe.bmp", Caption="", nPag=1;
        , ToolTipText = "Ripulisce l'output su schermo";
        , HelpContextID = 167235766;
        , Caption='C\<LS';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_45.Click()
        With This.Parent.oContained
            _Screen.Cls()
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_1_46 As StdButton With uid="MEVYVBNJJZ",Left=562, Top=474, Width=48,Height=45,;
        Picture="CONTRATT.BMP", Caption="", nPag=1;
        , ToolTipText = "Scrive le impostazioni nel file CNF";
        , HelpContextID = 167235779;
        , Caption='\<Cnf';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_46.Click()
        With This.Parent.oContained
            cp_setuptrace_b(This.Parent.oContained,"C")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_1_48 As StdButton With uid="KCCEPGRZSR",Left=617, Top=474, Width=48,Height=45,;
        Picture="APPLICA.BMP", Caption="", nPag=1;
        , ToolTipText = "Applica le impostazioni alla sessione di lavoro corrente";
        , HelpContextID = 193999065;
        , Caption='\<Applica';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_48.Click()
        With This.Parent.oContained
            cp_setuptrace_b(This.Parent.oContained,"S")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_1_50 As StdButton With uid="HKYUBHHNJZ",Left=671, Top=474, Width=48,Height=45,;
        Picture="ESC.BMP", Caption="", nPag=1;
        , ToolTipText = "Chiude la maschera";
        , HelpContextID = 133679638;
        , Caption='\<Esci';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_50.Click()
        =cp_StandardFunction(This,"Quit")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oObj_1_55 As cp_setobjprop With uid="SKPUZOAYUU",Left=744, Top=114, Width=124,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_ROLLOVER",;
        cEvent = "w_TIPOROLLOVER changed",;
        nPag=1;
        , HelpContextID = 233842511;
        , bGlobalFont=.T.



    Add Object oObj_1_56 As cp_calclbl With uid="JWKRNWDWTT",Left=365, Top=106, Width=136,Height=25,;
        caption='Object',;
        caption="Record per log",;
        cEvent = "w_TIPOROLLOVER changed",;
        nPag=1;
        , HelpContextID = 233842511;
        , bGlobalFont=.T.



    Add Object oObj_1_62 As cp_runprogram With uid="FOSXEOKNMH",Left=744, Top=148, Width=192,Height=50,;
        caption='cp_setuptrace_b D',;
        prg="cp_setuptrace_b('D')",;
        cEvent = "w_POSIZIONELOG Changed",;
        nPag=1;
        , HelpContextID = 135448809;
        , bGlobalFont=.T.


    Add Object oStr_1_1 As StdString With uid="ASYEXESRCO",Visible=.T., Left=27, Top=16,;
        Alignment=1, Width=143, Height=18,;
        Caption="Attivazione log:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_4 As StdString With uid="EWYERTEVMX",Visible=.T., Left=29, Top=44,;
        Alignment=1, Width=141, Height=18,;
        Caption="Posizione log:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_6 As StdString With uid="VTGXORRIZR",Visible=.T., Left=42, Top=73,;
        Alignment=1, Width=128, Height=18,;
        Caption="Nome log:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_11 As StdString With uid="DJSWDSEWWZ",Visible=.T., Left=38, Top=135,;
        Alignment=1, Width=132, Height=18,;
        Caption="Log da mantenere:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_13 As StdString With uid="EZWUNVAJOO",Visible=.T., Left=26, Top=351,;
        Alignment=1, Width=150, Height=18,;
        Caption="Gestioni:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_14 As StdString With uid="TIHXOUMEKY",Visible=.T., Left=26, Top=379,;
        Alignment=1, Width=150, Height=18,;
        Caption="Routine:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_17 As StdString With uid="ZUVUGXVQLW",Visible=.T., Left=392, Top=320,;
        Alignment=1, Width=155, Height=18,;
        Caption="Query asincrona:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_19 As StdString With uid="CTTANLJVIY",Visible=.T., Left=26, Top=208,;
        Alignment=1, Width=150, Height=18,;
        Caption="SQL lettura:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_21 As StdString With uid="YKUFMJSPWU",Visible=.T., Left=26, Top=236,;
        Alignment=1, Width=150, Height=18,;
        Caption="SQL aggiornamento:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_23 As StdString With uid="VYDSTWEYQX",Visible=.T., Left=16, Top=264,;
        Alignment=1, Width=160, Height=18,;
        Caption="SQL inserimento:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_26 As StdString With uid="XNUKEVYSSZ",Visible=.T., Left=7, Top=292,;
        Alignment=1, Width=169, Height=18,;
        Caption="SQL cancellazione:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_29 As StdString With uid="RJBTKIHVSD",Visible=.T., Left=357, Top=236,;
        Alignment=1, Width=190, Height=18,;
        Caption="Creazione temporaneo:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_33 As StdString With uid="CVTCQVIYCZ",Visible=.T., Left=397, Top=208,;
        Alignment=1, Width=150, Height=18,;
        Caption="Modifica DB:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_38 As StdString With uid="BJUCAPEUKT",Visible=.T., Left=367, Top=264,;
        Alignment=1, Width=180, Height=18,;
        Caption="Altre istruzioni su DB:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_43 As StdString With uid="GCUVXGWWJM",Visible=.T., Left=397, Top=292,;
        Alignment=1, Width=150, Height=18,;
        Caption="Transazioni:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_44 As StdString With uid="PXAQHCUQKM",Visible=.T., Left=44, Top=174,;
        Alignment=1, Width=132, Height=19,;
        Caption="Configurazioni:"  ;
        , FontName = "Arial", FontSize = 10, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_1_47 As StdString With uid="XEVUUSCNYD",Visible=.T., Left=367, Top=376,;
        Alignment=1, Width=180, Height=18,;
        Caption="Deadlock:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_49 As StdString With uid="SZMMKKYOJR",Visible=.T., Left=397, Top=404,;
        Alignment=1, Width=150, Height=18,;
        Caption="Riconnessione:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_54 As StdString With uid="LSWBMIKZMI",Visible=.T., Left=85, Top=105,;
        Alignment=1, Width=85, Height=18,;
        Caption="Rollover su:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_59 As StdString With uid="PRJBNTFPCN",Visible=.T., Left=17, Top=627,;
        Alignment=0, Width=457, Height=18,;
        Caption="Attenzione le combo relative a SQL lettura e Asincrona sono sovrapposte"  ;
        , bGlobalFont=.T.

    Func oStr_1_59.mHide()
        With This.Parent.oContained
            Return (.T.)
        Endwith
    Endfunc

    Add Object oStr_1_60 As StdString With uid="WGBEEEDINI",Visible=.T., Left=40, Top=463,;
        Alignment=1, Width=136, Height=18,;
        Caption="Nro minimo righe:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_61 As StdString With uid="WCWIWEYKKB",Visible=.T., Left=38, Top=490,;
        Alignment=1, Width=138, Height=18,;
        Caption="Durata minima:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_63 As StdString With uid="UUEQFBGCZT",Visible=.T., Left=26, Top=404,;
        Alignment=1, Width=150, Height=18,;
        Caption="Tasti funzione:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_64 As StdString With uid="LAGBPFRSBS",Visible=.T., Left=26, Top=432,;
        Alignment=1, Width=150, Height=18,;
        Caption="Eventi:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_65 As StdString With uid="TWKNMRVKVZ",Visible=.T., Left=19, Top=319,;
        Alignment=1, Width=157, Height=18,;
        Caption="Chiamata di visual query:"  ;
        , bGlobalFont=.T.

    Add Object oBox_1_52 As StdBox With uid="FSFCXWUHTK",Left=10, Top=165, Width=705,Height=2
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_setuptrace','','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Dialog Window"
Endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
