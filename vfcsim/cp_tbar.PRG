* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_TBAR
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 20/03/97
* Translated    : 03/03/2000
* #%&%#Build:  59
* ----------------------------------------------------------------------------
* Toolbar standard
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

* --- Definizione classe ToolBar
Define Class CPToolBar As CommandBar
    Procedure Init(bStyle)
        DoDefault(m.bStyle)
        Local oBtn
        With This
            .Height=10
            *--- Help Btn
            m.oBtn = .AddButton("b1")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "help.bmp"
            m.oBtn._OnClick = "do cp_help"
            m.oBtn.ToolTipText = cp_Translate(MSG_HELP)+" (F1)"
            m.oBtn.Visible=.T.
            *--- Separatore
            .AddObject("s1","Separator")
            #If Version(5)>=900
                .s1.Visible=.T.
            #Endif
            *--- Edit Btn
            m.oBtn = .AddButton("b2")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "modiftbl.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpEdit'"
            m.oBtn.ToolTipText = cp_Translate(MSG_CHANGE)+" (F3)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=.T.
            *--- Load Btn
            m.oBtn = .AddButton("b3")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "addtabl.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpLoad'"
            m.oBtn.ToolTipText = cp_Translate(MSG_LOAD)+" (F4)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=.T.
            *--- Save Btn
            m.oBtn = .AddButton("b4")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "save.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpSave'"
            m.oBtn.ToolTipText = cp_Translate(MSG_SAVE)+" (F10)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=.T.
            *--- Separatore
            .AddObject("s2","Separator")
            #If Version(5)>=900
                .s2.Visible=.T.
            #Endif
            *---Delete Btn
            m.oBtn = .AddButton("b5")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "canc.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpDelete'"
            m.oBtn.ToolTipText = cp_Translate(MSG_DELETE)+" (F5)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=.T.
            *---Delete Row Btn
            m.oBtn = .AddButton("b5b")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "canc_riga.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpF6'"
            m.oBtn.ToolTipText = cp_Translate(MSG_DELETE)+" (F6)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=.T.
            *--- Separatore
            .AddObject("s3","Separator")
            #If Version(5)>=900
                .s3.Visible=.T.
            #Endif

            *---Print Btn
            m.oBtn = .AddButton("b6")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "print.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpPrint'"
            m.oBtn.ToolTipText = cp_Translate(MSG_PRINT) +" (F2)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=.T.
            *--- Separatore
            .AddObject("s4","Separator")
            #If Version(5)>=900
                .s4.Visible=.T.
            #Endif

            *---Prior Btn
            m.oBtn = .AddButton("b7")
            m.oBtn.Caption = ''
            m.oBtn.cImage = "prior.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpPrior'"
            m.oBtn.ToolTipText = cp_Translate(MGS_PREVIOUS_RECORD)+" (F7)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=.T.
            *---Next Btn
            m.oBtn = .AddButton("b8")
            m.oBtn.Caption = ''
            m.oBtn.cImage =  "next.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpNext'"
            m.oBtn.ToolTipText = cp_Translate(MSG_NEXT_RECORD)+" (F8)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=.T.
            *--- Separatore
            .AddObject("s5","Separator")
            #If Version(5)>=900
                .s5.Visible=.T.
            #Endif
            *---Zoom Btn
            m.oBtn = .AddButton("b9")
            m.oBtn.Caption = ''
            m.oBtn.cImage =  "zoom.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpZoom'"
            m.oBtn.ToolTipText = cp_Translate(MSG_ZOOM)+" (F9)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=.T.
            *--- Separatore
            .AddObject("s6","Separator")
            #If Version(5)>=900
                .s6.Visible=.T.
            #Endif

            *---PgUp Btn
            m.oBtn = .AddButton("b10")
            m.oBtn.Caption = ''
            m.oBtn.cImage =  "PgUp.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpPgUp'"
            m.oBtn.ToolTipText = cp_Translate(MSG_PAGE_UP)+" (PgUp)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=.T.
            *---PgDn Btn
            m.oBtn = .AddButton("b11")
            m.oBtn.Caption = ''
            m.oBtn.cImage =  "PgDn.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpPgDn'"
            m.oBtn.ToolTipText = cp_Translate(MSG_PAGE_DOWN)+" (PgDn)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=.T.
            *--- Separatore
            .AddObject("s7","Separator")
            #If Version(5)>=900
                .s7.Visible=.T.
            #Endif
            *---Filter Btn
            m.oBtn = .AddButton("b12")
            m.oBtn.Caption = ''
            m.oBtn.cImage =  "filter.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpFilter'"
            m.oBtn.ToolTipText = cp_Translate(MSG_FILTER)+" (F12)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=.T.
            *--- Separatore
            .AddObject("s8","Separator")
            #If Version(5)>=900
                .s8.Visible=.T.
            #Endif

            *---Filter Btn
            m.oBtn = .AddButton("b13")
            m.oBtn.Caption = ''
            m.oBtn.cImage =  "escTB.bmp"
            m.oBtn._OnClick = "do cp_DoAction with 'ecpQuit'"
            m.oBtn.ToolTipText = cp_Translate(MSG_EXIT)+" (Esc)"
            m.oBtn.Enabled=.F.
            m.oBtn.Visible=.T.
            *-* *--- Separatore
            .AddObject("s9","Separator")
            #If Version(5)>=900
                .s9.Visible=.T.
            #Endif
            Local nPixel, cChr
            nPixel = 6
            If i_nTBTNH>16
                cChr = Chr(10)
            Else
                cChr = ' '
            Endif
            *-* * --- Box Utente
            .AddObject("boxute","CpEditBox")
            .Boxute.SpecialEffect=1
            .Boxute.Alignment=0
            .Boxute.Value=MSG_TB_USER+cChr+Alltrim(Str(i_codute))
            .Boxute.Enabled=.F.
            .Boxute.DisabledForeColor=Rgb(157,157,161)
            .Boxute.BorderColor=Rgb(165,172,178)
            .Boxute.FontName="Arial"
            .Boxute.FontSize=7
            .Boxute.Width=40
            .Boxute.Height=i_nTBTNH+nPixel
            .Boxute.DisabledBackColor=Rgb(255,255,255)
            .Boxute.FontBold=.F.
            .Boxute.Visible=.T.
            *-* * --- Box Azienda
            .AddObject("boxazi","CpEditBox")
            .boxazi.Alignment=0
            .boxazi.Value=MSG_TB_COMPANY+cChr+Alltrim(i_codazi)
            .boxazi.Enabled=.F.
            .boxazi.SpecialEffect=1
            .boxazi.DisabledForeColor=Rgb(157,157,161)
            .boxazi.BorderColor=Rgb(165,172,178)
            .boxazi.FontName="Arial"
            .boxazi.FontSize=7
            .boxazi.Width=45
            .boxazi.Height=i_nTBTNH+nPixel
            .boxazi.DisabledBackColor=Rgb(255,255,255)
            .boxazi.FontBold=.F.
            .boxazi.Visible=.T.
            * --- Box Data
            .AddObject("boxdata","CpEditBox")
            .boxdata.Alignment=0
            .boxdata.Value=MSG_TB_DATE+cChr+Alltrim(Dtoc(i_datsys))
            .boxdata.Enabled=.F.
            .boxdata.SpecialEffect=1
            .boxdata.DisabledForeColor=Rgb(157,157,161)
            .boxdata.BorderColor=Rgb(165,172,178)
            .boxdata.FontName="Arial"
            .boxdata.FontSize=7
            .boxdata.Width=55
            .boxdata.Height=i_nTBTNH+nPixel
            .boxdata.DisabledBackColor=Rgb(255,255,255)
            .boxdata.Visible=.T.

            .Caption=cp_Translate(MSG_TOOLBAR)
            .ControlBox=.F.

            *--- Se non ho il ThemesManager devo utilizzare le vecchie bmp
            If !(Vartype(i_ThemesManager)=='O')
                .b1.Picture="help.bmp"
                .b2.Picture="modiftbl.bmp"
                .b2.DisabledPicture="modiftbd.bmp"
                .b3.Picture="addtabl.bmp"
                .b3.DisabledPicture="addtabld.bmp"
                .b4.Picture="save.bmp"
                .b4.DisabledPicture="saved.bmp"
                .b5.Picture="canc.bmp"
                .b5.DisabledPicture="cancd.bmp"
                .b5b.Picture="canc_riga.bmp"
                .b5b.DisabledPicture="canc_rigad.bmp"
                .b6.Picture="print.bmp"
                .b6.DisabledPicture="printd.bmp"
                .b7.Picture="prior.bmp"
                .b7.DisabledPicture="priord.bmp"
                .b8.Picture="next.bmp"
                .b8.DisabledPicture="nextd.bmp"
                .b9.Picture="zoom.bmp"
                .b9.DisabledPicture="zoomd.bmp"
                .b10.Picture="PgUp.bmp"
                .b10.DisabledPicture="PgUpd.bmp"
                .b11.Picture="PgDn.bmp"
                .b11.DisabledPicture="PgDnd.bmp"
                .b12.Picture="filter.bmp"
                .b12.DisabledPicture="filterd.bmp"
                .b13.Picture="esc.bmp"
                .b13.DisabledPicture="escd.bmp"
            Endif
            .Dock(0)
        Endwith
        m.oBtn = .Null.
    Endproc

    Procedure ChangeSettings()
        DoDefault()
        Local cChr, nPixel
        nPixel = 4
        With This
            If i_nTBTNH>16
                cChr = Chr(10)
                .Boxute.Width=40
                .boxazi.Width=45
                .boxdata.Width=55
                .Boxute.Value=MSG_TB_USER+cChr+Alltrim(Str(i_codute))
                .boxazi.Value=MSG_TB_COMPANY+cChr+Alltrim(i_codazi)
                .boxdata.Value=MSG_TB_DATE+cChr+Alltrim(Dtoc(i_datsys))
            Else
                cChr = ' '
                .Boxute.Value=MSG_TB_USER+cChr+Alltrim(Str(i_codute))
                .boxazi.Value=MSG_TB_COMPANY+cChr+Alltrim(i_codazi)
                .boxdata.Value=MSG_TB_DATE+cChr+Alltrim(Dtoc(i_datsys))
                .Boxute.Width=20+Fontmetric(6, .Boxute.FontName, .Boxute.FontSize)*Len(.Boxute.Value)
                .boxazi.Width=20+Fontmetric(6, .boxazi.FontName, .boxazi.FontSize)*Len(.boxazi.Value)
                .boxdata.Width=20+Fontmetric(6, .boxdata.FontName, .boxdata.FontSize)*Len(.boxdata.Value)
            Endif
            .Boxute.Height=i_nTBTNH+nPixel
            .boxazi.Height=i_nTBTNH+nPixel
            .boxdata.Height=i_nTBTNH+nPixel
        Endwith
    Endproc

    * --- Disabilita o abilita tutti i tasti
    Proc Enable
        Parameter m.Enable
        This.b2.Enabled = m.Enable
        This.b3.Enabled = m.Enable
        This.b4.Enabled = m.Enable
        This.b5.Enabled = m.Enable
        This.b5b.Enabled = m.Enable
        This.b6.Enabled = m.Enable
        This.b7.Enabled = m.Enable
        This.b8.Enabled = m.Enable
        This.b9.Enabled = m.Enable
        This.b10.Enabled = m.Enable
        This.b11.Enabled = m.Enable
        This.b12.Enabled = m.Enable
        This.b13.Enabled = m.Enable
    Endproc
    * --- Attivazione tasti Query
    Proc SetQuery(p_exist,p_print,p_zoom)
        This.b2.Enabled  = .T. && p_exist
        This.b3.Enabled  = .T.
        This.b4.Enabled  = .F.
        This.b5.Enabled  = .T. && p_exist
        This.b5b.Enabled  = .F. && p_exist
        This.b6.Enabled  = .T. && p_print
        This.b7.Enabled  = .T.
        This.b8.Enabled  = .T.
        This.b9.Enabled  = .T. && p_zoom
        This.b10.Enabled = .T.
        This.b11.Enabled = .T.
        This.b12.Enabled = .T.
        This.b13.Enabled = .T.
    Endproc
    * --- Attivazione tasti Edit
    Proc SetEdit()
        This.b2.Enabled=.F.
        This.b3.Enabled=.F.
        This.b4.Enabled=.T.
        This.b5.Enabled=.F.
        This.b5b.Enabled=.T.
        This.b6.Enabled=.F.
        This.b7.Enabled=.F.
        This.b8.Enabled=.F.
        This.b9.Enabled=.F.
        This.b10.Enabled=.T.
        This.b11.Enabled=.T.
        This.b12.Enabled=.F.
        This.b13.Enabled=.T.
    Endproc
    * --- Attivazione tasti Filtro
    Proc SetFilter()
        This.b2.Enabled=.F.
        This.b3.Enabled=.F.
        This.b4.Enabled=.T.
        This.b5.Enabled=.F.
        This.b6.Enabled=.F.
        This.b7.Enabled=.F.
        This.b8.Enabled=.F.
        This.b9.Enabled=.F.
        This.b10.Enabled=.T.
        This.b11.Enabled=.T.
        This.b12.Enabled=.F.
        This.b13.Enabled=.T.
    Endproc
    *--- Traduzione tooltip
    Procedure LocalizeString
        With This
            *--- Help Btn
            .b1.ToolTipText = cp_Translate(MSG_HELP)+" (F1)"
            *--- Edit Btn
            .b2.ToolTipText = cp_Translate(MSG_CHANGE)+" (F3)"
            *--- Load Btn
            .b3.ToolTipText = cp_Translate(MSG_LOAD)+" (F4)"
            *--- Save Btn
            .b4.ToolTipText = cp_Translate(MSG_SAVE)+" (F10)"
            *---Delete Btn
            .b5.ToolTipText = cp_Translate(MSG_DELETE)+" (F5)"
            *---Delete row Btn
            .b5b.ToolTipText = cp_Translate(MSG_DELETE_ROW)+" (F6)"
            *---Print Btn
            .b6.ToolTipText = cp_Translate(MSG_PRINT) +" (F2)"
            *---Prior Btn
            .b7.ToolTipText = cp_Translate(MGS_PREVIOUS_RECORD)+" (F7)"
            *---Next Btn
            .b8.ToolTipText = cp_Translate(MSG_NEXT_RECORD)+" (F8)"
            *---Zoom Btn
            .b9.ToolTipText = cp_Translate(MSG_ZOOM)+" (F9)"
            *---PgUp Btn
            .b10.ToolTipText = cp_Translate(MSG_PAGE_UP)+" (PgUp)"
            *---PgDn Btn
            .b11.ToolTipText = cp_Translate(MSG_PAGE_DOWN)+" (PgDn)"
            *---Filter Btn
            .b12.ToolTipText = cp_Translate(MSG_FILTER)+" (F12)"
            *---Filter Btn
            .b13.ToolTipText = cp_Translate(MSG_EXIT)+" (Esc)"
        Endwith
    Endproc
Enddefine

Define Class StdTableGroupCombo As StdCombo
    Dimension combovalues[1]
    nValues=0
    RowSourceType=0
    cTable='GROUPSROLE.VQR'
    cKey='code'
    cValue='name'
    cOrderBy='code'
    tablefilter=''
    xDefault=0
    bSetFont = .T.
    Proc Init()
        If Vartype(This.bNoBackColor)='U'
            This.BackColor=i_nEBackColor
        Endif
        This.ToolTipText=cp_Translate(This.ToolTipText)
        Local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
        Local i_fk,i_fd
        i_curs=Sys(2015)
        If Not Empty(This.cTable) And Lower(i_codazi)<>'xxx'
            vq_exec(This.cTable,.Null.,i_curs)
        Endif
        i_fk=This.cKey
        i_fd=This.cValue
        If Used(i_curs)
            Select (i_curs)
            This.nValues=Iif(Reccount()>1,Reccount()+1,1)
            For i_j=Alen(This.combovalues) To 1 Step -1
                Adel(This.combovalues,i_j)
                If This.ListCount>0 And i_j <= This.ListCount
                    This.RemoveItem(i_j)
                Endif
            Endfor
            Dimension This.combovalues[MAX(1,this.nValues)]
            i_bCharKey=Type(i_fk)='C'
            If This.nValues>1
                This.AddItem(' ')
                This.combovalues[1]=Iif(i_bCharKey,Space(1),0)
            Else
                i_GROUPROLE=Code
            Endif
            Do While !Eof()
                This.AddItem(Iif(Type(i_fd)='C',Alltrim(&i_fd),Alltrim(Str(&i_fd))))
                If i_bCharKey
                    This.combovalues[iif(this.nValues=1,0,1)+recno()]=Trim(&i_fk)
                Else
                    This.combovalues[iif(this.nValues=1,0,1)+recno()]=&i_fk
                Endif
                Skip
            Enddo
            This.SetRadio()
            This.Enabled=(This.nValues<>1)
            Select (i_curs)
            Use
        Else
            This.Enabled=.F.
        Endif
        If This.bSetFont
            This.SetFont()
        Endif
    Func RadioValue()
        If This.Value>0 And This.Value<=This.nValues
            Return(This.combovalues[this.value])
        Endif
        Return(This.xDefault)
    Func SetRadio()
        #If Version(5)>=900
            This.Value=Ascan(This.combovalues,i_GROUPROLE, -1, -1, -1, 6)
        #Else
            Local cSetExact
            cSetExact=Set("EXACT")
            Set Exact On
            This.Value=Ascan(This.combovalues,i_GROUPROLE)
            Set Exact &cSetExact
        #Endif
        Return
    Func GetRadio()
        Local i_n
        i_n=This.cFormvar
        i_GROUPROLE=This.RadioValue()
        Return(.T.)
        *
        * --- EVENTI/METODI VFP
        *
        * --- Quando prende il Focus
    Proc GotFocus()
        Local i_var
        Local i_oldvalue
        * --- il valore puo' essere cambiato dalla funzione di "Before"
        i_var=This.cFormvar
        If This.RadioValue()<>i_GROUPROLE
            i_GROUPROLE=This.Value
        Endif
        If Vartype(This.bNoBackColor)='U'
            This.BackColor = i_nBackColor
        Endif
        If Vartype(g_DispCnt)='N'
            This.DisplayCount = g_DispCnt
        Endif
        Return
    Proc LostFocus()
        If Vartype(This.bNoBackColor)='U'
            This.BackColor=i_nEBackColor
        Endif
        Return
    Func When()
        Local i_bRes
        * --- Procedure standard del campo
        i_bRes=.T.
        i_bRes = This.mCond()
        Return(i_bRes)
    Func Valid()
        i_GROUPROLE=This.Value
        cp_menu()
        cp_LoadTableSecArray()
        Return(.T.)
Enddefine
