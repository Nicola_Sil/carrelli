* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_infoctrl                                                     *
*              ProprietÓ controllo                                             *
*                                                                              *
*      Author: GiorGio Montali                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-06                                                      *
* Last revis.: 2008-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject
* --- Area Manuale = Header
* --- cp_infoctrl
cp_ReadXdc()
* --- Fine Area Manuale
Return(Createobject("tcp_infoctrl",oParentObject))

* --- Class definition
Define Class tcp_infoctrl As StdForm
    Top    = 71
    Left   = 134

    * --- Standard Properties
    Width  = 608
    Height = 200
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2008-11-20"
    HelpContextID=170563081
    max_rt_seq=8

    * --- Constant Properties
    _IDX = 0
    cPrg = "cp_infoctrl"
    cComment = "ProprietÓ controllo"
    oParentObject = .Null.
    Icon = "mask.ico"
    *closable = .f.

    * --- Local Variables
    w_CONTROL = Space(20)
    w_HELP = Space(100)
    w_TIPO = Space(20)
    w_GESTIONE = Space(20)
    w_LOGTAB = Space(30)
    w_FISTAB = Space(20)
    w_LENGTH = Space(5)
    w_cMenuFile = Space(30)
    * --- Area Manuale = Declare Variables
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=1, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tcp_infoctrlPag1","cp_infoctrl",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate("Pag.1")
            .Pages(1).oPag.oContained=Thisform
        Endwith
        This.Parent.oFirstControl = This.Page1.oPag.oCONTROL_1_1
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
        * --- Area Manuale = Init Page Frame
        * --- cp_infoctrl
        * --- Translate interface...
        This.Parent.cComment=cp_Translate(MSG_CTLR_PROPERTY)
        * --- Fixed dimension
        This.Parent.MaxWidth  = This.Parent.Width
        This.Parent.MaxHeight = This.Parent.Height
        This.Parent.AutoCenter=.T.
        * --- Fine Area Manuale
    Endproc
    Proc Init(oParentObject)
        If Vartype(m.oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        DoDefault()

        * --- Open tables
    Function OpenWorkTables()
        Return(This.OpenAllTables(0))

    Procedure SetPostItConn()
        Return


        * --- Read record and initialize Form variables
    Procedure LoadRec()
    Endproc

    * --- ecpQuery
    Procedure ecpQuery()
        This.BlankRec()
        This.cFunction="Edit"
        This.SetStatus()
        If This.cFunction="Edit"
            This.mEnableControls()
        Endif
    Endproc
    * --- Esc
    Proc ecpQuit()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            This.cFunction = "Edit"
            This.oFirstControl.SetFocus()
            Return
        Endif
        * ---
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Endproc
    Proc QueryUnload()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            Nodefault
            Return
        Endif
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Proc ecpSave()
        If This.TerminateEdit() And This.CheckForm()
            This.LockScreen=.T.
            This.mReplace(.T.)
            This.LockScreen=.F.
            This.NotifyEvent("Done")
            This.Release()
        Endif
    Endproc
    Func OkToQuit()
        Return .T.
    Endfunc

    * --- Blank Form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        With This
            .w_CONTROL=Space(20)
            .w_HELP=Space(100)
            .w_TIPO=Space(20)
            .w_GESTIONE=Space(20)
            .w_LOGTAB=Space(30)
            .w_FISTAB=Space(20)
            .w_LENGTH=Space(5)
            .w_cMenuFile=Space(30)
            .w_CONTROL = Alltrim(Substr(This.oParentObject.oCtrl.cFormVar,3))
            .w_HELP = Alltrim(This.oParentObject.oCtrl.ToolTipText)
            .w_TIPO = getType(Iif(Inlist(Upper(This.oParentObject.oCtrl.Class), 'STDCOMBO','STDCHECK','STDRADIO','STDTABLECOMBO','STDTRSCOMBO','STDTRSCHECK','STDTRSRADIO','STDTRSTABLECOMBO') , This.oParentObject.oCtrl.RadioValue(), This.oParentObject.oCtrl.Value))
            .w_GESTIONE = Alltrim(This.oParentObject.oContained.cPrg)
            .w_LOGTAB = i_dcx.GetTableDescr(i_dcx.GetTableIdx(Upper(Alltrim(This.oParentObject.oCtrl.cLinkFile))))
            .w_FISTAB = Alltrim(This.oParentObject.oCtrl.cLinkFile)
            .w_LENGTH = getLength(Iif(Inlist(Upper(This.oParentObject.oCtrl.Class), 'STDCOMBO','STDCHECK','STDRADIO','STDTABLECOMBO','STDTRSCOMBO','STDTRSCHECK','STDTRSRADIO','STDTRSTABLECOMBO') , This.oParentObject.oCtrl.RadioValue(),This.oParentObject.oCtrl.Value))
            .w_cMenuFile = .oParentObject.cMenuFile
            .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_NAME+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_CTLR_NAME)
            .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_HELP+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_DESCRIPTION)
            .oPgFrm.Page1.oPag.oObj_1_28.Calculate(MSG_TYPE+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_29.Calculate(MSG_CTLR_TYPE)
            .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_LENGTH+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_LENGTH)
            .oPgFrm.Page1.oPag.oObj_1_32.Calculate(MSG_ENTITY+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_33.Calculate(MSG_PRG_NAME)
            .oPgFrm.Page1.oPag.oObj_1_34.Calculate(MSG_CONTEXT_MENU+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_35.Calculate(MSG_SHOW_CONTEXT_MENU_NAME)
            .oPgFrm.Page1.oPag.oObj_1_36.Calculate(MSG_TABLE_NAME)
            .oPgFrm.Page1.oPag.oObj_1_37.Calculate(MSG_LOGICAL+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_38.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
            .oPgFrm.Page1.oPag.oObj_1_39.Calculate(MSG_PHYSICAL+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_40.Calculate(MSG_SHOW_PHYSICAL_TABLENAME)
        Endwith
        This.SaveDependsOn()
        This.SetControlsValue()
        This.oPgFrm.Page1.oPag.oBtn_1_21.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
        This.mHideControls()
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = Setting operation
    *     Allowed Parameters: Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        * --- Deactivating List page when <> da Query
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt
        i_cFlt =""
        Return (i_cFlt)
    Endfunc

    Proc QueryKeySet(i_cWhere,i_cOrderBy)
    Endproc

    Proc SelectCursor
    Proc GetXKey

        * --- Update Database
    Function mReplace(i_bEditing)
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        This.NotifyEvent('Update start')
        With This
        Endwith
        This.NotifyEvent('Update end')
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(.T.)

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        If i_bUpd
            With This
                .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_NAME+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_CTLR_NAME)
                .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_HELP+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_DESCRIPTION)
                .oPgFrm.Page1.oPag.oObj_1_28.Calculate(MSG_TYPE+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_29.Calculate(MSG_CTLR_TYPE)
                .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_LENGTH+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_LENGTH)
                .oPgFrm.Page1.oPag.oObj_1_32.Calculate(MSG_ENTITY+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_33.Calculate(MSG_PRG_NAME)
                .oPgFrm.Page1.oPag.oObj_1_34.Calculate(MSG_CONTEXT_MENU+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_35.Calculate(MSG_SHOW_CONTEXT_MENU_NAME)
                .oPgFrm.Page1.oPag.oObj_1_36.Calculate(MSG_TABLE_NAME)
                .oPgFrm.Page1.oPag.oObj_1_37.Calculate(MSG_LOGICAL+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_38.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
                .oPgFrm.Page1.oPag.oObj_1_39.Calculate(MSG_PHYSICAL+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_40.Calculate(MSG_SHOW_PHYSICAL_TABLENAME)
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
            Endwith
            This.DoRTCalc(1,8,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
            .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_NAME+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_CTLR_NAME)
            .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_HELP+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_DESCRIPTION)
            .oPgFrm.Page1.oPag.oObj_1_28.Calculate(MSG_TYPE+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_29.Calculate(MSG_CTLR_TYPE)
            .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_LENGTH+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_LENGTH)
            .oPgFrm.Page1.oPag.oObj_1_32.Calculate(MSG_ENTITY+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_33.Calculate(MSG_PRG_NAME)
            .oPgFrm.Page1.oPag.oObj_1_34.Calculate(MSG_CONTEXT_MENU+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_35.Calculate(MSG_SHOW_CONTEXT_MENU_NAME)
            .oPgFrm.Page1.oPag.oObj_1_36.Calculate(MSG_TABLE_NAME)
            .oPgFrm.Page1.oPag.oObj_1_37.Calculate(MSG_LOGICAL+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_38.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
            .oPgFrm.Page1.oPag.oObj_1_39.Calculate(MSG_PHYSICAL+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_40.Calculate(MSG_SHOW_PHYSICAL_TABLENAME)
        Endwith
        Return


        * --- Enable controls under condition
    Procedure mEnableControls()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        DoDefault()
        Return

        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
        Endwith
        * --- Area Manuale = Notify Event End
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

    Function SetControlsValue()
        If Not(This.oPgFrm.Page1.oPag.oCONTROL_1_1.Value==This.w_CONTROL)
            This.oPgFrm.Page1.oPag.oCONTROL_1_1.Value=This.w_CONTROL
        Endif
        If Not(This.oPgFrm.Page1.oPag.oHELP_1_2.Value==This.w_HELP)
            This.oPgFrm.Page1.oPag.oHELP_1_2.Value=This.w_HELP
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTIPO_1_3.Value==This.w_TIPO)
            This.oPgFrm.Page1.oPag.oTIPO_1_3.Value=This.w_TIPO
        Endif
        If Not(This.oPgFrm.Page1.oPag.oGESTIONE_1_4.Value==This.w_GESTIONE)
            This.oPgFrm.Page1.oPag.oGESTIONE_1_4.Value=This.w_GESTIONE
        Endif
        If Not(This.oPgFrm.Page1.oPag.oLOGTAB_1_5.Value==This.w_LOGTAB)
            This.oPgFrm.Page1.oPag.oLOGTAB_1_5.Value=This.w_LOGTAB
        Endif
        If Not(This.oPgFrm.Page1.oPag.oFISTAB_1_6.Value==This.w_FISTAB)
            This.oPgFrm.Page1.oPag.oFISTAB_1_6.Value=This.w_FISTAB
        Endif
        If Not(This.oPgFrm.Page1.oPag.oLENGTH_1_7.Value==This.w_LENGTH)
            This.oPgFrm.Page1.oPag.oLENGTH_1_7.Value=This.w_LENGTH
        Endif
        If Not(This.oPgFrm.Page1.oPag.ocMenuFile_1_17.Value==This.w_cMenuFile)
            This.oPgFrm.Page1.oPag.ocMenuFile_1_17.Value=This.w_cMenuFile
        Endif
        cp_SetControlsValueExtFlds(This,'')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
            Else
                If Not(i_bnoChk)
                    cp_ErrorMsg(i_cErrorMsg)
                Endif
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

Enddefine

* --- Define pages as container
Define Class tcp_infoctrlPag1 As StdContainer
    Width  = 604
    Height = 200
    stdWidth  = 604
    stdheight = 200
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.

    Add Object oCONTROL_1_1 As StdField With uid="LBCSHXROLU",rtseq=1,rtrep=.F.,;
        cFormVar = "w_CONTROL", cQueryName = "CONTROL",;
        bObbl = .F. , nPag = 1, Value=Space(20), bMultilanguage =  .F.,;
        ToolTipText = "Nome controllo",;
        HelpContextID = 151956281,;
        bGlobalFont=.T.,;
        Height=21, Width=153, Left=118, Top=13, InputMask=Replicate('X',20), ReadOnly=.T.

    Add Object oHELP_1_2 As StdField With uid="ZDCAJAZYII",rtseq=2,rtrep=.F.,;
        cFormVar = "w_HELP", cQueryName = "HELP",;
        bObbl = .F. , nPag = 1, Value=Space(100), bMultilanguage =  .F.,;
        ToolTipText = "Descrizione",;
        HelpContextID = 160212489,;
        bGlobalFont=.T.,;
        Height=21, Width=205, Left=388, Top=13, InputMask=Replicate('X',100), ReadOnly=.T.

    Add Object oTIPO_1_3 As StdField With uid="TGRGFMTVCD",rtseq=3,rtrep=.F.,;
        cFormVar = "w_TIPO", cQueryName = "TIPO",;
        bObbl = .F. , nPag = 1, Value=Space(20), bMultilanguage =  .F.,;
        ToolTipText = "Tipo contenuto",;
        HelpContextID = 88122889,;
        bGlobalFont=.T.,;
        Height=21, Width=153, Left=118, Top=44, InputMask=Replicate('X',20), ReadOnly=.T.

    Add Object oGESTIONE_1_4 As StdField With uid="QPLSALJFVF",rtseq=4,rtrep=.F.,;
        cFormVar = "w_GESTIONE", cQueryName = "GESTIONE",;
        bObbl = .F. , nPag = 1, Value=Space(20), bMultilanguage =  .F.,;
        ToolTipText = "Nome del programma che contiene il controllo",;
        HelpContextID = 225901872,;
        bGlobalFont=.T.,;
        Height=21, Width=153, Left=118, Top=76, InputMask=Replicate('X',20), ReadOnly=.T.

    Add Object oLOGTAB_1_5 As StdField With uid="XPACORHWJU",rtseq=5,rtrep=.F.,;
        cFormVar = "w_LOGTAB", cQueryName = "LOGTAB",;
        bObbl = .F. , nPag = 1, Value=Space(30), bMultilanguage =  .F.,;
        ToolTipText = "Visualizza il nome logico della tabella",;
        HelpContextID = 35085912,;
        bGlobalFont=.T.,;
        Height=21, Width=159, Left=119, Top=128, InputMask=Replicate('X',30), ReadOnly=.T.

    Add Object oFISTAB_1_6 As StdField With uid="TZPDAHCDVK",rtseq=6,rtrep=.F.,;
        cFormVar = "w_FISTAB", cQueryName = "FISTAB",;
        bObbl = .F. , nPag = 1, Value=Space(20), bMultilanguage =  .F.,;
        ToolTipText = "Visualizza il nome fisico della tabella",;
        HelpContextID = 229727832,;
        bGlobalFont=.T.,;
        Height=21, Width=159, Left=413, Top=128, InputMask=Replicate('X',20), ReadOnly=.T.

    Add Object oLENGTH_1_7 As StdField With uid="BSUWDFMBIZ",rtseq=7,rtrep=.F.,;
        cFormVar = "w_LENGTH", cQueryName = "LENGTH",;
        bObbl = .F. , nPag = 1, Value=Space(5), bMultilanguage =  .F.,;
        ToolTipText = "Lunghezza",;
        HelpContextID = 142040779,;
        bGlobalFont=.T.,;
        Height=21, Width=75, Left=517, Top=44, InputMask=Replicate('X',5), ReadOnly=.T.

    Add Object ocMenuFile_1_17 As StdField With uid="YLUBTYZYIL",rtseq=8,rtrep=.F.,;
        cFormVar = "w_cMenuFile", cQueryName = "cMenuFile",;
        bObbl = .F. , nPag = 1, Value=Space(30), bMultilanguage =  .F.,;
        ToolTipText = "Visualizza il nome del menu contestuale associato",;
        HelpContextID = 260405300,;
        bGlobalFont=.T.,;
        Height=21, Width=149, Left=443, Top=77, InputMask=Replicate('X',30), ReadOnly=.T.


    Add Object oObj_1_19 As cp_setCtrlObjprop With uid="WTFHXDANKA",Left=144, Top=207, Width=168,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Nome:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.



    Add Object oObj_1_20 As cp_setobjprop With uid="SKPUZOAYUU",Left=417, Top=207, Width=168,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CONTROL",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.



    Add Object oBtn_1_21 As StdButton With uid="FZOOQYPNID",Left=536, Top=171, Width=50,Height=20,;
        caption="Ok", nPag=1;
        , HelpContextID = 52798985;
        , Caption=MSG_OK_BUTTON;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_21.Click()
        =cp_StandardFunction(This,"Quit")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oObj_1_22 As cp_setCtrlObjprop With uid="ERPBVLZROL",Left=144, Top=233, Width=168,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Guida in linea:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.



    Add Object oObj_1_23 As cp_setobjprop With uid="UWNIRGQRUQ",Left=417, Top=233, Width=168,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_HELP",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.



    Add Object oObj_1_28 As cp_setCtrlObjprop With uid="MQMSEQTTWN",Left=144, Top=259, Width=168,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Tipo:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.



    Add Object oObj_1_29 As cp_setobjprop With uid="YLJCDSBJVP",Left=417, Top=259, Width=168,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_TIPO",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.



    Add Object oObj_1_30 As cp_setCtrlObjprop With uid="HTWFCECFKC",Left=144, Top=285, Width=168,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Lunghezza:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.



    Add Object oObj_1_31 As cp_setobjprop With uid="OVNWHCVKIW",Left=417, Top=285, Width=168,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_LENGHT",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.



    Add Object oObj_1_32 As cp_setCtrlObjprop With uid="WSSTXAONIK",Left=144, Top=311, Width=168,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Gestione:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.



    Add Object oObj_1_33 As cp_setobjprop With uid="RBBKDJKLLT",Left=417, Top=311, Width=168,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_GESTIONE",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.



    Add Object oObj_1_34 As cp_setCtrlObjprop With uid="SKMQWAALBO",Left=144, Top=337, Width=168,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Menu contestuale:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.



    Add Object oObj_1_35 As cp_setobjprop With uid="ORWTLCWGZE",Left=417, Top=337, Width=168,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_cMenuFile",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.



    Add Object oObj_1_36 As cp_setCtrlObjprop With uid="HYQKHWFMMN",Left=-52, Top=347, Width=168,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Nome Tabella",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.



    Add Object oObj_1_37 As cp_setCtrlObjprop With uid="BSNCVUWGAS",Left=144, Top=363, Width=168,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Logico:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.



    Add Object oObj_1_38 As cp_setobjprop With uid="RVKFDLBTMV",Left=417, Top=363, Width=168,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_logtab",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.



    Add Object oObj_1_39 As cp_setCtrlObjprop With uid="OAFQHIXFQX",Left=144, Top=389, Width=168,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Fisico:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.



    Add Object oObj_1_40 As cp_setobjprop With uid="KLOCHIONDT",Left=417, Top=389, Width=168,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_fistab",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 105537946;
        , bGlobalFont=.T.


    Add Object oStr_1_8 As StdString With uid="APJVQEIHTH",Visible=.T., Left=64, Top=13,;
        Alignment=1, Width=50, Height=18,;
        Caption="Nome:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_9 As StdString With uid="JTTAJLASHK",Visible=.T., Left=26, Top=44,;
        Alignment=1, Width=88, Height=18,;
        Caption="Tipo:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_10 As StdString With uid="EUNOUSSAOP",Visible=.T., Left=408, Top=44,;
        Alignment=1, Width=107, Height=18,;
        Caption="Lunghezza:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_11 As StdString With uid="ICKGCHGJEG",Visible=.T., Left=15, Top=76,;
        Alignment=1, Width=99, Height=18,;
        Caption="Gestione:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_12 As StdString With uid="DHVXOCQNLS",Visible=.T., Left=72, Top=105,;
        Alignment=0, Width=132, Height=18,;
        Caption="Nome Tabella"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_14 As StdString With uid="OIBSMWNTWV",Visible=.T., Left=11, Top=128,;
        Alignment=1, Width=103, Height=18,;
        Caption="Logico:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_15 As StdString With uid="AAZOZHRFZA",Visible=.T., Left=292, Top=128,;
        Alignment=1, Width=119, Height=18,;
        Caption="Fisico:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_16 As StdString With uid="QGDWNXVMBZ",Visible=.T., Left=308, Top=13,;
        Alignment=1, Width=79, Height=18,;
        Caption="Guida in linea:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_18 As StdString With uid="FFBNAJVRKR",Visible=.T., Left=285, Top=77,;
        Alignment=1, Width=156, Height=18,;
        Caption="Menu contestuale:"  ;
        , bGlobalFont=.T.

    Add Object oBox_1_13 As StdBox With uid="GGOOVTWZJH",Left=10, Top=113, Width=53,Height=2

    Add Object oBox_1_24 As StdBox With uid="RLLRDGUCLR",Left=9, Top=114, Width=2,Height=46

    Add Object oBox_1_25 As StdBox With uid="SWPZABPGCR",Left=586, Top=115, Width=2,Height=46

    Add Object oBox_1_26 As StdBox With uid="KVVXQRBPRW",Left=163, Top=115, Width=423,Height=1

    Add Object oBox_1_27 As StdBox With uid="QYMUVVOCEP",Left=9, Top=159, Width=578,Height=2
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_infoctrl','','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Dialog Window"
Endproc

* --- Area Manuale = Functions & Procedures
* --- cp_infoctrl
Proc getType(valore)
    Do Case
        Case Vartype(valore)=='C'
            Return(cp_Translate(MSG_CHARACTER))
        Case Vartype(valore)=='N'
            Return(cp_Translate(MSG_NUMERIC))
        Case Vartype(valore)=='D'
            Return(cp_Translate(MSG_DATE))
        Case Vartype(valore)=='T'
            Return(cp_Translate(MSG_DATETIME))
        Case Vartype(valore)=='L'
            Return(cp_Translate(MSG_LOGICAL))
        Case Vartype(valore)=='M'
            Return(cp_Translate(MSG_MEMO))
        Case Vartype(valore)=='O'
            Return(cp_Translate(MSG_OBJECT))
        Otherwise
            Return(cp_Translate(MSG_UNKNOWN))
    Endcase
Endproc

Proc getLength(valore)
    Do Case
        Case Vartype(valore)=='C'
            Return(Alltrim(Str(Len(valore))))
        Case Vartype(valore)=='N'
            Return('N.D.')
        Case Vartype(valore)=='D'
            Return('8')
        Case Vartype(valore)=='T'
            Return('14')
        Case Vartype(valore)=='L'
            Return('1')
        Case Vartype(valore)=='M'
            Return('10')
        Case Vartype(valore)=='O'
            Return('N.D.')
        Otherwise
            Return('N.D.')
    Endcase
    * --- Fine Area Manuale
