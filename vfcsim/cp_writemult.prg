* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_writemult                                                    *
*              Write fields multilanguage                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-31                                                      *
* Last revis.: 2008-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
Private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
Createobject("tcp_writemult",oParentObject)
Return(i_retval)

Define Class tcp_writemult As StdBatch
    * --- Local variables
    w_idxlang = 0
    w_NT = 0
    w_nConn = 0
    w_cTable = Space(100)
    w_cUpd = Space(200)
    w_nRes = 0
    w_GESTDEF = .Null.
    * --- WorkFile variables
    runtime_filters = 1

    Procedure Pag1
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        * --- scrivo nella tabella i valori dei campi multilingua
        This.w_idxlang = Alen(i_aCpLangs)+1
        This.w_NT = cp_TablePropScan(This.oParentObject.w_NameTable)
        If This.w_NT<>0
            This.w_GESTDEF = This.oParentObject.oParentObject.Parent.ocontained
            This.w_nConn = i_TableProp[this.w_NT,3]
            This.w_cTable = cp_SetAzi(i_TableProp[this.w_NT,2])
            This.w_cUpd = ""
            For i=1 To This.w_idxlang
                If i<>1
                    This.w_cUpd = This.w_cUpd+","
                Endif
                This.w_cUpd = This.w_cUpd+Alltrim(This.oParentObject.w_VARIABLE)
                If i<>1
                    This.w_cUpd = This.w_cUpd+"_"+Trim(i_aCpLangs(i-1))
                Endif
                If Empty( This.oParentObject.Ctrls[i*2].Value )
                    This.w_cUpd = This.w_cUpd+"=NULL"
                Else
                    This.w_cUpd = This.w_cUpd+"="+cp_ToStrODBC( This.oParentObject.Ctrls[i*2].Value )
                Endif
            Endfor
            * --- Try
            Local bErr_00EEE030
            bErr_00EEE030=bTrsErr
            This.Try_00EEE030()
            * --- Catch
            If !Empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- rollback
                bTrsErr=.T.
                cp_EndTrs(.T.)
                * --- accept error
                bTrsErr=.F.
            Endif
            bTrsErr=bTrsErr Or bErr_00EEE030
            * --- End
        Endif
        This.oParentObject.ecpQuit()
    Endproc
    Proc Try_00EEE030()
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
        * --- begin transaction
        cp_BeginTrs()
        Local i_ccchkf, i_ccchkw
        i_ccchkf=""
        i_ccchkw=""
        This.w_GESTDEF.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,.T.,This.w_NT,This.w_nConn,("Detail"$cp_getEntityType(This.w_GESTDEF.cprg) And This.oParentObject.oParentObject.nPag=2))
        This.w_nRes = cp_TrsSQL(This.w_nConn,"UPDATE "+This.w_cTable+" SET "+This.w_cUpd+i_ccchkf+" where "+This.oParentObject.w_cWhere+i_ccchkw)
        * --- commit
        cp_EndTrs(.T.)
        If "Detail"$cp_getEntityType(This.w_GESTDEF.cprg) And This.oParentObject.oParentObject.nPag=2
            This.w_GESTDEF.SetCCCHKCursor(i_ccchkf,This.w_NT)
        Else
            This.w_GESTDEF.loadRecWarn()
            This.w_GESTDEF.Refresh()
        Endif
        Return


    Function OpenTables()
        Dimension This.cWorkTables[max(1,0)]
        Return(This.OpenAllTables(0))

        * --- Area Manuale = Functions & Procedures
        * --- Fine Area Manuale
Enddefine

Procedure getEntityType(result)
    result="Routine"
Endproc
Procedure getEntityParmList(result)
    result=""
Endproc
