* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_runrep                                                       *
*              Esegue report                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-10-07                                                      *
* Last revis.: 2011-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject,i_nomerep,i_TipoRep,i_NomeFil,i_NroCopie
* --- Area Manuale = Header
* --- Fine Area Manuale
Private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
Createobject("tcp_runrep",oParentObject,m.i_nomerep,m.i_TipoRep,m.i_NomeFil,m.i_NroCopie)
Return(i_retval)

Define Class tcp_runrep As StdBatch
    * --- Local variables
    i_nomerep = Space(10)
    i_TipoRep = Space(10)
    i_NomeFil = Space(10)
    i_NroCopie = 0
    i_cCurrDir = 0
    * --- WorkFile variables
    runtime_filters = 2

    Procedure Pag1
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        * --- Parametri: i_NomeRep = nome del Report da Stampare (FRX)
        *                 i_TipoRep = Tipo Scelta stampa effettuata
        *                            'V' = Video
        *                             'S' = Stampa
        *                             'F' = File
        *                 i_NomeFil = Eventuale Nome Del File di Output
        This.i_cCurrDir = Sys(5)+Sys(2003)
        L_MsgError = ""
        i_olderr=On("ERROR")
        On Error L_MsgError = Message()
        Select "__tmp__"
        If Reccount()<>0

            Local L_NomeRep
            L_NomeRep = This.i_nomerep
            Set Console Off
            Do Case
                Case This.i_TipoRep = "V"
                    * --- Anteprima
                    Push Key Clear
                    i_curform=.Null.
                    Push Menu _Msysmenu
                    Set Sysmenu To
                    Report Form (L_NomeRep) Preview
                    Pop Menu _Msysmenu
                    Pop Key
                Case This.i_TipoRep = "S"
                    * --- Stampa
                    Do While  This.i_NroCopie>0
                        Report Form (L_NomeRep) Noconsole To Printer
                        This.i_NroCopie = This.i_NroCopie - 1
                    Enddo
                Case This.i_TipoRep = "O"
                    * --- Stampa con Opzioni
                    Report Form (L_NomeRep) Noconsole To Printer Prompt
                Case This.i_TipoRep = "F"
                    * --- File
                    Report Form (L_NomeRep) Noconsole To File (This.i_NomeFil) Ascii
            Endcase
            On Error &i_olderr
            If !Empty(L_MsgError)
                cp_Msg("Errors executing report:"+Chr(13)+Alltrim(L_MsgError), .F.)
            Endif
            Cd (This.i_cCurrDir)
        Else
            cp_Msg(cp_MsgFormat("Non ci sono dati da stampare !"))
        Endif
        i_retcode = 'stop'
        Return
    Endproc


    Proc Init(oParentObject,i_nomerep,i_TipoRep,i_NomeFil,i_NroCopie)
        This.i_nomerep=i_nomerep
        This.i_TipoRep=i_TipoRep
        This.i_NomeFil=i_NomeFil
        This.i_NroCopie=i_NroCopie
        DoDefault(oParentObject)
        Return
    Function OpenTables()
        Dimension This.cWorkTables[max(1,0)]
        Return(This.OpenAllTables(0))

        * --- Area Manuale = Functions & Procedures
        * --- Fine Area Manuale
Enddefine

Procedure getEntityType(result)
    result="Routine"
Endproc
Procedure getEntityParmList(result)
    result="i_nomerep,i_TipoRep,i_NomeFil,i_NroCopie"
Endproc
