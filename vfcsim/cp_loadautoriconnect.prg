* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_loadautoriconnect                                            *
*              Auto riconnect                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-11-02                                                      *
* Last revis.: 2011-05-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
Private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
Createobject("tcp_loadautoriconnect",oParentObject)
Return(i_retval)

Define Class tcp_loadautoriconnect As StdBatch
    * --- Local variables
    w_RCDEADRQ = Space(1)
    w_RCNUMDRQ = 0
    w_RCDELAYD = 0
    w_RCAUTORC = Space(1)
    w_RCNUMRIC = 0
    w_RCDELAYR = 0
    w_RCCNTIME = 0
    w_RCCONMSG = Space(1)
    * --- WorkFile variables
    paramric_idx=0
    runtime_filters = 2

    Procedure Pag1
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        * --- Riconnessione automatica
        * --- Read from paramric
        i_nOldArea=Select()
        If Used('_read_')
            Select _read_
            Use
        Endif
        i_nConn=i_TableProp[this.paramric_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.paramric_idx,2])
        If i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
                "rcautorc,rcnumric,rcdelayr,rccntime,rcdeadrq,rcnumdrq,rcdelayd,rcconmsg"+;
                " from "+i_cTable+" paramric where ";
                +"rcserial = "+cp_ToStrODBC("00001");
                ,"_read_")
            i_Rows=Iif(Used('_read_'),Reccount(),0)
        Else
            Select;
                rcautorc,rcnumric,rcdelayr,rccntime,rcdeadrq,rcnumdrq,rcdelayd,rcconmsg;
                from (i_cTable) Where;
                rcserial = "00001";
                into Cursor _read_
            i_Rows=_Tally
        Endif
        If Used('_read_')
            Locate For 1=1
            This.w_RCAUTORC = Nvl(cp_ToDate(_read_.rcautorc),cp_NullValue(_read_.rcautorc))
            This.w_RCNUMRIC = Nvl(cp_ToDate(_read_.rcnumric),cp_NullValue(_read_.rcnumric))
            This.w_RCDELAYR = Nvl(cp_ToDate(_read_.rcdelayr),cp_NullValue(_read_.rcdelayr))
            This.w_RCCNTIME = Nvl(cp_ToDate(_read_.rccntime),cp_NullValue(_read_.rccntime))
            This.w_RCDEADRQ = Nvl(cp_ToDate(_read_.rcdeadrq),cp_NullValue(_read_.rcdeadrq))
            This.w_RCNUMDRQ = Nvl(cp_ToDate(_read_.rcnumdrq),cp_NullValue(_read_.rcnumdrq))
            This.w_RCDELAYD = Nvl(cp_ToDate(_read_.rcdelayd),cp_NullValue(_read_.rcdelayd))
            This.w_RCCONMSG = Nvl(cp_ToDate(_read_.rcconmsg),cp_NullValue(_read_.rcconmsg))
            Use
        Else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            Return
        Endif
        Select (i_nOldArea)
        Public i_DEADREQUERY , i_NUMDEADREQUERY, i_DELAYDEADREQUERY, i_CONNECTMSG, i_AUTORICONNECT, i_CONNECTLOGIN
        Public i_NUMRICONNECT, i_NUMRICONNECT, i_DELAYRICONNECT, i_ConnectTimeOutEndProc

        i_DEADREQUERY = (This.w_RCDEADRQ="S")
        i_NUMDEADREQUERY = This.w_RCNUMDRQ
        i_DELAYDEADREQUERY = This.w_RCDELAYD
        i_AUTORICONNECT = (This.w_RCAUTORC = "S")
        i_NUMRICONNECT = This.w_RCNUMRIC
        i_DELAYRICONNECT = This.w_RCDELAYR
        i_ConnectTimeOut = This.w_RCCNTIME
        i_CONNECTMSG = This.w_RCCONMSG
        i_CONNECTLOGIN = ""
    Endproc


    Function OpenTables()
        Dimension This.cWorkTables[max(1,1)]
        This.cWorkTables[1]='paramric'
        Return(This.OpenAllTables(1))

        * --- Area Manuale = Functions & Procedures
        * --- Fine Area Manuale
Enddefine

Procedure getEntityType(result)
    result="Routine"
Endproc
Procedure getEntityParmList(result)
    result=""
Endproc
