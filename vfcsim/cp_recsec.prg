* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_recsec                                                       *
*              Configurazione accessi record                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-06-18                                                      *
* Last revis.: 2008-10-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- cp_recsec
*Questa gestione la pu� usare solo l'amministratore
If Not cp_IsAdministrator(.T.) Or Not i_bSecurityRecord
    cp_ERRORMSG(MSG_ACCESS_DENIED,'stop',MSG_SECURITY_ADMIN)
    Return Null
Endif

=Cp_ReadXdc()
* --- Fine Area Manuale
Return(Createobject("tcp_recsec"))

* --- Class definition
Define Class tcp_recsec As StdTrsForm
    Top    = 12
    Left   = 14

    * --- Standard Properties
    Width  = 753
    Height = 354+35
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2008-10-07"
    HelpContextID=107182394
    max_rt_seq=9

    * --- Master/Detail Properties
    cTrsName=''

    * --- Constant Properties
    cprecsec_IDX = 0
    cprecsecd_IDX = 0
    cpazi_IDX = 0
    cFile = "cprecsec"
    cFileDetail = "cprecsecd"
    cKeySelect = "tablename,progname"
    cKeyWhere  = "tablename=this.w_tablename and progname=this.w_progname"
    cKeyDetail  = "tablename=this.w_tablename and progname=this.w_progname"
    cKeyWhereODBC = '"tablename="+cp_ToStrODBC(this.w_tablename)';
        +'+" and progname="+cp_ToStrODBC(this.w_progname)';

    cKeyDetailWhereODBC = '"tablename="+cp_ToStrODBC(this.w_tablename)';
        +'+" and progname="+cp_ToStrODBC(this.w_progname)';
        +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
    cKeyWhereODBCqualified = '"cprecsecd.tablename="+cp_ToStrODBC(this.w_tablename)';
        +'+" and cprecsecd.progname="+cp_ToStrODBC(this.w_progname)';

    cOrderByDett = ''
    cOrderByDettCustom = ''
    bApplyOrderDetail=.T.
    bApplyOrderDetailCustom=.T.
    cOrderByDett = 'cprecsecd.cproword,cprecsecd.descri'
    cPrg = "cp_recsec"
    cComment = "Configurazione accessi record"
    i_nRowNum = 0
    w_CPROWNUM = 0
    i_nRowPerPage = 5
    Icon = "movi.ico"
    i_lastcheckrow = 0
    * --- Area Manuale = Properties
    * --- Fine Area Manuale

    * --- Local Variables
    w_tablename = Space(50)
    o_tablename = Space(50)
    w_progname = Space(50)
    w_TBCOMMENT = Space(60)
    w_checkact = Space(1)
    w_cproword = 0
    w_queryfilter = Space(254)
    w_descri = Space(150)
    w_codazi = Space(5)
    w_AZRAGAZI = Space(40)

    * --- Children pointers
    cp_recsed = .Null.
    * --- Area Manuale = Declare Variables
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPageFrame With PageCount=2, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        cp_AddExtFldsTab(This,'cprecsec','cp_recsec')
        StdPageFrame::Init()
        *set procedure to cp_recsed additive
        With This
            .Pages(1).AddObject("oPag","tcp_recsecPag1","cp_recsec",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate("Generale")
            .Pages(1).HelpContextID = 39922290
        Endwith
        This.Parent.oFirstControl = This.Page1.oPag.otablename_1_1
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
        Local qf
        QueryFilter(@qf)
        This.Parent.cQueryFilter=qf
        *release procedure cp_recsed
        * --- Area Manuale = Init Page Frame
        * --- cp_recsec
        * --- Translate interface...
        This.Parent.cComment=cp_Translate(MSG_ACCESS_FILTER_RULE)
        This.Pages(1).Caption=cp_Translate(MSG_GENERAL)
        * --- Fine Area Manuale
        This.Parent.Resize()
    Endproc
    Proc Init(oParentObject)
        If Vartype(m.oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        DoDefault()

        * --- Open tables
    Function OpenWorkTables
        Dimension This.cWorkTables[3]
        This.cWorkTables[1]='cpazi'
        This.cWorkTables[2]='cprecsec'
        This.cWorkTables[3]='cprecsecd'
        * --- Area Manuale = Open Work Table
        * --- Fine Area Manuale
        Return(This.OpenAllTables(3))

    Procedure SetPostItConn()
        This.bPostIt=i_ServerConn[i_TableProp[this.cprecsec_IDX,5],7]
        This.nPostItConn=i_TableProp[this.cprecsec_IDX,3]
        Return

    Function CreateChildren()
        This.cp_recsed = Createobject('stdDynamicChild',This,'cp_recsed',This.oPgFrm.Page1.oPag.oLinkPC_2_6)
        This.cp_recsed.createrealchild()
        Return

    Procedure DestroyChildren()
        If !Isnull(This.cp_recsed)
            This.cp_recsed.DestroyChildrenChain()
            This.cp_recsed=.Null.
        Endif
        This.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_6')
        Return

    Function IsAChildUpdated()
        Local i_bRes
        i_bRes = This.bUpdated
        i_bRes = i_bRes .Or. This.cp_recsed.IsAChildUpdated()
        Return(i_bRes)

    Procedure ChildrenNewDocument()
        This.cp_recsed.NewDocument()
        Return

    Procedure ChildrenChangeRow()
        Local i_cOldSel,i_cRow
        i_cOldSel=Select()
        Select (This.cTrsName)
        i_cRow=Str(Recno(),7,0)
        With This
            .cp_recsed.ChangeRow(This.cRowID+i_cRow,1;
                ,.w_tablename,"tablename";
                ,.w_progname,"progname";
                ,.w_CPROWNUM,"rfrownum";
                )
        Endwith
        Select (i_cOldSel)
        Return

    Procedure SetWorkFromKeySet
        * --- Initializing work variables from KeySet. They will be used to load the record.
        Select (This.cKeySet)
        With This
            .w_tablename = Nvl(tablename,Space(50))
            .w_progname = Nvl(progname,Space(50))
        Endwith
    Endproc

    * --- Reading record and initializing Form variables
    Procedure LoadRec()
        Local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
        Local i_cSel,i_cDatabaseType,i_nFlds
        Local link_2_4_joined
        link_2_4_joined=.F.
        * --- Area Manuale = Load Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        This.bUpdated=.F.
        This.bHeaderUpdated=.F.
        * --- Select reading record from Master table
        *
        * select * from cprecsec where tablename=KeySet.tablename
        *                            and progname=KeySet.progname
        *
        i_nConn = i_TableProp[this.cprecsec_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.cprecsec_IDX,2],This.bLoadRecFilter,This.cprecsec_IDX,"cp_recsec")
        If i_nConn<>0
            i_nFlds = i_dcx.getfieldscount('cprecsec')
            i_cDatabaseType = cp_GetDatabaseType(i_nConn)
            i_cSel = "cprecsec.*"
            i_cKey = Strtran(This.cKeyWhereODBCqualified,"cprecsecd.","cprecsec.")
            i_cTable = i_cTable+' cprecsec '
            i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,This.cCursor)
            This.bLoaded = i_nRes<>-1 And Not(Eof())
        Else
            * i_cKey = this.cKeyWhere
            i_cKey = cp_PKFox(i_cTable  ,'tablename',This.w_tablename  ,'progname',This.w_progname  )
            Select * From (i_cTable) Where &i_cKey Into Cursor (This.cCursor) nofilter
            This.bLoaded = Not(Eof())
        Endif
        * --- Copy values in work variables
        i_cTF = This.cCursorTrs
        If This.bLoaded
            With This
                .w_AZRAGAZI = Space(40)
                .w_tablename = Nvl(tablename,Space(50))
                .w_progname = Nvl(progname,Space(50))
                .w_TBCOMMENT = i_dcx.Gettabledescr( i_dcx.GettableIdx( .w_tablename ) )
                .w_checkact = Nvl(checkact,Space(1))
                .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_TABLENAME_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_TABLE+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_TABLENAME_DESC_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_PROGRAM+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_PROGRAM_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_ACTIVATE_RULE)
                .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_ACTIVATE_RULE)
                .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_ROW_LABEL)
                .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_FILTER_RULE_DESC)
                .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_COMPANY)
                .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_COND_QUERY_FILTER)
                .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_RULE_DESC)
                .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_COND_QUERY_FILTER)
                .oPgFrm.Page1.oPag.oObj_1_26.Calculate(MSG_COMPANY+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_27.Calculate(MSG_COMPANY_RULE)
                cp_LoadRecExtFlds(This,'cprecsec')
            Endwith
            * === TEMPORARY
            Select (This.cTrsName)
            Zap
            * --- Select reading records from Detail table
            *
            * select * from cprecsecd where tablename=KeySet.tablename
            *                            and progname=KeySet.progname
            *
            i_cOrder = ''
            If This.bApplyOrderDetail
                If This.bApplyOrderDetailCustom And Not Empty(This.cOrderByDettCustom)
                    i_cOrder = 'order by '+This.cOrderByDettCustom
                Else
                    If Not Empty(This.cOrderByDett)
                        i_cOrder = 'order by '+This.cOrderByDett
                    Endif
                Endif
            Endif
            * --- Area Manuale = Before Load Detail
            * --- Fine Area Manuale
            If Used(This.cTrsName+'_bis')
                Select (This.cTrsName+'_bis')
                Zap
            Endif
            i_nConn = i_TableProp[this.cprecsecd_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.cprecsecd_IDX,2])
            If i_nConn<>0
                i_nFlds = i_dcx.getfieldscount('cprecsecd')
                i_cDatabaseType = cp_GetDatabaseType(i_nConn)
                i_cSel = "cprecsecd.* "
                i_cKey = This.cKeyWhereODBCqualified
                i_cTable = i_cTable+" cprecsecd"
                link_2_4_joined=This.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
                =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,This.cCursorTrs)
            Else
                *i_cKey = this.cKeyWhere
                i_cKey = cp_PKFox(i_cTable  ,'tablename',This.w_tablename  ,'progname',This.w_progname  )
                Select * From (i_cTable) cprecsecd Where &i_cKey &i_cOrder Into Cursor (This.cCursorTrs) nofilter
            Endif
            This.i_nRowNum = 0
            Scan
                With This
                    .w_CPROWNUM = CPROWNUM
                    .w_cproword = Nvl(cproword,0)
                    .w_queryfilter = Nvl(QueryFilter,Space(254))
                    .w_descri = Nvl(Descri,Space(150))
                    .w_codazi = Nvl(codazi,Space(5))
                    If link_2_4_joined
                        This.w_codazi = Nvl(CODAZI204,Nvl(This.w_codazi,Space(5)))
                        This.w_AZRAGAZI = Nvl(DESAZI204,Space(40))
                    Else
                        .link_2_4('Load')
                    Endif
                    Select (This.cTrsName)
                    Append Blank
                    Replace CPCCCHK With &i_cTF..CPCCCHK
                    .TrsFromWork()
                    Replace CPROWNUM With &i_cTF..CPROWNUM
                    Replace I_SRV With " "
                    .nLastRow = Recno()
                    Select (This.cCursorTrs)
                    .i_nRowNum = Max(.i_nRowNum,CPROWNUM)
                Endwith
            Endscan
            This.nFirstrow=1
            Select (This.cTrsName)
            If Reccount()=0
                This.InitRow()
            Endif
            With This
                .w_TBCOMMENT = i_dcx.Gettabledescr( i_dcx.GettableIdx( .w_tablename ) )
                .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_TABLENAME_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_TABLE+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_TABLENAME_DESC_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_PROGRAM+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_PROGRAM_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_ACTIVATE_RULE)
                .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_ACTIVATE_RULE)
                .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_ROW_LABEL)
                .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_FILTER_RULE_DESC)
                .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_COMPANY)
                .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_COND_QUERY_FILTER)
                .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_RULE_DESC)
                .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_COND_QUERY_FILTER)
                .oPgFrm.Page1.oPag.oObj_1_26.Calculate(MSG_COMPANY+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_27.Calculate(MSG_COMPANY_RULE)
            Endwith
            Go Top
            With This
                .oPgFrm.Page1.oPag.oBody.Refresh()
                .WorkFromTrs()
                .mCalcRowObjs()
                .SaveDependsOn()
                .SetControlsValue()
                .mHideControls()
                .ChildrenChangeRow()
                .oPgFrm.Page1.oPag.oBody.nAbsRow=1
                .oPgFrm.Page1.oPag.oBody.nRelRow=1
                .NotifyEvent('Load')
            Endwith
        Else
            This.BlankRec()
        Endif
        * --- Area Manuale = Load Record End
        * --- Fine Area Manuale
    Endproc


    * --- Blanking Form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        If Used(This.cTrsName+'_bis')
            Select (This.cTrsName+'_bis')
            Zap
        Endif
        Select (This.cTrsName)
        Zap
        Append Blank
        This.nLastRow  = Recno()
        This.nFirstrow = 1
        This.i_nRowNum = 1
        This.w_CPROWNUM = 1
        Replace CPROWNUM With This.i_nRowNum
        Replace I_SRV    With "A"
        With This
            .w_tablename=Space(50)
            .w_progname=Space(50)
            .w_TBCOMMENT=Space(60)
            .w_checkact=Space(1)
            .w_cproword=10
            .w_queryfilter=Space(254)
            .w_descri=Space(150)
            .w_codazi=Space(5)
            .w_AZRAGAZI=Space(40)
            If .cFunction<>"Filter"
                .DoRTCalc(1,1,.F.)
                .w_progname = '*'
                .w_TBCOMMENT = i_dcx.Gettabledescr( i_dcx.GettableIdx( .w_tablename ) )
                .w_checkact = 'S'
                .DoRTCalc(5,6,.F.)
                .w_descri = .w_TBCOMMENT
                .DoRTCalc(8,8,.F.)
                If Not(Empty(.w_codazi))
                    .link_2_4('Full')
                Endif
                .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_TABLENAME_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_TABLE+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_TABLENAME_DESC_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_PROGRAM+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_PROGRAM_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_ACTIVATE_RULE)
                .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_ACTIVATE_RULE)
                .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_ROW_LABEL)
                .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_FILTER_RULE_DESC)
                .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_COMPANY)
                .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_COND_QUERY_FILTER)
                .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_RULE_DESC)
                .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_COND_QUERY_FILTER)
                .oPgFrm.Page1.oPag.oObj_1_26.Calculate(MSG_COMPANY+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_27.Calculate(MSG_COMPANY_RULE)
            Endif
        Endwith
        cp_BlankRecExtFlds(This,'cprecsec')
        This.DoRTCalc(9,9,.F.)
        This.SaveDependsOn()
        This.SetControlsValue()
        This.TrsFromWork()
        This.mHideControls()
        This.ChildrenChangeRow()
        This.oPgFrm.Page1.oPag.oBody.nAbsRow=0
        This.oPgFrm.Page1.oPag.oBody.nRelRow=1
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = Setting operation
    *     Allowed Parameters: Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        Local i_bVal
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        i_bVal = i_cOp<>"Query"
        * --- Disabling List page when <> from Query
        If Type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
            This.oPgFrm.Pages(This.oPgFrm.PageCount).Enabled=Not(i_bVal)
        Else
            Local i_nPageCount
            For i_nPageCount=This.oPgFrm.PageCount To 1 Step -1
                If Type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
                    This.oPgFrm.Pages(i_nPageCount).Enabled=Not(i_bVal)
                    Exit
                Endif
            Next
        Endif
        With This.oPgFrm
            .Page1.oPag.otablename_1_1.Enabled = i_bVal
            .Page1.oPag.oprogname_1_2.Enabled = i_bVal
            .Page1.oPag.ocheckact_1_6.Enabled = i_bVal
            .Page1.oPag.oObj_1_13.Enabled = i_bVal
            .Page1.oPag.oObj_1_14.Enabled = i_bVal
            .Page1.oPag.oObj_1_15.Enabled = i_bVal
            .Page1.oPag.oObj_1_16.Enabled = i_bVal
            .Page1.oPag.oObj_1_17.Enabled = i_bVal
            .Page1.oPag.oObj_1_18.Enabled = i_bVal
            .Page1.oPag.oObj_1_19.Enabled = i_bVal
            .Page1.oPag.oObj_1_20.Enabled = i_bVal
            .Page1.oPag.oObj_1_21.Enabled = i_bVal
            .Page1.oPag.oObj_1_22.Enabled = i_bVal
            .Page1.oPag.oObj_1_23.Enabled = i_bVal
            .Page1.oPag.oObj_1_24.Enabled = i_bVal
            .Page1.oPag.oObj_1_25.Enabled = i_bVal
            .Page1.oPag.oObj_1_26.Enabled = i_bVal
            .Page1.oPag.oObj_1_27.Enabled = i_bVal
            .Page1.oPag.oBody.Enabled = .T.
            .Page1.oPag.oBody.oBodyCol.Enabled = i_bVal
            If i_cOp = "Edit"
                .Page1.oPag.otablename_1_1.Enabled = .F.
                .Page1.oPag.oprogname_1_2.Enabled = .F.
            Endif
            If i_cOp = "Query"
                .Page1.oPag.otablename_1_1.Enabled = .T.
                .Page1.oPag.oprogname_1_2.Enabled = .T.
            Endif
        Endwith
        This.cp_recsed.SetStatus(i_cOp)
        cp_SetEnabledExtFlds(This,'cprecsec',i_cOp)
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *  this.cp_recsed.SetChildrenStatus(i_cOp)
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt,i_nConn
        i_nConn = i_TableProp[this.cprecsec_IDX,3]
        i_cFlt = This.cQueryFilter
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_tablename,"tablename",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_progname,"progname",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_checkact,"checkact",i_nConn)
        * --- Area Manuale = Build Filter
        * --- Fine Area Manuale
        Return (i_cFlt)
    Endfunc

    * --- Creating cKeySet cursor only with primary key only
    Proc QueryKeySet(i_cWhere,i_cOrderBy)
        Local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
        This.cLastWhere = i_cWhere
        This.cLastOrderBy = i_cOrderBy
        i_cWhere = Iif(Not(Empty(i_cWhere)),' where '+i_cWhere,'')
        i_cOrderBy = Iif(Not(Empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
        i_cKey = This.cKeySelect
        * --- Area Manuale = Query Key Set Init
        * --- Fine Area Manuale
        i_nConn = i_TableProp[this.cprecsec_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.cprecsec_IDX,2])
        i_lTable = "cprecsec"
        i_cDatabaseType = i_ServerConn[i_TableProp[this.cprecsec_IDX,5],6]
        If i_nConn<>0
            Local i_oldzr
            If Vartype(i_nZoomMaxRows)='N'
                i_oldzr=i_nZoomMaxRows
                i_nZoomMaxRows=10000
            Endif
            If !Empty(i_cOrderBy) And At(Substr(i_cOrderBy,11),i_cKey)=0
                i_cKey=i_cKey+','+Substr(i_cOrderBy,11)
            Endif
            Thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,This.cKeySet,i_cDatabaseType)
            If Vartype(i_nZoomMaxRows)='N'
                i_nZoomMaxRows=i_oldzr
            Endif
        Else
            Select &i_cKey From (i_cTable) As (i_lTable) &i_cWhere &i_cOrderBy Into Cursor (This.cKeySet)
            Locate For 1=1 && because cursors reuse main file
        Endif
        * --- Area Manuale = Query Key Set End
        * --- Fine Area Manuale
    Endproc

    *
    *  --- Transaction procedures
    *
    Proc CreateTrs
        This.cTrsName=Sys(2015)
        Create Cursor (This.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
            ,t_cproword N(6);
            ,t_queryfilter C(254);
            ,t_descri C(150);
            ,t_codazi C(5);
            ,CPROWNUM N(10);
            )
        This.oPgFrm.Page1.oPag.oBody.ColumnCount=0
        This.oPgFrm.Page1.oPag.oBody.RecordSource=This.cTrsName
        This.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tcp_recsecbodyrow')
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.Width=This.oPgFrm.Page1.oPag.oBody.Width
        This.oPgFrm.Page1.oPag.oBody.RowHeight=This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.Height
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.F.
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.Visible=.T.
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=This
        * --- Row charateristics
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.ocproword_2_1.ControlSource=This.cTrsName+'.t_cproword'
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oqueryfilter_2_2.ControlSource=This.cTrsName+'.t_queryfilter'
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.odescri_2_3.ControlSource=This.cTrsName+'.t_descri'
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.ocodazi_2_4.ControlSource=This.cTrsName+'.t_codazi'
        This.oPgFrm.Page1.oPag.oBody.ZOrder(1)
        This.oPgFrm.Page1.oPag.oBody3D.ZOrder(1)
        This.oPgFrm.Page1.oPag.pagebmp.ZOrder(1)
        This.oPgFrm.Page1.oPag.oBody.oFirstControl=This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.ocproword_2_1
        * --- New table already open in exclusive mode
        * --- Area Manuale = Create Trs
        * --- Fine Area Manuale

        * --- Insert new Record in Master table
    Function mInsert()
        Local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
        * --- Area Manuale = Insert Master Init
        * --- Fine Area Manuale
        If This.bUpdated .Or. This.IsAChildUpdated()
            i_nConn = i_TableProp[this.cprecsec_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.cprecsec_IDX,2])
            * --- Area Manuale = Autonum Assigned
            * --- Fine Area Manuale
            *
            * insert into cprecsec
            *
            This.NotifyEvent('Insert start')
            This.mUpdateTrs()
            If i_nConn<>0
                i_extfld=cp_InsertFldODBCExtFlds(This,'cprecsec')
                i_extval=cp_InsertValODBCExtFlds(This,'cprecsec')
                Local i_cFld
                i_cFld=" "+;
                    "(tablename,progname,checkact"+i_extfld+",CPCCCHK) "
                i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(This.w_tablename)+;
                    ","+cp_ToStrODBC(This.w_progname)+;
                    ","+cp_ToStrODBC(This.w_checkact)+;
                    i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
                =cp_TrsSQL(i_nConn, i_nnn)
            Else
                i_extfld=cp_InsertFldVFPExtFlds(This,'cprecsec')
                i_extval=cp_InsertValVFPExtFlds(This,'cprecsec')
                cp_CheckDeletedKey(i_cTable,0,'tablename',This.w_tablename,'progname',This.w_progname)
                Insert Into (i_cTable);
                    (tablename,progname,checkact &i_extfld. ,CPCCCHK) Values (;
                    this.w_tablename;
                    ,This.w_progname;
                    ,This.w_checkact;
                    &i_extval. ,cp_NewCCChk())
            Endif
            This.NotifyEvent('Insert end')
        Endif
        * --- Area Manuale = Insert Master End
        * --- Fine Area Manuale
        Return(Not(bTrsErr))

        * --- Insert new Record in Detail table
    Function mInsertDetail(i_nCntLine)
        Local i_cKey,i_nConn,i_cTable,i_TN
        * --- Area Manuale = Insert Detail Init
        * --- Fine Area Manuale
        If This.bUpdated .Or. This.IsAChildUpdated()
            i_nConn = i_TableProp[this.cprecsecd_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.cprecsecd_IDX,2])
            *
            * insert into cprecsecd
            *
            This.NotifyEvent('Insert row start')
            i_TN = This.cTrsName
            Local i_cFldBody,i_cFldValBody
            i_cFldBody=" "+;
                "(tablename,progname,cproword,queryfilter,descri"+;
                ",codazi,CPROWNUM,CPCCCHK)"
            If i_nConn<>0
                i_cFldValBody=" "+;
                    "("+cp_ToStrODBC(This.w_tablename)+","+cp_ToStrODBC(This.w_progname)+","+cp_ToStrODBC(This.w_cproword)+","+cp_ToStrODBC(This.w_queryfilter)+","+cp_ToStrODBC(This.w_descri)+;
                    ","+cp_ToStrODBCNull(This.w_codazi)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
                =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
            Else
                cp_CheckDeletedKey(i_cTable,i_nCntLine,'tablename',This.w_tablename,'progname',This.w_progname)
                Local i_cFldValBodyFox
                i_cFldValBodyFox=" "+;
                    "(this.w_tablename,this.w_progname,this.w_cproword,this.w_queryfilter,this.w_descri"+;
                    ",this.w_codazi,i_nCntLine,cp_NewCCChk())"
                Insert Into (i_cTable) &i_cFldBody Values &i_cFldValBodyFox
            Endif
            This.NotifyEvent('Insert row end')
        Endif
        * --- Area Manuale = Insert Detail End
        * --- Fine Area Manuale
        Return(Not(bTrsErr))

        * --- Update Database
    Function mReplace(i_bEditing)
        Local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
        Local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        If This.bUpdated
            i_nConn = i_TableProp[this.cprecsec_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.cprecsec_IDX,2])
            i_nModRow = 1
            If This.bHeaderUpdated And i_bEditing
                This.mRestoreTrs(.T.)
                This.mUpdateTrs(.T.)
                i_NF = This.cCursor
                i_OldCCCHK = Iif(i_bEditing,&i_NF..CPCCCHK,'')
                *
                * update cprecsec
                *
                This.NotifyEvent('Update start')
                If i_nConn<>0
                    i_cWhere = This.cKeyWhereODBC
                    i_extfld=cp_ReplaceODBCExtFlds(This,'cprecsec')
                    i_nnn="UPDATE "+i_cTable+" SET "+;
                        " checkact="+cp_ToStrODBC(This.w_checkact)+;
                        ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                        +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                    i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
                Else
                    * i_cWhere = this.cKeyWhere
                    i_extfld=cp_ReplaceVFPExtFlds(This,'cprecsec')
                    i_cWhere = cp_PKFox(i_cTable  ,'tablename',This.w_tablename  ,'progname',This.w_progname  )
                    Update (i_cTable) Set;
                        checkact=This.w_checkact;
                        ,CPCCCHK=cp_NewCCChk() &i_extfld. Where &i_cWhere And CPCCCHK==i_OldCCCHK
                    i_nModRow=_Tally
                Endif
                This.NotifyEvent('Update end')
            Endif
            * --- Update Detail table
            If i_nModRow>0   && Master table updated with success
                Set Delete Off
                Select (This.cTrsName)
                i_TN = This.cTrsName
                i_bUpdAll = .F.
                Scan For (Not Empty(t_queryfilter)) And ((I_SRV="U" Or i_bUpdAll) Or I_SRV="A" Or Deleted())
                    This.WorkFromTrs()
                    This.oPgFrm.Page1.oPag.oBody.nAbsRow=Recno()
                    i_OldCCCHK = Iif(i_bEditing.Or.I_SRV<>"A",&i_TN..CPCCCHK,'')
                    i_nConn = i_TableProp[this.cprecsecd_IDX,3]
                    i_cTable = cp_SetAzi(i_TableProp[this.cprecsecd_IDX,2])
                    i_nRec = Recno()
                    Set Delete On
                    If Deleted()
                        If I_SRV<>"A"
                            * --- Deleting row children
                            This.cp_recsed.ChangeRow(This.cRowID+Str(i_nRec,7,0),0;
                                ,This.w_tablename,"tablename";
                                ,This.w_progname,"progname";
                                ,This.w_CPROWNUM,"rfrownum";
                                )
                            This.cp_recsed.mDelete()
                            *
                            * delete from cprecsecd
                            *
                            This.NotifyEvent('Delete row start')
                            This.mRestoreTrsDetail()
                            If i_nConn<>0
                                i_cWhere = This.cKeyWhereODBC
                                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                                    " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                                    " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
                            Else
                                i_cWhere = This.cKeyWhere
                                Delete From (i_cTable) Where &i_cWhere And CPROWNUM=&i_TN.->CPROWNUM;
                                    and CPCCCHK==i_OldCCCHK
                                i_nModRow=_Tally
                            Endif
                            This.NotifyEvent('Delete row end')
                        Endif
                    Else
                        If I_SRV="A"
                            This.mUpdateTrsDetail()
                            * --- Insert new row in the database table
                            i_NR = CPROWNUM
                            =This.mInsertDetail(i_NR)
                        Else
                            * --- Area Manuale = Replace Loop
                            * --- Fine Area Manuale
                            *
                            * update cprecsecd
                            *
                            This.NotifyEvent('Update row start')
                            This.mRestoreTrsDetail(.T.)
                            This.mUpdateTrsDetail(.T.)
                            If i_nConn<>0
                                i_cWhere = This.cKeyWhereODBC
                                i_nnn="UPDATE "+i_cTable+" SET "+;
                                    " cproword="+cp_ToStrODBC(This.w_cproword)+;
                                    ",queryfilter="+cp_ToStrODBC(This.w_queryfilter)+;
                                    ",descri="+cp_ToStrODBC(This.w_descri)+;
                                    ",codazi="+cp_ToStrODBCNull(This.w_codazi)+;
                                    ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                                    " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                                    " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
                            Else
                                i_cWhere = This.cKeyWhere
                                Update (i_cTable) Set ;
                                    cproword=This.w_cproword;
                                    ,QueryFilter=This.w_queryfilter;
                                    ,Descri=This.w_descri;
                                    ,codazi=This.w_codazi;
                                    ,CPCCCHK=cp_NewCCChk() Where &i_cWhere And CPROWNUM=&i_TN.->CPROWNUM;
                                    and CPCCCHK==i_OldCCCHK
                                i_nModRow=_Tally
                            Endif
                            This.NotifyEvent('Update row end')
                        Endif
                    Endif
                    If i_nModRow<1
                        Exit
                    Endif
                    Set Delete Off
                Endscan
                Set Delete On
                * --- Make the last record the actual position (mcalc problem with double transitory)1
                This.SetRow(Reccount(This.cTrsName), .F.)
                This.SetControlsValue()
            Endif
            =cp_CheckMultiuser(i_nModRow)
        Endif
        If Not(bTrsErr)
            * --- Ask children belonging to rows to save themselves
            Select (This.cTrsName)
            i_TN = This.cTrsName
            Scan For (Not Empty(t_queryfilter))
                * --- > Optimize children saving
                i_nRec = Recno()
                This.WorkFromTrs()
                This.cp_recsed.ChangeRow(This.cRowID+Str(i_nRec,7,0),0;
                    ,This.w_tablename,"tablename";
                    ,This.w_progname,"progname";
                    ,This.w_CPROWNUM,"rfrownum";
                    )
                This.cp_recsed.mReplace()
                This.cp_recsed.bSaveContext=.F.
            Endscan
            This.cp_recsed.bSaveContext=.T.
            * --- Make the last record the actual position (mcalc problem with double transitory)2
            This.SetRow(Reccount(This.cTrsName), .F.)
            This.SetControlsValue()
        Endif
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(bTrsErr)

        * --- Delete Records
    Function mDelete()
        Local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
        Local i_cDel,i_nRec
        * --- Area Manuale = Delete Init
        * --- Fine Area Manuale
        If Not(bTrsErr)
            Select (This.cTrsName)
            i_TN = This.cTrsName
            i_nModRow = 1
            i_cDel = Set('DELETED')
            Set Delete Off
            Scan For (Not Empty(t_queryfilter)) And I_SRV<>'A'
                This.WorkFromTrs()
                i_nRec = Recno()
                * --- cp_recsed : Deleting
                This.cp_recsed.bSaveContext=.F.
                This.cp_recsed.ChangeRow(This.cRowID+Str(i_nRec,7,0),0;
                    ,This.w_tablename,"tablename";
                    ,This.w_progname,"progname";
                    ,This.w_CPROWNUM,"rfrownum";
                    )
                This.cp_recsed.bSaveContext=.T.
                This.cp_recsed.mDelete()
                If bTrsErr
                    i_nModRow = -1
                    Exit
                Endif
                i_OldCCCHK=&i_TN..CPCCCHK
                i_nConn = i_TableProp[this.cprecsecd_IDX,3]
                i_cTable = cp_SetAzi(i_TableProp[this.cprecsecd_IDX,2])
                *
                * delete cprecsecd
                *
                This.NotifyEvent('Delete row start')
                If i_nConn<>0
                    i_cWhere = This.cKeyWhereODBC
                    i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                        " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                        " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
                Else
                    i_cWhere = This.cKeyWhere
                    Delete From (i_cTable) Where &i_cWhere And CPROWNUM=&i_TN.->CPROWNUM;
                        and CPCCCHK==i_OldCCCHK
                    i_nModRow=_Tally
                Endif
                This.NotifyEvent('Delete row end')
                If i_nModRow<1
                    Exit
                Endif
            Endscan
            Set Delete &i_cDel
            * --- Make the last record the actual position (mcalc problem with double transitory)3
            This.SetRow(Reccount(This.cTrsName), .F.)
            This.SetControlsValue()
            If i_nModRow>0 And Not(bTrsErr)
                i_NF = This.cCursor
                i_OldCCCHK = &i_NF..CPCCCHK
                i_nConn = i_TableProp[this.cprecsec_IDX,3]
                i_cTable = cp_SetAzi(i_TableProp[this.cprecsec_IDX,2])
                *
                * delete cprecsec
                *
                This.NotifyEvent('Delete start')
                If i_nConn<>0
                    i_cWhere = This.cKeyWhereODBC
                    i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                        " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
                Else
                    i_cWhere = This.cKeyWhere
                    Delete From (i_cTable) Where &i_cWhere And CPCCCHK==i_OldCCCHK
                    i_nModRow=_Tally
                Endif
                This.NotifyEvent('Delete end')
            Endif
            =cp_CheckMultiuser(i_nModRow)
            If Not(bTrsErr)
                This.mRestoreTrs()
                Select (This.cTrsName)
                i_TN = This.cTrsName
                i_cDel = Set('DELETED')
                Set Delete Off
                Scan For (Not Empty(t_queryfilter)) And I_SRV<>'A'
                    This.WorkFromTrs()
                    i_OldCCCHK=&i_TN..CPCCCHK
                    This.mRestoreTrsDetail()
                Endscan
                Set Delete &i_cDel
                * --- Make the last record the actual position (mcalc problem with double transitory)4
                This.SetRow(Reccount(This.cTrsName), .F.)
                This.SetControlsValue()
            Endif
        Endif
        This.mDeleteWarnings()
        * --- Area Manuale = Delete End
        * --- Fine Area Manuale
        Return

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        Local i_cTable,i_nConn
        i_nConn = i_TableProp[this.cprecsec_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.cprecsec_IDX,2])
        If i_bUpd
            With This
                .DoRTCalc(1,2,.T.)
                If .o_tablename<>.w_tablename
                    .w_TBCOMMENT = i_dcx.Gettabledescr( i_dcx.GettableIdx( .w_tablename ) )
                Endif
                .DoRTCalc(4,6,.T.)
                If .o_tablename<>.w_tablename
                    .w_descri = .w_TBCOMMENT
                Endif
                .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_TABLENAME_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_TABLE+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_TABLENAME_DESC_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_PROGRAM+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_PROGRAM_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_ACTIVATE_RULE)
                .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_ACTIVATE_RULE)
                .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_ROW_LABEL)
                .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_FILTER_RULE_DESC)
                .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_COMPANY)
                .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_COND_QUERY_FILTER)
                .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_RULE_DESC)
                .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_COND_QUERY_FILTER)
                .oPgFrm.Page1.oPag.oObj_1_26.Calculate(MSG_COMPANY+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_27.Calculate(MSG_COMPANY_RULE)
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
            Endwith
            This.DoRTCalc(8,9,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
            .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_TABLENAME_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_TABLE+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_TABLENAME_DESC_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_PROGRAM+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_PROGRAM_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_ACTIVATE_RULE)
            .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_ACTIVATE_RULE)
            .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_ROW_LABEL)
            .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_FILTER_RULE_DESC)
            .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_COMPANY)
            .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_COND_QUERY_FILTER)
            .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_RULE_DESC)
            .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_COND_QUERY_FILTER)
            .oPgFrm.Page1.oPag.oObj_1_26.Calculate(MSG_COMPANY+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_27.Calculate(MSG_COMPANY_RULE)
        Endwith
        Return

    Proc mCalcRowObjs()
        With This
        Endwith
        Return

        * --- Enable controls under condition
    Procedure mEnableControls()
        This.mEnableControlsFixed()
        This.mHideControls()
        DoDefault()
        Return

        * --- Enable controls under condition for Grid and for FixedPos fields
    Procedure mEnableControlsFixed()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        This.mHideRowControls()
        DoDefault()
        Return

    Procedure mHideRowControls()
        DoDefault()
        Return


        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
        Endwith
        * --- Area Manuale = Notify Event End
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

        * --- Link procedure for entity name=codazi
    Func link_2_4(i_cCtrl,oSource)
        Local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
        i_nConn = i_TableProp[this.cpazi_IDX,3]
        i_lTable = "cpazi"
        If This.cFunction='Load'
            i_cTable = cp_SetAzi(i_TableProp[this.cpazi_IDX,2], .T., This.cpazi_IDX)
        Else
            i_cTable = cp_SetAzi(i_TableProp[this.cpazi_IDX,2])
        Endif
        i_nArea = Select()
        i_bEmpty = .F.
        If Used("_Link_")
            Select _Link_
            Use
        Endif
        Do Case
            Case Empty(This.w_codazi) And i_cCtrl<>"Drop"
                i_bEmpty = .T.
                i_reccount=0
            Case i_cCtrl='Part'
                i_cFlt=cp_QueryEntityFilter('',True,'cpazi')
                If i_nConn<>0
                    i_cWhere = i_cFlt+" CODAZI like "+cp_ToStrODBC(Trim(This.w_codazi)+"%");

                    i_ret=cp_SQL(i_nConn,"select CODAZI,DESAZI";
                        +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CODAZI","_Link_",2)
                    i_reccount = Iif(i_ret=-1,0,Reccount())
                Else
                    i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                        ,'CODAZI',Trim(This.w_codazi))
                    Select codazi,DESAZI;
                        from (i_cTable) As (i_lTable) Where &i_cWhere. Order By codazi Into Cursor _Link_
                    i_reccount = _Tally
                Endif
                If Trim(This.w_codazi)==Trim(_Link_.codazi)
                    i_reccount=1
                Endif
                If i_reccount>1
                    If !Empty(This.w_codazi) And !This.bDontReportError
                        deferred_cp_zoom('cpazi','*','CODAZI',cp_AbsName(oSource.Parent,'ocodazi_2_4'),i_cWhere,'',"",'',This)
                    Endif
                    This.bDontReportError = .T.
                    i_reccount = 0

                Endif
            Case i_cCtrl='Drop'
                If i_nConn<>0
                    i_ret=cp_SQL(i_nConn,"select CODAZI,DESAZI";
                        +" from "+i_cTable+" where CODAZI="+cp_ToStrODBC(oSource.xKey(1));
                        ,"_Link_")
                    i_reccount = Iif(i_ret=-1,0,Reccount())
                Else
                    i_cWhere = cp_PKFox(i_cTable;
                        ,'CODAZI',oSource.xKey(1))
                    Select codazi,DESAZI;
                        from (i_cTable) Where &i_cWhere. Into Cursor _Link_
                    i_reccount = _Tally
                Endif
            Case i_cCtrl='Full' Or i_cCtrl='Load' Or i_cCtrl='Extend'
                If .Not. Empty(This.w_codazi)
                    If i_nConn<>0
                        i_ret=cp_SQL(i_nConn,"select CODAZI,DESAZI";
                            +" from "+i_cTable+" where CODAZI="+cp_ToStrODBC(This.w_codazi);
                            ,"_Link_")
                        i_reccount = Iif(i_ret=-1,0,Reccount())
                    Else
                        i_cWhere = cp_PKFox(i_cTable;
                            ,'CODAZI',This.w_codazi)
                        Select codazi,DESAZI;
                            from (i_cTable) Where &i_cWhere. Into Cursor _Link_
                        i_reccount = _Tally
                    Endif
                Else
                    i_reccount = 0
                Endif
        Endcase
        If i_reccount>0 And Used("_Link_")
            This.w_codazi = Nvl(_Link_.codazi,Space(5))
            This.w_AZRAGAZI = Nvl(_Link_.DESAZI,Space(40))
        Else
            If i_cCtrl<>'Load'
                This.w_codazi = Space(5)
            Endif
            This.w_AZRAGAZI = Space(40)
        Endif
        i_bRes=i_reccount=1
        If i_bRes And i_cCtrl<>'Full' And i_cCtrl<>'Load' And Used("_Link_")
            i_cKey = cp_SetAzi(i_TableProp[this.cpazi_IDX,2])+'\'+cp_ToStr(_Link_.codazi,1)
            cp_ShowWarn(i_cKey,This.cpazi_IDX)
        Endif
        If i_cCtrl='Drop'
            This.TrsFromWork()
            This.NotifyEvent('w_codazi Changed')
        Endif
        Select (i_nArea)
        Return(i_bRes Or i_bEmpty)
    Endfunc

    Func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
        * questo link aggiunge 2 campi al cursore risultato
        Local i_cLinkedTable,i_cNewSel,i_res
        i_res=.F.
        If i_nConn=i_TableProp[this.cpazi_IDX,3] And i_nFlds+2<200
            i_cLinkedTable = cp_SetAzi(i_TableProp[this.cpazi_IDX,2])
            i_cNewSel = i_cSel+ ",link_2_4.CODAZI as CODAZI204"+ ",link_2_4.DESAZI as DESAZI204"
            Do Case
                Case Inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle") Or Left(i_cDatabaseType,3)="DB2"
                    i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on cprecsecd.codazi=link_2_4.CODAZI"
                    i_cSel = i_cNewSel
                    i_res = .T.
                    i_nFlds = i_nFlds+2
                Case Inlist(i_cDatabaseType,"Adabas","SAPDB")
                    i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
                    i_cKey=i_cKey+'+" and cprecsecd.codazi=link_2_4.CODAZI(+)"'
                    i_cSel = i_cNewSel
                    i_res = .T.
                    i_nFlds = i_nFlds+2
            Endcase
        Endif
        Return(i_res)
    Endfunc

    Function SetControlsValue()
        Select (This.cTrsName)
        If Not(This.oPgFrm.Page1.oPag.otablename_1_1.Value==This.w_tablename)
            This.oPgFrm.Page1.oPag.otablename_1_1.Value=This.w_tablename
        Endif
        If Not(This.oPgFrm.Page1.oPag.oprogname_1_2.Value==This.w_progname)
            This.oPgFrm.Page1.oPag.oprogname_1_2.Value=This.w_progname
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTBCOMMENT_1_5.Value==This.w_TBCOMMENT)
            This.oPgFrm.Page1.oPag.oTBCOMMENT_1_5.Value=This.w_TBCOMMENT
        Endif
        If Not(This.oPgFrm.Page1.oPag.ocheckact_1_6.RadioValue()==This.w_checkact)
            This.oPgFrm.Page1.oPag.ocheckact_1_6.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oAZRAGAZI_2_7.Value==This.w_AZRAGAZI)
            This.oPgFrm.Page1.oPag.oAZRAGAZI_2_7.Value=This.w_AZRAGAZI
        Endif
        If Not(This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.ocproword_2_1.Value==This.w_cproword)
            This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.ocproword_2_1.Value=This.w_cproword
            Replace t_cproword With This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.ocproword_2_1.Value
        Endif
        If Not(This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oqueryfilter_2_2.Value==This.w_queryfilter)
            This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oqueryfilter_2_2.Value=This.w_queryfilter
            Replace t_queryfilter With This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oqueryfilter_2_2.Value
        Endif
        If Not(This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.odescri_2_3.Value==This.w_descri)
            This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.odescri_2_3.Value=This.w_descri
            Replace t_descri With This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.odescri_2_3.Value
        Endif
        If Not(This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.ocodazi_2_4.Value==This.w_codazi)
            This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.ocodazi_2_4.Value=This.w_codazi
            Replace t_codazi With This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.ocodazi_2_4.Value
        Endif
        cp_SetControlsValueExtFlds(This,'cprecsec')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            If .bUpdated
                Do Case
                    Otherwise
                        i_bRes = .CheckRow()
                        If .Not. i_bRes
                            If !Isnull(.oNewFocus)
                                If .oPgFrm.ActivePage>1
                                    .oPgFrm.ActivePage=1
                                Endif
                                .oNewFocus.SetFocus()
                                .oNewFocus=.Null.
                            Endif
                        Endif
                        * return(i_bRes)
                Endcase
            Endif
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ERRORMSG(MSG_FIELD_CANNOT_BE_NULL_QM)
                Return(i_bRes)
            Endif
            If Not(i_bnoChk)
                cp_ERRORMSG(i_cErrorMsg)
                Return(i_bRes)
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

    * --- CheckRow
    Func CheckRow()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            Do Case
            Endcase
            i_bRes = i_bRes .And. .cp_recsed.CheckForm()
            If Not Empty(.w_queryfilter)
                * --- Area Manuale = Check Row
                * --- Fine Area Manuale
            Else
                If This.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 And This.oPgFrm.Page1.oPag.oBody.nAbsRow<>This.nLastRow And (Type('i_bCheckEmptyRows')='U' Or i_bCheckEmptyRows)
                    i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
                    i_bRes=.F.
                    i_bnoChk=.F.
                Endif
            Endif
            If Not(i_bnoChk)
                Do cp_ERRORMSG With i_cErrorMsg
                Return(i_bRes)
            Endif
            If Not(i_bnoObbl)
                Do cp_ERRORMSG With MSG_FIELD_CANNOT_BE_NULL_QM
                Return(i_bRes)
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

    * --- SaveDependsOn
    Proc SaveDependsOn()
        This.o_tablename = This.w_tablename
        * --- cp_recsed : Depends On
        This.cp_recsed.SaveDependsOn()
        Return


        * --- FullRow
    Func FullRow()
        Local i_bRes,i_nArea
        i_nArea=Select()
        Select (This.cTrsName)
        i_bRes=(Not Empty(t_queryfilter))
        Select(i_nArea)
        Return(i_bRes)
    Endfunc

    * --- InitRow
    Proc InitRow()
        This.NotifyEvent("Before Init Row")
        Select (This.cTrsName)
        Append Blank
        This.nLastRow = Recno()
        This.i_nRowNum = This.i_nRowNum+1
        This.w_CPROWNUM = This.i_nRowNum
        Replace CPROWNUM With This.i_nRowNum
        Replace I_SRV    With "A"
        Replace I_RECNO  With Recno()
        With This
            .w_cproword=Min(999999,cp_maxroword()+10)
            .w_queryfilter=Space(254)
            .w_descri=Space(150)
            .w_codazi=Space(5)
            .DoRTCalc(1,6,.F.)
            .w_descri = .w_TBCOMMENT
            .DoRTCalc(8,8,.F.)
            If Not(Empty(.w_codazi))
                .link_2_4('Full')
            Endif
        Endwith
        This.DoRTCalc(9,9,.F.)
        This.oPgFrm.Page1.oPag.oBody.nAbsRow=Recno()
        This.TrsFromWork()
        This.ChildrenChangeRow()
        This.NotifyEvent("Init Row")
    Endproc

    * --- WorkFromTrs
    Proc WorkFromTrs()
        Select (This.cTrsName)
        This.w_cproword = t_cproword
        This.w_queryfilter = t_queryfilter
        This.w_descri = t_descri
        This.w_codazi = t_codazi
        This.w_CPROWNUM = CPROWNUM
        This.oPgFrm.Page1.oPag.oBody.nAbsRow=Recno()
    Endproc

    * --- TrsFromWork
    Proc TrsFromWork()
        Select (This.cTrsName)
        Replace I_RECNO With Recno()
        Replace t_cproword With This.w_cproword
        Replace t_queryfilter With This.w_queryfilter
        Replace t_descri With This.w_descri
        Replace t_codazi With This.w_codazi
    Endproc

    * --- SubtractTotals
    Proc SubtractTotals()
    Endproc
Enddefine

* --- Define pages as container
Define Class tcp_recsecPag1 As StdContainer
    Width  = 749
    Height = 354
    stdWidth  = 749
    stdheight = 354
    resizeXpos=452
    resizeYpos=245
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.

    Add Object otablename_1_1 As StdField With uid="NRBQWSBWLH",rtseq=1,rtrep=.F.,;
        cFormVar = "w_tablename", cQueryName = "tablename",;
        bObbl = .F. , nPag = 1, Value=Space(50), bMultilanguage =  .F.,;
        ToolTipText = "Nome della tabella su cui applicare la politica di sicurezza",;
        HelpContextID = 96828647,;
        bGlobalFont=.T.,;
        Height=21, Width=599, Left=85, Top=11, InputMask=Replicate('X',50)

    Func otablename_1_1.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            If bRes And !(i_dcx.GettableIdx( Alltrim( .w_tablename ) )>0)
                bRes=(Messagebox(cp_Translate(Thisform.msgFmt("La tabella selezionata non � disponibile all'interno del dizionario dati"))+". "+cp_Translate(MSG_ACCEPT_ANYWAY),4+32,"")=6)
                This.Parent.oContained.bDontReportError=!bRes
            Endif
        Endwith
        Return bRes
    Endfunc

    Add Object oprogname_1_2 As StdField With uid="ESJNKXIVCU",rtseq=2,rtrep=.F.,;
        cFormVar = "w_progname", cQueryName = "tablename,progname",;
        bObbl = .F. , nPag = 1, Value=Space(50), bMultilanguage =  .F.,;
        ToolTipText = "Nome programma su cui applicare la politica di sicurezza",;
        HelpContextID = 243475315,;
        bGlobalFont=.T.,;
        Height=21, Width=363, Left=85, Top=59, InputMask=Replicate('X',50)

    Add Object oTBCOMMENT_1_5 As StdField With uid="GWXCAUQGAN",rtseq=3,rtrep=.F.,;
        cFormVar = "w_TBCOMMENT", cQueryName = "TBCOMMENT",Enabled=.F.,;
        bObbl = .F. , nPag = 1, Value=Space(60), bMultilanguage =  .F.,;
        ToolTipText = "Descrizione tabella",;
        HelpContextID = 69969445,;
        bGlobalFont=.T.,;
        Height=21, Width=599, Left=85, Top=35, InputMask=Replicate('X',60)

    Add Object ocheckact_1_6 As StdCheck With uid="YNWBKZRIGR",rtseq=4,rtrep=.F.,Left=85, Top=83, Caption="Regola attiva",;
        ToolTipText = "Regola attiva",;
        HelpContextID = 188249945,;
        cFormVar="w_checkact", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func ocheckact_1_6.RadioValue(i_bTrs,i_bOld)
        Local xVal,i_cF
        i_cF=This.Parent.oContained.cTrsName
        xVal=Iif(i_bTrs,Iif(i_bOld,&i_cF..checkact,&i_cF..t_checkact),This.Value)
        Return(Iif(xVal =1,'S',;
            space(1)))
    Endfunc
    Func ocheckact_1_6.GetRadio()
        This.Parent.oContained.w_checkact = This.RadioValue()
        Return .T.
    Endfunc

    Func ocheckact_1_6.ToRadio()
        This.Parent.oContained.w_checkact=Trim(This.Parent.oContained.w_checkact)
        Return(;
            iif(This.Parent.oContained.w_checkact=='S',1,;
            0))
    Endfunc

    Func ocheckact_1_6.SetRadio()
        This.Value=This.ToRadio()
    Endfunc


    Add Object oObj_1_13 As cp_setobjprop With uid="SKPUZOAYUU",Left=894, Top=9, Width=93,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_tablename",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425398;
        , bGlobalFont=.T.



    Add Object oObj_1_14 As cp_setCtrlObjprop With uid="WTFHXDANKA",Left=792, Top=10, Width=93,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Tabella:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425398;
        , bGlobalFont=.T.



    Add Object oObj_1_15 As cp_setobjprop With uid="SAFEMKZHUJ",Left=894, Top=35, Width=93,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_TBCOMMENT",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425398;
        , bGlobalFont=.T.



    Add Object oObj_1_16 As cp_setCtrlObjprop With uid="IMXMZFRSVJ",Left=792, Top=59, Width=93,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Programma:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425398;
        , bGlobalFont=.T.



    Add Object oObj_1_17 As cp_setobjprop With uid="SIDWZYGKOQ",Left=894, Top=59, Width=93,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_progname",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425398;
        , bGlobalFont=.T.



    Add Object oObj_1_18 As cp_setobjprop With uid="RKGBESZNIH",Left=792, Top=83, Width=93,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_checkact",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425398;
        , bGlobalFont=.T.



    Add Object oObj_1_19 As cp_setobjprop With uid="KAENEUPWTM",Left=894, Top=83, Width=93,Height=18,;
        caption='Object',;
        cProp="caption",cObj="w_checkact",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425398;
        , bGlobalFont=.T.



    Add Object oObj_1_20 As cp_setCtrlObjprop With uid="YRZXFUYBXN",Left=791, Top=114, Width=93,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Riga",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425398;
        , bGlobalFont=.T.



    Add Object oObj_1_21 As cp_setCtrlObjprop With uid="AAEXIHZLDH",Left=894, Top=114, Width=93,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Filtro da applicare / descrizione regola",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425398;
        , bGlobalFont=.T.



    Add Object oObj_1_22 As cp_setCtrlObjprop With uid="TNTKFGFRZH",Left=992, Top=114, Width=93,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Azienda",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425398;
        , bGlobalFont=.T.



    Add Object oObj_1_23 As cp_setobjprop With uid="YFKZLBUTEW",Left=894, Top=139, Width=93,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_queryfilter",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425398;
        , bGlobalFont=.T.



    Add Object oObj_1_24 As cp_setobjprop With uid="WPKILLVJVN",Left=894, Top=158, Width=93,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_TBCOMMENT",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425398;
        , bGlobalFont=.T.



    Add Object oObj_1_25 As cp_setobjprop With uid="CGPHDVALPP",Left=992, Top=139, Width=93,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_queryfilter",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425398;
        , bGlobalFont=.T.



    Add Object oObj_1_26 As cp_setCtrlObjprop With uid="BINUQMREVJ",Left=791, Top=331, Width=93,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Azienda:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425398;
        , bGlobalFont=.T.



    Add Object oObj_1_27 As cp_setobjprop With uid="FUEPHDVJAN",Left=890, Top=331, Width=93,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_AZRAGAZI",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425398;
        , bGlobalFont=.T.


    Add Object oStr_1_3 As StdString With uid="CJOAMTEIGX",Visible=.T., Left=2, Top=11,;
        Alignment=1, Width=77, Height=18,;
        Caption="Tabella:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_4 As StdString With uid="JLRACMPNQZ",Visible=.T., Left=-1, Top=331,;
        Alignment=1, Width=54, Height=18,;
        Caption="Azienda:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_8 As StdString With uid="IZOYZHZQQW",Visible=.T., Left=67, Top=112,;
        Alignment=0, Width=208, Height=18,;
        Caption="Filtro da applicare / descrizione regola"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_9 As StdString With uid="YRTLUFAJCL",Visible=.T., Left=10, Top=59,;
        Alignment=1, Width=69, Height=18,;
        Caption="Programma:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_11 As StdString With uid="OGTDHCGELL",Visible=.T., Left=3, Top=112,;
        Alignment=0, Width=46, Height=18,;
        Caption="Riga"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_12 As StdString With uid="JIBLMUSDNE",Visible=.T., Left=532, Top=112,;
        Alignment=2, Width=62, Height=18,;
        Caption="Azienda"  ;
        , bGlobalFont=.T.

    Add Object oBox_1_7 As StdBox With uid="XQZCGMPEWE",Left=-3, Top=108, Width=750,Height=28

    Add Object oBox_1_10 As StdBox With uid="DBSUBMREXX",Left=55, Top=108, Width=2,Height=219
    Proc UIEnable(i_a)
        If This.CreateChildren And i_a And This.Parent.Parent.Parent.cFunction<>'Filter'
            If Type('this.oContained')='O' And At("cp_recsed",Lower(This.oContained.cp_recsed.Class))=0
                This.oContained.cp_recsed.createrealchild()
            Endif
            This.CreateChildren=.F.
        Endif
    Endproc

    Add Object oEndHeader As BodyKeyMover With nDirection=1
    *
    * --- BODY transaction
    *
    Add Object oBody3D As Shape With Left=-11,Top=137,;
        width=608,Height=Int(Fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,SpecialEffect=0

    Add Object oBody As StdBody Noinit With ;
        left=-10,Top=138,Width=607,Height=Int(Fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,ColumnCount=0,GridLines=1,;
        HeaderHeight=0,DeleteMark=.F.,ScrollBars=0,Enabled=.F.,;
        cLinkFile='cpazi|'

    Proc oBody.SetCurrentRow()
        Thisform.LockScreen=.T.
        Select (This.Parent.oContained.cTrsName)
        If Recno()<>This.nAbsRow
            If This.nAbsRow<>0 And Inlist(This.Parent.oContained.cFunction,'Edit','Load')
                If This.nAbsRow<>This.Parent.oContained.i_lastcheckrow And !This.Parent.oContained.CheckRow()
                    This.Parent.oContained.i_lastcheckrow=0
                    This.Parent.oContained.__dummy__.Enabled=.T.
                    This.Parent.oContained.__dummy__.SetFocus()
                    Select (This.Parent.oContained.cTrsName)
                    Go (This.nAbsRow)
                    This.SetFullFocus()
                    If !Isnull(This.Parent.oContained.oNewFocus)
                        This.Parent.oContained.oNewFocus.SetFocus()
                        This.Parent.oContained.oNewFocus=.Null.
                    Endif
                    This.Parent.oContained.__dummy__.Enabled=.F.
                    Thisform.LockScreen=.F.
                    Return
                Endif
            Endif
            This.nAbsRow=Recno()
            This.Parent.oContained.WorkFromTrs()
            This.Parent.oContained.mEnableControlsFixed()
            This.Parent.oContained.mCalcRowObjs()
            This.Parent.oContained.ChildrenChangeRow()
            * --- Area Manuale = Set Current Row
            * --- Fine Area Manuale
        Else
            This.Parent.oContained.mEnableControlsFixed()
        Endif
        If !Isnull(This.Parent.oContained.oNewFocus)
            This.Parent.oContained.oNewFocus.SetFocus()
            This.Parent.oContained.oNewFocus=.Null.
        Endif
        If This.RelativeRow<>0
            This.nRelRow=This.RelativeRow
        Endif
        If This.nBeforeAfter>0 && and Version(5)<700
            This.nBeforeAfter=This.nBeforeAfter-1
        Endif
        Thisform.LockScreen=.F.
        This.Parent.oContained.i_lastcheckrow=0
    Endproc
    Func oBody.GetDropTarget(cFile,nX,nY)
        Local oDropInto
        oDropInto=.Null.
        Do Case
            Case cFile='cpazi'
                oDropInto=This.oBodyCol.oRow.ocodazi_2_4
        Endcase
        Return(oDropInto)
    Endfunc

    Add Object oBeginFixedBody As FixedBodyKeyMover With nDirection=1


    Add Object oLinkPC_2_6 As stdDynamicChildContainer With uid="VYZLQSNUZT",bOnScreen=.T.,Width=145,Height=213,;
        left=601, Top=139;


    Add Object oAZRAGAZI_2_7 As StdField With uid="OTGJVDLKOA",rtseq=9,rtrep=.F.,;
        cFormVar="w_AZRAGAZI",Value=Space(40),Enabled=.F.,;
        ToolTipText = "Azienda",;
        HelpContextID = 84177568,;
        cTotal="", bFixedPos=.T., cQueryName = "AZRAGAZI",;
        bObbl = .F. , nPag = 2, bMultilanguage =  .F.,;
        bGlobalFont=.T.,;
        Height=18, Width=517, Left=56, Top=331, InputMask=Replicate('X',40)

    Add Object oBox_2_5 As StdBox With uid="ZKRLAVLJRL",Left=528, Top=108, Width=2,Height=219

    Add Object oEndFixedBody As FixedBodyKeyMover With nDirection=-1
    Add Object oBeginFooter As BodyKeyMover With nDirection=-1
Enddefine

* --- Defining Body row
Define Class tcp_recsecBodyRow As Container
    Width=598
    Height=Int(Fontmetric(1,"Arial",9,"")*2*1.3000000000000000)
    BackStyle=0                                                && 0=trasparente
    BorderWidth=0                                              && Spessore Bordo
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    oContained = .Null.

    Add Object ocproword_2_1 As StdTrsField With uid="LWZYBISXQD",rtseq=5,rtrep=.T.,;
        cFormVar="w_cproword",Value=0,;
        HelpContextID = 134608488,;
        cTotal = "", SpecialEffect=1, BorderStyle=0,;
        bObbl = .F. , nPag = 2, bIsInHeader=.F., bMultilanguage =  .F.,;
        bGlobalFont=.T.,;
        Height=17, Width=55, Left=-2, Top=0

    Add Object oqueryfilter_2_2 As StdTrsField With uid="GZWBCSNQQP",rtseq=6,rtrep=.T.,;
        cFormVar="w_queryfilter",Value=Space(254),;
        ToolTipText = "Condizione da applicare nella Query Filter (doppio click per la selezione della query)",;
        HelpContextID = 90040097,;
        cTotal = "", SpecialEffect=1, BorderStyle=0,;
        bObbl = .F. , nPag = 2, bIsInHeader=.F., bMultilanguage =  .F.,;
        bGlobalFont=.T.,;
        Height=17, Width=470, Left=55, Top=0, InputMask=Replicate('X',254), bHasZoom = .T.

    Proc oqueryfilter_2_2.mZoom
        This.Parent.oContained.w_queryfilter=Getfile( "VQR" )
        If !Isnull(This.Parent.oContained)
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object odescri_2_3 As StdTrsField With uid="WYUVXGPKUX",rtseq=7,rtrep=.T.,;
        cFormVar="w_descri",Value=Space(150),;
        ToolTipText = "Descrizione regola",;
        HelpContextID = 230276938,;
        cTotal = "", SpecialEffect=1, BorderStyle=0,;
        bObbl = .F. , nPag = 2, bIsInHeader=.F., bMultilanguage =  .F.,;
        bGlobalFont=.T.,;
        Height=17, Width=470, Left=55, Top=19, InputMask=Replicate('X',150)

    Add Object ocodazi_2_4 As StdTrsField With uid="ZZAHVSUYCS",rtseq=8,rtrep=.T.,;
        cFormVar="w_codazi",Value=Space(5),;
        ToolTipText = "Nome azienda su cui applicare il filtro",;
        HelpContextID = 99098698,;
        cTotal = "", SpecialEffect=1, BorderStyle=0,;
        bObbl = .F. , nPag = 2, bIsInHeader=.F., bMultilanguage =  .F.,;
        bGlobalFont=.T.,;
        Height=17, Width=62, Left=531, Top=0, InputMask=Replicate('X',5), bHasZoom = .T. , cLinkFile="cpazi", oKey_1_1="CODAZI", oKey_1_2="this.w_codazi"

    Func ocodazi_2_4.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            bRes=.link_2_4('Part',This)
        Endwith
        Return bRes
    Endfunc

    Proc ocodazi_2_4.ecpDrop(oSource)
        This.Parent.oContained.link_2_4('Drop',oSource)
        This.Parent.oContained.mCalc(.T.)
        Select (This.Parent.oContained.cTrsName)
        If I_SRV<>'A'
            Replace I_SRV With 'U'
        Endif
        This.SetFocus()
    Endproc

    Proc ocodazi_2_4.mZoom
        Private i_cWhere
        i_cWhere = ""
        Do cp_zoom With 'cpazi','*','CODAZI',cp_AbsName(This.Parent,'ocodazi_2_4'),Iif(Empty(i_cWhere),.F.,i_cWhere),'',"",'',This.Parent.oContained
    Endproc
    Add Object oLast As LastKeyMover With bHasFixedBody=.T.
    * ---
    Func ocproword_2_1.When()
        Return(.T.)
    Proc ocproword_2_1.GotFocus()
        If Inlist(This.Parent.oContained.cFunction,'Edit','Load')
            This.Parent.Parent.Parent.SetCurrentRow()
            This.Parent.oContained.SetControlsValue()
        Endif
        DoDefault()
    Proc ocproword_2_1.KeyPress(nKeyCode,nShift)
        DoDefault(nKeyCode,nShift)
        Do Case
            Case Inlist(nKeyCode,5,15)
                Nodefault
                If This.Valid()=0
                    Return
                Endif
                If This.Parent.oContained.CheckRow()
                    If Recno()>This.Parent.oContained.nFirstrow
                        This.Parent.oContained.i_lastcheckrow=This.Parent.Parent.Parent.nAbsRow
                        Thisform.LockScreen=.T.
                        Skip -1
                        This.Parent.Parent.Parent.SetFullFocus()
                    Else
                        This.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
                    Endif
                Else
                    If !Isnull(This.Parent.oContained.oNewFocus)
                        This.Parent.oContained.oNewFocus.SetFocus()
                        This.Parent.oContained.oNewFocus=.Null.
                    Endif
                Endif
            Case nKeyCode=24
                Nodefault
                If This.Valid()=0
                    Return
                Endif
                If This.Parent.oContained.CheckRow()
                    If Recno()=This.Parent.oContained.nLastRow
                        If This.Parent.oContained.FullRow() And This.Parent.oContained.CanAddRow()
                            Thisform.LockScreen=.T.
                            This.Parent.oContained.__dummy__.Enabled=.T.
                            This.Parent.oContained.__dummy__.SetFocus()
                            This.Parent.oContained.InitRow()
                            This.Parent.Parent.Parent.SetFocus()
                            This.Parent.oContained.__dummy__.Enabled=.F.
                        Else
                            This.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
                        Endif
                    Else
                        Thisform.LockScreen=.T.
                        This.Parent.oContained.i_lastcheckrow=This.Parent.Parent.Parent.nAbsRow
                        If This.Parent.Parent.Parent.RelativeRow=4
                            This.Parent.Parent.Parent.DoScroll(1)
                        Endif
                        Skip
                        This.Parent.Parent.Parent.SetFullFocus()
                    Endif
                Else
                    If !Isnull(This.Parent.oContained.oNewFocus)
                        This.Parent.oContained.oNewFocus.SetFocus()
                        This.Parent.oContained.oNewFocus=.Null.
                    Endif
                Endif
        Endcase
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_recsec','cprecsec','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +" "+i_cAliasName2+".tablename=cprecsec.tablename";
                +" and "+i_cAliasName2+".progname=cprecsec.progname";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Master/Detail"
Endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
