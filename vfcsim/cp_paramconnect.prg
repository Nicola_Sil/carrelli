* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_paramconnect                                                 *
*              Riconnessione automatica                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-05                                                      *
* Last revis.: 2009-11-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject
* --- Area Manuale = Header
* --- cp_paramconnect
If i_ServerConn[1,2]=0
    cp_ErrorMsg(cp_translate("Riconnessione automatica non disponibile per database Visual FoxPro"))
    Return
Endif
cp_ReadXdc()
* --- Fine Area Manuale
Return(Createobject("tcp_paramconnect",oParentObject))

* --- Class definition
Define Class tcp_paramconnect As StdForm
    Top    = 14
    Left   = 8

    * --- Standard Properties
    Width  = 480
    Height = 317
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2009-11-02"
    HelpContextID=215696458
    max_rt_seq=1

    * --- Constant Properties
    _IDX = 0
    cPrg = "cp_paramconnect"
    cComment = "Riconnessione automatica"
    oParentObject = .Null.
    Icon = "mask.ico"
    *closable = .f.

    * --- Local Variables
    w_RCSERIAL = Space(5)
    o_RCSERIAL = Space(5)

    * --- Children pointers
    cp_paramconnetM = .Null.
    * --- Area Manuale = Declare Variables
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=1, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        *set procedure to cp_paramconnetM additive
        With This
            .Pages(1).AddObject("oPag","tcp_paramconnectPag1","cp_paramconnect",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_translate("Pag.1")
            .Pages(1).oPag.oContained=Thisform
        Endwith
        This.Parent.oFirstControl = .Null.
        This.Parent.cComment=cp_translate(This.Parent.cComment)
        *release procedure cp_paramconnetM
        * --- Area Manuale = Init Page Frame
        * --- cp_paramconnect
        * --- Translate interface...
        This.Parent.cComment=cp_translate(MSG_PARCON_PROPERTY)
        * --- Fine Area Manuale
    Endproc
    Proc Init(oParentObject)
        If Vartype(m.oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        DoDefault()

        * --- Open tables
    Function OpenWorkTables()
        Return(This.OpenAllTables(0))

    Procedure SetPostItConn()
        Return

    Function CreateChildren()
        This.cp_paramconnetM = Createobject('stdDynamicChild',This,'cp_paramconnetM',This.oPgFrm.Page1.oPag.oLinkPC_1_2)
        This.cp_paramconnetM.createrealchild()
        Return

    Procedure DestroyChildren()
        If !Isnull(This.cp_paramconnetM)
            This.cp_paramconnetM.DestroyChildrenChain()
            This.cp_paramconnetM=.Null.
        Endif
        This.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_2')
        Return

    Function IsAChildUpdated()
        Local i_bRes
        i_bRes = This.bUpdated
        i_bRes = i_bRes .Or. This.cp_paramconnetM.IsAChildUpdated()
        Return(i_bRes)

    Procedure ChildrenNewDocument()
        This.cp_paramconnetM.NewDocument()
        Return

    Procedure SetChildrenKeys()
        With This
            This.cp_paramconnetM.SetKey(;
                .w_RCSERIAL,"rcserial";
                )
        Endwith
        Return

    Procedure ChildrenChangeRow()
        With This
            .cp_paramconnetM.ChangeRow(This.cRowID+'      1',1;
                ,.w_RCSERIAL,"rcserial";
                )
        Endwith
        Return

    Function AddSonsFilter(i_cFlt,i_oTopObject)
        Local i_f,i_fnidx,i_cDatabaseType
        With This
            If !Isnull(.cp_paramconnetM)
                i_f=.cp_paramconnetM.BuildFilter()
                If !(i_f==.cp_paramconnetM.cQueryFilter)
                    i_fnidx=.cp_paramconnetM.cFile+'_IDX'
                    i_cDatabaseType = i_ServerConn[i_TableProp[.cp_paramconnetM.&i_fnidx.,5],6]
                    i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.cp_paramconnetM.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.cp_paramconnetM.&i_fnidx.,2])+' where '+i_f+')'
                Else
                    i_f=''
                Endif
                i_f=.cp_paramconnetM.AddSonsFilter(i_f,i_oTopObject)
                If !Empty(i_f)
                    i_cFlt=i_cFlt+Iif(Empty(i_cFlt),'',' AND ')+i_f
                Endif
            Endif
        Endwith
        Return(i_cFlt)


        * --- Read record and initialize Form variables
    Procedure LoadRec()
    Endproc

    * --- ecpQuery
    Procedure ecpQuery()
        This.BlankRec()
        This.cFunction="Edit"
        This.SetStatus()
        If This.cFunction="Edit"
            This.mEnableControls()
        Endif
    Endproc
    * --- Esc
    Proc ecpQuit()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            This.cFunction = "Edit"
            This.oFirstControl.SetFocus()
            Return
        Endif
        * ---
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Endproc
    Proc QueryUnload()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            Nodefault
            Return
        Endif
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Proc ecpSave()
        If This.TerminateEdit() And This.CheckForm()
            This.Save_cp_paramconnetM(.F.)
            This.LockScreen=.T.
            This.mReplace(.T.)
            This.LockScreen=.F.
            This.NotifyEvent("Done")
            This.Release()
        Endif
    Endproc
    Func OkToQuit()
        If  This.cp_paramconnetM.IsAChildUpdated()
            Return cp_YesNo(MSG_DISCARD_CHANGES_QP)
        Endif
        Return .T.
    Endfunc
    Proc Save_cp_paramconnetM(i_ask)
        If This.cp_paramconnetM.IsAChildUpdated() And (!i_ask Or cp_YesNo(MSG_SAVE_CHANGES_QP+' (LinkPC)'))
            cp_BeginTrs()
            This.cp_paramconnetM.mReplace(.T.)
            cp_EndTrs()
        Endif
    Endproc

    * --- Blank Form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        With This
            .w_RCSERIAL=Space(5)
            .w_RCSERIAL = '00001'
            .cp_paramconnetM.NewDocument()
            .cp_paramconnetM.ChangeRow('1',1,.w_RCSERIAL,"rcserial")
            If Not(.cp_paramconnetM.bLoaded)
                .cp_paramconnetM.SetKey(.w_RCSERIAL,"rcserial")
            Endif
        Endwith
        This.SaveDependsOn()
        This.SetControlsValue()
        This.oPgFrm.Page1.oPag.oBtn_1_3.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_4.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
        This.mHideControls()
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = Setting operation
    *     Allowed Parameters: Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        * --- Deactivating List page when <> da Query
        This.cp_paramconnetM.SetStatus(i_cOp)
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *  this.cp_paramconnetM.SetChildrenStatus(i_cOp)
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt
        i_cFlt =""
        Return (i_cFlt)
    Endfunc

    Proc QueryKeySet(i_cWhere,i_cOrderBy)
    Endproc

    Proc SelectCursor
    Proc GetXKey

        * --- Update Database
    Function mReplace(i_bEditing)
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        This.NotifyEvent('Update start')
        With This
        Endwith
        This.NotifyEvent('Update end')
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(.T.)

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        If i_bUpd
            With This
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
                If .w_RCSERIAL<>.o_RCSERIAL
                    .Save_cp_paramconnetM(.T.)
                    .cp_paramconnetM.NewDocument()
                    .cp_paramconnetM.ChangeRow('1',1,.w_RCSERIAL,"rcserial")
                    If Not(.cp_paramconnetM.bLoaded)
                        .cp_paramconnetM.SetKey(.w_RCSERIAL,"rcserial")
                    Endif
                Endif
            Endwith
            This.DoRTCalc(1,1,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
        Endwith
        Return


        * --- Enable controls under condition
    Procedure mEnableControls()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        DoDefault()
        Return

        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
        Endwith
        * --- Area Manuale = Notify Event End
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

    Function SetControlsValue()
        cp_SetControlsValueExtFlds(This,'')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            *i_bRes = i_bRes .and. .cp_paramconnetM.CheckForm()
            If i_bRes
                i_bRes=  .cp_paramconnetM.CheckForm()
                If Not(i_bRes)
                    This.oPgFrm.ActivePage=1
                Endif
            Endif
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
            Else
                If Not(i_bnoChk)
                    cp_ErrorMsg(i_cErrorMsg)
                Endif
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

    * --- SaveDependsOn
    Proc SaveDependsOn()
        This.o_RCSERIAL = This.w_RCSERIAL
        * --- cp_paramconnetM : Depends On
        This.cp_paramconnetM.SaveDependsOn()
        Return

Enddefine

* --- Define pages as container
Define Class tcp_paramconnectPag1 As StdContainer
    Width  = 476
    Height = 317
    stdWidth  = 476
    stdheight = 317
    resizeXpos=387
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.


    Add Object oLinkPC_1_2 As stdDynamicChildContainer With uid="UQXJNIYWKX",Left=6, Top=2, Width=463, Height=262, bOnScreen=.T.;



    Add Object oBtn_1_3 As StdButton With uid="YCTTRQWDEB",Left=369, Top=268, Width=48,Height=45,;
        Picture="OK.BMP", Caption="", nPag=1;
        , ToolTipText = "Salva";
        , HelpContextID = 208826855;
        , Caption=MSG_SAVE;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_3.Click()
        With This.Parent.oContained
            Settaggio( This.Parent.Parent.Parent.Parent )
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_1_4 As StdButton With uid="TAPXNQTUEA",Left=421, Top=268, Width=48,Height=45,;
        Picture="ESC.BMP", Caption="", nPag=1;
        , ToolTipText = "Premere per uscire";
        , HelpContextID = 215239125;
        , Caption=MSG_CANCEL_BUTTON;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_4.Click()
        =cp_StandardFunction(This,"Quit")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_paramconnect','','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Dialog Window"
Endproc

* --- Area Manuale = Functions & Procedures
* --- cp_paramconnect
*--- Imposta le variabili globali
Procedure Settaggio (pParent)
    Local oMaster
    oMaster = pParent.cp_paramconnetM
    Public	i_DEADREQUERY ,	i_NUMDEADREQUERY,	i_DELAYDEADREQUERY ,i_CONNECTMSG , i_AUTORICONNECT , i_CONNECTLOGIN
    Public i_NUMRICONNECT ,	i_NUMRICONNECT ,	i_DELAYRICONNECT ,	i_ConnectTimeOutEndProc
    i_DEADREQUERY = (oMaster.w_RCDEADRQ='S')
    i_NUMDEADREQUERY = oMaster.w_RCNUMDRQ
    i_DELAYDEADREQUERY = oMaster.w_RCDELAYD
    i_AUTORICONNECT = (oMaster.w_RCAUTORC = 'S')
    i_NUMRICONNECT = oMaster.w_RCNUMRIC
    i_DELAYRICONNECT = oMaster.w_RCDELAYR
    i_ConnectTimeOut=oMaster.w_RCCNTIME
    i_CONNECTMSG = oMaster.w_RCCONMSG
    i_CONNECTLOGIN = ""
    oMaster = .Null.
    pParent.ecpSave()
Endproc
* --- Fine Area Manuale
