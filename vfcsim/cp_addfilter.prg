* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_addfilter                                                    *
*              Editor filtri                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-10-11                                                      *
* Last revis.: 2011-05-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- cp_addfilter
Parameters pCurName, pFltExp, pField, pParent
* --- Fine Area Manuale
Return(Createobject("tcp_addfilter"))

* --- Class definition
Define Class tcp_addfilter As StdTrsForm
    Top    = 10
    Left   = 10

    * --- Standard Properties
    Width  = 797
    Height = 274+35
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2011-05-11"
    HelpContextID=55442673
    max_rt_seq=9

    * --- Detail File Properties
    cTrsName=''

    * --- Constant Properties
    FILTERSB_IDX = 0
    cFile = "FILTERSB"
    cKeySelect = "FBCURNAM"
    cKeyWhere  = "FBCURNAM=this.w_FBCURNAM"
    cKeyDetail  = "FBCURNAM=this.w_FBCURNAM"
    cKeyWhereODBC = '"FBCURNAM="+cp_ToStrODBC(this.w_FBCURNAM)';

    cKeyDetailWhereODBC = '"FBCURNAM="+cp_ToStrODBC(this.w_FBCURNAM)';
        +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
    cKeyWhereODBCqualified = '"FILTERSB.FBCURNAM="+cp_ToStrODBC(this.w_FBCURNAM)';

    cOrderByDett = ''
    cOrderByDettCustom = ''
    bApplyOrderDetail=.T.
    bApplyOrderDetailCustom=.T.
    cOrderByDett = 'FILTERSB.CPROWNUM '
    cPrg = "cp_addfilter"
    cComment = "Editor filtri"
    i_nRowNum = 0
    w_CPROWNUM = 0
    i_nRowPerPage = 10
    Icon = "movi.ico"
    i_lastcheckrow = 0
    WindowType = 1
    MinButton = .F.
    * --- Area Manuale = Properties
    * --- Fine Area Manuale

    * --- Local Variables
    w_FBCURNAM = Space(15)
    w_FBFLDNAM = Space(254)
    w_FBCONDIT = Space(10)
    o_FBCONDIT = Space(10)
    w_FBVALFLT = Space(254)
    w_FBCONDAO = Space(3)
    w_FBOLDEXP = Space(254)
    w_FBOBJECT = Space(10)
    w_FBPARENT = .F.
    w_FBMANFLT = Space(0)
    * --- Area Manuale = Declare Variables
    * --- cp_addfilter
    pCurName = ' '
    pFltExp = ' '
    pField = ' '
    pParent = .Null.
    bQuitting = .F.

    Proc EcpQuit()
        Local i_oldFunc
        i_oldFunc = This.cFunction
        This.bQuitting = .T.
        DoDefault()
        If i_oldFunc='Load'
            This.cFunction='Query'
            This.bUpdated = .F.
            This.EcpQuit()
        Endif
    Endproc

    Proc EcpSave()
        This.NotifyEvent('SaveAndClose')
        *Per evitare msg "Abbandoni Modifiche.."
        If Pemstatus(pParent,"bSaveWhere",5)
            pParent.bSaveWhere=.T.
        Endif
        This.cFunction='Query'
        This.bUpdated = .F.
        This.bQuitting = .T.
        This.EcpQuit()
    Endproc
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPageFrame With PageCount=3, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        cp_AddExtFldsTab(This,'FILTERSB','cp_addfilter')
        StdPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tcp_addfilterPag1","cp_addfilter",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate("Editor filtro")
            .Pages(1).HelpContextID = 128351275
            .Pages(2).AddObject("oPag","tcp_addfilterPag2")
            .Pages(2).oPag.Visible=.T.
            .Pages(2).Caption=cp_Translate("Modifica manuale filtro")
            .Pages(2).HelpContextID = 146552130
        Endwith
        This.Parent.oFirstControl = .Null.
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
        Local qf
        QueryFilter(@qf)
        This.Parent.cQueryFilter=qf
        * --- Area Manuale = Init Page Frame
        * --- cp_addfilter
        This.Parent.AutoCenter=.T.

        With This.Parent
            .pCurName = pCurName
            .pFltExp = pFltExp
            .pField = pField
            .pParent = pParent
        Endwith
        * --- Fine Area Manuale
        This.Parent.Resize()
    Endproc
    Proc Init(oParentObject)
        If Vartype(m.oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        DoDefault()

        * --- Open tables
    Function OpenWorkTables()
        Dimension This.cWorkTables[1]
        This.cWorkTables[1]='FILTERSB'
        * --- Area Manuale = Open Work Table
        * --- Fine Area Manuale
        Return(This.OpenAllTables(1))

    Procedure SetPostItConn()
        This.bPostIt=i_ServerConn[i_TableProp[this.FILTERSB_IDX,5],7]
        This.nPostItConn=i_TableProp[this.FILTERSB_IDX,3]
        Return

    Procedure SetWorkFromKeySet
        * --- Initializing work variables from KeySet. They will be used to load the record.
        Select (This.cKeySet)
        With This
            .w_FBCURNAM = Nvl(FBCURNAM,Space(15))
        Endwith
    Endproc

    * --- Read record and initialize Form variables
    Procedure LoadRec()
        Local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
        Local i_cSel,i_cDatabaseType,i_nFlds
        * --- Area Manuale = Load Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        This.bUpdated=.F.
        This.bHeaderUpdated=.F.
        * --- Select reading the record
        *
        * select * from FILTERSB where FBCURNAM=KeySet.FBCURNAM
        *
        i_cOrder = ''
        If This.bApplyOrderDetail
            If This.bApplyOrderDetailCustom And Not Empty(This.cOrderByDettCustom)
                i_cOrder = 'order by '+This.cOrderByDettCustom
            Else
                If Not Empty(This.cOrderByDett)
                    i_cOrder = 'order by '+This.cOrderByDett
                Endif
            Endif
        Endif
        * --- Area Manuale = Before Load Detail
        * --- Fine Area Manuale
        If Used(This.cTrsName+'_bis')
            Select (This.cTrsName+'_bis')
            Zap
        Endif
        i_nConn = i_TableProp[this.FILTERSB_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.FILTERSB_IDX,2],This.bLoadRecFilter,This.FILTERSB_IDX,"cp_addfilter")
        If i_nConn<>0
            i_nFlds = i_dcx.getfieldscount('FILTERSB')
            i_cDatabaseType = cp_GetDatabaseType(i_nConn)
            i_cSel = "FILTERSB.*"
            i_cKey = This.cKeyWhereODBCqualified
            i_cTable = i_cTable+' FILTERSB '
            i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,This.cCursor)
            This.bLoaded = i_nRes<>-1 And Not(Eof())
        Else
            * i_cKey = this.cKeyWhere
            i_cKey = cp_PKFox(i_cTable  ,'FBCURNAM',This.w_FBCURNAM  )
            Select * From (i_cTable) FILTERSB Where &i_cKey &i_cOrder Into Cursor (This.cCursor) nofilter
            This.bLoaded = Not(Eof())
        Endif
        i_cTF = This.cCursor
        * --- Copy values in work variables
        If This.bLoaded
            With This
                .w_FBOLDEXP = .pFltExp
                .w_FBOBJECT = .pField
                .w_FBPARENT = .pParent
                .w_FBMANFLT = Space(0)
                .w_FBCURNAM = Nvl(FBCURNAM,Space(15))
                cp_LoadRecExtFlds(This,'FILTERSB')
            Endwith
            * === TEMPORARY
            Select (This.cTrsName)
            Zap
            Select (This.cCursor)
            This.i_nRowNum = 0
            Scan
                With This
                    .w_CPROWNUM = &i_cTF..CPROWNUM
                    .w_FBFLDNAM = Nvl(FBFLDNAM,Space(254))
                    .w_FBCONDIT = Nvl(FBCONDIT,Space(10))
                    .w_FBVALFLT = Nvl(FBVALFLT,Space(254))
                    .w_FBCONDAO = Nvl(FBCONDAO,Space(3))
                    Select (This.cTrsName)
                    Append Blank
                    Replace CPCCCHK With &i_cTF..CPCCCHK
                    .TrsFromWork()
                    Replace CPROWNUM With &i_cTF..CPROWNUM
                    Replace I_SRV With " "
                    .nLastRow = Recno()
                    Select (This.cCursor)
                    .i_nRowNum = Max(.i_nRowNum,CPROWNUM)
                Endwith
            Endscan
            This.nFirstrow=1
            Select (This.cCursor)
            Go Top
            Select (This.cTrsName)
            Go Top
            With This
                .oPgFrm.Page1.oPag.oBody.Refresh()
                .WorkFromTrs()
                .mCalcRowObjs()
                .SaveDependsOn()
                .SetControlsValue()
                .oPgFrm.Page1.oPag.oBtn_2_5.Enabled = .oPgFrm.Page1.oPag.oBtn_2_5.mCond()
                .oPgFrm.Page1.oPag.oBtn_2_6.Enabled = .oPgFrm.Page1.oPag.oBtn_2_6.mCond()
                .oPgFrm.Page1.oPag.oBtn_2_7.Enabled = .oPgFrm.Page1.oPag.oBtn_2_7.mCond()
                .oPgFrm.Page1.oPag.oBtn_1_5.Enabled = .oPgFrm.Page1.oPag.oBtn_1_5.mCond()
                .oPgFrm.Page1.oPag.oBtn_1_6.Enabled = .oPgFrm.Page1.oPag.oBtn_1_6.mCond()
                .oPgFrm.Page2.oPag.oBtn_4_2.Enabled = .oPgFrm.Page2.oPag.oBtn_4_2.mCond()
                .oPgFrm.Page2.oPag.oBtn_4_3.Enabled = .oPgFrm.Page2.oPag.oBtn_4_3.mCond()
                .oPgFrm.Page2.oPag.oBtn_4_4.Enabled = .oPgFrm.Page2.oPag.oBtn_4_4.mCond()
                .mHideControls()
                .ChildrenChangeRow()
                .oPgFrm.Page1.oPag.oBody.nAbsRow=1
                .oPgFrm.Page1.oPag.oBody.nRelRow=1
                .NotifyEvent('Load')
            Endwith
        Else
            This.BlankRec()
        Endif
        * --- Area Manuale = Load Record End
        * --- Fine Area Manuale
    Endproc

    * --- Blanking Form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        If Used(This.cTrsName+'_bis')
            Select (This.cTrsName+'_bis')
            Zap
        Endif
        Select (This.cTrsName)
        Zap
        Append Blank
        This.nLastRow  = Recno()
        This.nFirstrow = 1
        This.i_nRowNum = 1
        This.w_CPROWNUM = 1
        Replace CPROWNUM With This.i_nRowNum
        Replace I_SRV    With "A"
        With This
            .w_FBCURNAM=Space(15)
            .w_FBFLDNAM=Space(254)
            .w_FBCONDIT=Space(10)
            .w_FBVALFLT=Space(254)
            .w_FBCONDAO=Space(3)
            .w_FBOLDEXP=Space(254)
            .w_FBOBJECT=Space(10)
            .w_FBPARENT=.F.
            .w_FBMANFLT=Space(0)
            If .cFunction<>"Filter"
                .w_FBCURNAM = .pCurName
                .DoRTCalc(2,2,.F.)
                .w_FBCONDIT = ' '
                .w_FBVALFLT = Iif(Empty(.w_FBCONDIT), "", .w_FBVALFLT )
                .w_FBCONDAO = 'AND'
                .w_FBOLDEXP = .pFltExp
                .w_FBOBJECT = .pField
                .w_FBPARENT = .pParent
            Endif
        Endwith
        cp_BlankRecExtFlds(This,'FILTERSB')
        This.DoRTCalc(9,9,.F.)
        This.SaveDependsOn()
        This.SetControlsValue()
        This.TrsFromWork()
        This.oPgFrm.Page1.oPag.oBtn_2_5.Enabled = This.oPgFrm.Page1.oPag.oBtn_2_5.mCond()
        This.oPgFrm.Page1.oPag.oBtn_2_6.Enabled = This.oPgFrm.Page1.oPag.oBtn_2_6.mCond()
        This.oPgFrm.Page1.oPag.oBtn_2_7.Enabled = This.oPgFrm.Page1.oPag.oBtn_2_7.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_5.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_6.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
        This.oPgFrm.Page2.oPag.oBtn_4_2.Enabled = This.oPgFrm.Page2.oPag.oBtn_4_2.mCond()
        This.oPgFrm.Page2.oPag.oBtn_4_3.Enabled = This.oPgFrm.Page2.oPag.oBtn_4_3.mCond()
        This.oPgFrm.Page2.oPag.oBtn_4_4.Enabled = This.oPgFrm.Page2.oPag.oBtn_4_4.mCond()
        This.mHideControls()
        This.ChildrenChangeRow()
        This.oPgFrm.Page1.oPag.oBody.nAbsRow=0
        This.oPgFrm.Page1.oPag.oBody.nRelRow=1
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- cp_addfilter
        If Not This.bQuitting
            This.EcpLoad()
        Endif
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = Setting operation
    *     Allowed Parameters: Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        Local i_bVal
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        i_bVal = i_cOp<>"Query"
        * --- Disabling List page when <> from Query
        If Type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
            This.oPgFrm.Pages(This.oPgFrm.PageCount).Enabled=Not(i_bVal)
        Else
            Local i_nPageCount
            For i_nPageCount=This.oPgFrm.PageCount To 1 Step -1
                If Type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
                    This.oPgFrm.Pages(i_nPageCount).Enabled=Not(i_bVal)
                    Exit
                Endif
            Next
        Endif
        With This.oPgFrm
            .Page2.oPag.oFBMANFLT_4_1.Enabled = i_bVal
            .Page1.oPag.oBtn_2_5.Enabled = .Page1.oPag.oBtn_2_5.mCond()
            .Page1.oPag.oBtn_2_6.Enabled = .Page1.oPag.oBtn_2_6.mCond()
            .Page1.oPag.oBtn_2_7.Enabled = .Page1.oPag.oBtn_2_7.mCond()
            .Page1.oPag.oBtn_1_5.Enabled = .Page1.oPag.oBtn_1_5.mCond()
            .Page1.oPag.oBtn_1_6.Enabled = .Page1.oPag.oBtn_1_6.mCond()
            .Page2.oPag.oBtn_4_2.Enabled = .Page2.oPag.oBtn_4_2.mCond()
            .Page2.oPag.oBtn_4_3.Enabled = .Page2.oPag.oBtn_4_3.mCond()
            .Page2.oPag.oBtn_4_4.Enabled = .Page2.oPag.oBtn_4_4.mCond()
            .Page1.oPag.oBody.Enabled = .T.
            .Page1.oPag.oBody.oBodyCol.Enabled = i_bVal
        Endwith
        cp_SetEnabledExtFlds(This,'FILTERSB',i_cOp)
        * --- Area Manuale = Enable Controls End
        * --- cp_addfilter
        * disabilito il tab elenco
        This.oPgFrm.Pages(This.oPgFrm.PageCount).Enabled=.F.
        This.oPgFrm.Pages(This.oPgFrm.PageCount).Caption=""
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate  filter
    Func BuildFilter()
        Local i_cFlt,i_nConn
        i_nConn = i_TableProp[this.FILTERSB_IDX,3]
        i_cFlt = This.cQueryFilter
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_FBCURNAM,"FBCURNAM",i_nConn)
        * --- Area Manuale = Build Filter
        * --- Fine Area Manuale
        Return (i_cFlt)
    Endfunc

    * --- Creating cKeySet cursor with primary key only
    Proc QueryKeySet(i_cWhere,i_cOrderBy)
        Local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
        This.cLastWhere = i_cWhere
        This.cLastOrderBy = i_cOrderBy
        i_cWhere = Iif(Not(Empty(i_cWhere)),' where '+i_cWhere,'')
        i_cOrderBy = Iif(Not(Empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
        i_cKey = This.cKeySelect
        * --- Area Manuale = Query Key Set Init
        * --- Fine Area Manuale
        i_nConn = i_TableProp[this.FILTERSB_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.FILTERSB_IDX,2])
        i_lTable = "FILTERSB"
        i_cDatabaseType = i_ServerConn[i_TableProp[this.FILTERSB_IDX,5],6]
        If i_nConn<>0
            Local i_oldzr
            If Vartype(i_nZoomMaxRows)='N'
                i_oldzr=i_nZoomMaxRows
                i_nZoomMaxRows=10000
            Endif
            If !Empty(i_cOrderBy) And At(Substr(i_cOrderBy,11),i_cKey)=0
                i_cKey=i_cKey+','+Substr(i_cOrderBy,11)
            Endif
            Thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,This.cKeySet,i_cDatabaseType)
            If Vartype(i_nZoomMaxRows)='N'
                i_nZoomMaxRows=i_oldzr
            Endif
        Else
            Select Distinct &i_cKey From (i_cTable) As (i_lTable) &i_cWhere &i_cOrderBy Into Cursor (This.cKeySet)
            Locate For 1=1 && because cursors reuse main file
        Endif
        * --- Area Manuale = Query Key Set End
        * --- Fine Area Manuale
    Endproc

    *
    *  --- Transaction procedures
    *
    Proc CreateTrs
        This.cTrsName=Sys(2015)
        Create Cursor (This.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
            ,t_FBFLDNAM C(254);
            ,t_FBCONDIT N(3);
            ,t_FBVALFLT C(254);
            ,t_FBCONDAO N(3);
            ,CPROWNUM N(10);
            )
        This.oPgFrm.Page1.oPag.oBody.ColumnCount=0
        This.oPgFrm.Page1.oPag.oBody.RecordSource=This.cTrsName
        This.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tcp_addfilterbodyrow')
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.Width=This.oPgFrm.Page1.oPag.oBody.Width
        This.oPgFrm.Page1.oPag.oBody.RowHeight=This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.Height
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.F.
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.Visible=.T.
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=This
        * --- Row charateristics
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBFLDNAM_2_1.ControlSource=This.cTrsName+'.t_FBFLDNAM'
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDIT_2_2.ControlSource=This.cTrsName+'.t_FBCONDIT'
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBVALFLT_2_3.ControlSource=This.cTrsName+'.t_FBVALFLT'
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDAO_2_4.ControlSource=This.cTrsName+'.t_FBCONDAO'
        This.oPgFrm.Page1.oPag.oBody.ZOrder(1)
        This.oPgFrm.Page1.oPag.oBody3D.ZOrder(1)
        This.oPgFrm.Page1.oPag.pagebmp.ZOrder(1)
        This.AddVLine(288)
        This.AddVLine(376)
        This.AddVLine(707)
        This.oPgFrm.Page1.oPag.oBody.oFirstControl=This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBFLDNAM_2_1
        * --- New table already open in exclusive mode
        * --- Area Manuale = Create Trs
        * --- Fine Area Manuale

    Function mInsert()
        Local i_nConn,i_cTable,i_extfld,i_extval
        * --- Area Manuale = Insert Master Init
        * --- Fine Area Manuale
        i_nConn = i_TableProp[this.FILTERSB_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.FILTERSB_IDX,2])
        * --- Area Manuale = Autonum Assigned
        * --- Fine Area Manuale
        * --- Area Manuale = Insert Master End
        * --- Fine Area Manuale
        Return(Not(bTrsErr))

        * --- Insert new Record in Detail table
    Function mInsertDetail(i_nCntLine)
        Local i_cKey,i_nConn,i_cTable,i_TN
        * --- Area Manuale = Insert Detail Init
        * --- Fine Area Manuale
        If This.bUpdated .Or. This.IsAChildUpdated()
            i_nConn = i_TableProp[this.FILTERSB_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.FILTERSB_IDX,2])
            *
            * insert into FILTERSB
            *
            i_TN = This.cTrsName
            This.NotifyEvent('Insert row start')
            Local i_cFldBody,i_cFldValBody
            If i_nConn<>0
                i_extfld=cp_InsertFldODBCExtFlds(This,'FILTERSB')
                i_extval=cp_InsertValODBCExtFlds(This,'FILTERSB')
                i_cFldBody=" "+;
                    "(FBCURNAM,FBFLDNAM,FBCONDIT,FBVALFLT,FBCONDAO,CPROWNUM,CPCCCHK"+i_extfld+")"
                i_cFldValBody=" "+;
                    "("+cp_ToStrODBC(This.w_FBCURNAM)+","+cp_ToStrODBC(This.w_FBFLDNAM)+","+cp_ToStrODBC(This.w_FBCONDIT)+","+cp_ToStrODBC(This.w_FBVALFLT)+","+cp_ToStrODBC(This.w_FBCONDAO)+;
                    ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
                =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
            Else
                i_extfld=cp_InsertFldVFPExtFlds(This,'FILTERSB')
                i_extval=cp_InsertValVFPExtFlds(This,'FILTERSB')
                cp_CheckDeletedKey(i_cTable,i_nCntLine,'FBCURNAM',This.w_FBCURNAM)
                Insert Into (i_cTable) (;
                    FBCURNAM;
                    ,FBFLDNAM;
                    ,FBCONDIT;
                    ,FBVALFLT;
                    ,FBCONDAO;
                    ,CPROWNUM,CPCCCHK &i_extfld.) Values (;
                    this.w_FBCURNAM;
                    ,This.w_FBFLDNAM;
                    ,This.w_FBCONDIT;
                    ,This.w_FBVALFLT;
                    ,This.w_FBCONDAO;
                    ,i_nCntLine,cp_NewCCChk() &i_extval. )
            Endif
            This.NotifyEvent('Insert row end')
        Endif
        * --- Area Manuale = Insert Detail End
        * --- Fine Area Manuale
        Return(Not(bTrsErr))

        * --- Update Database
    Function mReplace(i_bEditing)
        Local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
        Local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        If This.bUpdated
            i_nConn = i_TableProp[this.FILTERSB_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.FILTERSB_IDX,2])
            i_nModRow = 1
            If i_bEditing .And. This.bHeaderUpdated
                This.mRestoreTrs()
            Endif
            If This.bHeaderUpdated
                This.mUpdateTrs()
            Endif
            If This.bHeaderUpdated And i_bEditing
                Select (This.cTrsName)
                i_TN = This.cTrsName
                i_bUpdAll = .F.
                Go Top
                If i_bEditing And I_SRV<>'A'
                Endif
                If Not(i_bUpdAll)
                    Scan For (Not(Empty(t_FBFLDNAM))) And (I_SRV<>"U" And I_SRV<>"A")
                        i_OldCCCHK=Iif(i_bEditing,&i_TN..CPCCCHK,'')
                        * --- Updating Master table
                        This.NotifyEvent('Update start')
                        If i_nConn<>0
                            i_extfld=cp_ReplaceODBCExtFlds(This,'FILTERSB')
                            i_cWhere = This.cKeyWhereODBC
                            i_nnn="UPDATE "+i_cTable+" SET "+;
                                "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                                " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                                " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                            i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
                        Else
                            i_extfld=cp_ReplaceVFPExtFlds(This,'FILTERSB')
                            i_cWhere = This.cKeyWhere
                            Update (i_cTable) Set ;
                                CPCCCHK=cp_NewCCChk() &i_extfld. Where &i_cWhere;
                                and CPROWNUM=&i_TN.->CPROWNUM;
                                and CPCCCHK==i_OldCCCHK
                            i_nModRow=_Tally
                        Endif
                        This.NotifyEvent('Update end')
                        If i_nModRow<1
                            Exit
                        Endif
                    Endscan
                    * --- Make the last record the actual position (mcalc problem with double transitory)
                    This.SetRow(Reccount(This.cTrsName), .F.)
                    This.SetControlsValue()
                Endif
            Endif

            * --- Update Detail table
            If i_nModRow>0   && Master table updated with success
                Set Delete Off
                Select (This.cTrsName)
                i_TN = This.cTrsName
                i_bUpdAll = .F.
                Scan For (Not(Empty(t_FBFLDNAM))) And ((I_SRV="U" Or i_bUpdAll) Or I_SRV="A" Or Deleted())
                    This.WorkFromTrs()
                    This.oPgFrm.Page1.oPag.oBody.nAbsRow=Recno()
                    i_OldCCCHK=Iif(i_bEditing.Or.I_SRV<>"A",&i_TN..CPCCCHK,'')
                    i_nRec = Recno()
                    Set Delete On
                    If Deleted()
                        If I_SRV<>"A"
                            * --- Delete from database an erased row
                            This.NotifyEvent('Delete row start')
                            This.mRestoreTrsDetail()
                            If i_nConn<>0
                                i_cWhere = This.cKeyWhereODBC
                                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                                    " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                                    " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
                            Else
                                i_cWhere = This.cKeyWhere
                                Delete From (i_cTable) Where &i_cWhere And CPROWNUM=&i_TN.->CPROWNUM;
                                    and CPCCCHK==i_OldCCCHK
                                i_nModRow=_Tally
                            Endif
                            This.NotifyEvent('Delete row end')
                        Endif
                    Else
                        If I_SRV="A"
                            * --- Insert new row in database
                            This.mUpdateTrsDetail()
                            i_NR = CPROWNUM
                            =This.mInsertDetail(i_NR)
                        Else
                            * --- Area Manuale = Replace Loop
                            * --- Fine Area Manuale
                            *
                            * update FILTERSB
                            *
                            This.NotifyEvent('Update row start')
                            This.mRestoreTrsDetail()
                            This.mUpdateTrsDetail()
                            If i_nConn<>0
                                i_extfld=cp_ReplaceODBCExtFlds(This,'FILTERSB')
                                i_cWhere = This.cKeyWhereODBC
                                i_nnn="UPDATE "+i_cTable+" SET "+;
                                    " FBFLDNAM="+cp_ToStrODBC(This.w_FBFLDNAM)+;
                                    ",FBCONDIT="+cp_ToStrODBC(This.w_FBCONDIT)+;
                                    ",FBVALFLT="+cp_ToStrODBC(This.w_FBVALFLT)+;
                                    ",FBCONDAO="+cp_ToStrODBC(This.w_FBCONDAO)+;
                                    ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                                    " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                                    " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
                            Else
                                i_extfld=cp_ReplaceVFPExtFlds(This,'FILTERSB')
                                i_cWhere = This.cKeyWhere
                                Update (i_cTable) Set ;
                                    FBFLDNAM=This.w_FBFLDNAM;
                                    ,FBCONDIT=This.w_FBCONDIT;
                                    ,FBVALFLT=This.w_FBVALFLT;
                                    ,FBCONDAO=This.w_FBCONDAO;
                                    ,CPCCCHK=cp_NewCCChk() &i_extfld. Where &i_cWhere And CPROWNUM=&i_TN.->CPROWNUM;
                                    and CPCCCHK==i_OldCCCHK
                                i_nModRow=_Tally
                            Endif
                            This.NotifyEvent('Update row end')
                        Endif
                    Endif
                    If i_nModRow<1
                        Exit
                    Endif
                    Set Delete Off
                Endscan
                Set Delete On
                * --- Make the last record the actual position (mcalc problem with double transitory)
                This.SetRow(Reccount(This.cTrsName), .F.)
                This.SetControlsValue()
            Endif
            =cp_CheckMultiuser(i_nModRow)
        Endif
        If Not(bTrsErr)
        Endif
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(bTrsErr)

        * --- Delete Records
    Function mDelete()
        Local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
        Local i_cDel,i_nRec
        * --- Area Manuale = Delete Init
        * --- Fine Area Manuale
        If Not(bTrsErr)
            i_nConn = i_TableProp[this.FILTERSB_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.FILTERSB_IDX,2])
            This.NotifyEvent("Delete Start")
            Select (This.cTrsName)
            i_TN = This.cTrsName
            i_nModRow = 1
            i_cDel = Set('DELETED')
            Set Delete Off
            Scan For (Not(Empty(t_FBFLDNAM))) And I_SRV<>'A'
                This.WorkFromTrs()
                i_OldCCCHK=&i_TN..CPCCCHK
                *
                * delete FILTERSB
                *
                This.NotifyEvent('Delete row start')
                If i_nConn<>0
                    i_cWhere = This.cKeyWhereODBC
                    i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                        " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                        " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
                Else
                    i_cWhere = This.cKeyWhere
                    Delete From (i_cTable) Where &i_cWhere And CPROWNUM=&i_TN.->CPROWNUM;
                        and CPCCCHK==i_OldCCCHK
                    i_nModRow=_Tally
                Endif
                This.NotifyEvent('Delete row end')
                If i_nModRow<1
                    Exit
                Endif
            Endscan
            Set Delete &i_cDel
            * --- Make the last record the actual position (mcalc problem with double transitory)
            This.SetRow(Reccount(This.cTrsName), .F.)
            This.SetControlsValue()
            =cp_CheckMultiuser(i_nModRow)
            If Not(bTrsErr)
                This.mRestoreTrs()
                Select (This.cTrsName)
                i_TN = This.cTrsName
                i_cDel = Set('DELETED')
                Set Delete Off
                Scan For (Not(Empty(t_FBFLDNAM))) And I_SRV<>'A'
                    i_OldCCCHK=&i_TN..CPCCCHK
                    This.mRestoreTrsDetail()
                Endscan
                Set Delete &i_cDel
                * --- Make the last record the actual position (mcalc problem with double transitory)
                This.SetRow(Reccount(This.cTrsName), .F.)
                This.SetControlsValue()
            Endif
            This.NotifyEvent("Delete End")
        Endif
        This.mDeleteWarnings()
        * --- Area Manuale = Delete End
        * --- Fine Area Manuale
        Return

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        Local i_cTable,i_nConn
        i_nConn = i_TableProp[this.FILTERSB_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.FILTERSB_IDX,2])
        If i_bUpd
            With This
                .DoRTCalc(1,3,.T.)
                If .o_FBCONDIT<>.w_FBCONDIT
                    .w_FBVALFLT = Iif(Empty(.w_FBCONDIT), "", .w_FBVALFLT )
                Endif
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
            Endwith
            This.DoRTCalc(5,9,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
        Endwith
        Return

    Proc mCalcRowObjs()
        With This
        Endwith
        Return
    Proc Calculate_LRHIKWJFWE()
        With This
            * --- Trasforma il filtro da parametro a righe e inizializza tabella temporanea zoom
            cp_AddFilterR(This;
                ,'NEW_RECORD';
                )
        Endwith
    Endproc
    Proc Calculate_WOIVNPSMAC()
        With This
            * --- Elimina tabella temporanea
            cp_AddFilterR(This;
                ,"CLOSE_MASK";
                )
        Endwith
    Endproc
    Proc Calculate_FUOAQULCGV()
        With This
            * --- Salvataggio delle modifiche nell'oggetto della gestione chiamante
            cp_AddFilterR(This;
                ,"SAVEANDCLS";
                )
        Endwith
    Endproc
    Proc Calculate_JBCOLAHMPQ()
        With This
            * --- Riporta le righe dell'editor nel campo per le modifiche manuali
            cp_AddFilterR(This;
                ,"EDITOR2MAN";
                )
        Endwith
    Endproc

    * --- Enable controls under condition
    Procedure mEnableControls()
        This.mEnableControlsFixed()
        This.mHideControls()
        DoDefault()
        Return

        * --- Enable controls under condition for Grid and for FixedPos fields
    Procedure mEnableControlsFixed()
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBVALFLT_2_3.Enabled = Inlist(This.cFunction,"Edit","Load") And This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBVALFLT_2_3.mCond()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        Local i_show1
        i_show1=Not(1=2 Or 1=3)
        This.oPgFrm.Pages(2).Enabled=i_show1
        This.oPgFrm.Pages(2).Caption=Iif(i_show1,cp_Translate("Modifica manuale filtro"),"")
        This.oPgFrm.Pages(2).oPag.Visible=This.oPgFrm.Pages(2).Enabled
        This.mHideRowControls()
        DoDefault()
        Return

    Procedure mHideRowControls()
        DoDefault()
        Return


        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            If Lower(cEvent)==Lower("New record")
                .Calculate_LRHIKWJFWE()
                bRefresh=.T.
            Endif
            If Lower(cEvent)==Lower("Done")
                .Calculate_WOIVNPSMAC()
                bRefresh=.T.
            Endif
            If Lower(cEvent)==Lower("SaveAndClose")
                .Calculate_FUOAQULCGV()
                bRefresh=.T.
            Endif
            If Lower(cEvent)==Lower("ActivatePage 2")
                .Calculate_JBCOLAHMPQ()
                bRefresh=.T.
            Endif
        Endwith
        * --- Area Manuale = Notify Event End
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

    Function SetControlsValue()
        Select (This.cTrsName)
        If Not(This.oPgFrm.Page2.oPag.oFBMANFLT_4_1.Value==This.w_FBMANFLT)
            This.oPgFrm.Page2.oPag.oFBMANFLT_4_1.Value=This.w_FBMANFLT
        Endif
        If Not(This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBFLDNAM_2_1.Value==This.w_FBFLDNAM)
            This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBFLDNAM_2_1.Value=This.w_FBFLDNAM
            Replace t_FBFLDNAM With This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBFLDNAM_2_1.Value
        Endif
        If Not(This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDIT_2_2.RadioValue()==This.w_FBCONDIT)
            This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDIT_2_2.SetRadio()
            Replace t_FBCONDIT With This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDIT_2_2.Value
        Endif
        If Not(This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBVALFLT_2_3.Value==This.w_FBVALFLT)
            This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBVALFLT_2_3.Value=This.w_FBVALFLT
            Replace t_FBVALFLT With This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBVALFLT_2_3.Value
        Endif
        If Not(This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDAO_2_4.RadioValue()==This.w_FBCONDAO)
            This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDAO_2_4.SetRadio()
            Replace t_FBCONDAO With This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDAO_2_4.Value
        Endif
        cp_SetControlsValueExtFlds(This,'FILTERSB')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            If .bUpdated
                Do Case
                    Otherwise
                        i_bRes = .CheckRow()
                        If .Not. i_bRes
                            If !Isnull(.oNewFocus)
                                If .oPgFrm.ActivePage>1
                                    .oPgFrm.ActivePage=1
                                Endif
                                .oNewFocus.SetFocus()
                                .oNewFocus=.Null.
                            Endif
                        Endif
                        * return(i_bRes)
                Endcase
            Endif
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
                Return(i_bRes)
            Endif
            If Not(i_bnoChk)
                cp_ErrorMsg(i_cErrorMsg)
                Return(i_bRes)
            Endif
            Local i_oldarea
            i_oldarea=Select()
            Select Count(*) As Cnt From (This.cTrsName);
                where Not(Deleted()) And (Not(Empty(t_FBFLDNAM)));
                into Cursor __chk__
            If Not(Max(0,1)<=Cnt)
                Do cp_ErrorMsg With MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
                Use
                Select (i_oldarea)
                Return(.F.)
            Endif
            Use
            Select (i_oldarea)
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

    * --- CheckRow
    Func CheckRow()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            If Not(Empty(.w_FBFLDNAM))
                * --- Area Manuale = Check Row
                * --- Fine Area Manuale
            Else
                If This.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 And This.oPgFrm.Page1.oPag.oBody.nAbsRow<>This.nLastRow And (Type('i_bCheckEmptyRows')='U' Or i_bCheckEmptyRows)
                    i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
                    i_bRes=.F.
                    i_bnoChk=.F.
                Endif
            Endif
            If Not(i_bnoChk)
                Do cp_ErrorMsg With i_cErrorMsg
                Return(i_bRes)
            Endif
            If Not(i_bnoObbl)
                Do cp_ErrorMsg With MSG_FIELD_CANNOT_BE_NULL_QM
                Return(i_bRes)
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

    * --- SaveDependsOn
    Proc SaveDependsOn()
        This.o_FBCONDIT = This.w_FBCONDIT
        Return


        * --- FullRow
    Func FullRow()
        Local i_bRes,i_nArea
        i_nArea=Select()
        Select (This.cTrsName)
        i_bRes=(Not(Empty(t_FBFLDNAM)))
        Select(i_nArea)
        Return(i_bRes)
    Endfunc

    * --- InitRow
    Proc InitRow()
        This.NotifyEvent("Before Init Row")
        Select (This.cTrsName)
        Append Blank
        This.nLastRow = Recno()
        This.i_nRowNum = This.i_nRowNum+1
        This.w_CPROWNUM = This.i_nRowNum
        Replace CPROWNUM With This.i_nRowNum
        Replace I_SRV    With "A"
        Replace I_RECNO  With Recno()
        With This
            .w_FBFLDNAM=Space(254)
            .w_FBCONDIT=Space(10)
            .w_FBVALFLT=Space(254)
            .w_FBCONDAO=Space(3)
            .DoRTCalc(1,2,.F.)
            .w_FBCONDIT = ' '
            .w_FBVALFLT = Iif(Empty(.w_FBCONDIT), "", .w_FBVALFLT )
            .w_FBCONDAO = 'AND'
        Endwith
        This.DoRTCalc(6,9,.F.)
        This.oPgFrm.Page1.oPag.oBody.nAbsRow=Recno()
        This.TrsFromWork()
        This.ChildrenChangeRow()
        This.NotifyEvent("Init Row")
    Endproc

    * --- WorkFromTrs
    Proc WorkFromTrs()
        Select (This.cTrsName)
        This.w_FBFLDNAM = t_FBFLDNAM
        This.w_FBCONDIT = This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDIT_2_2.RadioValue(.T.)
        This.w_FBVALFLT = t_FBVALFLT
        This.w_FBCONDAO = This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDAO_2_4.RadioValue(.T.)
        This.w_CPROWNUM = CPROWNUM
        This.oPgFrm.Page1.oPag.oBody.nAbsRow=Recno()
    Endproc

    * --- TrsFromWork
    Proc TrsFromWork()
        Select (This.cTrsName)
        Replace I_RECNO With Recno()
        Replace t_FBFLDNAM With This.w_FBFLDNAM
        Replace t_FBCONDIT With This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDIT_2_2.ToRadio()
        Replace t_FBVALFLT With This.w_FBVALFLT
        Replace t_FBCONDAO With This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oFBCONDAO_2_4.ToRadio()
    Endproc

    * --- SubtractTotals
    Proc SubtractTotals()
    Endproc
Enddefine

* --- Define pages as container
Define Class tcp_addfilterPag1 As StdContainer
    Width  = 793
    Height = 274
    stdWidth  = 793
    stdheight = 274
    resizeXpos=484
    resizeYpos=155
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.


    Add Object oBtn_1_5 As StdButton With uid="ZGAJYUAGXG",Left=672, Top=226, Width=48,Height=45,;
        Picture="BMP\OK.BMP", Caption="", nPag=1;
        , ToolTipText = "Premere per salvare le modifiche effettuate";
        , HelpContextID = 54593941;
        , Caption='Sa\<lva';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_5.Click()
        =cp_StandardFunction(This,"Save")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.bHeaderUpdated=.T.
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_1_6 As StdButton With uid="XOOSNRSXLS",Left=724, Top=226, Width=48,Height=45,;
        Picture="BMP\ESC.BMP", Caption="", nPag=1;
        , ToolTipText = "Premere per annullare le modifiche effettuate";
        , HelpContextID = 254248516;
        , Caption='\<Esci';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_6.Click()
        =cp_StandardFunction(This,"Quit")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.bHeaderUpdated=.T.
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object oStr_1_10 As StdString With uid="EEXUMZINQJ",Visible=.T., Left=15, Top=12,;
        Alignment=0, Width=75, Height=18,;
        Caption="Nome campo"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_11 As StdString With uid="GGXKTMPLVJ",Visible=.T., Left=289, Top=12,;
        Alignment=2, Width=85, Height=18,;
        Caption="Condizione"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_12 As StdString With uid="DYQCLQLFIH",Visible=.T., Left=387, Top=12,;
        Alignment=0, Width=306, Height=18,;
        Caption="Valore"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_13 As StdString With uid="BWFCGECYUW",Visible=.T., Left=708, Top=12,;
        Alignment=2, Width=75, Height=18,;
        Caption="And/Or"  ;
        , bGlobalFont=.T.

    Add Object oBox_1_14 As StdBox With uid="JXSELCIQPQ",Left=7, Top=10, Width=781,Height=20

    Add Object oBox_1_15 As StdBox With uid="LLKZILFGPW",Left=288, Top=11, Width=1,Height=18

    Add Object oBox_1_16 As StdBox With uid="SHPYZMTITJ",Left=376, Top=10, Width=1,Height=18

    Add Object oBox_1_17 As StdBox With uid="SBJXHIXCVZ",Left=707, Top=10, Width=1,Height=18

    Add Object oEndHeader As BodyKeyMover With nDirection=1
    *
    * --- BODY transaction
    *
    Add Object oBody3D As Shape With Left=-1,Top=28,;
        width=776+Sysmetric(5),Height=Int(Fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,SpecialEffect=0

    Add Object oBody As StdBody Noinit With ;
        left=0,Top=29,Width=775+Sysmetric(5),Height=Int(Fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,ColumnCount=0,GridLines=1,;
        HeaderHeight=0,DeleteMark=.F.,ScrollBars=2,Enabled=.F.,;
        cLinkFile=''

    Proc oBody.SetCurrentRow()
        Thisform.LockScreen=.T.
        Select (This.Parent.oContained.cTrsName)
        If Recno()<>This.nAbsRow
            If This.nAbsRow<>0 And Inlist(This.Parent.oContained.cFunction,'Edit','Load')
                If This.nAbsRow<>This.Parent.oContained.i_lastcheckrow And !This.Parent.oContained.CheckRow()
                    This.Parent.oContained.i_lastcheckrow=0
                    This.Parent.oContained.__dummy__.Enabled=.T.
                    This.Parent.oContained.__dummy__.SetFocus()
                    Select (This.Parent.oContained.cTrsName)
                    Go (This.nAbsRow)
                    This.SetFullFocus()
                    If !Isnull(This.Parent.oContained.oNewFocus)
                        This.Parent.oContained.oNewFocus.SetFocus()
                        This.Parent.oContained.oNewFocus=.Null.
                    Endif
                    This.Parent.oContained.__dummy__.Enabled=.F.
                    Thisform.LockScreen=.F.
                    Return
                Endif
            Endif
            This.nAbsRow=Recno()
            This.Parent.oContained.WorkFromTrs()
            This.Parent.oContained.mEnableControlsFixed()
            This.Parent.oContained.mCalcRowObjs()
            * --- Area Manuale = Set Current Row
            * --- Fine Area Manuale
        Else
            This.Parent.oContained.mEnableControlsFixed()
        Endif
        If !Isnull(This.Parent.oContained.oNewFocus)
            This.Parent.oContained.oNewFocus.SetFocus()
            This.Parent.oContained.oNewFocus=.Null.
        Endif
        If This.RelativeRow<>0
            This.nRelRow=This.RelativeRow
        Endif
        If This.nBeforeAfter>0 && and Version(5)<700
            This.nBeforeAfter=This.nBeforeAfter-1
        Endif
        Thisform.LockScreen=.F.
        This.Parent.oContained.i_lastcheckrow=0
    Endproc
    Func oBody.GetDropTarget(cFile,nX,nY)
        Local oDropInto
        oDropInto=.Null.
        Do Case
        Endcase
        Return(oDropInto)
    Endfunc

    Add Object oBeginFixedBody As FixedBodyKeyMover With nDirection=1


    Add Object oBtn_2_5 As StdButton With uid="EAGCZXLWAI",Width=48,Height=45,;
        left=6, Top=226,;
        Picture="UP.BMP", Caption="", nPag=2;
        , ToolTipText = "Premere per spostare in alto di una riga la condizione selezionata";
        , HelpContextID = 121701135;
        , Caption='\<Su';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_2_5.Click()
        With This.Parent.oContained
            cp_AddFilterR(This.Parent.oContained,"FILTER_UP")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            Select (This.Parent.oContained.cTrsName)
            If I_SRV=" "
                Replace I_SRV With "U"
            Endif
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object oBtn_2_6 As StdButton With uid="CRHSFBPJWF",Width=48,Height=45,;
        left=60, Top=226,;
        Picture="DOWN.BMP", Caption="", nPag=2;
        , ToolTipText = "Premere per spostare in basso di una riga la condizione selezionata";
        , HelpContextID = 92208369;
        , Caption='\<Gi�';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_2_6.Click()
        With This.Parent.oContained
            cp_AddFilterR(This.Parent.oContained,"FILTERDOWN")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            Select (This.Parent.oContained.cTrsName)
            If I_SRV=" "
                Replace I_SRV With "U"
            Endif
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object oBtn_2_7 As StdButton With uid="LEHLVRFBAF",Width=48,Height=45,;
        left=114, Top=226,;
        Picture="BMP\REQUERY.BMP", Caption="", nPag=2;
        , ToolTipText = "Premere per testare il filtro";
        , HelpContextID = 145685629;
        , Caption='\<Test';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_2_7.Click()
        With This.Parent.oContained
            cp_AddFilterR(This.Parent.oContained,"FILTERTEST")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            Select (This.Parent.oContained.cTrsName)
            If I_SRV=" "
                Replace I_SRV With "U"
            Endif
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object oEndFixedBody As FixedBodyKeyMover With nDirection=-1
    Add Object oBeginFooter As BodyKeyMover With nDirection=-1
Enddefine

Define Class tcp_addfilterPag2 As StdContainer
    Width  = 793
    Height = 274
    stdWidth  = 793
    stdheight = 274
    resizeXpos=594
    resizeYpos=184
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.

    Add Object oFBMANFLT_4_1 As StdMemo With uid="XKVOFEBLAU",rtseq=9,rtrep=.F.,;
        cFormVar = "w_FBMANFLT", cQueryName = "FBMANFLT",;
        bObbl = .F. , nPag = 4, Value=Space(0), bMultilanguage =  .F.,;
        HelpContextID = 63573552,;
        bGlobalFont=.T.,;
        Height=215, Width=783, Left=6, Top=8


    Add Object oBtn_4_2 As StdButton With uid="IDGCXLPLNB",Left=9, Top=226, Width=48,Height=45,;
        Picture="BMP\OK.BMP", Caption="", nPag=4;
        , ToolTipText = "Premere per importare il filtro modificato manualmente nelle righe dell'editor";
        , HelpContextID = 178160525;
        , Caption='\<Editor';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_4_2.Click()
        With This.Parent.oContained
            cp_AddFilterR(This.Parent.oContained,"MAN2EDITOR")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.bHeaderUpdated=.T.
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_4_3 As StdButton With uid="FWJFZEEEGM",Left=672, Top=226, Width=48,Height=45,;
        Picture="BMP\OK.BMP", Caption="", nPag=4;
        , ToolTipText = "Premere per salvare le modifiche effettuate";
        , HelpContextID = 54593941;
        , Caption='Sa\<lva';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_4_3.Click()
        =cp_StandardFunction(This,"Save")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.bHeaderUpdated=.T.
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_4_4 As StdButton With uid="LFCTXPRHIY",Left=724, Top=226, Width=48,Height=45,;
        Picture="BMP\ESC.BMP", Caption="", nPag=4;
        , ToolTipText = "Premere per annullare le modifiche effettuate";
        , HelpContextID = 254248516;
        , Caption='\<Esci';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_4_4.Click()
        =cp_StandardFunction(This,"Quit")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.bHeaderUpdated=.T.
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc
Enddefine

* --- Defining Body row
Define Class tcp_addfilterBodyRow As Container
    Width=766
    Height=Int(Fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
    BackStyle=0                                                && 0=trasparente
    BorderWidth=0                                              && Spessore Bordo
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    oContained = .Null.

    Add Object oFBFLDNAM_2_1 As StdTrsField With uid="FOKQNQXLHB",rtseq=2,rtrep=.T.,;
        cFormVar="w_FBFLDNAM",Value=Space(254),;
        HelpContextID = 205363813,;
        cTotal = "", SpecialEffect=1, BorderStyle=0,;
        bObbl = .F. , nPag = 2, bIsInHeader=.F., bMultilanguage =  .F.,;
        bGlobalFont=.T.,;
        Height=17, Width=277, Left=-2, Top=0, InputMask=Replicate('X',254), bHasZoom = .T.

    Proc oFBFLDNAM_2_1.mZoom
        With This.Parent.oContained
            This.Parent.oContained.w_FBFLDNAM = cp_AddFilterR(This.Parent.oContained,"ZOOMFIELDS")
        Endwith
        If !Isnull(This.Parent.oContained)
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object oFBCONDIT_2_2 As StdTrsCombo With uid="JYJCEVRFSR",rtrep=.T.,;
        cFormVar="w_FBCONDIT", RowSource=""+","+"=,"+">=,"+">,"+"<>,"+"<,"+"<=,"+"like,"+"not like,"+"in,"+"not in,"+"exist,"+"not exist,"+"between" , ;
        HelpContextID = 63560766,;
        Height=22, Width=83, Left=280, Top=0,;
        cTotal = "", SpecialEffect=1, BorderStyle=0,;
        bObbl = .F. , nPag=2, bIsInHeader=.F.;
        , bGlobalFont=.T.



    Func oFBCONDIT_2_2.RadioValue(i_bTrs,i_bOld)
        Local xVal,i_cF
        i_cF=This.Parent.oContained.cTrsName
        xVal=Iif(i_bTrs,Iif(i_bOld,&i_cF..FBCONDIT,&i_cF..t_FBCONDIT),This.Value)
        Return(Iif(xVal =1,' ',;
            iif(xVal =2,'=',;
            iif(xVal =3,'>=',;
            iif(xVal =4,'>',;
            iif(xVal =5,'<>',;
            iif(xVal =6,'<',;
            iif(xVal =7,'<=',;
            iif(xVal =8,'like',;
            iif(xVal =9,'not like',;
            iif(xVal =10,'in',;
            iif(xVal =11,'not in',;
            iif(xVal =12,'exist',;
            iif(xVal =13,'not exist',;
            iif(xVal =14,'between',;
            space(10))))))))))))))))
    Endfunc
    Func oFBCONDIT_2_2.GetRadio()
        This.Parent.oContained.w_FBCONDIT = This.RadioValue()
        Return .T.
    Endfunc

    Func oFBCONDIT_2_2.ToRadio()
        This.Parent.oContained.w_FBCONDIT=Trim(This.Parent.oContained.w_FBCONDIT)
        Return(;
            iif(This.Parent.oContained.w_FBCONDIT=='',1,;
            iif(This.Parent.oContained.w_FBCONDIT=='=',2,;
            iif(This.Parent.oContained.w_FBCONDIT=='>=',3,;
            iif(This.Parent.oContained.w_FBCONDIT=='>',4,;
            iif(This.Parent.oContained.w_FBCONDIT=='<>',5,;
            iif(This.Parent.oContained.w_FBCONDIT=='<',6,;
            iif(This.Parent.oContained.w_FBCONDIT=='<=',7,;
            iif(This.Parent.oContained.w_FBCONDIT=='like',8,;
            iif(This.Parent.oContained.w_FBCONDIT=='not like',9,;
            iif(This.Parent.oContained.w_FBCONDIT=='in',10,;
            iif(This.Parent.oContained.w_FBCONDIT=='not in',11,;
            iif(This.Parent.oContained.w_FBCONDIT=='exist',12,;
            iif(This.Parent.oContained.w_FBCONDIT=='not exist',13,;
            iif(This.Parent.oContained.w_FBCONDIT=='between',14,;
            0)))))))))))))))
    Endfunc

    Func oFBCONDIT_2_2.SetRadio()
        This.Value=This.ToRadio()
    Endfunc

    Add Object oFBVALFLT_2_3 As StdTrsField With uid="ERNMTUVHQY",rtseq=4,rtrep=.T.,;
        cFormVar="w_FBVALFLT",Value=Space(254),;
        HelpContextID = 204861936,;
        cTotal = "", SpecialEffect=1, BorderStyle=0,;
        bObbl = .F. , nPag = 2, bIsInHeader=.F., bMultilanguage =  .F.,;
        bGlobalFont=.T.,;
        Height=17, Width=326, Left=368, Top=0, InputMask=Replicate('X',254), bHasZoom = .T.

    Func oFBVALFLT_2_3.mCond()
        With This.Parent.oContained
            Return (!Empty(.w_FBCONDIT))
        Endwith
    Endfunc

    Proc oFBVALFLT_2_3.mZoom
        With This.Parent.oContained
            This.Parent.oContained.w_FBVALFLT = cp_AddFilterR(This.Parent.oContained,"ZOOMFIELDS")
        Endwith
        If !Isnull(This.Parent.oContained)
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object oFBCONDAO_2_4 As StdTrsCombo With uid="DFIBXKFYUY",rtrep=.T.,;
        cFormVar="w_FBCONDAO", RowSource=""+"AND,"+"OR" , ;
        HelpContextID = 63200318,;
        Height=22, Width=62, Left=699, Top=0,;
        cTotal = "", SpecialEffect=1, BorderStyle=0,;
        bObbl = .F. , nPag=2, bIsInHeader=.F.;
        , bGlobalFont=.T.



    Func oFBCONDAO_2_4.RadioValue(i_bTrs,i_bOld)
        Local xVal,i_cF
        i_cF=This.Parent.oContained.cTrsName
        xVal=Iif(i_bTrs,Iif(i_bOld,&i_cF..FBCONDAO,&i_cF..t_FBCONDAO),This.Value)
        Return(Iif(xVal =1,'AND',;
            iif(xVal =2,'OR',;
            space(3))))
    Endfunc
    Func oFBCONDAO_2_4.GetRadio()
        This.Parent.oContained.w_FBCONDAO = This.RadioValue()
        Return .T.
    Endfunc

    Func oFBCONDAO_2_4.ToRadio()
        This.Parent.oContained.w_FBCONDAO=Trim(This.Parent.oContained.w_FBCONDAO)
        Return(;
            iif(This.Parent.oContained.w_FBCONDAO=='AND',1,;
            iif(This.Parent.oContained.w_FBCONDAO=='OR',2,;
            0)))
    Endfunc

    Func oFBCONDAO_2_4.SetRadio()
        This.Value=This.ToRadio()
    Endfunc
    Add Object oLast As LastKeyMover With bHasFixedBody=.T.
    * ---
    Func oFBFLDNAM_2_1.When()
        Return(.T.)
    Proc oFBFLDNAM_2_1.GotFocus()
        If Inlist(This.Parent.oContained.cFunction,'Edit','Load')
            This.Parent.Parent.Parent.SetCurrentRow()
            This.Parent.oContained.SetControlsValue()
        Endif
        DoDefault()
    Proc oFBFLDNAM_2_1.KeyPress(nKeyCode,nShift)
        DoDefault(nKeyCode,nShift)
        Do Case
            Case Inlist(nKeyCode,5,15)
                Nodefault
                If This.Valid()=0
                    Return
                Endif
                If This.Parent.oContained.CheckRow()
                    If Recno()>This.Parent.oContained.nFirstrow
                        This.Parent.oContained.i_lastcheckrow=This.Parent.Parent.Parent.nAbsRow
                        Thisform.LockScreen=.T.
                        Skip -1
                        This.Parent.Parent.Parent.SetFullFocus()
                    Else
                        This.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
                    Endif
                Else
                    If !Isnull(This.Parent.oContained.oNewFocus)
                        This.Parent.oContained.oNewFocus.SetFocus()
                        This.Parent.oContained.oNewFocus=.Null.
                    Endif
                Endif
            Case nKeyCode=24
                Nodefault
                If This.Valid()=0
                    Return
                Endif
                If This.Parent.oContained.CheckRow()
                    If Recno()=This.Parent.oContained.nLastRow
                        If This.Parent.oContained.FullRow() And This.Parent.oContained.CanAddRow()
                            Thisform.LockScreen=.T.
                            This.Parent.oContained.__dummy__.Enabled=.T.
                            This.Parent.oContained.__dummy__.SetFocus()
                            This.Parent.oContained.InitRow()
                            This.Parent.Parent.Parent.SetFocus()
                            This.Parent.oContained.__dummy__.Enabled=.F.
                        Else
                            This.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
                        Endif
                    Else
                        Thisform.LockScreen=.T.
                        This.Parent.oContained.i_lastcheckrow=This.Parent.Parent.Parent.nAbsRow
                        If This.Parent.Parent.Parent.RelativeRow=9
                            This.Parent.Parent.Parent.DoScroll(1)
                        Endif
                        Skip
                        This.Parent.Parent.Parent.SetFullFocus()
                    Endif
                Else
                    If !Isnull(This.Parent.oContained.oNewFocus)
                        This.Parent.oContained.oNewFocus.SetFocus()
                        This.Parent.oContained.oNewFocus=.Null.
                    Endif
                Endif
        Endcase
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_addfilter','FILTERSB','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +" "+i_cAliasName2+".FBCURNAM=FILTERSB.FBCURNAM";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Detail File"
Endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
