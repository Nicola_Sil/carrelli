* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_infozoom                                                     *
*              ProprietÓ zoom                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-05                                                      *
* Last revis.: 2012-02-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject
* --- Area Manuale = Header
* --- cp_infozoom
cp_ReadXdc()
* --- Fine Area Manuale
Return(Createobject("tcp_infozoom",oParentObject))

* --- Class definition
Define Class tcp_infozoom As StdForm
    Top    = 144
    Left   = 219

    * --- Standard Properties
    Width  = 541
    Height = 146
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2012-02-02"
    HelpContextID=97872578
    max_rt_seq=6

    * --- Constant Properties
    _IDX = 0
    cPrg = "cp_infozoom"
    cComment = "ProprietÓ zoom"
    oParentObject = .Null.
    Icon = "mask.ico"
    WindowType = 1
    MinButton = .F.
    *closable = .f.

    * --- Local Variables
    w_cFisTab = Space(30)
    w_cLogTab = Space(30)
    w_cZoomName = Space(254)
    w_cQueryName = Space(254)
    w_cMenuFile = Space(254)
    w_cZoomOnZoom = Space(254)
    * --- Area Manuale = Declare Variables
    * --- cp_infozoom
    AutoCenter = .T.
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=1, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tcp_infozoomPag1","cp_infozoom",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate("Pag.1")
            .Pages(1).oPag.oContained=Thisform
        Endwith
        This.Parent.oFirstControl = This.Page1.oPag.ocFisTab_1_1
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
        * --- Area Manuale = Init Page Frame
        * --- cp_infozoom
        * --- Translate interface...
        This.Parent.cComment=cp_Translate(MSG_ZOOM_PROPERTY)
        * --- Fine Area Manuale
    Endproc
    Proc Init(oParentObject)
        If Vartype(m.oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        DoDefault()

        * --- Open tables
    Function OpenWorkTables()
        Return(This.OpenAllTables(0))

    Procedure SetPostItConn()
        Return


        * --- Read record and initialize Form variables
    Procedure LoadRec()
    Endproc

    * --- ecpQuery
    Procedure ecpQuery()
        This.BlankRec()
        This.cFunction="Edit"
        This.SetStatus()
        If This.cFunction="Edit"
            This.mEnableControls()
        Endif
    Endproc
    * --- Esc
    Proc ecpQuit()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            This.cFunction = "Edit"
            This.oFirstControl.SetFocus()
            Return
        Endif
        * ---
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Endproc
    Proc QueryUnload()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            Nodefault
            Return
        Endif
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Proc ecpSave()
        If This.TerminateEdit() And This.CheckForm()
            This.LockScreen=.T.
            This.mReplace(.T.)
            This.LockScreen=.F.
            This.Hide()
            This.NotifyEvent("Done")
            This.Release()
        Endif
    Endproc
    Func OkToQuit()
        Return .T.
    Endfunc

    * --- Blank Form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        With This
            .w_cFisTab=Space(30)
            .w_cLogTab=Space(30)
            .w_cZoomName=Space(254)
            .w_cQueryName=Space(254)
            .w_cMenuFile=Space(254)
            .w_cZoomOnZoom=Space(254)
            .w_cFisTab = .oParentObject.oContained.cSymFile
            .w_cLogTab = Upper(Alltrim(i_dcx.GetTableDescr(i_dcx.GetTableIdx(Upper(Alltrim(.oParentObject.oContained.cSymFile))))))
            .w_cZoomName = .oParentObject.oContained.cZoomName
            .w_cQueryName = .oParentObject.oContained.ccpQueryName
            .w_cMenuFile = .oParentObject.cMenuFile
            .w_cZoomOnZoom = .oParentObject.oContained.cZoomOnZoom
            .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_TABLES+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_SETUP+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_QUERY_FILE+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_CONTEXT_MENU+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_ZOOM+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_SHOW_PHYSICAL_TABLENAME)
            .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
            .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
            .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_SHOW_NAME_QUERY_FILE)
            .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_SHOW_CONTEXT_MENU_NAME)
            .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_SHOW_ENTITY_NAME)
        Endwith
        This.SaveDependsOn()
        This.SetControlsValue()
        This.oPgFrm.Page1.oPag.oBtn_1_4.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_6.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_9.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
        This.mHideControls()
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = Setting operation
    *     Allowed Parameters: Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        * --- Deactivating List page when <> da Query
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt
        i_cFlt =""
        Return (i_cFlt)
    Endfunc

    Proc QueryKeySet(i_cWhere,i_cOrderBy)
    Endproc

    Proc SelectCursor
    Proc GetXKey

        * --- Update Database
    Function mReplace(i_bEditing)
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        This.NotifyEvent('Update start')
        With This
        Endwith
        This.NotifyEvent('Update end')
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(.T.)

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        If i_bUpd
            With This
                .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_TABLES+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_SETUP+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_QUERY_FILE+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_CONTEXT_MENU+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_ZOOM+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_SHOW_PHYSICAL_TABLENAME)
                .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
                .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
                .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_SHOW_NAME_QUERY_FILE)
                .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_SHOW_CONTEXT_MENU_NAME)
                .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_SHOW_ENTITY_NAME)
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
            Endwith
            This.DoRTCalc(1,6,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
            .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_TABLES+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_SETUP+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_QUERY_FILE+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_CONTEXT_MENU+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_ZOOM+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_SHOW_PHYSICAL_TABLENAME)
            .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
            .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_SHOW_LOGICAL_TABLENAME)
            .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_SHOW_NAME_QUERY_FILE)
            .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_SHOW_CONTEXT_MENU_NAME)
            .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_SHOW_ENTITY_NAME)
        Endwith
        Return


        * --- Enable controls under condition
    Procedure mEnableControls()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        DoDefault()
        Return

        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
        Endwith
        * --- Area Manuale = Notify Event End
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

    Function SetControlsValue()
        If Not(This.oPgFrm.Page1.oPag.ocFisTab_1_1.Value==This.w_cFisTab)
            This.oPgFrm.Page1.oPag.ocFisTab_1_1.Value=This.w_cFisTab
        Endif
        If Not(This.oPgFrm.Page1.oPag.ocLogTab_1_2.Value==This.w_cLogTab)
            This.oPgFrm.Page1.oPag.ocLogTab_1_2.Value=This.w_cLogTab
        Endif
        If Not(This.oPgFrm.Page1.oPag.ocZoomName_1_3.Value==This.w_cZoomName)
            This.oPgFrm.Page1.oPag.ocZoomName_1_3.Value=This.w_cZoomName
        Endif
        If Not(This.oPgFrm.Page1.oPag.ocQueryName_1_5.Value==This.w_cQueryName)
            This.oPgFrm.Page1.oPag.ocQueryName_1_5.Value=This.w_cQueryName
        Endif
        If Not(This.oPgFrm.Page1.oPag.ocMenuFile_1_7.Value==This.w_cMenuFile)
            This.oPgFrm.Page1.oPag.ocMenuFile_1_7.Value=This.w_cMenuFile
        Endif
        If Not(This.oPgFrm.Page1.oPag.ocZoomOnZoom_1_8.Value==This.w_cZoomOnZoom)
            This.oPgFrm.Page1.oPag.ocZoomOnZoom_1_8.Value=This.w_cZoomOnZoom
        Endif
        cp_SetControlsValueExtFlds(This,'')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
            Else
                If Not(i_bnoChk)
                    cp_ErrorMsg(i_cErrorMsg)
                Endif
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

Enddefine

* --- Define pages as container
Define Class tcp_infozoomPag1 As StdContainer
    Width  = 537
    Height = 146
    stdWidth  = 537
    stdheight = 146
    resizeXpos=226
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.

    Add Object ocFisTab_1_1 As StdField With uid="DJGFQBJCBL",rtseq=1,rtrep=.F.,;
        cFormVar = "w_cFisTab", cQueryName = "cFisTab",;
        bObbl = .F. , nPag = 1, Value=Space(30), bMultilanguage =  .F.,;
        ToolTipText = "Visualizza il nome fisico della tabella",;
        HelpContextID = 207699162,;
        bGlobalFont=.T.,;
        Height=21, Width=228, Left=116, Top=8, InputMask=Replicate('X',30), ReadOnly=.T.

    Add Object ocLogTab_1_2 As StdField With uid="WHKUHZFYWQ",rtseq=2,rtrep=.F.,;
        cFormVar = "w_cLogTab", cQueryName = "cLogTab",;
        bObbl = .F. , nPag = 1, Value=Space(30), bMultilanguage =  .F.,;
        ToolTipText = "Visualizza il nome logico della tabella",;
        HelpContextID = 100744410,;
        bGlobalFont=.T.,;
        Height=21, Width=178, Left=350, Top=8, InputMask=Replicate('X',30), ReadOnly=.T.

    Add Object ocZoomName_1_3 As StdField With uid="JLOLDFOPRK",rtseq=3,rtrep=.F.,;
        cFormVar = "w_cZoomName", cQueryName = "cZoomName",;
        bObbl = .F. , nPag = 1, Value=Space(254), bMultilanguage =  .F.,;
        ToolTipText = "Visualizza il nome dello zoom",;
        HelpContextID = 189436175,;
        bGlobalFont=.T.,;
        Height=21, Width=228, Left=116, Top=35, InputMask=Replicate('X',254), ReadOnly=.T.


    Add Object oBtn_1_4 As StdButton With uid="CQXXUHUYYM",Left=354, Top=35, Width=76,Height=22,;
        caption="OpenZoom", nPag=1;
        , ToolTipText = "Premere per aprire lo zoom";
        , HelpContextID = 36806924;
        , Caption=MSG_OPEN_BUTTON;
        , bGlobalFont=.T.

    Proc oBtn_1_4.Click()
        With This.Parent.oContained
            Do OpenZoom With This.Parent.oContained
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object ocQueryName_1_5 As StdField With uid="ZGXJEPKUWX",rtseq=4,rtrep=.F.,;
        cFormVar = "w_cQueryName", cQueryName = "cQueryName",;
        bObbl = .F. , nPag = 1, Value=Space(254), bMultilanguage =  .F.,;
        ToolTipText = "Visualizza il nome della query",;
        HelpContextID = 149831484,;
        bGlobalFont=.T.,;
        Height=21, Width=228, Left=116, Top=62, InputMask=Replicate('X',254), ReadOnly=.T.


    Add Object oBtn_1_6 As StdButton With uid="YQRXVYETMW",Left=354, Top=62, Width=76,Height=22,;
        caption="OpenQuery", nPag=1;
        , ToolTipText = "Premere per aprire la query";
        , HelpContextID = 44754787;
        , Caption=MSG_OPEN_BUTTON;
        , bGlobalFont=.T.

    Proc oBtn_1_6.Click()
        With This.Parent.oContained
            Do OpenQuery With This.Parent.oContained
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object ocMenuFile_1_7 As StdField With uid="YLUBTYZYIL",rtseq=5,rtrep=.F.,;
        cFormVar = "w_cMenuFile", cQueryName = "cMenuFile",;
        bObbl = .F. , nPag = 1, Value=Space(254), bMultilanguage =  .F.,;
        ToolTipText = "Visualizza il nome del menu contestuale associato",;
        HelpContextID = 8030359,;
        bGlobalFont=.T.,;
        Height=21, Width=228, Left=116, Top=89, InputMask=Replicate('X',254), ReadOnly=.T.

    Add Object ocZoomOnZoom_1_8 As StdField With uid="XBFEHCUWDM",rtseq=6,rtrep=.F.,;
        cFormVar = "w_cZoomOnZoom", cQueryName = "cZoomOnZoom",;
        bObbl = .F. , nPag = 1, Value=Space(254), bMultilanguage =  .F.,;
        ToolTipText = "Visualizza il nome dello gestione associata",;
        HelpContextID = 12358113,;
        bGlobalFont=.T.,;
        Height=21, Width=228, Left=116, Top=116, InputMask=Replicate('X',254), ReadOnly=.T.


    Add Object oBtn_1_9 As StdButton With uid="YVVNOWRCVM",Left=478, Top=116, Width=50,Height=20,;
        caption="Ok", nPag=1;
        , HelpContextID = 215636674;
        , Caption=MSG_OK_BUTTON;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_9.Click()
        =cp_StandardFunction(This,"Quit")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oObj_1_15 As cp_setCtrlObjprop With uid="WTFHXDANKA",Left=7, Top=197, Width=168,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Tabella:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 162897307;
        , bGlobalFont=.T.



    Add Object oObj_1_16 As cp_setCtrlObjprop With uid="MXMKFOCINM",Left=7, Top=223, Width=168,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Configurazione:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 162897307;
        , bGlobalFont=.T.



    Add Object oObj_1_17 As cp_setCtrlObjprop With uid="RUSBZEOSXF",Left=7, Top=249, Width=168,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Query File:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 162897307;
        , bGlobalFont=.T.



    Add Object oObj_1_18 As cp_setCtrlObjprop With uid="IUHGJLEFDC",Left=7, Top=275, Width=168,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Menu contestuale:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 162897307;
        , bGlobalFont=.T.



    Add Object oObj_1_19 As cp_setCtrlObjprop With uid="GKILVNDKTQ",Left=7, Top=301, Width=168,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Ricerca:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 162897307;
        , bGlobalFont=.T.



    Add Object oObj_1_20 As cp_setobjprop With uid="SKPUZOAYUU",Left=280, Top=197, Width=168,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_FisTab",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 162897307;
        , bGlobalFont=.T.



    Add Object oObj_1_21 As cp_setobjprop With uid="EHQUCINFUP",Left=280, Top=223, Width=168,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_cLogTab",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 162897307;
        , bGlobalFont=.T.



    Add Object oObj_1_22 As cp_setobjprop With uid="CRQJWNEWNL",Left=280, Top=249, Width=168,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_cZoomName",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 162897307;
        , bGlobalFont=.T.



    Add Object oObj_1_23 As cp_setobjprop With uid="RUDVSJVAGV",Left=280, Top=275, Width=168,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_cQueryName",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 162897307;
        , bGlobalFont=.T.



    Add Object oObj_1_24 As cp_setobjprop With uid="UCSUEVDYTN",Left=280, Top=301, Width=168,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_cMenufile",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 162897307;
        , bGlobalFont=.T.



    Add Object oObj_1_25 As cp_setobjprop With uid="BJZBTYIQBF",Left=280, Top=327, Width=168,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_cZoomOnZoom",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 162897307;
        , bGlobalFont=.T.


    Add Object oStr_1_10 As StdString With uid="TQWDNGNLBG",Visible=.T., Left=13, Top=37,;
        Alignment=1, Width=98, Height=18,;
        Caption="Configurazione:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_11 As StdString With uid="FFBNAJVRKR",Visible=.T., Left=1, Top=91,;
        Alignment=1, Width=110, Height=18,;
        Caption="Menu contestuale:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_12 As StdString With uid="YMKICHSDRF",Visible=.T., Left=6, Top=64,;
        Alignment=1, Width=105, Height=18,;
        Caption="Query File:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_13 As StdString With uid="ZSGXKEPLFV",Visible=.T., Left=16, Top=10,;
        Alignment=1, Width=95, Height=18,;
        Caption="Tabella:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_14 As StdString With uid="KFQQAZYJHZ",Visible=.T., Left=19, Top=118,;
        Alignment=1, Width=92, Height=18,;
        Caption="Ricerca:"  ;
        , bGlobalFont=.T.
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_infozoom','','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Dialog Window"
Endproc

* --- Area Manuale = Functions & Procedures
* --- cp_infozoom
Proc OpenQuery(obj)
    Public i_designfile
    i_designfile=Fullpath(Forceext(obj.w_cQueryName,'VQR'))
    vq_build()
    Release i_designfile
Endproc

Proc OpenZoom(obj)
    vz_build()
    i_curform.oFORM.Zoom.askfile(Fullpath(Forceext(obj.w_cZoomName,Alltrim(obj.w_cFisTab)+'_VZM')))
Endproc
* --- Fine Area Manuale
