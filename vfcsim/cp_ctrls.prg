* ----------------------------------------------------------------------------
* Modulo   : Interface Manager
* Programma: CP_STFLD
* Ver      : 2.0.0
* Linguaggi: Visual FoxPro
* Ambienti : Windows
* ----------------------------------------------------------------------------
* Autore        : Gregorio Piccoli
* Data creazione: 20/03/97
* Aggiornato il : 13/03/98
* Translated    : 02/03/2000
* #%&%#Build:  59
* ----------------------------------------------------------------------------
* Controlli standard
* ----------------------------------------------------------------------------

#include "cp_app_lang.inc"

Define Class CPTextBox As TextBox
    *forecolor=rgb(96,0,0)
    *backcolor=rgb(255,242,192)
    * propriet� per contenere il nome del shape per i campi obbligatori
    CShape=''
    * --- Gestione men� contestuale (tasto destro)
    cMenuFile=''
    zbName='' && gestione bottone contestuale
    transbName='' && Nome bottoncino traduzione del dato
    bNoRightClick = .F. && disabilita tasto dx
    bNoBackColor = .F. &&Di Default non applica i colori di sfondo (disabilita il cambio colore di sfondo alla getfocus)
    bNoDisabledBackColor = .F. &&Di Default non applica la configurazione interfaccia
    Procedure Init
        This.BackColor = Iif(This.bNoBackColor, This.BackColor, i_nEBackColor)
        This.SpecialEffect = i_nSpecialEffect
        This.BorderColor = i_nBorderColor
        This.DisabledForeColor = i_udisabledforecolor
        This.DisabledBackColor = Iif(This.bNoDisabledBackColor, This.DisabledBackColor, i_udisabledbackcolor)
        #If Version(5)<800
            This.StatusBarText=Padr(This.ToolTipText,100)
        #Else
            This.StatusBarText = This.ToolTipText
        #Endif
        DoDefault()
    Endproc

    * --- Al cambio della posizione (Run-Time) del control distruggo e ricreo eventuale rettangolo
    * --- per evidenziare campo obbligatorio o bottone contestuale
    Procedure Left_Assign(xvalue)
        If xvalue<>This.Left
            This.Left=xvalue
            This.Draw_Link_Obj()
        Endif
    Endproc

    Procedure Top_Assign(xvalue)
        If xvalue<>This.Top
            This.Top=xvalue
            This.Draw_Link_Obj()
        Endif
    Endproc

    Procedure Width_Assign(xvalue)
        If xvalue<>This.Width
            This.Width=xvalue
            This.Draw_Link_Obj()
        Endif
    Endproc

    Procedure Height_Assign(xvalue)
        If xvalue<>This.Height
            This.Height=xvalue
            This.Draw_Link_Obj()
        Endif
    Endproc

    Proc Draw_Link_Obj
        Local btnName
        * --- Bottone contestuale
        Local i_btnZoom,ctShape
        i_btnZoom=.F.
        btnName='this.parent.'+This.zbName
        * --- Riposiziono bottone contestuale se presente...
        If Type(btnName)='O'
            btnName=This.zbName
            This.Parent.&btnName..Riposiziona_Btn()
            i_btnZoom=.T.
        Endif
        * --- Zucchetti Aulla fine - Bottone contestuale
        btnName='this.parent.'+This.transbName
        * --- Riposiziono bottone traduzioni...
        If Type(btnName)='O'
            btnName=This.transbName
            This.Parent.&btnName..Riposiziona_Btn(i_btnZoom)
        Endif

        * ---  Campo obbligatorio
        * --- Riposiziono shape se campo obbligatorio e se shape presente...
        If Type('this.bObbl')='L' And This.bObbl And Not Empty(This.CShape)
            * --- verifico se l'oggetto esiste...
            ctShape='this.parent.'+This.CShape
            If Type(ctShape)='O'
                ctShape=This.CShape
                This.Parent.&ctShape..Riposiziona()
            Endif
        Endif
    Endproc

    Procedure visible_assign(xvalue)
        If This.Visible <> xvalue
            This.Visible = xvalue
            * --- Campo obbligatorio
            Local CShape
            If Type('this.cShape')='C' And Not Empty(This.CShape)
                CShape = This.CShape
                * nascondo il rettangolo di campo obbligatorio
                * se campo non visible
                This.Parent.&CShape..Visible = This.Visible And This.Enabled
            Endif

            Local zbName
            zbName = This.zbName
            * --- Mostro bottone contestuale se presente...
            If Not Empty(zbName) And Type('this.bHasZoom')='L' And This.bHasZoom
                This.Parent.&zbName..Visible = xvalue
            Endif
            Local transbName
            transbName = This.transbName
            * --- Mostro bottone contestuale se presente...
            If Not Empty(transbName)
                This.Parent.&transbName..Visible = xvalue
            Endif
        Endif
    Endproc

    * --- Campo obbligatorio
    Procedure enabled_assign(xvalue)
        Local CShape
        If This.Enabled <> xvalue
            This.Enabled = xvalue
            If Type('this.cShape')='C' And Not Empty(This.CShape)
                CShape = This.CShape
                * nascondo il rettangolo di campo obbligatorio
                * se campo non editabile
                This.Parent.&CShape..Visible = This.Visible And This.Enabled
            Endif
        Endif
    Endproc

    Procedure KeyPress(nKeyCode, nShiftAltCtrl)
        Local pt
        If Vartype(This.Value)='N'
            pt=Set('point')
            Do Case
                Case nKeyCode=46 And nKeyCode<>Asc(pt)
                    * Sostituzione carattere '.'
                    Nodefault
                    Keyboard pt
                Case nKeyCode=44 And nKeyCode<>Asc(pt)
                    * Sostituzione carattere '.'
                    Nodefault
                    Keyboard pt
                Case nKeyCode=9
                    * uscita con il tasto return, in modo da sovrascrivere sempre il contenuto
                    Nodefault
                    Keyboar '{ENTER}'
            Endcase
        Endif
    Endproc

    Procedure RightClick
        If !This.bNoRightClick   &&  disabilito men� contestuale
            If Vartype(This.Parent.oContained)='O' And Vartype( This.cMenuFile )='C'
                g_oMenu=Createobject("IM_ContextMenu",This,This.Parent.oContained,This.cMenuFile,1)
                g_oMenu.creaMenu()
                ** Deve essere rilasciata la risorsa occupata, causa chiusura non corretta di Revolution
                g_oMenu=.Null.
            Endif
        Endif
    Endproc

    *PROCEDURE MouseEnter(nButton, nShift, nXCoord, nYCoord)
    *  IF VARTYPE(thisform.__tb__)='O'
    *    thisform.__tb__.top=this.top+28
    *    thisform.__tb__.left=this.left+this.width-5
    *    thisform.__tb__.height=this.height
    *    thisform.__tb__.zorder(0)
    *    thisform.__tb__.visible=.t.
    *  ENDIF
    *  return
    *PROCEDURE MouseLeave(nButton, nShift, nXCoord, nYCoord)
    *  IF VARTYPE(thisform.__tb__)='O'
    *    thisform.__tb__.visible=.f.
    *  ENDIF
    *  return
Enddefine

Define Class CPEditBox As EditBox
    *forecolor=rgb(96,0,0)
    *backcolor=rgb(255,255,224)
    cMenuFile=''
    nSeconds=0 && Gestione frasi modello, ritardo zoom on zoom
    bNoRightClick = .F. && disabilito il tasto dx

    bNoBackColor = .F. &&Di Default non applica i colori di sfondo (disabilita il cambio colore di sfondo alla getfocus)
    bNoDisabledBackColor = .F. &&Di Default non applica la configurazione interfaccia

    Procedure Init
        This.BackColor=Iif(This.bNoBackColor, This.BackColor, i_nEBackColor)
        This.SpecialEffect = i_nSpecialEffect
        This.BorderColor = i_nBorderColor
        This.DisabledForeColor = i_udisabledforecolor
        This.DisabledBackColor = Iif(This.bNoDisabledBackColor, This.DisabledBackColor, i_udisabledbackcolor)
        #If Version(5)<800
            This.StatusBarText=Padr(This.ToolTipText,100)
        #Else
            This.StatusBarText = This.ToolTipText
        #Endif
        DoDefault()
    Endproc

    Procedure RightClick
        ** Se il campo memo non � editabile allora non viene visualizzato il menu contestuale
        If This.ReadOnly=.T.
            Return
        Endif
        If Vartype(This.Parent.oContained)='O' Or Vartype( This.cMenuFile )='C'
            This.Parent.oContained.NotifyEvent(This.cFormVar+' MouseRightClick')
            If !This.bNoRightClick  && disabilito il tasto dx
                g_oMenu=Createobject("IM_ContextMenu",This,This.Parent.oContained,This.cMenuFile,1)
                g_oMenu.creaMenu()
                ** Deve essere rilasciata la risorsa occupata, causa chiusura non corretta dell'applicazione
                g_oMenu=.Null.
            Endif
        Endif
    Endproc
Enddefine
Define Class CPLabel As Label
    *forecolor=rgb(255,0,0)
    Procedure RightClick
        This.Parent.RightClick()
Enddefine
Define Class CPCommandButton As CommandButton
    *forecolor=rgb(255,0,0)
    ImgSize = i_nBtnImgSize	&&Dimenisione bmp
    NoImgAllign = 1 &&Allignamento in caso di g_NoButtonImage; 0 = Top, 1 = Bottom
    Procedure Init
        #If Version(5)>=800
            SpecialEffect = i_nBtnSpEfc
        #Endif
        This.StatusBarText=Padr(This.ToolTipText,100)
        DoDefault()
	If Pemstatus(This, "CpPicture", 5)
	  This.Picture = This.CpPicture
	Else
          This.Picture = This.Picture
	Endif
    Endproc

    Procedure Picture_assign
        Lparameters cNewVal
        If Empty(m.cNewVal)
            This.Picture = ''
            Return
        Endif
        Local cFileBmp, l_Dim
        l_Dim= Iif(This.ImgSize<>0, This.ImgSize, i_nBtnImgSize)
        If Vartype(i_ThemesManager)=='O'
            m.cFileBmp=i_ThemesManager.PutBmp(This,m.cNewVal, l_Dim)
        Else
            This.Picture = m.cNewVal
        Endif
    Endproc

Enddefine
Define Class CPShape As Shape
Enddefine
Define Class CPCheckBox As Checkbox
    cMenuFile=""
    bNoRightClick = .F.  && Disabilito tasto dx
    bNoBackColor = .F. &&Di Default non applica i colori di sfondo (disabilita il cambio colore di sfondo alla getfocus)
    bNoDisabledBackColor = .F. &&Di Default non applica la configurazione interfaccia

    Procedure RightClick
        If Vartype(This.Parent.oContained)='O' And Vartype( This.cMenuFile )='C'
            This.Parent.oContained.NotifyEvent(This.cFormVar+' MouseRightClick')
            If !This.bNoRightClick  &&  Disabilito tasto dx
                g_oMenu=Createobject("IM_ContextMenu",This,This.Parent.oContained,This.cMenuFile,1)
                g_oMenu.creaMenu()
                ** Deve essere rilasciata la risorsa occupata, causa chiusura non corretta di Revolution
                g_oMenu=.Null.
            Endif
        Endif
    Endproc
    Procedure Init
        #If Version(5)<800
            This.StatusBarText=Padr(This.ToolTipText,100)
        #Else
            This.StatusBarText = This.ToolTipText
        #Endif
        This.SpecialEffect = i_nSpecialEffect
        DoDefault()
    Endproc
Enddefine
Define Class CPOptionGroup As OptionGroup
    cMenuFile=""
    bNoRightClick = .F.  && Disabilito tasto dx

    Procedure RightClick
        If Vartype(This.Parent.oContained)='O' And Vartype( This.cMenuFile )='C'
            If !This.bNoRightClick  && disabilito tasto dx
                g_oMenu=Createobject("IM_ContextMenu",This,This.Parent.oContained,This.cMenuFile,1)
                g_oMenu.creaMenu()
                ** Deve essere rilasciata la risorsa occupata, causa chiusura non corretta di Revolution
                g_oMenu=.Null.
            Endif
        Endif
    Endproc

    Proc Init
        DoDefault()
        This.SpecialEffect = i_nSpecialEffect
        This.BorderColor = i_nBorderColor
    Endproc
Enddefine
Define Class CPComboBox As ComboBox
    *forecolor=rgb(96,0,0)
    *backcolor=rgb(255,255,224)
    *disabledforecolor=rgb(0,0,96)
    *disabledbackcolor=rgb(224,224,224)
    cMenuFile=""
    bNoRightClick = .F.  && Disabilito tasto dx

    bNoBackColor = .F. &&Di Default non applica i colori di sfondo (disabilita il cambio colore di sfondo alla getfocus)
    bNoDisabledBackColor=.F. &&Di Default non applica la configurazione interfaccia

    Procedure Init
        This.BackColor=Iif(This.bNoBackColor, This.BackColor, i_nEBackColor)
        This.SpecialEffect = i_nSpecialEffect
        This.BorderColor = i_nBorderColor
        This.DisabledBackColor = Iif(This.bNoDisabledBackColor, This.DisabledBackColor, i_udisabledbackcolor)
        #If Version(5)<800
            This.StatusBarText=Padr(This.ToolTipText,100)
        #Else
            This.StatusBarText = This.ToolTipText
        #Endif
        DoDefault()
    Endproc
    Procedure RightClick
        If Vartype(This.Parent.oContained)='O' And Vartype( This.cMenuFile )='C'
            This.Parent.oContained.NotifyEvent(This.cFormVar+' MouseRightClick')
            If !This.bNoRightClick    && Disabilito tasto dx
                g_oMenu=Createobject("IM_ContextMenu",This,This.Parent.oContained,This.cMenuFile,1)
                g_oMenu.creaMenu()
                ** Deve essere rilasciata la risorsa occupata, causa chiusura non corretta di Revolution
                g_oMenu=.Null.
            Endif
        Endif
    Endproc
Enddefine
Define Class CPGrid As Grid
	* --- Gestione ottimizzata Scroll griglie e metodo Paint della form che le ospita
	PROCEDURE scrolled(nDir)
		local i_olderr
		i_olderr=on('ERROR')
		* -- La on error potrebbe essere ommessa in questo caso (lasciata per similitudine di comportamento con la Scrolled di Zoombox
		on error =.t.
		thisform._bPaint_ErrorGrid=.t.    
		on error &i_olderr 
		DODEFAULT(ndir)  
	ENDPROC	
Enddefine

Define Class cpzoomcolumn As Column
Enddefine

Define Class cpzoomHeader As Header
Enddefine

Define Class cpzoomtextbox As TextBox
Enddefine

Define Class cpcolumn As Column
    DynamicBackColor="iif(this.nAbsRow<>RECNO(),RGB(255,255,255), i_nDtlRowClr)"
Enddefine

Define Class CpPrnBtn As CommandButton

    * classe per i bottoni della Print System (cp_chprn)
    Height = 45
    Width = 48
    BackStyle=0
    #If Version(5)>=700
        SpecialEffect = i_nPrnBtnSpEfc
    #Endif
    FontName = "Arial"
    FontSize = 7
    FontBold = .F.
    FontItalic=.F.
    FontUnderline=.F.
    FontStrikethru=.F.

    Procedure Init
        DoDefault()
        This.Picture = This.Picture
    Endproc

    Procedure Picture_assign
        Lparameters cNewVal
        Local cFileBmp
        If Vartype(i_ThemesManager)=='O'
            m.cFileBmp  = i_ThemesManager.PutBmp(This,m.cNewVal, 24)
        Else
            This.Picture = m.cNewVal
        Endif
    Endproc

Enddefine

Define Class CPToolBtn As CommandButton

    * classe per i bottoni della toolbar
    Height=i_nTBTNH+6
    Width=i_nTBTNW+9
    BackStyle=0
    #If Version(5)>=700
        SpecialEffect = i_nTbBtnSpEfc
    #Endif
    cImage = "" &&Bitmap Pulsante
    _OnClick = ""  &&Comando da eseguire al click
    _OnRightClick = ""	&&Comando da eseguire al rightclick
    cType='' 	&& Utilizzato per disegnatore di maschere (tipo di operazione associato al bottone)
    Ndx=0 	&& per compatabilit� con le chiamate alla nuova interfaccia
    nDY=0 	&& per compatabilit� con le chiamate alla nuova interfaccia
    cName=''	&& per compatabilit� con le chiamate alla nuova interfaccia
    *--- Valorizzazione cambio immagine
    Procedure cImage_Assign(xvalue)
        Local nDimension
        With This
            .cImage = m.xvalue
            Do Case
                Case i_nTBTNH>30
                    m.nDimension = 32
                Case i_nTBTNH>22
                    m.nDimension = 24
                Otherwise
                    m.nDimension = 16
            Endcase
            *--- Assegno la nuova bitmap
            If Vartype(i_ThemesManager)=='O'
                i_ThemesManager.PutBmp(This, .cImage ,m.nDimension)
            Else
                .Picture = .cImage
            Endif
        Endwith
    Endproc
    *--- OnClick
    Procedure Click()
        If Not Empty(This._OnClick)
            Local l_OnClick
            m.l_OnClick = This._OnClick
            &l_OnClick
        Endif
    Endproc
    *--- OnRightClick
    Procedure RightClick()
        If Not Empty(This._OnRightClick)
            Local l_OnRightClick
            m.l_OnRightClick = This._OnRightClick
            &l_OnRightClick
        Endif
    Endproc

Enddefine

* === Definizione Campo Standard
Define Class StdField As CPTextBox
    *
    * --- PROPRIETA' VFP
    *
    Height = 23
    Caption = ""
    * visible = .t.                          && ERRORE Per gli oggetti contenuti va settato da fuori
    FontSize   = 9
    FontBold   = .F.
    FontItalic = .F.
    FontUnderline  = .F.
    FontStrikethru = .F.
    FontName   = ""
    *DisabledBackColor=rgb(255,255,255)
    Format='K'
    Margin=1
    *
    * --- PROPRIETA' CODEPAINTER
    *
    cQueryName = ""
    bObbl = .F.                            && Obbligatorio
    sErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    cLinkFile=''
    cOldIcon=''
    bHasZoom=.F.
    nPag=0
    cFormVar=''
    bIsInHeader=.T.
    nZero=0
    bUpd=.F.
    cSayPict=''
    cGetPict=''
    nSeconds=0
    bInInput=.F.
    * --
    cfgCalc=''
    cfgCond=''
    cfgCheck=''
    cfgDefault=''
    cfgHide=''
    cfgInit=''
    bNoMenuFunction=.F.           && Disabilita nel tasto destro sottomenu Funzionalit�
    bNoMenuAction=.F.         	&& Disabilita nel tasto destro sottomenu Azioni
    bNoMenuProperty=.F.			&& Disabilita nel tasto destro sottomenu Propiet�
    bNoMenuCut=.F.			    && Disabilita nel tasto destro sottomenu Taglia
    bNoMenuCopy=.F.			    && Disabilita nel tasto destro sottomenu Copia
    bNoMenuPaste=.F.			    && Disabilita nel tasto destro sottomenu Incolla
    bNoMenuSelAll=.F.			    && Disabilita nel tasto destro sottomenu Seleziona Tutto
    bSetFont = .T. && Attiva / disattiva la possibilt� di pilotare i font
    bGlobalFont=.F.
    bNoZoomDateType=.F.

    *
    * --- COSTRUTTORE/DISTRUTTORE
    *
    * --- Inizializzazione
    Procedure Init()
        Local i_im
        * ---Add the button contextual
        Local i_cond
        * Aggiunta del bottoncino contestuale di fianco ai campi con linktable
        i_cond=((i_cZBtnShw='S' And (Type('this.bNoContxtBtn')='U' Or (Type('this.bNoContxtBtn')='N' And This.bNoContxtBtn=2 ))) Or;
            (i_cZBtnShw='N' And Type('this.bNoContxtBtn')='N' And This.bNoContxtBtn=2 ))
        If Type('this.bHasZoom')='L' And This.bHasZoom And i_cond
            Local btnName
            This.zbName = 'z'+This.Name
            * --- Gestisco l'errore in alcuni casi va in errore perch� il Parent non � ancora stato definito
            Local l_olderr, berr
            l_olderr=On('error')
            berr=.F.
            On Error berr=.T.
            This.Parent.AddObject(This.zbName,'btnZoom',This,This.Top,This.Left,This.Width,This.Height)
            On Error &l_olderr
            If Not berr
                * Per la macro non posso utilizzare una propriet�
                btnName = This.zbName
                This.Parent.&btnName..Visible = This.Visible
                This.Width=This.Width-i_nZBtnWidth
            Endif
        Endif
        * --- Add button translations
        If Type('this.bMultilanguage')='L' And This.bMultilanguage And Not Empty(i_aCpLangs[1])
            Local btnName
            This.transbName = 'trans'+This.Name
            * --- Gestisco l'errore in alcuni casi va in errore perch� il Parent non � ancora stato definito
            Local l_olderr, berr
            l_olderr=On('error')
            berr=.F.
            On Error berr=.T.
            This.Parent.AddObject(This.transbName,'btnTrans',This,This.Top,This.Left,This.Width,This.Height)
            On Error &l_olderr
            If Not berr
                * Per la macro non posso utilizzare una propriet�
                btnName = This.transbName
                This.Parent.&btnName..Visible = This.Visible
                This.Width=This.Width-i_nZBtnWidth
            Endif
        Endif
        * Add a red rectangle around the required fields
        If i_cHlOblColor='S' And Type('this.bObbl')='L' And This.bObbl
            Local l_olderr, berr
            l_olderr=On('error')
            On Error berr=.T.
            This.CShape='TTxOb'+This.Name
            This.Parent.AddObject(This.CShape,'OblShape',This.Top-1,This.Left-1,This.Width+2,This.Height+2,1,This)
            On Error &l_olderr
        Endif
        If Empty(This.FontName)
            This.FontName = This.Parent.FontName
            This.FontSize = This.Parent.FontSize
            This.FontBold = This.Parent.FontBold
            This.FontItalic = This.Parent.FontItalic
            This.FontUnderline  = This.Parent.FontUnderline
            This.FontStrikethru = This.Parent.FontStrikethru
        Endif
        If This.bSetFont
            This.SetFont()
        Endif
        If Empty(This.cSayPict) And !Empty(This.cGetPict)
            This.cSayPict=This.cGetPict
        Endif
        If !Empty(This.cSayPict) And Empty(This.cGetPict)
            This.cGetPict=This.cSayPict
        Endif
        If !Empty(This.cSayPict)
            i_im=Trim(cp_GetMask(This.cSayPict))
            If !Empty(i_im)
                This.InputMask=i_im
            Endif
            This.Format='K'+cp_GetFormat(This.cSayPict)
        Endif
        This.ToolTipText=cp_Translate(This.ToolTipText)
        DoDefault()
        Return

    Procedure SetFont()
		If This.bGlobalFont
			SetFont('T', This)
		Endif
    Endproc
    *
    * --- EVENTI/METODI VFP
    *
    * --- Quando prende il Focus
    Proc GotFocus()
        Local i_var,i_im,e
        Local i_oldvalue
        This.bUpd=.F.
        If This.Parent.oContained.cFunction="Query"
            * --- attiva il bottone di edit
            oCpToolBar.b2.Enabled=This.Parent.oContained.bLoaded
            * --- attiva i bottoni prev-next
            oCpToolBar.b7.Enabled=Used(This.Parent.oContained.cKeySet) And !Bof(This.Parent.oContained.cKeySet)
            oCpToolBar.b8.Enabled=Used(This.Parent.oContained.cKeySet) And !Eof(This.Parent.oContained.cKeySet)
        Endif
        * --- calcola il default
        If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
            This.Parent.oContained.SaveDependsOn()
            This.ResetTotal()
            i_oldvalue=This.Value
            This.mDefault()
            oCpToolBar.b9.Enabled=This.bHasZoom Or Type('this.parent.oContained.'+This.cFormVar)='D'
            This.bUpd=i_oldvalue<>This.Value
        Endif
        * -- default da configurazione
        If Not(Empty(This.cfgDefault)) And Empty(This.Value)
            i_oldvalue=This.Value
            e=Strtran(This.cfgDefault,'w_','this.parent.oContained.w_')
            This.Value=&e
            This.bUpd=This.bUpd Or This.Value<>i_oldvalue
        Endif
        * --- esegue la funzione "before input"
        If This.Parent.oContained.cFunction<>"Filter"
            This.mBefore()
            * --- il valore puo' essere cambiato dalla funzione di "Before"
            i_var=This.cFormVar
            If This.Value<>This.Parent.oContained.&i_var And This.IsUpdated()
                This.SetControlValue()
                This.bUpd=.T.
            Endif
        Endif
        This.bInInput=.T.
        * --- setta la picture di "get"
        If !Empty(This.cGetPict)
            i_im=Trim(cp_GetMask(This.cGetPict))
            If !Empty(i_im)
                This.InputMask=i_im
            Endif
            This.Format='K'+cp_GetFormat(This.cGetPict)
        Endif
        If !This.bNoBackColor
            This.BackColor= i_nBackColor
        Endif
        If Inlist(This.Parent.oContained.cFunction,"Load","Edit")
            If Vartype(i_var)='C'
                This.Parent.oContained.NotifyEvent(i_var+' GotFocus')
            Endif
        Endif

        Return
    Func When()
        Local i_bRes,i_e
        * --- Procedure standard del campo
        i_bRes=.T.
        If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
            i_bRes = This.mCond()
            If !Empty(This.cfgCond)
                i_e=Strtran(This.cfgCond,'w_','this.parent.oContained.w_')
                i_bRes=&i_e
            Endif
        Endif
        Return(i_bRes)
    Func Valid()
        Local i_nRes,i_bErr,i_var,i_bUpd,e,l,i_oldform
        i_nRes=1
        * --- Il value ha il nuovo valore, la variabile di work deve essere aggiornata
        i_var=This.cFormVar
        i_bUpd=.F.
        If Vartype(i_curform)='U'
            i_oldform=.Null.
        Else
            i_oldform=i_curform
        Endif
        If This.nZero<>0
            If !Empty(This.Value) And Left(This.Value,1)$'123456789'
                l=Iif(This.nZero>0,This.nZero,Len(This.Value))
                This.Value=Right(Repl('0',l)+Alltrim(This.Value),l)
            Endif
        Endif
        If This.IsUpdated()
            i_bUpd=.T.
            This.Parent.oContained.bUpdated=.T.
            If This.bIsInHeader
                This.Parent.oContained.bHeaderUpdated=.T.
            Endif
            This.Parent.oContained.&i_var=This.Value
            This.SetModified()
        Endif
        This.bUpd=.F.
        * --- After Input solo alla variazione del campo
        If i_bUpd
            This.mAfter()
            * --- il valore puo' essere cambiato dalla funzione di "After"
            If This.Value<>This.Parent.oContained.&i_var
                This.SetControlValue()
            Endif
        Endif
        * ---
        If Not(Inlist(This.Parent.oContained.cFunction,'Filter','Query'))
            i_bErr=.F.
            i_nRes=Iif(i_bUpd,0,1)
            oCpToolBar.b9.Enabled=.F.
            * --- Obbligatorieta'
            If This.bObbl And Empty(This.Value) And This.CondObbl()
                If Not(This.Parent.oContained.bDontReportError)
                    Do cp_ErrorMsg With MSG_FIELD_CANNOT_BE_NULL_QM
                Endif
                i_nRes=0
                i_bErr=.T.
            Endif
            If Not(i_bErr) And i_bUpd
                If !Empty(This.cfgCheck)
                    e=Strtran(This.cfgCheck,'w_','this.parent.oContained.w_')
                    i_bErr=Not(&e)
                    If i_bErr
                        If Not(This.Parent.oContained.bDontReportError)
                            Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
                        Endif
                        This.Parent.oContained.&i_var=cp_NullValue(This.Parent.oContained.&i_var)
                        This.SetControlValue()
                        i_nRes=0
                    Endif
                Endif
                If Not(i_bErr) And i_bUpd
                    * --- Procedura di check e calcolo se il campo e' cambiato
                    If This.Check()
                        i_nRes=1
                    Else
                        If Not(This.Parent.oContained.bDontReportError)
                            This.SpecialEffect=1
                            This.BorderColor=Rgb(255,0,0)
                            Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
                        Endif
                        * --- azzeramento della variabile e del control
                        This.Parent.oContained.&i_var=cp_NullValue(This.Parent.oContained.&i_var)
                        This.SetControlValue()
                    Endif
                Endif
                This.SetTotal()
                This.Parent.oContained.NotifyEvent(i_var+' Changed')
                This.Parent.oContained.mCalc(i_bUpd)
                This.Parent.oContained.SaveDependsOn()
            Else
                This.SetTotal()
            Endif
        Endif
        i_curform=i_oldform
        This.Parent.oContained.bDontReportError=.F.
        Return(i_nRes)
    Proc LostFocus()
        Local i_cFlt,i_cFile,i_nConn,i_im
        This.SpecialEffect=0
        If This.Parent.oContained.cFunction='Query'
            If This.Parent.oContained.bUpdated And Not(Empty(This.cQueryName))
                Nodefault
                i_cFile=This.Parent.oContained.cFile
                i_nConn=i_TableProp[this.parent.oContained.&i_cFile._idx,3]
                i_cFlt=cp_BuildKeyWhere(This.Parent.oContained,This.cQueryName,i_nConn)
                This.Parent.oContained.QueryKeySet(i_cFlt,This.cQueryName)
                This.Parent.oContained.LoadRecWarn()
            Endif
        Endif
        If !Empty(This.cSayPict)
            i_im=Trim(cp_GetMask(This.cSayPict))
            If !Empty(i_im)
                This.InputMask=i_im
            Endif
            This.Format='K'+cp_GetFormat(This.cSayPict)
        Endif
        This.bInInput=.F.
        This.Parent.oContained.NotifyEvent(This.cFormVar+' LostFocus')
        If !This.bNoBackColor
            This.BackColor=i_nEBackColor
        Endif
        Return
    Func IsUpdated()
        If This.bUpd
            Return (.T.)
        Endif
        Local i_var,i_m
        i_var=This.cFormVar
        i_m=This.InputMask
        If Vartype(This.Value)='C'
            If Empty(i_m)
                Return(Not(Trim(This.Value)==Trim(This.Parent.oContained.&i_var)))
            Else
                * c'e una picture, il valore della varibile deve essere formattato di conseguenza
                Return(Not(Trim(Transform(This.Value,i_m))==Trim(Transform(This.Parent.oContained.&i_var,i_m))))
            Endif
        Endif
        Return(Not(This.Value==This.Parent.oContained.&i_var))
    Proc MouseDown(nButton, nShift, nXCoord, nYCoord)
        If nButton=1
            If Seconds()-0.3<This.nSeconds
                If Inlist(This.Parent.oContained.cFunction,'Edit','Load')
                    Nodefault
                    This.Parent.oContained.bDontReportError=.T.
                    Public i_lastindirectaction
                    i_lastindirectaction='ecpZoom'
                    This.mZoom()
                Endif
            Endif
            This.nSeconds=Seconds()
        Endif
        Return
        *
        * ------ METODI CODEPAINTER
        *
        * --- Procedura di default standard del campo
    Proc mDefault()
        Return
        * --- Procedura di Condizione standard del campo
    Func mCond()
        Return (.T.)
        * --- Procedura di Hide standard del campo
    Func mHide()
        Return (.F.)
        * --- Procedura di Before input standard del campo
    Proc mBefore()
        Return
        * --- Procedura di After input standard del campo
    Proc mAfter()
        Return
        * --- Procedura di check
    Func Check()
        Return(.T.)
    Func CondObbl()
        Return (.T.)
    Proc mZoom()
        If Not This.bNoZoomDateType And Type('this.parent.oContained.'+This.cFormVar)='D'
            cp_zdate(This)
        Endif
        Return
    Proc DragOver(oSource,nXCoord,nYCoord,nState)
        If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And oSource.oContained.bLoaded
            Do Case
                Case nState=0   && Enter
                    This.cOldIcon=oSource.DragIcon
                    If oSource.cFile==This.cLinkFile
                        oSource.DragIcon=i_cBmpPath+"cross01.cur"
                    Endif
                Case nState=1   && Leave
                    oSource.DragIcon=This.cOldIcon
            Endcase
        Endif
        Return
    Proc DragDrop(oSource, nXCoord, nYCoord)
        If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And oSource.oContained.bLoaded
            If oSource.cFile==This.cLinkFile
                This.ecpDrop(oSource)
            Endif
        Endif
        Return
    Proc ecpDrop(oSource)
        Return
    Proc SetModified()
        Return
    Proc ResetTotal()
        Return
    Proc SetTotal()
        Return
    Proc SetControlValue()
        Local i_var,i_cs
        i_var=This.cFormVar
        i_cs=This.ControlSource
        This.Value=This.Parent.oContained.&i_var
        If !(Empty(i_cs))
            Select (This.Parent.oContained.cTrsName)
            Replace &i_cs With This.Value
        Endif
        Return
    Proc ProgrammaticChange()
        * --- questa routine gestisce le picture dinamiche
        Local i_im
        If !This.bInInput And !Empty(This.cSayPict) And !(Left(This.cSayPict,1)$["'])
            * wait window "calcola picture "+this.name nowait
            i_im=Trim(cp_GetMask(This.cSayPict))
            If !Empty(i_im)
                This.InputMask=i_im
            Endif
            This.Format='K'+cp_GetFormat(This.cSayPict)
        Endif
Enddefine

* === Definizione campo MEMO Standard
Define Class StdMemo As CPEditBox
    *
    * --- PROPRIETA' VFP
    *
    Height = 23
    Caption = ""
    * visible = .t.                          && ERRORE Per gli oggetti contenuti va settato da fuori
    FontSize   = 9
    FontBold   = .F.
    FontItalic = .F.
    FontUnderline  = .F.
    FontStrikethru = .F.
    FontName   = ""
    *
    * --- PROPRIETA' CODEPAINTER
    *
    cQueryName = ""
    bObbl = .F.                            && Obbligatorio
    sErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    cLinkFile=''
    cOldIcon=''
    bHasZoom=.F.
    nPag=0
    cFormVar=''
    bIsInHeader=.T.
    bUpd=.F.
    nSeconds=0
    * --
    cfgCalc=''
    cfgCond=''
    cfgCheck=''
    cfgDefault=''
    cfgHide=''
    cfgInit=''
    bNoMenuFunction=.F.           && Disabilita nel tasto destro sottomenu Funzionalit�
    bNoMenuAction=.F.         	&& Disabilita nel tasto destro sottomenu Azioni
    bNoMenuProperty=.F.			&& Disabilita nel tasto destro sottomenu Propiet�
    bNoMenuCut=.F.			    && Disabilita nel tasto destro sottomenu Taglia
    bNoMenuCopy=.F.			    && Disabilita nel tasto destro sottomenu Copia
    bNoMenuPaste=.F.			    && Disabilita nel tasto destro sottomenu Incolla
    bNoMenuSelAll=.F.			    && Disabilita nel tasto destro sottomenu Seleziona Tutto
    bSetFont = .T.
    bGlobalFont=.F.

    *
    * --- COSTRUTTORE/DISTRUTTORE
    *
    * --- Inizializzazione
    Procedure Init()
        If Empty(This.FontName)
            This.FontName   = This.Parent.FontName
            This.FontSize   = This.Parent.FontSize
            This.FontBold   = This.Parent.FontBold
            This.FontItalic = This.Parent.FontItalic
            This.FontUnderline  = This.Parent.FontUnderline
            This.FontStrikethru = This.Parent.FontStrikethru
        Endif
        If This.bSetFont
            This.SetFont()
        Endif
        This.ToolTipText=cp_Translate(This.ToolTipText)
        DoDefault()
        Return

    Procedure SetFont()
		If This.bGlobalFont
			SetFont('M', This)
		Endif
    Endproc
    *
    * --- EVENTI/METODI VFP
    *
    * --- Quando prende il Focus
    Proc GotFocus()
        Local i_var
        Local i_oldvalue
        If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
            This.Parent.oContained.SaveDependsOn()
            i_oldvalue=This.Value
            This.mDefault()
            oCpToolBar.b9.Enabled=This.bHasZoom
            This.bUpd=This.Value<>i_oldvalue
        Endif
        If This.Parent.oContained.cFunction<>"Filter"
            This.bUpd=.F.
            This.mBefore()
            * --- il valore puo' essere cambiato dalla funzione di "Before"
            i_var=This.cFormVar
            If This.Value<>This.Parent.oContained.&i_var
                This.SetControlValue()
                This.bUpd=.T.
            Endif
        Endif
        If !This.bNoBackColor
            This.BackColor = i_nBackColor
        Endif
        Return
    Func When()
        Local bRes,i_e
        * --- Procedure standard del campo
        bRes=.T.
        If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
            bRes = This.mCond()
            If !Empty(This.cfgCond)
                i_e=Strtran(This.cfgCond,'w_','this.parent.oContained.w_')
                bRes=&i_e
            Endif
        Endif
        Return(bRes)
    Func Valid()
        Local i_nRes,i_bErr,i_var,i_bUpd
        i_nRes=1
        * --- Il value ha il nuovo valore, la variabile di work deve essere aggiornata
        i_var=This.cFormVar
        i_bUpd=.F.
        If This.bUpd Or (Not(Trim(This.Value)==Trim(This.Parent.oContained.&i_var)))
            i_bUpd=.T.
            This.Parent.oContained.bUpdated=.T.
            If This.bIsInHeader
                This.Parent.oContained.bHeaderUpdated=.T.
            Endif
            This.Parent.oContained.&i_var=This.Value
            This.SetModified()
            This.bUpd=.F.
        Endif
        * --- After Input solo alla variazione del campo
        If i_bUpd
            This.mAfter()
            * --- il valore puo' essere cambiato dalla funzione di "After"
            If This.Value<>This.Parent.oContained.&i_var
                This.SetControlValue()
            Endif
        Endif
        * ---
        If Not(Inlist(This.Parent.oContained.cFunction,'Filter','Query'))
            i_bErr=.F.
            i_nRes=Iif(i_bUpd,0,1)
            oCpToolBar.b9.Enabled=.F.
            * --- Obbligatorieta'
            If This.bObbl And Empty(This.Value) And This.CondObbl()
                If Not(This.Parent.oContained.bDontReportError)
                    Do cp_ErrorMsg With MSG_FIELD_CANNOT_BE_NULL_QM
                Endif
                i_nRes=0
                i_bErr=.T.
            Endif
            If Not(i_bErr) And i_bUpd
                * --- Procedura di check e calcolo se il campo e' cambiato
                If This.Check()
                    i_nRes=1
                Else
                    If Not(This.Parent.oContained.bDontReportError)
                        Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
                    Endif
                    * --- azzeramento della variabile e del control
                    This.Parent.oContained.&i_var=cp_NullValue(This.Parent.oContained.&i_var)
                    This.SetControlValue()
                Endif
                This.SetTotal()
                This.Parent.oContained.NotifyEvent(i_var+' Changed')
                This.Parent.oContained.mCalc(i_bUpd)
                This.Parent.oContained.SaveDependsOn()
            Else
                This.SetTotal()
            Endif
        Endif
        This.Parent.oContained.bDontReportError=.F.
        Return(i_nRes)
    Proc LostFocus()
        Local i_cFile,i_nConn
        If This.Parent.oContained.cFunction='Query'
            If This.Parent.oContained.bUpdated And Not(Empty(This.cQueryName)) And Upper(This.cQueryName) $ Upper(This.Parent.oContained.cKeyselect) && All'uscita di un campo memo il filtro va in errore
                Nodefault
                i_cFile=This.Parent.oContained.cFile
                i_nConn=i_TableProp[this.parent.oContained.&i_cFile._idx,3]
                cFlt=cp_BuildWhere('',This.Value,This.cQueryName,i_nConn)
                This.Parent.oContained.QueryKeySet(cFlt,This.cQueryName)
                This.Parent.oContained.LoadRecWarn()
            Endif
        Endif
        This.Parent.oContained.NotifyEvent(This.cFormVar+' LostFocus')
        If !This.bNoBackColor
            This.BackColor=i_nEBackColor
        Endif
        Return
        *
        * ------ METODI CODEPAINTER
        *
        * --- Procedura di default standard del campo
    Proc mDefault()
        Return
        * --- Procedura di Conizione standard del campo
    Func mCond()
        Return (.T.)
        * --- Procedura di Hide standard del campo
    Func mHide()
        Return (.F.)
        * --- Procedura di Before input standard del campo
    Proc mBefore()
        Return
        * --- Procedura di After input standard del campo
    Proc mAfter()
        Return
        * --- Procedura di check
    Func Check()
        Return(.T.)
    Func CondObbl()
        Return (.T.)
        * --- Procedure di Drag & Drop
    Proc DragOver(oSource,nXCoord,nYCoord,nState)
        If Not(Empty(This.cLinkFile)) And oSource.ParentClass='Stdcontainer' And oSource.oContained.bLoaded
            Do Case
                Case nState=0   && Enter
                    This.cOldIcon=oSource.DragIcon
                    If oSource.Parent.Parent.Parent.cFile==This.cLinkFile
                        oSource.DragIcon=i_cBmpPath+"cross01.cur"
                    Endif
                Case nState=1   && Leave
                    oSource.DragIcon=This.cOldIcon
            Endcase
        Endif
        Return
    Proc DragDrop(oSource, nXCoord, nYCoord)
        If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And oSource.oContained.bLoaded
            If oSource.cFile==This.cLinkFile
                This.ecpDrop(oSource)
            Endif
        Endif
        Return
    Proc SetModified()
        Return
    Proc ResetTotal()
        Return
    Proc SetTotal()
        Return
    Proc SetControlValue()
        Local i_var,i_cs
        i_var=This.cFormVar
        i_cs=This.ControlSource
        This.Value=This.Parent.oContained.&i_var
        If !(Empty(i_cs))
            Select (This.Parent.oContained.cTrsName)
            Replace &i_cs With This.Value
        Endif
        Return
    Proc mZoom()
        Return
    Proc MouseDown(nButton, nShift, nXCoord, nYCoord)
        If nButton=1
            If Seconds()-0.3<This.nSeconds
                If Inlist(This.Parent.oContained.cFunction,'Edit','Load')
                    Nodefault
                    This.Parent.oContained.bDontReportError=.T.
                    Public i_lastindirectaction
                    i_lastindirectaction='ecpZoom'
                    This.mZoom()
                Endif
            Endif
            This.nSeconds=Seconds()
        Endif
        Return
Enddefine

* === Definizione stringa Standard
Define Class StdString As CPLabel
    *
    * --- PROPRIETA' VFP
    *
    *autosize = .t.
    FontSize   = 9
    FontBold   = .F.
    FontItalic = .F.
    FontUnderline  = .F.
    FontStrikethru = .F.
    FontName   = ""
    BackStyle=0
    * Visible = .t.
    * --
    cfgHide=''
    bSetFont = .T.
    bGlobalFont=.F.
    *
    * --- COSTRUTTORE/DISTRUTTORE
    *
    Procedure Init()
        If Empty(This.FontName)
            This.FontName   = This.Parent.FontName
            This.FontSize   = This.Parent.FontSize
            This.FontBold   = This.Parent.FontBold
            This.FontItalic = This.Parent.FontItalic
            This.FontUnderline  = This.Parent.FontUnderline
            This.FontStrikethru = This.Parent.FontStrikethru
        Endif
        If This.bSetFont
            This.SetFont()
        Endif
        This.Caption=cp_Translate(This.Caption)
        DoDefault()
        Return
    Procedure SetFont()
		If This.bGlobalFont
			SetFont('L', This)
		Endif
    Endproc

    * --- Procedura di Hide standard del campo
    Func mHide()
        Return (.F.)
Enddefine

* === Definizione Button Standard
Define Class StdButton As CPCommandButton
    FontSize   = 9
    FontBold   = .F.
    FontItalic = .F.
    FontUnderline  = .F.
    FontStrikethru = .F.
    FontName   = ""
    * --
    cfgCond=''
    cfgHide=''
    bSetFont = .T.
    bGlobalFont=.F.
    #If Version(5)>=800
        SpecialEffect = i_nBtnSpEfc
    #Endif

    Procedure Init()
        If Empty(This.FontName)
            This.FontName   = This.Parent.FontName
            This.FontSize   = This.Parent.FontSize
            This.FontBold   = This.Parent.FontBold
            This.FontItalic = This.Parent.FontItalic
            This.FontUnderline  = This.Parent.FontUnderline
            This.FontStrikethru = This.Parent.FontStrikethru
        Endif
        If This.bSetFont
            This.SetFont()
        Endif
        This.Caption=cp_Translate(This.Caption)
        This.ToolTipText=cp_Translate(This.ToolTipText)
        DoDefault()
        Return

    Procedure SetFont()
		If This.bGlobalFont
			SetFont('B', This)
		Endif
    Endproc

    Func When()
        Local i_bRes,i_e
        * --- Procedure standard del campo
        i_bRes=.T.
        If Type('this.parent.oContained.cFunction')='O' && bisogna controllare: un bottone che implementa la
            && funzione di sistema "Quit" puo' non trovare piu' il contenitore
            If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
                i_bRes = This.mCond()
                If !Empty(This.cfgCond)
                    i_e=Strtran(This.cfgCond,'w_','this.parent.oContained.w_')
                    i_bRes=&i_e
                Endif
            Endif
        Endif
        Return(i_bRes)
    Func mCond()
        Return(.T.)
        * --- Procedura di Hide standard del campo
    Func mHide()
        Return (.F.)
Enddefine

* === Definizione Box Standard
Define Class StdBox As CPShape
    *
    * --- PROPRIETA' VFP
    *
    SpecialEffect = 0
    BackStyle = 0
    *
    * --- COSTRUTTORE/DISTRUTTORE
    *
    * --- Inizializzazione
    Proc Init()
        This.ZOrder(1)
        If Type("this.parent.pagebmp")='O'
            This.Parent.pagebmp.ZOrder(1)
        Endif
        DoDefault()
        Return
    Procedure RightClick()
        *--- Ribalto il rightclick sul parent
        This.Parent.RightClick()
    Endproc
Enddefine

* === Definizione CheckBox Standard
Define Class StdCheck As CPCheckBox
    *
    * --- PROPRIETA' VFP
    *
    Caption = ""
    BackStyle=0
    AutoSize = .T.
    FontName = ""
    FontSize   = 9
    FontBold   = .F.
    FontItalic = .F.
    FontUnderline  = .F.
    FontStrikethru = .F.
    Value=0
    *
    * --- PROPRIETA' CODEPAINTER
    *
    cQueryName = ""
    bObbl = .F.                            && Obbligatorio
    sErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    cLinkFile=''
    cOldIcon=''
    bHasZoom=.F.
    nPag=0
    cFormVar=''
    bIsInHeader=.T.
    bUpd=.F.
    * --
    cfgCalc=''
    cfgCond=''
    cfgCheck=''
    cfgDefault=''
    cfgHide=''
    cfgInit=''
    bNoMenuFunction=.F.           && Disabilita nel tasto destro sottomenu Funzionalit�
    bNoMenuAction=.T.         	&& Disabilita nel tasto destro sottomenu Azioni
    bNoMenuProperty=.F.			&& Disabilita nel tasto destro sottomenu Propiet�
    bNoMenuCut=.F.			    && Disabilita nel tasto destro sottomenu Taglia
    bNoMenuCopy=.F.			    && Disabilita nel tasto destro sottomenu Copia
    bNoMenuPaste=.F.			    && Disabilita nel tasto destro sottomenu Incolla
    bNoMenuSelAll=.F.			    && Disabilita nel tasto destro sottomenu Seleziona Tutto
    bSetFont = .T.
    bGlobalFont=.F.
    *
    * --- COSTRUTTORE/DISTRUTTORE
    *
    * --- Inizializzazione
    Procedure Init()
        If Empty(This.FontName)
            This.FontName       = This.Parent.FontName
            This.FontSize       = This.Parent.FontSize
            This.FontBold       = This.Parent.FontBold
            This.FontItalic     = This.Parent.FontItalic
            This.FontUnderline  = This.Parent.FontUnderline
            This.FontStrikethru = This.Parent.FontStrikethru
        Endif
        If This.bSetFont
            This.SetFont()
        Endif
        This.Caption=cp_Translate(This.Caption)
        This.ToolTipText=cp_Translate(This.ToolTipText)
        DoDefault()
        Return

    Procedure SetFont()
		If This.bGlobalFont
			SetFont('X', This)
		Endif
    Endproc
    *
    * --- EVENTI/METODI VFP
    *
    * --- Quando prende il Focus
    Proc GotFocus()
        Local i_var
        Local i_oldvalue
        If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
            This.Parent.oContained.SaveDependsOn()
            i_oldvalue=This.Value
            This.mDefault()
            oCpToolBar.b9.Enabled=This.bHasZoom
            This.bUpd=This.Value<>i_oldvalue
        Endif
        If This.Parent.oContained.cFunction<>"Filter"
            This.bUpd=.F.
            This.mBefore()
            * --- il valore puo' essere cambiato dalla funzione di "Before"
            i_var=This.cFormVar
            If This.RadioValue()<>This.Parent.oContained.&i_var
                This.SetControlValue()
                This.bUpd=.T.
            Endif
        Endif
        If !This.bNoBackColor
            This.BackColor = i_nBackColor
        Endif
        Return
    Func When()
        Local i_bRes
        * --- Procedure standard del campo
        i_bRes=.T.
        If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
            i_bRes = This.mCond()
            If !Empty(This.cfgCond)
                i_e=Strtran(This.cfgCond,'w_','this.parent.oContained.w_')
                i_bRes=&i_e
            Endif
        Endif
        Return(i_bRes)
    Func Valid()
        Local i_nRes,i_bErr,i_var,i_bUpd,i_xOldValue
        i_nRes=1
        * --- Il value ha il nuovo valore, la variabile di work deve essere aggiornata
        i_var=This.cFormVar
        i_bUpd=.F.
        i_xOldValue=This.Parent.oContained.&i_var
        If This.bUpd Or (Not(This.RadioValue()==i_xOldValue) And Not(Empty(This.Value) And Empty(This.Parent.oContained.&i_var)))
            i_bUpd=.T.
            This.Parent.oContained.bUpdated=.T.
            If This.bIsInHeader
                This.Parent.oContained.bHeaderUpdated=.T.
            Endif
            This.GetRadio()
            This.SetModified()
            This.bUpd=.F.
        Endif
        * --- After Input solo alla variazione del campo
        If i_bUpd
            This.mAfter()
            If This.RadioValue()<>This.Parent.oContained.&i_var
                This.SetControlValue()
            Endif
        Endif
        * ---
        If Not(Inlist(This.Parent.oContained.cFunction,'Filter','Query'))
            i_bErr=.F.
            i_nRes=Iif(i_bUpd, 0, 1)
            oCpToolBar.b9.Enabled=.F.
            * --- Obbligatorieta'
            If This.bObbl And Empty(This.Value)
                If Not(This.Parent.oContained.bDontReportError)
                    Do cp_ErrorMsg With MSG_FIELD_CANNOT_BE_NULL_QM
                Endif
                i_nRes=0
                i_bErr=.T.
            Endif
            If Not(i_bErr) And i_bUpd
                * --- Procedura di check e calcolo se il campo e' cambiato
                If This.Check()
                    i_nRes=1
                Else
                    If Not(This.Parent.oContained.bDontReportError)
                        Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
                    Endif
                    * --- azzeramento della variabile e del control
                    This.Parent.oContained.&i_var=cp_NullValue(This.Parent.oContained.&i_var)
                    This.SetControlValue()
                Endif
                This.SetTotal(i_xOldValue)
                This.Parent.oContained.NotifyEvent(i_var+' Changed')
                This.Parent.oContained.mCalc(i_bUpd)
                This.Parent.oContained.SaveDependsOn()
            Else
                This.SetTotal(i_xOldValue)
            Endif
        Endif
        This.Parent.oContained.bDontReportError=.F.
        Return(i_nRes)
    Proc LostFocus()
        Local i_cFlt,i_cFile,i_nConn
        If This.Parent.oContained.cFunction='Query'
            If This.Parent.oContained.bUpdated And Not(Empty(This.cQueryName))
                Nodefault
                i_cFile=This.Parent.oContained.cFile
                i_nConn=i_TableProp[this.parent.oContained.&i_cFile._idx,3]
                i_cFlt=cp_BuildWhere('',This.RadioValue(),This.cQueryName,i_nConn)
                This.Parent.oContained.QueryKeySet(i_cFlt,This.cQueryName)
                This.Parent.oContained.LoadRecWarn()
            Endif
        Endif
        This.Parent.oContained.NotifyEvent(This.cFormVar+' LostFocus')
        If !This.bNoBackColor
            This.BackColor=i_nEBackColor
        Endif
        Return
        *
        * ------ METODI CODEPAINTER
        *
        * --- Procedura di default standard del campo
    Proc mDefault()
        Return
        * --- Procedura di Conizione standard del campo
    Func mCond()
        Return (.T.)
        * --- Procedura di Hide standard del campo
    Func mHide()
        Return (.F.)
        * --- Procedura di Before input standard del campo
    Proc mBefore()
        Return
        * --- Procedura di After input standard del campo
    Proc mAfter()
        Return
        * --- Procedura di check
    Func Check()
        Return(.T.)
        * --- Procedure di Drag & Drop
    Proc DragOver(oSource,nXCoord,nYCoord,nState)
        If Not(Empty(This.cLinkFile)) And oSource.ParentClass='Stdcontainer' And oSource.oContained.bLoaded
            Do Case
                Case nState=0   && Enter
                    This.cOldIcon=oSource.DragIcon
                    If oSource.Parent.Parent.Parent.cFile==This.cLinkFile
                        oSource.DragIcon=i_cBmpPath+"cross01.cur"
                    Endif
                Case nState=1   && Leave
                    oSource.DragIcon=This.cOldIcon
            Endcase
        Endif
        Return
    Proc DragDrop(oSource, nXCoord, nYCoord)
        If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And oSource.oContained.bLoaded
            If oSource.cFile==This.cLinkFile
                This.ecpDrop(oSource)
            Endif
        Endif
        Return
    Proc SetModified()
        Return
    Proc SetTotal(i_xOldValue)
        Return
    Proc SetControlValue()
        Local i_var,i_cs
        i_var=This.cFormVar
        i_cs=This.ControlSource
        This.SetRadio()
        If !(Empty(i_cs))
            Select (This.Parent.oContained.cTrsName)
            Replace &i_cs With This.Value
        Endif
        Return
Enddefine
* ---

* === Definizione Radio Button Standard
Define Class StdRadio As CPOptionGroup
    *
    * --- PROPRIETA' VFP
    *
    BackStyle=0
    Visible = .T.
    BorderStyle = 0
    ButtonCount=1
    FntName   = ""
    FntSize   = 9
    FntBold   = .F.
    FntItalic = .F.
    FntUnderline  = .F.
    FntStrikeThru = .F.
    Value=0
    *
    * --- PROPRIETA' CODEPAINTER
    *
    cQueryName = ""
    bObbl = .F.                            && Obbligatorio
    sErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    cLinkFile=''
    cOldIcon=''
    bHasZoom=.F.
    nPag=0
    cFormVar=''
    bIsInHeader=.T.
    bUpd=.F.
    ToolTipText=''
    * --
    cfgCalc=''
    cfgCond=''
    cfgCheck=''
    cfgDefault=''
    cfgHide=''
    cfgInit=''
    bNoMenuFunction=.F.           && Disabilita nel tasto destro sottomenu Funzionalit�
    bNoMenuAction=.T.         	&& Disabilita nel tasto destro sottomenu Azioni
    bNoMenuProperty=.F.			&& Disabilita nel tasto destro sottomenu Propiet�
    bNoMenuCut=.F.			    && Disabilita nel tasto destro sottomenu Taglia
    bNoMenuCopy=.F.			    && Disabilita nel tasto destro sottomenu Copia
    bNoMenuPaste=.F.			    && Disabilita nel tasto destro sottomenu Incolla
    bNoMenuSelAll=.F.			    && Disabilita nel tasto destro sottomenu Seleziona Tutto
    bSetFont = .T.
    bGlobalFont=.F.
    bNoBackColor = .F.
    bNoDisabledBackColor = .F.
    *
    * --- COSTRUTTORE/DISTRUTTORE
    *
    * --- Inizializzazione
    Procedure Init()
        Local i
        If This.bSetFont
            This.SetFont()
        Endif
        If Empty(This.FntName)
            This.SetAll("FontName",This.Parent.FontName)
            This.SetAll("FontSize",This.Parent.FontSize)
            This.SetAll("FontBold",This.Parent.FontBold)
            This.SetAll("FontItalic",This.Parent.FontItalic)
            This.SetAll("FontUnderline",This.Parent.FontUnderline)
            This.SetAll("FontStrikeThru",This.Parent.FontStrikethru)
        Else
            This.SetAll("FontName",This.FntName)
            This.SetAll("FontSize",This.FntSize)
            This.SetAll("FontBold",This.FntBold)
            This.SetAll("FontItalic",This.FntItalic)
            This.SetAll("FontUnderline",This.FntUnderline)
            This.SetAll("FontStrikeThru",This.FntStrikeThru)
        Endif
        For i=1 To This.ButtonCount
            This.Buttons(i).Caption=cp_Translate(This.Buttons(i).Caption)
        Next
        Local translatedtooltiptext
        translatedtooltiptext=cp_Translate(This.ToolTipText)
        This.SetAll("ToolTipText",translatedtooltiptext)
        DoDefault()
        Return
    Procedure SetFont()
        This.FntName = SetFontName('R', Evl(This.FntName, This.Parent.FontName), This.bGlobalFont)
        This.FntSize = SetFontSize('R', Evl(This.FntSize, This.Parent.FontSize), This.bGlobalFont)
        This.FntItalic = SetFontItalic('R', Evl(This.FntItalic, This.Parent.FontItalic), This.bGlobalFont)
        This.FntBold = SetFontBold('R', Evl(This.FntBold, This.Parent.FontBold), This.bGlobalFont)
    Endproc
    *
    * --- EVENTI/METODI VFP
    *
    * --- Quando prende il Focus
    Proc GotFocus()
        Local i_var
        Local i_oldvalue
        If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
            This.Parent.oContained.SaveDependsOn()
            i_oldvalue=This.Value
            This.mDefault()
            oCpToolBar.b9.Enabled=This.bHasZoom
            This.bUpd=This.Value<>i_oldvalue
        Endif
        If This.Parent.oContained.cFunction<>"Filter"
            This.bUpd=.F.
            This.mBefore()
            i_var=This.cFormVar
            If This.RadioValue()<>This.Parent.oContained.&i_var
                This.SetControlValue()
                This.bUpd=.T.
            Endif
        Endif
        If !This.bNoBackColor
            This.BackColor = i_nBackColor
        Endif
        Return
    Func When()
        Local i_bRes
        * --- Procedure standard del campo
        i_bRes=.T.
        If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
            i_bRes = This.mCond()
        Endif
        Return(i_bRes)
    Func Valid()
        Local i_nRes,i_bErr,i_var,i_bUpd,i_xOldValue
        i_nRes=1
        * --- Il value ha il nuovo valore, la variabile di work deve essere aggiornata
        i_var=This.cFormVar
        i_bUpd=.F.
        i_xOldValue=This.Parent.oContained.&i_var
        If This.bUpd Or (Not(This.RadioValue()==i_xOldValue) And Not(Empty(This.Value) And Empty(This.Parent.oContained.&i_var)))
            i_bUpd=.T.
            This.Parent.oContained.bUpdated=.T.
            If This.bIsInHeader
                This.Parent.oContained.bHeaderUpdated=.T.
            Endif
            This.GetRadio()
            This.SetModified()
            This.bUpd=.F.
        Endif
        * --- After Input solo alla variazione del campo
        If i_bUpd
            This.mAfter()
            If This.RadioValue()<>This.Parent.oContained.&i_var
                This.SetControlValue()
            Endif
        Endif
        * ---
        If Not(Inlist(This.Parent.oContained.cFunction,'Filter','Query'))
            i_bErr=.F.
            i_nRes=Iif(i_bUpd, 0, 1)
            oCpToolBar.b9.Enabled=.F.
            * --- Obbligatorieta'
            If This.bObbl And Empty(This.Value)
                If Not(This.Parent.oContained.bDontReportError)
                    Do cp_ErrorMsg With MSG_FIELD_CANNOT_BE_NULL_QM
                Endif
                i_nRes=0
                i_bErr=.T.
            Endif
            If Not(i_bErr) And i_bUpd
                * --- Procedura di check e calcolo se il campo e' cambiato
                If This.Check()
                    i_nRes=1
                Else
                    If Not(This.Parent.oContained.bDontReportError)
                        Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
                    Endif
                    This.Parent.oContained.&i_var=cp_NullValue(This.Parent.oContained.&i_var)
                    This.SetControlValue()
                Endif
                This.SetTotal(i_xOldValue)
                This.Parent.oContained.NotifyEvent(i_var+' Changed')
                This.Parent.oContained.mCalc(i_bUpd)
                This.Parent.oContained.SaveDependsOn()
            Else
                This.SetTotal(i_xOldValue)
            Endif
        Endif
        This.Parent.oContained.bDontReportError=.F.
        Return(i_nRes)
    Proc LostFocus()
        Local i_cFlt,i_cFile,i_nConn
        If This.Parent.oContained.cFunction='Query'
            If This.Parent.oContained.bUpdated And Not(Empty(This.cQueryName))
                Nodefault
                i_cFile=This.Parent.oContained.cFile
                i_nConn=i_TableProp[this.parent.oContained.&i_cFile._idx,3]
                i_cFlt=cp_BuildWhere('',This.RadioValue(),This.cQueryName,i_nConn)
                This.Parent.oContained.QueryKeySet(i_cFlt,This.cQueryName)
                This.Parent.oContained.LoadRecWarn()
            Endif
        Endif
        This.Parent.oContained.NotifyEvent(This.cFormVar+' LostFocus')
        If !This.bNoBackColor
            This.BackColor=i_nEBackColor
        Endif
        Return
        *
        * ------ METODI CODEPAINTER
        *
        * --- Procedura di default standard del campo
    Proc mDefault()
        Return
        * --- Procedura di Conizione standard del campo
    Func mCond()
        Return (.T.)
        * --- Procedura di Hide standard del campo
    Func mHide()
        Return (.F.)
        * --- Procedura di Before input standard del campo
    Proc mBefore()
        Return
        * --- Procedura di After input standard del campo
    Proc mAfter()
        Return
        * --- Procedura di check
    Func Check()
        Return(.T.)
        * --- Procedure di Drag & Drop
    Proc DragOver(oSource,nXCoord,nYCoord,nState)
        If Not(Empty(This.cLinkFile)) And oSource.ParentClass='Stdcontainer' And oSource.oContained.bLoaded
            Do Case
                Case nState=0   && Enter
                    This.cOldIcon=oSource.DragIcon
                    If oSource.Parent.Parent.Parent.cFile==This.cLinkFile
                        oSource.DragIcon=i_cBmpPath+"cross01.cur"
                    Endif
                Case nState=1   && Leave
                    oSource.DragIcon=This.cOldIcon
            Endcase
        Endif
        Return
    Proc DragDrop(oSource, nXCoord, nYCoord)
        If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And oSource.oContained.bLoaded
            If oSource.cFile==This.cLinkFile
                This.ecpDrop(oSource)
            Endif
        Endif
        Return
    Proc SetModified()
        Return
    Proc ResetTotal()
        Return
    Proc SetTotal(i_xOldValue)
        Return
    Proc SetControlValue()
        Local i_var,i_cs
        i_var=This.cFormVar
        i_cs=This.ControlSource
        This.SetRadio()
        If !(Empty(i_cs))
            Select (This.Parent.oContained.cTrsName)
            Replace &i_cs With This.Value
        Endif
        Return
    Proc SetFocus()
        This.Buttons(1).SetFocus()
        Return
    Proc Enabled_(bEnable)
        Local i
        For i=1 To This.ButtonCount
            This.Buttons(i).Enabled=bEnable
        Next
        Return
Enddefine
* ---

* === Definizione ComboBox Standard
Define Class StdCombo As CPComboBox
    *
    * --- PROPRIETA' VFP
    *
    RowSourceType = 0
    FontName   = ""
    FontSize   = 9
    FontBold   = .F.
    FontItalic = .F.
    FontUnderline  = .F.
    FontStrikethru = .F.
    Value=-1
    Style=2
    *
    * --- PROPRIETA' CODEPAINTER
    *
    cQueryName = ""
    bObbl = .F.                            && Obbligatorio
    sErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    cLinkFile=''
    cOldIcon=''
    bHasZoom=.F.
    nPag=0
    cFormVar=''
    bIsInHeader=.T.
    bUpd=.F.
    * --
    cfgCalc=''
    cfgCond=''
    cfgCheck=''
    cfgDefault=''
    cfgHide=''
    cfgInit=''
    bNoMenuFunction=.F.           && Disabilita nel tasto destro sottomenu Funzionalit�
    bNoMenuAction=.T.         	&& Disabilita nel tasto destro sottomenu Azioni
    bNoMenuProperty=.F.			&& Disabilita nel tasto destro sottomenu Propiet�
    bNoMenuCut=.F.			    && Disabilita nel tasto destro sottomenu Taglia
    bNoMenuCopy=.F.			    && Disabilita nel tasto destro sottomenu Copia
    bNoMenuPaste=.F.			    && Disabilita nel tasto destro sottomenu Incolla
    bNoMenuSelAll=.F.			    && Disabilita nel tasto destro sottomenu Seleziona Tutto
    bSetFont = .T.
    bGlobalFont=.F.
    *
    * --- COSTRUTTORE/DISTRUTTORE
    *
    * --- Inizializzazione
    Procedure Init()
        If Empty(This.FontName)
            This.FontName   = This.Parent.FontName
            This.FontSize   = This.Parent.FontSize
            This.FontBold   = This.Parent.FontBold
            This.FontItalic = This.Parent.FontItalic
            This.FontUnderline  = This.Parent.FontUnderline
            This.FontStrikethru = This.Parent.FontStrikethru
        Endif
        If This.bSetFont
            This.SetFont()
        Endif
        If .T. Or Type('i_cLanguage')<>'U' And !Empty(i_cLanguage)
            Local i,j,s,o
            o=This.RowSource
            s=''
            i=At(',',o)
            Do While i<>0
                *s=s+','+cp_Translate(left(o,i-1))
                This.AddItem(cp_Translate(Left(o,i-1)))
                o=Substr(o,i+1)
                i=At(',',o)
            Enddo
            *s=s+','+cp_Translate(o)
            *this.RowSource=substr(s,2)
            This.AddItem(cp_Translate(o))
        Endif
        This.ToolTipText=cp_Translate(This.ToolTipText)
        DoDefault()
        Return
    Procedure SetFont()
		If This.bGlobalFont
			SetFont('C', This)
		Endif
    Endproc
    *
    * --- EVENTI/METODI VFP
    *
    * --- Quando prende il Focus
    Proc GotFocus()
        Local i_var
        Local i_oldvalue
        If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
            This.Parent.oContained.SaveDependsOn()
            i_oldvalue=This.Value
            This.mDefault()
            oCpToolBar.b9.Enabled=This.bHasZoom
            This.bUpd=This.Value<>i_oldvalue
        Endif
        If This.Parent.oContained.cFunction<>"Filter"
            This.bUpd=.F.
            This.mBefore()
            * --- il valore puo' essere cambiato dalla funzione di "Before"
            i_var=This.cFormVar
            If This.RadioValue()<>This.Parent.oContained.&i_var
                This.SetControlValue()
                This.bUpd=.T.
            Endif
        Endif
        If !This.bNoBackColor
            This.BackColor = i_nBackColor
        Endif
        * --- Numero di elementi nella lista di drop down della combo
        If Vartype(g_DispCnt)='N'
            This.DisplayCount = g_DispCnt
        Endif
        Return
    Func When()
        Local i_bRes
        * --- Procedure standard del campo
        i_bRes=.T.
        If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
            i_bRes = This.mCond()
            If !Empty(This.cfgCond)
                i_e=Strtran(This.cfgCond,'w_','this.parent.oContained.w_')
                i_bRes=&i_e
            Endif
        Endif
        Return(i_bRes)
    Func Valid()
        Local i_nRes,i_bErr,i_var,i_bUpd,i_xOldValue
        i_nRes=1
        * --- Il value ha il nuovo valore, la variabile di work deve essere aggiornata
        i_var=This.cFormVar
        i_bUpd=.F.
        i_xOldValue=This.Parent.oContained.&i_var
        If This.bUpd Or (Not(This.RadioValue()==i_xOldValue) And Not(Empty(This.Value) And Empty(This.Parent.oContained.&i_var)))
            i_bUpd=.T.
            This.Parent.oContained.bUpdated=.T.
            If This.bIsInHeader
                This.Parent.oContained.bHeaderUpdated=.T.
            Endif
            This.GetRadio()
            This.SetModified()
            This.bUpd=.F.
        Endif
        * --- After Input solo alla variazione del campo
        If i_bUpd
            This.mAfter()
            If This.RadioValue()<>This.Parent.oContained.&i_var
                This.SetControlValue()
            Endif
        Endif
        * ---
        If Not(Inlist(This.Parent.oContained.cFunction,'Filter','Query'))
            i_bErr=.F.
            i_nRes=Iif(i_bUpd, 0, 1)
            oCpToolBar.b9.Enabled=.F.
            * --- Obbligatorieta'
            If This.bObbl And Empty(Nvl(This.RadioValue(),"")) And This.CondObbl()
                If Not(This.Parent.oContained.bDontReportError)
                    Do cp_ErrorMsg With MSG_FIELD_CANNOT_BE_NULL_QM
                Endif
                i_nRes=0
                i_bErr=.T.
            Endif
            If Not(i_bErr) And i_bUpd
                * --- Procedura di check e calcolo se il campo e' cambiato
                If This.Check()
                    i_nRes=1
                Else
                    If Not(This.Parent.oContained.bDontReportError)
                        Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
                    Endif
                    This.Parent.oContained.&i_var=cp_NullValue(This.Parent.oContained.&i_var)
                    This.SetControlValue()
                Endif
                This.SetTotal(i_xOldValue)
                This.Parent.oContained.NotifyEvent(i_var+' Changed')
                This.Parent.oContained.mCalc(i_bUpd)
                This.Parent.oContained.SaveDependsOn()
            Else
                This.SetTotal(i_xOldValue)
            Endif
        Endif
        This.Parent.oContained.bDontReportError=.F.
        Return(i_nRes)
    Proc LostFocus()
        Local i_cFlt,i_cFile,i_nConn
        If This.Parent.oContained.cFunction='Query'
            If This.Parent.oContained.bUpdated And Not(Empty(This.cQueryName))
                Nodefault
                i_cFile=This.Parent.oContained.cFile
                i_nConn=i_TableProp[this.parent.oContained.&i_cFile._idx,3]
                i_cFlt=cp_BuildWhere('',This.Value,This.cQueryName,i_nConn)
                This.Parent.oContained.QueryKeySet(i_cFlt,This.cQueryName)
                This.Parent.oContained.LoadRecWarn()
            Endif
        Endif
        This.Parent.oContained.NotifyEvent(This.cFormVar+' LostFocus')
        If !This.bNoBackColor
            This.BackColor=i_nEBackColor
        Endif
        Return
        *
        * ------ METODI CODEPAINTER
        *
        * --- Procedura di default standard del campo
    Proc mDefault()
        Return
        * --- Procedura di Conizione standard del campo
    Func mCond()
        Return(.T.)
        * --- Procedura di Hide standard del campo
    Func mHide()
        Return (.F.)
        * --- Procedura di Before input standard del campo
    Proc mBefore()
        Return
        * --- Procedura di After input standard del campo
    Proc mAfter()
        Return
        * --- Procedura di check
    Func Check()
        Return(.T.)
        * --- Procedure di Drag & Drop
    Proc DragOver(oSource,nXCoord,nYCoord,nState)
        If Not(Empty(This.cLinkFile)) And oSource.ParentClass='Stdcontainer' And oSource.oContained.bLoaded
            Do Case
                Case nState=0   && Enter
                    This.cOldIcon=oSource.DragIcon
                    If oSource.Parent.Parent.Parent.cFile==This.cLinkFile
                        oSource.DragIcon=i_cBmpPath+"cross01.cur"
                    Endif
                Case nState=1   && Leave
                    oSource.DragIcon=This.cOldIcon
            Endcase
        Endif
        Return
    Proc DragDrop(oSource, nXCoord, nYCoord)
        If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And oSource.oContained.bLoaded
            If oSource.cFile==This.cLinkFile
                This.ecpDrop(oSource)
            Endif
        Endif
        Return
    Proc SetModified()
        Return
    Proc SetTotal(i_xOldValue)
        Return
    Proc SetControlValue()
        Local i_var,i_cs
        i_var=This.cFormVar
        i_cs=This.ControlSource
        This.SetRadio()
        If !(Empty(i_cs))
            Select (This.Parent.oContained.cTrsName)
            Replace &i_cs With This.Value
        Endif
        Return
    Func CondObbl()
        Return (.T.)
Enddefine
* ---

Define Class StdTableCombo As StdCombo
    Dimension combovalues[1]
    nValues=0
    RowSourceType=0
    tablefilter=''
    bSetFont = .T.
    bNoBackColor=.F.
    bNoDisabledBackColor = .F.

    Proc Init()
        This.BackColor=Iif(This.bNoBackColor, This.BackColor, i_nEBackColor)
        This.DisabledBackColor = Iif(This.bNoDisabledBackColor, This.DisabledBackColor, i_udisabledbackcolor)
        This.ToolTipText=cp_Translate(This.ToolTipText)
        Local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
        Local i_fk,i_fd
        i_curs=Sys(2015)
        If Lower(Right(This.cTable,4))='.vqr'
            *--- Inizio richiesta parametri combotabellare
            vq_exec(This.cTable,Thisform,i_curs)
            i_fk=This.cKey
            i_fd=This.cValue
        Else
            i_nIdx=cp_OpenTable(This.cTable)
            If i_nIdx<>0
                i_nConn=i_TableProp[i_nIdx,3]
                i_cTable=cp_SetAzi(i_TableProp[i_nIdx,2])
                i_n1=This.cKey
                i_n2=This.cValue
                If !Empty(This.cOrderBy)
                    i_n3=' order by '+This.cOrderBy
                Else
                    i_n3=''
                Endif
                i_flt=Iif(Empty(This.tablefilter),'',' where '+This.tablefilter)
                If i_nConn<>0
                    cp_sql(i_nConn,"select "+i_n1+" as combokey,"+i_n2+" as combodescr from "+i_cTable+i_flt+i_n3,i_curs)
                Else
                    Select &i_n1 As combokey,&i_n2 As combodescr From (i_cTable) &i_flt &i_n3 Into Cursor (i_curs)
                Endif
                i_fk='combokey'
                i_fd='combodescr'
                cp_CloseTable(This.cTable)
            Endif
        Endif
        If Used(i_curs)
            Select (i_curs)
            This.nValues=Reccount()+1
            Dimension This.combovalues[MAX(1,this.nValues)]
            i_bCharKey=Type(i_fk)='C'
            *this.AddItem(cp_Translate('<Nessun Valore>'))
            This.AddItem(' ')
            This.combovalues[1]=Iif(i_bCharKey,Space(1),0)
            Do While !Eof()
                This.AddItem(Iif(Type(i_fd)$'CM',Alltrim(&i_fd),Alltrim(Str(&i_fd))))
                If i_bCharKey
                    This.combovalues[recno()+1]=Trim(&i_fk)
                Else
                    This.combovalues[recno()+1]=&i_fk
                Endif
                Skip
            Enddo
            Use
        Endif
        If This.bSetFont
            This.SetFont()
        Endif
    Func RadioValue()
        If This.Value>0 And This.Value<=This.nValues
            Return(This.combovalues[this.value])
        Endif
        Return(This.xDefault)
    Func SetRadio()
        Local i_n
        i_n=This.cFormVar
        If Type('this.Parent.oContained.'+i_n)='C'
            This.Parent.oContained.&i_n.=Trim(This.Parent.oContained.&i_n.)
        Endif
        *--- premendo esc se un master con combo in hide viene evidenziato l'errore che non esiste l'oggetto
        If Type('this.Parent.oContained.'+i_n)<>'U'
            If Version(5)>=900
                This.Value=Ascan(This.combovalues,This.Parent.oContained.&i_n., -1, -1, -1, 6)
            Else
                Local cSetExact
                cSetExact=Set("EXACT")
                Set Exact On
                This.Value=Ascan(This.combovalues,This.Parent.oContained.&i_n.)
                Set Exact &cSetExact
            Endif
        Endif
        Return
    Func GetRadio()
        Local i_n
        i_n=This.cFormVar
        This.Parent.oContained.&i_n.=This.RadioValue()
        Return(.T.)
Enddefine

Define Class StdTrsField As StdField
    cTotal=''
    *
    * --- questa implementazione serve a similare il @K che sulle righe sembra
    *     non funzionare correttamente
    isprimarykey=.F.
    bFixedPos=.F.
    oldvalue=0
    Procedure Init()
        DoDefault()
        If !This.bFixedPos
            This.BackStyle=Iif(i_nDtlRowClr=Rgb(255,255,255),1,0)
        Endif
        Return

        Local l_olderr,l_err
        l_olderr=On('Error')
        l_err=.F.
        On Error l_err=.T.

        Local L_Cmd,l_oBJ
        If Not Empty(This.Parent.oContained.cOldbtn)
            L_Cmd=This.Parent.oContained.cOldbtn+'.visible'
            l_oBJ=This.Parent.oContained.cOldbtn+'.visible'
            If Type("&l_obj")='L'
                &L_Cmd=.T.
            Endif
        Endif

        * visualizzo il bottone
        If Type('this.parent.'+This.zbName)='O'
            L_Cmd='this.parent.'+This.zbName+'.visible'
            l_oBJ='this.parent.'+This.zbName+'.visible'
            If Type("&L_obj")='L'
                &L_Cmd = .T.
            Endif
            This.Parent.oContained.cOldbtn='this.parent.'+This.zbName
        Endif

        * visualizzo il bottone per la gestione delle traduzioni
        If Type('this.parent.'+This.transbName)='O'
            L_Cmd='this.parent.'+This.transbName+'.visible'
            l_oBJ='this.parent.'+This.transbName+'.visible'
            If Type("&L_obj")='L'
                &L_Cmd = .T.
            Endif
            This.Parent.oContained.cOldbtn='this.parent.'+This.transbName
        Endif

        On Error &l_olderr

        Return
        * ---
    Proc SetModified()
        Select (This.Parent.oContained.cTrsName)
        If i_SRV<>'A'
            Replace i_SRV With 'U'
        Endif
        Return
    Proc ResetTotal()
        This.oldvalue=This.Value
        Return
    Proc SetTotal()
        Local i_tot
        If Not(Empty(This.cTotal))
            i_tot=This.cTotal
            &i_tot=&i_tot+This.Value-This.oldvalue
            This.oldvalue=This.Value
        Endif
        Return
    Function CheckObbl()
        Return This.Parent.oContained.FullRow()
Enddefine

Define Class StdTrsMemo As StdMemo
    cTotal=''
    oldvalue=0
    Proc SetModified()
        Select (This.Parent.oContained.cTrsName)
        If i_SRV<>'A'
            Replace i_SRV With 'U'
        Endif
        Return
    Proc ResetTotal()
        This.oldvalue=This.Value
        Return
    Proc SetTotal()
        Local i_tot
        If Not(Empty(This.cTotal))
            i_tot=This.cTotal
            &i_tot=&i_tot+This.Value-This.oldvalue
            This.oldvalue=This.Value
        Endif
        Return
Enddefine

Define Class StdTrsCheck As StdCheck
    cTotal=''
    Proc SetModified()
        Select (This.Parent.oContained.cTrsName)
        If i_SRV<>'A'
            Replace i_SRV With 'U'
        Endif
        Return
    Proc SetTotal(i_xOldValue)
        Local i_tot
        If Not(Empty(This.cTotal))
            i_tot=This.cTotal
            &i_tot=&i_tot+This.RadioValue()-i_xOldValue
        Endif
        Return
Enddefine

Define Class StdTrsRadio As StdRadio
    cTotal=''
    Proc SetModified()
        Select (This.Parent.oContained.cTrsName)
        If i_SRV<>'A'
            Replace i_SRV With 'U'
        Endif
        Return
    Proc SetTotal(i_xOldValue)
        Local i_tot
        If Not(Empty(This.cTotal))
            i_tot=This.cTotal
            &i_tot=&i_tot+This.RadioValue()-i_xOldValue
        Endif
        Return
Enddefine

Define Class StdTrsCombo As StdCombo
    cTotal=''
    Proc SetModified()
        Select (This.Parent.oContained.cTrsName)
        If i_SRV<>'A'
            Replace i_SRV With 'U'
        Endif
        Return
    Proc SetTotal(i_xOldValue)
        Local i_tot
        If Not(Empty(This.cTotal))
            i_tot=This.cTotal
            &i_tot=&i_tot+This.RadioValue()-i_xOldValue
        Endif
        Return
Enddefine

Define Class StdTrsTableCombo As StdTableCombo
    cTotal=''
    Proc SetModified()
        Select (This.Parent.oContained.cTrsName)
        If i_SRV<>'A'
            Replace i_SRV With 'U'
        Endif
        Return
    Proc SetTotal(i_xOldValue)
        Local i_tot
        If Not(Empty(This.cTotal))
            i_tot=This.cTotal
            &i_tot=&i_tot+This.RadioValue()-i_xOldValue
        Endif
        Return
    Func RadioValue(i_bTrs)
        Local i_xVal,i_n
        If i_bTrs
            i_n=Substr(This.cFormVar,3)
            i_xVal=t_&i_n.
        Else
            i_xVal=This.Value
        Endif
        If i_xVal>0 And i_xVal<=This.nValues
            Return(This.combovalues[i_xVal])
        Endif
        Return(This.xDefault)
    Func ToRadio()
        Local i_n
        i_n=This.cFormVar
        If Type('this.Parent.oContained.'+i_n)='C'
            This.Parent.oContained.&i_n.=Trim(This.Parent.oContained.&i_n.)
        Endif
        Local nRetVal
        If Version(5)>=900
            nRetVal=Ascan(This.combovalues,This.Parent.oContained.&i_n., -1, -1, -1, 6)
        Else
            Local cSetExact
            cSetExact=Set("EXACT")
            Set Exact On
            nRetVal=Ascan(This.combovalues,This.Parent.oContained.&i_n.)
            Set Exact &cSetExact
        Endif
        Return(nRetVal)
Enddefine
* ---

Define Class StdBody As CPGrid
    * --- Proprieta' VFP
    FontName = i_cProjectFontName
    FontSize=9
    FontBold=.F.
    FontItalic=.F.
    FontUnderline=.F.
    FontStrikethru=.F.
    GridLineColor = i_nGridLineColor
    * --- Proprieta' CP
    oFirstControl=.Null.
    cLinkFile=''
    cOldIcon=''
    nRelRow=0
    nAbsRow=0
    *bRowChanged=.f.
    nPrevRow=0
    nBeforeAfter=0
    AllowHeaderSizing=.F.
    Proc BeforeRowColChange(nIdx)
        *ACTIVATE SCREEN
        *? 'before',nIdx,RECNO(),this.nAbsRow
        If Type("this.parent.parent.parent.parent._paint_error_c")='N' And This.Parent.Parent.Parent.Parent._paint_error_c>=6
            Nodefault
            Return
        Endif
        If This.nBeforeAfter<1
            This.nBeforeAfter=This.nBeforeAfter+1
        Endif
        Return
    Proc AfterRowColChange(nIdx)
        *ACTIVATE SCREEN
        *? 'after',nIdx,RECNO(),this.nAbsRow
        If This.nBeforeAfter>0
            This.nBeforeAfter=This.nBeforeAfter-1
        Endif
        If This.nBeforeAfter=0
            Thisform.LockScreen=.F.
        Endif
        If !Inlist(This.Parent.oContained.cFunction,'Edit','Load')
            This.SetCurrentRow()
            This.Refresh()
        Endif
        Return
    Proc Click()
        Thisform.LockScreen=.T.
        This.Parent.oContained.__dummy__.Enabled=.T.
        This.Parent.oContained.__dummy__.SetFocus()
        If !(Alias()==This.Parent.oContained.cTrsName)
            Select (This.Parent.oContained.cTrsName)
        Endif
        If This.Parent.oContained.CheckRow()
            Go Bottom
            This.Parent.oContained.WorkFromTrs()
            If This.Parent.oContained.FullRow() And This.Parent.oContained.CanAddRow() And This.Parent.oContained.cFunction<>'Query'
                This.Parent.oContained.InitRow()
            Else
                This.Parent.oContained.ChildrenChangeRow()
            Endif
        Endif
        This.SetFocus()
        This.Parent.oContained.__dummy__.Enabled=.F.
        Thisform.LockScreen=.F.
        Return
    Proc mZoom()
        This.oBodyCol.oRow.ActiveControl.mZoom()
        Return
    Proc mZoomOnZoom()
        This.oBodyCol.oRow.ActiveControl.mZoomOnZoom()
        Return
    Proc SetFullFocus()
        This.SetFocus()
        This.oFirstControl.SetFocus()
        Return
    Proc DragOver(oSource,nXCoord,nYCoord,nState)
        If Not(Empty(This.cLinkFile)) And oSource.ParentClass='Stdcontainer' And oSource.oContained.bLoaded
            Do Case
                Case nState=0   && Enter
                    This.cOldIcon=oSource.DragIcon
                    If oSource.Parent.Parent.Parent.cFile+'|'$This.cLinkFile
                        oSource.DragIcon=i_cBmpPath+"cross01.cur"
                    Endif
                Case nState=1   && Leave
                    oSource.DragIcon=This.cOldIcon
            Endcase
        Endif
        Return
    Proc DragDrop(oSource, nXCoord, nYCoord)
        Local nRiga,nCurrRec,nRec,oDropInto,i
        If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And oSource.oContained.bLoaded
            oDropInto=This.GetDropTarget(oSource.cFile,nXCoord,nYCoord)
            If Not(Isnull(oDropInto))
                If This.Parent.oContained.CheckRow()
                    Select (This.Parent.oContained.cTrsName)
                    nRiga=Int((nYCoord-cp_AbsTop(This,0))/This.RowHeight)+1
                    nCurrRec=Recno()
                    nRec=Recno()-Iif(This.nRelRow=0,1,This.nRelRow)+nRiga
                    If nRec>Reccount()
                        Go Bottom
                        This.Parent.oContained.WorkFromTrs()
                        If This.Parent.oContained.FullRow() And This.Parent.oContained.CanAddRow()
                            This.Parent.oContained.InitRow()
                        Endif
                    Else
                        Goto nRec
                        This.Parent.oContained.WorkFromTrs()
                    Endif
                    This.SetFullFocus()
                    oDropInto.ecpDrop(oSource)
                    This.Parent.oContained.mCalc(.T.)
                    This.Parent.oContained.SaveDependsOn()
                    If i_SRV<>'A'
                        Replace i_SRV With 'U'
                    Endif
                    This.Parent.oContained.TrsFromWork()
                    Goto nCurrRec
                    oSource.SetFocus()
                Else
                    oSource.SetFocus()
                    This.Parent.oContained.oNewFocus=.Null.
                Endif
            Endif
        Endif
        Return
    Func GetDropTarget(cFile,nX,nY)
        Return(.Null.)
Enddefine

Define Class BodyKeyMover As CPCommandButton
    Height=0
    Width=0
    BorderWidth=0
    Style=1
    nDirection=0
    bMoveFocus=.F.
    Proc GotFocus()
        If !This.bMoveFocus
            If This.nDirection=-1
                If Inlist(Lastkey(),5,15)
                    This.Parent.oBody.SetFullFocus()
                Else
                    Keyboard '{TAB}'
                Endif
            Else
                If Inlist(Lastkey(),5,15)
                    Keyboard '{UPARROW}'
                Else
                    This.Parent.oBody.SetFullFocus()
                Endif
            Endif
        Endif
        Return
    Proc MoveFocus()
        This.bMoveFocus=.T.
        This.SetFocus()
        If This.nDirection=-1
            Thisform.LockScreen=.F.
            Keyboard '{TAB}'
        Else
            Keyboard '{UPARROW}'
        Endif
        This.bMoveFocus=.F.
        Return
Enddefine

Define Class LastKeyMover As CPCommandButton
    Height=0
    Width=0
    Style=1
    BorderWidth=0
    bHasFixedBody=.F.
    bMoveFocus=.F.
    Proc GotFocus()
        If !This.bMoveFocus
            If This.bHasFixedBody
                This.Parent.Parent.Parent.Parent.oBeginFixedBody.MoveFocus()
            Else
                If This.Parent.oContained.CheckRow()
                    Thisform.LockScreen=.T.
                    Select (This.Parent.oContained.cTrsName)
                    *if recno()>=reccount()
                    If Recno()>=This.Parent.oContained.nLastRow
                        If This.Parent.oContained.FullRow() And This.Parent.oContained.CanAddRow()
                            This.Parent.oContained.InitRow()
                        Else
                            This.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
                        Endif
                    Else
                        This.Parent.oContained.i_lastcheckrow=This.Parent.oContained.oPgFrm.Page1.oPag.oBody.nAbsRow
                        If This.Parent.oContained.oPgFrm.Page1.oPag.oBody.RelativeRow=(This.Parent.oContained.i_nRowPerPage-1)
                            This.Parent.oContained.oPgFrm.Page1.oPag.oBody.DoScroll(1)
                        Endif
                        Skip
                        This.Parent.Parent.Parent.SetFullFocus()
                    Endif
                Else
                    If Not(Isnull(This.Parent.oContained.oNewFocus))
                        This.Parent.oContained.oNewFocus.SetFocus()
                        This.Parent.oContained.oNewFocus=.Null.
                    Endif
                Endif
            Endif
        Endif
        Return
    Proc MoveFocus()
        This.bMoveFocus=.T.
        This.SetFocus()
        Keyboard '{UPARROW}{UPARROW}'
        This.bMoveFocus=.F.
        Return
Enddefine

Define Class FixedBodyKeyMover As CPCommandButton
    Height=0
    Width=0
    Style=1
    nDirection=0
    bMoveFocus=.F.
    Proc GotFocus()
        If !This.bMoveFocus
            If This.nDirection=-1
                If Inlist(Lastkey(),5,15)
                    Keyboard '{UPARROW}'
                Else
                    If This.Parent.oContained.CheckRow()
                        Thisform.LockScreen=.T.
                        Select (This.Parent.oContained.cTrsName)
                        *if recno()>=reccount()
                        If Recno()>=This.Parent.oContained.nLastRow
                            If This.Parent.oContained.FullRow() And This.Parent.oContained.CanAddRow()
                                This.Parent.oContained.InitRow()
                                This.Parent.oBody.SetFullFocus()
                            Else
                                This.Parent.oBeginFooter.MoveFocus()
                            Endif
                        Else
                            This.Parent.oContained.i_lastcheckrow=This.Parent.oContained.oPgFrm.Page1.oPag.oBody.nAbsRow
                            If This.Parent.oContained.oPgFrm.Page1.oPag.oBody.RelativeRow=(This.Parent.oContained.i_nRowPerPage-1)
                                This.Parent.oContained.oPgFrm.Page1.oPag.oBody.DoScroll(1)
                            Endif
                            Skip
                            This.Parent.oBody.SetFullFocus()
                        Endif
                    Else
                        If Not(Isnull(This.Parent.oContained.oNewFocus))
                            If Lower(This.Parent.oContained.oNewFocus.Parent.Name)=='orow'
                                * --- il campo che ha dato errore e' nel corpo, bisogna selezionare sia il corpo che il campo
                                * Focus da campi fixed position INIZIO
                                Local i_obj_focus
                                i_obj_focus=This.Parent.oContained.oNewFocus
                                This.Parent.oBody.SetFocus()
                                This.Parent.oContained.oNewFocus=i_obj_focus
                                i_obj_focus=.Null.
                            Endif
                            * --- il campo che ha dato errore e' nella parte fissa, bisogna selezionare solo il campo
                            This.Parent.oContained.oNewFocus.SetFocus()
                            This.Parent.oContained.oNewFocus=.Null.
                        Endif
                    Endif
                Endif
            Else
                If Inlist(Lastkey(),5,15)
                    This.Parent.oBody.oBodyCol.oRow.oLast.MoveFocus()
                Endif
            Endif
        Endif
        Return
    Proc MoveFocus()
        This.bMoveFocus=.T.
        This.SetFocus()
        If This.nDirection=1
            Keyboard '{TAB}'
        Else
            Keyboard '{UPARROW}'
        Endif
        This.bMoveFocus=.F.
        Return
Enddefine
* --- Gestione Bottone contestuale
Define Class btnZoom As CommandButton
    Caption = ''
    Visible = .T.
    Width = i_nZBtnWidth
    TabStop = .F.
    ObjColl = .Null.
    Picture = ''
    ToolTipText = MSG_CONTEXT_MENU
    BackColor=Rgb(206,219,255)
    ImgSize=16

    #If Version(5)>=800
        SpecialEffect = i_nBtnSpEfc
    #Endif

    Procedure Init(tObj,Ttop,tLeft,tWidth,tHeight)
        This.ObjColl = tObj
        If Empty(This.ObjColl.cLinkFile)
            If Vartype(g_BtnZoomGear)<>'C'
                Local cFileBmp, l_Dim
                l_Dim= Iif(This.ImgSize<>0, This.ImgSize, i_nBtnImgSize)
                Public g_BtnZoomGear
                If Vartype(i_ThemesManager)=='O'
                    g_BtnZoomGear  = Forceext(i_ThemesManager.RetBmpFromIco('gear.bmp', m.l_Dim),'bmp')
                Else
                    g_BtnZoomGear = 'gear.bmp'
                Endif
            Endif
            This.Picture= g_BtnZoomGear
        Else
            If Vartype(g_BtnZoomArrow)<>'C'
                Local cFileBmp, l_Dim
                l_Dim= Iif(This.ImgSize<>0, This.ImgSize, i_nBtnImgSize)
                Public g_BtnZoomArrow
                If Vartype(i_ThemesManager)=='O'
                    g_BtnZoomArrow  = Forceext(i_ThemesManager.RetBmpFromIco('arrow.bmp', m.l_Dim),'bmp')
                Else
                    g_BtnZoomArrow = 'arrow.bmp'
                Endif
            Endif
            This.Picture= g_BtnZoomArrow
        Endif
        This.Riposiziona_Btn()
        This.Enabled = .T. &&this.ObjColl.Enabled
        * Sono un control su di una griglia (propriet� ctotal vediamo)
        * se possibile fare meglio
        * se bottone di una griglia lo disabilito
        This.Visible = Type('tObj.cTotal')='U' Or tObj.Visible
        This.ToolTipText=cp_Translate(This.ToolTipText)
    Endproc

    * --- Zucchetti Aulla Inizio - Gestione Bottoncino contestuale
    * --- Al cambio di posizione/dimensione del ctrl allinea il bottone
    Procedure Riposiziona_Btn
        This.Top = This.ObjColl.Top
        This.Left = This.ObjColl.Left + This.ObjColl.Width
        This.Height = This.ObjColl.Height
    Endproc

    Procedure Click
        If Type('This.ObjColl.name')='C'
            If This.ObjColl.Enabled And Not This.ObjColl.ReadOnly And This.ObjColl.bHasZoom And (Upper(This.ObjColl.Parent.oContained.cFunction)==Upper("Edit") Or Upper(This.ObjColl.Parent.oContained.cFunction)==Upper("Load") )
                * lancio eventuale zoom...
                This.ObjColl.Parent.oContained.bDontReportError=.T.
                Public i_lastindirectaction
                i_lastindirectaction='ecpZoom'
                This.ObjColl.SetFocus()
                This.ObjColl.mZoom()
            Else
                This.RightClick
            Endif
        Endif

    Endproc
    * --- Zucchetti Aulla Fine - Gestione Bottoncino contestuale

    Procedure RightClick
        If Type('This.ObjColl.name')='C'
            This.ObjColl.RightClick()
        Endif

    Endproc

Enddefine
*** Bottone per traduzione del dato
Define Class btnTrans As btnZoom
    Picture = ''
    ToolTipText = MSG_CONTEXT_TRANSLATE
    #If Version(5)>=800
        SpecialEffect = i_nBtnSpEfc
    #Endif

    Procedure Init(tObj,Ttop,tLeft,tWidth,tHeight)
        This.ObjColl = tObj
        This.Picture=i_ThemesManager.RetBmpFromIco('trans.bmp',16)+'.bmp'
        This.Riposiziona_Btn()
        This.Enabled = .T. &&this.ObjColl.Enabled
        This.Visible = Type('tObj.cTotal')='U' Or tObj.Visible
        This.ToolTipText=cp_Translate(This.ToolTipText)
    Endproc

    Procedure Click
        If Type('This.ObjColl.name')='C'
            Local i_ObjColl, i_ObjCollPar
            i_ObjColl=This.ObjColl
            i_ObjCollPar=This.ObjColl.Parent.oContained
            If Not i_ObjColl.ReadOnly And i_ObjColl.bMultilanguage And (Upper(i_ObjCollPar.cFunction)==Upper("Query") Or (Upper(i_ObjCollPar.cFunction)==Upper("Edit") And "Detail"$cp_getEntityType(i_ObjCollPar.cprg))) And i_ObjCollPar.bLoaded
                * lancio la maschera di traduzione
                This.ObjColl.Parent.oContained.bDontReportError=.T.
                cp_TranslateMask(This.ObjColl)
            Endif
        Endif
    Endproc

    * --- Al cambio di posizione/dimensione del ctrl allinea il bottone
    Procedure Riposiziona_Btn(i_btnZoom)
        This.Top = This.ObjColl.Top
        This.Left = This.ObjColl.Left + Iif(i_btnZoom=.T.,This.Width,0) + This.ObjColl.Width
        This.Height = This.ObjColl.Height
    Endproc
Enddefine

* --- Classe per definire rettangolo da disegnare attorno
* --- a campi obbligatori.
Define Class OblShape As Shape
    SpecialEffect=1
    Visible=.T.
    BackStyle=0
    BorderStyle=1
    oFather= .Null.

    Procedure Init(Ttop,tLeft,tWidth,tHeight,_Zorder,father)
        * lo sposto in secondo piano...
        If _Zorder=1
            This.ZOrder(1)
        Endif
        This.oFather=father
        This.Riposiziona()
        This.BorderColor=i_nOblColor
        This.Curvature=i_nCurvature
        This.Visible = This.oFather.Visible And This.oFather.Enabled
    Endproc

    * --- Allinea shape in caso si sposti a run-time il control
    Procedure Riposiziona
        This.Top = This.oFather.Top-1
        This.Left = This.oFather.Left -1
        This.Height = This.oFather.Height + 2
        * --- Se il control al quale � legato ha lo zoom devo includerlo nello shape
        If Type('this.oFather.bHasZoom')='L' And This.oFather.bHasZoom And i_cZBtnShw='S' And Type('this.oFather.bNoContxtBtn')='U'
            This.Width=This.oFather.Width + 2 + i_nZBtnWidth
        Else
            This.Width=This.oFather.Width + 2
        Endif
    Endproc

Enddefine

* ===================================================================================================
* cp_Spinner
* ===================================================================================================
Define Class CPSpinner As Spinner
    *forecolor=rgb(96,0,0)
    *backcolor=rgb(255,242,192)
    * propriet� per contenere il nome del shape per i campi obbligatori
    CShape=''
    * --- Gestione men� contestuale (tasto destro)
    cMenuFile=''
    zbName='' && gestione bottone contestuale
    transbName='' && Nome bottoncino traduzione del dato
    bNoRightClick = .F. && disabilita tasto dx

    bNoBackColor = .F. &&Di Default non applica i colori di sfondo (disabilita il cambio colore di sfondo alla getfocus)
    bNoDisabledBackColor = .F. &&Di Default non applica la configurazione interfaccia
    SpecialEffect = i_nSpecialEffect
    BorderColor = i_nBorderColor
    DisabledForeColor = i_udisabledforecolor

    Procedure Init
        This.BackColor = Iif(This.bNoBackColor, This.BackColor, i_nEBackColor)
        This.DisabledBackColor = Iif(This.bNoDisabledBackColor, This.DisabledBackColor, i_udisabledbackcolor)
        #If Version(5)<800
            This.StatusBarText=Padr(This.ToolTipText,100)
        #Else
            This.StatusBarText = This.ToolTipText
        #Endif
        DoDefault()
    Endproc

    * --- Al cambio della posizione (Run-Time) del control distruggo e ricreo eventuale rettangolo
    * --- per evidenziare campo obbligatorio o bottone contestuale
    Procedure Left_Assign(xvalue)
        If xvalue<>This.Left
            This.Left=xvalue
            This.Draw_Link_Obj()
        Endif
    Endproc

    Procedure Top_Assign(xvalue)
        If xvalue<>This.Top
            This.Top=xvalue
            This.Draw_Link_Obj()
        Endif
    Endproc

    Procedure Width_Assign(xvalue)
        If xvalue<>This.Width
            This.Width=xvalue
            This.Draw_Link_Obj()
        Endif
    Endproc

    Procedure Height_Assign(xvalue)
        If xvalue<>This.Height
            This.Height=xvalue
            This.Draw_Link_Obj()
        Endif
    Endproc

    Proc Draw_Link_Obj
        Local btnName
        * --- Bottone contestuale
        Local i_btnZoom,ctShape
        i_btnZoom=.F.
        btnName='this.parent.'+This.zbName
        * --- Riposiziono bottone contestuale se presente...
        If Type(btnName)='O'
            btnName=This.zbName
            This.Parent.&btnName..Riposiziona_Btn()
            i_btnZoom=.T.
        Endif
        * --- Zucchetti Aulla fine - Bottone contestuale
        btnName='this.parent.'+This.transbName
        * --- Riposiziono bottone traduzioni...
        If Type(btnName)='O'
            btnName=This.transbName
            This.Parent.&btnName..Riposiziona_Btn(i_btnZoom)
        Endif

        * ---  Campo obbligatorio
        * --- Riposiziono shape se campo obbligatorio e se shape presente...
        If Type('this.bObbl')='L' And This.bObbl And Not Empty(This.CShape)
            * --- verifico se l'oggetto esiste...
            ctShape='this.parent.'+This.CShape
            If Type(ctShape)='O'
                ctShape=This.CShape
                This.Parent.&ctShape..Riposiziona()
            Endif
        Endif
    Endproc

    Procedure visible_assign(xvalue)
        If This.Visible <> xvalue
            This.Visible = xvalue
            * --- Campo obbligatorio
            Local CShape
            If Type('this.cShape')='C' And Not Empty(This.CShape)
                CShape = This.CShape
                * nascondo il rettangolo di campo obbligatorio
                * se campo non visible
                This.Parent.&CShape..Visible = This.Visible And This.Enabled
            Endif

            Local zbName
            zbName = This.zbName
            * --- Mostro bottone contestuale se presente...
            If Not Empty(zbName) And Type('this.bHasZoom')='L' And This.bHasZoom
                This.Parent.&zbName..Visible = xvalue
            Endif
            Local transbName
            transbName = This.transbName
            * --- Mostro bottone contestuale se presente...
            If Not Empty(transbName)
                This.Parent.&transbName..Visible = xvalue
            Endif
        Endif
    Endproc

    * --- Campo obbligatorio
    Procedure enabled_assign(xvalue)
        Local CShape
        If This.Enabled <> xvalue
            This.Enabled = xvalue
            If Type('this.cShape')='C' And Not Empty(This.CShape)
                CShape = This.CShape
                * nascondo il rettangolo di campo obbligatorio
                * se campo non editabile
                This.Parent.&CShape..Visible = This.Visible And This.Enabled
            Endif
        Endif
    Endproc

    Procedure KeyPress(nKeyCode, nShiftAltCtrl)
        Local pt
        If Vartype(This.Value)='N'
            pt=Set('point')
            Do Case
                Case nKeyCode=46 And nKeyCode<>Asc(pt)
                    * Sostituzione carattere '.'
                    Nodefault
                    Keyboard pt
                Case nKeyCode=44 And nKeyCode<>Asc(pt)
                    * Sostituzione carattere '.'
                    Nodefault
                    Keyboard pt
                Case nKeyCode=9
                    * uscita con il tasto return, in modo da sovrascrivere sempre il contenuto
                    Nodefault
                    Keyboar '{ENTER}'
            Endcase
        Endif
    Endproc

    Procedure RightClick
        If !This.bNoRightClick   &&  disabilito men� contestuale
            If Vartype(This.Parent.oContained)='O' And Vartype( This.cMenuFile )='C'
                g_oMenu=Createobject("IM_ContextMenu",This,This.Parent.oContained,This.cMenuFile,1)
                g_oMenu.creaMenu()
                ** Deve essere rilasciata la risorsa occupata, causa chiusura non corretta di Revolution
                g_oMenu=.Null.
            Endif
        Endif
    Endproc

    *PROCEDURE MouseEnter(nButton, nShift, nXCoord, nYCoord)
    *  IF VARTYPE(thisform.__tb__)='O'
    *    thisform.__tb__.top=this.top+28
    *    thisform.__tb__.left=this.left+this.width-5
    *    thisform.__tb__.height=this.height
    *    thisform.__tb__.zorder(0)
    *    thisform.__tb__.visible=.t.
    *  ENDIF
    *  return
    *PROCEDURE MouseLeave(nButton, nShift, nXCoord, nYCoord)
    *  IF VARTYPE(thisform.__tb__)='O'
    *    thisform.__tb__.visible=.f.
    *  ENDIF
    *  return
Enddefine

* === Definizione Spinner Standard
Define Class StdSpinner As CPSpinner
    *
    * --- PROPRIETA' VFP
    *
    Height = 23
    Caption = ""
    * visible = .t.                          && ERRORE Per gli oggetti contenuti va settato da fuori
    FontSize   = 9
    FontBold   = .F.
    FontItalic = .F.
    FontUnderline  = .F.
    FontStrikethru = .F.
    FontName   = ""
    *DisabledBackColor=rgb(255,255,255)
    Format='K'
    Margin=1
    *
    * --- PROPRIETA' CODEPAINTER
    *
    cQueryName = ""
    bObbl = .F.                            && Obbligatorio
    sErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    cLinkFile=''
    cOldIcon=''
    bHasZoom=.F.
    nPag=0
    cFormVar=''
    bIsInHeader=.T.
    nZero=0
    bUpd=.F.
    cSayPict=''
    cGetPict=''
    nSeconds=0
    bInInput=.F.
    * --
    cfgCalc=''
    cfgCond=''
    cfgCheck=''
    cfgDefault=''
    cfgHide=''
    cfgInit=''
    bNoMenuFunction=.F.           && Disabilita nel tasto destro sottomenu Funzionalit�
    bNoMenuAction=.F.         	&& Disabilita nel tasto destro sottomenu Azioni
    bNoMenuProperty=.F.			&& Disabilita nel tasto destro sottomenu Propiet�
    bNoMenuCut=.F.			    && Disabilita nel tasto destro sottomenu Taglia
    bNoMenuCopy=.F.			    && Disabilita nel tasto destro sottomenu Copia
    bNoMenuPaste=.F.			    && Disabilita nel tasto destro sottomenu Incolla
    bNoMenuSelAll=.F.			    && Disabilita nel tasto destro sottomenu Seleziona Tutto
    bSetFont = .T. && Attiva / disattiva la possibilt� di pilotare i font
    *** Correzione Aulla Gest. Font
    bGlobalFont=.F.
    *** Fine Aulla Gest. Font

    *
    * --- COSTRUTTORE/DISTRUTTORE
    *
    * --- Inizializzazione
    Procedure Init()
        Local i_im
        * ---Add the button contextual
        Local i_cond
        * Aggiunta del bottoncino contestuale di fianco ai campi con linktable
        i_cond=((i_cZBtnShw='S' And (Type('this.bNoContxtBtn')='U' Or (Type('this.bNoContxtBtn')='N' And This.bNoContxtBtn=2 ))) Or;
            (i_cZBtnShw='N' And Type('this.bNoContxtBtn')='N' And This.bNoContxtBtn=2 ))
        If Type('this.bHasZoom')='L' And This.bHasZoom And i_cond
            Local btnName
            This.zbName = 'z'+This.Name
            * --- Gestisco l'errore in alcuni casi va in errore perch� il Parent non � ancora stato definito
            Local l_olderr, berr
            l_olderr=On('error')
            berr=.F.
            On Error berr=.T.
            This.Parent.AddObject(This.zbName,'btnZoom',This,This.Top,This.Left,This.Width,This.Height)
            On Error &l_olderr
            If Not berr
                * Per la macro non posso utilizzare una propriet�
                btnName = This.zbName
                This.Parent.&btnName..Visible = This.Visible
                This.Width=This.Width-i_nZBtnWidth
            Endif
        Endif
        * --- Add button translations
        If Type('this.bMultilanguage')='L' And This.bMultilanguage And Not Empty(i_aCpLangs[1])
            Local btnName
            This.transbName = 'trans'+This.Name
            * --- Gestisco l'errore in alcuni casi va in errore perch� il Parent non � ancora stato definito
            Local l_olderr, berr
            l_olderr=On('error')
            berr=.F.
            On Error berr=.T.
            This.Parent.AddObject(This.transbName,'btnTrans',This,This.Top,This.Left,This.Width,This.Height)
            On Error &l_olderr
            If Not berr
                * Per la macro non posso utilizzare una propriet�
                btnName = This.transbName
                This.Parent.&btnName..Visible = This.Visible
                This.Width=This.Width-i_nZBtnWidth
            Endif
        Endif
        * Add a red rectangle around the required fields
        If i_cHlOblColor='S' And Type('this.bObbl')='L' And This.bObbl
            Local l_olderr, berr
            l_olderr=On('error')
            On Error berr=.T.
            This.CShape='TTxOb'+This.Name
            This.Parent.AddObject(This.CShape,'OblShape',This.Top-1,This.Left-1,This.Width+2,This.Height+2,1,This)
            On Error &l_olderr
        Endif
        If Empty(This.FontName)
            This.FontName = This.Parent.FontName
            This.FontSize = This.Parent.FontSize
            This.FontBold = This.Parent.FontBold
            This.FontItalic = This.Parent.FontItalic
            This.FontUnderline  = This.Parent.FontUnderline
            This.FontStrikethru = This.Parent.FontStrikethru
        Endif
        If This.bSetFont
            This.SetFont()
        Endif
        If Empty(This.cSayPict) And !Empty(This.cGetPict)
            This.cSayPict=This.cGetPict
        Endif
        If !Empty(This.cSayPict) And Empty(This.cGetPict)
            This.cGetPict=This.cSayPict
        Endif
        If !Empty(This.cSayPict)
            i_im=Trim(cp_GetMask(This.cSayPict))
            If !Empty(i_im)
                This.InputMask=i_im
            Endif
            This.Format='K'+cp_GetFormat(This.cSayPict)
        Endif
        This.ToolTipText=cp_Translate(This.ToolTipText)
        DoDefault()
        Return

    Procedure SetFont()
		If This.bGlobalFont
			SetFont('T', This)
		Endif
    Endproc
    *
    * --- EVENTI/METODI VFP
    *
    * --- Quando prende il Focus
    Proc GotFocus()
        Local i_var,i_im,e
        Local i_oldvalue
        This.bUpd=.F.
        If This.Parent.oContained.cFunction="Query"
            * --- attiva il bottone di edit
            oCpToolBar.b2.Enabled=This.Parent.oContained.bLoaded
            * --- attiva i bottoni prev-next
            oCpToolBar.b7.Enabled=Used(This.Parent.oContained.cKeySet) And !Bof(This.Parent.oContained.cKeySet)
            oCpToolBar.b8.Enabled=Used(This.Parent.oContained.cKeySet) And !Eof(This.Parent.oContained.cKeySet)
        Endif
        * --- calcola il default
        If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
            This.Parent.oContained.SaveDependsOn()
            This.ResetTotal()
            i_oldvalue=This.Value
            This.mDefault()
            oCpToolBar.b9.Enabled=This.bHasZoom Or Type('this.parent.oContained.'+This.cFormVar)='D'
            This.bUpd=i_oldvalue<>This.Value
        Endif
        * -- default da configurazione
        If Not(Empty(This.cfgDefault)) And Empty(This.Value)
            i_oldvalue=This.Value
            e=Strtran(This.cfgDefault,'w_','this.parent.oContained.w_')
            This.Value=&e
            This.bUpd=This.bUpd Or This.Value<>i_oldvalue
        Endif
        * --- esegue la funzione "before input"
        If This.Parent.oContained.cFunction<>"Filter"
            This.mBefore()
            * --- il valore puo' essere cambiato dalla funzione di "Before"
            i_var=This.cFormVar
            If This.Value<>This.Parent.oContained.&i_var And This.IsUpdated()
                This.SetControlValue()
                This.bUpd=.T.
            Endif
        Endif
        This.bInInput=.T.
        * --- setta la picture di "get"
        If !Empty(This.cGetPict)
            i_im=Trim(cp_GetMask(This.cGetPict))
            If !Empty(i_im)
                This.InputMask=i_im
            Endif
            This.Format='K'+cp_GetFormat(This.cGetPict)
        Endif
        If !This.bNoBackColor
            This.BackColor= i_nBackColor
        Endif
        If Inlist(This.Parent.oContained.cFunction,"Load","Edit")
            If Vartype(i_var)='C'
                This.Parent.oContained.NotifyEvent(i_var+' GotFocus')
            Endif
        Endif

        Return
    Func When()
        Local i_bRes,i_e
        * --- Procedure standard del campo
        i_bRes=.T.
        If !Inlist(This.Parent.oContained.cFunction,"Query","Filter")
            i_bRes = This.mCond()
            If !Empty(This.cfgCond)
                i_e=Strtran(This.cfgCond,'w_','this.parent.oContained.w_')
                i_bRes=&i_e
            Endif
        Endif
        Return(i_bRes)
    Func Valid()
        Local i_nRes,i_bErr,i_var,i_bUpd,e,l
        i_nRes=1
        * --- Il value ha il nuovo valore, la variabile di work deve essere aggiornata
        i_var=This.cFormVar
        i_bUpd=.F.
        If This.nZero<>0
            If !Empty(This.Value) And Left(This.Value,1)$'123456789'
                l=Iif(This.nZero>0,This.nZero,Len(This.Value))
                This.Value=Right(Repl('0',l)+Alltrim(This.Value),l)
            Endif
        Endif
        If This.IsUpdated()
            i_bUpd=.T.
            This.Parent.oContained.bUpdated=.T.
            If This.bIsInHeader
                This.Parent.oContained.bHeaderUpdated=.T.
            Endif
            This.Parent.oContained.&i_var=This.Value
            This.SetModified()
        Endif
        This.bUpd=.F.
        * --- After Input solo alla variazione del campo
        If i_bUpd
            This.mAfter()
            * --- il valore puo' essere cambiato dalla funzione di "After"
            If This.Value<>This.Parent.oContained.&i_var
                This.SetControlValue()
            Endif
        Endif
        * ---
        If Not(Inlist(This.Parent.oContained.cFunction,'Filter','Query'))
            i_bErr=.F.
            i_nRes=Iif(i_bUpd,0,1)
            oCpToolBar.b9.Enabled=.F.
            * --- Obbligatorieta'
            If This.bObbl And Empty(This.Value) And This.CondObbl()
                If Not(This.Parent.oContained.bDontReportError)
                    Do cp_ErrorMsg With MSG_FIELD_CANNOT_BE_NULL_QM
                Endif
                i_nRes=0
                i_bErr=.T.
            Endif
            If Not(i_bErr) And i_bUpd
                If !Empty(This.cfgCheck)
                    e=Strtran(This.cfgCheck,'w_','this.parent.oContained.w_')
                    i_bErr=Not(&e)
                    If i_bErr
                        If Not(This.Parent.oContained.bDontReportError)
                            Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
                        Endif
                        This.Parent.oContained.&i_var=cp_NullValue(This.Parent.oContained.&i_var)
                        This.SetControlValue()
                        i_nRes=0
                    Endif
                Endif
                If Not(i_bErr) And i_bUpd
                    * --- Procedura di check e calcolo se il campo e' cambiato
                    If This.Check()
                        i_nRes=1
                    Else
                        If Not(This.Parent.oContained.bDontReportError)
                            This.SpecialEffect=1
                            This.BorderColor=Rgb(255,0,0)
                            Do cp_ErrorMsg With Thisform.msgFmt(This.sErrorMsg)
                        Endif
                        * --- azzeramento della variabile e del control
                        This.Parent.oContained.&i_var=cp_NullValue(This.Parent.oContained.&i_var)
                        This.SetControlValue()
                    Endif
                Endif
                This.SetTotal()
                This.Parent.oContained.NotifyEvent(i_var+' Changed')
                This.Parent.oContained.mCalc(i_bUpd)
                This.Parent.oContained.SaveDependsOn()
            Else
                This.SetTotal()
            Endif
        Endif
        This.Parent.oContained.bDontReportError=.F.
        Return(i_nRes)
    Proc LostFocus()
        Local i_cFlt,i_cFile,i_nConn,i_im
        This.SpecialEffect=0
        If This.Parent.oContained.cFunction='Query'
            If This.Parent.oContained.bUpdated And Not(Empty(This.cQueryName))
                Nodefault
                i_cFile=This.Parent.oContained.cFile
                i_nConn=i_TableProp[this.parent.oContained.&i_cFile._idx,3]
                i_cFlt=cp_BuildKeyWhere(This.Parent.oContained,This.cQueryName,i_nConn)
                This.Parent.oContained.QueryKeySet(i_cFlt,This.cQueryName)
                This.Parent.oContained.LoadRecWarn()
            Endif
        Endif
        If !Empty(This.cSayPict)
            i_im=Trim(cp_GetMask(This.cSayPict))
            If !Empty(i_im)
                This.InputMask=i_im
            Endif
            This.Format='K'+cp_GetFormat(This.cSayPict)
        Endif
        This.bInInput=.F.
        This.Parent.oContained.NotifyEvent(This.cFormVar+' LostFocus')
        If !This.bNoBackColor
            This.BackColor=i_nEBackColor
        Endif
        Return
    Func IsUpdated()
        If This.bUpd
            Return (.T.)
        Endif
        Local i_var,i_m
        i_var=This.cFormVar
        i_m=This.InputMask
        If Vartype(This.Value)='C'
            If Empty(i_m)
                Return(Not(Trim(This.Value)==Trim(This.Parent.oContained.&i_var)))
            Else
                * c'e una picture, il valore della varibile deve essere formattato di conseguenza
                Return(Not(Trim(Transform(This.Value,i_m))==Trim(Transform(This.Parent.oContained.&i_var,i_m))))
            Endif
        Endif
        Return(Not(This.Value==This.Parent.oContained.&i_var))
    Proc MouseDown(nButton, nShift, nXCoord, nYCoord)
        If nButton=1
            If Seconds()-0.3<This.nSeconds
                If Inlist(This.Parent.oContained.cFunction,'Edit','Load')
                    Nodefault
                    This.Parent.oContained.bDontReportError=.T.
                    Public i_lastindirectaction
                    i_lastindirectaction='ecpZoom'
                    This.mZoom()
                Endif
            Endif
            This.nSeconds=Seconds()
        Endif
        Return
        *
        * ------ METODI CODEPAINTER
        *
        * --- Procedura di default standard del campo
    Proc mDefault()
        Return
        * --- Procedura di Condizione standard del campo
    Func mCond()
        Return (.T.)
        * --- Procedura di Hide standard del campo
    Func mHide()
        Return (.F.)
        * --- Procedura di Before input standard del campo
    Proc mBefore()
        Return
        * --- Procedura di After input standard del campo
    Proc mAfter()
        Return
        * --- Procedura di check
    Func Check()
        Return(.T.)
    Proc mZoom()
        If Type('this.parent.oContained.'+This.cFormVar)='D'
            cp_zdate(This)
        Endif
        Return
    Proc DragOver(oSource,nXCoord,nYCoord,nState)
        If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And oSource.oContained.bLoaded
            Do Case
                Case nState=0   && Enter
                    This.cOldIcon=oSource.DragIcon
                    If oSource.cFile==This.cLinkFile
                        oSource.DragIcon=i_cBmpPath+"cross01.cur"
                    Endif
                Case nState=1   && Leave
                    oSource.DragIcon=This.cOldIcon
            Endcase
        Endif
        Return
    Proc DragDrop(oSource, nXCoord, nYCoord)
        If Not(Empty(This.cLinkFile)) And (oSource.ParentClass='Stdcontainer' Or oSource.Class='Stdztextbox') And oSource.oContained.bLoaded
            If oSource.cFile==This.cLinkFile
                This.ecpDrop(oSource)
            Endif
        Endif
        Return
    Proc ecpDrop(oSource)
        Return
    Proc SetModified()
        Return
    Proc ResetTotal()
        Return
    Proc SetTotal()
        Return
    Proc SetControlValue()
        Local i_var,i_cs
        i_var=This.cFormVar
        i_cs=This.ControlSource
        This.Value=This.Parent.oContained.&i_var
        If !(Empty(i_cs))
            Select (This.Parent.oContained.cTrsName)
            Replace &i_cs With This.Value
        Endif
        Return
    Proc ProgrammaticChange()
        * --- questa routine gestisce le picture dinamiche
        Local i_im
        If !This.bInInput And !Empty(This.cSayPict) And !(Left(This.cSayPict,1)$["'])
            * wait window "calcola picture "+this.name nowait
            i_im=Trim(cp_GetMask(This.cSayPict))
            If !Empty(i_im)
                This.InputMask=i_im
            Endif
            This.Format='K'+cp_GetFormat(This.cSayPict)
        Endif
    Function CondObbl()
        Return (.T.)
    Proc ProgrammaticChange()
        * --- questa routine gestisce le picture dinamiche
        Local i_im
        If !This.bInInput And !Empty(This.cSayPict) And !(Left(This.cSayPict,1)$["'])
            * wait window "calcola picture "+this.name nowait
            i_im=Trim(cp_GetMask(This.cSayPict))
            If !Empty(i_im)
                This.InputMask=i_im
            Endif
            This.Format='K'+cp_GetFormat(This.cSayPict)
        Endif
Enddefine
* --- FINE ---
