* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_translatemask                                                *
*              Traduzione del dato                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-30                                                      *
* Last revis.: 2008-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
Return(Createobject("tcp_translatemask",oParentObject))

* --- Class definition
Define Class tcp_translatemask As StdForm
    Top    = 10
    Left   = 10

    * --- Standard Properties
    Width  = 178
    Height = 92
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2008-10-02"
    HelpContextID=32673569
    max_rt_seq=4

    * --- Constant Properties
    _IDX = 0
    cPrg = "cp_translatemask"
    cComment = "Traduzione del dato"
    oParentObject = .Null.
    Icon = "mask.ico"
    WindowType = 1
    MinButton = .F.
    *closable = .f.

    * --- Local Variables
    w_NameTable = Space(200)
    w_VARIABLE = Space(50)
    w_cWhere = Space(200)
    w_CPCCCHK = Space(10)
    * --- Area Manuale = Declare Variables
    * --- cp_translatemask
    Dimension Ctrls[(alen(i_aCpLangs)+1)*2]
    nY=5
    AutoCenter=.T.
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=1, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tcp_translatemaskPag1","cp_translatemask",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate("Pag.1")
            .Pages(1).oPag.oContained=Thisform
        Endwith
        This.Parent.oFirstControl = .Null.
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
        * --- Area Manuale = Init Page Frame
        * --- cp_translatemask
        * --- Translate interface...
        This.Parent.cComment=cp_Translate(MSG_TRANSLATIONS_DATA)
        * --- Fine Area Manuale
    Endproc
    Proc Init(oParentObject)
        If Vartype(m.oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        DoDefault()

        * --- Open tables
    Function OpenWorkTables()
        Return(This.OpenAllTables(0))

    Procedure SetPostItConn()
        Return


        * --- Read record and initialize Form variables
    Procedure LoadRec()
    Endproc

    * --- ecpQuery
    Procedure ecpQuery()
        This.BlankRec()
        This.cFunction="Edit"
        This.SetStatus()
        If This.cFunction="Edit"
            This.mEnableControls()
        Endif
    Endproc
    * --- Esc
    Proc ecpQuit()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            This.cFunction = "Edit"
            This.oFirstControl.SetFocus()
            Return
        Endif
        * ---
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Endproc
    Proc QueryUnload()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            Nodefault
            Return
        Endif
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Proc ecpSave()
        If This.TerminateEdit() And This.CheckForm()
            This.LockScreen=.T.
            This.mReplace(.T.)
            This.LockScreen=.F.
            This.Hide()
            This.NotifyEvent("Done")
            This.Release()
        Endif
    Endproc
    Func OkToQuit()
        Return .T.
    Endfunc

    * --- Blank Form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        With This
            .w_NameTable=Space(200)
            .w_VARIABLE=Space(50)
            .w_cWhere=Space(200)
            .w_CPCCCHK=Space(10)
            .w_NameTable = Iif(cp_getEntityType(This.oParentObject.Parent.oContained.cPrg)="Master/Detail" And This.oParentObject.nPag=2, This.oParentObject.Parent.oContained.cFileDetail, This.oParentObject.Parent.oContained.cFile)
            .w_VARIABLE = Substr( This.oParentObject.cFormvar, 3 )
            .w_cWhere = Iif("Detail"$cp_getEntityType(This.oParentObject.Parent.oContained.cPrg) And This.oParentObject.nPag=2, This.oParentObject.Parent.oContained.GetcKeyDetailWhereODBC(), This.oParentObject.Parent.oContained.GetcKeyWhereODBC())
        Endwith
        This.DoRTCalc(4,4,.F.)
        This.SaveDependsOn()
        This.SetControlsValue()
        This.oPgFrm.Page1.oPag.oBtn_1_1.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_2.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
        This.mHideControls()
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- cp_translatemask
    Endproc
    Proc Init(oParentObject)
        If Type("oParentObject")<>'L'
            This.oParentObject=oParentObject
        Endif
        nObj=Alen(i_aCpLangs)+1
        For i=1 To nObj
            *** aggiungo label
            This.AddObject('this.Ctrls[i*2-1]','StdString')
            If i=1
                This.Ctrls[i*2-1].Caption=Alltrim(cp_Translate(MSG_DEFAULT_VALUE+MSG_FS))
            Else
                This.Ctrls[i*2-1].Caption=Alltrim(looktab("cplangs","name","code",Trim(i_aCpLangs(i-1))))
                If Empty(This.Ctrls[i*2-1].Caption)
                    This.Ctrls[i*2-1].Caption=Trim(i_aCpLangs(i-1))+':'
                Else
                    This.Ctrls[i*2-1].Caption=This.Ctrls[i*2-1].Caption+':'
                Endif
            Endif
            This.Ctrls[i*2-1].Move(5,Iif(i=1,10,25)+((Max(This.oParentObject.Height,21)+5)*(i-1)))
            This.Ctrls[i*2-1].Width=120
            This.Ctrls[i*2-1].FontBold=.F.
            This.Ctrls[i*2-1].Alignment=1
            This.Ctrls[i*2-1].Visible=.T.
            *** aggiungo text box
            This.AddObject('this.Ctrls[i*2]','textbox')
            This.Ctrls[i*2].Move(130,Iif(i=1,10,25)+((Max(This.oParentObject.Height,21)+5)*(i-1)))
            This.Ctrls[i*2].FontBold=.F.
            This.Ctrls[i*2].Width=This.oParentObject.Width
            This.Ctrls[i*2].Height=Max(This.oParentObject.Height,21)
            This.Ctrls[i*2].InputMask=This.oParentObject.InputMask
            This.Ctrls[i*2].Value=This.oParentObject.Value
            This.Ctrls[i*2].FontName=This.oParentObject.FontName
            This.Ctrls[i*2].FontSize=This.oParentObject.FontSize
            This.Ctrls[i*2].FontBold=This.oParentObject.FontBold
            This.Ctrls[i*2].FontItalic=This.oParentObject.FontItalic
            This.Ctrls[i*2].FontUnderline=This.oParentObject.FontUnderline
            This.Ctrls[i*2].FontStrikethru=This.oParentObject.FontStrikethru
            This.Ctrls[i*2].Visible=.T.
            If Upper(This.oParentObject.Parent.oContained.cFunction)=Upper('Edit') And Iif(i=1,g_PROJECTLANGUAGE,i_aCpLangs(i-1))=Iif(Empty(i_cLanguageData),g_PROJECTLANGUAGE,i_cLanguageData)
                This.Ctrls[i*2].Enabled=.F.
            Endif
        Endfor
        This.Height=This.Height+((Max(This.oParentObject.Height,21)+10)*(nObj-1))
        This.oPgFrm.Height=This.Height
        This.Width=This.Width+This.oParentObject.Width
        This.oPgFrm.Width=This.Width

        DoDefault()
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = Setting operation
    *     Allowed Parameters: Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        * --- Deactivating List page when <> da Query
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt
        i_cFlt =""
        Return (i_cFlt)
    Endfunc

    Proc QueryKeySet(i_cWhere,i_cOrderBy)
    Endproc

    Proc SelectCursor
    Proc GetXKey

        * --- Update Database
    Function mReplace(i_bEditing)
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        This.NotifyEvent('Update start')
        With This
        Endwith
        This.NotifyEvent('Update end')
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(.T.)

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        If i_bUpd
            With This
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
            Endwith
            This.DoRTCalc(1,4,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
        Endwith
        Return

    Proc Calculate_GKMFCTJJIV()
        With This
            * --- Lettura campi multilingua
            cp_ReadMult(This;
                )
        Endwith
    Endproc

    * --- Enable controls under condition
    Procedure mEnableControls()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        DoDefault()
        Return

        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            If Lower(cEvent)==Lower("Init")
                .Calculate_GKMFCTJJIV()
                bRefresh=.T.
            Endif
        Endwith
        * --- Area Manuale = Notify Event End
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

    Function SetControlsValue()
        cp_SetControlsValueExtFlds(This,'')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
            Else
                If Not(i_bnoChk)
                    cp_ErrorMsg(i_cErrorMsg)
                Endif
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

Enddefine

* --- Define pages as container
Define Class tcp_translatemaskPag1 As StdContainer
    Width  = 174
    Height = 92
    stdWidth  = 174
    stdheight = 92
    resizeXpos=30
    resizeYpos=60
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.


    Add Object oBtn_1_1 As StdButton With uid="YCTTRQWDEB",Left=34, Top=65, Width=63,Height=23,;
        caption="Salva", nPag=1;
        , HelpContextID = 191195282;
        , Caption=MSG_S_SAVE;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_1.Click()
        With This.Parent.oContained
            Do cp_WriteMult With This.Parent.oContained
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_1_2 As StdButton With uid="TAPXNQTUEA",Left=101, Top=65, Width=63,Height=23,;
        caption="Esci", nPag=1;
        , HelpContextID = 25356146;
        , Caption=MSG_CANCEL_BUTTON;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_2.Click()
        =cp_StandardFunction(This,"Quit")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_translatemask','','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Dialog Window"
Endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
