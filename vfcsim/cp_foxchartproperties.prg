* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_foxchartproperties                                           *
*              FoxChartProperties                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-10-06                                                      *
* Last revis.: 2012-05-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tcp_foxchartproperties")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tccp_foxchartproperties")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tccp_foxchartproperties")
  return

* --- Class definition
define class tcp_foxchartproperties as StdPCForm
  Width  = 631
  Height = 225+35
  Top    = 10
  Left   = 10
  cComment = "FoxChartProperties"
  cPrg = "cp_foxchartproperties"
  HelpContextID=220659050
  add object cnt as tccp_foxchartproperties
enddefine

define class tscp_foxchartproperties as PCContext
  w_code = space(3)
  w_oChart = space(10)
  w_CHARTTYPE = 0
  w_LEGENDPOSITION = 0
  w_SHOWVALUESONSHAPES = space(1)
  w_CHANGECOLORONMOUSE = space(1)
  w_SHOWTIPS = space(1)
  w_DEPTH = 0
  w_SHADOW = space(1)
  w_PIEENHANCEDDRAWING = space(1)
  w_QUERYMODEL = space(250)
  w_SHOWAXIS = space(1)
  w_FILEMODEL = space(250)
  w_QUERY = space(254)
  w_FILECFG = space(254)
  w_FILEREPORT = space(250)
  w_FILERPT = space(254)
  w_cSource = space(10)
  w_MARGIN = 0
  w_MARGINTOP = 0
  w_MARGINBOTTOM = 0
  w_MARGINLEFT = 0
  w_MARGINRIGHT = 0
  w_SHOWSCALE = space(1)
  w_SCALE = 0
  w_MINVALUE = 0
  w_MAXVALUE = 0
  w_SCALEDIVIDER = 0
  w_CHKMINVALUE = space(1)
  w_CHKMAXVALUE = space(1)
  w_BARSPERSCALE = 0
  w_MINNUMBERSCALELEGENDS = 0
  w_SCALEAUTOFORMAT = space(1)
  w_COLORTYPE = 0
  w_BRUSHTYPE = 0
  w_ALPHACHANNEL = 0
  w_GRADIENTSHAPEDIRECTION = 0
  w_GRADIENTLEVEL = 0
  w_GRADIENTINVERTCOLORS = space(1)
  w_GRADIENTPOSITION = 0
  w_GRADIENTTYPE = 0
  w_BACKCOLOR = 0
  w_BACKCOLOR2 = 0
  w_BACKGRADIENTMODE = 0
  w_CHANGECOLOR = 0
  w_BACKCOLORALPHA = 0
  w_SHPMAINCOLOR = 0
  w_RET = space(1)
  w_ROTATIONCENTER = 0
  w_ROTATION = 0
  w_LBACKCOLOR = 0
  w_LFORECOLOR = 0
  w_FIELDCOLOR = space(25)
  w_FIELDDETACHSLICE = space(25)
  w_FIELDHIDESLICE = space(25)
  w_FIELDAXIS2 = space(25)
  w_FIELDLEGEND = space(25)
  w_LBACKCOLORALPHA = 0
  w_ALIGNMENT = 0
  w_FONTUNDERLINE = space(1)
  w_FONTITALIC = space(1)
  w_FONTBOLD = space(1)
  w_FONTNAME = space(50)
  w_LFORECOLORALPHA = 0
  w_FONTSIZE = 0
  w_FORMAT = space(200)
  w_CAPTION = space(200)
  w_LEGENDS = 0
  w_SERIAL = space(10)
  w_DONUTRATIO = 0
  w_PIEDETACHPIXELS = 0
  w_PIECOMPENSATEANGLES = space(1)
  w_PIESHOWPERCENT = space(1)
  w_PIEDETACHSLICEONCLICK = space(1)
  w_PIEDETACHSLICEONLEGENDCLICK = space(1)
  w_BARSSPACEBETWEEN = 0
  w_BARTYPE = 0
  w_BARLEGENDDIRECTION = 0
  w_AREA3DTOP = space(1)
  w_AREADRAWBORDERS = space(1)
  w_POINTSHAPEWIDTH = 0
  w_Uniqueshape = space(1)
  w_LINECAPS = space(1)
  w_LINECAPSSHAPE = 0
  w_PieDetachAnimationSteps = 0
  w_NUMSLICE = 0
  w_MODIFIED = space(1)
  w_ShowAxis2Tics = space(1)
  w_TicLength = 0
  w_ScaleBackLinesDash = 0
  w_ScaleBackLinesWidth = 0
  w_ScaleBackAlpha = 0
  w_AxisAlpha = 0
  w_ShowLineZero = space(1)
  w_ScaleLineColor = 0
  w_ScaleBackColor = 0
  w_AxisColor = 0
  w_ScaleLineZeroColor = 0
  w_MultiChart = space(1)
  w_MultiChartMargin = space(1)
  w_ChartRow = 0
  proc Save(oFrom)
    this.w_code = oFrom.w_code
    this.w_oChart = oFrom.w_oChart
    this.w_CHARTTYPE = oFrom.w_CHARTTYPE
    this.w_LEGENDPOSITION = oFrom.w_LEGENDPOSITION
    this.w_SHOWVALUESONSHAPES = oFrom.w_SHOWVALUESONSHAPES
    this.w_CHANGECOLORONMOUSE = oFrom.w_CHANGECOLORONMOUSE
    this.w_SHOWTIPS = oFrom.w_SHOWTIPS
    this.w_DEPTH = oFrom.w_DEPTH
    this.w_SHADOW = oFrom.w_SHADOW
    this.w_PIEENHANCEDDRAWING = oFrom.w_PIEENHANCEDDRAWING
    this.w_QUERYMODEL = oFrom.w_QUERYMODEL
    this.w_SHOWAXIS = oFrom.w_SHOWAXIS
    this.w_FILEMODEL = oFrom.w_FILEMODEL
    this.w_QUERY = oFrom.w_QUERY
    this.w_FILECFG = oFrom.w_FILECFG
    this.w_FILEREPORT = oFrom.w_FILEREPORT
    this.w_FILERPT = oFrom.w_FILERPT
    this.w_cSource = oFrom.w_cSource
    this.w_MARGIN = oFrom.w_MARGIN
    this.w_MARGINTOP = oFrom.w_MARGINTOP
    this.w_MARGINBOTTOM = oFrom.w_MARGINBOTTOM
    this.w_MARGINLEFT = oFrom.w_MARGINLEFT
    this.w_MARGINRIGHT = oFrom.w_MARGINRIGHT
    this.w_SHOWSCALE = oFrom.w_SHOWSCALE
    this.w_SCALE = oFrom.w_SCALE
    this.w_MINVALUE = oFrom.w_MINVALUE
    this.w_MAXVALUE = oFrom.w_MAXVALUE
    this.w_SCALEDIVIDER = oFrom.w_SCALEDIVIDER
    this.w_CHKMINVALUE = oFrom.w_CHKMINVALUE
    this.w_CHKMAXVALUE = oFrom.w_CHKMAXVALUE
    this.w_BARSPERSCALE = oFrom.w_BARSPERSCALE
    this.w_MINNUMBERSCALELEGENDS = oFrom.w_MINNUMBERSCALELEGENDS
    this.w_SCALEAUTOFORMAT = oFrom.w_SCALEAUTOFORMAT
    this.w_COLORTYPE = oFrom.w_COLORTYPE
    this.w_BRUSHTYPE = oFrom.w_BRUSHTYPE
    this.w_ALPHACHANNEL = oFrom.w_ALPHACHANNEL
    this.w_GRADIENTSHAPEDIRECTION = oFrom.w_GRADIENTSHAPEDIRECTION
    this.w_GRADIENTLEVEL = oFrom.w_GRADIENTLEVEL
    this.w_GRADIENTINVERTCOLORS = oFrom.w_GRADIENTINVERTCOLORS
    this.w_GRADIENTPOSITION = oFrom.w_GRADIENTPOSITION
    this.w_GRADIENTTYPE = oFrom.w_GRADIENTTYPE
    this.w_BACKCOLOR = oFrom.w_BACKCOLOR
    this.w_BACKCOLOR2 = oFrom.w_BACKCOLOR2
    this.w_BACKGRADIENTMODE = oFrom.w_BACKGRADIENTMODE
    this.w_CHANGECOLOR = oFrom.w_CHANGECOLOR
    this.w_BACKCOLORALPHA = oFrom.w_BACKCOLORALPHA
    this.w_SHPMAINCOLOR = oFrom.w_SHPMAINCOLOR
    this.w_RET = oFrom.w_RET
    this.w_ROTATIONCENTER = oFrom.w_ROTATIONCENTER
    this.w_ROTATION = oFrom.w_ROTATION
    this.w_LBACKCOLOR = oFrom.w_LBACKCOLOR
    this.w_LFORECOLOR = oFrom.w_LFORECOLOR
    this.w_FIELDCOLOR = oFrom.w_FIELDCOLOR
    this.w_FIELDDETACHSLICE = oFrom.w_FIELDDETACHSLICE
    this.w_FIELDHIDESLICE = oFrom.w_FIELDHIDESLICE
    this.w_FIELDAXIS2 = oFrom.w_FIELDAXIS2
    this.w_FIELDLEGEND = oFrom.w_FIELDLEGEND
    this.w_LBACKCOLORALPHA = oFrom.w_LBACKCOLORALPHA
    this.w_ALIGNMENT = oFrom.w_ALIGNMENT
    this.w_FONTUNDERLINE = oFrom.w_FONTUNDERLINE
    this.w_FONTITALIC = oFrom.w_FONTITALIC
    this.w_FONTBOLD = oFrom.w_FONTBOLD
    this.w_FONTNAME = oFrom.w_FONTNAME
    this.w_LFORECOLORALPHA = oFrom.w_LFORECOLORALPHA
    this.w_FONTSIZE = oFrom.w_FONTSIZE
    this.w_FORMAT = oFrom.w_FORMAT
    this.w_CAPTION = oFrom.w_CAPTION
    this.w_LEGENDS = oFrom.w_LEGENDS
    this.w_SERIAL = oFrom.w_SERIAL
    this.w_DONUTRATIO = oFrom.w_DONUTRATIO
    this.w_PIEDETACHPIXELS = oFrom.w_PIEDETACHPIXELS
    this.w_PIECOMPENSATEANGLES = oFrom.w_PIECOMPENSATEANGLES
    this.w_PIESHOWPERCENT = oFrom.w_PIESHOWPERCENT
    this.w_PIEDETACHSLICEONCLICK = oFrom.w_PIEDETACHSLICEONCLICK
    this.w_PIEDETACHSLICEONLEGENDCLICK = oFrom.w_PIEDETACHSLICEONLEGENDCLICK
    this.w_BARSSPACEBETWEEN = oFrom.w_BARSSPACEBETWEEN
    this.w_BARTYPE = oFrom.w_BARTYPE
    this.w_BARLEGENDDIRECTION = oFrom.w_BARLEGENDDIRECTION
    this.w_AREA3DTOP = oFrom.w_AREA3DTOP
    this.w_AREADRAWBORDERS = oFrom.w_AREADRAWBORDERS
    this.w_POINTSHAPEWIDTH = oFrom.w_POINTSHAPEWIDTH
    this.w_Uniqueshape = oFrom.w_Uniqueshape
    this.w_LINECAPS = oFrom.w_LINECAPS
    this.w_LINECAPSSHAPE = oFrom.w_LINECAPSSHAPE
    this.w_PieDetachAnimationSteps = oFrom.w_PieDetachAnimationSteps
    this.w_NUMSLICE = oFrom.w_NUMSLICE
    this.w_MODIFIED = oFrom.w_MODIFIED
    this.w_ShowAxis2Tics = oFrom.w_ShowAxis2Tics
    this.w_TicLength = oFrom.w_TicLength
    this.w_ScaleBackLinesDash = oFrom.w_ScaleBackLinesDash
    this.w_ScaleBackLinesWidth = oFrom.w_ScaleBackLinesWidth
    this.w_ScaleBackAlpha = oFrom.w_ScaleBackAlpha
    this.w_AxisAlpha = oFrom.w_AxisAlpha
    this.w_ShowLineZero = oFrom.w_ShowLineZero
    this.w_ScaleLineColor = oFrom.w_ScaleLineColor
    this.w_ScaleBackColor = oFrom.w_ScaleBackColor
    this.w_AxisColor = oFrom.w_AxisColor
    this.w_ScaleLineZeroColor = oFrom.w_ScaleLineZeroColor
    this.w_MultiChart = oFrom.w_MultiChart
    this.w_MultiChartMargin = oFrom.w_MultiChartMargin
    this.w_ChartRow = oFrom.w_ChartRow
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_code = this.w_code
    oTo.w_oChart = this.w_oChart
    oTo.w_CHARTTYPE = this.w_CHARTTYPE
    oTo.w_LEGENDPOSITION = this.w_LEGENDPOSITION
    oTo.w_SHOWVALUESONSHAPES = this.w_SHOWVALUESONSHAPES
    oTo.w_CHANGECOLORONMOUSE = this.w_CHANGECOLORONMOUSE
    oTo.w_SHOWTIPS = this.w_SHOWTIPS
    oTo.w_DEPTH = this.w_DEPTH
    oTo.w_SHADOW = this.w_SHADOW
    oTo.w_PIEENHANCEDDRAWING = this.w_PIEENHANCEDDRAWING
    oTo.w_QUERYMODEL = this.w_QUERYMODEL
    oTo.w_SHOWAXIS = this.w_SHOWAXIS
    oTo.w_FILEMODEL = this.w_FILEMODEL
    oTo.w_QUERY = this.w_QUERY
    oTo.w_FILECFG = this.w_FILECFG
    oTo.w_FILEREPORT = this.w_FILEREPORT
    oTo.w_FILERPT = this.w_FILERPT
    oTo.w_cSource = this.w_cSource
    oTo.w_MARGIN = this.w_MARGIN
    oTo.w_MARGINTOP = this.w_MARGINTOP
    oTo.w_MARGINBOTTOM = this.w_MARGINBOTTOM
    oTo.w_MARGINLEFT = this.w_MARGINLEFT
    oTo.w_MARGINRIGHT = this.w_MARGINRIGHT
    oTo.w_SHOWSCALE = this.w_SHOWSCALE
    oTo.w_SCALE = this.w_SCALE
    oTo.w_MINVALUE = this.w_MINVALUE
    oTo.w_MAXVALUE = this.w_MAXVALUE
    oTo.w_SCALEDIVIDER = this.w_SCALEDIVIDER
    oTo.w_CHKMINVALUE = this.w_CHKMINVALUE
    oTo.w_CHKMAXVALUE = this.w_CHKMAXVALUE
    oTo.w_BARSPERSCALE = this.w_BARSPERSCALE
    oTo.w_MINNUMBERSCALELEGENDS = this.w_MINNUMBERSCALELEGENDS
    oTo.w_SCALEAUTOFORMAT = this.w_SCALEAUTOFORMAT
    oTo.w_COLORTYPE = this.w_COLORTYPE
    oTo.w_BRUSHTYPE = this.w_BRUSHTYPE
    oTo.w_ALPHACHANNEL = this.w_ALPHACHANNEL
    oTo.w_GRADIENTSHAPEDIRECTION = this.w_GRADIENTSHAPEDIRECTION
    oTo.w_GRADIENTLEVEL = this.w_GRADIENTLEVEL
    oTo.w_GRADIENTINVERTCOLORS = this.w_GRADIENTINVERTCOLORS
    oTo.w_GRADIENTPOSITION = this.w_GRADIENTPOSITION
    oTo.w_GRADIENTTYPE = this.w_GRADIENTTYPE
    oTo.w_BACKCOLOR = this.w_BACKCOLOR
    oTo.w_BACKCOLOR2 = this.w_BACKCOLOR2
    oTo.w_BACKGRADIENTMODE = this.w_BACKGRADIENTMODE
    oTo.w_CHANGECOLOR = this.w_CHANGECOLOR
    oTo.w_BACKCOLORALPHA = this.w_BACKCOLORALPHA
    oTo.w_SHPMAINCOLOR = this.w_SHPMAINCOLOR
    oTo.w_RET = this.w_RET
    oTo.w_ROTATIONCENTER = this.w_ROTATIONCENTER
    oTo.w_ROTATION = this.w_ROTATION
    oTo.w_LBACKCOLOR = this.w_LBACKCOLOR
    oTo.w_LFORECOLOR = this.w_LFORECOLOR
    oTo.w_FIELDCOLOR = this.w_FIELDCOLOR
    oTo.w_FIELDDETACHSLICE = this.w_FIELDDETACHSLICE
    oTo.w_FIELDHIDESLICE = this.w_FIELDHIDESLICE
    oTo.w_FIELDAXIS2 = this.w_FIELDAXIS2
    oTo.w_FIELDLEGEND = this.w_FIELDLEGEND
    oTo.w_LBACKCOLORALPHA = this.w_LBACKCOLORALPHA
    oTo.w_ALIGNMENT = this.w_ALIGNMENT
    oTo.w_FONTUNDERLINE = this.w_FONTUNDERLINE
    oTo.w_FONTITALIC = this.w_FONTITALIC
    oTo.w_FONTBOLD = this.w_FONTBOLD
    oTo.w_FONTNAME = this.w_FONTNAME
    oTo.w_LFORECOLORALPHA = this.w_LFORECOLORALPHA
    oTo.w_FONTSIZE = this.w_FONTSIZE
    oTo.w_FORMAT = this.w_FORMAT
    oTo.w_CAPTION = this.w_CAPTION
    oTo.w_LEGENDS = this.w_LEGENDS
    oTo.w_SERIAL = this.w_SERIAL
    oTo.w_DONUTRATIO = this.w_DONUTRATIO
    oTo.w_PIEDETACHPIXELS = this.w_PIEDETACHPIXELS
    oTo.w_PIECOMPENSATEANGLES = this.w_PIECOMPENSATEANGLES
    oTo.w_PIESHOWPERCENT = this.w_PIESHOWPERCENT
    oTo.w_PIEDETACHSLICEONCLICK = this.w_PIEDETACHSLICEONCLICK
    oTo.w_PIEDETACHSLICEONLEGENDCLICK = this.w_PIEDETACHSLICEONLEGENDCLICK
    oTo.w_BARSSPACEBETWEEN = this.w_BARSSPACEBETWEEN
    oTo.w_BARTYPE = this.w_BARTYPE
    oTo.w_BARLEGENDDIRECTION = this.w_BARLEGENDDIRECTION
    oTo.w_AREA3DTOP = this.w_AREA3DTOP
    oTo.w_AREADRAWBORDERS = this.w_AREADRAWBORDERS
    oTo.w_POINTSHAPEWIDTH = this.w_POINTSHAPEWIDTH
    oTo.w_Uniqueshape = this.w_Uniqueshape
    oTo.w_LINECAPS = this.w_LINECAPS
    oTo.w_LINECAPSSHAPE = this.w_LINECAPSSHAPE
    oTo.w_PieDetachAnimationSteps = this.w_PieDetachAnimationSteps
    oTo.w_NUMSLICE = this.w_NUMSLICE
    oTo.w_MODIFIED = this.w_MODIFIED
    oTo.w_ShowAxis2Tics = this.w_ShowAxis2Tics
    oTo.w_TicLength = this.w_TicLength
    oTo.w_ScaleBackLinesDash = this.w_ScaleBackLinesDash
    oTo.w_ScaleBackLinesWidth = this.w_ScaleBackLinesWidth
    oTo.w_ScaleBackAlpha = this.w_ScaleBackAlpha
    oTo.w_AxisAlpha = this.w_AxisAlpha
    oTo.w_ShowLineZero = this.w_ShowLineZero
    oTo.w_ScaleLineColor = this.w_ScaleLineColor
    oTo.w_ScaleBackColor = this.w_ScaleBackColor
    oTo.w_AxisColor = this.w_AxisColor
    oTo.w_ScaleLineZeroColor = this.w_ScaleLineZeroColor
    oTo.w_MultiChart = this.w_MultiChart
    oTo.w_MultiChartMargin = this.w_MultiChartMargin
    oTo.w_ChartRow = this.w_ChartRow
    PCContext::Load(oTo)
enddefine

define class tccp_foxchartproperties as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 631
  Height = 225+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-05-09"
  HelpContextID=220659050
  max_rt_seq=101

  * --- Constant Properties
  cpfcprop_IDX = 0
  cFile = "cpfcprop"
  cKeySelect = "code"
  cKeyWhere  = "code=this.w_code"
  cKeyWhereODBC = '"code="+cp_ToStrODBC(this.w_code)';

  cKeyWhereODBCqualified = '"cpfcprop.code="+cp_ToStrODBC(this.w_code)';

  cPrg = "cp_foxchartproperties"
  cComment = "FoxChartProperties"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_code = space(3)
  w_oChart = space(10)
  w_CHARTTYPE = 0
  o_CHARTTYPE = 0
  w_LEGENDPOSITION = 0
  o_LEGENDPOSITION = 0
  w_SHOWVALUESONSHAPES = .F.
  o_SHOWVALUESONSHAPES = .F.
  w_CHANGECOLORONMOUSE = .F.
  o_CHANGECOLORONMOUSE = .F.
  w_SHOWTIPS = .F.
  o_SHOWTIPS = .F.
  w_DEPTH = 0
  o_DEPTH = 0
  w_SHADOW = .F.
  o_SHADOW = .F.
  w_PIEENHANCEDDRAWING = .F.
  o_PIEENHANCEDDRAWING = .F.
  w_QUERYMODEL = space(250)
  o_QUERYMODEL = space(250)
  w_SHOWAXIS = .F.
  o_SHOWAXIS = .F.
  w_FILEMODEL = space(250)
  o_FILEMODEL = space(250)
  w_QUERY = space(254)
  o_QUERY = space(254)
  w_FILECFG = space(254)
  o_FILECFG = space(254)
  w_FILEREPORT = space(250)
  o_FILEREPORT = space(250)
  w_FILERPT = space(254)
  o_FILERPT = space(254)
  w_cSource = space(10)
  w_MARGIN = 0
  o_MARGIN = 0
  w_MARGINTOP = 0
  o_MARGINTOP = 0
  w_MARGINBOTTOM = 0
  o_MARGINBOTTOM = 0
  w_MARGINLEFT = 0
  o_MARGINLEFT = 0
  w_MARGINRIGHT = 0
  o_MARGINRIGHT = 0
  w_SHOWSCALE = .F.
  o_SHOWSCALE = .F.
  w_SCALE = 0
  o_SCALE = 0
  w_MINVALUE = 0
  o_MINVALUE = 0
  w_MAXVALUE = 0
  o_MAXVALUE = 0
  w_SCALEDIVIDER = 0
  o_SCALEDIVIDER = 0
  w_CHKMINVALUE = .F.
  o_CHKMINVALUE = .F.
  w_CHKMAXVALUE = .F.
  o_CHKMAXVALUE = .F.
  w_BARSPERSCALE = 0
  o_BARSPERSCALE = 0
  w_MINNUMBERSCALELEGENDS = 0
  o_MINNUMBERSCALELEGENDS = 0
  w_SCALEAUTOFORMAT = .F.
  o_SCALEAUTOFORMAT = .F.
  w_COLORTYPE = 0
  o_COLORTYPE = 0
  w_BRUSHTYPE = 0
  o_BRUSHTYPE = 0
  w_ALPHACHANNEL = 0
  o_ALPHACHANNEL = 0
  w_GRADIENTSHAPEDIRECTION = 0
  o_GRADIENTSHAPEDIRECTION = 0
  w_GRADIENTLEVEL = 0
  o_GRADIENTLEVEL = 0
  w_GRADIENTINVERTCOLORS = .F.
  o_GRADIENTINVERTCOLORS = .F.
  w_GRADIENTPOSITION = 0
  o_GRADIENTPOSITION = 0
  w_GRADIENTTYPE = 0
  o_GRADIENTTYPE = 0
  w_BACKCOLOR = 0
  o_BACKCOLOR = 0
  w_BACKCOLOR2 = 0
  o_BACKCOLOR2 = 0
  w_BACKGRADIENTMODE = 0
  o_BACKGRADIENTMODE = 0
  w_CHANGECOLOR = 0
  w_BACKCOLORALPHA = 0
  o_BACKCOLORALPHA = 0
  w_SHPMAINCOLOR = 0
  o_SHPMAINCOLOR = 0
  w_RET = .F.
  w_ROTATIONCENTER = 0
  o_ROTATIONCENTER = 0
  w_ROTATION = 0
  o_ROTATION = 0
  w_LBACKCOLOR = 0
  o_LBACKCOLOR = 0
  w_LFORECOLOR = 0
  o_LFORECOLOR = 0
  w_FIELDCOLOR = space(25)
  o_FIELDCOLOR = space(25)
  w_FIELDDETACHSLICE = space(25)
  o_FIELDDETACHSLICE = space(25)
  w_FIELDHIDESLICE = space(25)
  o_FIELDHIDESLICE = space(25)
  w_FIELDAXIS2 = space(25)
  o_FIELDAXIS2 = space(25)
  w_FIELDLEGEND = space(25)
  o_FIELDLEGEND = space(25)
  w_LBACKCOLORALPHA = 0
  o_LBACKCOLORALPHA = 0
  w_ALIGNMENT = 0
  o_ALIGNMENT = 0
  w_FONTUNDERLINE = .F.
  o_FONTUNDERLINE = .F.
  w_FONTITALIC = .F.
  o_FONTITALIC = .F.
  w_FONTBOLD = .F.
  o_FONTBOLD = .F.
  w_FONTNAME = space(50)
  o_FONTNAME = space(50)
  w_LFORECOLORALPHA = 0
  o_LFORECOLORALPHA = 0
  w_FONTSIZE = 0
  o_FONTSIZE = 0
  w_FORMAT = space(200)
  o_FORMAT = space(200)
  w_CAPTION = space(200)
  o_CAPTION = space(200)
  w_LEGENDS = 0
  w_SERIAL = space(10)
  w_DONUTRATIO = 0
  o_DONUTRATIO = 0
  w_PIEDETACHPIXELS = 0
  o_PIEDETACHPIXELS = 0
  w_PIECOMPENSATEANGLES = .F.
  o_PIECOMPENSATEANGLES = .F.
  w_PIESHOWPERCENT = .F.
  o_PIESHOWPERCENT = .F.
  w_PIEDETACHSLICEONCLICK = .F.
  o_PIEDETACHSLICEONCLICK = .F.
  w_PIEDETACHSLICEONLEGENDCLICK = .F.
  o_PIEDETACHSLICEONLEGENDCLICK = .F.
  w_BARSSPACEBETWEEN = 0
  o_BARSSPACEBETWEEN = 0
  w_BARTYPE = 0
  o_BARTYPE = 0
  w_BARLEGENDDIRECTION = 0
  o_BARLEGENDDIRECTION = 0
  w_AREA3DTOP = .F.
  o_AREA3DTOP = .F.
  w_AREADRAWBORDERS = .F.
  o_AREADRAWBORDERS = .F.
  w_POINTSHAPEWIDTH = 0
  o_POINTSHAPEWIDTH = 0
  w_Uniqueshape = .F.
  o_Uniqueshape = .F.
  w_LINECAPS = .F.
  o_LINECAPS = .F.
  w_LINECAPSSHAPE = 0
  o_LINECAPSSHAPE = 0
  w_PieDetachAnimationSteps = 0
  o_PieDetachAnimationSteps = 0
  w_NUMSLICE = 0
  o_NUMSLICE = 0
  w_MODIFIED = .F.
  w_ShowAxis2Tics = .F.
  o_ShowAxis2Tics = .F.
  w_TicLength = 0
  o_TicLength = 0
  w_ScaleBackLinesDash = 0
  o_ScaleBackLinesDash = 0
  w_ScaleBackLinesWidth = 0
  o_ScaleBackLinesWidth = 0
  w_ScaleBackAlpha = 0
  o_ScaleBackAlpha = 0
  w_AxisAlpha = 0
  o_AxisAlpha = 0
  w_ShowLineZero = .F.
  o_ShowLineZero = .F.
  w_ScaleLineColor = 0
  o_ScaleLineColor = 0
  w_ScaleBackColor = 0
  o_ScaleBackColor = 0
  w_AxisColor = 0
  o_AxisColor = 0
  w_ScaleLineZeroColor = 0
  o_ScaleLineZeroColor = 0
  w_MultiChart = .F.
  o_MultiChart = .F.
  w_MultiChartMargin = .F.
  o_MultiChartMargin = .F.
  w_ChartRow = 0
  o_ChartRow = 0

  * --- Children pointers
  cp_foxchartfields = .NULL.
  w_BTNQRY = .NULL.
  w_BTNMOD = .NULL.
  w_BTNREP = .NULL.
  w_GraphData = .NULL.
  w_REFSHPMAIN = .NULL.
  w_oBckColorChart1 = .NULL.
  w_oBckColorChart2 = .NULL.
  w_oBckColorChartL = .NULL.
  w_oForeColorChartL = .NULL.
  w_oScaleLineColor = .NULL.
  w_oScaleBackColor = .NULL.
  w_oAxisColor = .NULL.
  w_oScaleLineZeroColor = .NULL.
  * --- Area Manuale = Declare Variables
  * --- cp_foxchartproperties
  Anchor=15
  
  Proc SetColor(pVar)
     Local L_ExecCMD, l_Color
     L_ExecCMD= 'this.w_'+pVar
     l_Color = GetColor(&L_ExecCMD)
     If l_Color = -1
       l_Color = RGB(255,255,255)
     EndIf
     If l_Color <> -1
       L_ExecCMD= L_ExecCMD+'= m.l_COLOR'
       &L_ExecCMD
     EndIf
     this.mCalc(.t.)
  EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=8, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_foxchartpropertiesPag1","cp_foxchartproperties",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Style")
      .Pages(1).HelpContextID = 197750479
      .Pages(2).addobject("oPag","tcp_foxchartpropertiesPag2","cp_foxchartproperties",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Properties")
      .Pages(2).HelpContextID = 187396347
      .Pages(3).addobject("oPag","tcp_foxchartpropertiesPag3","cp_foxchartproperties",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Fields")
      .Pages(3).HelpContextID = 19882565
      .Pages(4).addobject("oPag","tcp_foxchartpropertiesPag4","cp_foxchartproperties",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Label / Legends")
      .Pages(4).HelpContextID = 210490881
      .Pages(5).addobject("oPag","tcp_foxchartpropertiesPag5","cp_foxchartproperties",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Axes")
      .Pages(5).HelpContextID = 164170283
      .Pages(6).addobject("oPag","tcp_foxchartpropertiesPag6","cp_foxchartproperties",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Color / BackGround")
      .Pages(6).HelpContextID = 171906555
      .Pages(7).addobject("oPag","tcp_foxchartpropertiesPag7","cp_foxchartproperties",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Scales / Margins")
      .Pages(7).HelpContextID = 109419832
      .Pages(8).addobject("oPag","tcp_foxchartpropertiesPag8","cp_foxchartproperties",8)
      .Pages(8).oPag.Visible=.t.
      .Pages(8).Caption=cp_Translate("Data")
      .Pages(8).HelpContextID = 214501658
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCHARTTYPE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
    proc Init()
      this.w_BTNQRY = this.oPgFrm.Pages(1).oPag.BTNQRY
      this.w_BTNMOD = this.oPgFrm.Pages(1).oPag.BTNMOD
      this.w_BTNREP = this.oPgFrm.Pages(1).oPag.BTNREP
      this.w_GraphData = this.oPgFrm.Pages(8).oPag.GraphData
      this.w_REFSHPMAIN = this.oPgFrm.Pages(6).oPag.REFSHPMAIN
      this.w_oBckColorChart1 = this.oPgFrm.Pages(6).oPag.oBckColorChart1
      this.w_oBckColorChart2 = this.oPgFrm.Pages(6).oPag.oBckColorChart2
      this.w_oBckColorChartL = this.oPgFrm.Pages(4).oPag.oBckColorChartL
      this.w_oForeColorChartL = this.oPgFrm.Pages(4).oPag.oForeColorChartL
      this.w_oScaleLineColor = this.oPgFrm.Pages(5).oPag.oScaleLineColor
      this.w_oScaleBackColor = this.oPgFrm.Pages(5).oPag.oScaleBackColor
      this.w_oAxisColor = this.oPgFrm.Pages(5).oPag.oAxisColor
      this.w_oScaleLineZeroColor = this.oPgFrm.Pages(5).oPag.oScaleLineZeroColor
      DoDefault()
    proc Destroy()
      this.w_BTNQRY = .NULL.
      this.w_BTNMOD = .NULL.
      this.w_BTNREP = .NULL.
      this.w_GraphData = .NULL.
      this.w_REFSHPMAIN = .NULL.
      this.w_oBckColorChart1 = .NULL.
      this.w_oBckColorChart2 = .NULL.
      this.w_oBckColorChartL = .NULL.
      this.w_oForeColorChartL = .NULL.
      this.w_oScaleLineColor = .NULL.
      this.w_oScaleBackColor = .NULL.
      this.w_oAxisColor = .NULL.
      this.w_oScaleLineZeroColor = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='cpfcprop'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.cpfcprop_IDX,5],7]
    this.nPostItConn=i_TableProp[this.cpfcprop_IDX,3]
  return

  function CreateChildren()
    this.cp_foxchartfields = CREATEOBJECT('stdDynamicChild',this,'cp_foxchartfields',this.oPgFrm.Page3.oPag.oLinkPC_3_2)
    this.cp_foxchartfields.createrealchild()
    return

  procedure NewContext()
    return(createobject('tscp_foxchartproperties'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.cp_foxchartfields)
      this.cp_foxchartfields.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.cp_foxchartfields.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.cp_foxchartfields.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.cp_foxchartfields)
      this.cp_foxchartfields.DestroyChildrenChain()
      this.cp_foxchartfields=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_2')
    return

  function IsAChildUpdated()
    local i_bRes
    i_bRes = this.bUpdated
    i_bRes = i_bRes .or. this.cp_foxchartfields.IsAChildUpdated()
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.cp_foxchartfields.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.cp_foxchartfields.SetKey(;
            .w_SERIAL,"FDSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .cp_foxchartfields.ChangeRow(this.cRowID+'      1',1;
             ,.w_SERIAL,"FDSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.cp_foxchartfields)
        i_f=.cp_foxchartfields.BuildFilter()
        if !(i_f==.cp_foxchartfields.cQueryFilter)
          i_fnidx=.cp_foxchartfields.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.cp_foxchartfields.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.cp_foxchartfields.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.cp_foxchartfields.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.cp_foxchartfields.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from cpfcprop where code=KeySet.code
    *
    i_nConn = i_TableProp[this.cpfcprop_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.cpfcprop_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('cpfcprop')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "cpfcprop.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' cpfcprop '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'code',this.w_code  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_oChart = this.oParentoBject .w_oChart
        .w_CHARTTYPE = 2
        .w_LEGENDPOSITION = 0
        .w_SHOWVALUESONSHAPES = .F.
        .w_CHANGECOLORONMOUSE = .F.
        .w_SHOWTIPS = .F.
        .w_DEPTH = 20
        .w_SHADOW = .F.
        .w_PIEENHANCEDDRAWING = .T.
        .w_SHOWAXIS = .f.
        .w_QUERY = space(254)
        .w_FILECFG = space(254)
        .w_FILERPT = space(254)
        .w_cSource = space(10)
        .w_MARGIN = 4
        .w_MARGINTOP = 10
        .w_MARGINBOTTOM = 10
        .w_MARGINLEFT = 10
        .w_MARGINRIGHT = 10
        .w_SHOWSCALE = .F.
        .w_SCALE = 0
        .w_MINVALUE = 0
        .w_MAXVALUE = 0
        .w_SCALEDIVIDER = 1
        .w_CHKMINVALUE = .F.
        .w_CHKMAXVALUE = .F.
        .w_BARSPERSCALE = 10
        .w_MINNUMBERSCALELEGENDS = 10
        .w_SCALEAUTOFORMAT = .f.
        .w_COLORTYPE = 3
        .w_BRUSHTYPE = 1
        .w_ALPHACHANNEL = 255
        .w_GRADIENTSHAPEDIRECTION = 0
        .w_GRADIENTLEVEL = 5
        .w_GRADIENTINVERTCOLORS = .F.
        .w_GRADIENTPOSITION = 1.0
        .w_GRADIENTTYPE = 1
        .w_BACKCOLOR = RGB(255,255,255)
        .w_BACKCOLOR2 = RGB(255,255,255)
        .w_BACKGRADIENTMODE = 0
        .w_CHANGECOLOR = 5
        .w_BACKCOLORALPHA = 0
        .w_SHPMAINCOLOR = 0
        .w_RET = .f.
        .w_ROTATIONCENTER = 0
        .w_ROTATION = 0
        .w_LBACKCOLOR = RGB(255,255,255)
        .w_LFORECOLOR = 0
        .w_FIELDCOLOR = space(25)
        .w_LBACKCOLORALPHA = 0
        .w_ALIGNMENT = 2
        .w_FONTUNDERLINE = .F.
        .w_FONTITALIC = .F.
        .w_FONTBOLD = .T.
        .w_FONTNAME = "Tahoma"
        .w_LFORECOLORALPHA = 255
        .w_FONTSIZE = IIF(.w_LEGENDS = 0 , 16 , IIF(.w_LEGENDS=1, 11, IIF(.w_LEGENDS=2 or .w_LEGENDS=4,10,IIF(.w_LEGENDS=3 or .w_LEGENDS=5 or .w_LEGENDs=7, 9 , 6))))
        .w_FORMAT = space(200)
        .w_CAPTION = space(200)
        .w_LEGENDS = 0
        .w_SERIAL = '0000000001'
        .w_DONUTRATIO = 0.50
        .w_PIEDETACHPIXELS = 30
        .w_PIECOMPENSATEANGLES = IIF( INLIST(.w_CHARTTYPE, 1, 2), .w_PIECOMPENSATEANGLES , .F. )
        .w_PIESHOWPERCENT = IIF(.w_SHOWVALUESONSHAPES , .w_PIECOMPENSATEANGLES , .F.)
        .w_PIEDETACHSLICEONCLICK = .F.
        .w_PIEDETACHSLICEONLEGENDCLICK = .F.
        .w_BARSSPACEBETWEEN = 0
        .w_BARTYPE = 0
        .w_BARLEGENDDIRECTION = 0
        .w_AREA3DTOP = .F.
        .w_AREADRAWBORDERS = .F.
        .w_POINTSHAPEWIDTH = 1
        .w_Uniqueshape = IIF( .w_CHARTTYPE=4, .T,  IIF( .w_CHARTTYPE=5, .F. , .F.))
        .w_PieDetachAnimationSteps = 3
        .w_MODIFIED = .F.
        .w_ShowAxis2Tics = .f.
        .w_TicLength = 5
        .w_ScaleBackLinesDash = 0
        .w_ScaleBackLinesWidth = 3
        .w_ScaleBackAlpha = 0
        .w_AxisAlpha = 255
        .w_ShowLineZero = .f.
        .w_ScaleLineColor = RGB(255,255,255)
        .w_ScaleBackColor = RGB(255,255,255)
        .w_AxisColor = RGB(255,255,255)
        .w_ScaleLineZeroColor = RGB(255,255,255)
        .w_MultiChart = .F.
        .w_ChartRow = 1
        .w_code = NVL(code,space(3))
        .w_QUERYMODEL = SYS(2014, .w_QUERY)
        .w_FILEMODEL = SYS(2014, .w_FILECFG)
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .oPgFrm.Page1.oPag.BTNMOD.Calculate()
        .w_FILEREPORT = SYS(2014, .w_FILERPT)
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page8.oPag.GraphData.Calculate()
        .oPgFrm.Page6.oPag.REFSHPMAIN.Calculate('      1234567890     ',.w_SHPMAINCOLOR,.w_SHPMAINCOLOR)
        .oPgFrm.Page6.oPag.oBckColorChart1.Calculate('      1234567890     ',.w_BACKCOLOR,.w_BACKCOLOR)
        .oPgFrm.Page6.oPag.oBckColorChart2.Calculate('      1234567890     ',.w_BACKCOLOR2, .w_BACKCOLOR2)
        .oPgFrm.Page4.oPag.oBckColorChartL.Calculate('      1234567890     ',.w_LBACKCOLOR, .w_LBACKCOLOR)
        .oPgFrm.Page4.oPag.oForeColorChartL.Calculate('      1234567890     ',.w_LFORECOLOR,.w_LFORECOLOR)
        .w_FIELDDETACHSLICE = IIF( Inlist(.w_CHARTTYPE, 1, 2 ), .w_FIELDDETACHSLICE , ' ' )
        .w_FIELDHIDESLICE = IIF( Inlist(.w_CHARTTYPE, 1, 2 ), .w_FIELDHIDESLICE , ' ' )
        .w_FIELDAXIS2 = IIF( Inlist(.w_CHARTTYPE, 1, 2), ' ' , .w_FIELDAXIS2 )
        .w_FIELDLEGEND = IIF( Inlist(.w_CHARTTYPE, 1, 2, 7), .w_FIELDLEGEND , ' ' )
        .w_LINECAPS = IIF(.w_Uniqueshape , .w_LINECAPS  , .F.)
        .w_LINECAPSSHAPE = IIF(.w_CHARTTYPE=5 And .w_DEPTH = 0, .w_LINECAPSSHAPE , 0)
        .w_NUMSLICE = IIF( Inlist(.w_CHARTTYPE , 1, 2) , .w_NUMSLICE, 0)
        .oPgFrm.Page5.oPag.oScaleLineColor.Calculate('      1234567890     ', .w_ScaleLineColor , .w_ScaleLineColor)
        .oPgFrm.Page5.oPag.oScaleBackColor.Calculate('      1234567890     ', .w_ScaleBackColor ,  .w_ScaleBackColor)
        .oPgFrm.Page5.oPag.oAxisColor.Calculate('      1234567890     ', .w_AxisColor, .w_AxisColor)
        .oPgFrm.Page5.oPag.oScaleLineZeroColor.Calculate('      1234567890     ',.w_ScaleLineZeroColor ,  .w_ScaleLineZeroColor)
        .w_MultiChartMargin = .F.
        cp_LoadRecExtFlds(this,'cpfcprop')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page6.oPag.oBtn_6_10.enabled = this.oPgFrm.Page6.oPag.oBtn_6_10.mCond()
      this.oPgFrm.Page6.oPag.oBtn_6_27.enabled = this.oPgFrm.Page6.oPag.oBtn_6_27.mCond()
      this.oPgFrm.Page6.oPag.oBtn_6_29.enabled = this.oPgFrm.Page6.oPag.oBtn_6_29.mCond()
      this.oPgFrm.Page4.oPag.oBtn_4_8.enabled = this.oPgFrm.Page4.oPag.oBtn_4_8.mCond()
      this.oPgFrm.Page4.oPag.oBtn_4_10.enabled = this.oPgFrm.Page4.oPag.oBtn_4_10.mCond()
      this.oPgFrm.Page5.oPag.oBtn_5_10.enabled = this.oPgFrm.Page5.oPag.oBtn_5_10.mCond()
      this.oPgFrm.Page5.oPag.oBtn_5_12.enabled = this.oPgFrm.Page5.oPag.oBtn_5_12.mCond()
      this.oPgFrm.Page5.oPag.oBtn_5_18.enabled = this.oPgFrm.Page5.oPag.oBtn_5_18.mCond()
      this.oPgFrm.Page5.oPag.oBtn_5_20.enabled = this.oPgFrm.Page5.oPag.oBtn_5_20.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_code = space(3)
      .w_oChart = space(10)
      .w_CHARTTYPE = 0
      .w_LEGENDPOSITION = 0
      .w_SHOWVALUESONSHAPES = .f.
      .w_CHANGECOLORONMOUSE = .f.
      .w_SHOWTIPS = .f.
      .w_DEPTH = 0
      .w_SHADOW = .f.
      .w_PIEENHANCEDDRAWING = .f.
      .w_QUERYMODEL = space(250)
      .w_SHOWAXIS = .f.
      .w_FILEMODEL = space(250)
      .w_QUERY = space(254)
      .w_FILECFG = space(254)
      .w_FILEREPORT = space(250)
      .w_FILERPT = space(254)
      .w_cSource = space(10)
      .w_MARGIN = 0
      .w_MARGINTOP = 0
      .w_MARGINBOTTOM = 0
      .w_MARGINLEFT = 0
      .w_MARGINRIGHT = 0
      .w_SHOWSCALE = .f.
      .w_SCALE = 0
      .w_MINVALUE = 0
      .w_MAXVALUE = 0
      .w_SCALEDIVIDER = 0
      .w_CHKMINVALUE = .f.
      .w_CHKMAXVALUE = .f.
      .w_BARSPERSCALE = 0
      .w_MINNUMBERSCALELEGENDS = 0
      .w_SCALEAUTOFORMAT = .f.
      .w_COLORTYPE = 0
      .w_BRUSHTYPE = 0
      .w_ALPHACHANNEL = 0
      .w_GRADIENTSHAPEDIRECTION = 0
      .w_GRADIENTLEVEL = 0
      .w_GRADIENTINVERTCOLORS = .f.
      .w_GRADIENTPOSITION = 0
      .w_GRADIENTTYPE = 0
      .w_BACKCOLOR = 0
      .w_BACKCOLOR2 = 0
      .w_BACKGRADIENTMODE = 0
      .w_CHANGECOLOR = 0
      .w_BACKCOLORALPHA = 0
      .w_SHPMAINCOLOR = 0
      .w_RET = .f.
      .w_ROTATIONCENTER = 0
      .w_ROTATION = 0
      .w_LBACKCOLOR = 0
      .w_LFORECOLOR = 0
      .w_FIELDCOLOR = space(25)
      .w_FIELDDETACHSLICE = space(25)
      .w_FIELDHIDESLICE = space(25)
      .w_FIELDAXIS2 = space(25)
      .w_FIELDLEGEND = space(25)
      .w_LBACKCOLORALPHA = 0
      .w_ALIGNMENT = 0
      .w_FONTUNDERLINE = .f.
      .w_FONTITALIC = .f.
      .w_FONTBOLD = .f.
      .w_FONTNAME = space(50)
      .w_LFORECOLORALPHA = 0
      .w_FONTSIZE = 0
      .w_FORMAT = space(200)
      .w_CAPTION = space(200)
      .w_LEGENDS = 0
      .w_SERIAL = space(10)
      .w_DONUTRATIO = 0
      .w_PIEDETACHPIXELS = 0
      .w_PIECOMPENSATEANGLES = .f.
      .w_PIESHOWPERCENT = .f.
      .w_PIEDETACHSLICEONCLICK = .f.
      .w_PIEDETACHSLICEONLEGENDCLICK = .f.
      .w_BARSSPACEBETWEEN = 0
      .w_BARTYPE = 0
      .w_BARLEGENDDIRECTION = 0
      .w_AREA3DTOP = .f.
      .w_AREADRAWBORDERS = .f.
      .w_POINTSHAPEWIDTH = 0
      .w_Uniqueshape = .f.
      .w_LINECAPS = .f.
      .w_LINECAPSSHAPE = 0
      .w_PieDetachAnimationSteps = 0
      .w_NUMSLICE = 0
      .w_MODIFIED = .f.
      .w_ShowAxis2Tics = .f.
      .w_TicLength = 0
      .w_ScaleBackLinesDash = 0
      .w_ScaleBackLinesWidth = 0
      .w_ScaleBackAlpha = 0
      .w_AxisAlpha = 0
      .w_ShowLineZero = .f.
      .w_ScaleLineColor = 0
      .w_ScaleBackColor = 0
      .w_AxisColor = 0
      .w_ScaleLineZeroColor = 0
      .w_MultiChart = .f.
      .w_MultiChartMargin = .f.
      .w_ChartRow = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_oChart = this.oParentoBject .w_oChart
        .w_CHARTTYPE = 2
        .w_LEGENDPOSITION = 0
        .w_SHOWVALUESONSHAPES = .F.
        .w_CHANGECOLORONMOUSE = .F.
        .w_SHOWTIPS = .F.
        .w_DEPTH = 20
        .w_SHADOW = .F.
        .w_PIEENHANCEDDRAWING = .T.
        .w_QUERYMODEL = SYS(2014, .w_QUERY)
          .DoRTCalc(12,12,.f.)
        .w_FILEMODEL = SYS(2014, .w_FILECFG)
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .oPgFrm.Page1.oPag.BTNMOD.Calculate()
          .DoRTCalc(14,15,.f.)
        .w_FILEREPORT = SYS(2014, .w_FILERPT)
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page8.oPag.GraphData.Calculate()
          .DoRTCalc(17,18,.f.)
        .w_MARGIN = 4
        .w_MARGINTOP = 10
        .w_MARGINBOTTOM = 10
        .w_MARGINLEFT = 10
        .w_MARGINRIGHT = 10
        .w_SHOWSCALE = .F.
        .w_SCALE = 0
        .w_MINVALUE = 0
        .w_MAXVALUE = 0
        .w_SCALEDIVIDER = 1
        .w_CHKMINVALUE = .F.
        .w_CHKMAXVALUE = .F.
        .w_BARSPERSCALE = 10
        .w_MINNUMBERSCALELEGENDS = 10
          .DoRTCalc(33,33,.f.)
        .w_COLORTYPE = 3
        .w_BRUSHTYPE = 1
        .w_ALPHACHANNEL = 255
        .oPgFrm.Page6.oPag.REFSHPMAIN.Calculate('      1234567890     ',.w_SHPMAINCOLOR,.w_SHPMAINCOLOR)
        .w_GRADIENTSHAPEDIRECTION = 0
        .w_GRADIENTLEVEL = 5
        .w_GRADIENTINVERTCOLORS = .F.
        .w_GRADIENTPOSITION = 1.0
        .w_GRADIENTTYPE = 1
        .w_BACKCOLOR = RGB(255,255,255)
        .oPgFrm.Page6.oPag.oBckColorChart1.Calculate('      1234567890     ',.w_BACKCOLOR,.w_BACKCOLOR)
        .w_BACKCOLOR2 = RGB(255,255,255)
        .oPgFrm.Page6.oPag.oBckColorChart2.Calculate('      1234567890     ',.w_BACKCOLOR2, .w_BACKCOLOR2)
        .w_BACKGRADIENTMODE = 0
        .w_CHANGECOLOR = 5
        .w_BACKCOLORALPHA = 0
          .DoRTCalc(47,48,.f.)
        .w_ROTATIONCENTER = 0
        .w_ROTATION = 0
        .w_LBACKCOLOR = RGB(255,255,255)
        .w_LFORECOLOR = 0
        .oPgFrm.Page4.oPag.oBckColorChartL.Calculate('      1234567890     ',.w_LBACKCOLOR, .w_LBACKCOLOR)
        .oPgFrm.Page4.oPag.oForeColorChartL.Calculate('      1234567890     ',.w_LFORECOLOR,.w_LFORECOLOR)
          .DoRTCalc(53,53,.f.)
        .w_FIELDDETACHSLICE = IIF( Inlist(.w_CHARTTYPE, 1, 2 ), .w_FIELDDETACHSLICE , ' ' )
        .w_FIELDHIDESLICE = IIF( Inlist(.w_CHARTTYPE, 1, 2 ), .w_FIELDHIDESLICE , ' ' )
        .w_FIELDAXIS2 = IIF( Inlist(.w_CHARTTYPE, 1, 2), ' ' , .w_FIELDAXIS2 )
        .w_FIELDLEGEND = IIF( Inlist(.w_CHARTTYPE, 1, 2, 7), .w_FIELDLEGEND , ' ' )
        .w_LBACKCOLORALPHA = 0
        .w_ALIGNMENT = 2
        .w_FONTUNDERLINE = .F.
        .w_FONTITALIC = .F.
        .w_FONTBOLD = .T.
        .w_FONTNAME = "Tahoma"
        .w_LFORECOLORALPHA = 255
        .w_FONTSIZE = IIF(.w_LEGENDS = 0 , 16 , IIF(.w_LEGENDS=1, 11, IIF(.w_LEGENDS=2 or .w_LEGENDS=4,10,IIF(.w_LEGENDS=3 or .w_LEGENDS=5 or .w_LEGENDs=7, 9 , 6))))
          .DoRTCalc(66,67,.f.)
        .w_LEGENDS = 0
        .w_SERIAL = '0000000001'
        .w_DONUTRATIO = 0.50
        .w_PIEDETACHPIXELS = 30
        .w_PIECOMPENSATEANGLES = IIF( INLIST(.w_CHARTTYPE, 1, 2), .w_PIECOMPENSATEANGLES , .F. )
        .w_PIESHOWPERCENT = IIF(.w_SHOWVALUESONSHAPES , .w_PIECOMPENSATEANGLES , .F.)
        .w_PIEDETACHSLICEONCLICK = .F.
        .w_PIEDETACHSLICEONLEGENDCLICK = .F.
          .DoRTCalc(76,78,.f.)
        .w_AREA3DTOP = .F.
        .w_AREADRAWBORDERS = .F.
        .w_POINTSHAPEWIDTH = 1
        .w_Uniqueshape = IIF( .w_CHARTTYPE=4, .T,  IIF( .w_CHARTTYPE=5, .F. , .F.))
        .w_LINECAPS = IIF(.w_Uniqueshape , .w_LINECAPS  , .F.)
        .w_LINECAPSSHAPE = IIF(.w_CHARTTYPE=5 And .w_DEPTH = 0, .w_LINECAPSSHAPE , 0)
        .w_PieDetachAnimationSteps = 3
        .w_NUMSLICE = IIF( Inlist(.w_CHARTTYPE , 1, 2) , .w_NUMSLICE, 0)
        .w_MODIFIED = .F.
          .DoRTCalc(88,88,.f.)
        .w_TicLength = 5
        .w_ScaleBackLinesDash = 0
        .w_ScaleBackLinesWidth = 3
        .oPgFrm.Page5.oPag.oScaleLineColor.Calculate('      1234567890     ', .w_ScaleLineColor , .w_ScaleLineColor)
        .oPgFrm.Page5.oPag.oScaleBackColor.Calculate('      1234567890     ', .w_ScaleBackColor ,  .w_ScaleBackColor)
        .oPgFrm.Page5.oPag.oAxisColor.Calculate('      1234567890     ', .w_AxisColor, .w_AxisColor)
        .oPgFrm.Page5.oPag.oScaleLineZeroColor.Calculate('      1234567890     ',.w_ScaleLineZeroColor ,  .w_ScaleLineZeroColor)
        .w_ScaleBackAlpha = 0
        .w_AxisAlpha = 255
          .DoRTCalc(94,94,.f.)
        .w_ScaleLineColor = RGB(255,255,255)
        .w_ScaleBackColor = RGB(255,255,255)
        .w_AxisColor = RGB(255,255,255)
        .w_ScaleLineZeroColor = RGB(255,255,255)
        .w_MultiChart = .F.
        .w_MultiChartMargin = .F.
        .w_ChartRow = 1
      endif
    endwith
    cp_BlankRecExtFlds(this,'cpfcprop')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page6.oPag.oBtn_6_10.enabled = this.oPgFrm.Page6.oPag.oBtn_6_10.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_27.enabled = this.oPgFrm.Page6.oPag.oBtn_6_27.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_29.enabled = this.oPgFrm.Page6.oPag.oBtn_6_29.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_8.enabled = this.oPgFrm.Page4.oPag.oBtn_4_8.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_10.enabled = this.oPgFrm.Page4.oPag.oBtn_4_10.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_10.enabled = this.oPgFrm.Page5.oPag.oBtn_5_10.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_12.enabled = this.oPgFrm.Page5.oPag.oBtn_5_12.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_18.enabled = this.oPgFrm.Page5.oPag.oBtn_5_18.mCond()
    this.oPgFrm.Page5.oPag.oBtn_5_20.enabled = this.oPgFrm.Page5.oPag.oBtn_5_20.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCHARTTYPE_1_3.enabled = i_bVal
      .Page1.oPag.oLEGENDPOSITION_1_5.enabled = i_bVal
      .Page1.oPag.oSHOWVALUESONSHAPES_1_7.enabled = i_bVal
      .Page1.oPag.oCHANGECOLORONMOUSE_1_8.enabled = i_bVal
      .Page1.oPag.oSHOWTIPS_1_9.enabled = i_bVal
      .Page1.oPag.oDEPTH_1_10.enabled = i_bVal
      .Page1.oPag.oSHADOW_1_12.enabled = i_bVal
      .Page1.oPag.oQUERYMODEL_1_14.enabled = i_bVal
      .Page5.oPag.oSHOWAXIS_5_1.enabled = i_bVal
      .Page1.oPag.oFILEMODEL_1_18.enabled = i_bVal
      .Page1.oPag.oFILEREPORT_1_27.enabled = i_bVal
      .Page7.oPag.oMARGIN_7_4.enabled = i_bVal
      .Page7.oPag.oMARGINTOP_7_6.enabled = i_bVal
      .Page7.oPag.oMARGINBOTTOM_7_8.enabled = i_bVal
      .Page7.oPag.oMARGINLEFT_7_10.enabled = i_bVal
      .Page7.oPag.oMARGINRIGHT_7_12.enabled = i_bVal
      .Page7.oPag.oSHOWSCALE_7_13.enabled = i_bVal
      .Page7.oPag.oSCALE_7_18.enabled = i_bVal
      .Page7.oPag.oMINVALUE_7_19.enabled = i_bVal
      .Page7.oPag.oMAXVALUE_7_20.enabled = i_bVal
      .Page7.oPag.oSCALEDIVIDER_7_21.enabled = i_bVal
      .Page7.oPag.oCHKMINVALUE_7_22.enabled = i_bVal
      .Page7.oPag.oCHKMAXVALUE_7_23.enabled = i_bVal
      .Page7.oPag.oBARSPERSCALE_7_26.enabled = i_bVal
      .Page7.oPag.oMINNUMBERSCALELEGENDS_7_27.enabled = i_bVal
      .Page7.oPag.oSCALEAUTOFORMAT_7_28.enabled = i_bVal
      .Page6.oPag.oCOLORTYPE_6_1.enabled = i_bVal
      .Page6.oPag.oBRUSHTYPE_6_3.enabled = i_bVal
      .Page6.oPag.oALPHACHANNEL_6_8.enabled = i_bVal
      .Page6.oPag.oGRADIENTSHAPEDIRECTION_6_15.enabled = i_bVal
      .Page6.oPag.oGRADIENTLEVEL_6_16.enabled = i_bVal
      .Page6.oPag.oGRADIENTINVERTCOLORS_6_17.enabled = i_bVal
      .Page6.oPag.oGRADIENTPOSITION_6_19.enabled = i_bVal
      .Page6.oPag.oGRADIENTTYPE_6_21.enabled = i_bVal
      .Page6.oPag.oBACKGRADIENTMODE_6_33.enabled = i_bVal
      .Page6.oPag.oCHANGECOLOR_6_35.enabled = i_bVal
      .Page6.oPag.oBACKCOLORALPHA_6_38.enabled = i_bVal
      .Page4.oPag.oROTATIONCENTER_4_4.enabled = i_bVal
      .Page4.oPag.oROTATION_4_5.enabled = i_bVal
      .Page4.oPag.oFIELDCOLOR_4_17.enabled = i_bVal
      .Page4.oPag.oFIELDDETACHSLICE_4_18.enabled = i_bVal
      .Page4.oPag.oFIELDHIDESLICE_4_19.enabled = i_bVal
      .Page4.oPag.oFIELDAXIS2_4_20.enabled = i_bVal
      .Page4.oPag.oFIELDLEGEND_4_21.enabled = i_bVal
      .Page4.oPag.oLBACKCOLORALPHA_4_23.enabled = i_bVal
      .Page4.oPag.oALIGNMENT_4_24.enabled = i_bVal
      .Page4.oPag.oFONTUNDERLINE_4_29.enabled = i_bVal
      .Page4.oPag.oFONTITALIC_4_30.enabled = i_bVal
      .Page4.oPag.oFONTBOLD_4_31.enabled = i_bVal
      .Page4.oPag.oFONTNAME_4_36.enabled = i_bVal
      .Page4.oPag.oLFORECOLORALPHA_4_37.enabled = i_bVal
      .Page4.oPag.oFONTSIZE_4_38.enabled = i_bVal
      .Page4.oPag.oFORMAT_4_39.enabled = i_bVal
      .Page4.oPag.oCAPTION_4_40.enabled = i_bVal
      .Page4.oPag.oLEGENDS_4_41.enabled = i_bVal
      .Page2.oPag.oDONUTRATIO_2_3.enabled = i_bVal
      .Page2.oPag.oPIEDETACHPIXELS_2_5.enabled = i_bVal
      .Page2.oPag.oPIECOMPENSATEANGLES_2_7.enabled = i_bVal
      .Page2.oPag.oPIESHOWPERCENT_2_8.enabled = i_bVal
      .Page2.oPag.oPIEDETACHSLICEONCLICK_2_9.enabled = i_bVal
      .Page2.oPag.oPIEDETACHSLICEONLEGENDCLICK_2_10.enabled = i_bVal
      .Page2.oPag.oBARSSPACEBETWEEN_2_13.enabled = i_bVal
      .Page2.oPag.oBARTYPE_2_15.enabled = i_bVal
      .Page2.oPag.oBARLEGENDDIRECTION_2_18.enabled = i_bVal
      .Page2.oPag.oAREA3DTOP_2_21.enabled = i_bVal
      .Page2.oPag.oAREADRAWBORDERS_2_22.enabled = i_bVal
      .Page2.oPag.oPOINTSHAPEWIDTH_2_25.enabled = i_bVal
      .Page2.oPag.oUniqueshape_2_27.enabled = i_bVal
      .Page2.oPag.oLINECAPS_2_28.enabled = i_bVal
      .Page2.oPag.oLINECAPSSHAPE_2_29.enabled = i_bVal
      .Page2.oPag.oPieDetachAnimationSteps_2_30.enabled = i_bVal
      .Page1.oPag.oNUMSLICE_1_40.enabled = i_bVal
      .Page5.oPag.oShowAxis2Tics_5_2.enabled = i_bVal
      .Page5.oPag.oTicLength_5_3.enabled = i_bVal
      .Page5.oPag.oScaleBackLinesDash_5_5.enabled = i_bVal
      .Page5.oPag.oScaleBackLinesWidth_5_7.enabled = i_bVal
      .Page5.oPag.oScaleBackAlpha_5_25.enabled = i_bVal
      .Page5.oPag.oAxisAlpha_5_27.enabled = i_bVal
      .Page5.oPag.oShowLineZero_5_29.enabled = i_bVal
      .Page1.oPag.oMultiChart_1_43.enabled = i_bVal
      .Page1.oPag.oMultiChartMargin_1_44.enabled = i_bVal
      .Page2.oPag.oChartRow_2_34.enabled = i_bVal
      .Page6.oPag.oBtn_6_10.enabled = .Page6.oPag.oBtn_6_10.mCond()
      .Page6.oPag.oBtn_6_27.enabled = .Page6.oPag.oBtn_6_27.mCond()
      .Page6.oPag.oBtn_6_29.enabled = .Page6.oPag.oBtn_6_29.mCond()
      .Page4.oPag.oBtn_4_8.enabled = .Page4.oPag.oBtn_4_8.mCond()
      .Page4.oPag.oBtn_4_10.enabled = .Page4.oPag.oBtn_4_10.mCond()
      .Page5.oPag.oBtn_5_10.enabled = .Page5.oPag.oBtn_5_10.mCond()
      .Page5.oPag.oBtn_5_12.enabled = .Page5.oPag.oBtn_5_12.mCond()
      .Page5.oPag.oBtn_5_18.enabled = .Page5.oPag.oBtn_5_18.mCond()
      .Page5.oPag.oBtn_5_20.enabled = .Page5.oPag.oBtn_5_20.mCond()
      .Page1.oPag.BTNQRY.enabled = i_bVal
      .Page1.oPag.BTNMOD.enabled = i_bVal
      .Page1.oPag.BTNREP.enabled = i_bVal
      .Page1.oPag.oObj_1_35.enabled = i_bVal
      .Page1.oPag.oObj_1_36.enabled = i_bVal
      .Page1.oPag.oObj_1_37.enabled = i_bVal
      .Page1.oPag.oObj_1_38.enabled = i_bVal
      .Page1.oPag.oObj_1_39.enabled = i_bVal
      .Page8.oPag.GraphData.enabled = i_bVal
      .Page6.oPag.REFSHPMAIN.enabled = i_bVal
      .Page6.oPag.oBckColorChart1.enabled = i_bVal
      .Page6.oPag.oBckColorChart2.enabled = i_bVal
      .Page4.oPag.oBckColorChartL.enabled = i_bVal
      .Page4.oPag.oForeColorChartL.enabled = i_bVal
      .Page5.oPag.oScaleLineColor.enabled = i_bVal
      .Page5.oPag.oScaleBackColor.enabled = i_bVal
      .Page5.oPag.oAxisColor.enabled = i_bVal
      .Page5.oPag.oScaleLineZeroColor.enabled = i_bVal
    endwith
    this.cp_foxchartfields.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'cpfcprop',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.cp_foxchartfields.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.cpfcprop_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_code,"code",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.cpfcprop_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cpfcprop_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.cpfcprop_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into cpfcprop
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'cpfcprop')
        i_extval=cp_InsertValODBCExtFlds(this,'cpfcprop')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(code "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_code)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'cpfcprop')
        i_extval=cp_InsertValVFPExtFlds(this,'cpfcprop')
        cp_CheckDeletedKey(i_cTable,0,'code',this.w_code)
        INSERT INTO (i_cTable);
              (code  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_code;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.cpfcprop_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cpfcprop_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.cpfcprop_IDX,i_nConn)
      *
      * update cpfcprop
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'cpfcprop')
        i_nnn="UPDATE "+i_cTable+" SET"+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'cpfcprop')
        i_cWhere = cp_PKFox(i_cTable  ,'code',this.w_code  )
        UPDATE (i_cTable) SET;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- cp_foxchartfields : Saving
      this.cp_foxchartfields.ChangeRow(this.cRowID+'      1',0;
             ,this.w_SERIAL,"FDSERIAL";
             )
      this.cp_foxchartfields.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    * --- cp_foxchartfields : Deleting
    this.cp_foxchartfields.ChangeRow(this.cRowID+'      1',0;
           ,this.w_SERIAL,"FDSERIAL";
           )
    this.cp_foxchartfields.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.cpfcprop_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.cpfcprop_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.cpfcprop_IDX,i_nConn)
      *
      * delete cpfcprop
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'code',this.w_code  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.cpfcprop_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.cpfcprop_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,10,.t.)
        if .o_QUERY<>.w_QUERY
            .w_QUERYMODEL = SYS(2014, .w_QUERY)
        endif
        .DoRTCalc(12,12,.t.)
        if .o_FILECFG<>.w_FILECFG
            .w_FILEMODEL = SYS(2014, .w_FILECFG)
        endif
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .oPgFrm.Page1.oPag.BTNMOD.Calculate()
        if .o_QUERYMODEL<>.w_QUERYMODEL.or. .o_QUERY<>.w_QUERY
          .Calculate_CRPMUEKPBL()
        endif
        if .o_FILECFG<>.w_FILECFG.or. .o_FILEMODEL<>.w_FILEMODEL
          .Calculate_TGSVFQUMEV()
        endif
        .DoRTCalc(14,15,.t.)
        if .o_FILERPT<>.w_FILERPT
            .w_FILEREPORT = SYS(2014, .w_FILERPT)
        endif
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
        if .o_FILEREPORT<>.w_FILEREPORT
          .Calculate_GSMAXOICAW()
        endif
        Local l_Dep1,l_Dep2,l_Dep3,l_Dep4,l_Dep5,l_Dep6,l_Dep7,l_Dep8,l_Dep9,l_Dep10,l_Dep11,l_Dep12,l_Dep13,l_Dep14,l_Dep15
        l_Dep1= .o_CHARTTYPE<>.w_CHARTTYPE .or. .o_LEGENDPOSITION<>.w_LEGENDPOSITION .or. .o_SHOWVALUESONSHAPES<>.w_SHOWVALUESONSHAPES .or. .o_CHANGECOLORONMOUSE<>.w_CHANGECOLORONMOUSE .or. .o_SHOWTIPS<>.w_SHOWTIPS        l_Dep2= .o_SHADOW<>.w_SHADOW .or. .o_SHOWAXIS<>.w_SHOWAXIS .or. .o_DEPTH<>.w_DEPTH .or. .o_LINECAPS<>.w_LINECAPS .or. .o_POINTSHAPEWIDTH<>.w_POINTSHAPEWIDTH        l_Dep3= .o_LINECAPSSHAPE<>.w_LINECAPSSHAPE .or. .o_DONUTRATIO<>.w_DONUTRATIO .or. .o_PIEDETACHPIXELS<>.w_PIEDETACHPIXELS .or. .o_PIECOMPENSATEANGLES<>.w_PIECOMPENSATEANGLES .or. .o_PIESHOWPERCENT<>.w_PIESHOWPERCENT        l_Dep4= .o_PIEDETACHSLICEONCLICK<>.w_PIEDETACHSLICEONCLICK .or. .o_PIEDETACHSLICEONLEGENDCLICK<>.w_PIEDETACHSLICEONLEGENDCLICK .or. .o_PIEENHANCEDDRAWING<>.w_PIEENHANCEDDRAWING .or. .o_BARSSPACEBETWEEN<>.w_BARSSPACEBETWEEN .or. .o_BARTYPE<>.w_BARTYPE        l_Dep5= .o_BARLEGENDDIRECTION<>.w_BARLEGENDDIRECTION .or. .o_AREA3DTOP<>.w_AREA3DTOP .or. .o_AREADRAWBORDERS<>.w_AREADRAWBORDERS .or. .o_FIELDLEGEND<>.w_FIELDLEGEND .or. .o_FIELDAXIS2<>.w_FIELDAXIS2        l_Dep6= .o_FIELDHIDESLICE<>.w_FIELDHIDESLICE .or. .o_FIELDDETACHSLICE<>.w_FIELDDETACHSLICE .or. .o_FIELDCOLOR<>.w_FIELDCOLOR .or. .o_COLORTYPE<>.w_COLORTYPE .or. .o_BRUSHTYPE<>.w_BRUSHTYPE        l_Dep7= .o_ALPHACHANNEL<>.w_ALPHACHANNEL .or. .o_GRADIENTLEVEL<>.w_GRADIENTLEVEL .or. .o_GRADIENTSHAPEDIRECTION<>.w_GRADIENTSHAPEDIRECTION .or. .o_GRADIENTINVERTCOLORS<>.w_GRADIENTINVERTCOLORS .or. .o_GRADIENTPOSITION<>.w_GRADIENTPOSITION        l_Dep8= .o_GRADIENTTYPE<>.w_GRADIENTTYPE .or. .o_BACKGRADIENTMODE<>.w_BACKGRADIENTMODE .or. .o_MARGINRIGHT<>.w_MARGINRIGHT .or. .o_BACKCOLOR<>.w_BACKCOLOR .or. .o_BACKCOLOR2<>.w_BACKCOLOR2        l_Dep9= .o_MARGIN<>.w_MARGIN .or. .o_MARGINTOP<>.w_MARGINTOP .or. .o_MARGINBOTTOM<>.w_MARGINBOTTOM .or. .o_MARGINLEFT<>.w_MARGINLEFT .or. .o_MINNUMBERSCALELEGENDS<>.w_MINNUMBERSCALELEGENDS        l_Dep10= .o_SHOWSCALE<>.w_SHOWSCALE .or. .o_SCALE<>.w_SCALE .or. .o_CHKMINVALUE<>.w_CHKMINVALUE .or. .o_MINVALUE<>.w_MINVALUE .or. .o_CHKMAXVALUE<>.w_CHKMAXVALUE        l_Dep11= .o_MAXVALUE<>.w_MAXVALUE .or. .o_SCALEDIVIDER<>.w_SCALEDIVIDER .or. .o_BARSPERSCALE<>.w_BARSPERSCALE .or. .o_SCALEAUTOFORMAT<>.w_SCALEAUTOFORMAT .or. .o_BACKCOLORALPHA<>.w_BACKCOLORALPHA        l_Dep12= .o_SHPMAINCOLOR<>.w_SHPMAINCOLOR .or. .o_NUMSLICE<>.w_NUMSLICE .or. .o_TicLength<>.w_TicLength .or. .o_ScaleBackLinesDash<>.w_ScaleBackLinesDash .or. .o_ScaleBackLinesWidth<>.w_ScaleBackLinesWidth        l_Dep13= .o_ShowAxis2Tics<>.w_ShowAxis2Tics .or. .o_ScaleBackAlpha<>.w_ScaleBackAlpha .or. .o_AxisAlpha<>.w_AxisAlpha .or. .o_ShowLineZero<>.w_ShowLineZero .or. .o_ScaleLineColor<>.w_ScaleLineColor        l_Dep14= .o_ScaleBackColor<>.w_ScaleBackColor .or. .o_AxisColor<>.w_AxisColor .or. .o_ScaleLineZeroColor<>.w_ScaleLineZeroColor .or. .o_MultiChart<>.w_MultiChart .or. .o_MultiChartMargin<>.w_MultiChartMargin        l_Dep15= .o_PieDetachAnimationSteps<>.w_PieDetachAnimationSteps .or. .o_ChartRow<>.w_ChartRow
        if m.l_Dep1 .or. m.l_Dep2 .or. m.l_Dep3 .or. m.l_Dep4 .or. m.l_Dep5 .or. m.l_Dep6 .or. m.l_Dep7 .or. m.l_Dep8 .or. m.l_Dep9 .or. m.l_Dep10 .or. m.l_Dep11 .or. m.l_Dep12 .or. m.l_Dep13 .or. m.l_Dep14 .or. m.l_Dep15
          .Calculate_XYBDOPIVLM()
        endif
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page8.oPag.GraphData.Calculate()
        .oPgFrm.Page6.oPag.REFSHPMAIN.Calculate('      1234567890     ',.w_SHPMAINCOLOR,.w_SHPMAINCOLOR)
        if .o_BACKCOLOR<>.w_BACKCOLOR
        .oPgFrm.Page6.oPag.oBckColorChart1.Calculate('      1234567890     ',.w_BACKCOLOR,.w_BACKCOLOR)
        endif
        if .o_BACKCOLOR2<>.w_BACKCOLOR2
        .oPgFrm.Page6.oPag.oBckColorChart2.Calculate('      1234567890     ',.w_BACKCOLOR2, .w_BACKCOLOR2)
        endif
        if .o_LBACKCOLOR<>.w_LBACKCOLOR
        .oPgFrm.Page4.oPag.oBckColorChartL.Calculate('      1234567890     ',.w_LBACKCOLOR, .w_LBACKCOLOR)
        endif
        if .o_LFORECOLOR<>.w_LFORECOLOR
        .oPgFrm.Page4.oPag.oForeColorChartL.Calculate('      1234567890     ',.w_LFORECOLOR,.w_LFORECOLOR)
        endif
        .DoRTCalc(17,53,.t.)
        if .o_CHARTTYPE<>.w_CHARTTYPE
            .w_FIELDDETACHSLICE = IIF( Inlist(.w_CHARTTYPE, 1, 2 ), .w_FIELDDETACHSLICE , ' ' )
        endif
        if .o_CHARTTYPE<>.w_CHARTTYPE
            .w_FIELDHIDESLICE = IIF( Inlist(.w_CHARTTYPE, 1, 2 ), .w_FIELDHIDESLICE , ' ' )
        endif
        if .o_CHARTTYPE<>.w_CHARTTYPE
            .w_FIELDAXIS2 = IIF( Inlist(.w_CHARTTYPE, 1, 2), ' ' , .w_FIELDAXIS2 )
        endif
        if .o_CHARTTYPE<>.w_CHARTTYPE
            .w_FIELDLEGEND = IIF( Inlist(.w_CHARTTYPE, 1, 2, 7), .w_FIELDLEGEND , ' ' )
        endif
        Local l_Dep1,l_Dep2,l_Dep3
        l_Dep1= .o_CAPTION<>.w_CAPTION .or. .o_FORMAT<>.w_FORMAT .or. .o_FONTNAME<>.w_FONTNAME .or. .o_FONTSIZE<>.w_FONTSIZE .or. .o_FONTBOLD<>.w_FONTBOLD        l_Dep2= .o_FONTITALIC<>.w_FONTITALIC .or. .o_FONTUNDERLINE<>.w_FONTUNDERLINE .or. .o_ALIGNMENT<>.w_ALIGNMENT .or. .o_LFORECOLORALPHA<>.w_LFORECOLORALPHA .or. .o_LBACKCOLORALPHA<>.w_LBACKCOLORALPHA        l_Dep3= .o_LFORECOLOR<>.w_LFORECOLOR .or. .o_LBACKCOLOR<>.w_LBACKCOLOR .or. .o_ROTATION<>.w_ROTATION .or. .o_ROTATIONCENTER<>.w_ROTATIONCENTER
        if m.l_Dep1 .or. m.l_Dep2 .or. m.l_Dep3
          .Calculate_UQSHANLGMN()
        endif
        .DoRTCalc(58,82,.t.)
        if .o_Uniqueshape<>.w_Uniqueshape
            .w_LINECAPS = IIF(.w_Uniqueshape , .w_LINECAPS  , .F.)
        endif
        if .o_CHARTTYPE<>.w_CHARTTYPE.or. .o_DEPTH<>.w_DEPTH
            .w_LINECAPSSHAPE = IIF(.w_CHARTTYPE=5 And .w_DEPTH = 0, .w_LINECAPSSHAPE , 0)
        endif
        .DoRTCalc(85,85,.t.)
        if .o_CHARTTYPE<>.w_CHARTTYPE
            .w_NUMSLICE = IIF( Inlist(.w_CHARTTYPE , 1, 2) , .w_NUMSLICE, 0)
        endif
        if .o_ScaleLineColor<>.w_ScaleLineColor
        .oPgFrm.Page5.oPag.oScaleLineColor.Calculate('      1234567890     ', .w_ScaleLineColor , .w_ScaleLineColor)
        endif
        if .o_ScaleBackColor<>.w_ScaleBackColor
        .oPgFrm.Page5.oPag.oScaleBackColor.Calculate('      1234567890     ', .w_ScaleBackColor ,  .w_ScaleBackColor)
        endif
        if .o_AxisColor<>.w_AxisColor
        .oPgFrm.Page5.oPag.oAxisColor.Calculate('      1234567890     ', .w_AxisColor, .w_AxisColor)
        endif
        if .o_ScaleLineZeroColor<>.w_ScaleLineZeroColor
        .oPgFrm.Page5.oPag.oScaleLineZeroColor.Calculate('      1234567890     ',.w_ScaleLineZeroColor ,  .w_ScaleLineZeroColor)
        endif
        .DoRTCalc(87,99,.t.)
        if .o_MultiChart<>.w_MultiChart
            .w_MultiChartMargin = .F.
        endif
        if .o_CHARTTYPE<>.w_CHARTTYPE.or. .o_SHOWVALUESONSHAPES<>.w_SHOWVALUESONSHAPES
          .Calculate_MFVLNXNGPY()
        endif
        * --- Area Manuale = Calculate
        * --- cp_foxchartproperties
        *--- DrawChart
        if this.oParentObject.w_CHANGEONFLY
            cp_setfoxchartproperties(this, "DRAWFOXCHARTS")
        endif
        
        if this.w_MODIFIED
          if type("this.oParentObject.oParentObject")='O'
            this.oParentObject.oParentObject.bModified=.T.
          endif
        endif
        
        this.w_oBckColorChart1.Calculate('      1234567890     ', this.w_BACKCOLOR, this.w_BACKCOLOR)
        this.w_oBckColorChart2.Calculate('      1234567890     ', this.w_BACKCOLOR2, this.w_BACKCOLOR2)
        
        * --- Axis Page
        this.w_oScaleLineColor.Calculate('      1234567890     ', this.w_ScaleLineColor , this.w_ScaleLineColor)
        this.w_oScaleBackColor.Calculate('      1234567890     ', this.w_ScaleBackColor ,  this.w_ScaleBackColor)
        this.w_oAxisColor.Calculate('      1234567890     ', this.w_AxisColor, this.w_AxisColor)
        this.w_oScaleLineZeroColor.Calculate('      1234567890     ', this.w_ScaleLineZeroColor ,  this.w_ScaleLineZeroColor)
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(101,101,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .oPgFrm.Page1.oPag.BTNMOD.Calculate()
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page8.oPag.GraphData.Calculate()
        .oPgFrm.Page6.oPag.REFSHPMAIN.Calculate('      1234567890     ',.w_SHPMAINCOLOR,.w_SHPMAINCOLOR)
        .oPgFrm.Page6.oPag.oBckColorChart1.Calculate('      1234567890     ',.w_BACKCOLOR,.w_BACKCOLOR)
        .oPgFrm.Page6.oPag.oBckColorChart2.Calculate('      1234567890     ',.w_BACKCOLOR2, .w_BACKCOLOR2)
        .oPgFrm.Page4.oPag.oBckColorChartL.Calculate('      1234567890     ',.w_LBACKCOLOR, .w_LBACKCOLOR)
        .oPgFrm.Page4.oPag.oForeColorChartL.Calculate('      1234567890     ',.w_LFORECOLOR,.w_LFORECOLOR)
        .oPgFrm.Page5.oPag.oScaleLineColor.Calculate('      1234567890     ', .w_ScaleLineColor , .w_ScaleLineColor)
        .oPgFrm.Page5.oPag.oScaleBackColor.Calculate('      1234567890     ', .w_ScaleBackColor ,  .w_ScaleBackColor)
        .oPgFrm.Page5.oPag.oAxisColor.Calculate('      1234567890     ', .w_AxisColor, .w_AxisColor)
        .oPgFrm.Page5.oPag.oScaleLineZeroColor.Calculate('      1234567890     ',.w_ScaleLineZeroColor ,  .w_ScaleLineZeroColor)
    endwith
  return

  proc Calculate_IEFUUJDSMN()
    with this
          * --- Blank
          cp_setfoxchartproperties(this;
              ,"RESETCOMBOBOX";
             )
    endwith
  endproc
  proc Calculate_CRPMUEKPBL()
    with this
          * --- QUERYMODEL
          .w_oChart.QUERYMODEL = SYS(2014, .w_QUERYMODEL)
          .w_cSource = IIF(EMPTY(.w_cSource), .w_oChart.QUERYMODEL, .w_cSource)
          .w_oChart.cSource = .w_cSource
          .w_oChart.oChart.QUERYMODEL = SYS(2014, .w_QUERYMODEL)
          cp_setfoxchartproperties(this;
              ,"REDRAWFOXCHARTS";
             )
          cp_setfoxchartproperties(this;
              ,"SETCURSORDATA";
             )
    endwith
  endproc
  proc Calculate_TGSVFQUMEV()
    with this
          * --- FILEMODEL
          .w_oChart.FILEMODEL = .w_FILEMODEL
          .w_oChart.oChart.FILEMODEL = .w_FILEMODEL
          cp_setfoxchartproperties(this;
              ,"LOADMODELFROMFILE";
             )
    endwith
  endproc
  proc Calculate_GSMAXOICAW()
    with this
          * --- FILEREPORT
          .w_oChart.FILEREPORT = .w_FILEREPORT
          .w_oChart.oChart.FILEREPORT = .w_FILEREPORT
    endwith
  endproc
  proc Calculate_XYBDOPIVLM()
    with this
          * --- Assegno il valore alle proprietÓ del FoxCharts
          .w_oChart.cSource = .w_cSource
          .w_oChart.QUERYMODEL = .w_QUERYMODEL
          .w_oChart.oChart.MultiChart = .w_MultiChart
          .w_oChart.oChart.CHARTTYPE = .w_CHARTTYPE
          .w_oChart.oChart.LEGENDPOSITION = .w_LEGENDPOSITION
          .w_oChart.oChart.SHOWVALUESONSHAPES = .w_SHOWVALUESONSHAPES
          .w_oChart.oChart.CHANGECOLORONMOUSE = .w_CHANGECOLORONMOUSE
          .w_oChart.oChart.SHOWTIPS = .w_SHOWTIPS
          .w_oChart.oChart.SHADOW = .w_SHADOW
          .w_oChart.oChart.SHOWAXIS = .w_SHOWAXIS
          .w_oChart.oChart._3D = .w_DEPTH
          .w_oChart.oChart.DEPTH = .w_DEPTH
          .w_oChart.oChart.LINECAPS = .w_LINECAPS
          .w_oChart.oChart.POINTSHAPEWIDTH = .w_POINTSHAPEWIDTH
          .w_oChart.oChart.LINECAPSSHAPE = IIF( !.w_MultiChart , .w_LINECAPSSHAPE , .F.)
          .w_oChart.oChart.DONUTRATIO = .w_DONUTRATIO
          .w_oChart.oChart.PIEDETACHPIXELS = .w_PIEDETACHPIXELS
          .w_RET = ExecScript("IF .w_oChart.oChart.Fields.Count>0" + chr(13) + ".w_oChart.oChart.Fields(1).Color = .w_SHPMAINCOLOR" + chr(13) + "EndIf")
          .w_oChart.oChart.PIECOMPENSATEANGLES = .w_PIECOMPENSATEANGLES
          .w_oChart.oChart.PieDetachAnimationSteps = .w_PieDetachAnimationSteps
          .w_oChart.oChart.PIESHOWPERCENT = .w_PIESHOWPERCENT
          .w_oChart.oChart.PIEDETACHSLICEONCLICK = .w_PIEDETACHSLICEONCLICK
          .w_oChart.oChart.PIEDETACHSLICEONLEGENDCLICK = .w_PIEDETACHSLICEONLEGENDCLICK
          .w_oChart.oChart.PIEENHANCEDDRAWING = .w_PIEENHANCEDDRAWING
          .w_oChart.oChart.BARSSPACEBETWEEN = .w_BARSSPACEBETWEEN
          .w_oChart.oChart.BARTYPE = .w_BARTYPE
          .w_oChart.oChart.BARLEGENDDIRECTION = .w_BARLEGENDDIRECTION
          .w_oChart.oChart.AREA3DTOP = .w_AREA3DTOP
          .w_oChart.oChart.AREADRAWBORDERS = .w_AREADRAWBORDERS
          .w_oChart.oChart.FIELDLEGEND = .w_FIELDLEGEND
          .w_oChart.oChart.FIELDAXIS2 = .w_FIELDAXIS2
          .w_oChart.oChart.FIELDHIDESLICE = .w_FIELDHIDESLICE
          .w_oChart.oChart.FIELDDETACHSLICE = .w_FIELDDETACHSLICE
          .w_oChart.oChart.FIELDCOLOR = .w_FIELDCOLOR
          .w_oChart.oChart.COLORTYPE = .w_COLORTYPE - 1
          .w_oChart.oChart.BRUSHTYPE = .w_BRUSHTYPE
          .w_oChart.oChart.ALPHACHANNEL = .w_ALPHACHANNEL
          .w_oChart.oChart.GRADIENTLEVEL = .w_GRADIENTLEVEL
          .w_oChart.oChart.GRADIENTSHAPEDIRECTION = .w_GRADIENTSHAPEDIRECTION
          .w_oChart.oChart.GRADIENTINVERTCOLORS = .w_GRADIENTINVERTCOLORS
          .w_oChart.oChart.GRADIENTPOSITION = .w_GRADIENTPOSITION
          .w_oChart.oChart.GRADIENTTYPE = .w_GRADIENTTYPE
          .w_oChart.oChart.BACKGRADIENTMODE = .w_BACKGRADIENTMODE
          .w_oChart.oChart.MARGINRIGHT = .w_MARGINRIGHT
          .w_oChart.oChart.BACKCOLOR = .w_BACKCOLOR
          .w_oChart.oChart.BACKCOLOR2 = .w_BACKCOLOR2
          .w_oChart.oChart.MARGIN = .w_MARGIN
          .w_oChart.oChart.MARGINTOP = .w_MARGINTOP
          .w_oChart.oChart.MARGINBOTTOM = .w_MARGINBOTTOM
          .w_oChart.oChart.MARGINLEFT = .w_MARGINLEFT
          .w_oChart.oChart.MINNUMBERSCALELEGENDS = .w_MINNUMBERSCALELEGENDS
          .w_oChart.oChart.SHOWSCALE = .w_SHOWSCALE
          .w_oChart.oChart.SCALE = .w_SCALE
          .w_oChart.oChart.CHKMINVALUE = .w_CHKMINVALUE
          .w_oChart.oChart.CHKMAXVALUE = .w_CHKMAXVALUE
          .w_oChart.oChart.MINVALUE = IIF(!.w_CHKMINVALUE , .F. , .w_MINVALUE)
          .w_oChart.oChart.MAXVALUE = IIF(!.w_CHKMAXVALUE , .F. , .w_MAXVALUE)
          .w_oChart.oChart.SCALEDIVIDER = .w_SCALEDIVIDER
          .w_oChart.oChart.BARSPERSCALE = .w_BARSPERSCALE
          .w_oChart.oChart.SCALEAUTOFORMAT = .w_SCALEAUTOFORMAT
          .w_oChart.oChart.BACKCOLORALPHA = .w_BACKCOLORALPHA
          .w_oChart.oChart.NumSlice = .w_NUMSLICE
          .w_oChart.oChart.ChartRow = .w_ChartRow
          .w_oChart.oChart.TicLength = .w_TicLength
          .w_oChart.oChart.ScaleBackLinesDash = .w_ScaleBackLinesDash
          .w_oChart.oChart.ScaleBackLinesWidth = .w_ScaleBackLinesWidth
          .w_oChart.oChart.ShowAxis2Tics = .w_ShowAxis2Tics
          .w_oChart.oChart.ScaleBackAlpha = .w_ScaleBackAlpha
          .w_oChart.oChart.AxisAlpha = .w_AxisAlpha
          .w_oChart.oChart.ShowLineZero = .w_ShowLineZero
          .w_oChart.oChart.ScaleLineColor = .w_ScaleLineColor
          .w_oChart.oChart.ScaleBackColor = .w_ScaleBackColor
          .w_oChart.oChart.AxisColor = .w_AxisColor
          .w_oChart.oChart.ScaleLineZeroColor = .w_ScaleLineZeroColor
          .w_oChart.oChart.MultiChartMargin = .w_MultiChartMargin
          .w_oChart.oChart.Shape1 = .w_SHPMAINCOLOR
          .w_MODIFIED = .T.
    endwith
  endproc
  proc Calculate_FAXKGGQQEC()
    with this
          * --- Assegno il valore delle proprietÓ del FoxCharts alla variabili della maschera
          .w_cSource = .w_oChart.cSource
          .w_CHARTTYPE = .w_oChart.oChart.CHARTTYPE
          .w_LEGENDPOSITION = .w_oChart.oChart.LEGENDPOSITION
          .w_SHOWVALUESONSHAPES = .w_oChart.oChart.SHOWVALUESONSHAPES
          .w_CHANGECOLORONMOUSE = .w_oChart.oChart.CHANGECOLORONMOUSE
          .w_SHOWTIPS = .w_oChart.oChart.SHOWTIPS
          .w_SHADOW = .w_oChart.oChart.SHADOW
          .w_SHOWAXIS = .w_oChart.oChart.SHOWAXIS
          .w_oChart.oChart._3D = .w_oChart.oChart.DEPTH
          .w_DEPTH = .w_oChart.oChart.DEPTH
          .w_LINECAPS = .w_oChart.oChart.LINECAPS
          .w_POINTSHAPEWIDTH = .w_oChart.oChart.POINTSHAPEWIDTH
          .w_LINECAPSSHAPE = IIF( TYPE('this..w_oChart.oChart.LINECAPSSHAPE')='N' , .w_oChart.oChart.LINECAPSSHAPE , 0)
          .w_DONUTRATIO = .w_oChart.oChart.DONUTRATIO
          .w_PIEDETACHPIXELS = .w_oChart.oChart.PIEDETACHPIXELS
          .w_PIECOMPENSATEANGLES = .w_oChart.oChart.PIECOMPENSATEANGLES
          .w_PieDetachAnimationSteps = .w_oChart.oChart.PieDetachAnimationSteps
          .w_PIESHOWPERCENT = .w_oChart.oChart.PIESHOWPERCENT
          .w_PIEDETACHSLICEONCLICK = .w_oChart.oChart.PIEDETACHSLICEONCLICK
          .w_PIEDETACHSLICEONLEGENDCLICK = .w_oChart.oChart.PIEDETACHSLICEONLEGENDCLICK
          .w_PIEENHANCEDDRAWING = .w_oChart.oChart.PIEENHANCEDDRAWING
          .w_BARSSPACEBETWEEN = .w_oChart.oChart.BARSSPACEBETWEEN
          .w_BARTYPE = .w_oChart.oChart.BARTYPE
          .w_BARLEGENDDIRECTION = .w_oChart.oChart.BARLEGENDDIRECTION
          .w_AREA3DTOP = .w_oChart.oChart.AREA3DTOP
          .w_AREADRAWBORDERS = .w_oChart.oChart.AREADRAWBORDERS
          .w_FIELDLEGEND = .w_oChart.oChart.FIELDLEGEND
          .w_FIELDAXIS2 = .w_oChart.oChart.FIELDAXIS2
          .w_FIELDHIDESLICE = .w_oChart.oChart.FIELDHIDESLICE
          .w_FIELDDETACHSLICE = .w_oChart.oChart.FIELDDETACHSLICE
          .w_FIELDCOLOR = .w_oChart.oChart.FIELDCOLOR
          .w_COLORTYPE = .w_oChart.oChart.COLORTYPE + 1
          .w_BRUSHTYPE = .w_oChart.oChart.BRUSHTYPE
          .w_ALPHACHANNEL = .w_oChart.oChart.ALPHACHANNEL
          .w_GRADIENTLEVEL = .w_oChart.oChart.GRADIENTLEVEL
          .w_GRADIENTSHAPEDIRECTION = .w_oChart.oChart.GRADIENTSHAPEDIRECTION
          .w_GRADIENTINVERTCOLORS = .w_oChart.oChart.GRADIENTINVERTCOLORS
          .w_GRADIENTPOSITION = .w_oChart.oChart.GRADIENTPOSITION
          .w_GRADIENTTYPE = .w_oChart.oChart.GRADIENTTYPE
          .w_BACKGRADIENTMODE = .w_oChart.oChart.BACKGRADIENTMODE
          .w_MARGINRIGHT = .w_oChart.oChart.MARGINRIGHT
          .w_BACKCOLOR = .w_oChart.oChart.BACKCOLOR
          .w_BACKCOLOR2 = .w_oChart.oChart.BACKCOLOR2
          .w_MARGIN = .w_oChart.oChart.MARGIN
          .w_MARGINTOP = .w_oChart.oChart.MARGINTOP
          .w_MARGINBOTTOM = .w_oChart.oChart.MARGINBOTTOM
          .w_MARGINLEFT = .w_oChart.oChart.MARGINLEFT
          .w_MINNUMBERSCALELEGENDS = .w_oChart.oChart.MINNUMBERSCALELEGENDS
          .w_SHOWSCALE = .w_oChart.oChart.SHOWSCALE
          .w_SCALE = .w_oChart.oChart.SCALE
          .w_CHKMINVALUE = .w_oChart.oChart.CHKMINVALUE
          .w_CHKMAXVALUE = .w_oChart.oChart.CHKMAXVALUE
          .w_MINVALUE = IIF(!.w_CHKMINVALUE, 0, .w_oChart.oChart.MINVALUE )
          .w_MAXVALUE = IIF( !.w_CHKMAXVALUE, 0, .w_oChart.oChart.MAXVALUE)
          .w_SCALEDIVIDER = .w_oChart.oChart.SCALEDIVIDER
          .w_BARSPERSCALE = .w_oChart.oChart.BARSPERSCALE
          .w_SCALEAUTOFORMAT = .w_oChart.oChart.SCALEAUTOFORMAT
          .w_BACKCOLORALPHA = .w_oChart.oChart.BACKCOLORALPHA
          .w_QUERYMODEL = .w_oChart.oChart.QueryModel
          .w_FILEREPORT = .w_oChart.oChart.FileReport
          .w_NumSlice = .w_oChart.oChart.NumSlice
          .w_MultiChart = .w_oChart.oChart.MultiChart
          .w_ChartRow = .w_oChart.oChart.ChartRow
          .w_TicLength = .w_oChart.oChart.TicLength
          .w_ScaleBackLinesDash = .w_oChart.oChart.ScaleBackLinesDash
          .w_ScaleBackLinesWidth = .w_oChart.oChart.ScaleBackLinesWidth
          .w_ShowAxis2Tics = .w_oChart.oChart.ShowAxis2Tics
          .w_ScaleBackAlpha = .w_oChart.oChart.ScaleBackAlpha
          .w_AxisAlpha = .w_oChart.oChart.AxisAlpha
          .w_ShowLineZero = .w_oChart.oChart.ShowLineZero
          .w_ScaleLineColor = .w_oChart.oChart.ScaleLineColor
          .w_ScaleBackColor = .w_oChart.oChart.ScaleBackColor
          .w_AxisColor = .w_oChart.oChart.AxisColor
          .w_ScaleLineZeroColor = .w_oChart.oChart.ScaleLineZeroColor
          .w_MultiChartMargin = .w_oChart.oChart.MultiChartMargin
          .w_RET = ExecScript("IF .w_oChart.oChart.Fields.Count>0" + chr(13) + ".w_SHPMAINCOLOR = .w_oChart.oChart.Fields(1).Color" + chr(13) + "EndIf")
          cp_setfoxchartproperties(this;
              ,"GETLEGENDS";
             )
    endwith
  endproc
  proc Calculate_UQSHANLGMN()
    with this
          * --- SET LEGEND PROPERTIES
          cp_setfoxchartproperties(this;
              ,"SETLEGENDS";
             )
    endwith
  endproc
  proc Calculate_MFVLNXNGPY()
    with this
          * --- Show percentage as values
          .w_PIESHOWPERCENT = IIF(INLIST(.w_CHARTTYPE, 1, 2) and .w_SHOWVALUESONSHAPES, .w_PIESHOWPERCENT, .F.)
          .w_oChart.oChart.PIESHOWPERCENT = .w_PIESHOWPERCENT
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page5.oPag.oSHOWAXIS_5_1.enabled = this.oPgFrm.Page5.oPag.oSHOWAXIS_5_1.mCond()
    this.oPgFrm.Page7.oPag.oSCALE_7_18.enabled = this.oPgFrm.Page7.oPag.oSCALE_7_18.mCond()
    this.oPgFrm.Page7.oPag.oMINVALUE_7_19.enabled = this.oPgFrm.Page7.oPag.oMINVALUE_7_19.mCond()
    this.oPgFrm.Page7.oPag.oMAXVALUE_7_20.enabled = this.oPgFrm.Page7.oPag.oMAXVALUE_7_20.mCond()
    this.oPgFrm.Page7.oPag.oSCALEDIVIDER_7_21.enabled = this.oPgFrm.Page7.oPag.oSCALEDIVIDER_7_21.mCond()
    this.oPgFrm.Page7.oPag.oCHKMINVALUE_7_22.enabled = this.oPgFrm.Page7.oPag.oCHKMINVALUE_7_22.mCond()
    this.oPgFrm.Page7.oPag.oCHKMAXVALUE_7_23.enabled = this.oPgFrm.Page7.oPag.oCHKMAXVALUE_7_23.mCond()
    this.oPgFrm.Page7.oPag.oBARSPERSCALE_7_26.enabled = this.oPgFrm.Page7.oPag.oBARSPERSCALE_7_26.mCond()
    this.oPgFrm.Page7.oPag.oMINNUMBERSCALELEGENDS_7_27.enabled = this.oPgFrm.Page7.oPag.oMINNUMBERSCALELEGENDS_7_27.mCond()
    this.oPgFrm.Page7.oPag.oSCALEAUTOFORMAT_7_28.enabled = this.oPgFrm.Page7.oPag.oSCALEAUTOFORMAT_7_28.mCond()
    this.oPgFrm.Page6.oPag.oGRADIENTSHAPEDIRECTION_6_15.enabled = this.oPgFrm.Page6.oPag.oGRADIENTSHAPEDIRECTION_6_15.mCond()
    this.oPgFrm.Page4.oPag.oFIELDDETACHSLICE_4_18.enabled = this.oPgFrm.Page4.oPag.oFIELDDETACHSLICE_4_18.mCond()
    this.oPgFrm.Page4.oPag.oFIELDHIDESLICE_4_19.enabled = this.oPgFrm.Page4.oPag.oFIELDHIDESLICE_4_19.mCond()
    this.oPgFrm.Page4.oPag.oFIELDAXIS2_4_20.enabled = this.oPgFrm.Page4.oPag.oFIELDAXIS2_4_20.mCond()
    this.oPgFrm.Page4.oPag.oFIELDLEGEND_4_21.enabled = this.oPgFrm.Page4.oPag.oFIELDLEGEND_4_21.mCond()
    this.oPgFrm.Page2.oPag.oDONUTRATIO_2_3.enabled = this.oPgFrm.Page2.oPag.oDONUTRATIO_2_3.mCond()
    this.oPgFrm.Page2.oPag.oPIEDETACHPIXELS_2_5.enabled = this.oPgFrm.Page2.oPag.oPIEDETACHPIXELS_2_5.mCond()
    this.oPgFrm.Page2.oPag.oPIECOMPENSATEANGLES_2_7.enabled = this.oPgFrm.Page2.oPag.oPIECOMPENSATEANGLES_2_7.mCond()
    this.oPgFrm.Page2.oPag.oPIESHOWPERCENT_2_8.enabled = this.oPgFrm.Page2.oPag.oPIESHOWPERCENT_2_8.mCond()
    this.oPgFrm.Page2.oPag.oPIEDETACHSLICEONCLICK_2_9.enabled = this.oPgFrm.Page2.oPag.oPIEDETACHSLICEONCLICK_2_9.mCond()
    this.oPgFrm.Page2.oPag.oPIEDETACHSLICEONLEGENDCLICK_2_10.enabled = this.oPgFrm.Page2.oPag.oPIEDETACHSLICEONLEGENDCLICK_2_10.mCond()
    this.oPgFrm.Page2.oPag.oBARSSPACEBETWEEN_2_13.enabled = this.oPgFrm.Page2.oPag.oBARSSPACEBETWEEN_2_13.mCond()
    this.oPgFrm.Page2.oPag.oBARTYPE_2_15.enabled = this.oPgFrm.Page2.oPag.oBARTYPE_2_15.mCond()
    this.oPgFrm.Page2.oPag.oBARLEGENDDIRECTION_2_18.enabled = this.oPgFrm.Page2.oPag.oBARLEGENDDIRECTION_2_18.mCond()
    this.oPgFrm.Page2.oPag.oAREA3DTOP_2_21.enabled = this.oPgFrm.Page2.oPag.oAREA3DTOP_2_21.mCond()
    this.oPgFrm.Page2.oPag.oAREADRAWBORDERS_2_22.enabled = this.oPgFrm.Page2.oPag.oAREADRAWBORDERS_2_22.mCond()
    this.oPgFrm.Page2.oPag.oPOINTSHAPEWIDTH_2_25.enabled = this.oPgFrm.Page2.oPag.oPOINTSHAPEWIDTH_2_25.mCond()
    this.oPgFrm.Page2.oPag.oUniqueshape_2_27.enabled = this.oPgFrm.Page2.oPag.oUniqueshape_2_27.mCond()
    this.oPgFrm.Page2.oPag.oLINECAPS_2_28.enabled = this.oPgFrm.Page2.oPag.oLINECAPS_2_28.mCond()
    this.oPgFrm.Page2.oPag.oLINECAPSSHAPE_2_29.enabled = this.oPgFrm.Page2.oPag.oLINECAPSSHAPE_2_29.mCond()
    this.oPgFrm.Page1.oPag.oNUMSLICE_1_40.enabled = this.oPgFrm.Page1.oPag.oNUMSLICE_1_40.mCond()
    this.oPgFrm.Page1.oPag.oMultiChartMargin_1_44.enabled = this.oPgFrm.Page1.oPag.oMultiChartMargin_1_44.mCond()
    this.oPgFrm.Page2.oPag.oChartRow_2_34.enabled = this.oPgFrm.Page2.oPag.oChartRow_2_34.mCond()
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(5).enabled=not(INLIST(this.w_CHARTTYPE, 1, 2))
    this.oPgFrm.Pages(5).oPag.visible=this.oPgFrm.Pages(5).enabled
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- cp_foxchartproperties
            IF Upper(CEVENT)='INIT'
              if Upper(this.cp_foxchartfields.class)='STDDYNAMICCHILD'
                This.oPgFrm.Pages[2].opag.uienable(.T.)
                This.oPgFrm.ActivePage=1
              Endif
            Endif          
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.BTNQRY.Event(cEvent)
      .oPgFrm.Page1.oPag.BTNMOD.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_IEFUUJDSMN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("w_QUERYMODEL Changed")
          .Calculate_CRPMUEKPBL()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("w_FILEMODEL Changed")
          .Calculate_TGSVFQUMEV()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.BTNREP.Event(cEvent)
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("w_FILEREPORT Changed")
          .Calculate_GSMAXOICAW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("SetValueToChart")
          .Calculate_XYBDOPIVLM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("GetValueFromChart") or lower(cEvent)==lower("w_ochart config loaded")
          .Calculate_FAXKGGQQEC()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page8.oPag.GraphData.Event(cEvent)
      .oPgFrm.Page6.oPag.REFSHPMAIN.Event(cEvent)
      .oPgFrm.Page6.oPag.oBckColorChart1.Event(cEvent)
      .oPgFrm.Page6.oPag.oBckColorChart2.Event(cEvent)
      .oPgFrm.Page4.oPag.oBckColorChartL.Event(cEvent)
      .oPgFrm.Page4.oPag.oForeColorChartL.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_UQSHANLGMN()
          bRefresh=.t.
        endif
      .oPgFrm.Page5.oPag.oScaleLineColor.Event(cEvent)
      .oPgFrm.Page5.oPag.oScaleBackColor.Event(cEvent)
      .oPgFrm.Page5.oPag.oAxisColor.Event(cEvent)
      .oPgFrm.Page5.oPag.oScaleLineZeroColor.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCHARTTYPE_1_3.RadioValue()==this.w_CHARTTYPE)
      this.oPgFrm.Page1.oPag.oCHARTTYPE_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLEGENDPOSITION_1_5.RadioValue()==this.w_LEGENDPOSITION)
      this.oPgFrm.Page1.oPag.oLEGENDPOSITION_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSHOWVALUESONSHAPES_1_7.RadioValue()==this.w_SHOWVALUESONSHAPES)
      this.oPgFrm.Page1.oPag.oSHOWVALUESONSHAPES_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHANGECOLORONMOUSE_1_8.RadioValue()==this.w_CHANGECOLORONMOUSE)
      this.oPgFrm.Page1.oPag.oCHANGECOLORONMOUSE_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSHOWTIPS_1_9.RadioValue()==this.w_SHOWTIPS)
      this.oPgFrm.Page1.oPag.oSHOWTIPS_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDEPTH_1_10.value==this.w_DEPTH)
      this.oPgFrm.Page1.oPag.oDEPTH_1_10.value=this.w_DEPTH
    endif
    if not(this.oPgFrm.Page1.oPag.oSHADOW_1_12.RadioValue()==this.w_SHADOW)
      this.oPgFrm.Page1.oPag.oSHADOW_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oQUERYMODEL_1_14.value==this.w_QUERYMODEL)
      this.oPgFrm.Page1.oPag.oQUERYMODEL_1_14.value=this.w_QUERYMODEL
    endif
    if not(this.oPgFrm.Page5.oPag.oSHOWAXIS_5_1.RadioValue()==this.w_SHOWAXIS)
      this.oPgFrm.Page5.oPag.oSHOWAXIS_5_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILEMODEL_1_18.value==this.w_FILEMODEL)
      this.oPgFrm.Page1.oPag.oFILEMODEL_1_18.value=this.w_FILEMODEL
    endif
    if not(this.oPgFrm.Page1.oPag.oFILEREPORT_1_27.value==this.w_FILEREPORT)
      this.oPgFrm.Page1.oPag.oFILEREPORT_1_27.value=this.w_FILEREPORT
    endif
    if not(this.oPgFrm.Page7.oPag.oMARGIN_7_4.value==this.w_MARGIN)
      this.oPgFrm.Page7.oPag.oMARGIN_7_4.value=this.w_MARGIN
    endif
    if not(this.oPgFrm.Page7.oPag.oMARGINTOP_7_6.value==this.w_MARGINTOP)
      this.oPgFrm.Page7.oPag.oMARGINTOP_7_6.value=this.w_MARGINTOP
    endif
    if not(this.oPgFrm.Page7.oPag.oMARGINBOTTOM_7_8.value==this.w_MARGINBOTTOM)
      this.oPgFrm.Page7.oPag.oMARGINBOTTOM_7_8.value=this.w_MARGINBOTTOM
    endif
    if not(this.oPgFrm.Page7.oPag.oMARGINLEFT_7_10.value==this.w_MARGINLEFT)
      this.oPgFrm.Page7.oPag.oMARGINLEFT_7_10.value=this.w_MARGINLEFT
    endif
    if not(this.oPgFrm.Page7.oPag.oMARGINRIGHT_7_12.value==this.w_MARGINRIGHT)
      this.oPgFrm.Page7.oPag.oMARGINRIGHT_7_12.value=this.w_MARGINRIGHT
    endif
    if not(this.oPgFrm.Page7.oPag.oSHOWSCALE_7_13.RadioValue()==this.w_SHOWSCALE)
      this.oPgFrm.Page7.oPag.oSHOWSCALE_7_13.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oSCALE_7_18.value==this.w_SCALE)
      this.oPgFrm.Page7.oPag.oSCALE_7_18.value=this.w_SCALE
    endif
    if not(this.oPgFrm.Page7.oPag.oMINVALUE_7_19.value==this.w_MINVALUE)
      this.oPgFrm.Page7.oPag.oMINVALUE_7_19.value=this.w_MINVALUE
    endif
    if not(this.oPgFrm.Page7.oPag.oMAXVALUE_7_20.value==this.w_MAXVALUE)
      this.oPgFrm.Page7.oPag.oMAXVALUE_7_20.value=this.w_MAXVALUE
    endif
    if not(this.oPgFrm.Page7.oPag.oSCALEDIVIDER_7_21.value==this.w_SCALEDIVIDER)
      this.oPgFrm.Page7.oPag.oSCALEDIVIDER_7_21.value=this.w_SCALEDIVIDER
    endif
    if not(this.oPgFrm.Page7.oPag.oCHKMINVALUE_7_22.RadioValue()==this.w_CHKMINVALUE)
      this.oPgFrm.Page7.oPag.oCHKMINVALUE_7_22.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oCHKMAXVALUE_7_23.RadioValue()==this.w_CHKMAXVALUE)
      this.oPgFrm.Page7.oPag.oCHKMAXVALUE_7_23.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oBARSPERSCALE_7_26.value==this.w_BARSPERSCALE)
      this.oPgFrm.Page7.oPag.oBARSPERSCALE_7_26.value=this.w_BARSPERSCALE
    endif
    if not(this.oPgFrm.Page7.oPag.oMINNUMBERSCALELEGENDS_7_27.value==this.w_MINNUMBERSCALELEGENDS)
      this.oPgFrm.Page7.oPag.oMINNUMBERSCALELEGENDS_7_27.value=this.w_MINNUMBERSCALELEGENDS
    endif
    if not(this.oPgFrm.Page7.oPag.oSCALEAUTOFORMAT_7_28.RadioValue()==this.w_SCALEAUTOFORMAT)
      this.oPgFrm.Page7.oPag.oSCALEAUTOFORMAT_7_28.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCOLORTYPE_6_1.RadioValue()==this.w_COLORTYPE)
      this.oPgFrm.Page6.oPag.oCOLORTYPE_6_1.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oBRUSHTYPE_6_3.RadioValue()==this.w_BRUSHTYPE)
      this.oPgFrm.Page6.oPag.oBRUSHTYPE_6_3.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oALPHACHANNEL_6_8.value==this.w_ALPHACHANNEL)
      this.oPgFrm.Page6.oPag.oALPHACHANNEL_6_8.value=this.w_ALPHACHANNEL
    endif
    if not(this.oPgFrm.Page6.oPag.oGRADIENTSHAPEDIRECTION_6_15.RadioValue()==this.w_GRADIENTSHAPEDIRECTION)
      this.oPgFrm.Page6.oPag.oGRADIENTSHAPEDIRECTION_6_15.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oGRADIENTLEVEL_6_16.value==this.w_GRADIENTLEVEL)
      this.oPgFrm.Page6.oPag.oGRADIENTLEVEL_6_16.value=this.w_GRADIENTLEVEL
    endif
    if not(this.oPgFrm.Page6.oPag.oGRADIENTINVERTCOLORS_6_17.RadioValue()==this.w_GRADIENTINVERTCOLORS)
      this.oPgFrm.Page6.oPag.oGRADIENTINVERTCOLORS_6_17.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oGRADIENTPOSITION_6_19.value==this.w_GRADIENTPOSITION)
      this.oPgFrm.Page6.oPag.oGRADIENTPOSITION_6_19.value=this.w_GRADIENTPOSITION
    endif
    if not(this.oPgFrm.Page6.oPag.oGRADIENTTYPE_6_21.RadioValue()==this.w_GRADIENTTYPE)
      this.oPgFrm.Page6.oPag.oGRADIENTTYPE_6_21.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oBACKGRADIENTMODE_6_33.RadioValue()==this.w_BACKGRADIENTMODE)
      this.oPgFrm.Page6.oPag.oBACKGRADIENTMODE_6_33.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oCHANGECOLOR_6_35.value==this.w_CHANGECOLOR)
      this.oPgFrm.Page6.oPag.oCHANGECOLOR_6_35.value=this.w_CHANGECOLOR
    endif
    if not(this.oPgFrm.Page6.oPag.oBACKCOLORALPHA_6_38.value==this.w_BACKCOLORALPHA)
      this.oPgFrm.Page6.oPag.oBACKCOLORALPHA_6_38.value=this.w_BACKCOLORALPHA
    endif
    if not(this.oPgFrm.Page4.oPag.oROTATIONCENTER_4_4.value==this.w_ROTATIONCENTER)
      this.oPgFrm.Page4.oPag.oROTATIONCENTER_4_4.value=this.w_ROTATIONCENTER
    endif
    if not(this.oPgFrm.Page4.oPag.oROTATION_4_5.value==this.w_ROTATION)
      this.oPgFrm.Page4.oPag.oROTATION_4_5.value=this.w_ROTATION
    endif
    if not(this.oPgFrm.Page4.oPag.oFIELDCOLOR_4_17.RadioValue()==this.w_FIELDCOLOR)
      this.oPgFrm.Page4.oPag.oFIELDCOLOR_4_17.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFIELDDETACHSLICE_4_18.RadioValue()==this.w_FIELDDETACHSLICE)
      this.oPgFrm.Page4.oPag.oFIELDDETACHSLICE_4_18.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFIELDHIDESLICE_4_19.RadioValue()==this.w_FIELDHIDESLICE)
      this.oPgFrm.Page4.oPag.oFIELDHIDESLICE_4_19.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFIELDAXIS2_4_20.RadioValue()==this.w_FIELDAXIS2)
      this.oPgFrm.Page4.oPag.oFIELDAXIS2_4_20.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFIELDLEGEND_4_21.RadioValue()==this.w_FIELDLEGEND)
      this.oPgFrm.Page4.oPag.oFIELDLEGEND_4_21.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oLBACKCOLORALPHA_4_23.value==this.w_LBACKCOLORALPHA)
      this.oPgFrm.Page4.oPag.oLBACKCOLORALPHA_4_23.value=this.w_LBACKCOLORALPHA
    endif
    if not(this.oPgFrm.Page4.oPag.oALIGNMENT_4_24.RadioValue()==this.w_ALIGNMENT)
      this.oPgFrm.Page4.oPag.oALIGNMENT_4_24.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFONTUNDERLINE_4_29.RadioValue()==this.w_FONTUNDERLINE)
      this.oPgFrm.Page4.oPag.oFONTUNDERLINE_4_29.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFONTITALIC_4_30.RadioValue()==this.w_FONTITALIC)
      this.oPgFrm.Page4.oPag.oFONTITALIC_4_30.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFONTBOLD_4_31.RadioValue()==this.w_FONTBOLD)
      this.oPgFrm.Page4.oPag.oFONTBOLD_4_31.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oFONTNAME_4_36.RadioValue()==this.w_FONTNAME)
      this.oPgFrm.Page4.oPag.oFONTNAME_4_36.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oLFORECOLORALPHA_4_37.value==this.w_LFORECOLORALPHA)
      this.oPgFrm.Page4.oPag.oLFORECOLORALPHA_4_37.value=this.w_LFORECOLORALPHA
    endif
    if not(this.oPgFrm.Page4.oPag.oFONTSIZE_4_38.value==this.w_FONTSIZE)
      this.oPgFrm.Page4.oPag.oFONTSIZE_4_38.value=this.w_FONTSIZE
    endif
    if not(this.oPgFrm.Page4.oPag.oFORMAT_4_39.value==this.w_FORMAT)
      this.oPgFrm.Page4.oPag.oFORMAT_4_39.value=this.w_FORMAT
    endif
    if not(this.oPgFrm.Page4.oPag.oCAPTION_4_40.value==this.w_CAPTION)
      this.oPgFrm.Page4.oPag.oCAPTION_4_40.value=this.w_CAPTION
    endif
    if not(this.oPgFrm.Page4.oPag.oLEGENDS_4_41.RadioValue()==this.w_LEGENDS)
      this.oPgFrm.Page4.oPag.oLEGENDS_4_41.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDONUTRATIO_2_3.value==this.w_DONUTRATIO)
      this.oPgFrm.Page2.oPag.oDONUTRATIO_2_3.value=this.w_DONUTRATIO
    endif
    if not(this.oPgFrm.Page2.oPag.oPIEDETACHPIXELS_2_5.value==this.w_PIEDETACHPIXELS)
      this.oPgFrm.Page2.oPag.oPIEDETACHPIXELS_2_5.value=this.w_PIEDETACHPIXELS
    endif
    if not(this.oPgFrm.Page2.oPag.oPIECOMPENSATEANGLES_2_7.RadioValue()==this.w_PIECOMPENSATEANGLES)
      this.oPgFrm.Page2.oPag.oPIECOMPENSATEANGLES_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPIESHOWPERCENT_2_8.RadioValue()==this.w_PIESHOWPERCENT)
      this.oPgFrm.Page2.oPag.oPIESHOWPERCENT_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPIEDETACHSLICEONCLICK_2_9.RadioValue()==this.w_PIEDETACHSLICEONCLICK)
      this.oPgFrm.Page2.oPag.oPIEDETACHSLICEONCLICK_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPIEDETACHSLICEONLEGENDCLICK_2_10.RadioValue()==this.w_PIEDETACHSLICEONLEGENDCLICK)
      this.oPgFrm.Page2.oPag.oPIEDETACHSLICEONLEGENDCLICK_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oBARSSPACEBETWEEN_2_13.value==this.w_BARSSPACEBETWEEN)
      this.oPgFrm.Page2.oPag.oBARSSPACEBETWEEN_2_13.value=this.w_BARSSPACEBETWEEN
    endif
    if not(this.oPgFrm.Page2.oPag.oBARTYPE_2_15.RadioValue()==this.w_BARTYPE)
      this.oPgFrm.Page2.oPag.oBARTYPE_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oBARLEGENDDIRECTION_2_18.RadioValue()==this.w_BARLEGENDDIRECTION)
      this.oPgFrm.Page2.oPag.oBARLEGENDDIRECTION_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAREA3DTOP_2_21.RadioValue()==this.w_AREA3DTOP)
      this.oPgFrm.Page2.oPag.oAREA3DTOP_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAREADRAWBORDERS_2_22.RadioValue()==this.w_AREADRAWBORDERS)
      this.oPgFrm.Page2.oPag.oAREADRAWBORDERS_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPOINTSHAPEWIDTH_2_25.value==this.w_POINTSHAPEWIDTH)
      this.oPgFrm.Page2.oPag.oPOINTSHAPEWIDTH_2_25.value=this.w_POINTSHAPEWIDTH
    endif
    if not(this.oPgFrm.Page2.oPag.oUniqueshape_2_27.RadioValue()==this.w_Uniqueshape)
      this.oPgFrm.Page2.oPag.oUniqueshape_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oLINECAPS_2_28.RadioValue()==this.w_LINECAPS)
      this.oPgFrm.Page2.oPag.oLINECAPS_2_28.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oLINECAPSSHAPE_2_29.RadioValue()==this.w_LINECAPSSHAPE)
      this.oPgFrm.Page2.oPag.oLINECAPSSHAPE_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPieDetachAnimationSteps_2_30.value==this.w_PieDetachAnimationSteps)
      this.oPgFrm.Page2.oPag.oPieDetachAnimationSteps_2_30.value=this.w_PieDetachAnimationSteps
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMSLICE_1_40.value==this.w_NUMSLICE)
      this.oPgFrm.Page1.oPag.oNUMSLICE_1_40.value=this.w_NUMSLICE
    endif
    if not(this.oPgFrm.Page5.oPag.oShowAxis2Tics_5_2.RadioValue()==this.w_ShowAxis2Tics)
      this.oPgFrm.Page5.oPag.oShowAxis2Tics_5_2.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oTicLength_5_3.value==this.w_TicLength)
      this.oPgFrm.Page5.oPag.oTicLength_5_3.value=this.w_TicLength
    endif
    if not(this.oPgFrm.Page5.oPag.oScaleBackLinesDash_5_5.value==this.w_ScaleBackLinesDash)
      this.oPgFrm.Page5.oPag.oScaleBackLinesDash_5_5.value=this.w_ScaleBackLinesDash
    endif
    if not(this.oPgFrm.Page5.oPag.oScaleBackLinesWidth_5_7.value==this.w_ScaleBackLinesWidth)
      this.oPgFrm.Page5.oPag.oScaleBackLinesWidth_5_7.value=this.w_ScaleBackLinesWidth
    endif
    if not(this.oPgFrm.Page5.oPag.oScaleBackAlpha_5_25.value==this.w_ScaleBackAlpha)
      this.oPgFrm.Page5.oPag.oScaleBackAlpha_5_25.value=this.w_ScaleBackAlpha
    endif
    if not(this.oPgFrm.Page5.oPag.oAxisAlpha_5_27.value==this.w_AxisAlpha)
      this.oPgFrm.Page5.oPag.oAxisAlpha_5_27.value=this.w_AxisAlpha
    endif
    if not(this.oPgFrm.Page5.oPag.oShowLineZero_5_29.RadioValue()==this.w_ShowLineZero)
      this.oPgFrm.Page5.oPag.oShowLineZero_5_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMultiChart_1_43.RadioValue()==this.w_MultiChart)
      this.oPgFrm.Page1.oPag.oMultiChart_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMultiChartMargin_1_44.RadioValue()==this.w_MultiChartMargin)
      this.oPgFrm.Page1.oPag.oMultiChartMargin_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oChartRow_2_34.value==this.w_ChartRow)
      this.oPgFrm.Page2.oPag.oChartRow_2_34.value=this.w_ChartRow
    endif
    cp_SetControlsValueExtFlds(this,'cpfcprop')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DEPTH>=0 AND .w_DEPTH<=50)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDEPTH_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MARGIN >= 0 and .w_MARGIN<= 50)
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oMARGIN_7_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MARGINTOP >= 0 and .w_MARGINTOP<= 50)
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oMARGINTOP_7_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MARGINBOTTOM >= 0 and .w_MARGINBOTTOM<= 50)
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oMARGINBOTTOM_7_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MARGINLEFT >= 0 and .w_MARGINLEFT<= 50)
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oMARGINLEFT_7_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MARGINRIGHT >= 0 and .w_MARGINRIGHT<= 50)
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oMARGINRIGHT_7_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_BARSPERSCALE >= 0 and .w_BARSPERSCALE<= 50)  and (.w_SHOWSCALE)
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oBARSPERSCALE_7_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MINNUMBERSCALELEGENDS >= 0 and .w_MINNUMBERSCALELEGENDS <= 50)  and (.w_SHOWSCALE)
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oMINNUMBERSCALELEGENDS_7_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRADIENTLEVEL >= -10 and .w_GRADIENTLEVEL<= 10)
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oGRADIENTLEVEL_6_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_GRADIENTPOSITION >= 0 and .w_GRADIENTPOSITION <= 1)
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oGRADIENTPOSITION_6_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CHANGECOLOR >= -10 and .w_CHANGECOLOR<= 10)
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oCHANGECOLOR_6_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_BACKCOLORALPHA >= 0 and .w_BACKCOLORALPHA <=255)
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oBACKCOLORALPHA_6_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_LBACKCOLORALPHA >= 0 and .w_LBACKCOLORALPHA <=255)
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oLBACKCOLORALPHA_4_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_LFORECOLORALPHA >= 0 and .w_LFORECOLORALPHA <=255)
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oLFORECOLORALPHA_4_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DONUTRATIO>=0.01 And .w_DONUTRATIO<= 1)  and (INLIST(.w_CHARTTYPE, 1, 2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDONUTRATIO_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PIEDETACHPIXELS>=0 And .w_PIEDETACHPIXELS<=50)  and (INLIST(.w_CHARTTYPE, 1, 2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPIEDETACHPIXELS_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_BARSSPACEBETWEEN>=0 And .w_BARSSPACEBETWEEN<=50)  and (INLIST(.w_CHARTTYPE, 3,7,8,9,11,12,13,14,15,17))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oBARSSPACEBETWEEN_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_POINTSHAPEWIDTH>=1 And .w_POINTSHAPEWIDTH<=10)  and (INLIST(.w_CHARTTYPE, 4, 5))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPOINTSHAPEWIDTH_2_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PieDetachAnimationSteps >=1 And .w_PieDetachAnimationSteps <=50)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPieDetachAnimationSteps_2_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TicLength >=1 And .w_TicLength <= 40)
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oTicLength_5_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ScaleBackLinesDash>=0 And .w_ScaleBackLinesDash<=4)
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oScaleBackLinesDash_5_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ScaleBackAlpha >=0 And .w_ScaleBackAlpha <= 255)
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oScaleBackAlpha_5_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_AxisAlpha >=0 And .w_AxisAlpha <=255)
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oAxisAlpha_5_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DEPTH>=0 AND .w_DEPTH<=50)  and (.w_MultiChart)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oChartRow_2_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .cp_foxchartfields.CheckForm()
      if i_bres
        i_bres=  .cp_foxchartfields.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CHARTTYPE = this.w_CHARTTYPE
    this.o_LEGENDPOSITION = this.w_LEGENDPOSITION
    this.o_SHOWVALUESONSHAPES = this.w_SHOWVALUESONSHAPES
    this.o_CHANGECOLORONMOUSE = this.w_CHANGECOLORONMOUSE
    this.o_SHOWTIPS = this.w_SHOWTIPS
    this.o_DEPTH = this.w_DEPTH
    this.o_SHADOW = this.w_SHADOW
    this.o_PIEENHANCEDDRAWING = this.w_PIEENHANCEDDRAWING
    this.o_QUERYMODEL = this.w_QUERYMODEL
    this.o_SHOWAXIS = this.w_SHOWAXIS
    this.o_FILEMODEL = this.w_FILEMODEL
    this.o_QUERY = this.w_QUERY
    this.o_FILECFG = this.w_FILECFG
    this.o_FILEREPORT = this.w_FILEREPORT
    this.o_FILERPT = this.w_FILERPT
    this.o_MARGIN = this.w_MARGIN
    this.o_MARGINTOP = this.w_MARGINTOP
    this.o_MARGINBOTTOM = this.w_MARGINBOTTOM
    this.o_MARGINLEFT = this.w_MARGINLEFT
    this.o_MARGINRIGHT = this.w_MARGINRIGHT
    this.o_SHOWSCALE = this.w_SHOWSCALE
    this.o_SCALE = this.w_SCALE
    this.o_MINVALUE = this.w_MINVALUE
    this.o_MAXVALUE = this.w_MAXVALUE
    this.o_SCALEDIVIDER = this.w_SCALEDIVIDER
    this.o_CHKMINVALUE = this.w_CHKMINVALUE
    this.o_CHKMAXVALUE = this.w_CHKMAXVALUE
    this.o_BARSPERSCALE = this.w_BARSPERSCALE
    this.o_MINNUMBERSCALELEGENDS = this.w_MINNUMBERSCALELEGENDS
    this.o_SCALEAUTOFORMAT = this.w_SCALEAUTOFORMAT
    this.o_COLORTYPE = this.w_COLORTYPE
    this.o_BRUSHTYPE = this.w_BRUSHTYPE
    this.o_ALPHACHANNEL = this.w_ALPHACHANNEL
    this.o_GRADIENTSHAPEDIRECTION = this.w_GRADIENTSHAPEDIRECTION
    this.o_GRADIENTLEVEL = this.w_GRADIENTLEVEL
    this.o_GRADIENTINVERTCOLORS = this.w_GRADIENTINVERTCOLORS
    this.o_GRADIENTPOSITION = this.w_GRADIENTPOSITION
    this.o_GRADIENTTYPE = this.w_GRADIENTTYPE
    this.o_BACKCOLOR = this.w_BACKCOLOR
    this.o_BACKCOLOR2 = this.w_BACKCOLOR2
    this.o_BACKGRADIENTMODE = this.w_BACKGRADIENTMODE
    this.o_BACKCOLORALPHA = this.w_BACKCOLORALPHA
    this.o_SHPMAINCOLOR = this.w_SHPMAINCOLOR
    this.o_ROTATIONCENTER = this.w_ROTATIONCENTER
    this.o_ROTATION = this.w_ROTATION
    this.o_LBACKCOLOR = this.w_LBACKCOLOR
    this.o_LFORECOLOR = this.w_LFORECOLOR
    this.o_FIELDCOLOR = this.w_FIELDCOLOR
    this.o_FIELDDETACHSLICE = this.w_FIELDDETACHSLICE
    this.o_FIELDHIDESLICE = this.w_FIELDHIDESLICE
    this.o_FIELDAXIS2 = this.w_FIELDAXIS2
    this.o_FIELDLEGEND = this.w_FIELDLEGEND
    this.o_LBACKCOLORALPHA = this.w_LBACKCOLORALPHA
    this.o_ALIGNMENT = this.w_ALIGNMENT
    this.o_FONTUNDERLINE = this.w_FONTUNDERLINE
    this.o_FONTITALIC = this.w_FONTITALIC
    this.o_FONTBOLD = this.w_FONTBOLD
    this.o_FONTNAME = this.w_FONTNAME
    this.o_LFORECOLORALPHA = this.w_LFORECOLORALPHA
    this.o_FONTSIZE = this.w_FONTSIZE
    this.o_FORMAT = this.w_FORMAT
    this.o_CAPTION = this.w_CAPTION
    this.o_DONUTRATIO = this.w_DONUTRATIO
    this.o_PIEDETACHPIXELS = this.w_PIEDETACHPIXELS
    this.o_PIECOMPENSATEANGLES = this.w_PIECOMPENSATEANGLES
    this.o_PIESHOWPERCENT = this.w_PIESHOWPERCENT
    this.o_PIEDETACHSLICEONCLICK = this.w_PIEDETACHSLICEONCLICK
    this.o_PIEDETACHSLICEONLEGENDCLICK = this.w_PIEDETACHSLICEONLEGENDCLICK
    this.o_BARSSPACEBETWEEN = this.w_BARSSPACEBETWEEN
    this.o_BARTYPE = this.w_BARTYPE
    this.o_BARLEGENDDIRECTION = this.w_BARLEGENDDIRECTION
    this.o_AREA3DTOP = this.w_AREA3DTOP
    this.o_AREADRAWBORDERS = this.w_AREADRAWBORDERS
    this.o_POINTSHAPEWIDTH = this.w_POINTSHAPEWIDTH
    this.o_Uniqueshape = this.w_Uniqueshape
    this.o_LINECAPS = this.w_LINECAPS
    this.o_LINECAPSSHAPE = this.w_LINECAPSSHAPE
    this.o_PieDetachAnimationSteps = this.w_PieDetachAnimationSteps
    this.o_NUMSLICE = this.w_NUMSLICE
    this.o_ShowAxis2Tics = this.w_ShowAxis2Tics
    this.o_TicLength = this.w_TicLength
    this.o_ScaleBackLinesDash = this.w_ScaleBackLinesDash
    this.o_ScaleBackLinesWidth = this.w_ScaleBackLinesWidth
    this.o_ScaleBackAlpha = this.w_ScaleBackAlpha
    this.o_AxisAlpha = this.w_AxisAlpha
    this.o_ShowLineZero = this.w_ShowLineZero
    this.o_ScaleLineColor = this.w_ScaleLineColor
    this.o_ScaleBackColor = this.w_ScaleBackColor
    this.o_AxisColor = this.w_AxisColor
    this.o_ScaleLineZeroColor = this.w_ScaleLineZeroColor
    this.o_MultiChart = this.w_MultiChart
    this.o_MultiChartMargin = this.w_MultiChartMargin
    this.o_ChartRow = this.w_ChartRow
    * --- cp_foxchartfields : Depends On
    this.cp_foxchartfields.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tcp_foxchartpropertiesPag1 as StdContainer
  Width  = 627
  height = 225
  stdWidth  = 627
  stdheight = 225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCHARTTYPE_1_3 as StdCombo with uid="SCDJLCZFJE",rtseq=3,rtrep=.f.,left=96,top=87,width=243,height=21;
    , ToolTipText = "Determines the type of chart to be drawn";
    , HelpContextID = 165176329;
    , cFormVar="w_CHARTTYPE",RowSource=""+"Pie,"+"Doughnut,"+"Full-Stacked Bars,"+"Points,"+"Lines,"+"Area,"+"Simple Bars,"+"Multiple Bars,"+"Stacked Bars,"+"Stacked Area,"+"3D Bars,"+"Horiz. Simple Bars,"+"Horiz. Multiple Bars,"+"Horiz. Stacked Bars,"+"Horiz. Full-Stacked Bars,"+"Full-Stacked Area,"+"Paired Horizontal Bars", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oCHARTTYPE_1_3.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    iif(this.value =5,5,;
    iif(this.value =6,6,;
    iif(this.value =7,7,;
    iif(this.value =8,8,;
    iif(this.value =9,9,;
    iif(this.value =10,10,;
    iif(this.value =11,11,;
    iif(this.value =12,12,;
    iif(this.value =13,13,;
    iif(this.value =14,14,;
    iif(this.value =15,15,;
    iif(this.value =16,16,;
    iif(this.value =17,17,;
    0))))))))))))))))))
  endfunc
  func oCHARTTYPE_1_3.GetRadio()
    this.Parent.oContained.w_CHARTTYPE = this.RadioValue()
    return .t.
  endfunc

  func oCHARTTYPE_1_3.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CHARTTYPE==1,1,;
      iif(this.Parent.oContained.w_CHARTTYPE==2,2,;
      iif(this.Parent.oContained.w_CHARTTYPE==3,3,;
      iif(this.Parent.oContained.w_CHARTTYPE==4,4,;
      iif(this.Parent.oContained.w_CHARTTYPE==5,5,;
      iif(this.Parent.oContained.w_CHARTTYPE==6,6,;
      iif(this.Parent.oContained.w_CHARTTYPE==7,7,;
      iif(this.Parent.oContained.w_CHARTTYPE==8,8,;
      iif(this.Parent.oContained.w_CHARTTYPE==9,9,;
      iif(this.Parent.oContained.w_CHARTTYPE==10,10,;
      iif(this.Parent.oContained.w_CHARTTYPE==11,11,;
      iif(this.Parent.oContained.w_CHARTTYPE==12,12,;
      iif(this.Parent.oContained.w_CHARTTYPE==13,13,;
      iif(this.Parent.oContained.w_CHARTTYPE==14,14,;
      iif(this.Parent.oContained.w_CHARTTYPE==15,15,;
      iif(this.Parent.oContained.w_CHARTTYPE==16,16,;
      iif(this.Parent.oContained.w_CHARTTYPE==17,17,;
      0)))))))))))))))))
  endfunc


  add object oLEGENDPOSITION_1_5 as StdCombo with uid="JRGUHOEBUN",value=1,rtseq=4,rtrep=.f.,left=119,top=113,width=227,height=19;
    , ToolTipText = "Determines the Side Legend position relative to the chart.";
    , HelpContextID = 219334929;
    , cFormVar="w_LEGENDPOSITION",RowSource=""+"No Legend,"+"Vertical Top Left ,"+"Vertical Bottom Left ,"+"Vertical Top Right  ,"+"Vertical Bottom Right  ,"+"Horiz Top Left  ,"+"Horiz Top Center  ,"+"Horiz Top Right  ,"+"Horiz Bottom L  ,"+"Horiz Bottom Center   ,"+"Horiz Bottom Right   ", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oLEGENDPOSITION_1_5.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    iif(this.value =4,3,;
    iif(this.value =5,4,;
    iif(this.value =6,5,;
    iif(this.value =7,6,;
    iif(this.value =8,7,;
    iif(this.value =9,8,;
    iif(this.value =10,9,;
    iif(this.value =11,10,;
    0))))))))))))
  endfunc
  func oLEGENDPOSITION_1_5.GetRadio()
    this.Parent.oContained.w_LEGENDPOSITION = this.RadioValue()
    return .t.
  endfunc

  func oLEGENDPOSITION_1_5.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_LEGENDPOSITION==0,1,;
      iif(this.Parent.oContained.w_LEGENDPOSITION==1,2,;
      iif(this.Parent.oContained.w_LEGENDPOSITION==2,3,;
      iif(this.Parent.oContained.w_LEGENDPOSITION==3,4,;
      iif(this.Parent.oContained.w_LEGENDPOSITION==4,5,;
      iif(this.Parent.oContained.w_LEGENDPOSITION==5,6,;
      iif(this.Parent.oContained.w_LEGENDPOSITION==6,7,;
      iif(this.Parent.oContained.w_LEGENDPOSITION==7,8,;
      iif(this.Parent.oContained.w_LEGENDPOSITION==8,9,;
      iif(this.Parent.oContained.w_LEGENDPOSITION==9,10,;
      iif(this.Parent.oContained.w_LEGENDPOSITION==10,11,;
      0)))))))))))
  endfunc

  add object oSHOWVALUESONSHAPES_1_7 as StdCheck with uid="CMUXKTJFBU",rtseq=5,rtrep=.f.,left=17, top=143, caption="Show values on shapes",;
    ToolTipText = "Determines if the source values will be drawn inside the shapes of the chart.",;
    HelpContextID = 256948604,;
    cFormVar="w_SHOWVALUESONSHAPES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSHOWVALUESONSHAPES_1_7.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oSHOWVALUESONSHAPES_1_7.GetRadio()
    this.Parent.oContained.w_SHOWVALUESONSHAPES = this.RadioValue()
    return .t.
  endfunc

  func oSHOWVALUESONSHAPES_1_7.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_SHOWVALUESONSHAPES==.T.,1,;
      0)
  endfunc

  add object oCHANGECOLORONMOUSE_1_8 as StdCheck with uid="KYPJLAIYFI",rtseq=6,rtrep=.f.,left=17, top=168, caption="Change color on mouse over",;
    ToolTipText = "Determines if the scale in the Axis will be shown.",;
    HelpContextID = 94683641,;
    cFormVar="w_CHANGECOLORONMOUSE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHANGECOLORONMOUSE_1_8.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oCHANGECOLORONMOUSE_1_8.GetRadio()
    this.Parent.oContained.w_CHANGECOLORONMOUSE = this.RadioValue()
    return .t.
  endfunc

  func oCHANGECOLORONMOUSE_1_8.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CHANGECOLORONMOUSE==.T.,1,;
      0)
  endfunc

  add object oSHOWTIPS_1_9 as StdCheck with uid="ILBFZUFVVZ",rtseq=7,rtrep=.f.,left=17, top=195, caption="Show tooltips",;
    ToolTipText = "If true, tips are shown when user moves the mouse over the chart shapes.",;
    HelpContextID = 246551467,;
    cFormVar="w_SHOWTIPS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSHOWTIPS_1_9.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oSHOWTIPS_1_9.GetRadio()
    this.Parent.oContained.w_SHOWTIPS = this.RadioValue()
    return .t.
  endfunc

  func oSHOWTIPS_1_9.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_SHOWTIPS==.T.,1,;
      0)
  endfunc

  add object oDEPTH_1_10 as StdSpinner with uid="TPVYVBVHJP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DEPTH", cQueryName = "DEPTH",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Control the depth effect (3D level) of chart. Use the value 0 (zero) for plain charts",;
    HelpContextID = 214519846,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=329, Top=196, cSayPict='"99"', cGetPict='"99"', SpinnerLowValue=0, SpinnerHighValue=50, Increment=1

  func oDEPTH_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DEPTH>=0 AND .w_DEPTH<=50)
    endwith
    return bRes
  endfunc

  add object oSHADOW_1_12 as StdCheck with uid="AYYQNVUXJY",rtseq=9,rtrep=.f.,left=241, top=168, caption="Shadow",;
    ToolTipText = "Determines if a shadow will be drawn instead of the 3d - depth efect",;
    HelpContextID = 70334953,;
    cFormVar="w_SHADOW", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSHADOW_1_12.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oSHADOW_1_12.GetRadio()
    this.Parent.oContained.w_SHADOW = this.RadioValue()
    return .t.
  endfunc

  func oSHADOW_1_12.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_SHADOW==.T.,1,;
      0)
  endfunc

  add object oQUERYMODEL_1_14 as StdField with uid="PZEVTCGDRS",rtseq=11,rtrep=.f.,;
    cFormVar = "w_QUERYMODEL", cQueryName = "QUERYMODEL",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Visual query for configuration",;
    HelpContextID = 56439035,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=96, Top=13, InputMask=replicate('X',250), bHasZoom = .t. 

  proc oQUERYMODEL_1_14.mZoom
    this.parent.oContained.w_QUERYMODEL = SYS(2014, GETFILE("VQR"))
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oFILEMODEL_1_18 as StdField with uid="FTBVWLQFEX",rtseq=13,rtrep=.f.,;
    cFormVar = "w_FILEMODEL", cQueryName = "FILEMODEL",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "File model for configuration",;
    HelpContextID = 10337230,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=96, Top=37, InputMask=replicate('X',250), bHasZoom = .t. 

  proc oFILEMODEL_1_18.mZoom
    this.parent.oContained.w_FILEMODEL = SYS(2014, GETFILE("VFC"))
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object BTNQRY as cp_askfile with uid="SVNANQOYOT",left=381, top=14, width=19,height=18,;
    caption='...',;
    var="w_QUERY",cExt ="VQR",;
    nPag=1;
    , ToolTipText = "Visual query for configuration";
    , HelpContextID = 154598716;
  , bGlobalFont=.t.



  add object BTNMOD as cp_askfile with uid="SWQPTUSBXY",left=381, top=39, width=19,height=18,;
    caption='...',;
    var="w_FILECFG",cExt="VFC",;
    nPag=1;
    , ToolTipText = "File model for configuration";
    , HelpContextID = 154598716;
  , bGlobalFont=.t.


  add object oFILEREPORT_1_27 as StdField with uid="WECPPPLBHZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_FILEREPORT", cQueryName = "FILEREPORT",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Report for FoxCharts",;
    HelpContextID = 101558578,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=96, Top=62, InputMask=replicate('X',250), bHasZoom = .t. 

  proc oFILEREPORT_1_27.mZoom
    this.parent.oContained.w_FILEREPORT = SYS(2014, GETFILE("FRX"))
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object BTNREP as cp_askfile with uid="GLZBKPQGHR",left=381, top=64, width=19,height=18,;
    caption='...',;
    var="w_FILERPT",cExt="FRX",;
    nPag=1;
    , ToolTipText = "Report for FoxCharts";
    , HelpContextID = 154598716;
  , bGlobalFont=.t.



  add object oObj_1_35 as cp_runprogram with uid="UXGCHEWVEU",left=12, top=462, width=306,height=26,;
    caption='GETTFIELDSFROMCHART',;
    prg="cp_setfoxchartproperties('GETTFIELDSFROMCHART')",;
    cEvent = "GETTFIELDSFROMCHART",;
    nPag=1;
    , HelpContextID = 245039844;
  , bGlobalFont=.t.



  add object oObj_1_36 as cp_runprogram with uid="AURNWUWPKF",left=12, top=434, width=290,height=26,;
    caption='SETCURSORDATA',;
    prg="cp_setfoxchartproperties('SETCURSORDATA')",;
    cEvent = "SETCURSORDATA",;
    nPag=1;
    , HelpContextID = 216939877;
  , bGlobalFont=.t.



  add object oObj_1_37 as cp_runprogram with uid="TGRWNTHCUB",left=12, top=404, width=275,height=26,;
    caption='REDRAWFOXCHARTS',;
    prg="cp_setfoxchartproperties('REDRAWFOXCHARTS')",;
    cEvent = "REDRAWFOXCHARTS",;
    nPag=1;
    , HelpContextID = 128102574;
  , bGlobalFont=.t.



  add object oObj_1_38 as cp_runprogram with uid="IGYJWLZOKN",left=12, top=375, width=253,height=26,;
    caption='CONFIGLOADED',;
    prg="cp_setfoxchartproperties('CONFIGLOADED')",;
    cEvent = "CONFIGLOADED",;
    nPag=1;
    , HelpContextID = 34194633;
  , bGlobalFont=.t.



  add object oObj_1_39 as cp_runprogram with uid="RBDFQPDADH",left=314, top=376, width=246,height=26,;
    caption='GETLEGENDS',;
    prg="cp_setfoxchartproperties('RUNBYPARAM')",;
    cEvent = "RUNBYPARAM",;
    nPag=1;
    , HelpContextID = 118309718;
  , bGlobalFont=.t.


  add object oNUMSLICE_1_40 as StdField with uid="OYMHNACJGZ",rtseq=86,rtrep=.f.,;
    cFormVar = "w_NUMSLICE", cQueryName = "NUMSLICE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Number of Slices",;
    HelpContextID = 190899219,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=344, Top=87, cSayPict='"99"', cGetPict='"99"'

  func oNUMSLICE_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Inlist(.w_CHARTTYPE , 1, 2))
    endwith
   endif
  endfunc

  add object oMultiChart_1_43 as StdCheck with uid="CRIFWOSQOC",rtseq=99,rtrep=.f.,left=501, top=160, caption="Multi Chart",;
    ToolTipText = "Determines if more than one kind of chart is allowed to run at the same time. ",;
    HelpContextID = 239444802,;
    cFormVar="w_MultiChart", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMultiChart_1_43.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oMultiChart_1_43.GetRadio()
    this.Parent.oContained.w_MultiChart = this.RadioValue()
    return .t.
  endfunc

  func oMultiChart_1_43.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_MultiChart==.T.,1,;
      0)
  endfunc

  add object oMultiChartMargin_1_44 as StdCheck with uid="BZFIGPWUDI",rtseq=100,rtrep=.f.,left=501, top=192, caption="Multi Chart Margin",;
    ToolTipText = "Determines if there will be a margin at the beginning and at the end of the chart. To be used when a Bar chart is not present, in order to obtain a better effect.",;
    HelpContextID = 93686687,;
    cFormVar="w_MultiChartMargin", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMultiChartMargin_1_44.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oMultiChartMargin_1_44.GetRadio()
    this.Parent.oContained.w_MultiChartMargin = this.RadioValue()
    return .t.
  endfunc

  func oMultiChartMargin_1_44.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_MultiChartMargin==.T.,1,;
      0)
  endfunc

  func oMultiChartMargin_1_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MultiChart)
    endwith
   endif
  endfunc

  add object oStr_1_4 as StdString with uid="XMYMSMIWQH",Visible=.t., Left=29, Top=90,;
    Alignment=1, Width=62, Height=18,;
    Caption="Chart Type"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="XMIUEPEIOF",Visible=.t., Left=17, Top=115,;
    Alignment=1, Width=89, Height=18,;
    Caption="Legend position"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="STZSFSLNBD",Visible=.t., Left=234, Top=197,;
    Alignment=1, Width=91, Height=18,;
    Caption="Depth (3D level):"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="TPQNJTTBUV",Visible=.t., Left=17, Top=16,;
    Alignment=1, Width=74, Height=18,;
    Caption="Visual Query"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="MASOWPSCGG",Visible=.t., Left=32, Top=40,;
    Alignment=1, Width=59, Height=18,;
    Caption="File model"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="XKZWUSEWPR",Visible=.t., Left=32, Top=65,;
    Alignment=1, Width=59, Height=18,;
    Caption="Report"  ;
  , bGlobalFont=.t.

  add object oBox_1_16 as StdBox with uid="RRRJGREXTH",left=9, top=7, width=397,height=133

  add object oBox_1_17 as StdBox with uid="ZDNHMVTIVK",left=9, top=142, width=396,height=82

  add object oBox_1_42 as StdBox with uid="RJAALJHLEJ",left=492, top=153, width=132,height=71
enddefine
define class tcp_foxchartpropertiesPag2 as StdContainer
  Width  = 627
  height = 225
  stdWidth  = 627
  stdheight = 225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDONUTRATIO_2_3 as StdSpinner with uid="FORFAWBZHG",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DONUTRATIO", cQueryName = "DONUTRATIO",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 189758516,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=100, Top=28, cSayPict='"9.99"', cGetPict='"9.99"', SpinnerLowValue=0.10, SpinnerHighValue=1, Increment=0.05

  func oDONUTRATIO_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.w_CHARTTYPE, 1, 2))
    endwith
   endif
  endfunc

  func oDONUTRATIO_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DONUTRATIO>=0.01 And .w_DONUTRATIO<= 1)
    endwith
    return bRes
  endfunc

  add object oPIEDETACHPIXELS_2_5 as StdSpinner with uid="PKQPVIVJGJ",rtseq=71,rtrep=.f.,;
    cFormVar = "w_PIEDETACHPIXELS", cQueryName = "PIEDETACHPIXELS",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 174614812,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=100, Top=52, SpinnerLowValue=0, SpinnerHighValue=50

  func oPIEDETACHPIXELS_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.w_CHARTTYPE, 1, 2))
    endwith
   endif
  endfunc

  func oPIEDETACHPIXELS_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PIEDETACHPIXELS>=0 And .w_PIEDETACHPIXELS<=50)
    endwith
    return bRes
  endfunc

  add object oPIECOMPENSATEANGLES_2_7 as StdCheck with uid="QSIVMUSPTM",rtseq=72,rtrep=.f.,left=11, top=78, caption="Allow wide ellipse",;
    ToolTipText = "Recalculates the needed angles, adjusting for a better visualisation when the pie has an important difference between width and height. Set this to false to force circular shapes.",;
    HelpContextID = 63930145,;
    cFormVar="w_PIECOMPENSATEANGLES", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPIECOMPENSATEANGLES_2_7.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oPIECOMPENSATEANGLES_2_7.GetRadio()
    this.Parent.oContained.w_PIECOMPENSATEANGLES = this.RadioValue()
    return .t.
  endfunc

  func oPIECOMPENSATEANGLES_2_7.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PIECOMPENSATEANGLES==.T.,1,;
      0)
  endfunc

  func oPIECOMPENSATEANGLES_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.w_CHARTTYPE, 1, 2))
    endwith
   endif
  endfunc

  add object oPIESHOWPERCENT_2_8 as StdCheck with uid="KZGAYLWIXH",rtseq=73,rtrep=.f.,left=11, top=102, caption="Show percentage as values",;
    ToolTipText = "Shows the percentages on each slice instead of values.",;
    HelpContextID = 53138078,;
    cFormVar="w_PIESHOWPERCENT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPIESHOWPERCENT_2_8.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oPIESHOWPERCENT_2_8.GetRadio()
    this.Parent.oContained.w_PIESHOWPERCENT = this.RadioValue()
    return .t.
  endfunc

  func oPIESHOWPERCENT_2_8.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PIESHOWPERCENT==.T.,1,;
      0)
  endfunc

  func oPIESHOWPERCENT_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.w_CHARTTYPE, 1, 2) and .w_SHOWVALUESONSHAPES)
    endwith
   endif
  endfunc

  add object oPIEDETACHSLICEONCLICK_2_9 as StdCheck with uid="OYJSIKNGSR",rtseq=74,rtrep=.f.,left=11, top=126, caption="Detach slices on click",;
    HelpContextID = 225670000,;
    cFormVar="w_PIEDETACHSLICEONCLICK", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPIEDETACHSLICEONCLICK_2_9.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oPIEDETACHSLICEONCLICK_2_9.GetRadio()
    this.Parent.oContained.w_PIEDETACHSLICEONCLICK = this.RadioValue()
    return .t.
  endfunc

  func oPIEDETACHSLICEONCLICK_2_9.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PIEDETACHSLICEONCLICK==.T.,1,;
      0)
  endfunc

  func oPIEDETACHSLICEONCLICK_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.w_CHARTTYPE, 1, 2))
    endwith
   endif
  endfunc

  add object oPIEDETACHSLICEONLEGENDCLICK_2_10 as StdCheck with uid="QQLTNCEUEE",rtseq=75,rtrep=.f.,left=11, top=150, caption="Detach on legend click",;
    HelpContextID = 75151065,;
    cFormVar="w_PIEDETACHSLICEONLEGENDCLICK", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPIEDETACHSLICEONLEGENDCLICK_2_10.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oPIEDETACHSLICEONLEGENDCLICK_2_10.GetRadio()
    this.Parent.oContained.w_PIEDETACHSLICEONLEGENDCLICK = this.RadioValue()
    return .t.
  endfunc

  func oPIEDETACHSLICEONLEGENDCLICK_2_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PIEDETACHSLICEONLEGENDCLICK==.T.,1,;
      0)
  endfunc

  func oPIEDETACHSLICEONLEGENDCLICK_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.w_CHARTTYPE, 1, 2))
    endwith
   endif
  endfunc

  add object oBARSSPACEBETWEEN_2_13 as StdSpinner with uid="TIFOABIVVB",rtseq=76,rtrep=.f.,;
    cFormVar = "w_BARSSPACEBETWEEN", cQueryName = "BARSSPACEBETWEEN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "For bars chart - the distance in pixels between bars",;
    HelpContextID = 157878173,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=387, Top=144, SpinnerLowValue=0, SpinnerHighValue=50, Increment=1

  func oBARSSPACEBETWEEN_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.w_CHARTTYPE, 3,7,8,9,11,12,13,14,15,17))
    endwith
   endif
  endfunc

  func oBARSSPACEBETWEEN_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_BARSSPACEBETWEEN>=0 And .w_BARSSPACEBETWEEN<=50)
    endwith
    return bRes
  endfunc


  add object oBARTYPE_2_15 as StdCombo with uid="XUWZHJLJTQ",value=1,rtseq=77,rtrep=.f.,left=387,top=172,width=157,height=18;
    , HelpContextID = 185819432;
    , cFormVar="w_BARTYPE",RowSource=""+"Rectangular shaped bars ,"+"Cilyndrical shaped bars ,"+"Triangular shaped bars ", bObbl = .f. , nPag = 2;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oBARTYPE_2_15.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    0))))
  endfunc
  func oBARTYPE_2_15.GetRadio()
    this.Parent.oContained.w_BARTYPE = this.RadioValue()
    return .t.
  endfunc

  func oBARTYPE_2_15.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_BARTYPE==0,1,;
      iif(this.Parent.oContained.w_BARTYPE==1,2,;
      iif(this.Parent.oContained.w_BARTYPE==2,3,;
      0)))
  endfunc

  func oBARTYPE_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.w_CHARTTYPE, 3,7,8,9,11,12,13,14,15,17))
    endwith
   endif
  endfunc


  add object oBARLEGENDDIRECTION_2_18 as StdCombo with uid="DIETFBUOSA",value=1,rtseq=78,rtrep=.f.,left=387,top=198,width=157,height=18;
    , HelpContextID = 136628463;
    , cFormVar="w_BARLEGENDDIRECTION",RowSource=""+"Horizontal,"+"Vertical (from top to bottom) ,"+"Vertical 2 (from bottom to top) ", bObbl = .f. , nPag = 2;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oBARLEGENDDIRECTION_2_18.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    0))))
  endfunc
  func oBARLEGENDDIRECTION_2_18.GetRadio()
    this.Parent.oContained.w_BARLEGENDDIRECTION = this.RadioValue()
    return .t.
  endfunc

  func oBARLEGENDDIRECTION_2_18.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_BARLEGENDDIRECTION==0,1,;
      iif(this.Parent.oContained.w_BARLEGENDDIRECTION==1,2,;
      iif(this.Parent.oContained.w_BARLEGENDDIRECTION==2,3,;
      0)))
  endfunc

  func oBARLEGENDDIRECTION_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.w_CHARTTYPE, 3,7,8,9,11,12,13,14,15,17))
    endwith
   endif
  endfunc

  add object oAREA3DTOP_2_21 as StdCheck with uid="VKOHGLEWGZ",rtseq=79,rtrep=.f.,left=17, top=203, caption="Draw 3D line in top",;
    HelpContextID = 252803563,;
    cFormVar="w_AREA3DTOP", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAREA3DTOP_2_21.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oAREA3DTOP_2_21.GetRadio()
    this.Parent.oContained.w_AREA3DTOP = this.RadioValue()
    return .t.
  endfunc

  func oAREA3DTOP_2_21.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_AREA3DTOP==.T.,1,;
      0)
  endfunc

  func oAREA3DTOP_2_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.w_CHARTTYPE, 6, 10, 16))
    endwith
   endif
  endfunc

  add object oAREADRAWBORDERS_2_22 as StdCheck with uid="MPXJWURLJJ",rtseq=80,rtrep=.f.,left=176, top=203, caption="Draw borders",;
    HelpContextID = 30921853,;
    cFormVar="w_AREADRAWBORDERS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAREADRAWBORDERS_2_22.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oAREADRAWBORDERS_2_22.GetRadio()
    this.Parent.oContained.w_AREADRAWBORDERS = this.RadioValue()
    return .t.
  endfunc

  func oAREADRAWBORDERS_2_22.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_AREADRAWBORDERS==.T.,1,;
      0)
  endfunc

  func oAREADRAWBORDERS_2_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.w_CHARTTYPE, 6, 10, 16))
    endwith
   endif
  endfunc

  add object oPOINTSHAPEWIDTH_2_25 as StdSpinner with uid="HXBPDBLAZW",rtseq=81,rtrep=.f.,;
    cFormVar = "w_POINTSHAPEWIDTH", cQueryName = "POINTSHAPEWIDTH",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "For Point and Plain Line charts, determines the width of the pen that will draw the shapes.",;
    HelpContextID = 42724186,;
   bGlobalFont=.t.,;
    Height=21, Width=47, Left=505, Top=27, cSayPict='"99"', cGetPict='"99"', SpinnerLowValue=1, SpinnerHighValue=50, Increment=1

  func oPOINTSHAPEWIDTH_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.w_CHARTTYPE, 4, 5))
    endwith
   endif
  endfunc

  func oPOINTSHAPEWIDTH_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_POINTSHAPEWIDTH>=1 And .w_POINTSHAPEWIDTH<=10)
    endwith
    return bRes
  endfunc

  add object oUniqueshape_2_27 as StdCheck with uid="BWOUPQVTCK",rtseq=82,rtrep=.f.,left=293, top=53, caption="Unique shape",;
    HelpContextID = 96653172,;
    cFormVar="w_Uniqueshape", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oUniqueshape_2_27.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oUniqueshape_2_27.GetRadio()
    this.Parent.oContained.w_Uniqueshape = this.RadioValue()
    return .t.
  endfunc

  func oUniqueshape_2_27.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Uniqueshape==.T.,1,;
      0)
  endfunc

  func oUniqueshape_2_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.w_CHARTTYPE, 4, 5))
    endwith
   endif
  endfunc

  add object oLINECAPS_2_28 as StdCheck with uid="XWCEBMFOKJ",rtseq=83,rtrep=.f.,left=293, top=26, caption="Line caps",;
    HelpContextID = 95593932,;
    cFormVar="w_LINECAPS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oLINECAPS_2_28.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oLINECAPS_2_28.GetRadio()
    this.Parent.oContained.w_LINECAPS = this.RadioValue()
    return .t.
  endfunc

  func oLINECAPS_2_28.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_LINECAPS==.T.,1,;
      0)
  endfunc

  func oLINECAPS_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (INLIST(.w_CHARTTYPE, 4, 5) and .w_Uniqueshape)
    endwith
   endif
  endfunc


  add object oLINECAPSSHAPE_2_29 as StdCombo with uid="CTQVUPFJOE",value=1,rtseq=84,rtrep=.f.,left=293,top=104,width=173,height=19;
    , HelpContextID = 223192181;
    , cFormVar="w_LINECAPSSHAPE",RowSource=""+"Default,"+"Closed circle ,"+"Closed square  ,"+"Closed triangle ,"+"Plus sign ,"+"Star,"+"Square with plus sign inside ,"+"Open square ,"+"Open circle with dot inside,"+"Lightning,"+"Man,"+"Dot ", bObbl = .f. , nPag = 2;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oLINECAPSSHAPE_2_29.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    iif(this.value =4,3,;
    iif(this.value =5,4,;
    iif(this.value =6,5,;
    iif(this.value =7,6,;
    iif(this.value =8,7,;
    iif(this.value =9,8,;
    iif(this.value =10,9,;
    iif(this.value =11,10,;
    iif(this.value =12,11,;
    0)))))))))))))
  endfunc
  func oLINECAPSSHAPE_2_29.GetRadio()
    this.Parent.oContained.w_LINECAPSSHAPE = this.RadioValue()
    return .t.
  endfunc

  func oLINECAPSSHAPE_2_29.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_LINECAPSSHAPE==0,1,;
      iif(this.Parent.oContained.w_LINECAPSSHAPE==1,2,;
      iif(this.Parent.oContained.w_LINECAPSSHAPE==2,3,;
      iif(this.Parent.oContained.w_LINECAPSSHAPE==3,4,;
      iif(this.Parent.oContained.w_LINECAPSSHAPE==4,5,;
      iif(this.Parent.oContained.w_LINECAPSSHAPE==5,6,;
      iif(this.Parent.oContained.w_LINECAPSSHAPE==6,7,;
      iif(this.Parent.oContained.w_LINECAPSSHAPE==7,8,;
      iif(this.Parent.oContained.w_LINECAPSSHAPE==8,9,;
      iif(this.Parent.oContained.w_LINECAPSSHAPE==9,10,;
      iif(this.Parent.oContained.w_LINECAPSSHAPE==10,11,;
      iif(this.Parent.oContained.w_LINECAPSSHAPE==11,12,;
      0))))))))))))
  endfunc

  func oLINECAPSSHAPE_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_MultiChart and .w_LINECAPS)
    endwith
   endif
  endfunc

  add object oPieDetachAnimationSteps_2_30 as StdSpinner with uid="BMUYABRJRH",rtseq=85,rtrep=.f.,;
    cFormVar = "w_PieDetachAnimationSteps", cQueryName = "PieDetachAnimationSteps",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 233217228,;
   bGlobalFont=.t.,;
    Height=21, Width=43, Left=221, Top=52, SpinnerLowValue=0, SpinnerHighValue=50, Increment=1

  func oPieDetachAnimationSteps_2_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PieDetachAnimationSteps >=1 And .w_PieDetachAnimationSteps <=50)
    endwith
    return bRes
  endfunc

  add object oChartRow_2_34 as StdSpinner with uid="DBUAAYDBIA",rtseq=101,rtrep=.f.,;
    cFormVar = "w_ChartRow", cQueryName = "ChartRow",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "For Pie, Doughnut and SingleBars charts, determines which row will be used to create the chart, for the case when more than one sequence of data is passed. ",;
    HelpContextID = 206725609,;
   bGlobalFont=.t.,;
    Height=21, Width=43, Left=221, Top=28, cSayPict='"99"', cGetPict='"99"', SpinnerLowValue=0, SpinnerHighValue=50, Increment=1

  func oChartRow_2_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MultiChart)
    endwith
   endif
  endfunc

  func oChartRow_2_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DEPTH>=0 AND .w_DEPTH<=50)
    endwith
    return bRes
  endfunc

  add object oStr_2_2 as StdString with uid="APGWLKWYQN",Visible=.t., Left=9, Top=5,;
    Alignment=0, Width=195, Height=19,;
    Caption="Pie and Doughnut Properties"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_4 as StdString with uid="PQCUALRSXI",Visible=.t., Left=11, Top=31,;
    Alignment=1, Width=89, Height=18,;
    Caption="Doughnut Ratio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="LUXYYTHZNY",Visible=.t., Left=22, Top=55,;
    Alignment=1, Width=78, Height=18,;
    Caption="Detach Pixels:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="VUIYFXQYZP",Visible=.t., Left=450, Top=140,;
    Alignment=0, Width=104, Height=19,;
    Caption="Bars Properties"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_14 as StdString with uid="ASBRYJUFFK",Visible=.t., Left=288, Top=149,;
    Alignment=1, Width=80, Height=18,;
    Caption="Between Bars:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="WJYORHFKCR",Visible=.t., Left=288, Top=174,;
    Alignment=0, Width=57, Height=18,;
    Caption="Bar shape"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="NLRSXMIBJP",Visible=.t., Left=288, Top=199,;
    Alignment=0, Width=92, Height=18,;
    Caption="Legend direction"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="WHIZTDEIKY",Visible=.t., Left=9, Top=182,;
    Alignment=0, Width=108, Height=19,;
    Caption="Area properties"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_24 as StdString with uid="FDXEIFYOJI",Visible=.t., Left=283, Top=6,;
    Alignment=0, Width=147, Height=19,;
    Caption="Point/Line properties"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_26 as StdString with uid="CRKQWZRMXF",Visible=.t., Left=432, Top=30,;
    Alignment=1, Width=73, Height=18,;
    Caption="Shape Width:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="WEIPYMFCBD",Visible=.t., Left=293, Top=87,;
    Alignment=1, Width=112, Height=18,;
    Caption="Caps Unique Shape"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="XZHKWQTDCX",Visible=.t., Left=160, Top=55,;
    Alignment=1, Width=54, Height=18,;
    Caption="Det Steps"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="BABOHLQBYS",Visible=.t., Left=156, Top=31,;
    Alignment=1, Width=58, Height=18,;
    Caption="Chart Row"  ;
  , bGlobalFont=.t.

  add object oBox_2_1 as StdBox with uid="TTJAEQMWXC",left=5, top=5, width=271,height=174

  add object oBox_2_11 as StdBox with uid="PFWFZMAZYM",left=279, top=140, width=303,height=84

  add object oBox_2_19 as StdBox with uid="ACLQHOZQCO",left=5, top=182, width=271,height=42

  add object oBox_2_23 as StdBox with uid="UNSQJCBOOY",left=279, top=6, width=302,height=129
enddefine
define class tcp_foxchartpropertiesPag3 as StdContainer
  Width  = 627
  height = 225
  stdWidth  = 627
  stdheight = 225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_2 as stdDynamicChildContainer with uid="RPVPLYGQQU",left=6, top=8, width=587, height=211, bOnScreen=.t.;

enddefine
define class tcp_foxchartpropertiesPag4 as StdContainer
  Width  = 627
  height = 225
  stdWidth  = 627
  stdheight = 225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oROTATIONCENTER_4_4 as StdSpinner with uid="JCJXXHFMLF",rtseq=49,rtrep=.f.,;
    cFormVar = "w_ROTATIONCENTER", cQueryName = "ROTATIONCENTER",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Specifies the rotation of the text. The point of rotation is at the center of the string",;
    HelpContextID = 217950840,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=561, Top=97, cSayPict='"999"', cGetPict='"999"', SpinnerLowValue=0, SpinnerHighValue=255, Increment=1

  add object oROTATION_4_5 as StdSpinner with uid="UCHWINAPKU",rtseq=50,rtrep=.f.,;
    cFormVar = "w_ROTATION", cQueryName = "ROTATION",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Specifies the rotation of the text. The point of rotation is determined by the alignment property value of text",;
    HelpContextID = 201990,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=561, Top=74, cSayPict='"999"', cGetPict='"999"', SpinnerLowValue=0, SpinnerHighValue=255, Increment=1


  add object oBtn_4_8 as StdButton with uid="ZXSDJSXNDQ",left=254, top=111, width=21,height=20,;
    caption="...", nPag=4;
    , ToolTipText = "Press to specifies the background color used to display text";
    , HelpContextID = 154598716;
  , bGlobalFont=.t.

    proc oBtn_4_8.Click()
      with this.Parent.oContained
        .SetColor('LBACKCOLOR')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBckColorChartL as cp_calclbl with uid="EVFCJWFLHA",left=280, top=112, width=74,height=18,;
    caption='Object',;
    caption=" ",;
    nPag=4;
    , ToolTipText = "background color";
    , HelpContextID = 131116112;
  , bGlobalFont=.t.



  add object oBtn_4_10 as StdButton with uid="CWJFULNZBS",left=19, top=111, width=21,height=20,;
    caption="...", nPag=4;
    , ToolTipText = "Press to specifies the foreground color used to display text ";
    , HelpContextID = 154598716;
  , bGlobalFont=.t.

    proc oBtn_4_10.Click()
      with this.Parent.oContained
        .SetColor('LFORECOLOR')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oForeColorChartL as cp_calclbl with uid="WWWSMGRLRR",left=45, top=112, width=74,height=18,;
    caption='Object',;
    caption=" ",BorderStyle=1,BoderWidth=1,;
    nPag=4;
    , ToolTipText = "Foreground color";
    , HelpContextID = 131116112;
  , bGlobalFont=.t.



  add object oFIELDCOLOR_4_17 as StdTableComboALias with uid="WIKSICVGYO",rtseq=53,rtrep=.f.,left=342,top=187,width=118,height=19;
    , ToolTipText = "The field name of the cursor that contains the RGB values of the custom colors for the chart";
    , HelpContextID = 48003995;
    , cFormVar="w_FIELDCOLOR",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(25);
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.


  add object oFIELDDETACHSLICE_4_18 as StdTableComboALias with uid="BKADHROAFI",rtseq=54,rtrep=.f.,left=342,top=163,width=118,height=19;
    , ToolTipText = "The field name of the cursor that contains the logical values that tell if the slice of the Pie or Doughnut chart will be detached or not";
    , HelpContextID = 166358291;
    , cFormVar="w_FIELDDETACHSLICE",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(25);
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oFIELDDETACHSLICE_4_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Inlist(.w_CHARTTYPE, 1, 2 ))
    endwith
   endif
  endfunc


  add object oFIELDHIDESLICE_4_19 as StdTableComboALias with uid="QEGQRJMKSC",rtseq=55,rtrep=.f.,left=342,top=140,width=118,height=19;
    , ToolTipText = "The field name of the cursor that contains the logical values that tell if the slice of the Pie or Doughnut chart will be hidden or not";
    , HelpContextID = 140206967;
    , cFormVar="w_FIELDHIDESLICE",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(25);
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oFIELDHIDESLICE_4_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Inlist(.w_CHARTTYPE, 1,2))
    endwith
   endif
  endfunc


  add object oFIELDAXIS2_4_20 as StdTableComboALias with uid="JRUZSPAQUV",rtseq=56,rtrep=.f.,left=101,top=165,width=118,height=19;
    , ToolTipText = "The name of the field that contains the text to be drawn in the axys opposite to the scale.";
    , HelpContextID = 112548763;
    , cFormVar="w_FIELDAXIS2",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(25);
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oFIELDAXIS2_4_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!Inlist(.w_CHARTTYPE, 1, 2))
    endwith
   endif
  endfunc


  add object oFIELDLEGEND_4_21 as StdTableComboALias with uid="USNYOLTNNA",rtseq=57,rtrep=.f.,left=101,top=142,width=118,height=19;
    , ToolTipText = "The field name that contains the character values that contain the main legends of the Pie, Doughnut or Single bar ";
    , HelpContextID = 125629473;
    , cFormVar="w_FIELDLEGEND",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(25);
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oFIELDLEGEND_4_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Inlist(.w_CHARTTYPE, 1, 2, 7))
    endwith
   endif
  endfunc

  add object oLBACKCOLORALPHA_4_23 as StdSpinner with uid="QXILVRATKJ",rtseq=58,rtrep=.f.,;
    cFormVar = "w_LBACKCOLORALPHA", cQueryName = "LBACKCOLORALPHA",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Background color trasparency",;
    HelpContextID = 115190008,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=363, Top=108, cSayPict='"999"', cGetPict='"999"', SpinnerLowValue=0, SpinnerHighValue=255, Increment=1

  func oLBACKCOLORALPHA_4_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_LBACKCOLORALPHA >= 0 and .w_LBACKCOLORALPHA <=255)
    endwith
    return bRes
  endfunc


  add object oALIGNMENT_4_24 as StdCombo with uid="EIQTUYQFWM",value=1,rtseq=59,rtrep=.f.,left=339,top=74,width=86,height=19;
    , ToolTipText = "Specifies the alignment of the text";
    , HelpContextID = 49489231;
    , cFormVar="w_ALIGNMENT",RowSource=""+"Left,"+"Right,"+"Center", bObbl = .f. , nPag = 4;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oALIGNMENT_4_24.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    0))))
  endfunc
  func oALIGNMENT_4_24.GetRadio()
    this.Parent.oContained.w_ALIGNMENT = this.RadioValue()
    return .t.
  endfunc

  func oALIGNMENT_4_24.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ALIGNMENT==0,1,;
      iif(this.Parent.oContained.w_ALIGNMENT==1,2,;
      iif(this.Parent.oContained.w_ALIGNMENT==2,3,;
      0)))
  endfunc

  add object oFONTUNDERLINE_4_29 as StdCheck with uid="FTVKGNQDTU",rtseq=60,rtrep=.f.,left=301, top=80, caption="",;
    ToolTipText = "Specifies if the text is underlined",;
    HelpContextID = 90343245,;
    cFormVar="w_FONTUNDERLINE", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFONTUNDERLINE_4_29.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oFONTUNDERLINE_4_29.GetRadio()
    this.Parent.oContained.w_FONTUNDERLINE = this.RadioValue()
    return .t.
  endfunc

  func oFONTUNDERLINE_4_29.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FONTUNDERLINE==.T.,1,;
      0)
  endfunc

  add object oFONTITALIC_4_30 as StdCheck with uid="CTJSGDHDAM",rtseq=61,rtrep=.f.,left=266, top=80, caption="",;
    ToolTipText = "Specifies if the text is italic",;
    HelpContextID = 214929700,;
    cFormVar="w_FONTITALIC", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFONTITALIC_4_30.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oFONTITALIC_4_30.GetRadio()
    this.Parent.oContained.w_FONTITALIC = this.RadioValue()
    return .t.
  endfunc

  func oFONTITALIC_4_30.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FONTITALIC==.T.,1,;
      0)
  endfunc

  add object oFONTBOLD_4_31 as StdCheck with uid="LLCOZBGVVO",rtseq=62,rtrep=.f.,left=231, top=80, caption="",;
    ToolTipText = "Specifies if the text is bold ",;
    HelpContextID = 212190684,;
    cFormVar="w_FONTBOLD", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFONTBOLD_4_31.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oFONTBOLD_4_31.GetRadio()
    this.Parent.oContained.w_FONTBOLD = this.RadioValue()
    return .t.
  endfunc

  func oFONTBOLD_4_31.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FONTBOLD==.T.,1,;
      0)
  endfunc


  add object oFONTNAME_4_36 as StdTableComboFont with uid="LLEZQJAASU",rtseq=63,rtrep=.f.,left=13,top=74,width=151,height=19;
    , ToolTipText = "Brush type";
    , HelpContextID = 211130844;
    , cFormVar="w_FONTNAME",tablefilter="", bObbl = .f. , nPag = 4;
    , cTable='',cKey='',cValue='',cOrderBy='',xDefault=space(50);
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oLFORECOLORALPHA_4_37 as StdSpinner with uid="ZWIDECJKBJ",rtseq=64,rtrep=.f.,;
    cFormVar = "w_LFORECOLORALPHA", cQueryName = "LFORECOLORALPHA",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Foreground color trasparency",;
    HelpContextID = 115191290,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=129, Top=108, cSayPict='"999"', cGetPict='"999"', SpinnerLowValue=0, SpinnerHighValue=255, Increment=1

  func oLFORECOLORALPHA_4_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_LFORECOLORALPHA >= 0 and .w_LFORECOLORALPHA <=255)
    endwith
    return bRes
  endfunc

  add object oFONTSIZE_4_38 as StdSpinner with uid="FJQMMRNRVL",rtseq=65,rtrep=.f.,;
    cFormVar = "w_FONTSIZE", cQueryName = "FONTSIZE",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 210244828,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=171, Top=75, cSayPict='"99"', cGetPict='"99"', SpinnerLowValue=6, SpinnerHighValue=50, Increment=1

  add object oFORMAT_4_39 as StdField with uid="RRSNPIUESJ",rtseq=66,rtrep=.f.,;
    cFormVar = "w_FORMAT", cQueryName = "FORMAT",;
    bObbl = .f. , nPag = 4, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Specifies the formatting of the text's value",;
    HelpContextID = 20019016,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=377, Top=35, InputMask=replicate('X',200)

  add object oCAPTION_4_40 as StdField with uid="WHHSZGCEOG",rtseq=67,rtrep=.f.,;
    cFormVar = "w_CAPTION", cQueryName = "CAPTION",;
    bObbl = .f. , nPag = 4, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Specifies the text that will be shown. Can be set only for Title, SubTitle, XAxis and YAxis. For the other controls, this is ignored because the text is determined during the chart creation.",;
    HelpContextID = 203178278,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=145, Top=35, InputMask=replicate('X',200)


  add object oLEGENDS_4_41 as StdCombo with uid="ETCQCIMKBT",value=1,rtseq=68,rtrep=.f.,left=13,top=35,width=122,height=19;
    , ToolTipText = "Leggenda";
    , HelpContextID = 86021421;
    , cFormVar="w_LEGENDS",RowSource=""+"Title,"+"SubTitle,"+"X Axis,"+"Axis 2,"+"Y Axis,"+"Shape Legend,"+"Scale Legend,"+"Side Legend", bObbl = .f. , nPag = 4;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oLEGENDS_4_41.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    iif(this.value =4,3,;
    iif(this.value =5,4,;
    iif(this.value =6,5,;
    iif(this.value =7,6,;
    iif(this.value =8,7,;
    0)))))))))
  endfunc
  func oLEGENDS_4_41.GetRadio()
    this.Parent.oContained.w_LEGENDS = this.RadioValue()
    return .t.
  endfunc

  func oLEGENDS_4_41.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_LEGENDS==0,1,;
      iif(this.Parent.oContained.w_LEGENDS==1,2,;
      iif(this.Parent.oContained.w_LEGENDS==2,3,;
      iif(this.Parent.oContained.w_LEGENDS==3,4,;
      iif(this.Parent.oContained.w_LEGENDS==4,5,;
      iif(this.Parent.oContained.w_LEGENDS==5,6,;
      iif(this.Parent.oContained.w_LEGENDS==6,7,;
      iif(this.Parent.oContained.w_LEGENDS==7,8,;
      0))))))))
  endfunc

  proc oLEGENDS_4_41.mBefore
      with this.Parent.oContained
        cp_setfoxchartproperties(this.Parent.oContained,"SETLEGENDS")
      endwith
  endproc

  proc oLEGENDS_4_41.mAfter
      with this.Parent.oContained
        cp_setfoxchartproperties(this.Parent.oContained,"GETLEGENDS")
      endwith
  endproc

  add object oStr_4_1 as StdString with uid="XXQENJZSCB",Visible=.t., Left=129, Top=98,;
    Alignment=1, Width=30, Height=12,;
    Caption="Alpha"  ;
    , FontName = "Tahoma", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_2 as StdString with uid="SPKIBNHUOR",Visible=.t., Left=20, Top=98,;
    Alignment=1, Width=47, Height=11,;
    Caption="Forecolor"  ;
    , FontName = "Tahoma", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_3 as StdString with uid="KEVTTGJNYY",Visible=.t., Left=254, Top=98,;
    Alignment=1, Width=47, Height=12,;
    Caption="Backcolor"  ;
    , FontName = "Tahoma", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_12 as StdString with uid="QKXQNYDMUE",Visible=.t., Left=14, Top=166,;
    Alignment=1, Width=81, Height=18,;
    Caption="Field Axis2"  ;
  , bGlobalFont=.t.

  add object oStr_4_13 as StdString with uid="ATITGUWPKZ",Visible=.t., Left=17, Top=144,;
    Alignment=1, Width=78, Height=18,;
    Caption="Field legend:"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="EDZXIVESHW",Visible=.t., Left=234, Top=186,;
    Alignment=1, Width=102, Height=16,;
    Caption="Field Color:"  ;
  , bGlobalFont=.t.

  add object oStr_4_15 as StdString with uid="NLGJNNSFSP",Visible=.t., Left=234, Top=164,;
    Alignment=1, Width=102, Height=18,;
    Caption="Field Detach Slice:"  ;
  , bGlobalFont=.t.

  add object oStr_4_16 as StdString with uid="GXSOZQKHZX",Visible=.t., Left=247, Top=142,;
    Alignment=1, Width=89, Height=18,;
    Caption="Field Hide Slice:"  ;
  , bGlobalFont=.t.

  add object oStr_4_22 as StdString with uid="RZZHGLUYSL",Visible=.t., Left=339, Top=61,;
    Alignment=0, Width=42, Height=14,;
    Caption="Alignment"  ;
    , FontName = "Tahoma", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_25 as StdString with uid="AUQANIOWZK",Visible=.t., Left=145, Top=23,;
    Alignment=0, Width=45, Height=14,;
    Caption="Caption"  ;
    , FontName = "Tahoma", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_26 as StdString with uid="LALNHYYHMG",Visible=.t., Left=363, Top=98,;
    Alignment=1, Width=30, Height=12,;
    Caption="Alpha"  ;
    , FontName = "Tahoma", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_27 as StdString with uid="LWTLFHOFUS",Visible=.t., Left=171, Top=61,;
    Alignment=0, Width=22, Height=14,;
    Caption="Size"  ;
    , FontName = "Tahoma", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_28 as StdString with uid="EMZGYIKXVH",Visible=.t., Left=15, Top=61,;
    Alignment=0, Width=45, Height=14,;
    Caption="Font name"  ;
    , FontName = "Tahoma", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_32 as StdString with uid="WBFABBZISJ",Visible=.t., Left=291, Top=61,;
    Alignment=0, Width=37, Height=14,;
    Caption="underline"  ;
    , FontName = "Tahoma", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_33 as StdString with uid="MZGQWVVFTB",Visible=.t., Left=262, Top=61,;
    Alignment=0, Width=26, Height=14,;
    Caption="Italic"  ;
    , FontName = "Tahoma", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_34 as StdString with uid="ZOZNQDZMMX",Visible=.t., Left=230, Top=61,;
    Alignment=0, Width=22, Height=14,;
    Caption="Bold"  ;
    , FontName = "Tahoma", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_43 as StdString with uid="RPLEHEOTWQ",Visible=.t., Left=11, Top=11,;
    Alignment=0, Width=103, Height=19,;
    Caption="Label / Legends"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_44 as StdString with uid="BHVDXLIHXF",Visible=.t., Left=377, Top=23,;
    Alignment=0, Width=45, Height=14,;
    Caption="Format"  ;
    , FontName = "Tahoma", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_45 as StdString with uid="HRLJTARTAC",Visible=.t., Left=453, Top=99,;
    Alignment=1, Width=102, Height=18,;
    Caption="Rotation center:"  ;
  , bGlobalFont=.t.

  add object oStr_4_46 as StdString with uid="OPVKPLXQBJ",Visible=.t., Left=466, Top=74,;
    Alignment=1, Width=89, Height=18,;
    Caption="Rotation angle:"  ;
  , bGlobalFont=.t.

  add object oBox_4_42 as StdBox with uid="AVCKFWNZOX",left=6, top=9, width=614,height=212
enddefine
define class tcp_foxchartpropertiesPag5 as StdContainer
  Width  = 627
  height = 225
  stdWidth  = 627
  stdheight = 225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSHOWAXIS_5_1 as StdCheck with uid="DQSLPAIAUM",rtseq=12,rtrep=.f.,left=15, top=17, caption="Show Axis",;
    ToolTipText = "For Bars, Lines, Area or Point charts; defines if the X and Y axys will be drawn",;
    HelpContextID = 246953643,;
    cFormVar="w_SHOWAXIS", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oSHOWAXIS_5_1.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oSHOWAXIS_5_1.GetRadio()
    this.Parent.oContained.w_SHOWAXIS = this.RadioValue()
    return .t.
  endfunc

  func oSHOWAXIS_5_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_SHOWAXIS==.T.,1,;
      0)
  endfunc

  func oSHOWAXIS_5_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! INLIST(.w_CHARTTYPE, 1, 2))
    endwith
   endif
  endfunc

  add object oShowAxis2Tics_5_2 as StdCheck with uid="GCPZLYJBHY",rtseq=88,rtrep=.f.,left=15, top=48, caption="Show Tics on legend axis",;
    ToolTipText = "For Bars, Lines, Area or Point charts; determines if the legend axis (axis2) will show tic marks on each legend.",;
    HelpContextID = 90850574,;
    cFormVar="w_ShowAxis2Tics", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oShowAxis2Tics_5_2.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oShowAxis2Tics_5_2.GetRadio()
    this.Parent.oContained.w_ShowAxis2Tics = this.RadioValue()
    return .t.
  endfunc

  func oShowAxis2Tics_5_2.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ShowAxis2Tics==.T.,1,;
      0)
  endfunc

  add object oTicLength_5_3 as StdSpinner with uid="TBITMSOYLO",rtseq=89,rtrep=.f.,;
    cFormVar = "w_TicLength", cQueryName = "TicLength",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Determines if the gradient start and destination colors will be inverted.",;
    HelpContextID = 209145017,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=93, Top=83, cSayPict='"99"', cGetPict='"99"', SpinnerLowValue=1, SpinnerHighValue=1, Increment=1

  func oTicLength_5_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TicLength >=1 And .w_TicLength <= 40)
    endwith
    return bRes
  endfunc

  add object oScaleBackLinesDash_5_5 as StdSpinner with uid="VXBJZEKEHR",rtseq=90,rtrep=.f.,;
    cFormVar = "w_ScaleBackLinesDash", cQueryName = "ScaleBackLinesDash",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 112864000,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=93, Top=116, SpinnerLowValue=0, SpinnerHighValue=4, Increment=1

  func oScaleBackLinesDash_5_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ScaleBackLinesDash>=0 And .w_ScaleBackLinesDash<=4)
    endwith
    return bRes
  endfunc

  add object oScaleBackLinesWidth_5_7 as StdSpinner with uid="DDUHIZWHZS",rtseq=91,rtrep=.f.,;
    cFormVar = "w_ScaleBackLinesWidth", cQueryName = "ScaleBackLinesWidth",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Width in pixels of the GDI+ pen used to draw the background scale.",;
    HelpContextID = 139275112,;
   bGlobalFont=.t.,;
    Height=23, Width=49, Left=94, Top=149, SpinnerLowValue=0, SpinnerHighValue=50, Increment=1


  add object oScaleLineColor as cp_calclbl with uid="BPRYGDWHIC",left=339, top=21, width=80,height=18,;
    caption='Object',;
    caption=" ",BorderStyle=1,BoderWidth=1,;
    nPag=5;
    , HelpContextID = 131116112;
  , bGlobalFont=.t.



  add object oBtn_5_10 as StdButton with uid="SBPBTRCBWJ",left=313, top=20, width=21,height=20,;
    caption="...", nPag=5;
    , ToolTipText = "Press to specifies the RGB value for the Background scale line color.";
    , HelpContextID = 154598716;
  , bGlobalFont=.t.

    proc oBtn_5_10.Click()
      with this.Parent.oContained
        .SetColor('ScaleLineColor')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oScaleBackColor as cp_calclbl with uid="YTSIPFXTLS",left=339, top=47, width=80,height=18,;
    caption='Object',;
    caption=" ",;
    nPag=5;
    , HelpContextID = 131116112;
  , bGlobalFont=.t.



  add object oBtn_5_12 as StdButton with uid="MWTUDOOASZ",left=313, top=46, width=21,height=20,;
    caption="...", nPag=5;
    , ToolTipText = "Press to specifies the RGB value for the Background scale bar color.";
    , HelpContextID = 154598716;
  , bGlobalFont=.t.

    proc oBtn_5_12.Click()
      with this.Parent.oContained
        .SetColor('ScaleBackColor')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oAxisColor as cp_calclbl with uid="XMIHFBMDKA",left=339, top=75, width=80,height=18,;
    caption='Object',;
    caption=" ",BorderStyle=1,BoderWidth=1,;
    nPag=5;
    , HelpContextID = 131116112;
  , bGlobalFont=.t.



  add object oBtn_5_18 as StdButton with uid="KZQOAATSZS",left=314, top=74, width=21,height=20,;
    caption="...", nPag=5;
    , ToolTipText = "Press to specifies the RGB value for the Axys main color ";
    , HelpContextID = 154598716;
  , bGlobalFont=.t.

    proc oBtn_5_18.Click()
      with this.Parent.oContained
        .SetColor('AxisColor')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oScaleLineZeroColor as cp_calclbl with uid="IRDCCZGKHG",left=339, top=101, width=80,height=18,;
    caption='Object',;
    caption=" ",;
    nPag=5;
    , HelpContextID = 131116112;
  , bGlobalFont=.t.



  add object oBtn_5_20 as StdButton with uid="ZQAVUCAMOJ",left=314, top=100, width=21,height=20,;
    caption="...", nPag=5;
    , ToolTipText = "Press to specifies the RGB value for the color of the line of the zero scale.";
    , HelpContextID = 154598716;
  , bGlobalFont=.t.

    proc oBtn_5_20.Click()
      with this.Parent.oContained
        .SetColor('ScaleLineZeroColor')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oScaleBackAlpha_5_25 as StdSpinner with uid="AADXGPWUQR",rtseq=92,rtrep=.f.,;
    cFormVar = "w_ScaleBackAlpha", cQueryName = "ScaleBackAlpha",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 43708893,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=562, Top=20, cSayPict='"999"', cGetPict='"999"', SpinnerLowValue=0, SpinnerHighValue=255,  Increment=1

  func oScaleBackAlpha_5_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ScaleBackAlpha >=0 And .w_ScaleBackAlpha <= 255)
    endwith
    return bRes
  endfunc

  add object oAxisAlpha_5_27 as StdSpinner with uid="HLOWZUGGSO",rtseq=93,rtrep=.f.,;
    cFormVar = "w_AxisAlpha", cQueryName = "AxisAlpha",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 239072465,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=562, Top=74, cSayPict='"999"', cGetPict='"999"', SpinnerLowValue=0, SpinnerHighValue=255,  Increment=1

  func oAxisAlpha_5_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_AxisAlpha >=0 And .w_AxisAlpha <=255)
    endwith
    return bRes
  endfunc

  add object oShowLineZero_5_29 as StdCheck with uid="NIIWFCMUKE",rtseq=94,rtrep=.f.,left=497, top=99, caption="Show Line Zero",;
    ToolTipText = "Determines if the background line for the zero scale will be shown or not.",;
    HelpContextID = 210628055,;
    cFormVar="w_ShowLineZero", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oShowLineZero_5_29.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oShowLineZero_5_29.GetRadio()
    this.Parent.oContained.w_ShowLineZero = this.RadioValue()
    return .t.
  endfunc

  func oShowLineZero_5_29.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ShowLineZero==.T.,1,;
      0)
  endfunc

  add object oStr_5_4 as StdString with uid="BGKQMAGZKG",Visible=.t., Left=2, Top=87,;
    Alignment=1, Width=89, Height=18,;
    Caption="Tic length:"  ;
  , bGlobalFont=.t.

  add object oStr_5_6 as StdString with uid="OLZYZCIRLE",Visible=.t., Left=13, Top=119,;
    Alignment=1, Width=78, Height=18,;
    Caption="Line style:"  ;
  , bGlobalFont=.t.

  add object oStr_5_8 as StdString with uid="WXDADNNYLA",Visible=.t., Left=4, Top=151,;
    Alignment=1, Width=89, Height=18,;
    Caption="Line width:"  ;
  , bGlobalFont=.t.

  add object oStr_5_13 as StdString with uid="CHJAHHBLRD",Visible=.t., Left=222, Top=23,;
    Alignment=1, Width=86, Height=18,;
    Caption="Line color"  ;
  , bGlobalFont=.t.

  add object oStr_5_14 as StdString with uid="AGUDLWZSKN",Visible=.t., Left=212, Top=49,;
    Alignment=1, Width=96, Height=18,;
    Caption="Bar color"  ;
  , bGlobalFont=.t.

  add object oStr_5_21 as StdString with uid="LVCRZFYIIC",Visible=.t., Left=217, Top=77,;
    Alignment=1, Width=86, Height=18,;
    Caption="Axis color"  ;
  , bGlobalFont=.t.

  add object oStr_5_22 as StdString with uid="PTYOBLAOWX",Visible=.t., Left=207, Top=103,;
    Alignment=1, Width=96, Height=18,;
    Caption="Line ZERO color"  ;
  , bGlobalFont=.t.

  add object oStr_5_26 as StdString with uid="MUYYPLBFMC",Visible=.t., Left=441, Top=23,;
    Alignment=1, Width=121, Height=18,;
    Caption="Lines and Bars Alpha:"  ;
  , bGlobalFont=.t.

  add object oStr_5_28 as StdString with uid="DCDWDBWWOG",Visible=.t., Left=484, Top=77,;
    Alignment=1, Width=78, Height=18,;
    Caption="Axis Alpha:"  ;
  , bGlobalFont=.t.

  add object oBox_5_15 as StdBox with uid="JXPVXPQKHX",left=308, top=15, width=118,height=29

  add object oBox_5_16 as StdBox with uid="LWTQBATWDD",left=309, top=42, width=118,height=29

  add object oBox_5_23 as StdBox with uid="MBEZVHNNNG",left=309, top=69, width=118,height=29

  add object oBox_5_24 as StdBox with uid="ERSZTIMMSX",left=309, top=96, width=118,height=29
enddefine
define class tcp_foxchartpropertiesPag6 as StdContainer
  Width  = 627
  height = 225
  stdWidth  = 627
  stdheight = 225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCOLORTYPE_6_1 as StdCombo with uid="EDYSZUJYQZ",rtseq=34,rtrep=.f.,left=13,top=46,width=160,height=19;
    , ToolTipText = "Color type";
    , HelpContextID = 103258578;
    , cFormVar="w_COLORTYPE",RowSource=""+"Basic colors,"+"Custom colors,"+"Random colors,"+"Gradient colors", bObbl = .f. , nPag = 6;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oCOLORTYPE_6_1.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    0)))))
  endfunc
  func oCOLORTYPE_6_1.GetRadio()
    this.Parent.oContained.w_COLORTYPE = this.RadioValue()
    return .t.
  endfunc

  func oCOLORTYPE_6_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_COLORTYPE==1,1,;
      iif(this.Parent.oContained.w_COLORTYPE==2,2,;
      iif(this.Parent.oContained.w_COLORTYPE==3,3,;
      iif(this.Parent.oContained.w_COLORTYPE==4,4,;
      0))))
  endfunc


  add object oBRUSHTYPE_6_3 as StdCombo with uid="UGBGWWTGVA",rtseq=35,rtrep=.f.,left=186,top=46,width=160,height=19;
    , ToolTipText = "Brush type";
    , HelpContextID = 86478875;
    , cFormVar="w_BRUSHTYPE",RowSource=""+"Solid colors,"+"Gradient colors,"+"Monochrome Hatch brush", bObbl = .f. , nPag = 6;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oBRUSHTYPE_6_3.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    0))))
  endfunc
  func oBRUSHTYPE_6_3.GetRadio()
    this.Parent.oContained.w_BRUSHTYPE = this.RadioValue()
    return .t.
  endfunc

  func oBRUSHTYPE_6_3.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_BRUSHTYPE==1,1,;
      iif(this.Parent.oContained.w_BRUSHTYPE==2,2,;
      iif(this.Parent.oContained.w_BRUSHTYPE==3,3,;
      0)))
  endfunc

  add object oALPHACHANNEL_6_8 as StdSpinner with uid="HWOKMHRLOR",rtseq=36,rtrep=.f.,;
    cFormVar = "w_ALPHACHANNEL", cQueryName = "ALPHACHANNEL",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Trasparency",;
    HelpContextID = 64651925,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=13, Top=87, cSayPict='"999"', cGetPict='"999"', SpinnerLowValue=0, SpinnerHighValue=255, Increment=1


  add object REFSHPMAIN as cp_calclbl with uid="PNSUPLBHKZ",left=213, top=88, width=133,height=18,;
    caption='Object',;
    caption="  ",;
    nPag=6;
    , HelpContextID = 131116112;
  , bGlobalFont=.t.



  add object oBtn_6_10 as StdButton with uid="SJZCYREAQT",left=187, top=87, width=21,height=20,;
    caption="...", nPag=6;
    , ToolTipText = "Determines the shape that will be shown for the current chart row";
    , HelpContextID = 154598716;
  , bGlobalFont=.t.

    proc oBtn_6_10.Click()
      with this.Parent.oContained
        .SetColor('SHPMAINCOLOR')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oGRADIENTSHAPEDIRECTION_6_15 as StdCombo with uid="QTRZJQSDDI",value=1,rtseq=37,rtrep=.f.,left=16,top=173,width=160,height=19;
    , ToolTipText = "Brush type";
    , HelpContextID = 47078972;
    , cFormVar="w_GRADIENTSHAPEDIRECTION",RowSource=""+"Left to Right,"+"Top to Bottom,"+"Upper Left to Lower Right,"+"Upper Right to Lower Left", bObbl = .f. , nPag = 6;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oGRADIENTSHAPEDIRECTION_6_15.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    iif(this.value =4,3,;
    0)))))
  endfunc
  func oGRADIENTSHAPEDIRECTION_6_15.GetRadio()
    this.Parent.oContained.w_GRADIENTSHAPEDIRECTION = this.RadioValue()
    return .t.
  endfunc

  func oGRADIENTSHAPEDIRECTION_6_15.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GRADIENTSHAPEDIRECTION==0,1,;
      iif(this.Parent.oContained.w_GRADIENTSHAPEDIRECTION==1,2,;
      iif(this.Parent.oContained.w_GRADIENTSHAPEDIRECTION==2,3,;
      iif(this.Parent.oContained.w_GRADIENTSHAPEDIRECTION==3,4,;
      0))))
  endfunc

  func oGRADIENTSHAPEDIRECTION_6_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_BRUSHTYPE=2)
    endwith
   endif
  endfunc

  add object oGRADIENTLEVEL_6_16 as StdSpinner with uid="TIZXTGQZUN",rtseq=38,rtrep=.f.,;
    cFormVar = "w_GRADIENTLEVEL", cQueryName = "GRADIENTLEVEL",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 245632835,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=171, Top=128, cSayPict='"999"', cGetPict='"999"', SpinnerLowValue=-10, SpinnerHighValue=10

  func oGRADIENTLEVEL_6_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_GRADIENTLEVEL >= -10 and .w_GRADIENTLEVEL<= 10)
    endwith
    return bRes
  endfunc

  add object oGRADIENTINVERTCOLORS_6_17 as StdCheck with uid="RQFWKXVTPS",rtseq=39,rtrep=.f.,left=16, top=200, caption="Invert gradient colors",;
    HelpContextID = 7062465,;
    cFormVar="w_GRADIENTINVERTCOLORS", bObbl = .f. , nPag = 6;
   , bGlobalFont=.t.


  func oGRADIENTINVERTCOLORS_6_17.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oGRADIENTINVERTCOLORS_6_17.GetRadio()
    this.Parent.oContained.w_GRADIENTINVERTCOLORS = this.RadioValue()
    return .t.
  endfunc

  func oGRADIENTINVERTCOLORS_6_17.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GRADIENTINVERTCOLORS==.T.,1,;
      0)
  endfunc

  add object oGRADIENTPOSITION_6_19 as StdSpinner with uid="WQABTDUNGK",rtseq=40,rtrep=.f.,;
    cFormVar = "w_GRADIENTPOSITION", cQueryName = "GRADIENTPOSITION",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 177178874,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=335, Top=128, cSayPict='"9.9"', cGetPict='"9.9"', SpinnerLowValue=-0, SpinnerHighValue=1, Increment=0.1

  func oGRADIENTPOSITION_6_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_GRADIENTPOSITION >= 0 and .w_GRADIENTPOSITION <= 1)
    endwith
    return bRes
  endfunc


  add object oGRADIENTTYPE_6_21 as StdCombo with uid="OZTSXGAYZE",value=1,rtseq=41,rtrep=.f.,left=290,top=176,width=97,height=20;
    , HelpContextID = 157000887;
    , cFormVar="w_GRADIENTTYPE",RowSource=""+"Sigma Bell,"+"Triangular", bObbl = .f. , nPag = 6;
    , FontName = "Tahoma", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oGRADIENTTYPE_6_21.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    0)))
  endfunc
  func oGRADIENTTYPE_6_21.GetRadio()
    this.Parent.oContained.w_GRADIENTTYPE = this.RadioValue()
    return .t.
  endfunc

  func oGRADIENTTYPE_6_21.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GRADIENTTYPE==0,1,;
      iif(this.Parent.oContained.w_GRADIENTTYPE==1,2,;
      0))
  endfunc


  add object oBckColorChart1 as cp_calclbl with uid="RDLQNHKWZW",left=506, top=29, width=80,height=18,;
    caption='Object',;
    caption=" ",BorderStyle=1,BoderWidth=1,;
    nPag=6;
    , HelpContextID = 131116112;
  , bGlobalFont=.t.



  add object oBtn_6_27 as StdButton with uid="FODNICHIUC",left=480, top=28, width=21,height=20,;
    caption="...", nPag=6;
    , ToolTipText = "Press to specifies the background color Start";
    , HelpContextID = 154598716;
  , bGlobalFont=.t.

    proc oBtn_6_27.Click()
      with this.Parent.oContained
        .SetColor('BACKCOLOR')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBckColorChart2 as cp_calclbl with uid="JAPQDWVENK",left=506, top=55, width=80,height=18,;
    caption='Object',;
    caption=" ",;
    nPag=6;
    , HelpContextID = 131116112;
  , bGlobalFont=.t.



  add object oBtn_6_29 as StdButton with uid="EWIFHTNLEY",left=480, top=54, width=21,height=20,;
    caption="...", nPag=6;
    , ToolTipText = "Press to specifies the background color End";
    , HelpContextID = 154598716;
  , bGlobalFont=.t.

    proc oBtn_6_29.Click()
      with this.Parent.oContained
        .SetColor('BACKCOLOR2')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBACKGRADIENTMODE_6_33 as StdCombo with uid="WOQNDHHJZY",value=1,rtseq=44,rtrep=.f.,left=421,top=163,width=161,height=22;
    , ToolTipText = "If using gradient background (having the property BackColor2 specified), determines the direction of the gradient colors.";
    , HelpContextID = 51859177;
    , cFormVar="w_BACKGRADIENTMODE",RowSource=""+"Left to Right,"+"Top to Bottom,"+"Upper Left to Lover Right,"+"Upper Right to Lower Left", bObbl = .f. , nPag = 6;
  , bGlobalFont=.t.


  func oBACKGRADIENTMODE_6_33.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    iif(this.value =4,3,;
    0)))))
  endfunc
  func oBACKGRADIENTMODE_6_33.GetRadio()
    this.Parent.oContained.w_BACKGRADIENTMODE = this.RadioValue()
    return .t.
  endfunc

  func oBACKGRADIENTMODE_6_33.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_BACKGRADIENTMODE==0,1,;
      iif(this.Parent.oContained.w_BACKGRADIENTMODE==1,2,;
      iif(this.Parent.oContained.w_BACKGRADIENTMODE==2,3,;
      iif(this.Parent.oContained.w_BACKGRADIENTMODE==3,4,;
      0))))
  endfunc

  add object oCHANGECOLOR_6_35 as StdSpinner with uid="JNLIHUHFWT",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CHANGECOLOR", cQueryName = "CHANGECOLOR",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 50290935,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=544, Top=193, cSayPict='"999"', cGetPict='"999"', SpinnerLowValue=-10, SpinnerHighValue=10, Increment=1

  func oCHANGECOLOR_6_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CHANGECOLOR >= -10 and .w_CHANGECOLOR<= 10)
    endwith
    return bRes
  endfunc

  add object oBACKCOLORALPHA_6_38 as StdSpinner with uid="STZDCXBIMC",rtseq=46,rtrep=.f.,;
    cFormVar = "w_BACKCOLORALPHA", cQueryName = "BACKCOLORALPHA",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Determines the transparency level, of the background of the chart.",;
    HelpContextID = 34510805,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=111, Top=87, cSayPict='"999"', cGetPict='"999"', SpinnerLowValue=0, SpinnerHighValue=255, Increment=1

  func oBACKCOLORALPHA_6_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_BACKCOLORALPHA >= 0 and .w_BACKCOLORALPHA <=255)
    endwith
    return bRes
  endfunc

  add object oStr_6_2 as StdString with uid="PEKYWNFQWL",Visible=.t., Left=13, Top=28,;
    Alignment=0, Width=79, Height=18,;
    Caption="Color type"  ;
  , bGlobalFont=.t.

  add object oStr_6_4 as StdString with uid="LGFRZBIFNU",Visible=.t., Left=186, Top=28,;
    Alignment=0, Width=79, Height=18,;
    Caption="Brush type"  ;
  , bGlobalFont=.t.

  add object oStr_6_5 as StdString with uid="ZTZOCGLRGF",Visible=.t., Left=7, Top=6,;
    Alignment=0, Width=183, Height=19,;
    Caption="Colors for the chart objects"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_7 as StdString with uid="ZEXXNGAACH",Visible=.t., Left=13, Top=70,;
    Alignment=0, Width=111, Height=18,;
    Caption="Trasparency (0-255)"  ;
  , bGlobalFont=.t.

  add object oStr_6_11 as StdString with uid="RTBTRVLXIS",Visible=.t., Left=186, Top=70,;
    Alignment=0, Width=63, Height=18,;
    Caption="Main color"  ;
  , bGlobalFont=.t.

  add object oStr_6_13 as StdString with uid="DBLKOVLTYQ",Visible=.t., Left=16, Top=130,;
    Alignment=0, Width=145, Height=18,;
    Caption="Grad. Level (-10 +10)"  ;
  , bGlobalFont=.t.

  add object oStr_6_14 as StdString with uid="HUKRACQURK",Visible=.t., Left=16, Top=154,;
    Alignment=0, Width=99, Height=18,;
    Caption="Gradient Direction"  ;
  , bGlobalFont=.t.

  add object oStr_6_18 as StdString with uid="ABHLGJKARL",Visible=.t., Left=237, Top=130,;
    Alignment=0, Width=95, Height=18,;
    Caption="Grad. Position"  ;
  , bGlobalFont=.t.

  add object oStr_6_20 as StdString with uid="YXQQVMDMCB",Visible=.t., Left=290, Top=158,;
    Alignment=0, Width=76, Height=18,;
    Caption="Grad. Type"  ;
  , bGlobalFont=.t.

  add object oStr_6_23 as StdString with uid="ZWYSFZTCKK",Visible=.t., Left=367, Top=5,;
    Alignment=0, Width=183, Height=19,;
    Caption="Background of chart"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_30 as StdString with uid="FXAKSQGWOE",Visible=.t., Left=373, Top=31,;
    Alignment=0, Width=86, Height=18,;
    Caption="Main BackColor"  ;
  , bGlobalFont=.t.

  add object oStr_6_31 as StdString with uid="ZZVACSOMNY",Visible=.t., Left=373, Top=57,;
    Alignment=0, Width=96, Height=18,;
    Caption="Main BackColor 2"  ;
  , bGlobalFont=.t.

  add object oStr_6_32 as StdString with uid="UNFHEJDUJI",Visible=.t., Left=420, Top=145,;
    Alignment=0, Width=99, Height=18,;
    Caption="Grad. Direction"  ;
  , bGlobalFont=.t.

  add object oStr_6_34 as StdString with uid="TIXYUYXIIM",Visible=.t., Left=417, Top=196,;
    Alignment=0, Width=118, Height=18,;
    Caption="Grad. Level (-10 +10)"  ;
  , bGlobalFont=.t.

  add object oStr_6_42 as StdString with uid="VXFTWGAQVB",Visible=.t., Left=417, Top=121,;
    Alignment=0, Width=148, Height=19,;
    Caption="Background of chart"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_6_6 as StdBox with uid="NTPKWKRJYC",left=4, top=5, width=353,height=110

  add object oBox_6_12 as StdBox with uid="YKNTUSNVLD",left=4, top=121, width=400,height=103

  add object oBox_6_22 as StdBox with uid="RUJKYCNKRT",left=364, top=5, width=237,height=110

  add object oBox_6_36 as StdBox with uid="EYSKOGJMJM",left=475, top=23, width=118,height=29

  add object oBox_6_37 as StdBox with uid="KFPCYTJYYW",left=475, top=50, width=118,height=29

  add object oBox_6_41 as StdBox with uid="OVOGUYWIOQ",left=411, top=121, width=191,height=102
enddefine
define class tcp_foxchartpropertiesPag7 as StdContainer
  Width  = 627
  height = 225
  stdWidth  = 627
  stdheight = 225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMARGIN_7_4 as StdSpinner with uid="MVYQCXRYFK",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MARGIN", cQueryName = "MARGIN",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 97398872,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=113, Top=26, cSayPict='"99"', cGetPict='"99"', SpinnerLowValue= 0, SpinnerHighValue=50, Increment=1

  func oMARGIN_7_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MARGIN >= 0 and .w_MARGIN<= 50)
    endwith
    return bRes
  endfunc

  add object oMARGINTOP_7_6 as StdSpinner with uid="FEEYSZAAND",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MARGINTOP", cQueryName = "MARGINTOP",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 185741400,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=113, Top=50, cSayPict='"99"', cGetPict='"99"', SpinnerLowValue= 0, SpinnerHighValue=50, Increment=1

  func oMARGINTOP_7_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MARGINTOP >= 0 and .w_MARGINTOP<= 50)
    endwith
    return bRes
  endfunc

  add object oMARGINBOTTOM_7_8 as StdSpinner with uid="ACZFQAIXWG",rtseq=21,rtrep=.f.,;
    cFormVar = "w_MARGINBOTTOM", cQueryName = "MARGINBOTTOM",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 16763529,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=113, Top=74, cSayPict='"99"', cGetPict='"99"', SpinnerLowValue= 0, SpinnerHighValue=50, Increment=1

  func oMARGINBOTTOM_7_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MARGINBOTTOM >= 0 and .w_MARGINBOTTOM<= 50)
    endwith
    return bRes
  endfunc

  add object oMARGINLEFT_7_10 as StdSpinner with uid="WOFTHJDFNZ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MARGINLEFT", cQueryName = "MARGINLEFT",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 261476264,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=282, Top=50, cSayPict='"99"', cGetPict='"99"', SpinnerLowValue= 0, SpinnerHighValue=50, Increment=1

  func oMARGINLEFT_7_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MARGINLEFT >= 0 and .w_MARGINLEFT<= 50)
    endwith
    return bRes
  endfunc

  add object oMARGINRIGHT_7_12 as StdSpinner with uid="IKONSXHKBI",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MARGINRIGHT", cQueryName = "MARGINRIGHT",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 240111444,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=282, Top=74, cSayPict='"99"', cGetPict='"99"', SpinnerLowValue= 0, SpinnerHighValue=50, Increment=1

  func oMARGINRIGHT_7_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MARGINRIGHT >= 0 and .w_MARGINRIGHT<= 50)
    endwith
    return bRes
  endfunc

  add object oSHOWSCALE_7_13 as StdCheck with uid="MLNXZVLOXN",rtseq=24,rtrep=.f.,left=199, top=116, caption="Show Scales",;
    ToolTipText = "Show Scales",;
    HelpContextID = 171013291,;
    cFormVar="w_SHOWSCALE", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oSHOWSCALE_7_13.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oSHOWSCALE_7_13.GetRadio()
    this.Parent.oContained.w_SHOWSCALE = this.RadioValue()
    return .t.
  endfunc

  func oSHOWSCALE_7_13.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_SHOWSCALE==.T.,1,;
      0)
  endfunc

  add object oSCALE_7_18 as StdField with uid="TPKDZDHISS",rtseq=25,rtrep=.f.,;
    cFormVar = "w_SCALE", cQueryName = "SCALE",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 197741719,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=140, Top=116

  func oSCALE_7_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SHOWSCALE)
    endwith
   endif
  endfunc

  add object oMINVALUE_7_19 as StdField with uid="HBLSQAPHFO",rtseq=26,rtrep=.f.,;
    cFormVar = "w_MINVALUE", cQueryName = "MINVALUE",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 93124284,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=140, Top=139, SpinnerLowValue= 0, SpinnerHighValue=50, Increment=1

  func oMINVALUE_7_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SHOWSCALE And .w_CHKMINVALUE)
    endwith
   endif
  endfunc

  add object oMAXVALUE_7_20 as StdField with uid="AKVRDWATSJ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_MAXVALUE", cQueryName = "MAXVALUE",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 175311182,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=140, Top=162, SpinnerLowValue= 0, SpinnerHighValue=50, Increment=1

  func oMAXVALUE_7_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SHOWSCALE and .w_CHKMAXVALUE)
    endwith
   endif
  endfunc

  add object oSCALEDIVIDER_7_21 as StdField with uid="YUMESCSGEP",rtseq=28,rtrep=.f.,;
    cFormVar = "w_SCALEDIVIDER", cQueryName = "SCALEDIVIDER",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 92892676,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=140, Top=185

  func oSCALEDIVIDER_7_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SHOWSCALE)
    endwith
   endif
  endfunc

  add object oCHKMINVALUE_7_22 as StdCheck with uid="WFKARHDOVN",rtseq=29,rtrep=.f.,left=121, top=142, caption="",;
    HelpContextID = 204746998,;
    cFormVar="w_CHKMINVALUE", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oCHKMINVALUE_7_22.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oCHKMINVALUE_7_22.GetRadio()
    this.Parent.oContained.w_CHKMINVALUE = this.RadioValue()
    return .t.
  endfunc

  func oCHKMINVALUE_7_22.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CHKMINVALUE==.T.,1,;
      0)
  endfunc

  func oCHKMINVALUE_7_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SHOWSCALE)
    endwith
   endif
  endfunc

  add object oCHKMAXVALUE_7_23 as StdCheck with uid="KOXTESLVYQ",rtseq=30,rtrep=.f.,left=121, top=166, caption="",;
    HelpContextID = 204785910,;
    cFormVar="w_CHKMAXVALUE", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oCHKMAXVALUE_7_23.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oCHKMAXVALUE_7_23.GetRadio()
    this.Parent.oContained.w_CHKMAXVALUE = this.RadioValue()
    return .t.
  endfunc

  func oCHKMAXVALUE_7_23.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CHKMAXVALUE==.T.,1,;
      0)
  endfunc

  func oCHKMAXVALUE_7_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SHOWSCALE)
    endwith
   endif
  endfunc

  add object oBARSPERSCALE_7_26 as StdSpinner with uid="CSXMMYFRIZ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_BARSPERSCALE", cQueryName = "BARSPERSCALE",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 55553204,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=368, Top=139, cSayPict='"99"', cGetPict='"99"', SpinnerLowValue= 0, SpinnerHighValue=50, Increment=1

  func oBARSPERSCALE_7_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SHOWSCALE)
    endwith
   endif
  endfunc

  func oBARSPERSCALE_7_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_BARSPERSCALE >= 0 and .w_BARSPERSCALE<= 50)
    endwith
    return bRes
  endfunc

  add object oMINNUMBERSCALELEGENDS_7_27 as StdSpinner with uid="BWBWSWUVKP",rtseq=32,rtrep=.f.,;
    cFormVar = "w_MINNUMBERSCALELEGENDS", cQueryName = "MINNUMBERSCALELEGENDS",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 134142811,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=368, Top=162, cSayPict='"99"', cGetPict='"99"', SpinnerLowValue= 0, SpinnerHighValue=50, Increment=1

  func oMINNUMBERSCALELEGENDS_7_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SHOWSCALE)
    endwith
   endif
  endfunc

  func oMINNUMBERSCALELEGENDS_7_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MINNUMBERSCALELEGENDS >= 0 and .w_MINNUMBERSCALELEGENDS <= 50)
    endwith
    return bRes
  endfunc

  add object oSCALEAUTOFORMAT_7_28 as StdCheck with uid="QRXFYUCYDK",rtseq=33,rtrep=.f.,left=199, top=185, caption="Automatic scale formatting",;
    HelpContextID = 256196858,;
    cFormVar="w_SCALEAUTOFORMAT", bObbl = .f. , nPag = 7;
   , bGlobalFont=.t.


  func oSCALEAUTOFORMAT_7_28.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oSCALEAUTOFORMAT_7_28.GetRadio()
    this.Parent.oContained.w_SCALEAUTOFORMAT = this.RadioValue()
    return .t.
  endfunc

  func oSCALEAUTOFORMAT_7_28.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_SCALEAUTOFORMAT==.T.,1,;
      0)
  endfunc

  func oSCALEAUTOFORMAT_7_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SHOWSCALE)
    endwith
   endif
  endfunc

  add object oStr_7_2 as StdString with uid="ENLPYIFNEC",Visible=.t., Left=7, Top=5,;
    Alignment=0, Width=183, Height=19,;
    Caption="Scales / Margin"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_7_3 as StdString with uid="NXCTUWOPGG",Visible=.t., Left=10, Top=30,;
    Alignment=0, Width=97, Height=18,;
    Caption="Global Margin"  ;
  , bGlobalFont=.t.

  add object oStr_7_5 as StdString with uid="FAAJQIPESX",Visible=.t., Left=10, Top=54,;
    Alignment=0, Width=97, Height=18,;
    Caption="Top Margin"  ;
  , bGlobalFont=.t.

  add object oStr_7_7 as StdString with uid="JHVLKXGIMH",Visible=.t., Left=10, Top=78,;
    Alignment=0, Width=97, Height=18,;
    Caption="Bottom Margin"  ;
  , bGlobalFont=.t.

  add object oStr_7_9 as StdString with uid="ALEHUDRQLZ",Visible=.t., Left=179, Top=54,;
    Alignment=0, Width=97, Height=18,;
    Caption="Left Margin"  ;
  , bGlobalFont=.t.

  add object oStr_7_11 as StdString with uid="KECWLDVSAV",Visible=.t., Left=179, Top=78,;
    Alignment=0, Width=97, Height=18,;
    Caption="Right Margin"  ;
  , bGlobalFont=.t.

  add object oStr_7_14 as StdString with uid="MWAOCYMWSP",Visible=.t., Left=14, Top=119,;
    Alignment=0, Width=97, Height=18,;
    Caption="Scale Value"  ;
  , bGlobalFont=.t.

  add object oStr_7_15 as StdString with uid="OSZDNBJGJF",Visible=.t., Left=14, Top=143,;
    Alignment=0, Width=97, Height=18,;
    Caption="Scale Min Value"  ;
  , bGlobalFont=.t.

  add object oStr_7_16 as StdString with uid="SFMFLPCOGV",Visible=.t., Left=14, Top=167,;
    Alignment=0, Width=97, Height=18,;
    Caption="Scale Max Value"  ;
  , bGlobalFont=.t.

  add object oStr_7_17 as StdString with uid="HWAHRUMUTF",Visible=.t., Left=14, Top=191,;
    Alignment=0, Width=97, Height=18,;
    Caption="Scale Divider"  ;
  , bGlobalFont=.t.

  add object oStr_7_24 as StdString with uid="LPMNROHVLW",Visible=.t., Left=199, Top=143,;
    Alignment=0, Width=145, Height=18,;
    Caption="Horizontal bars per legend"  ;
  , bGlobalFont=.t.

  add object oStr_7_25 as StdString with uid="URBPAUOKRY",Visible=.t., Left=199, Top=167,;
    Alignment=0, Width=156, Height=18,;
    Caption="Minimum Nr of Y Axis legend"  ;
  , bGlobalFont=.t.

  add object oBox_7_1 as StdBox with uid="SCLBSHHTOK",left=4, top=5, width=335,height=103

  add object oBox_7_29 as StdBox with uid="QXNHCVEDMT",left=4, top=113, width=419,height=103
enddefine
define class tcp_foxchartpropertiesPag8 as StdContainer
  Width  = 627
  height = 225
  stdWidth  = 627
  stdheight = 225
  resizeXpos=330
  resizeYpos=126
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object GraphData as ah_CBrowse with uid="GRCTWICVMZ",left=1, top=5, width=621,height=219,;
    caption='Browse',;
    ReadOnly=.f.,;
    nPag=8;
    , HelpContextID = 181390709;
  , bGlobalFont=.t.

enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_foxchartproperties','cpfcprop','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".code=cpfcprop.code";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- cp_foxchartproperties
* ---------------------------------------------------
*               Displayng Field
* ---------------------------------------------------
define class StdTableComboALias as StdTableCombo
  proc Init
    This.Clear
  	IF VARTYPE(this.bNoBackColor)='U'
		  This.backcolor = i_EBackColor
	  ENDIF
    this.ToolTipText=cp_Translate(this.ToolTipText)
    local l_numf, l_i
    DIMENSION this.combovalues(1)
    This.nValues = 1
    this.combovalues[1] = ' '
    this.AddItem(this.combovalues[1])
  endproc
  Proc Popola(pCurs)
    local l_count, l_i
    if used(pCurs)
      select (pCurs)
      AFIELDS(l_ArrField, pCurs)
      l_count = ALEN(l_ArrField , 1)
      this.nValues=l_count+1
      dimension this.combovalues[MAX(1,this.nValues)]
      If this.nValues<1 
       this.combovalues[1]=cp_NullValue(this.RadioValue())
      endif
      this.Clear   
      this.AddItem(' ')
      this.combovalues[1] = ' '
      l_i = 1
      do while l_i <= l_count
        this.AddItem(l_ArrField[l_i,1])
        this.combovalues[l_i+1]=trim(l_ArrField[l_i,1])
        l_i = l_i + 1
      enddo  
    endif  
  endproc
enddefine
* ---------------------------------------------------
*               Displayng Font
* ---------------------------------------------------
define class StdTableComboFont as StdTableCombo
  proc Init
  	IF VARTYPE(this.bNoBackColor)='U'
		  This.backcolor = i_EBackColor
	  ENDIF
    this.ToolTipText=cp_Translate(this.ToolTipText)
    local l_numf, l_i
    AFONT(this.combovalues)
    This.nValues = ALEN(this.combovalues)
    l_i = 1
    do while l_i <= This.nValues
        this.AddItem(this.combovalues[l_i])
        l_i = l_i + 1
    enddo
    This.SetFont()
  endproc
enddefine

* ---------------------------------------------------
*               Data class browse
* ---------------------------------------------------
DEFINE CLASS ah_CBrowse AS Grid

  cCursor=""

  * Standard
  RecordQUERYMODEL=""
  RecordQUERYMODELType=1   && ALIAS
  ColumnCount=0
  ReadOnly=.f.
  DeleteMark=.f.
  RecordSource=''
  RecordSourceType=1
  GridLineColor=RGB(192,192,192)
  BackColor=RGB(255,255,255)
  HighlightStyle=2

  proc Calculate(xValue)
  endproc

  proc Event(cEvent)
    if cEvent="Browse"
       this.Browse()
    endif
  endproc

  proc Browse()
		LOCAL loChart
		LOCAL lcAlias, lcField, n, lnCount
    with this
       .RecordSource=''
       .ColumnCount=0
       if !empty(.cCursor)
          .ColumnCount=fcount(.cCursor)
          .recordsource=.cCursor
          for nI=1 to .ColumnCount
	    cI=alltrim(str(nI))
	    oCol=.Columns(nI)
            oCol.Header1.Caption=field(nI,.cCursor)
   	    oCol.Bound=.f.
            oCol.ReadOnly=.ReadOnly
   	    oCol.ControlSource=alltrim(.cCursor)+'.'+field(nI,.cCursor)
   	    oCol.SelectOnEntry=.f.   	
	   next
       endif
    endwith
    Grid::Refresh()
    if !empty(.cCursor) and Used(.cCursor)
      Select(.cCursor)
      Go Top
    endif
  endproc

ENDDEFINE
* --- Fine Area Manuale
