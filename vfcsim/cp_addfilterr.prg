* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_addfilterr                                                   *
*              Gestione editor filtro                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-10-11                                                      *
* Last revis.: 2011-05-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
Private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
Createobject("tcp_addfilterr",oParentObject,m.pOPER)
Return(i_retval)

Define Class tcp_addfilterr As StdBatch
    * --- Local variables
    pOPER = Space(10)
    w_PADRE = .Null.
    w_TMPFILTER = Space(254)
    w_SETCHSEP = Space(10)
    w_POS_OR = 0
    w_POS_AND = 0
    w_SPLITPOS = 0
    w_SPLITSTR = Space(3)
    w_FILTERAT = Space(254)
    w_FL_FOUND = .F.
    w_OR_FOUND = .F.
    w_AND_FOUND = .F.
    w_IDFLTATT = 0
    w_IDATTOCC = 0
    w_IDOR_OCC = 0
    w_IDANDOCC = 0
    w_CONDPOS = 0
    w_CONDLEN = 0
    w_FLTFIELD = Space(30)
    w_FLTCOND = Space(10)
    w_FLTVALUE = Space(30)
    w_ORFLDNAM = Space(30)
    w_ORCONDIT = Space(10)
    w_ORVALFLT = Space(30)
    w_ORCONDAO = Space(3)
    w_DEFLDNAM = Space(30)
    w_DECONDIT = Space(10)
    w_DEVALFLT = Space(30)
    w_DECONDAO = Space(3)
    w_FLNAME = Space(30)
    w_FLTYPE = Space(1)
    w_FLCOMMEN = Space(80)
    w_FLLENGHT = 0
    w_FLDECIMA = 0
    * --- WorkFile variables
    TMPADVFILTER_idx=0
    runtime_filters = 1

    Procedure Pag1
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        This.w_PADRE = This.oParentObject
        Do Case
            Case This.pOPER=="NEW_RECORD"
                * --- Inizializzo tabella
                CP_CURTOTAB(This.oParentObject.w_FBCURNAM, "TMPADVFILTER", This)
                Use In Select(This.oParentObject.w_FBCURNAM)
                * --- Analisi campo filtro e trasformazione in righe dettaglio
                This.w_TMPFILTER = Alltrim(This.oParentObject.w_FBOLDEXP)
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
            Case This.pOPER=="FILTER_UP" Or This.pOPER=="FILTERDOWN"
                If (This.pOPER=="FILTER_UP" And This.w_PADRE.RowIndex()>1) Or ( This.pOPER=="FILTERDOWN" And This.w_PADRE.RowIndex()<This.w_PADRE.NumRow())
                    This.w_PADRE.SetRow()
                    * --- Memorizzo le informazioni della riga attuale
                    This.w_ORFLDNAM = This.oParentObject.w_FBFLDNAM
                    This.w_ORCONDIT = This.oParentObject.w_FBCONDIT
                    This.w_ORVALFLT = This.oParentObject.w_FBVALFLT
                    This.w_ORCONDAO = This.oParentObject.w_FBCONDAO
                    This.w_PADRE.MarkPos()
                    * --- Mi posiziono nella nuova riga (superiore o inferiore)
                    If This.pOPER=="FILTER_UP"
                        This.w_PADRE.PriorRow()
                    Else
                        This.w_PADRE.NextRow()
                    Endif
                    This.w_PADRE.SetRow()
                    * --- Memorizzo le informazioni della riga di destinazione
                    This.w_DEFLDNAM = This.oParentObject.w_FBFLDNAM
                    This.w_DECONDIT = This.oParentObject.w_FBCONDIT
                    This.w_DEVALFLT = This.oParentObject.w_FBVALFLT
                    This.w_DECONDAO = This.oParentObject.w_FBCONDAO
                    * --- Sovrascrivo la riga con i dati della riga di partenza
                    This.oParentObject.w_FBFLDNAM = This.w_ORFLDNAM
                    This.oParentObject.w_FBCONDIT = This.w_ORCONDIT
                    This.oParentObject.w_FBVALFLT = This.w_ORVALFLT
                    This.oParentObject.w_FBCONDAO = This.w_ORCONDAO
                    This.w_PADRE.SaveRow()
                    * --- Riposiziono nella riga di partenza
                    If This.pOPER=="FILTER_UP"
                        This.w_PADRE.NextRow()
                    Else
                        This.w_PADRE.PriorRow()
                    Endif
                    This.w_PADRE.SetRow()
                    * --- Sovrascrivo la riga con le informazioni prelevate dalla diga di destinazione
                    This.oParentObject.w_FBFLDNAM = This.w_DEFLDNAM
                    This.oParentObject.w_FBCONDIT = This.w_DECONDIT
                    This.oParentObject.w_FBVALFLT = This.w_DEVALFLT
                    This.oParentObject.w_FBCONDAO = This.w_DECONDAO
                    This.w_PADRE.SaveRow()
                    * --- Riposiziono sul filtro di partenza (adesso si trova nella riga di destinazione
                    If This.pOPER=="FILTER_UP"
                        This.w_PADRE.PriorRow()
                    Else
                        This.w_PADRE.NextRow()
                    Endif
                    This.w_PADRE.SetRow()
                Else
                    cp_ErrorMsg("Operazione non consentita")
                Endif
            Case This.pOPER=="CLOSE_MASK"
                * --- Drop temporary table TMPADVFILTER
                i_nIdx=cp_GetTableDefIdx('TMPADVFILTER')
                If i_nIdx<>0
                    cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
                    cp_RemoveTableDef('TMPADVFILTER')
                Endif
            Case This.pOPER=="SAVEANDCLS"
                This.Pag4()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                Local L_Macro
                L_Macro = "this.oParentObject.w_FBPARENT."+Alltrim(This.oParentObject.w_FBOBJECT)
                &L_Macro = This.w_TMPFILTER
                Release L_Macro
            Case This.pOPER=="FILTERTEST"
                This.w_PADRE.MarkPos()
                This.Pag4()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_FL_FOUND = .T.
                If Occurs( "(", This.w_TMPFILTER) <> Occurs( ")", This.w_TMPFILTER)
                    cp_ErrorMsg(cp_msgformat("Coerenza parentesi errata.%1Aperte %2%0Chiuse %3", Chr(13),Alltrim(Str(Occurs("(", This.w_TMPFILTER))), Alltrim(Str(Occurs( ")", This.w_TMPFILTER)))))
                    This.w_FL_FOUND = .F.
                Endif
                If This.w_FL_FOUND
                    * --- Verifico se esiste almeno una parentesi aperta o chiusa e se non ho riscontrato errori
                    Do While This.w_FL_FOUND And !Empty(This.w_TMPFILTER) And (Rat( "(", This.w_TMPFILTER)>0 Or At( ")", Substr(This.w_TMPFILTER,Rat( "(", This.w_TMPFILTER)+1)) >0)
                        * --- Se esistono una parentesi aperta e una chiusa e la chiusa non precede l'aperta allora continuo l'analisi
                        *     estraenda la sola parte interna alle parentesi incontrate in modo da proseguire l'analisi sulle sezioni via via pi� interne del filtro
                        This.w_FL_FOUND = !( ( Rat( "(", This.w_TMPFILTER) > 0 And At(")", Substr(This.w_TMPFILTER, Rat( "(", This.w_TMPFILTER)+1)) =0 ) Or ( Rat( "(", This.w_TMPFILTER) = 0 And At(")", This.w_TMPFILTER) >0 ))
                        If This.w_FL_FOUND
                            This.w_TMPFILTER = Substr( This.w_TMPFILTER , 1, Rat( "(", This.w_TMPFILTER)-1)+ Substr(Substr( This.w_TMPFILTER, Rat( "(", This.w_TMPFILTER)+1), At(")", Substr( This.w_TMPFILTER, Rat( "(", This.w_TMPFILTER)+1))+1)
                        Endif
                    Enddo
                    If !This.w_FL_FOUND
                        cp_ErrorMsg("Bilanciamento parentesi errato.%0Verificare l'espressione di filtro")
                    Endif
                Endif
                If This.w_FL_FOUND
                    * --- Ignoro l'ultima riga dove la condizione pu� essere vuota
                    This.w_PADRE.LastRow()
                    This.w_PADRE.PriorRow()
                    This.w_PADRE.SetRow()
                    Do While This.w_FL_FOUND And !This.w_PADRE.Bof_Trs()
                        This.w_FL_FOUND = !Empty(This.oParentObject.w_FBCONDAO)
                        This.w_PADRE.PriorRow()
                    Enddo
                    If !This.w_FL_FOUND
                        cp_ErrorMsg("Occorre specificare la condizione di and/or per tutte le righe del filtro")
                    Endif
                Endif
                This.w_PADRE.RePos()
                This.w_FL_FOUND = !This.w_FL_FOUND Or cp_ErrorMsg("Sintassi filtro corretta", 64)
            Case This.pOPER=="MAN2EDITOR"
                * --- Analisi campo filtro e trasformazione in righe dettaglio
                This.w_PADRE.MarkPos()
                This.w_TMPFILTER = Alltrim(This.oParentObject.w_FBMANFLT)
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_PADRE.oPgFrm.ActivePage = 1
                This.w_PADRE.RePos()
            Case This.pOPER=="EDITOR2MAN"
                This.Pag4()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.oParentObject.w_FBMANFLT = This.w_TMPFILTER
            Case This.pOPER=="ZOOMFIELDS"
                This.w_FLNAME = ""
                This.w_FLTYPE = ""
                This.w_FLCOMMEN = ""
                This.w_FLLENGHT = 0
                This.w_FLDECIMA = 0
                vx_exec("CP_ADDFILTER.VZM",This)
                If !Empty(Nvl(This.w_FLNAME, " "))
                    i_retcode = 'stop'
                    i_retval = This.w_FLNAME
                    Return
                Endif
        Endcase
    Endproc


    Procedure Pag2
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        * --- Caratteri separatori del filtro (' " () [] SPAZIO .)
        *     uno di questi caratteri deve trovarsi prima e dopo di un AND o di un OR
        *     per evitare falsi positivi di AND ed OR nei nomi dei campi/tabelle
        This.w_SETCHSEP = "()'[] ."+'"'
        This.w_PADRE.FirstRow()
        Do While !This.w_PADRE.Eof_Trs() And This.w_PADRE.FullRow()
            This.w_PADRE.SetRow()
            If This.w_PADRE.FullRow()
                This.w_PADRE.DeleteRow()
            Endif
            This.w_PADRE.FirstRow()
        Enddo
        Do While !Empty(This.w_TMPFILTER)
            This.w_POS_OR = 1
            This.w_POS_AND = 1
            This.w_IDOR_OCC = 1
            This.w_IDANDOCC = 1
            This.w_OR_FOUND = .F.
            This.w_AND_FOUND = .F.
            Do While !This.w_OR_FOUND And This.w_POS_OR>0
                This.w_POS_OR = Atc( " OR ", Upper(This.w_TMPFILTER), This.w_IDOR_OCC )
                * --- Verifico se prima e dopo l'espressione logica esiste il separatore
                *     Se l'or trovato � in posizione 1 sicuramente non � valido
                If This.w_POS_OR>1 And !( Substr(This.w_TMPFILTER, This.w_POS_OR, 1)$This.w_SETCHSEP And Substr(This.w_TMPFILTER, This.w_POS_OR+3, 1)$This.w_SETCHSEP)
                    This.w_POS_OR = 1
                    This.w_IDOR_OCC = This.w_IDOR_OCC + 1
                    This.w_OR_FOUND = .F.
                Else
                    This.w_OR_FOUND = .T.
                Endif
            Enddo
            Do While !This.w_AND_FOUND And This.w_POS_AND>0
                This.w_POS_AND = Atc( " AND ", Upper(This.w_TMPFILTER), This.w_IDANDOCC )
                * --- Verifico se prima e dopo l'espressione logica esiste il separatore
                If This.w_POS_AND>1 And !(Substr(This.w_TMPFILTER, This.w_POS_AND, 1)$This.w_SETCHSEP And Substr(This.w_TMPFILTER, This.w_POS_AND+4, 1)$This.w_SETCHSEP)
                    This.w_POS_AND = 1
                    This.w_IDANDOCC = This.w_IDANDOCC + 1
                    This.w_AND_FOUND = .F.
                Else
                    This.w_AND_FOUND = .T.
                Endif
            Enddo
            If This.w_OR_FOUND Or This.w_AND_FOUND
                * --- Determino quale separatore viene prima in modo da analizzare la
                *     sola prima parte di filtro
                Do Case
                    Case This.w_POS_OR>0 And This.w_POS_AND>0 And This.w_POS_OR<This.w_POS_AND
                        This.w_SPLITPOS = This.w_POS_OR
                        This.w_SPLITSTR = "OR"
                    Case This.w_POS_AND>0 And This.w_POS_OR>0 And This.w_POS_AND<This.w_POS_OR
                        This.w_SPLITPOS = This.w_POS_AND
                        This.w_SPLITSTR = "AND"
                    Case This.w_POS_OR>0 And This.w_POS_AND=0
                        This.w_SPLITPOS = This.w_POS_OR
                        This.w_SPLITSTR = "OR"
                    Case This.w_POS_AND>0 And This.w_POS_OR=0
                        This.w_SPLITPOS = This.w_POS_AND
                        This.w_SPLITSTR = "AND"
                    Otherwise
                        This.w_SPLITPOS = Len(This.w_TMPFILTER)
                        This.w_SPLITSTR = "AND"
                Endcase
            Endif
            * --- Analizzo la sezione del filtro per individuare: campo condizione e valore
            *     Per ogni filtro esiste una sola condizione, testo le varie condizioni
            *     e appena ne trovo una suddivido il filtro
            * --- Per testare condizioni tipo in, not in, like, ecc. occorre verificare che
            *     prima e dopo la parola vi sia uno spazio, non si pu� utilizzare solo la
            *     ATC poich� in caso di falsi positivi occorre analizzare un'eventuale
            *     occorrenza successiva. Le espressioni precedute dal not (Es. Not like)
            *     devono precedere le corrispondenti (Es. like) altrimenti in presenza di
            *     una not like verrebbe cercata (e trovata) prima la like e si avrebbe
            *     un'errata separazione del filtro. Per le altre espressioni non � obbligatorio
            *     lo spazio prima e dopo quindi le valuto in tal modo
            This.w_FILTERAT = Left(This.w_TMPFILTER, This.w_SPLITPOS )
            This.w_FL_FOUND = .F.
            Dimension L_ArrCond(13)
            L_ArrCond(1)="not like"
            L_ArrCond(2)="like"
            L_ArrCond(3)="not in"
            L_ArrCond(4)="in"
            L_ArrCond(5)="not exist"
            L_ArrCond(6)="exist"
            L_ArrCond(7)="between"
            L_ArrCond(8)="<>"
            L_ArrCond(9)=">="
            L_ArrCond(10)="<="
            L_ArrCond(11)="="
            L_ArrCond(12)=">"
            L_ArrCond(13)="<"
            This.w_IDFLTATT = 1
            Do While !This.w_FL_FOUND And This.w_IDFLTATT<Alen(L_ArrCond,1)
                * --- Testo tutte le espressioi finch� non le ho esaurite o non ho trovato l'occorrenza
                This.w_IDATTOCC = 1
                Do While !This.w_FL_FOUND And Atc( L_ArrCond( This.w_IDFLTATT), This.w_FILTERAT, This.w_IDATTOCC) > 0
                    * --- Se trovo l'occorrenza della condizione attuale verifico se non � un falso
                    *     positivo mediante ricerca degli spazi prima e dopo.
                    *     Se la trovo setto il flag a .T. per terminare le ricerche, altrimenti aumento
                    *     il numero dell'occorrenza da ricercare
                    If This.w_IDFLTATT<8
                        If Substr( This.w_FILTERAT, Atc( L_ArrCond( This.w_IDFLTATT), This.w_FILTERAT, This.w_IDATTOCC) -1, 1)=" " And Substr( This.w_FILTERAT, Atc( L_ArrCond( This.w_IDFLTATT), This.w_FILTERAT, This.w_IDATTOCC) +Len( L_ArrCond( This.w_IDFLTATT) ), 1)=" "
                            This.w_FL_FOUND = .T.
                        Else
                            This.w_IDATTOCC = This.w_IDATTOCC + 1
                        Endif
                    Else
                        This.w_FL_FOUND = .T.
                    Endif
                    If This.w_FL_FOUND
                        This.w_CONDPOS = Atc( L_ArrCond( This.w_IDFLTATT), This.w_FILTERAT, This.w_IDATTOCC)
                        This.w_CONDLEN = Len( L_ArrCond( This.w_IDFLTATT) )
                        This.w_FLTFIELD = Alltrim( Substr( This.w_FILTERAT, 1, This.w_CONDPOS - 1 ) )
                        This.w_FLTCOND = L_ArrCond( This.w_IDFLTATT)
                        This.w_FLTVALUE = Alltrim( Substr( This.w_FILTERAT, This.w_CONDPOS + Len( This.w_FLTCOND) ) )
                        This.Pag3()
                        If i_retcode='stop' Or !Empty(i_Error)
                            Return
                        Endif
                    Endif
                Enddo
                This.w_IDFLTATT = This.w_IDFLTATT + 1
            Enddo
            If !This.w_FL_FOUND
                * --- Non ho trovato nessun separatore valido, probabilmente � un espressione fox
                This.w_FLTFIELD = Alltrim( This.w_FILTERAT )
                This.w_FLTCOND = " "
                This.w_FLTVALUE = ""
                This.Pag3()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
            Endif
            This.w_TMPFILTER = Alltrim(Substr( This.w_TMPFILTER, Len(This.w_FILTERAT) + Len(This.w_SPLITSTR) + 1 ) )
        Enddo
    Endproc


    Procedure Pag3
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        This.w_PADRE.AddRow()
        This.oParentObject.w_FBFLDNAM = This.w_FLTFIELD
        This.oParentObject.w_FBCONDIT = This.w_FLTCOND
        This.oParentObject.w_FBVALFLT = This.w_FLTVALUE
        This.oParentObject.w_FBCONDAO = This.w_SPLITSTR
        This.w_PADRE.SaveRow()
    Endproc


    Procedure Pag4
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        This.w_TMPFILTER = ""
        This.w_PADRE.FirstRow()
        Do While !This.w_PADRE.Eof_Trs()
            This.w_PADRE.SetRow()
            If This.w_PADRE.FullRow()
                This.w_TMPFILTER = This.w_TMPFILTER + " " + Alltrim( This.oParentObject.w_FBFLDNAM) + " " + Iif(Empty(Alltrim( This.oParentObject.w_FBCONDIT)), "" , Alltrim( This.oParentObject.w_FBCONDIT) + " " + Alltrim( This.oParentObject.w_FBVALFLT) + " " ) + Alltrim( This.oParentObject.w_FBCONDAO)
                This.w_SPLITSTR = Alltrim( This.oParentObject.w_FBCONDAO)
            Endif
            This.w_PADRE.NextRow()
        Enddo
        * --- Elimino l'ultima condizione AND/OR che per l'ultimo filtro � innutile e darebbe errore in esecuzione
        This.w_TMPFILTER = Alltrim(Left(This.w_TMPFILTER, Len(This.w_TMPFILTER)- Len(This.w_SPLITSTR) ))
    Endproc


    Proc Init(oParentObject,pOPER)
        This.pOPER=pOPER
        DoDefault(oParentObject)
        Return
    Function OpenTables()
        Dimension This.cWorkTables[max(1,1)]
        This.cWorkTables[1]='*TMPADVFILTER'
        Return(This.OpenAllTables(1))

        * --- Area Manuale = Functions & Procedures
        * --- Fine Area Manuale
Enddefine

Procedure getEntityType(result)
    result="Routine"
Endproc
Procedure getEntityParmList(result)
    result="pOPER"
Endproc
