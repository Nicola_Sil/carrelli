* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cplangs                                                         *
*              Tabelle lingue                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-16                                                      *
* Last revis.: 2011-04-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
Return(Createobject("tcplangs"))

* --- Class definition
Define Class tcplangs As StdForm
    Top    = 10
    Left   = 10

    * --- Standard Properties
    Width  = 360
    Height = 110+35
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2011-04-18"
    HelpContextID=165970787
    max_rt_seq=4

    * --- Constant Properties
    cplangs_IDX = 0
    cFile = "cplangs"
    cKeySelect = "code"
    cKeyWhere  = "code=this.w_code"
    cKeyWhereODBC = '"code="+cp_ToStrODBC(this.w_code)';

    cKeyWhereODBCqualified = '"cplangs.code="+cp_ToStrODBC(this.w_code)';

    cPrg = "cplangs"
    cComment = "Tabelle lingue"
    Icon = "anag.ico"
    * --- Area Manuale = Properties
    * --- Fine Area Manuale

    * --- Local Variables
    w_code = Space(3)
    w_name = Space(30)
    w_ptranslation = Space(1)
    w_datalanguage = Space(1)
    * --- Area Manuale = Declare Variables
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPageFrame With PageCount=2, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        cp_AddExtFldsTab(This,'cplangs','cplangs')
        StdPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tcplangsPag1","cplangs",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate("Lingua")
            .Pages(1).HelpContextID = 7366719
        Endwith
        This.Parent.oFirstControl = This.Page1.oPag.ocode_1_2
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
        Local qf
        QueryFilter(@qf)
        This.Parent.cQueryFilter=qf
        * --- Area Manuale = Init Page Frame
        * --- cplangs
        * --- Translate interface...
        This.Parent.cComment=cp_Translate(MSG_LANGUAGES)
        This.Pages(1).Caption=cp_Translate(MSG_LANGUAGE)
        * --- Fine Area Manuale
        This.Parent.Resize()
    Endproc

    * --- Opening Tables
    Function OpenWorkTables()
        Dimension This.cWorkTables[1]
        This.cWorkTables[1]='cplangs'
        * --- Area Manuale = Open Work Table
        * --- Fine Area Manuale
        Return(This.OpenAllTables(1))

    Procedure SetPostItConn()
        This.bPostIt=i_ServerConn[i_TableProp[this.cplangs_IDX,5],7]
        This.nPostItConn=i_TableProp[this.cplangs_IDX,3]
        Return

    Procedure SetWorkFromKeySet()
        * --- Initialize work variables from KeySet. They will be used to load the record
        Select (This.cKeySet)
        With This
            .w_code = Nvl(Code,Space(3))
        Endwith
    Endproc

    * --- Read record and initialize Form variables
    Procedure LoadRec()
        Local i_cKey,i_nRes,i_cTable,i_nConn
        Local i_cSel,i_cDatabaseType,i_nFlds
        * --- Area Manuale = Load Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        This.bUpdated=.F.
        * --- The following Select reads the record
        *
        * select * from cplangs where code=KeySet.code
        *
        i_nConn = i_TableProp[this.cplangs_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.cplangs_IDX,2])
        If i_nConn<>0
            i_nFlds = i_dcx.getfieldscount('cplangs')
            i_cDatabaseType = cp_GetDatabaseType(i_nConn)
            i_cSel = "cplangs.*"
            i_cKey = This.cKeyWhereODBCqualified
            i_cTable = i_cTable+' cplangs '
            i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,This.cCursor)
            This.bLoaded = i_nRes<>-1 And Not(Eof())
        Else
            * i_cKey = this.cKeyWhere
            i_cKey = cp_PKFox(i_cTable  ,'code',This.w_code  )
            Select * From (i_cTable) Where &i_cKey Into Cursor (This.cCursor) nofilter
            This.bLoaded = Not(Eof())
        Endif
        * --- Copy values in work variables
        If This.bLoaded
            With This
                .w_code = Nvl(Code,Space(3))
                .w_name = Nvl(Name,Space(30))
                .w_ptranslation = Nvl(ptranslation,Space(1))
                .w_datalanguage = Nvl(datalanguage,Space(1))
                .oPgFrm.Page1.oPag.oObj_1_7.Calculate(MSG_TRANSLATIONS_TOOLTIP_CODE)
                .oPgFrm.Page1.oPag.oObj_1_8.Calculate(MSG_LANGUAGE+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_DESCRIPTION)
                .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_DESCRIPTION+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_TRANSLATIONS_TOOLTIP_ACTIVE)
                .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_TRANSLATIONS_DATA)
                cp_LoadRecExtFlds(This,'cplangs')
            Endwith
            This.SaveDependsOn()
            This.SetControlsValue()
            This.mHideControls()
            This.ChildrenChangeRow()
            This.NotifyEvent('Load')
        Else
            This.BlankRec()
        Endif
        * --- Area Manuale = Load Record End
        * --- Fine Area Manuale
    Endproc

    * --- Blank form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        With This
            .w_code = Space(3)
            .w_name = Space(30)
            .w_ptranslation = Space(1)
            .w_datalanguage = Space(1)
            If .cFunction<>"Filter"
                .oPgFrm.Page1.oPag.oObj_1_7.Calculate(MSG_TRANSLATIONS_TOOLTIP_CODE)
                .oPgFrm.Page1.oPag.oObj_1_8.Calculate(MSG_LANGUAGE+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_DESCRIPTION)
                .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_DESCRIPTION+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_TRANSLATIONS_TOOLTIP_ACTIVE)
                .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_TRANSLATIONS_DATA)
            Endif
        Endwith
        cp_BlankRecExtFlds(This,'cplangs')
        This.DoRTCalc(1,4,.F.)
        This.SaveDependsOn()
        This.SetControlsValue()
        This.mHideControls()
        This.ChildrenChangeRow()
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = setting operation
    *     Allowed parameters  : Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        Local i_bVal
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        i_bVal = i_cOp<>"Query"
        * --- Disable List page when <> from Query
        If Type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
            This.oPgFrm.Pages(This.oPgFrm.PageCount).Enabled=Not(i_bVal)
        Else
            Local i_nPageCount
            For i_nPageCount=This.oPgFrm.PageCount To 1 Step -1
                If Type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
                    This.oPgFrm.Pages(i_nPageCount).Enabled=Not(i_bVal)
                    Exit
                Endif
            Next
        Endif
        With This.oPgFrm
            .Page1.oPag.ocode_1_2.Enabled = i_bVal
            .Page1.oPag.oname_1_4.Enabled = i_bVal
            .Page1.oPag.optranslation_1_5.Enabled = i_bVal
            .Page1.oPag.odatalanguage_1_6.Enabled = i_bVal
            .Page1.oPag.oObj_1_7.Enabled = i_bVal
            .Page1.oPag.oObj_1_8.Enabled = i_bVal
            .Page1.oPag.oObj_1_9.Enabled = i_bVal
            .Page1.oPag.oObj_1_10.Enabled = i_bVal
            .Page1.oPag.oObj_1_11.Enabled = i_bVal
            .Page1.oPag.oObj_1_12.Enabled = i_bVal
            If i_cOp = "Edit"
                .Page1.oPag.ocode_1_2.Enabled = .F.
            Endif
            If i_cOp = "Query"
                .Page1.oPag.ocode_1_2.Enabled = .T.
            Endif
        Endwith
        cp_SetEnabledExtFlds(This,'cplangs',i_cOp)
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt,i_nConn
        i_nConn = i_TableProp[this.cplangs_IDX,3]
        i_cFlt = This.cQueryFilter
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_code,"code",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_name,"name",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_ptranslation,"ptranslation",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_datalanguage,"datalanguage",i_nConn)
        * --- Area Manuale = Build Filter
        * --- Fine Area Manuale
        Return (i_cFlt)
    Endfunc

    * --- Create cursor cKeySet with primary key only
    Proc QueryKeySet(i_cWhere,i_cOrderBy)
        Local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
        This.cLastWhere = i_cWhere
        This.cLastOrderBy = i_cOrderBy
        i_cWhere = Iif(Not(Empty(i_cWhere)),' where '+i_cWhere,'')
        i_cOrderBy = Iif(Not(Empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
        i_cKey = This.cKeySelect
        * --- Area Manuale = Query Key Set Init
        * --- Fine Area Manuale
        i_nConn = i_TableProp[this.cplangs_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.cplangs_IDX,2])
        i_lTable = "cplangs"
        i_cDatabaseType = i_ServerConn[i_TableProp[this.cplangs_IDX,5],6]
        If i_nConn<>0
            Local i_oldzr
            If Vartype(i_nZoomMaxRows)='N'
                i_oldzr=i_nZoomMaxRows
                i_nZoomMaxRows=10000
            Endif
            If !Empty(i_cOrderBy) And At(Substr(i_cOrderBy,11),i_cKey)=0
                i_cKey=i_cKey+','+Substr(i_cOrderBy,11)
            Endif
            Thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,This.cKeySet,i_cDatabaseType)
            If Vartype(i_nZoomMaxRows)='N'
                i_nZoomMaxRows=i_oldzr
            Endif
        Else
            Select &i_cKey From (i_cTable) As (i_lTable) &i_cWhere &i_cOrderBy Into Cursor (This.cKeySet)
            Locate For 1=1 && per il problema dei cursori che riusano il file principale
        Endif
        * --- Area Manuale = Query Key Set End
        * --- Fine Area Manuale
    Endproc

    * --- Insert of new Record
    Function mInsert()
        Local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
        * --- Area Manuale = Insert Init
        * --- Fine Area Manuale
        If This.bUpdated .Or. This.IsAChildUpdated()
            i_nConn = i_TableProp[this.cplangs_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.cplangs_IDX,2])
            i_ccchkf=''
            i_ccchkv=''
            This.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,This.cplangs_IDX,i_nConn)
            * --- Area Manuale = Autonum Assigned
            * --- Fine Area Manuale
            *
            * insert into cplangs
            *
            This.NotifyEvent('Insert start')
            This.mUpdateTrs()
            If i_nConn<>0
                i_extfld=cp_InsertFldODBCExtFlds(This,'cplangs')
                i_extval=cp_InsertValODBCExtFlds(This,'cplangs')
                i_nnn="INSERT INTO "+i_cTable+" "+;
                    "(code,name,ptranslation,datalanguage "+i_extfld+i_ccchkf+") VALUES ("+;
                    cp_ToStrODBC(This.w_code)+;
                    ","+cp_ToStrODBC(This.w_name)+;
                    ","+cp_ToStrODBC(This.w_ptranslation)+;
                    ","+cp_ToStrODBC(This.w_datalanguage)+;
                    i_extval+i_ccchkv+")"
                =cp_TrsSQL(i_nConn,i_nnn)
            Else
                i_extfld=cp_InsertFldVFPExtFlds(This,'cplangs')
                i_extval=cp_InsertValVFPExtFlds(This,'cplangs')
                cp_CheckDeletedKey(i_cTable,0,'code',This.w_code)
                Insert Into (i_cTable);
                    (Code,Name,ptranslation,datalanguage  &i_extfld. &i_ccchkf.) Values (;
                    this.w_code;
                    ,This.w_name;
                    ,This.w_ptranslation;
                    ,This.w_datalanguage;
                    &i_extval. &i_ccchkv.)
            Endif
            This.NotifyEvent('Insert end')
        Endif
        * --- Area Manuale = Insert End
        * --- Fine Area Manuale
        Return(Not(bTrsErr))

        * --- Update Database
    Function mReplace(i_bEditing)
        Local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        If This.bUpdated And i_bEditing
            This.mRestoreTrs(.T.)
            This.mUpdateTrs(.T.)
        Endif
        If This.bUpdated And i_bEditing
            i_nConn = i_TableProp[this.cplangs_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.cplangs_IDX,2])
            i_ccchkf=''
            i_ccchkw=''
            This.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,This.cplangs_IDX,i_nConn)
            *
            * update cplangs
            *
            This.NotifyEvent('Update start')
            If i_nConn<>0
                i_cWhere = This.cKeyWhereODBC
                i_extfld=cp_ReplaceODBCExtFlds(This,'cplangs')
                i_nnn="UPDATE "+i_cTable+" SET"+;
                    " name="+cp_ToStrODBC(This.w_name)+;
                    ",ptranslation="+cp_ToStrODBC(This.w_ptranslation)+;
                    ",datalanguage="+cp_ToStrODBC(This.w_datalanguage)+;
                    i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            Else
                i_extfld=cp_ReplaceVFPExtFlds(This,'cplangs')
                i_cWhere = cp_PKFox(i_cTable  ,'code',This.w_code  )
                Update (i_cTable) Set;
                    name=This.w_name;
                    ,ptranslation=This.w_ptranslation;
                    ,datalanguage=This.w_datalanguage;
                    &i_extfld. &i_ccchkf. Where &i_cWhere. &i_ccchkw.
                i_nModRow=_Tally
            Endif
            =cp_CheckMultiuser(i_nModRow)
            This.NotifyEvent('Update end')
        Endif
        If Not(bTrsErr)
        Endif
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(bTrsErr)

        * --- Delete Records
    Function mDelete()
        Local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
        * --- Area Manuale = Delete Init
        * --- Fine Area Manuale
        If Not(bTrsErr)
            i_nConn = i_TableProp[this.cplangs_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.cplangs_IDX,2])
            i_ccchkw=''
            This.SetCCCHKVarDelete(@i_ccchkw,This.cplangs_IDX,i_nConn)
            *
            * delete cplangs
            *
            This.NotifyEvent('Delete start')
            If i_nConn<>0
                i_cWhere = This.cKeyWhereODBC
                i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                    " WHERE "+&i_cWhere+i_ccchkw)
            Else
                * i_cWhere = this.cKeyWhere
                i_cWhere = cp_PKFox(i_cTable  ,'code',This.w_code  )
                Delete From (i_cTable) Where &i_cWhere. &i_ccchkw.
                i_nModRow=_Tally
            Endif
            =cp_CheckMultiuser(i_nModRow)
            If Not(bTrsErr)
                This.mRestoreTrs()
            Endif
            This.NotifyEvent('Delete end')
        Endif
        This.mDeleteWarnings()
        * --- Area Manuale = Delete End
        * --- Fine Area Manuale
        Return

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        Local i_cTable,i_nConn
        i_nConn = i_TableProp[this.cplangs_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.cplangs_IDX,2])
        If i_bUpd
            With This
                .oPgFrm.Page1.oPag.oObj_1_7.Calculate(MSG_TRANSLATIONS_TOOLTIP_CODE)
                .oPgFrm.Page1.oPag.oObj_1_8.Calculate(MSG_LANGUAGE+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_DESCRIPTION)
                .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_DESCRIPTION+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_TRANSLATIONS_TOOLTIP_ACTIVE)
                .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_TRANSLATIONS_DATA)
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
            Endwith
            This.DoRTCalc(1,4,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
            .oPgFrm.Page1.oPag.oObj_1_7.Calculate(MSG_TRANSLATIONS_TOOLTIP_CODE)
            .oPgFrm.Page1.oPag.oObj_1_8.Calculate(MSG_LANGUAGE+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_DESCRIPTION)
            .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_DESCRIPTION+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_TRANSLATIONS_TOOLTIP_ACTIVE)
            .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_TRANSLATIONS_DATA)
        Endwith
        Return


        * --- Enable controls under condition
    Procedure mEnableControls()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        This.oPgFrm.Page1.oPag.odatalanguage_1_6.Visible=!This.oPgFrm.Page1.oPag.odatalanguage_1_6.mHide()
        DoDefault()
        Return

        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
        Endwith
        * --- Area Manuale = Notify Event End
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

    Function SetControlsValue()
        If Not(This.oPgFrm.Page1.oPag.ocode_1_2.Value==This.w_code)
            This.oPgFrm.Page1.oPag.ocode_1_2.Value=This.w_code
        Endif
        If Not(This.oPgFrm.Page1.oPag.oname_1_4.Value==This.w_name)
            This.oPgFrm.Page1.oPag.oname_1_4.Value=This.w_name
        Endif
        If Not(This.oPgFrm.Page1.oPag.optranslation_1_5.RadioValue()==This.w_ptranslation)
            This.oPgFrm.Page1.oPag.optranslation_1_5.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.odatalanguage_1_6.RadioValue()==This.w_datalanguage)
            This.oPgFrm.Page1.oPag.odatalanguage_1_6.SetRadio()
        Endif
        cp_SetControlsValueExtFlds(This,'cplangs')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
            Else
                If Not(i_bnoChk)
                    cp_ErrorMsg(i_cErrorMsg)
                Endif
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

Enddefine

* --- Define pages as container
Define Class tcplangsPag1 As StdContainer
    Width  = 356
    Height = 110
    stdWidth  = 356
    stdheight = 110
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.

    Add Object ocode_1_2 As StdField With uid="JDPNPMKTMI",rtseq=1,rtrep=.F.,;
        cFormVar = "w_code", cQueryName = "code",;
        bObbl = .F. , nPag = 1, Value=Space(3), bMultilanguage =  .F.,;
        HelpContextID = 166411958,;
        bGlobalFont=.T.,;
        Height=21, Width=41, Left=119, Top=12, InputMask=Replicate('X',3)

    Add Object oname_1_4 As StdField With uid="EUUQBJLLBI",rtseq=2,rtrep=.F.,;
        cFormVar = "w_name", cQueryName = "name",;
        bObbl = .F. , nPag = 1, Value=Space(30), bMultilanguage =  .F.,;
        HelpContextID = 166414049,;
        bGlobalFont=.T.,;
        Height=21, Width=223, Left=119, Top=36, InputMask=Replicate('X',30)

    Add Object optranslation_1_5 As StdCheck With uid="HONDRAYGLV",rtseq=3,rtrep=.F.,Left=119, Top=60, Caption="Abilita traduzione stampe a runtime",;
        ToolTipText = "Abilita traduzione stampe a runtime",;
        HelpContextID = 40869097,;
        cFormVar="w_ptranslation", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func optranslation_1_5.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func optranslation_1_5.GetRadio()
        This.Parent.oContained.w_ptranslation = This.RadioValue()
        Return .T.
    Endfunc

    Func optranslation_1_5.SetRadio()
        This.Parent.oContained.w_ptranslation=Trim(This.Parent.oContained.w_ptranslation)
        This.Value = ;
            iif(This.Parent.oContained.w_ptranslation=='S',1,;
            0)
    Endfunc

    Add Object odatalanguage_1_6 As StdCheck With uid="OCJOUXTUFG",rtseq=4,rtrep=.F.,Left=119, Top=82, Caption="Traduzione del dato",;
        HelpContextID = 242076508,;
        cFormVar="w_datalanguage", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func odatalanguage_1_6.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func odatalanguage_1_6.GetRadio()
        This.Parent.oContained.w_datalanguage = This.RadioValue()
        Return .T.
    Endfunc

    Func odatalanguage_1_6.SetRadio()
        This.Parent.oContained.w_datalanguage=Trim(This.Parent.oContained.w_datalanguage)
        This.Value = ;
            iif(This.Parent.oContained.w_datalanguage=='S',1,;
            0)
    Endfunc

    Func odatalanguage_1_6.mHide()
        With This.Parent.oContained
            Return (Empty(CP_DBTYPE) Or CP_DBTYPE='VFP')
        Endwith
    Endfunc

    Func odatalanguage_1_6.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            If bRes And !(.w_datalanguage<>'S')
                bRes=(Messagebox(cp_Translate(Thisform.msgFmt(""+MSG_TRANSLATIONS_WARNING_ACTIVE+""))+". "+cp_Translate(MSG_ACCEPT_ANYWAY),4+32,"")=6)
                This.Parent.oContained.bDontReportError=!bRes
            Endif
        Endwith
        Return bRes
    Endfunc


    Add Object oObj_1_7 As cp_setobjprop With uid="SKPUZOAYUU",Left=3, Top=163, Width=321,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_code",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 26100690;
        , bGlobalFont=.T.



    Add Object oObj_1_8 As cp_setCtrlObjprop With uid="WTFHXDANKA",Left=3, Top=189, Width=321,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Lingua:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 26100690;
        , bGlobalFont=.T.



    Add Object oObj_1_9 As cp_setobjprop With uid="PVHCXHZQRC",Left=3, Top=233, Width=321,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_name",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 26100690;
        , bGlobalFont=.T.



    Add Object oObj_1_10 As cp_setCtrlObjprop With uid="EYUZRJSFMA",Left=3, Top=259, Width=321,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Descrizione:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 26100690;
        , bGlobalFont=.T.



    Add Object oObj_1_11 As cp_setobjprop With uid="JIYJTKVSZC",Left=3, Top=302, Width=321,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_datalanguage",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 26100690;
        , bGlobalFont=.T.



    Add Object oObj_1_12 As cp_setobjprop With uid="ZDVHIOYSCI",Left=3, Top=328, Width=321,Height=26,;
        caption='Object',;
        cProp="caption",cObj="w_datalanguage",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 26100690;
        , bGlobalFont=.T.


    Add Object oStr_1_1 As StdString With uid="DKKCRKJQJS",Visible=.T., Left=0, Top=12,;
        Alignment=1, Width=116, Height=18,;
        Caption="Lingua:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_3 As StdString With uid="AWKJMMDCSE",Visible=.T., Left=-17, Top=36,;
        Alignment=1, Width=133, Height=18,;
        Caption="Descrizione:"  ;
        , bGlobalFont=.T.
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cplangs','cplangs','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +" "+i_cAliasName2+".code=cplangs.code";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Master File"
Endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
