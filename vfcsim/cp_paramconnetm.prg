* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_paramconnetm                                                 *
*              Riconnessione Automatica                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-11-02                                                      *
* Last revis.: 2009-11-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
Local i_Obj
If Vartype(m.oParentObject)<>'O'
    Wait Wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
    Return(.Null.)
Endif
If i_cType='edit'
    Return(.Null.)
Else
    If i_cType='paint'
        i_Obj=Createobject("tcp_paramconnetm")
        i_Obj.Cnt.oParentObject=oParentObject
        i_Obj.LinkPCClick()
    Else
        i_Obj=Createobject('stdLazyChild',oParentObject,Program())
    Endif
Endif
Return(i_Obj)

Proc _CreateObject(retobj)
    retobj=Createobject("tccp_paramconnetm")
    Return

Proc _AddObject(i_oCnt,i_name)
    i_oCnt.AddObject(i_name,"tccp_paramconnetm")
    Return

    * --- Class definition
Define Class tcp_paramconnetm As StdPCForm
    Width  = 469
    Height = 254
    Top    = 5
    Left   = 36
    cComment = "Riconnessione Automatica"
    cPrg = "cp_paramconnetm"
    HelpContextID=46875722
    Add Object Cnt As tccp_paramconnetm
Enddefine

Define Class tscp_paramconnetm As PCContext
    w_RCSERIAL = Space(5)
    w_RCAUTORC = Space(1)
    w_RCNUMRIC = 0
    w_RCDELAYR = 0
    w_RCCNTIME = 0
    w_RCDEADRQ = Space(1)
    w_RCNUMDRQ = 0
    w_RCDELAYD = 0
    w_RCCONMSG = Space(1)
    Proc Save(oFrom)
        This.w_RCSERIAL = oFrom.w_RCSERIAL
        This.w_RCAUTORC = oFrom.w_RCAUTORC
        This.w_RCNUMRIC = oFrom.w_RCNUMRIC
        This.w_RCDELAYR = oFrom.w_RCDELAYR
        This.w_RCCNTIME = oFrom.w_RCCNTIME
        This.w_RCDEADRQ = oFrom.w_RCDEADRQ
        This.w_RCNUMDRQ = oFrom.w_RCNUMDRQ
        This.w_RCDELAYD = oFrom.w_RCDELAYD
        This.w_RCCONMSG = oFrom.w_RCCONMSG
        PCContext::Save(oFrom)
    Proc Load(oTo)
        oTo.w_RCSERIAL = This.w_RCSERIAL
        oTo.w_RCAUTORC = This.w_RCAUTORC
        oTo.w_RCNUMRIC = This.w_RCNUMRIC
        oTo.w_RCDELAYR = This.w_RCDELAYR
        oTo.w_RCCNTIME = This.w_RCCNTIME
        oTo.w_RCDEADRQ = This.w_RCDEADRQ
        oTo.w_RCNUMDRQ = This.w_RCNUMDRQ
        oTo.w_RCDELAYD = This.w_RCDELAYD
        oTo.w_RCCONMSG = This.w_RCCONMSG
        PCContext::Load(oTo)
Enddefine

Define Class tccp_paramconnetm As StdPCContainer
    Top    = 0
    Left   = 0

    * --- Standard Properties
    Width  = 469
    Height = 254
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2009-11-02"
    HelpContextID=46875722
    max_rt_seq=9

    * --- Constant Properties
    paramric_IDX = 0
    cFile = "paramric"
    cKeySelect = "RCSERIAL"
    cKeyWhere  = "RCSERIAL=this.w_RCSERIAL"
    cKeyWhereODBC = '"RCSERIAL="+cp_ToStrODBC(this.w_RCSERIAL)';

    cKeyWhereODBCqualified = '"paramric.RCSERIAL="+cp_ToStrODBC(this.w_RCSERIAL)';

    cPrg = "cp_paramconnetm"
    cComment = "Riconnessione Automatica"
    Icon = "anag.ico"
    * --- Area Manuale = Properties
    * --- Fine Area Manuale

    * --- Local Variables
    w_RCSERIAL = Space(5)
    w_RCAUTORC = Space(1)
    w_RCNUMRIC = 0
    w_RCDELAYR = 0
    w_RCCNTIME = 0
    w_RCDEADRQ = Space(1)
    w_RCNUMDRQ = 0
    w_RCDELAYD = 0
    w_RCCONMSG = Space(1)
    * --- Area Manuale = Declare Variables
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=1, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tcp_paramconnetmPag1","cp_paramconnetm",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate("Pag.1")
            .Pages(1).HelpContextID = 43222762
        Endwith
        This.Parent.oFirstControl = This.Page1.oPag.oRCAUTORC_1_2
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
        * --- Area Manuale = Init Page Frame
        * --- Fine Area Manuale
    Endproc

    * --- Opening Tables
    Function OpenWorkTables()
        Dimension This.cWorkTables[1]
        This.cWorkTables[1]='paramric'
        * --- Area Manuale = Open Work Table
        * --- Fine Area Manuale
        Return(This.OpenAllTables(1))

    Procedure SetPostItConn()
        This.bPostIt=i_ServerConn[i_TableProp[this.paramric_IDX,5],7]
        This.nPostItConn=i_TableProp[this.paramric_IDX,3]
        Return

    Procedure NewContext()
        Return(Createobject('tscp_paramconnetm'))


        * --- Read record and initialize Form variables
    Procedure LoadRec()
        Local i_cKey,i_nRes,i_cTable,i_nConn
        Local i_cSel,i_cDatabaseType,i_nFlds
        * --- Area Manuale = Load Record Init
        * --- Fine Area Manuale
        If Not(This.bOnScreen)
            This.nDeferredFillRec=1
            Return
        Endif
        If This.cFunction='Load'
            This.BlankRec()
            Return
        Endif
        This.nDeferredFillRec=0
        This.bF10=.F.
        This.bUpdated=.F.
        * --- The following Select reads the record
        *
        * select * from paramric where RCSERIAL=KeySet.RCSERIAL
        *
        i_nConn = i_TableProp[this.paramric_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.paramric_IDX,2])
        If i_nConn<>0
            i_nFlds = i_dcx.getfieldscount('paramric')
            i_cDatabaseType = cp_GetDatabaseType(i_nConn)
            i_cSel = "paramric.*"
            i_cKey = This.cKeyWhereODBCqualified
            i_cTable = i_cTable+' paramric '
            i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,This.cCursor)
            This.bLoaded = i_nRes<>-1 And Not(Eof())
        Else
            * i_cKey = this.cKeyWhere
            i_cKey = cp_PKFox(i_cTable  ,'RCSERIAL',This.w_RCSERIAL  )
            Select * From (i_cTable) Where &i_cKey Into Cursor (This.cCursor) nofilter
            This.bLoaded = Not(Eof())
        Endif
        * --- Copy values in work variables
        If This.bLoaded
            With This
                .w_RCSERIAL = Nvl(RCSERIAL,Space(5))
                .w_RCAUTORC = Nvl(RCAUTORC,Space(1))
                .w_RCNUMRIC = Nvl(RCNUMRIC,0)
                .w_RCDELAYR = Nvl(RCDELAYR,0)
                .w_RCCNTIME = Nvl(RCCNTIME,0)
                .w_RCDEADRQ = Nvl(RCDEADRQ,Space(1))
                .w_RCNUMDRQ = Nvl(RCNUMDRQ,0)
                .w_RCDELAYD = Nvl(RCDELAYD,0)
                .w_RCCONMSG = Nvl(RCCONMSG,Space(1))
                .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_AUTORICONNECT)
                .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_NUMRICONNECT)
                .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_DELAYRICONNECT)
                .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_CONNECTTIMEOUT)
                .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_DEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_NUMDEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_DELAYDEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_CNF_MSG)
                .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_SHOW_NUMRICONNECT)
                .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_SHOW_DELAYRICONNECT)
                .oPgFrm.Page1.oPag.oObj_1_26.Calculate(MSG_SHOW_CONNECTTIMEOUT)
                .oPgFrm.Page1.oPag.oObj_1_27.Calculate(MSG_SHOW_DEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_28.Calculate(MSG_SHOW_NUMDEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_29.Calculate(MSG_SHOW_DELAYDEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_SHOW_MSG)
                .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_SHOW_AUTORICONNECT)
                cp_LoadRecExtFlds(This,'paramric')
            Endwith
            This.SaveDependsOn()
            This.SetControlsValue()
            This.mHideControls()
            This.ChildrenChangeRow()
            This.NotifyEvent('Load')
        Else
            This.BlankRec()
        Endif
        * --- Area Manuale = Load Record End
        * --- Fine Area Manuale
    Endproc

    * --- Blank form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        If Not(This.bOnScreen)
            This.nDeferredFillRec=2
            Return
        Endif
        This.nDeferredFillRec=0
        This.bUpdated=.F.
        This.bLoaded=.F.
        With This
            .w_RCSERIAL = Space(5)
            .w_RCAUTORC = Space(1)
            .w_RCNUMRIC = 0
            .w_RCDELAYR = 0
            .w_RCCNTIME = 0
            .w_RCDEADRQ = Space(1)
            .w_RCNUMDRQ = 0
            .w_RCDELAYD = 0
            .w_RCCONMSG = Space(1)
            If .cFunction<>"Filter"
                .DoRTCalc(1,8,.F.)
                .w_RCCONMSG = 'N'
                .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_AUTORICONNECT)
                .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_NUMRICONNECT)
                .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_DELAYRICONNECT)
                .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_CONNECTTIMEOUT)
                .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_DEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_NUMDEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_DELAYDEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_CNF_MSG)
                .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_SHOW_NUMRICONNECT)
                .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_SHOW_DELAYRICONNECT)
                .oPgFrm.Page1.oPag.oObj_1_26.Calculate(MSG_SHOW_CONNECTTIMEOUT)
                .oPgFrm.Page1.oPag.oObj_1_27.Calculate(MSG_SHOW_DEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_28.Calculate(MSG_SHOW_NUMDEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_29.Calculate(MSG_SHOW_DELAYDEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_SHOW_MSG)
                .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_SHOW_AUTORICONNECT)
            Endif
        Endwith
        cp_BlankRecExtFlds(This,'paramric')
        This.SaveDependsOn()
        This.SetControlsValue()
        This.mHideControls()
        This.ChildrenChangeRow()
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = setting operation
    *     Allowed parameters  : Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        Local i_bVal
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
        * --- Disable List page when <> from Query
        With This.oPgFrm
            .Page1.oPag.oRCAUTORC_1_2.Enabled = i_bVal
            .Page1.oPag.oRCNUMRIC_1_3.Enabled = i_bVal
            .Page1.oPag.oRCDELAYR_1_5.Enabled = i_bVal
            .Page1.oPag.oRCCNTIME_1_7.Enabled = i_bVal
            .Page1.oPag.oRCDEADRQ_1_9.Enabled = i_bVal
            .Page1.oPag.oRCNUMDRQ_1_10.Enabled = i_bVal
            .Page1.oPag.oRCDELAYD_1_12.Enabled = i_bVal
            .Page1.oPag.oRCCONMSG_1_14.Enabled = i_bVal
            .Page1.oPag.oObj_1_16.Enabled = i_bVal
            .Page1.oPag.oObj_1_17.Enabled = i_bVal
            .Page1.oPag.oObj_1_18.Enabled = i_bVal
            .Page1.oPag.oObj_1_19.Enabled = i_bVal
            .Page1.oPag.oObj_1_20.Enabled = i_bVal
            .Page1.oPag.oObj_1_21.Enabled = i_bVal
            .Page1.oPag.oObj_1_22.Enabled = i_bVal
            .Page1.oPag.oObj_1_23.Enabled = i_bVal
            .Page1.oPag.oObj_1_24.Enabled = i_bVal
            .Page1.oPag.oObj_1_25.Enabled = i_bVal
            .Page1.oPag.oObj_1_26.Enabled = i_bVal
            .Page1.oPag.oObj_1_27.Enabled = i_bVal
            .Page1.oPag.oObj_1_28.Enabled = i_bVal
            .Page1.oPag.oObj_1_29.Enabled = i_bVal
            .Page1.oPag.oObj_1_30.Enabled = i_bVal
            .Page1.oPag.oObj_1_31.Enabled = i_bVal
        Endwith
        cp_SetEnabledExtFlds(This,'paramric',i_cOp)
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt,i_nConn
        i_nConn = i_TableProp[this.paramric_IDX,3]
        i_cFlt = This.cQueryFilter
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_RCSERIAL,"RCSERIAL",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_RCAUTORC,"RCAUTORC",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_RCNUMRIC,"RCNUMRIC",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_RCDELAYR,"RCDELAYR",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_RCCNTIME,"RCCNTIME",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_RCDEADRQ,"RCDEADRQ",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_RCNUMDRQ,"RCNUMDRQ",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_RCDELAYD,"RCDELAYD",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_RCCONMSG,"RCCONMSG",i_nConn)
        * --- Area Manuale = Build Filter
        * --- Fine Area Manuale
        Return (i_cFlt)
    Endfunc

    * --- Insert of new Record
    Function mInsert()
        Local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
        * --- Area Manuale = Insert Init
        * --- Fine Area Manuale
        If This.nDeferredFillRec<>0
            Return
        Endif
        If This.bUpdated .Or. This.IsAChildUpdated()
            i_nConn = i_TableProp[this.paramric_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.paramric_IDX,2])
            i_ccchkf=''
            i_ccchkv=''
            This.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,This.paramric_IDX,i_nConn)
            * --- Area Manuale = Autonum Assigned
            * --- Fine Area Manuale
            *
            * insert into paramric
            *
            This.NotifyEvent('Insert start')
            This.mUpdateTrs()
            If i_nConn<>0
                i_extfld=cp_InsertFldODBCExtFlds(This,'paramric')
                i_extval=cp_InsertValODBCExtFlds(This,'paramric')
                i_nnn="INSERT INTO "+i_cTable+" "+;
                    "(RCSERIAL,RCAUTORC,RCNUMRIC,RCDELAYR,RCCNTIME"+;
                    ",RCDEADRQ,RCNUMDRQ,RCDELAYD,RCCONMSG "+i_extfld+i_ccchkf+") VALUES ("+;
                    cp_ToStrODBC(This.w_RCSERIAL)+;
                    ","+cp_ToStrODBC(This.w_RCAUTORC)+;
                    ","+cp_ToStrODBC(This.w_RCNUMRIC)+;
                    ","+cp_ToStrODBC(This.w_RCDELAYR)+;
                    ","+cp_ToStrODBC(This.w_RCCNTIME)+;
                    ","+cp_ToStrODBC(This.w_RCDEADRQ)+;
                    ","+cp_ToStrODBC(This.w_RCNUMDRQ)+;
                    ","+cp_ToStrODBC(This.w_RCDELAYD)+;
                    ","+cp_ToStrODBC(This.w_RCCONMSG)+;
                    i_extval+i_ccchkv+")"
                =cp_TrsSQL(i_nConn,i_nnn)
            Else
                i_extfld=cp_InsertFldVFPExtFlds(This,'paramric')
                i_extval=cp_InsertValVFPExtFlds(This,'paramric')
                cp_CheckDeletedKey(i_cTable,0,'RCSERIAL',This.w_RCSERIAL)
                Insert Into (i_cTable);
                    (RCSERIAL,RCAUTORC,RCNUMRIC,RCDELAYR,RCCNTIME,RCDEADRQ,RCNUMDRQ,RCDELAYD,RCCONMSG  &i_extfld. &i_ccchkf.) Values (;
                    this.w_RCSERIAL;
                    ,This.w_RCAUTORC;
                    ,This.w_RCNUMRIC;
                    ,This.w_RCDELAYR;
                    ,This.w_RCCNTIME;
                    ,This.w_RCDEADRQ;
                    ,This.w_RCNUMDRQ;
                    ,This.w_RCDELAYD;
                    ,This.w_RCCONMSG;
                    &i_extval. &i_ccchkv.)
            Endif
            This.NotifyEvent('Insert end')
        Endif
        * --- Area Manuale = Insert End
        * --- Fine Area Manuale
        Return(Not(bTrsErr))

        * --- Update Database
    Function mReplace(i_bEditing)
        Local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        If This.nDeferredFillRec<>0
            Return
        Endif
        If Not(This.bLoaded)
            This.mInsert()
            i_bEditing=.F.
        Else
            i_bEditing=.T.
        Endif
        If This.bUpdated And i_bEditing
            This.mRestoreTrs()
            This.mUpdateTrs()
        Endif
        If This.bUpdated And i_bEditing
            i_nConn = i_TableProp[this.paramric_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.paramric_IDX,2])
            i_ccchkf=''
            i_ccchkw=''
            This.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,This.paramric_IDX,i_nConn)
            *
            * update paramric
            *
            This.NotifyEvent('Update start')
            If i_nConn<>0
                i_cWhere = This.cKeyWhereODBC
                i_extfld=cp_ReplaceODBCExtFlds(This,'paramric')
                i_nnn="UPDATE "+i_cTable+" SET"+;
                    " RCAUTORC="+cp_ToStrODBC(This.w_RCAUTORC)+;
                    ",RCNUMRIC="+cp_ToStrODBC(This.w_RCNUMRIC)+;
                    ",RCDELAYR="+cp_ToStrODBC(This.w_RCDELAYR)+;
                    ",RCCNTIME="+cp_ToStrODBC(This.w_RCCNTIME)+;
                    ",RCDEADRQ="+cp_ToStrODBC(This.w_RCDEADRQ)+;
                    ",RCNUMDRQ="+cp_ToStrODBC(This.w_RCNUMDRQ)+;
                    ",RCDELAYD="+cp_ToStrODBC(This.w_RCDELAYD)+;
                    ",RCCONMSG="+cp_ToStrODBC(This.w_RCCONMSG)+;
                    i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            Else
                i_extfld=cp_ReplaceVFPExtFlds(This,'paramric')
                i_cWhere = cp_PKFox(i_cTable  ,'RCSERIAL',This.w_RCSERIAL  )
                Update (i_cTable) Set;
                    RCAUTORC=This.w_RCAUTORC;
                    ,RCNUMRIC=This.w_RCNUMRIC;
                    ,RCDELAYR=This.w_RCDELAYR;
                    ,RCCNTIME=This.w_RCCNTIME;
                    ,RCDEADRQ=This.w_RCDEADRQ;
                    ,RCNUMDRQ=This.w_RCNUMDRQ;
                    ,RCDELAYD=This.w_RCDELAYD;
                    ,RCCONMSG=This.w_RCCONMSG;
                    &i_extfld. &i_ccchkf. Where &i_cWhere. &i_ccchkw.
                i_nModRow=_Tally
            Endif
            =cp_CheckMultiuser(i_nModRow)
            This.NotifyEvent('Update end')
        Endif
        If Not(bTrsErr)
        Endif
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(bTrsErr)

        * --- Delete Records
    Function mDelete()
        Local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
        * --- Area Manuale = Delete Init
        * --- Fine Area Manuale
        Local i_bLoadNow
        If This.nDeferredFillRec<>0  && record is not loaded
            i_bLoadNow=.T.
            This.bOnScreen=.T.         && force loadrec in future
            This.LoadRec()             && record loaded, timestamp correct
        Endif
        If This.nDeferredFillRec=0 And This.bLoaded=.F.
            If i_bLoadNow
                This.bOnScreen=.F.
            Endif
            Return
        Endif
        *
        If Not(bTrsErr)
            i_nConn = i_TableProp[this.paramric_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.paramric_IDX,2])
            i_ccchkw=''
            This.SetCCCHKVarDelete(@i_ccchkw,This.paramric_IDX,i_nConn)
            *
            * delete paramric
            *
            This.NotifyEvent('Delete start')
            If i_nConn<>0
                i_cWhere = This.cKeyWhereODBC
                i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                    " WHERE "+&i_cWhere+i_ccchkw)
            Else
                * i_cWhere = this.cKeyWhere
                i_cWhere = cp_PKFox(i_cTable  ,'RCSERIAL',This.w_RCSERIAL  )
                Delete From (i_cTable) Where &i_cWhere. &i_ccchkw.
                i_nModRow=_Tally
            Endif
            =cp_CheckMultiuser(i_nModRow)
            If Not(bTrsErr)
                This.mRestoreTrs()
            Endif
            This.NotifyEvent('Delete end')
        Endif
        *
        If i_bLoadNow
            This.bOnScreen=.F.
        Endif
        *
        This.mDeleteWarnings()
        * --- Area Manuale = Delete End
        * --- Fine Area Manuale
        Return

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        Local i_cTable,i_nConn
        i_nConn = i_TableProp[this.paramric_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.paramric_IDX,2])
        If i_bUpd
            With This
                .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_AUTORICONNECT)
                .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_NUMRICONNECT)
                .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_DELAYRICONNECT)
                .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_CONNECTTIMEOUT)
                .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_DEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_NUMDEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_DELAYDEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_CNF_MSG)
                .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_SHOW_NUMRICONNECT)
                .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_SHOW_DELAYRICONNECT)
                .oPgFrm.Page1.oPag.oObj_1_26.Calculate(MSG_SHOW_CONNECTTIMEOUT)
                .oPgFrm.Page1.oPag.oObj_1_27.Calculate(MSG_SHOW_DEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_28.Calculate(MSG_SHOW_NUMDEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_29.Calculate(MSG_SHOW_DELAYDEADREQUERY)
                .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_SHOW_MSG)
                .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_SHOW_AUTORICONNECT)
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
            Endwith
            This.DoRTCalc(1,9,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
            .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_AUTORICONNECT)
            .oPgFrm.Page1.oPag.oObj_1_17.Calculate(MSG_NUMRICONNECT)
            .oPgFrm.Page1.oPag.oObj_1_18.Calculate(MSG_DELAYRICONNECT)
            .oPgFrm.Page1.oPag.oObj_1_19.Calculate(MSG_CONNECTTIMEOUT)
            .oPgFrm.Page1.oPag.oObj_1_20.Calculate(MSG_DEADREQUERY)
            .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_NUMDEADREQUERY)
            .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_DELAYDEADREQUERY)
            .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_CNF_MSG)
            .oPgFrm.Page1.oPag.oObj_1_24.Calculate(MSG_SHOW_NUMRICONNECT)
            .oPgFrm.Page1.oPag.oObj_1_25.Calculate(MSG_SHOW_DELAYRICONNECT)
            .oPgFrm.Page1.oPag.oObj_1_26.Calculate(MSG_SHOW_CONNECTTIMEOUT)
            .oPgFrm.Page1.oPag.oObj_1_27.Calculate(MSG_SHOW_DEADREQUERY)
            .oPgFrm.Page1.oPag.oObj_1_28.Calculate(MSG_SHOW_NUMDEADREQUERY)
            .oPgFrm.Page1.oPag.oObj_1_29.Calculate(MSG_SHOW_DELAYDEADREQUERY)
            .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_SHOW_MSG)
            .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_SHOW_AUTORICONNECT)
        Endwith
        Return


        * --- Enable controls under condition
    Procedure mEnableControls()
        This.oPgFrm.Page1.oPag.oRCNUMRIC_1_3.Enabled = This.oPgFrm.Page1.oPag.oRCNUMRIC_1_3.mCond()
        This.oPgFrm.Page1.oPag.oRCDELAYR_1_5.Enabled = This.oPgFrm.Page1.oPag.oRCDELAYR_1_5.mCond()
        This.oPgFrm.Page1.oPag.oRCCNTIME_1_7.Enabled = This.oPgFrm.Page1.oPag.oRCCNTIME_1_7.mCond()
        This.oPgFrm.Page1.oPag.oRCNUMDRQ_1_10.Enabled = This.oPgFrm.Page1.oPag.oRCNUMDRQ_1_10.mCond()
        This.oPgFrm.Page1.oPag.oRCDELAYD_1_12.Enabled = This.oPgFrm.Page1.oPag.oRCDELAYD_1_12.mCond()
        This.oPgFrm.Page1.oPag.oRCCONMSG_1_14.Enabled = This.oPgFrm.Page1.oPag.oRCCONMSG_1_14.mCond()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        DoDefault()
        Return

        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
        Endwith
        * --- Area Manuale = Notify Event End
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

    Function SetControlsValue()
        If Not(This.oPgFrm.Page1.oPag.oRCAUTORC_1_2.RadioValue()==This.w_RCAUTORC)
            This.oPgFrm.Page1.oPag.oRCAUTORC_1_2.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oRCNUMRIC_1_3.Value==This.w_RCNUMRIC)
            This.oPgFrm.Page1.oPag.oRCNUMRIC_1_3.Value=This.w_RCNUMRIC
        Endif
        If Not(This.oPgFrm.Page1.oPag.oRCDELAYR_1_5.Value==This.w_RCDELAYR)
            This.oPgFrm.Page1.oPag.oRCDELAYR_1_5.Value=This.w_RCDELAYR
        Endif
        If Not(This.oPgFrm.Page1.oPag.oRCCNTIME_1_7.Value==This.w_RCCNTIME)
            This.oPgFrm.Page1.oPag.oRCCNTIME_1_7.Value=This.w_RCCNTIME
        Endif
        If Not(This.oPgFrm.Page1.oPag.oRCDEADRQ_1_9.RadioValue()==This.w_RCDEADRQ)
            This.oPgFrm.Page1.oPag.oRCDEADRQ_1_9.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oRCNUMDRQ_1_10.Value==This.w_RCNUMDRQ)
            This.oPgFrm.Page1.oPag.oRCNUMDRQ_1_10.Value=This.w_RCNUMDRQ
        Endif
        If Not(This.oPgFrm.Page1.oPag.oRCDELAYD_1_12.Value==This.w_RCDELAYD)
            This.oPgFrm.Page1.oPag.oRCDELAYD_1_12.Value=This.w_RCDELAYD
        Endif
        If Not(This.oPgFrm.Page1.oPag.oRCCONMSG_1_14.RadioValue()==This.w_RCCONMSG)
            This.oPgFrm.Page1.oPag.oRCCONMSG_1_14.SetRadio()
        Endif
        cp_SetControlsValueExtFlds(This,'paramric')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
            Else
                If Not(i_bnoChk)
                    cp_ErrorMsg(i_cErrorMsg)
                Endif
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

Enddefine

* --- Define pages as container
Define Class tcp_paramconnetmPag1 As StdContainer
    Width  = 465
    Height = 254
    stdWidth  = 465
    stdheight = 254
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.

    Add Object oRCAUTORC_1_2 As StdCheck With uid="SJCHGAWBQV",rtseq=2,rtrep=.F.,Left=20, Top=14, Caption="Attiva riconnessione automatica",;
        HelpContextID = 75387192,;
        cFormVar="w_RCAUTORC", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oRCAUTORC_1_2.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func oRCAUTORC_1_2.GetRadio()
        This.Parent.oContained.w_RCAUTORC = This.RadioValue()
        Return .T.
    Endfunc

    Func oRCAUTORC_1_2.SetRadio()
        This.Parent.oContained.w_RCAUTORC=Trim(This.Parent.oContained.w_RCAUTORC)
        This.Value = ;
            iif(This.Parent.oContained.w_RCAUTORC=='S',1,;
            0)
    Endfunc

    Add Object oRCNUMRIC_1_3 As StdField With uid="DLUVLDMJCK",rtseq=3,rtrep=.F.,;
        cFormVar = "w_RCNUMRIC", cQueryName = "RCNUMRIC",;
        bObbl = .F. , nPag = 1, Value=0, bMultilanguage =  .F.,;
        HelpContextID = 195518008,;
        bGlobalFont=.T.,;
        Height=21, Width=83, Left=290, Top=45, cSayPict='"9999999999"', cGetPict='"9999999999"'

    Func oRCNUMRIC_1_3.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_RCAUTORC = 'S')
            Endwith
        Endif
    Endfunc

    Add Object oRCDELAYR_1_5 As StdField With uid="LKKYDIEWLM",rtseq=4,rtrep=.F.,;
        cFormVar = "w_RCDELAYR", cQueryName = "RCDELAYR",;
        bObbl = .F. , nPag = 1, Value=0, bMultilanguage =  .F.,;
        HelpContextID = 177558584,;
        bGlobalFont=.T.,;
        Height=21, Width=83, Left=290, Top=68, cSayPict='"9999999999"', cGetPict='"9999999999"'

    Func oRCDELAYR_1_5.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_RCAUTORC = 'S')
            Endwith
        Endif
    Endfunc

    Add Object oRCCNTIME_1_7 As StdField With uid="AXFVMPZZOQ",rtseq=5,rtrep=.F.,;
        cFormVar = "w_RCCNTIME", cQueryName = "RCCNTIME",;
        bObbl = .F. , nPag = 1, Value=0, bMultilanguage =  .F.,;
        HelpContextID = 253616952,;
        bGlobalFont=.T.,;
        Height=21, Width=83, Left=290, Top=91, cSayPict='"9999999999"', cGetPict='"9999999999"'

    Func oRCCNTIME_1_7.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_RCAUTORC = 'S')
            Endwith
        Endif
    Endfunc

    Add Object oRCDEADRQ_1_9 As StdCheck With uid="XWVMZRMTUW",rtseq=6,rtrep=.F.,Left=20, Top=126, Caption="Attiva gestione deadlock",;
        HelpContextID = 62542904,;
        cFormVar="w_RCDEADRQ", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oRCDEADRQ_1_9.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func oRCDEADRQ_1_9.GetRadio()
        This.Parent.oContained.w_RCDEADRQ = This.RadioValue()
        Return .T.
    Endfunc

    Func oRCDEADRQ_1_9.SetRadio()
        This.Parent.oContained.w_RCDEADRQ=Trim(This.Parent.oContained.w_RCDEADRQ)
        This.Value = ;
            iif(This.Parent.oContained.w_RCDEADRQ=='S',1,;
            0)
    Endfunc

    Add Object oRCNUMDRQ_1_10 As StdField With uid="DPKPCYHERC",rtseq=7,rtrep=.F.,;
        cFormVar = "w_RCNUMDRQ", cQueryName = "RCNUMDRQ",;
        bObbl = .F. , nPag = 1, Value=0, bMultilanguage =  .F.,;
        HelpContextID = 63397432,;
        bGlobalFont=.T.,;
        Height=21, Width=83, Left=290, Top=157, cSayPict='"9999999999"', cGetPict='"9999999999"'

    Func oRCNUMDRQ_1_10.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_RCDEADRQ = 'S')
            Endwith
        Endif
    Endfunc

    Add Object oRCDELAYD_1_12 As StdField With uid="TVKGSBUBYJ",rtseq=8,rtrep=.F.,;
        cFormVar = "w_RCDELAYD", cQueryName = "RCDELAYD",;
        bObbl = .F. , nPag = 1, Value=0, bMultilanguage =  .F.,;
        HelpContextID = 177558584,;
        bGlobalFont=.T.,;
        Height=21, Width=83, Left=290, Top=180, cSayPict='"9999999999"', cGetPict='"9999999999"'

    Func oRCDELAYD_1_12.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_RCDEADRQ = 'S')
            Endwith
        Endif
    Endfunc


    Add Object oRCCONMSG_1_14 As StdCombo With uid="TWIBGWPGHC",rtseq=9,rtrep=.F.,Left=290,Top=221,Width=165,Height=19;
        , HelpContextID = 89649976;
        , cFormVar="w_RCCONMSG",RowSource=""+"Disattivato,"+"Visualizza su desktop,"+"Visualizza su wait window", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oRCCONMSG_1_14.RadioValue()
        Return(Iif(This.Value =1,'N',;
            iif(This.Value =2,'D',;
            iif(This.Value =3,'W',;
            space(1)))))
    Endfunc
    Func oRCCONMSG_1_14.GetRadio()
        This.Parent.oContained.w_RCCONMSG = This.RadioValue()
        Return .T.
    Endfunc

    Func oRCCONMSG_1_14.SetRadio()
        This.Parent.oContained.w_RCCONMSG=Trim(This.Parent.oContained.w_RCCONMSG)
        This.Value = ;
            iif(This.Parent.oContained.w_RCCONMSG=='N',1,;
            iif(This.Parent.oContained.w_RCCONMSG=='D',2,;
            iif(This.Parent.oContained.w_RCCONMSG=='W',3,;
            0)))
    Endfunc

    Func oRCCONMSG_1_14.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_RCDEADRQ = 'S' Or .w_RCAUTORC = 'S')
            Endwith
        Endif
    Endfunc


    Add Object oObj_1_16 As cp_setCtrlObjprop With uid="JWCOTWJJSP",Left=518, Top=29, Width=142,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="RCAUTORC",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 186745819;
        , bGlobalFont=.T.



    Add Object oObj_1_17 As cp_setCtrlObjprop With uid="ILVEVHZWIV",Left=518, Top=79, Width=142,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Numero tentativi di riconnessione:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 186745819;
        , bGlobalFont=.T.



    Add Object oObj_1_18 As cp_setCtrlObjprop With uid="VITSABODHF",Left=518, Top=104, Width=142,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Delay di riconnessione:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 186745819;
        , bGlobalFont=.T.



    Add Object oObj_1_19 As cp_setCtrlObjprop With uid="NNNQDMVSSN",Left=518, Top=129, Width=142,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Timeout di connessione:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 186745819;
        , bGlobalFont=.T.



    Add Object oObj_1_20 As cp_setCtrlObjprop With uid="UJHTJIDMCE",Left=518, Top=54, Width=142,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="RCDEADRQ",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 186745819;
        , bGlobalFont=.T.



    Add Object oObj_1_21 As cp_setCtrlObjprop With uid="DTPYQPGYDP",Left=518, Top=154, Width=142,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Numero tentativi per gestione deadlock:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 186745819;
        , bGlobalFont=.T.



    Add Object oObj_1_22 As cp_setCtrlObjprop With uid="TDLGGOCDDN",Left=518, Top=179, Width=142,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Delay per gestione deadlock:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 186745819;
        , bGlobalFont=.T.



    Add Object oObj_1_23 As cp_setCtrlObjprop With uid="KWKMXLYNTN",Left=518, Top=204, Width=142,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Visualizzazione messaggi in fase di riconnessione:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 186745819;
        , bGlobalFont=.T.



    Add Object oObj_1_24 As cp_setCtrlObjprop With uid="GWXLLSZZAG",Left=669, Top=53, Width=142,Height=26,;
        caption='RCNUMRIC',;
        cProp="Tooltiptext",cObj="w_RCNUMRIC",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 195518008;
        , bGlobalFont=.T.



    Add Object oObj_1_25 As cp_setCtrlObjprop With uid="MXVKTREMSA",Left=669, Top=78, Width=142,Height=26,;
        caption='RCDELAYR',;
        cProp="Tooltiptext",cObj="w_RCDELAYR",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 177558584;
        , bGlobalFont=.T.



    Add Object oObj_1_26 As cp_setCtrlObjprop With uid="OVROXUKMMN",Left=669, Top=103, Width=142,Height=26,;
        caption='RCCNTIME',;
        cProp="Tooltiptext",cObj="w_RCCNTIME",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 253616952;
        , bGlobalFont=.T.



    Add Object oObj_1_27 As cp_setCtrlObjprop With uid="FIWBVBAQNV",Left=669, Top=128, Width=142,Height=26,;
        caption='RCDEADRQ',;
        cProp="Tooltiptext",cObj="w_RCDEADRQ",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 62542904;
        , bGlobalFont=.T.



    Add Object oObj_1_28 As cp_setCtrlObjprop With uid="HSQVSXYJPJ",Left=669, Top=153, Width=142,Height=26,;
        caption='RCNUMDRQ',;
        cProp="Tooltiptext",cObj="w_RCNUMDRQ",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 63397432;
        , bGlobalFont=.T.



    Add Object oObj_1_29 As cp_setCtrlObjprop With uid="MMWDUATBQS",Left=669, Top=178, Width=142,Height=26,;
        caption='RCDELAYD',;
        cProp="Tooltiptext",cObj="w_RCDELAYD",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 177558584;
        , bGlobalFont=.T.



    Add Object oObj_1_30 As cp_setCtrlObjprop With uid="IGHACVLJAT",Left=669, Top=203, Width=142,Height=26,;
        caption='RCCONMSG',;
        cProp="Tooltiptext",cObj="w_RCCONMSG",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 89649976;
        , bGlobalFont=.T.



    Add Object oObj_1_31 As cp_setCtrlObjprop With uid="JRJOGYRNGG",Left=669, Top=28, Width=142,Height=26,;
        caption='RCAUTORC',;
        cProp="Tooltiptext",cObj="w_RCAUTORC",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 75387192;
        , bGlobalFont=.T.


    Add Object oStr_1_4 As StdString With uid="EUUEFXQKPF",Visible=.T., Left=102, Top=48,;
        Alignment=1, Width=187, Height=18,;
        Caption="Numero tentativi di riconnessione:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_6 As StdString With uid="GKDPNLPGZV",Visible=.T., Left=160, Top=71,;
        Alignment=1, Width=129, Height=18,;
        Caption="Delay di riconnessione:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_8 As StdString With uid="PPUFCEKESR",Visible=.T., Left=153, Top=94,;
        Alignment=1, Width=136, Height=18,;
        Caption="Timeout di connessione:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_11 As StdString With uid="MJHIWCQYFW",Visible=.T., Left=72, Top=160,;
        Alignment=1, Width=217, Height=18,;
        Caption="Numero tentativi per gestione deadlock:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_13 As StdString With uid="IXLTASRJYB",Visible=.T., Left=130, Top=183,;
        Alignment=1, Width=159, Height=18,;
        Caption="Delay per gestione deadlock:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_15 As StdString With uid="UZBWWNLJUF",Visible=.T., Left=9, Top=223,;
        Alignment=1, Width=280, Height=18,;
        Caption="Visualizzazione messaggi in fase di riconnessione:"  ;
        , bGlobalFont=.T.
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_paramconnetm','paramric','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +" "+i_cAliasName2+".RCSERIAL=paramric.RCSERIAL";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Master File"
Endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
