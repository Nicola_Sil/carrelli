* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_exprep                                                       *
*              Stampa su file (print system)                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-10-07                                                      *
* Last revis.: 2009-10-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject,i_Tipo,i_nFile
* --- Area Manuale = Header
* --- Fine Area Manuale
Private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
Createobject("tcp_exprep",oParentObject,m.i_Tipo,m.i_nFile)
Return(i_retval)

Define Class tcp_exprep As StdBatch
    * --- Local variables
    i_Tipo = Space(10)
    i_nFile = Space(10)
    * --- WorkFile variables
    runtime_filters = 2

    Procedure Pag1
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        i_err=""
        Select "__tmp__"
        i_err=On("ERROR")
        * --- Traduzione: la cp_ErrorMsg contiene la cp_Translate. Il secondo paramentro non � tradotto
        *     perch� serve solo per decidere l'icona da utilizzare.
        Do Case
            Case This.i_Tipo = "DBF"
                * --- Export DBF
                On Error cp_ErrorMsg(cp_MsgFormat(MSG_CANNOT_CREATE_FILE_CL__ ,Upper(ExitName)))
                Copy To (This.i_nFile) Type Fox2x
            Case This.i_Tipo = "SDF"
                * --- Export SDF
                On Error cp_ErrorMsg(cp_MsgFormat(MSG_CANNOT_CREATE_FILE_CL__ ,Upper(ExitName)))
                Copy To (This.i_nFile) Sdf
            Case This.i_Tipo = "DLM"
                * --- Export DELIMITED
                On Error cp_ErrorMsg(cp_MsgFormat(MSG_CANNOT_CREATE_FILE_CL__ ,Upper(ExitName)))
                Copy To (This.i_nFile) Delimited
        Endcase
        On Error &i_err
    Endproc


    Proc Init(oParentObject,i_Tipo,i_nFile)
        This.i_Tipo=i_Tipo
        This.i_nFile=i_nFile
        DoDefault(oParentObject)
        Return
    Function OpenTables()
        Dimension This.cWorkTables[max(1,0)]
        Return(This.OpenAllTables(0))

        * --- Area Manuale = Functions & Procedures
        * --- Fine Area Manuale
Enddefine

Procedure getEntityType(result)
    result="Routine"
Endproc
Procedure getEntityParmList(result)
    result="i_Tipo,i_nFile"
Endproc
