* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_searchfilterr                                                *
*              Routine ctrl mask filter/search                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-12-22                                                      *
* Last revis.: 2011-07-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject,pCommandExec
* --- Area Manuale = Header
* --- Fine Area Manuale
Private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
Createobject("tcp_searchfilterr",oParentObject,m.pCommandExec)
Return(i_retval)

Define Class tcp_searchfilterr As StdBatch
    * --- Local variables
    pCommandExec = Space(10)
    w_VALUEFILTERTYPE = Space(1)
    w_POSITION = 0
    w_OLDPOSITION = 0
    w_FilterCond = Space(254)
    w_ZOOM = .Null.
    w_ControlSource = Space(50)
    w_FIRSTREC = .F.
    w_COL = 0
    w_FORMAT = Space(254)
    w_EOF = .F.
    * --- WorkFile variables
    runtime_filters = 2

    Procedure Pag1
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        * --- Routine per il controllo dela maschera di ricerca e filtro
        This.w_ControlSource = This.oParentObject.oParentObject.ControlSource
        If Left(This.w_ControlSource,17)="cp_MemoFirstLine("
            This.w_ControlSource = cp_ControlSourceMemo(This.w_ControlSource)
        Endif
        Local i_OldCursor
        i_OldCursor=Select ()
        Do Case
            Case This.pCommandExec="Type"
                Select (This.oParentObject.oParentObject.Parent.RecordSource)
                Do Case
                    Case Type(This.w_ControlSource)="D" Or Type(This.w_ControlSource)="T"
                        This.w_VALUEFILTERTYPE = "D"
                        This.oParentObject.w_ZoomValue.Visible = .T.
                        This.oParentObject.w_ZoomValueN.Visible = .F.
                    Case Type(This.w_ControlSource)="N"
                        This.w_VALUEFILTERTYPE = "N"
                        This.oParentObject.w_ZoomValue.Visible = .F.
                        This.oParentObject.w_ZoomValueN.Visible = .T.
                    Otherwise
                        This.w_VALUEFILTERTYPE = "C"
                        This.oParentObject.w_ZoomValue.Visible = .T.
                        This.oParentObject.w_ZoomValueN.Visible = .F.
                Endcase
                Select (i_OldCursor)
                i_retcode = 'stop'
                i_retval = This.w_VALUEFILTERTYPE
                Return
            Case This.pCommandExec="Search"
                This.oParentObject.RunDeactivateRelease = .F.
                If Reccount(This.oParentObject.oParentObject.Parent.RecordSource)>0
                    This.Pag2()
                    If i_retcode='stop' Or !Empty(i_Error)
                        Return
                    Endif
                    If Not This.w_EOF And Found()
                        This.w_POSITION = Recno()
                    Else
                        If This.oParentObject.w_Searchwrap="S"
                            This.w_POSITION = 1
                            Select (This.oParentObject.oParentObject.Parent.RecordSource)
                            Go (This.w_POSITION)
                            This.Pag2()
                            If i_retcode='stop' Or !Empty(i_Error)
                                Return
                            Endif
                            If Found()
                                This.w_POSITION = Recno()
                            Else
                                Cp_ErrorMsg("Valore non trovato")
                                This.w_POSITION = This.w_OLDPOSITION
                            Endif
                        Else
                            Cp_ErrorMsg("Valore non trovato")
                            This.w_POSITION = This.w_OLDPOSITION
                        Endif
                    Endif
                    Select (This.oParentObject.oParentObject.Parent.RecordSource)
                    Go (This.w_POSITION)
                    This.oParentObject.oParentObject.Parent.SetFocus()
                    This.oParentObject.oParentObject.Parent.Refresh()
                    This.oParentObject.RunDeactivateRelease = .T.
                Else
                    Cp_ErrorMsg("Valore non trovato")
                Endif
                If Vartype(This.oParentObject)="O"
                    If i_curform.WindowType=1
                        This.oParentObject.EcpQuit()
                    Else
                        This.oParentObject.SetFocusOnFirstControl()
                        If This.oParentObject.oPgFrm.Pages[1].oPag.oStr_StringAdvanced.Caption = cp_Translate("Nascondi filtri avanzati")
                            This.oParentObject.oPgFrm.Pages[1].oPag.oStr_StringAdvanced.Click()
                        Endif
                        This.oParentObject.oTimerInform.Enabled=.T.
                    Endif
                Endif
            Case This.pCommandExec="Filter"
                If This.oParentObject.w_RemPrevFilter="S"
                    cp_BlankWhereZoom(This.oParentObject.oParentObject)
                Endif
                This.w_FilterCond = Alltrim(This.w_ControlSource)
                Do Case
                    Case Inlist(This.oParentObject.w_TypeFilter, "=", ">=", ">", "<=", "<", "<>")
                        This.w_FilterCond = This.w_FilterCond + Alltrim(This.oParentObject.w_TypeFilter)
                    Case This.oParentObject.w_TypeFilter=="Like" Or This.oParentObject.w_TypeFilter=="Like %"
                        This.w_FilterCond = This.w_FilterCond + " Like "
                    Case This.oParentObject.w_TypeFilter=="Not Like" Or This.oParentObject.w_TypeFilter=="Not Like %"
                        This.w_FilterCond = This.w_FilterCond + " not Like "
                Endcase
                Do Case
                    Case This.oParentObject.w_CTRLTYPE="D"
                        If Empty(This.oParentObject.w_SearchValueD)
                            If This.oParentObject.w_TypeFilter="<>"
                                This.w_FilterCond = Alltrim(This.w_ControlSource) + " is not null"
                            Else
                                This.w_FilterCond = Alltrim(This.w_ControlSource) + " is null"
                            Endif
                        Else
                            This.w_FilterCond = This.w_FilterCond + cp_ToStrODBC(This.oParentObject.w_SearchValueD)
                        Endif
                    Case This.oParentObject.w_CTRLTYPE="N"
                        This.w_FilterCond = This.w_FilterCond + Chrtran(Alltrim(Str(This.oParentObject.w_SearchValueN, 18, 4)), ",", ".")
                    Otherwise
                        If Empty(This.oParentObject.w_SearchValue)
                            Do Case
                                Case Inlist(This.oParentObject.w_TypeFilter, "Not Like", "Not Like %")
                                    This.w_FilterCond = "[NOTEMPTYSTR(" + Alltrim(This.w_ControlSource) + ")] <> 0"
                                Case Inlist(This.oParentObject.w_TypeFilter, "Like", "Like %")
                                    This.w_FilterCond = "[NOTEMPTYSTR(" + Alltrim(This.w_ControlSource) + ")] = 0"
                                Otherwise
                                    This.w_FilterCond = "[NOTEMPTYSTR(" + Alltrim(This.w_ControlSource) + ")] "+Alltrim(This.oParentObject.w_TypeFilter)+" 0"
                            Endcase
                        Else
                            This.w_FilterCond = This.w_FilterCond + cp_ToStrODBC(Iif(Inlist(This.oParentObject.w_TypeFilter, "Like %", "Not Like %"), "%", "") + Alltrim(This.oParentObject.w_SearchValue) + Iif(Inlist(This.oParentObject.w_TypeFilter, "Like", "Like %", "Not Like", "Not Like %"), "%", ""))
                        Endif
                Endcase
                This.oParentObject.oParentObject.Parent.Parent.AddWhere(This.w_FilterCond)
                This.oParentObject.oParentObject.Parent.Parent.RefreshParamWnd()
                This.oParentObject.oParentObject.Parent.Parent.DeferredQuery()
                If Vartype(This.oParentObject)="O"
                    This.oParentObject.EcpQuit()
                Endif
            Case This.pCommandExec="LoadZoom"
                Local w_ColumnSearch
                w_ColumnSearch = This.w_ControlSource
                Do Case
                    Case This.oParentObject.w_CTRLTYPE="D"
                        This.w_ZOOM = This.oParentObject.w_ZoomValue
                        Select Distinct &w_ColumnSearch As VALUEZOOM From (This.oParentObject.oParentObject.Parent.RecordSource) Into Cursor elemzoom Where &w_ColumnSearch Is Not Null Order By 1 Desc
                    Case This.oParentObject.w_CTRLTYPE="N"
                        This.w_ZOOM = This.oParentObject.w_ZoomValueN
                        Select Distinct &w_ColumnSearch As VALUEZOOM From (This.oParentObject.oParentObject.Parent.RecordSource) Into Cursor elemzoom Order By 1
                    Otherwise
                        This.w_ZOOM = This.oParentObject.w_ZoomValue
                        Select Distinct Alltrim( &w_ColumnSearch ) As VALUEZOOM From (This.oParentObject.oParentObject.Parent.RecordSource) Into Cursor elemzoom Where &w_ColumnSearch Is Not Null And Not Empty( &w_ColumnSearch ) Order By 1
                Endcase
                If Used("elemzoom")
                    * --- Cancello il contenuto dello Zoom
                    Select (This.w_ZOOM.cCursor)
                    Zap
                    * --- Scan del cursore di appoggio e copia su cursore zoom
                    Select elemzoom
                    Go Top
                    Scan
                        Scatter Memvar
                        Select (This.w_ZOOM.cCursor)
                        Append Blank
                        Gather Memvar
                    Endscan
                    Use In elemzoom
                    Select (This.w_ZOOM.cCursor)
                    Go Top
                    This.w_ZOOM.grd.Refresh
                    This.w_ZOOM.grd.Refresh()
                Endif
            Case This.pCommandExec="Select"
                If This.oParentObject.w_RemPrevFilter="S"
                    cp_BlankWhereZoom(This.oParentObject.oParentObject)
                Endif
                Local w_ColumnSearch
                w_ColumnSearch = This.w_ControlSource
                If This.oParentObject.w_CTRLTYPE="N"
                    This.w_ZOOM = This.oParentObject.w_ZoomValueN
                Else
                    This.w_ZOOM = This.oParentObject.w_ZoomValue
                Endif
                This.w_FIRSTREC = .T.
                Select (This.w_ZOOM.cCursor)
                Scan For xchk=1
                    If This.w_FIRSTREC
                        This.w_FIRSTREC = .F.
                        This.w_FilterCond = "("+Alltrim(This.w_ControlSource)
                    Else
                        This.oParentObject.oParentObject.Parent.Parent.AddWhere(This.w_FilterCond)
                        This.w_FilterCond = "OR"
                        This.oParentObject.oParentObject.Parent.Parent.AddWhere(This.w_FilterCond)
                        This.w_FilterCond = Alltrim(This.w_ControlSource)
                    Endif
                    Select (This.w_ZOOM.cCursor)
                    Do Case
                        Case This.oParentObject.w_CTRLTYPE="N"
                            This.w_FilterCond = This.w_FilterCond + " = " + cp_ToStrODBC(VALUEZOOM)
                        Case This.oParentObject.w_CTRLTYPE="D"
                            This.w_FilterCond = This.w_FilterCond + " = " + cp_ToStrODBC(Ctot(Alltrim(VALUEZOOM)))
                        Otherwise
                            This.w_FilterCond = This.w_FilterCond + " = " + cp_ToStrODBC(Alltrim(VALUEZOOM))
                    Endcase
                Endscan
                If Not Empty(This.w_FilterCond)
                    This.w_FilterCond = This.w_FilterCond + ")"
                    This.oParentObject.oParentObject.Parent.Parent.AddWhere(This.w_FilterCond)
                Endif
                This.oParentObject.oParentObject.Parent.Parent.RefreshParamWnd()
                This.oParentObject.oParentObject.Parent.Parent.DeferredQuery()
                If Vartype(This.oParentObject)="O"
                    This.oParentObject.EcpQuit()
                Endif
            Case This.pCommandExec="Checked"
                If This.oParentObject.w_CTRLTYPE="N"
                    This.w_ZOOM = This.oParentObject.w_ZoomValueN
                Else
                    This.w_ZOOM = This.oParentObject.w_ZoomValue
                Endif
                Select (This.w_ZOOM.cCursor)
                This.w_OLDPOSITION = Recno()
                Count For xchk=1 To This.w_POSITION
                This.oParentObject.w_CHECKED = This.w_POSITION>0
                If Not This.oParentObject.w_CHECKED And This.oParentObject.w_CTRLTYPE="C"
                    This.oParentObject.w_TypeFilter = "Like"
                Endif
                If This.oParentObject.w_CHECKED
                    This.oParentObject.w_SearchValue = ""
                    This.oParentObject.w_SearchValueD = Null
                    This.oParentObject.w_SearchValueN = 0
                Endif
                Select (This.w_ZOOM.cCursor)
                Go This.w_OLDPOSITION
            Case This.pCommandExec="Format"
                This.w_FORMAT = ""
                This.w_COL = This.oParentObject.oParentObject.Parent.Parent.Getcol(This.w_ControlSource)
                If This.w_COL>0 And This.w_COL<=This.oParentObject.oParentObject.Parent.ColumnCount
                    This.w_FORMAT = This.oParentObject.oParentObject.Parent.Columns[this.w_COL].InputMask
                Endif
                Select (i_OldCursor)
                i_retcode = 'stop'
                i_retval = This.w_FORMAT
                Return
        Endcase
        Select (i_OldCursor)
    Endproc


    Procedure Pag2
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        Select (This.oParentObject.oParentObject.Parent.RecordSource)
        This.w_OLDPOSITION = Recno()
        This.w_EOF = .F.
        Do Case
            Case This.w_OLDPOSITION=Reccount()
                This.w_EOF = .T.
            Case This.w_OLDPOSITION=1 And Not This.oParentObject.w_SecondSearch
                This.oParentObject.w_SecondSearch = .T.
                Go This.w_OLDPOSITION
            Otherwise
                This.oParentObject.w_SecondSearch = .F.
                Go This.w_OLDPOSITION+1
        Endcase
        If Not Eof()
            Local w_ColumnSearch, w_Operand
            w_ColumnSearch = This.w_ControlSource
            w_Operand=Iif(This.oParentObject.w_TypeFilter="=", "==", This.oParentObject.w_TypeFilter)
            Do Case
                Case This.oParentObject.w_CTRLTYPE="D"
                    Locate For Nvl( &w_ColumnSearch , cp_CharToDate("  -  -    ")) &w_Operand Nvl( This.oParentObject.w_SearchValueD , cp_CharToDate("  -  -    ")) Rest
                Case This.oParentObject.w_CTRLTYPE="N"
                    Locate For &w_ColumnSearch &w_Operand This.oParentObject.w_SearchValueN Rest
                Otherwise
                    Do Case
                        Case This.oParentObject.w_TypeFilter="Like %"
                            Locate For Upper(Alltrim(This.oParentObject.w_SearchValue)) $ Upper(Alltrim( &w_ColumnSearch )) Rest
                        Case This.oParentObject.w_TypeFilter="Like"
                            Locate For Upper(Alltrim(This.oParentObject.w_SearchValue)) == Upper(Alltrim( Left( &w_ColumnSearch , Len(Alltrim(This.oParentObject.w_SearchValue))))) Rest
                        Case This.oParentObject.w_TypeFilter="Not Like %"
                            Locate For Not(Upper(Alltrim(This.oParentObject.w_SearchValue)) $ Upper(Alltrim( &w_ColumnSearch ))) Rest
                        Case This.oParentObject.w_TypeFilter="Not Like"
                            Locate For Upper(Alltrim(This.oParentObject.w_SearchValue)) <> Upper(Alltrim( Left( &w_ColumnSearch , Len(Alltrim(This.oParentObject.w_SearchValue))))) Rest
                        Otherwise
                            Locate For Upper(Alltrim( &w_ColumnSearch )) &w_Operand Upper(Alltrim(This.oParentObject.w_SearchValue)) Rest
                    Endcase
            Endcase
        Endif
    Endproc


    Proc Init(oParentObject,pCommandExec)
        This.pCommandExec=pCommandExec
        DoDefault(oParentObject)
        Return
    Function OpenTables()
        Dimension This.cWorkTables[max(1,0)]
        Return(This.OpenAllTables(0))

        * --- Area Manuale = Functions & Procedures
        * --- Fine Area Manuale
Enddefine

Procedure getEntityType(result)
    result="Routine"
Endproc
Procedure getEntityParmList(result)
    result="pCommandExec"
Endproc
