* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_setuptrace_b                                                 *
*              Messa a punto log                                               *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-24                                                      *
* Last revis.: 2009-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject,pAZIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
Private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
Createobject("tcp_setuptrace_b",oParentObject,m.pAZIONE)
Return(i_retval)

Define Class tcp_setuptrace_b As StdBatch
    * --- Local variables
    pAZIONE = Space(1)
    w_POSIZIONEZIP = Space(250)
    w_OLD_ACTIVATEPROFILER = 0
    w_FILECNFSTR = Space(250)
    w_LUNGHEZZA = 0
    w_POSIZIONECORRENTE = 0
    w_ACAPO = Space(2)
    w_VARIABILE = Space(10)
    w_VALORE = Space(10)
    w_TROVATO = .F.
    * --- WorkFile variables
    runtime_filters = 1

    Procedure Pag1
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        * --- Messa a punto log
        Do Case
            Case This.pAZIONE = "I"
                * --- Inizio
                *     Popola le variabili della maschera
                This.oParentObject.w_ATTIVATO = i_nACTIVATEPROFILER
                This.oParentObject.w_POSIZIONELOG = i_cACTIVATEPROFILER_POSIZIONELOG
                * --- Gesione multisessione
                If i_nACTIVATEPROFILER_MULTISESSION = 1
                    If Empty( i_nACTIVATEPROFILER_MULTISESSION )
                        This.oParentObject.w_SESSIONNAME = Sys( 2015 )
                    Else
                        This.oParentObject.w_SESSIONNAME = i_cACTIVATEPROFILER_SESSIONNAME
                    Endif
                    This.oParentObject.w_NOMELOG = Strtran( i_cACTIVATEPROFILER_NOMELOG , i_cACTIVATEPROFILER_SESSIONNAME , "" )
                Else
                    This.oParentObject.w_SESSIONNAME = ""
                    This.oParentObject.w_NOMELOG = i_cACTIVATEPROFILER_NOMELOG
                Endif
                This.oParentObject.w_TIPOROLLOVER = i_cACTIVATEPROFILER_TIPOROLLOVER
                If i_nACTIVATEPROFILER_ROLLOVER <> 0
                    This.oParentObject.w_ROLLOVER = i_nACTIVATEPROFILER_ROLLOVER
                    This.oParentObject.o_TIPOROLLOVER = This.oParentObject.w_TIPOROLLOVER
                Endif
                This.oParentObject.w_NUMERODIFILEDILOG = i_nACTIVATEPROFILER_NUMERODIFILEDILOG
                This.oParentObject.w_TRACCIATURAGESTIONI = i_nACTIVATEPROFILER_TRACCIATURAGESTIONI
                This.oParentObject.w_TRACCIATURAROUTINE = i_nACTIVATEPROFILER_TRACCIATURAROUTINE
                This.oParentObject.w_TRACCIATURAQUERYASINCRONA = i_nACTIVATEPROFILER_TRACCIATURAQUERYASINCRONA
                This.oParentObject.w_TRACCIATURASQLLETTURA = i_nACTIVATEPROFILER_TRACCIATURASQLLETTURA
                This.oParentObject.w_TRACCIATURASQLSCRITTURA = i_nACTIVATEPROFILER_TRACCIATURASQLSCRITTURA
                This.oParentObject.w_TRACCIATURASQLINSERIMENTO = i_nACTIVATEPROFILER_TRACCIATURASQLINSERIMENTO
                This.oParentObject.w_TRACCIATURASQLCANCELLAZIONE = i_nACTIVATEPROFILER_TRACCIATURASQLCANCELLAZIONE
                This.oParentObject.w_TRACCIATURASQLTEMPORANEO = i_nACTIVATEPROFILER_TRACCIATURASQLTEMPORANEO
                This.oParentObject.w_TRACCIATURASQLMODIFICADB = i_nACTIVATEPROFILER_TRACCIATURASQLMODIFICADB
                This.oParentObject.w_TRACCIATURASQLALTROSUDB = i_nACTIVATEPROFILER_TRACCIATURASQLALTROSUDB
                This.oParentObject.w_TRACCIATURATRANSAZIONI = i_nACTIVATEPROFILER_TRACCIATURATRANSAZIONI
                This.oParentObject.w_TRACCIATURADEADLOCK = i_nACTIVATEPROFILER_TRACCIATURADEADLOCK
                This.oParentObject.w_TRACCIATURARICONNESSIONE = i_nACTIVATEPROFILER_TRACCIATURARICONNESSIONE
                This.oParentObject.w_TRACCIATURATASTIFUN = i_nACTIVATEPROFILER_TRACCIATURATASTIFUN
                This.oParentObject.w_TRACCIATURAEVENTI = i_nACTIVATEPROFILER_TRACCIATURAEVENTI
                This.oParentObject.w_RIGHE = i_nACTIVATEPROFILER_RIGHE
                This.oParentObject.w_MILLISECONDI = i_nACTIVATEPROFILER_MILLISECONDI
                This.oParentObject.w_MULTISESSION = i_nACTIVATEPROFILER_MULTISESSION
                This.oParentObject.w_TRACCIATURAVQR = i_nACTIVATEPROFILER_TRACCIATURAVQR
            Case This.pAZIONE = "S"
                * --- Inizio
                *     Valorizza le variabili pubbliche
                i_nACTIVATEPROFILER = This.oParentObject.w_ATTIVATO
                If Alltrim( i_cACTIVATEPROFILER_POSIZIONELOG ) <> Alltrim( This.oParentObject.w_POSIZIONELOG ) Or Alltrim( i_cACTIVATEPROFILER_NOMELOG ) <> Alltrim( This.oParentObject.w_NOMELOG )
                    * --- Se cambia la posizione ( o il nome ) del file di log, occorre chiudere il log corrente
                    If Used( Alltrim( i_cACTIVATEPROFILER_POSIZIONELOG ) )
                        Select( Alltrim( i_cACTIVATEPROFILER_POSIZIONELOG ) )
                        Use
                    Endif
                Endif
                i_cACTIVATEPROFILER_POSIZIONELOG = Alltrim( This.oParentObject.w_POSIZIONELOG )
                * --- L'utente vede il nome senza la sessione
                If Empty( This.oParentObject.w_NOMELOG )
                    This.oParentObject.w_NOMELOG = "ActivityLogger"
                Endif
                If Alltrim( i_cACTIVATEPROFILER_NOMELOG ) <> Alltrim( This.oParentObject.w_NOMELOG ) + Iif( This.oParentObject.w_MULTISESSION = 1 , Alltrim( This.oParentObject.w_SESSIONNAME ) , "" )
                    If Used( Alltrim( i_cACTIVATEPROFILER_NOMELOG ) )
                        Select( Alltrim( i_cACTIVATEPROFILER_NOMELOG ) )
                        Use
                    Endif
                Endif
                i_cACTIVATEPROFILER_NOMELOG = Alltrim( This.oParentObject.w_NOMELOG ) + Iif( This.oParentObject.w_MULTISESSION = 1 , Alltrim( This.oParentObject.w_SESSIONNAME ) , "" )
                If Vartype( i_nACTIVATEPROFILER_ALIAS ) <> "C"
                    Public i_nACTIVATEPROFILER_ALIAS
                Else
                    If Used( i_nACTIVATEPROFILER_ALIAS )
                        Use In (i_nACTIVATEPROFILER_ALIAS)
                    Endif
                Endif
                i_nACTIVATEPROFILER_ALIAS = Sys(2015)
                i_cACTIVATEPROFILER_TIPOROLLOVER = This.oParentObject.w_TIPOROLLOVER
                i_nACTIVATEPROFILER_ROLLOVER = This.oParentObject.w_ROLLOVER
                i_nACTIVATEPROFILER_NUMERODIFILEDILOG = This.oParentObject.w_NUMERODIFILEDILOG
                i_nACTIVATEPROFILER_TRACCIATURAGESTIONI = This.oParentObject.w_TRACCIATURAGESTIONI
                i_nACTIVATEPROFILER_TRACCIATURAROUTINE = This.oParentObject.w_TRACCIATURAROUTINE
                i_nACTIVATEPROFILER_TRACCIATURAQUERYASINCRONA = This.oParentObject.w_TRACCIATURAQUERYASINCRONA
                i_nACTIVATEPROFILER_TRACCIATURASQLLETTURA = This.oParentObject.w_TRACCIATURASQLLETTURA
                i_nACTIVATEPROFILER_TRACCIATURASQLSCRITTURA = This.oParentObject.w_TRACCIATURASQLSCRITTURA
                i_nACTIVATEPROFILER_TRACCIATURASQLINSERIMENTO = This.oParentObject.w_TRACCIATURASQLINSERIMENTO
                i_nACTIVATEPROFILER_TRACCIATURASQLCANCELLAZIONE = This.oParentObject.w_TRACCIATURASQLCANCELLAZIONE
                i_nACTIVATEPROFILER_TRACCIATURASQLTEMPORANEO = This.oParentObject.w_TRACCIATURASQLTEMPORANEO
                i_nACTIVATEPROFILER_TRACCIATURASQLMODIFICADB = This.oParentObject.w_TRACCIATURASQLMODIFICADB
                i_nACTIVATEPROFILER_TRACCIATURASQLALTROSUDB = This.oParentObject.w_TRACCIATURASQLALTROSUDB
                i_nACTIVATEPROFILER_TRACCIATURATRANSAZIONI = This.oParentObject.w_TRACCIATURATRANSAZIONI
                i_nACTIVATEPROFILER_TRACCIATURADEADLOCK = This.oParentObject.w_TRACCIATURADEADLOCK
                i_nACTIVATEPROFILER_TRACCIATURARICONNESSIONE = This.oParentObject.w_TRACCIATURARICONNESSIONE
                i_nACTIVATEPROFILER_MULTISESSION = This.oParentObject.w_MULTISESSION
                i_cACTIVATEPROFILER_SESSIONNAME = This.oParentObject.w_SESSIONNAME
                i_nACTIVATEPROFILER_RIGHE = This.oParentObject.w_RIGHE
                i_nACTIVATEPROFILER_MILLISECONDI = This.oParentObject.w_MILLISECONDI
                i_nACTIVATEPROFILER_TRACCIATURAVQR = This.oParentObject.w_TRACCIATURAVQR
                i_nACTIVATEPROFILER_TRACCIATURATASTIFUN = This.oParentObject.w_TRACCIATURATASTIFUN
                i_nACTIVATEPROFILER_TRACCIATURAEVENTI = This.oParentObject.w_TRACCIATURAEVENTI
            Case This.pAZIONE = "G"
                * --- Zoom sulla posizione del file di log
                This.oParentObject.w_POSIZIONELOG = Getdir()
                This.oParentObject.NOTIFYEVENT("w_POSIZIONELOG Changed")
            Case This.pAZIONE = "Z"
                * --- Crea la cartella compressa con il contenuto del log
                This.w_POSIZIONEZIP = Getfile( "7Z" , "Selezionare un file per lo zip" )
                If Empty( This.w_POSIZIONEZIP )
                    cp_ERRORMSG("Operazione abbandonata","i","")
                    i_retcode = 'stop'
                    Return
                Endif
                This.w_OLD_ACTIVATEPROFILER = i_nACTIVATEPROFILER
                * --- Disattivazione log
                i_nACTIVATEPROFILER = 0
                If Upper( Alltrim( Right( This.w_POSIZIONEZIP , 3 ) ) ) <> ".7Z"
                    This.w_POSIZIONEZIP = Alltrim( This.w_POSIZIONEZIP ) + "7Z"
                Endif
                If Used( Alltrim( i_cACTIVATEPROFILER_NOMELOG ) )
                    Select( Alltrim( i_cACTIVATEPROFILER_NOMELOG ) )
                    Use
                Endif
                If Vartype( i_nACTIVATEPROFILER_ALIAS ) = "C"
                    If Used( Alltrim( i_nACTIVATEPROFILER_ALIAS ) )
                        Select( Alltrim( i_nACTIVATEPROFILER_ALIAS ) )
                        Use
                    Endif
                Endif
                Delete File (This.w_POSIZIONEZIP)
                L_ERRORE = 0
                If ZIPUNZIP("Z",This.w_POSIZIONEZIP,Addbs(Alltrim(This.oParentObject.w_POSIZIONELOG)),"",@L_ERRORE)
                    * --- Operazione eseguita con successo
                    cp_ERRORMSG("Operazione eseguita","i","")
                Else
                    _Cliptext = L_ERRORE
                    If Type("L_ERRORE")="C"
                        cp_ERRORMSG("Operazione fallita:"+Chr(13)+L_ERRORE,"!","")
                    Else
                        cp_ERRORMSG("Operazione fallita","!","")
                    Endif
                Endif
                * --- Riattivazione log
                i_nACTIVATEPROFILER = This.w_OLD_ACTIVATEPROFILER
            Case This.pAZIONE = "C"
                * --- Salva le impostazioni nel file CNF
                This.oParentObject.w_FILECNF = Getfile("CNF")
                If Empty( This.oParentObject.w_FILECNF )
                    cp_ERRORMSG("Operazione interrotta","i","")
                    i_retcode = 'stop'
                    Return
                Endif
                * --- Copia il file in una stringa
                If cp_FileExist(This.oParentObject.w_FILECNF)
                    This.w_FILECNFSTR = Filetostr( This.oParentObject.w_FILECNF )
                Else
                    This.w_FILECNFSTR = ""
                Endif
                * --- calcola il carattere utilizzato per l'invio a capo
                This.w_ACAPO = Chr( 13 ) + Chr ( 10 )
                If Not This.w_ACAPO $ This.w_FILECNFSTR And Chr( 10 ) $ This.w_FILECNFSTR
                    This.w_ACAPO = Chr ( 10 )
                Endif
                If Not This.w_ACAPO $ This.w_FILECNFSTR And Chr( 13 ) $ This.w_FILECNFSTR
                    This.w_ACAPO = Chr ( 13 )
                Endif
                * --- Viene copiato il contenuto in un array, compresi gli invii a capo
                Alines(ARRAYCNF, This.w_FILECNFSTR, 16)
                * --- Variabili da cercare e sostituire e relativo valore da assegnare
                This.w_VARIABILE = "i_nACTIVATEPROFILER"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_ATTIVATO ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_cACTIVATEPROFILER_POSIZIONELOG"
                This.w_VALORE = '"' + Strtran( Alltrim( This.oParentObject.w_POSIZIONELOG ) , '"', '""' ) + '"'
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_cACTIVATEPROFILER_NOMELOG"
                This.w_VALORE = '"' + Strtran( Alltrim( This.oParentObject.w_NOMELOG ) , '"', '""' ) + '"'
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_ROLLOVER"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_ROLLOVER ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_cACTIVATEPROFILER_TIPOROLLOVER"
                This.w_VALORE = "'" + This.oParentObject.w_TIPOROLLOVER + "'"
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_NUMERODIFILEDILOG"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_NUMERODIFILEDILOG ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURAGESTIONI"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_TRACCIATURAGESTIONI ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURAROUTINE"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_TRACCIATURAROUTINE ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURAQUERYASINCRONA"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_TRACCIATURAQUERYASINCRONA ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURASQLLETTURA"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_TRACCIATURASQLLETTURA ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURASQLSCRITTURA"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_TRACCIATURASQLSCRITTURA ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURASQLINSERIMENTO"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_TRACCIATURASQLINSERIMENTO ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURASQLCANCELLAZIONE"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_TRACCIATURASQLCANCELLAZIONE ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURASQLTEMPORANEO"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_TRACCIATURASQLTEMPORANEO ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURASQLMODIFICADB"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_TRACCIATURASQLMODIFICADB ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURASQLALTROSUDB"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_TRACCIATURASQLALTROSUDB ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURATRANSAZIONI"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_TRACCIATURATRANSAZIONI ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURADEADLOCK"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_TRACCIATURADEADLOCK ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURARICONNESSIONE"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_TRACCIATURARICONNESSIONE ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_MULTISESSION"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_MULTISESSION ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_RIGHE"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_RIGHE ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_MILLISECONDI"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_MILLISECONDI ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURAVQR"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_TRACCIATURAVQR ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURAEVENTI"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_TRACCIATURAEVENTI ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                This.w_VARIABILE = "i_nACTIVATEPROFILER_TRACCIATURATASTIFUN"
                This.w_VALORE = Alltrim( Str ( This.oParentObject.w_TRACCIATURATASTIFUN ) )
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                * --- --
                * --- Ricostruisce la stringa da copiare nel file
                This.w_LUNGHEZZA = Alen( ARRAYCNF )
                This.w_POSIZIONECORRENTE = 1
                This.w_FILECNFSTR = ""
                Do While This.w_POSIZIONECORRENTE <= This.w_LUNGHEZZA
                    This.w_FILECNFSTR = This.w_FILECNFSTR + ARRAYCNF(This.w_POSIZIONECORRENTE)
                    This.w_POSIZIONECORRENTE = This.w_POSIZIONECORRENTE + 1
                Enddo
                * --- Aggiorna il file CNF
                VecchioErrore=On("error")
                w_bErrore = .F.
                On Error w_bErrore = .T.
                Strtofile( This.w_FILECNFSTR, This.oParentObject.w_FILECNF )
                On Error &VecchioErrore
                If w_bErrore
                    cp_ERRORMSG("Operazione fallita."+Chr(13)+"Controllare i diritti di accesso al file","!","")
                Else
                    cp_ERRORMSG("Operazione eseguita","i","")
                Endif
            Case This.pAZIONE = "U"
                * --- Decomprime la cartella compressa contenente il log, all'interno della
                *     cartella corrente di log
                This.w_POSIZIONEZIP = Getfile( "7Z" , "Selezionare un file da decomprimere" )
                If Empty( This.w_POSIZIONEZIP )
                    cp_ERRORMSG("Operazione abbandonata","i","")
                    i_retcode = 'stop'
                    Return
                Endif
                L_ERRORE = 0
                If ZIPUNZIP("U",This.w_POSIZIONEZIP,Addbs(Alltrim(This.oParentObject.w_POSIZIONELOG))+Addbs(Juststem(This.w_POSIZIONEZIP)),"",@L_ERRORE)
                    cp_ERRORMSG("Operazione eseguita","i","")
                Else
                    _Cliptext = L_ERRORE
                    If Type("L_ERRORE")="C"
                        cp_ERRORMSG("Operazione fallita:"+Chr(13)+L_ERRORE,"!","")
                    Else
                        cp_ERRORMSG("Operazione fallita","!","")
                    Endif
                Endif
            Case This.pAZIONE = "D"
                * --- Controlla se la cartella di destinazione del log � su una unit� locale
                If Not Empty( Justdrive( This.oParentObject.w_POSIZIONELOG ) )
                    If Drivetype( Justdrive( This.oParentObject.w_POSIZIONELOG ) ) = 1
                        cp_ERRORMSG( "Le unit� diverse da disco rigido potrebbero non essere adatte a contenere la cartella del log" ,"!" , "" )
                    Endif
                    If Drivetype( Justdrive( This.oParentObject.w_POSIZIONELOG ) ) = 2
                        cp_ERRORMSG( "Le unit� disco di tipo floppy potrebbero non essere adatte a contenere la cartella del log" ,"!" , "" )
                    Endif
                    If Drivetype( Justdrive( This.oParentObject.w_POSIZIONELOG ) ) = 4
                        cp_ERRORMSG( "Le unit� disco di rete o removibili potrebbero non essere adatte a contenere la cartella del log" ,"!" , "" )
                    Endif
                    If Drivetype( Justdrive( This.oParentObject.w_POSIZIONELOG ) ) = 5
                        cp_ERRORMSG( "Le unit� disco di tipo CD-ROM potrebbero non essere adatte a contenere la cartella del log" ,"!" , "" )
                    Endif
                    If Drivetype( Justdrive( This.oParentObject.w_POSIZIONELOG ) ) = 6
                        cp_ERRORMSG( "Le unit� disco di tipo RAM-DISK potrebbero non essere adatte a contenere la cartella del log" ,"!" , "" )
                    Endif
                Endif
        Endcase
    Endproc


    Procedure Pag2
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        This.w_LUNGHEZZA = Alen( ARRAYCNF )
        This.w_POSIZIONECORRENTE = 1
        This.w_TROVATO = .F.
        Do While This.w_POSIZIONECORRENTE <= This.w_LUNGHEZZA
            If Left( Alltrim( ARRAYCNF(This.w_POSIZIONECORRENTE) ) , 1 ) <> "*" And Upper(This.w_VARIABILE)+"=" $ Strtran(Upper(ARRAYCNF(This.w_POSIZIONECORRENTE))," ","")
                * --- Trovata la stringa da sostituire
                ARRAYCNF(This.w_POSIZIONECORRENTE) = This.w_VARIABILE + " = " + This.w_VALORE + This.w_ACAPO
                This.w_TROVATO = .T.
            Endif
            This.w_POSIZIONECORRENTE = This.w_POSIZIONECORRENTE + 1
        Enddo
        If Not This.w_TROVATO
            * --- Se nell'ultimo elemento non c'� l'invio a capo lo aggiunge
            If Not This.w_ACAPO $ ARRAYCNF(This.w_LUNGHEZZA)
                ARRAYCNF(This.w_LUNGHEZZA) = ARRAYCNF(This.w_LUNGHEZZA) + This.w_ACAPO
            Endif
            * --- Aggiunge una riga
            Dimension ARRAYCNF( This.w_LUNGHEZZA+1)
            ARRAYCNF( This.w_LUNGHEZZA+1) = This.w_VARIABILE + " = " + This.w_VALORE + This.w_ACAPO
        Endif
        This.w_TROVATO = .F.
    Endproc


    Proc Init(oParentObject,pAZIONE)
        This.pAZIONE=pAZIONE
        DoDefault(oParentObject)
        Return
    Function OpenTables()
        Dimension This.cWorkTables[max(1,0)]
        Return(This.OpenAllTables(0))

        * --- Area Manuale = Functions & Procedures
        * --- Fine Area Manuale
Enddefine

Procedure getEntityType(result)
    result="Routine"
Endproc
Procedure getEntityParmList(result)
    result="pAZIONE"
Endproc
