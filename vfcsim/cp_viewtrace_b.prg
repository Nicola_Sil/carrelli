* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_viewtrace_b                                                  *
*              Visualizza log                                                  *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-24                                                      *
* Last revis.: 2009-10-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject,pAZIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
Private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
Createobject("tcp_viewtrace_b",oParentObject,m.pAZIONE)
Return(i_retval)

Define Class tcp_viewtrace_b As StdBatch
    * --- Local variables
    pAZIONE = Space(1)
    w_SHOWDBF_CURS = .Null.
    w_POSIZIONE = 0
    w_NUMEROELEMENTI = 0
    w_ELEMENTOCORRENTE = 0
    w_CP_LOGUNION = .Null.
    * --- WorkFile variables
    runtime_filters = 1

    Procedure Pag1
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        * --- Routine di gestione dell' Activity logger viewer
        *     pAZIONE = 'D' mostra i dati dell'istruzione select corrente memorizzati in un DBF al momento dell'esecuzione del programma tracciato
        *     pAZIONE = 'Z' azzera il risultato della traccia corrente
        *     pAZIONE = 'F' Seleziona un file di log da visualizzare
        *     pAZIONE = 'Q' Lanciato dal bottone ricerca a pag2. Si posiziona sul primo record in cui la stringa 'Cerca in frase SQL' (pag2) � contenuta nella frase sql o nel messaggio di errore
        *     pAZIONE = 'R' Lanciato dal bottone ricerca a pag1. Richiede una stringa e si posiziona sul primo record in cui la stringa stessa � contenuta nella frase sql o nel messaggio di errore
        *     pAZIONE = 'S' Lanciato dal bottone 'Successivo' ricerca a pag1. Si posiziona sul record successivo che soddisfa la ricerca fatta con parametro Q o R
        * --- --
        Do Case
            Case This.pAZIONE = "D"
                * --- Mostra i dati dell'istruzione eseguita
                VecchioErrore3 = On("error")
                bErroreAperturaDBF=.F.
                On Error bErroreAperturaDBF=.T.
                This.w_SHOWDBF_CURS = Sys( 2015 )
                Select * From ( This.oParentObject.w_NOMECURSOREDATI ) Into Cursor ( This.w_SHOWDBF_CURS )
                Do CP_SHOWDBF With This
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                If Used( Justfname( This.oParentObject.w_NOMECURSOREDATI ) )
                    Select( Justfname( This.oParentObject.w_NOMECURSOREDATI ) )
                    Use
                Endif
                If bErroreAperturaDBF
                    cp_ErrorMsg("Non � possibile aprire il DBF","!","")
                Endif
                On Error &VecchioErrore3
            Case This.pAZIONE = "Z"
                * --- Svuota il log
                * --- Mostra i dati dell'istruzione eseguita
                If CP_YESNO("Si desidera azzerare il log?")
                    VecchioErrore3 = On("error")
                    bErroreAperturaDBF=.F.
                    On Error bErroreAperturaDBF=.T.
                    If File( This.oParentObject.w_FILEDILOG )
                        * --- prima di eliminare il logo devo elminare tutti i dbf di lettura ad esso associati
                        If Not Used(i_nACTIVATEPROFILER_ALIAS)
                            * --- Attiva il cursore del log per poter eseguire la scansione
                            If i_nACTIVATEPROFILER_TRACCIATURAEVENTI = 0
                                i_nACTIVATEPROFILER_TRACCIATURAEVENTI = 1
                                cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UEV", "", This, 0, 0, "")
                                i_nACTIVATEPROFILER_TRACCIATURAEVENTI = 0
                            Else
                                cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UEV", "", This, 0, 0, "")
                            Endif
                        Endif
                        Select( i_nACTIVATEPROFILER_ALIAS )
                        Go Top
                        Scan For Not Empty(Nvl(ALCRDATI,""))
                            * --- Provo ad elminiare il dbf se non riesco lo lascio, non diamo avvisi
                            Try
                                cp_msg("Eliminazione DBF..",.T.,.F.,0,.F.)
                                Drop Table ( Addbs(Justpath(This.oParentObject.w_FILEDILOG) )+Alltrim(ALCRDATI)+".dbf" )
                            Catch
                            Endtry
                        Endscan
                        Delete From ( This.oParentObject.w_FILEDILOG )
                    Else
                        bErroreAperturaDBF=.T.
                    Endif
                    If bErroreAperturaDBF
                        cp_ErrorMsg("Non � possibile azzerare il log","!","")
                    Endif
                    On Error &VecchioErrore3
                    This.oParentObject.w_ELENCO.Query()
                    This.oParentObject.w_ELENCO.Refresh()
                Endif
            Case This.pAZIONE = "F"
                * --- Seleziona un file di log da visualizzare
                This.oParentObject.w_FILEDILOG = Getfile( "DBF" , "Selezionare un file di log" )
            Case This.pAZIONE = "Q" Or This.pAZIONE = "R" Or This.pAZIONE = "S"
                * --- R ricerca
                * --- S ricerca successivo
                * --- Q ricerca da pag2
                * --- Ricerca la frase SQL contenuta nella variabile w_CONTENUTOINFRASESQL
                Select( This.oParentObject.w_ELENCO.cCURSOR )
                This.w_POSIZIONE = Recno( This.oParentObject.w_ELENCO.cCURSOR )
                If This.pAZIONE = "R" Or This.pAZIONE = "Q"
                    If This.pAZIONE = "R"
                        * --- Chiede la stringa solo se lanciato da pag2
                        This.oParentObject.w_CONTENUTOINFRASESQL = Inputbox("Cerca in istruzione SQL:", i_MsgTitle)
                    Endif
                    * --- Cerca la prima occorrenza all'interno dell'istruzione SQL (ALCMDSQL) oppure nel messaggio di errore ( ALERROR )
                    *     La ricerca avviene tramite l'operatore $ (contenuto in).
                    L_FRASE = "LOCATE FOR " + CP_TOSTR( Upper( Alltrim( This.oParentObject.w_CONTENUTOINFRASESQL ) ) ) + " $ UPPER( ALCMDSQL ) OR " + CP_TOSTR( Upper( Alltrim( This.oParentObject.w_CONTENUTOINFRASESQL ) ) ) + " $ UPPER( ALERROR )"
                    L_FILTRO = Upper(Alltrim(This.oParentObject.w_CONTENUTOINFRASESQL))
                    L_FRASE = "LOCATE FOR UPPER(ALLTRIM(this.oParentObject.w_CONTENUTOINFRASESQL)) $ UPPER( ALCMDSQL ) OR UPPER(ALLTRIM(this.oParentObject.w_CONTENUTOINFRASESQL)) $ UPPER( ALERROR )"
                Else
                    * --- Cerca l'occorrenza successiva dell'ultima locate applicata
                    L_FRASE = "CONTINUE"
                Endif
                If Not Empty( This.oParentObject.w_CONTENUTOINFRASESQL )
                    &L_FRASE
                    If Found()
                        If This.oParentObject.opgfrm.ActivePage <> 1
                            * --- Se la ricerca � lanciata dal bottone sulla seconda pagina
                            This.oParentObject.opgfrm.ActivePage=1
                        Endif
                        * --- Abilita il bottone per la ricerca del record successivo
                        This.oParentObject.w_TROVASUCCESSIVO = .T.
                    Else
                        If This.w_POSIZIONE >0 And This.w_POSIZIONE <= Reccount( This.oParentObject.w_ELENCO.cCURSOR )
                            * --- La ricerca � fallita viene ripristinata la posizione originale e disabilito la ricerca successiva
                            Goto This.w_POSIZIONE
                            If This.pAZIONE = "R"
                                cp_ErrorMsg("La ricerca non ha prodotto risultati" , "i" , "" )
                            Else
                                cp_ErrorMsg("� stata raggiunta la fine del log" , "i" , "" )
                            Endif
                            This.oParentObject.w_TROVASUCCESSIVO = .F.
                        Endif
                    Endif
                    * --- Aggiorna lo zoom e passa il controllo alla griglia
                    This.oParentObject.w_ELENCO.Refresh()
                    This.oParentObject.w_ELENCO.SetFocus()
                Endif
            Case This.pAZIONE = "U"
                * --- Costruisce l'elenco dei dbf che possono essere uniti
                Adir(A_ELENCOFILEDILOG,Alltrim(This.oParentObject.w_CARTELLALOG)+"*.dbf")
                * --- A_ELENCOFILEDILOG contiene l'elenco dei dbf presenti nella cartella adibita a log.
                *     Di questi verranno tenuti solo quelli che contengono i campi previsti
                Create Cursor ELENCODBFDAUNIRE( NOMECURS C(250) )
                =WRCURSOR("ELENCODBFDAUNIRE")
                This.w_NUMEROELEMENTI = Alen( A_ELENCOFILEDILOG , 1)
                This.w_ELEMENTOCORRENTE = 1
                Do While This.w_ELEMENTOCORRENTE <= This.w_NUMEROELEMENTI
                    * --- Controlla se il DBF corrente contiene i campi previsti
                    L_ISTRUZIONETEST = "SELECT * FROM " + Alltrim( This.oParentObject.w_CARTELLALOG ) + Alltrim( A_ELENCOFILEDILOG( This.w_ELEMENTOCORRENTE , 1 ) ) + " WHERE 1=0 INTO CURSOR TESTCAMPI"
                    &L_ISTRUZIONETEST
                    If Type( "TESTCAMPI.ALDATTIM" ) = "T" And Type( "TESTCAMPI.AL____MS" ) = "N" And Type( "TESTCAMPI.AL__TIPO" ) = "C" And Type( "TESTCAMPI.ALCMDSQL" ) = "M"
                        If Len( Alltrim( This.oParentObject.w_CARTELLALOG ) + Alltrim( A_ELENCOFILEDILOG( This.w_ELEMENTOCORRENTE , 1 ) ) ) <= 254
                            Select( "ELENCODBFDAUNIRE" )
                            Append Blank
                            Replace NOMECURS With Alltrim( This.oParentObject.w_CARTELLALOG ) + Alltrim( A_ELENCOFILEDILOG( This.w_ELEMENTOCORRENTE , 1 ) )
                        Endif
                    Endif
                    This.w_ELEMENTOCORRENTE = This.w_ELEMENTOCORRENTE + 1
                Enddo
                This.w_CP_LOGUNION = cp_logunion( This )
                If Used( "TESTCAMPI" )
                    Select( "TESTCAMPI" )
                    Use
                Endif
                Select( "ELENCODBFDAUNIRE" )
                Browse
        Endcase
    Endproc


    Proc Init(oParentObject,pAZIONE)
        This.pAZIONE=pAZIONE
        DoDefault(oParentObject)
        Return
    Function OpenTables()
        Dimension This.cWorkTables[max(1,0)]
        Return(This.OpenAllTables(0))

        * --- Area Manuale = Functions & Procedures
        * --- Fine Area Manuale
Enddefine

Procedure getEntityType(result)
    result="Routine"
Endproc
Procedure getEntityParmList(result)
    result="pAZIONE"
Endproc
