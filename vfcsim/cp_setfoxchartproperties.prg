* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_setfoxchartproperties                                        *
*              SetFoxChartProperties                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-10-07                                                      *
* Last revis.: 2010-05-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
Private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
Createobject("tcp_setfoxchartproperties",oParentObject,m.pOPER)
Return(i_retval)

Define Class tcp_setfoxchartproperties As StdBatch
    * --- Local variables
    pOPER = Space(10)
    w_oChart = .Null.
    w_objProp = .Null.
    w_oParentObject = .Null.
    w_ParentClass = Space(10)
    cp_foxcharteditor = .Null.
    cp_foxchartproperties = .Null.
    cp_foxchartfields = .Null.
    oChart = .Null.
    w_FIELDSCOUNT = 0
    w_i = 0
    w_Cursor = Space(10)
    w_CLEGEND = Space(25)
    w_CCOLOR = Space(25)
    w_CCOLOR = Space(25)
    w_ChartsCount = 0
    w_CURRENTROW = 0
    * --- WorkFile variables
    runtime_filters = 2

    Procedure Pag1
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        * --- Definisco le variabili degli oggetti chiamanti
        This.w_ParentClass = Upper(Alltrim(This.oParentObject.Class))
        Do Case
            Case This.w_ParentClass = "TCP_FOXCHARTEDITOR"
                This.w_oChart = This.oParentObject.w_oChart
            Case This.w_ParentClass = "TCCP_FOXCHARTPROPERTIES"
                This.w_oChart = This.oParentObject.oParentObject.w_oChart
            Case This.w_ParentClass = "TCCP_FOXCHARTFIELDS"
                This.w_oChart = This.oParentObject.oParentObject.oParentObject.w_oChart
        Endcase
        This.oChart = This.w_oChart.oChart
        This.cp_foxcharteditor = This.w_oChart.oParentObject
        This.cp_foxchartproperties = This.cp_foxcharteditor.cp_foxchartproperties
        This.cp_foxchartfields = This.cp_foxchartproperties.cp_foxchartfields
        Do Case
            Case This.pOPER $ "SETLEGENDS-GETLEGENDS"
                This.w_oChart = This.oParentObject.w_oChart
                Do Case
                    Case This.oParentObject.w_LEGENDS=0
                        This.w_objProp = This.w_oChart.oChart.Title
                    Case This.oParentObject.w_LEGENDS=1
                        This.w_objProp = This.w_oChart.oChart.SubTitle
                    Case This.oParentObject.w_LEGENDS=2
                        This.w_objProp = This.w_oChart.oChart.XAxis
                    Case This.oParentObject.w_LEGENDS=3
                        This.w_objProp = This.w_oChart.oChart.AxisLegend2
                    Case This.oParentObject.w_LEGENDS=4
                        This.w_objProp = This.w_oChart.oChart.YAxis
                    Case This.oParentObject.w_LEGENDS=5
                        This.w_objProp = This.w_oChart.oChart.ShapeLegend
                    Case This.oParentObject.w_LEGENDS=6
                        This.w_objProp = This.w_oChart.oChart.ScaleLegend
                    Case This.oParentObject.w_LEGENDS=7
                        This.w_objProp = This.w_oChart.oChart.SideLegend
                Endcase
                Do Case
                    Case This.pOPER == "SETLEGENDS"
                        This.w_objProp.Caption = Alltrim(This.oParentObject.w_Caption)
                        This.w_objProp.Format = Alltrim(This.oParentObject.w_Format)
                        This.w_objProp.FontName = This.oParentObject.w_FontName
                        This.w_objProp.FontBold = This.oParentObject.w_FontBold
                        This.w_objProp.FontItalic = This.oParentObject.w_FontItalic
                        This.w_objProp.FontSize = This.oParentObject.w_FontSize
                        This.w_objProp.FontUnderline = This.oParentObject.w_FontUnderline
                        This.w_objProp.ForeColor = This.oParentObject.w_LForeColor
                        This.w_objProp.BackColor = This.oParentObject.w_LBackColor
                        This.w_objProp.ForeColorAlpha = This.oParentObject.w_LForeColorAlpha
                        This.w_objProp.BackColorAlpha = This.oParentObject.w_LBackColorAlpha
                        This.w_objProp.Alignment = This.oParentObject.w_Alignment
                        This.w_objProp.Rotation = This.oParentObject.w_Rotation
                        This.w_objProp.RotationCenter = This.oParentObject.w_RotationCenter
                    Case This.pOPER == "GETLEGENDS"
                        This.cp_foxchartproperties.SaveDependsOn()
                        This.oParentObject.w_Caption = This.w_objProp.Caption
                        This.oParentObject.w_Format = This.w_objProp.Format
                        This.oParentObject.w_FontName = This.w_objProp.FontName
                        This.oParentObject.w_FontBold = This.w_objProp.FontBold
                        This.oParentObject.w_FontItalic = This.w_objProp.FontItalic
                        This.oParentObject.w_FontSize = This.w_objProp.FontSize
                        This.oParentObject.w_FontUnderline = This.w_objProp.FontUnderline
                        This.oParentObject.w_LForeColor = This.w_objProp.ForeColor
                        This.cp_foxchartproperties.w_oForeColorChartL.Calculate("      1234567890     ",This.oParentObject.w_LForeColor,This.oParentObject.w_LForeColor)
                        This.oParentObject.w_LBackColor = This.w_objProp.BackColor
                        This.cp_foxchartproperties.w_oBckColorChartL.Calculate("      1234567890     ",This.oParentObject.w_LBackColor,This.oParentObject.w_LBackColor)
                        This.oParentObject.w_LForeColorAlpha = This.w_objProp.ForeColorAlpha
                        This.oParentObject.w_LBackColorAlpha = This.w_objProp.BackColorAlpha
                        This.oParentObject.w_Alignment = This.w_objProp.Alignment
                        This.oParentObject.w_Rotation = This.w_objProp.Rotation
                        This.oParentObject.w_RotationCenter = This.w_objProp.RotationCenter
                Endcase
            Case This.pOPER ="ADDFIELDSPROP"
                This.w_Cursor = Sys(2015)
                If !Empty(This.cp_foxchartproperties.w_QUERYMODEL)
                    vq_exec( This.cp_foxchartproperties.w_QUERYMODEL ,This, This.w_Cursor )
                    This.w_FIELDSCOUNT = Afields(ArrayFields) && Create array.
                    If This.w_FIELDSCOUNT > 0
                        This.w_i = 1
                        Select( This.cp_foxchartfields.cTrsName)
                        Zap
                        Do While This.w_i <= This.w_FIELDSCOUNT
                            This.cp_foxchartfields.InitRow()
                            This.cp_foxchartfields.w_FD_VALUE = Alltrim(ArrayFields( This.w_i, 1))
                            * --- Verifico se nel cursore esiste la colonna della leggenda relativa al fieldvalue
                            This.w_CLEGEND = Upper(Alltrim(ArrayFields( This.w_i, 1))+"_LEGEND")
                            If Ascan(ArrayFields , This.w_CLEGEND) >0
                                This.cp_foxchartfields.w_FDLEGEND = This.w_CLEGEND
                            Else
                                This.cp_foxchartfields.w_FDLEGEND = Space(25)
                            Endif
                            This.cp_foxchartfields.w_FDCOLOR = Rgb(Int(Rand() * 255), Int(Rand() * 255), Int(Rand() * 255))
                            This.cp_foxchartfields.w_FD_SHAPE = 1
                            This.cp_foxchartfields.w_FDVALONS = .T.
                            This.cp_foxchartfields.SaveRow()
                            This.w_i = This.w_i + 1
                        Enddo
                        If .F.
                            This.w_oChart.SourceAlias = This.w_Cursor
                            This.cp_foxchartfields.NotifyEvent("ADDFOXCHARTFIELDS")
                            This.w_oChart.DrawChart()
                        Endif
                    Endif
                Endif
            Case This.pOPER ="ADDFOXCHARTFIELDS"
                This.w_ChartsCount = This.cp_foxchartfields.NumRow()
                If This.w_ChartsCount > 0
                    This.oChart.ChartsCount = This.w_ChartsCount
                    This.cp_foxchartfields.MarkPos()
                    This.cp_foxchartfields.FirstRow()
                    Do While Not This.cp_foxchartfields.Eof_Trs()
                        This.cp_foxchartfields.SetRow()
                        If This.cp_foxchartfields.FullRow()
                            This.w_CURRENTROW = This.w_CURRENTROW + 1
                            This.oChart.Fields( This.w_CURRENTROW ).FieldValue = This.cp_foxchartfields.w_FD_VALUE
                            This.oChart.Fields( This.w_CURRENTROW ).Legend = This.cp_foxchartfields.w_FDLEGEND
                            This.oChart.Fields( This.w_CURRENTROW ).Color = This.cp_foxchartfields.w_FDCOLOR
                            This.oChart.Fields( This.w_CURRENTROW ).Shape = This.cp_foxchartfields.w_FD_SHAPE
                            This.oChart.Fields( This.w_CURRENTROW ).ShowValuesOnShape = This.cp_foxchartfields.w_FDVALONS
                        Endif
                        This.cp_foxchartfields.NextRow()
                    Enddo
                    This.cp_foxchartfields.RePos(.F.)
                    This.cp_foxcharteditor.NotifyEvent("Redraw")
                Endif
                This.bUpdateParentObject = .F.
            Case This.pOPER ="GETTFIELDSFROMCHART"
                Select (This.cp_foxchartfields.cTrsName)
                Zap
                This.w_ChartsCount = This.oChart.ChartsCount
                If This.w_ChartsCount > 0
                    This.w_CURRENTROW = 1
                    Do While This.w_CURRENTROW <= This.w_ChartsCount
                        This.cp_foxchartfields.InitRow()
                        This.cp_foxchartfields.w_FD_VALUE = This.oChart.Fields( This.w_CURRENTROW ).FieldValue
                        This.cp_foxchartfields.w_FDLEGEND = This.oChart.Fields( This.w_CURRENTROW ).Legend
                        This.cp_foxchartfields.w_FDCOLOR = This.oChart.Fields( This.w_CURRENTROW ).Color
                        This.cp_foxchartfields.w_FD_SHAPE = This.oChart.Fields( This.w_CURRENTROW ).Shape
                        This.cp_foxchartfields.w_FDVALONS = This.oChart.Fields( This.w_CURRENTROW ).ShowValuesOnShape
                        This.cp_foxchartfields.SaveDependsOn()
                        This.cp_foxchartfields.SetControlsValue()
                        This.cp_foxchartfields.SaveRow()
                        This.w_CURRENTROW = This.w_CURRENTROW + 1
                    Enddo
                    This.cp_foxchartfields.WorkfromTrs()
                    This.cp_foxchartfields.Refresh()
                    This.cp_foxchartproperties.w_SHPMAINCOLOR = This.oChart.Fields(1).Color
                    This.cp_foxchartproperties.w_REFSHPMAIN.Calculate("      1234567890     ", This.cp_foxchartproperties.w_SHPMAINCOLOR, This.cp_foxchartproperties.w_SHPMAINCOLOR)
                Endif
                This.bUpdateParentObject = .F.
            Case This.pOPER == "RESETCOMBOBOX"
                * --- Ho rieseguito anche la query, ora ho disponibile il cursore derivante dalla query
                *     posso ripopolare la combobox
                Local ObjCtrl
                ObjCtrl= This.cp_foxchartfields.GetbodyCtrl("w_FD_VALUE")
                ObjCtrl.Init()
                ObjCtrl=.Null.
                If .F.
                    ObjCtrl= This.cp_foxchartfields.GetbodyCtrl("w_FDLEGEND")
                    ObjCtrl.Init()
                    ObjCtrl=.Null.
                Endif
                Select (This.cp_foxchartfields.cTrsName)
                Zap
                This.cp_foxchartfields.InitRow()
                * --- --
                ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDLEGEND")
                ObjCtrl.Init()
                ObjCtrl=.Null.
                ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDDETACHSLICE")
                ObjCtrl.Init()
                ObjCtrl=.Null.
                ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDHIDESLICE")
                ObjCtrl.Init()
                ObjCtrl=.Null.
                ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDAXIS2")
                ObjCtrl.Init()
                ObjCtrl=.Null.
                ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDCOLOR")
                ObjCtrl.Init()
                ObjCtrl=.Null.
                This.cp_foxchartproperties.SetControlsValue()
                This.bUpdateParentObject = .F.
            Case This.pOPER == "SETCURSORDATA"
                * --- Ho rieseguito anche la query, ora ho disponibile il cursore derivante dalla query
                *     posso ripopolare la combobox
                Local ObjCtrl
                ObjCtrl= This.cp_foxchartfields.GetbodyCtrl("w_FD_VALUE")
                ObjCtrl.Popola(This.oChart.SourceAlias)
                ObjCtrl=.Null.
                * --- --
                If .F.
                    ObjCtrl= This.cp_foxchartfields.GetbodyCtrl("w_FDLEGEND")
                    ObjCtrl.Popola(This.oChart.SourceAlias)
                    ObjCtrl=.Null.
                Endif
                ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDLEGEND")
                ObjCtrl.Popola(This.oChart.SourceAlias)
                ObjCtrl=.Null.
                ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDDETACHSLICE")
                ObjCtrl.Popola(This.oChart.SourceAlias)
                ObjCtrl=.Null.
                ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDHIDESLICE")
                ObjCtrl.Popola(This.oChart.SourceAlias)
                ObjCtrl=.Null.
                ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDAXIS2")
                ObjCtrl.Popola(This.oChart.SourceAlias)
                ObjCtrl=.Null.
                ObjCtrl= This.cp_foxchartproperties.GetCtrl("w_FIELDCOLOR")
                ObjCtrl.Popola(This.oChart.SourceAlias)
                ObjCtrl=.Null.
                This.cp_foxchartproperties.SetControlsValue()
                This.bUpdateParentObject = .F.
            Case This.pOPER == "SETGRIDDATA"
                * --- Assegno alla griglia il cursore del grafico
                If Used(This.oChart.SourceAlias)
                    This.cp_foxchartproperties.w_GraphData.cCursor = This.oChart.SourceAlias
                    This.cp_foxchartproperties.w_GraphData.Browse()
                Endif
                This.bUpdateParentObject = .F.
            Case This.pOPER == "REDRAWFOXCHARTS"
                This.cp_foxcharteditor.NotifyEvent("Redraw")
                This.bUpdateParentObject = .F.
            Case This.pOPER == "DRAWFOXCHARTS"
                This.w_oChart.UpdateFoxCharts()
                This.bUpdateParentObject = .F.
            Case This.pOPER == "LOADMODELFROMFILE"
                If !Empty(This.cp_foxchartproperties.w_FILEMODEL)
                    This.w_oChart.LoadModelFromFile(This.cp_foxchartproperties.w_FILEMODEL)
                    This.cp_foxchartproperties.NotifyEvent("GetValueFromChart")
                    This.bUpdateParentObject = .F.
                Endif
            Case This.pOPER == "CONFIGLOADED"
                This.cp_foxcharteditor.w_CHANGEONFLY = .F.
                This.cp_foxchartproperties.NotifyEvent("GetValueFromChart")
                This.cp_foxchartproperties.SaveDependsOn()
                This.cp_foxchartproperties.SetControlsValue()
                If Not Empty(This.cp_foxchartproperties.w_QUERYMODEL) And Empty(This.cp_foxchartproperties.w_cSource)
                    This.w_oChart.cSource = This.cp_foxchartproperties.w_QUERYMODEL
                Endif
                This.cp_foxchartproperties.mEnableControls()
                This.cp_foxchartproperties.NotifyEvent("GETLEGENDS")
                This.cp_foxchartproperties.NotifyEvent("REDRAWFOXCHARTS")
                This.cp_foxchartproperties.NotifyEvent("SETCURSORDATA")
                This.cp_foxchartproperties.NotifyEvent("GETTFIELDSFROMCHART")
                This.bUpdateParentObject = .F.
            Case This.pOPER == "RUNBYPARAM"
                This.cp_foxcharteditor.w_CHANGEONFLY = .F.
                This.cp_foxchartproperties.NotifyEvent("GetValueFromChart")
                This.cp_foxchartproperties.SaveDependsOn()
                If Not Empty(This.cp_foxchartproperties.w_QUERYMODEL) And Empty(This.cp_foxchartproperties.w_cSource)
                    This.w_oChart.cSource = This.cp_foxchartproperties.w_QUERYMODEL
                Endif
                This.cp_foxchartproperties.SetControlsValue()
                This.cp_foxchartproperties.mEnableControls()
                This.cp_foxchartproperties.NotifyEvent("GETLEGENDS")
                This.cp_foxchartproperties.NotifyEvent("REDRAWFOXCHARTS")
                This.cp_foxchartproperties.NotifyEvent("SETCURSORDATA")
                This.bUpdateParentObject = .F.
        Endcase
    Endproc


    Proc Init(oParentObject,pOPER)
        This.pOPER=pOPER
        DoDefault(oParentObject)
        Return
    Function OpenTables()
        Dimension This.cWorkTables[max(1,0)]
        Return(This.OpenAllTables(0))

        * --- Area Manuale = Functions & Procedures
        * --- Fine Area Manuale
Enddefine

Procedure getEntityType(result)
    result="Routine"
Endproc
Procedure getEntityParmList(result)
    result="pOPER"
Endproc
