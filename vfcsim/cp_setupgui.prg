* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_setupgui                                                     *
*              Configurazione interfaccia                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-24                                                      *
* Last revis.: 2011-05-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
Return(Createobject("tcp_setupgui",oParentObject))

* --- Class definition
Define Class tcp_setupgui As StdForm
    Top    = 10
    Left   = 10

    * --- Standard Properties
    Width  = 555
    Height = 499
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2011-05-02"
    HelpContextID=212666138
    max_rt_seq=6

    * --- Constant Properties
    _IDX = 0
    CPUSERS_IDX = 0
    cpsetgui_IDX = 0
    cPrg = "cp_setupgui"
    cComment = "Configurazione interfaccia"
    oParentObject = .Null.
    Icon = "mask.ico"
    *closable = .f.

    * --- Local Variables
    w_CODUTE1 = Space(10)
    o_CODUTE1 = Space(10)
    w_CODUTE2 = Space(10)
    w_CODUTE = 0
    o_CODUTE = 0
    w_DESUTE = Space(20)
    w_DESUTE1 = Space(100)
    w_FLDELCAC = Space(1)

    * --- Children pointers
    cp_setgui = .Null.
    * --- Area Manuale = Declare Variables
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=1, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        *set procedure to cp_setgui additive
        With This
            .Pages(1).AddObject("oPag","tcp_setupguiPag1","cp_setupgui",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate("Pag.1")
            .Pages(1).oPag.oContained=Thisform
        Endwith
        This.Parent.oFirstControl = This.Page1.oPag.oCODUTE_1_3
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
        *release procedure cp_setgui
        * --- Area Manuale = Init Page Frame
        * --- cp_setupgui
        * --- Translate interface...
        This.Parent.cComment=cp_Translate(MSG_SETTING_GUI)
        * --- Fine Area Manuale
    Endproc
    Proc Init(oParentObject)
        If Vartype(m.oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        DoDefault()

        * --- Open tables
    Function OpenWorkTables()
        Dimension This.cWorkTables[2]
        This.cWorkTables[1]='CPUSERS'
        This.cWorkTables[2]='cpsetgui'
        Return(This.OpenAllTables(2))

    Procedure SetPostItConn()
        Return

    Function CreateChildren()
        This.cp_setgui = Createobject('stdDynamicChild',This,'cp_setgui',This.oPgFrm.Page1.oPag.oLinkPC_1_4)
        This.cp_setgui.createrealchild()
        Return

    Procedure DestroyChildren()
        If !Isnull(This.cp_setgui)
            This.cp_setgui.DestroyChildrenChain()
            This.cp_setgui=.Null.
        Endif
        This.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_4')
        Return

    Function IsAChildUpdated()
        Local i_bRes
        i_bRes = This.bUpdated
        i_bRes = i_bRes .Or. This.cp_setgui.IsAChildUpdated()
        Return(i_bRes)

    Procedure ChildrenNewDocument()
        This.cp_setgui.NewDocument()
        Return

    Procedure SetChildrenKeys()
        With This
            This.cp_setgui.SetKey(;
                .w_CODUTE,"usrcode";
                )
        Endwith
        Return

    Procedure ChildrenChangeRow()
        With This
            .cp_setgui.ChangeRow(This.cRowID+'      1',1;
                ,.w_CODUTE,"usrcode";
                )
        Endwith
        Return

    Function AddSonsFilter(i_cFlt,i_oTopObject)
        Local i_f,i_fnidx,i_cDatabaseType
        With This
            If !Isnull(.cp_setgui)
                i_f=.cp_setgui.BuildFilter()
                If !(i_f==.cp_setgui.cQueryFilter)
                    i_fnidx=.cp_setgui.cFile+'_IDX'
                    i_cDatabaseType = i_ServerConn[i_TableProp[.cp_setgui.&i_fnidx.,5],6]
                    i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.cp_setgui.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.cp_setgui.&i_fnidx.,2])+' where '+i_f+')'
                Else
                    i_f=''
                Endif
                i_f=.cp_setgui.AddSonsFilter(i_f,i_oTopObject)
                If !Empty(i_f)
                    i_cFlt=i_cFlt+Iif(Empty(i_cFlt),'',' AND ')+i_f
                Endif
            Endif
        Endwith
        Return(i_cFlt)


        * --- Read record and initialize Form variables
    Procedure LoadRec()
    Endproc

    * --- ecpQuery
    Procedure ecpQuery()
        This.BlankRec()
        This.cFunction="Edit"
        This.SetStatus()
        If This.cFunction="Edit"
            This.mEnableControls()
        Endif
    Endproc
    * --- Esc
    Proc ecpQuit()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            This.cFunction = "Edit"
            This.oFirstControl.SetFocus()
            Return
        Endif
        * ---
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Endproc
    Proc QueryUnload()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            Nodefault
            Return
        Endif
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Proc ecpSave()
        If This.TerminateEdit() And This.CheckForm()
            This.Save_cp_setgui(.F.)
            This.LockScreen=.T.
            This.mReplace(.T.)
            This.LockScreen=.F.
            This.NotifyEvent("Done")
            This.Release()
        Endif
    Endproc
    Func OkToQuit()
        If  This.cp_setgui.IsAChildUpdated()
            Return cp_YesNo(MSG_DISCARD_CHANGES_QP)
        Endif
        Return .T.
    Endfunc
    Proc Save_cp_setgui(i_ask)
        If This.cp_setgui.IsAChildUpdated() And (!i_ask Or cp_YesNo(MSG_SAVE_CHANGES_QP))
            cp_BeginTrs()
            This.cp_setgui.mReplace(.T.)
            cp_EndTrs()
        Endif
    Endproc

    * --- Blank Form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        With This
            .w_CODUTE1=Space(10)
            .w_CODUTE2=Space(10)
            .w_CODUTE=0
            .w_DESUTE=Space(20)
            .w_DESUTE1=Space(100)
            .w_FLDELCAC=Space(1)
            .w_CODUTE1 = i_CODUTE
            .DoRTCalc(1,1,.F.)
            If Not(Empty(.w_CODUTE1))
                .link_1_1('Full')
            Endif
            .w_CODUTE2 = i_CODUTE
            .DoRTCalc(2,2,.F.)
            If Not(Empty(.w_CODUTE2))
                .link_1_2('Full')
            Endif
            .w_CODUTE = Iif( Empty( .w_CODUTE2)  ,-1 ,  .w_CODUTE2 )
            .cp_setgui.NewDocument()
            .cp_setgui.ChangeRow('1',1,.w_CODUTE,"usrcode")
            If Not(.cp_setgui.bLoaded)
                .cp_setgui.SetKey(.w_CODUTE,"usrcode")
            Endif
            .DoRTCalc(4,4,.F.)
            .w_DESUTE1 = Iif( .w_CODUTE=-1 , MSG_INSTALLATION_SETTINGS   , .w_DESUTE )
            .w_FLDELCAC = 'N'
            .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_VALID_FOR + MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_VALID_FOR_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_APPLY_TO + MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_DELETE_TEMPORARY_FILES)
        Endwith
        This.SaveDependsOn()
        This.SetControlsValue()
        This.mHideControls()
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = Setting operation
    *     Allowed Parameters: Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        * --- Deactivating List page when <> da Query
        This.cp_setgui.SetStatus(i_cOp)
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *  this.cp_setgui.SetChildrenStatus(i_cOp)
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt
        i_cFlt =""
        Return (i_cFlt)
    Endfunc

    Proc QueryKeySet(i_cWhere,i_cOrderBy)
    Endproc

    Proc SelectCursor
    Proc GetXKey

        * --- Update Database
    Function mReplace(i_bEditing)
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        This.NotifyEvent('Update start')
        With This
        Endwith
        This.NotifyEvent('Update end')
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(.T.)

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        If i_bUpd
            With This
                .link_1_1('Full')
                .link_1_2('Full')
                If .o_CODUTE1<>.w_CODUTE1
                    .w_CODUTE = Iif( Empty( .w_CODUTE2)  ,-1 ,  .w_CODUTE2 )
                Endif
                .DoRTCalc(4,4,.T.)
                If .o_CODUTE<>.w_CODUTE
                    .w_DESUTE1 = Iif( .w_CODUTE=-1 , MSG_INSTALLATION_SETTINGS   , .w_DESUTE )
                Endif
                .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_VALID_FOR + MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_VALID_FOR_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_APPLY_TO + MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_DELETE_TEMPORARY_FILES)
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
                If .w_CODUTE<>.o_CODUTE
                    .Save_cp_setgui(.T.)
                    .cp_setgui.NewDocument()
                    .cp_setgui.ChangeRow('1',1,.w_CODUTE,"usrcode")
                    If Not(.cp_setgui.bLoaded)
                        .cp_setgui.SetKey(.w_CODUTE,"usrcode")
                    Endif
                Endif
            Endwith
            This.DoRTCalc(6,6,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
            .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_VALID_FOR + MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_14.Calculate(MSG_VALID_FOR_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_15.Calculate(MSG_APPLY_TO + MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_16.Calculate(MSG_DELETE_TEMPORARY_FILES)
        Endwith
        Return


        * --- Enable controls under condition
    Procedure mEnableControls()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        DoDefault()
        Return

        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
        Endwith
        * --- Area Manuale = Notify Event End
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

        * --- Link procedure for entity name=CODUTE1
    Func link_1_1(i_cCtrl,oSource)
        Local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
        i_nConn = i_TableProp[this.CPUSERS_IDX,3]
        i_lTable = "CPUSERS"
        If This.cFunction='Load'
            i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .T., This.CPUSERS_IDX)
        Else
            i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
        Endif
        i_nArea = Select()
        i_bEmpty = .F.
        If Used("_Link_")
            Select _Link_
            Use
        Endif
        Do Case
            Case Empty(This.w_CODUTE1) And i_cCtrl<>"Drop"
                i_bEmpty = .T.
                i_reccount=0
            Case i_cCtrl='Full' Or i_cCtrl='Load' Or i_cCtrl='Extend'
                If .Not. Empty(This.w_CODUTE1)
                    If i_nConn<>0
                        i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                            +" from "+i_cTable+" where CODE="+cp_ToStrODBC(This.w_CODUTE1);
                            ,"_Link_")
                        i_reccount = Iif(i_ret=-1,0,Reccount())
                    Else
                        i_cWhere = cp_PKFox(i_cTable;
                            ,'CODE',This.w_CODUTE1)
                        Select Code,Name;
                            from (i_cTable) Where &i_cWhere. Into Cursor _Link_
                        i_reccount = _Tally
                    Endif
                Else
                    i_reccount = 0
                Endif
        Endcase
        If i_reccount>0 And Used("_Link_")
            This.w_CODUTE1 = Nvl(_Link_.Code,Space(10))
            This.w_DESUTE = Nvl(_Link_.Name,Space(20))
        Else
            If i_cCtrl<>'Load'
                This.w_CODUTE1 = Space(10)
            Endif
            This.w_DESUTE = Space(20)
        Endif
        i_bRes=i_reccount=1
        If i_bRes And i_cCtrl<>'Full' And i_cCtrl<>'Load' And Used("_Link_")
            i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.Code,1)
            cp_ShowWarn(i_cKey,This.CPUSERS_IDX)
        Endif
        If i_cCtrl='Drop'
            This.TrsFromWork()
            This.NotifyEvent('w_CODUTE1 Changed')
        Endif
        Select (i_nArea)
        Return(i_bRes Or i_bEmpty)
    Endfunc

    * --- Link procedure for entity name=CODUTE2
    Func link_1_2(i_cCtrl,oSource)
        Local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
        i_nConn = i_TableProp[this.cpsetgui_IDX,3]
        i_lTable = "cpsetgui"
        If This.cFunction='Load'
            i_cTable = cp_SetAzi(i_TableProp[this.cpsetgui_IDX,2], .T., This.cpsetgui_IDX)
        Else
            i_cTable = cp_SetAzi(i_TableProp[this.cpsetgui_IDX,2])
        Endif
        i_nArea = Select()
        i_bEmpty = .F.
        If Used("_Link_")
            Select _Link_
            Use
        Endif
        Do Case
            Case Empty(This.w_CODUTE2) And i_cCtrl<>"Drop"
                i_bEmpty = .T.
                i_reccount=0
            Case i_cCtrl='Full' Or i_cCtrl='Load' Or i_cCtrl='Extend'
                If .Not. Empty(This.w_CODUTE2)
                    If i_nConn<>0
                        i_ret=cp_SQL(i_nConn,"select usrcode";
                            +" from "+i_cTable+" where usrcode="+cp_ToStrODBC(This.w_CODUTE2);
                            ,"_Link_")
                        i_reccount = Iif(i_ret=-1,0,Reccount())
                    Else
                        i_cWhere = cp_PKFox(i_cTable;
                            ,'usrcode',This.w_CODUTE2)
                        Select usrcode;
                            from (i_cTable) Where &i_cWhere. Into Cursor _Link_
                        i_reccount = _Tally
                    Endif
                Else
                    i_reccount = 0
                Endif
        Endcase
        If i_reccount>0 And Used("_Link_")
            This.w_CODUTE2 = Nvl(_Link_.usrcode,Space(10))
        Else
            If i_cCtrl<>'Load'
                This.w_CODUTE2 = Space(10)
            Endif
        Endif
        i_bRes=i_reccount=1
        If i_bRes And i_cCtrl<>'Full' And i_cCtrl<>'Load' And Used("_Link_")
            i_cKey = cp_SetAzi(i_TableProp[this.cpsetgui_IDX,2])+'\'+cp_ToStr(_Link_.usrcode,1)
            cp_ShowWarn(i_cKey,This.cpsetgui_IDX)
        Endif
        If i_cCtrl='Drop'
            This.TrsFromWork()
            This.NotifyEvent('w_CODUTE2 Changed')
        Endif
        Select (i_nArea)
        Return(i_bRes Or i_bEmpty)
    Endfunc

    Function SetControlsValue()
        If Not(This.oPgFrm.Page1.oPag.oCODUTE_1_3.RadioValue()==This.w_CODUTE)
            This.oPgFrm.Page1.oPag.oCODUTE_1_3.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oDESUTE1_1_7.Value==This.w_DESUTE1)
            This.oPgFrm.Page1.oPag.oDESUTE1_1_7.Value=This.w_DESUTE1
        Endif
        If Not(This.oPgFrm.Page1.oPag.oFLDELCAC_1_12.RadioValue()==This.w_FLDELCAC)
            This.oPgFrm.Page1.oPag.oFLDELCAC_1_12.SetRadio()
        Endif
        cp_SetControlsValueExtFlds(This,'')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            *i_bRes = i_bRes .and. .cp_setgui.CheckForm()
            If i_bRes
                i_bRes=  .cp_setgui.CheckForm()
                If Not(i_bRes)
                    This.oPgFrm.ActivePage=1
                Endif
            Endif
            * --- Area Manuale = Check Form
            * --- cp_setupgui
            *--- Cancella cartella bmp
            If i_bRes And .w_FLDELCAC='S' And Vartype(i_themesmanager)='O'
                L_FH = Fcreate(Addbs(Alltrim(i_themesmanager.Imgpath))+"bDelGraphics.fla" )
                Fclose(L_FH)
            Endif
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
            Else
                If Not(i_bnoChk)
                    cp_ErrorMsg(i_cErrorMsg)
                Endif
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

    * --- SaveDependsOn
    Proc SaveDependsOn()
        This.o_CODUTE1 = This.w_CODUTE1
        This.o_CODUTE = This.w_CODUTE
        * --- cp_setgui : Depends On
        This.cp_setgui.SaveDependsOn()
        Return

Enddefine

* --- Define pages as container
Define Class tcp_setupguiPag1 As StdContainer
    Width  = 551
    Height = 499
    stdWidth  = 551
    stdheight = 499
    resizeXpos=424
    resizeYpos=438
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.


    Add Object oCODUTE_1_3 As StdCombo With uid="OHDKSFOZIM",rtseq=3,rtrep=.F.,Left=123,Top=10,Width=101,Height=17;
        , ToolTipText = "Impostazioni per utente o generali (applicate all'avvio)";
        , HelpContextID = 98958270;
        , cFormVar="w_CODUTE",RowSource=""+"Utente,"+"Installazione", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oCODUTE_1_3.RadioValue()
        Return(Iif(This.Value =1,i_CODUTE,;
            iif(This.Value =2,-1,;
            0)))
    Endfunc
    Func oCODUTE_1_3.GetRadio()
        This.Parent.oContained.w_CODUTE = This.RadioValue()
        Return .T.
    Endfunc

    Func oCODUTE_1_3.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_CODUTE==i_CODUTE,1,;
            iif(This.Parent.oContained.w_CODUTE==-1,2,;
            0))
    Endfunc


    Add Object oLinkPC_1_4 As stdDynamicChildContainer With uid="FBYAQRAVBA",Left=7, Top=75, Width=535, Height=393, bOnScreen=.T.;


    Add Object oDESUTE1_1_7 As StdField With uid="ZPEAJLCWAJ",rtseq=5,rtrep=.F.,;
        cFormVar = "w_DESUTE1", cQueryName = "DESUTE1",Enabled=.F.,;
        bObbl = .F. , nPag = 1, Value=Space(100), bMultilanguage =  .F.,;
        HelpContextID = 71773374,;
        bGlobalFont=.T.,;
        Height=21, Width=392, Left=123, Top=42, InputMask=Replicate('X',100)


    Add Object oBtn_1_9 As StdButton With uid="KTQEUHINHK",Left=434, Top=472, Width=50,Height=20,;
        caption="Salva", nPag=1;
        , HelpContextID = 253097851;
        , Caption=MSG_S_SAVE;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_9.Click()
        =cp_StandardFunction(This,"Save")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_1_10 As StdButton With uid="VNDHAOKDHY",Left=489, Top=472, Width=50,Height=20,;
        caption="Esci", nPag=1;
        , HelpContextID = 120059674;
        , Caption=MSG_S_EXIT;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_10.Click()
        =cp_StandardFunction(This,"Quit")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_1_11 As StdButton With uid="YWHDTQKVAO",Left=7, Top=472, Width=50,Height=20,;
        caption="Default", nPag=1;
        , ToolTipText = ""+MSG_PRESS_TO_SET_DEFAULT_VALUE+"";
        , HelpContextID = 112758193;
        , Caption=MSG_DEFAULT;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_11.Click()
        With This.Parent.oContained
            .cp_setgui.NotifyEvent('SetDefault')
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object oFLDELCAC_1_12 As StdCheck With uid="NXJIZVNZSP",rtseq=6,rtrep=.F.,Left=129, Top=472, Caption="Elimina temporaneo immagini",;
        ToolTipText = "Se attivo, svuota la cartella delle immagini temporanee presenti sul pc corrente alla chiusura della procedura",;
        HelpContextID = 96300182,;
        cFormVar="w_FLDELCAC", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oFLDELCAC_1_12.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func oFLDELCAC_1_12.GetRadio()
        This.Parent.oContained.w_FLDELCAC = This.RadioValue()
        Return .T.
    Endfunc

    Func oFLDELCAC_1_12.SetRadio()
        This.Parent.oContained.w_FLDELCAC=Trim(This.Parent.oContained.w_FLDELCAC)
        This.Value = ;
            iif(This.Parent.oContained.w_FLDELCAC=='S',1,;
            0)
    Endfunc


    Add Object oObj_1_13 As cp_setCtrlObjprop With uid="WTFHXDANKA",Left=5, Top=529, Width=284,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Validi per:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 220331709;
        , bGlobalFont=.T.



    Add Object oObj_1_14 As cp_setobjprop With uid="SKPUZOAYUU",Left=293, Top=529, Width=257,Height=26,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CODUTE",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 220331709;
        , bGlobalFont=.T.



    Add Object oObj_1_15 As cp_setCtrlObjprop With uid="YWNLGUFSWM",Left=5, Top=555, Width=284,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Applicate su:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 220331709;
        , bGlobalFont=.T.



    Add Object oObj_1_16 As cp_setCtrlObjprop With uid="CHUEWDVMCO",Left=5, Top=580, Width=284,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="w_FLDELCAC",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 220331709;
        , bGlobalFont=.T.


    Add Object oStr_1_5 As StdString With uid="DACPYCDNIM",Visible=.T., Left=20, Top=10,;
        Alignment=1, Width=99, Height=18,;
        Caption="Validi per:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_8 As StdString With uid="QHXEZTNSJZ",Visible=.T., Left=20, Top=42,;
        Alignment=1, Width=99, Height=18,;
        Caption="Applicate su:"  ;
        , bGlobalFont=.T.
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_setupgui','','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Dialog Window"
Endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
