* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_chprndlg                                                     *
*              Stampe                                                          *
*                                                                              *
*      Author: GiorGio Montali                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-06                                                      *
* Last revis.: 2011-06-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
Return(Createobject("tcp_chprndlg",oParentObject))

* --- Class definition
Define Class tcp_chprndlg As StdForm
    Top    = 17
    Left   = 113

    * --- Standard Properties
    Width  = 498
    Height = 198
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2011-06-08"
    HelpContextID=158270599
    max_rt_seq=6

    * --- Constant Properties
    _IDX = 0
    cPrg = "cp_chprndlg"
    cComment = "Stampe"
    oParentObject = .Null.
    Icon = "mask.ico"
    *closable = .f.

    * --- Local Variables
    w_DEVICE = Space(200)
    w_Opt_File = Space(20)
    o_Opt_File = Space(20)
    w_Txt_File = Space(200)
    o_Txt_File = Space(200)
    w_lingua = Space(20)
    o_lingua = Space(20)
    w_reportLanguage = Space(3)
    w_cOrRepName = Space(100)
    * --- Area Manuale = Declare Variables
    * --- cp_chprndlg
    Icon = i_cBmpPath+i_cStdIcon
    DoCreate = .T.
    oParentObject = .Null.
    WindowType = 1
    ShowTips = .T.
    prTyExp = "TXT"
    prCopie = 1
    excel=.Null.
    Word=.Null.
    bModifiedFrx=.F.
    cNomeReport = ""
    cOrRepName  = ""
    cSessionCode = ""
    Func HasCPEvents(i_cOp)
        Return(i_cOp+'|'$'ecpQuit|ecpSecurity|')
    Endfunc


    Proc ecpSecurity()
        If cp_IsAdministrator()
            If cp_IsStdFile(This.cNomeReport,'FRX')
                Select __tmp__
                Local d
                d=Set('DATABASE')
                If Not(This.bModifiedFrx)
                    Local N
                    N=Getenv('TEMP')
                    * --- crea una tabella temporanea in un database temporaneo per la gestione dei nomi lunghi
                    Crea Database (N+'\_data_')
                    Copy To (N+'\__tmp__') Database (N+'\_data_')
                    Use
                    * --- usa la tabella appena creata (in \windows\temp) con alias __tmp__
                    Select 0
                    Use (N+'\__tmp__') Alias __tmp__
                    This.bModifiedFrx=.T.
                Endif
                *cp_ModifyReport(this.cNomeReport)
                cp_ModifyReport(cp_GetStdfile(This.cNomeReport,'FRX'))
                If Not(Empty(d))
                    * --- potrebbe non esserci un database aperto
                    Set Database To (d)
                Endif
            Else
                Messagebox(cp_Translate('File ')+This.cNomeReport+cp_Translate(' non trovato'),cp_Translate('Errore'))
            Endif
        Endif
    Endproc

    Proc Activate()
        i_curform=This
    Endproc

    Procedure Release()
        *--- Cancello i report tradotti
        Local cNomeFile,cDirRep, l_NumRep
        *--- Cancello FRX/XLT
        cDirRep=Alltrim(Addbs(cp_AdvancedDir(Addbs(Cp_getAppData())+Addbs(i_cAppDir),'REPORT','')))
        l_NumRep = Adir(aRepList,Addbs(cDirRep)+"*"+This.cSessionCode+".*")
        For i=1 To l_NumRep
            cNomeFile = cDirRep+aRepList[i,1]
            If cp_FileExist(cNomeFile)
                Erase(cNomeFile)
            Endif
        Next
        DoDefault()
    Endproc

    Proc Destroy()
        If Thisform.bModifiedFrx
            * --- distrugge la tabella e il database temporanei
            Local N
            N=Getenv('TEMP')
            Local d
            d=Set('DATABASE')
            Set Database To (N+'\_data_')
            Select __tmp__
            Use
            Close Database
            Delete Database (N+'\_data_')
            Delete File (N+'\__tmp__.dbf')
        Endif
        i_curform=.Null.
    Endproc
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=1, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tcp_chprndlgPag1","cp_chprndlg",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate("Pag.1")
            .Pages(1).oPag.oContained=Thisform
        Endwith
        This.Parent.oFirstControl = This.Page1.oPag.oOpt_File_1_15
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
        * --- Area Manuale = Init Page Frame
        * --- cp_chprndlg
        * --- Translate interface...
        This.Parent.cComment=cp_Translate(MSG_PRINT_SYSTEM)
        This.Parent.cSessionCode=Sys(2015)
        * --- Fine Area Manuale
    Endproc
    Proc Init(oParentObject)
        If Vartype(m.oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        DoDefault()

        * --- Open tables
    Function OpenWorkTables()
        Return(This.OpenAllTables(0))

    Procedure SetPostItConn()
        Return


        * --- Read record and initialize Form variables
    Procedure LoadRec()
    Endproc

    * --- ecpQuery
    Procedure ecpQuery()
        This.BlankRec()
        This.cFunction="Edit"
        This.SetStatus()
        If This.cFunction="Edit"
            This.mEnableControls()
        Endif
    Endproc
    * --- Esc
    Proc ecpQuit()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            This.cFunction = "Edit"
            This.oFirstControl.SetFocus()
            Return
        Endif
        * ---
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Endproc
    Proc QueryUnload()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            Nodefault
            Return
        Endif
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Proc ecpSave()
        If This.TerminateEdit() And This.CheckForm()
            This.LockScreen=.T.
            This.mReplace(.T.)
            This.LockScreen=.F.
            This.NotifyEvent("Done")
            This.Release()
        Endif
    Endproc
    Func OkToQuit()
        Return .T.
    Endfunc

    * --- Blank Form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        With This
            .w_DEVICE=Space(200)
            .w_Opt_File=Space(20)
            .w_Txt_File=Space(200)
            .w_lingua=Space(20)
            .w_reportLanguage=Space(3)
            .w_cOrRepName=Space(100)
            .DoRTCalc(1,1,.F.)
            .w_Opt_File = 'TXT'
            .w_Txt_File = 'DEFA'+Right('0000'+Alltrim(Str(i_codute,4,0)),4)+'.TXT'
            .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_PRINTER)
            .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_PRINT_ON_FILE)
            .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_FILE_NAME)
        Endwith
        This.DoRTCalc(4,6,.F.)
        This.SaveDependsOn()
        This.SetControlsValue()
        This.oPgFrm.Page1.oPag.oBtn_1_2.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_3.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_4.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_5.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_6.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_7.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_8.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_9.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_10.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_14.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_18.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
        This.mHideControls()
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- cp_chprndlg
        Local i_nCurs, i_found, i_oa,i_workstation , bottone
        i_oa = Select()
        i_nCurs = Sys(2015)
        This.cNomeReport = nReport
        This.w_cOrRepName = nReport
        This.w_lingua=i_cLanguage && forziamo la lingua dell'utente orig: legge la lingua dal report cp_ReportLanguage(NVL(This.cNomeReport,''))
        This.w_reportLanguage=cp_ReportLanguage(Nvl(This.cNomeReport,'')) && this.w_lingua
        i_workstation=Getenv("WORKSTATION")
        If i_ServerConn[1,2]<>0
            cp_sql(i_ServerConn[1,2],"select * from cpusrrep where repasso="+cp_ToStr(This.cNomeReport+'.FRX')+" and (aziasso="+cp_ToStr(i_codazi)+" or aziasso='          ') and (wstasso="+cp_ToStr(i_workstation)+" or wstasso='          ') and (usrasso="+cp_ToStr(i_codute)+" or usrasso=0) order by wstasso desc,aziasso desc, usrasso desc",i_nCurs)
        Else
            Select * From cpusrrep Where repasso=This.cNomeReport+'.FRX' And (Trim(i_codazi)==Trim(aziasso) Or Empty(aziasso)) And (Trim(wstasso)==Trim(i_workstation) Or Empty(wstasso)) And (usrasso=i_codute Or usrasso=0) Order By wstasso Desc,aziasso Desc, usrasso Desc Into Curs (i_nCurs)
        Endif
        If Used(i_nCurs)
            If Not(Eof())
                i_found = .F.
                Go Top
                This.w_DEVICE = Trim(DEVASSO)
                This.prCopie = Iif(COPASSO=0,1,COPASSO)
            Endif
            Use
        Else
            Wait Window cp_Translate(MSG_ERROR_OPENING_PRINTING_TABLE)
        Endif
        Select (i_oa)
        bottone=This.getctrl(cp_Translate('\<Excel'))
        **bottone.caption=''
        If !cp_IsStdFile(This.cNomeReport,'XLT')
            bottone.Picture = i_cBmpPath+"Excel.bmp"
        Endif

        bottone=This.getctrl(cp_Translate(MSG_GRAPH))
        **bottone.caption=''
        If !cp_IsStdFile(This.cNomeReport,'VGR') Or !cp_IsStdFile(This.cNomeReport,'VFC')
            bottone.Picture = i_cBmpPath+"foxcharts.ico"
        Endif
        This.mcalc(.T.)

        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = Setting operation
    *     Allowed Parameters: Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        * --- Deactivating List page when <> da Query
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt
        i_cFlt =""
        Return (i_cFlt)
    Endfunc

    Proc QueryKeySet(i_cWhere,i_cOrderBy)
    Endproc

    Proc SelectCursor
    Proc GetXKey

        * --- Update Database
    Function mReplace(i_bEditing)
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        This.NotifyEvent('Update start')
        With This
        Endwith
        This.NotifyEvent('Update end')
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(.T.)

        * --- Calculations
    Function mcalc(i_bUpd)
        This.bCalculating=.T.
        If i_bUpd
            With This
                If .o_Opt_File<>.w_Opt_File.Or. .o_Txt_File<>.w_Txt_File
                    .Calculate_SUBRBJSLMU()
                Endif
                .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_PRINTER)
                .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_PRINT_ON_FILE)
                .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_FILE_NAME)
                If .o_lingua<>.w_lingua
                    .Calculate_FQRIPJDTHG()
                Endif
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
            Endwith
            This.DoRTCalc(1,6,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
            .oPgFrm.Page1.oPag.oObj_1_21.Calculate(MSG_PRINTER)
            .oPgFrm.Page1.oPag.oObj_1_22.Calculate(MSG_PRINT_ON_FILE)
            .oPgFrm.Page1.oPag.oObj_1_23.Calculate(MSG_FILE_NAME)
        Endwith
        Return

    Proc Calculate_SUBRBJSLMU()
        With This
            * --- Aggiorna tipo file
            .w_Txt_File = Iif(.w_Opt_File='SDF' Or .w_Opt_File='Delimited', Addbs(Justpath(.w_Txt_File)) + Juststem(.w_Txt_File)+".TXT", Addbs(Justpath(.w_Txt_File)) + Juststem(.w_Txt_File)+"."+.w_Opt_File)
        Endwith
    Endproc
    Proc Calculate_FQRIPJDTHG()
        With This
            * --- Traduzione Report Runtime al cambio della lingua
            cp_tradreport(This;
                ,This.cNomeReport;
                ,.w_lingua;
                ,This.cSessionCode;
                ,.w_reportLanguage;
                ,This;
                ,.w_cOrRepName;
                )
        Endwith
    Endproc

    * --- Enable controls under condition
    Procedure mEnableControls()
        This.oPgFrm.Page1.oPag.oBtn_1_2.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_3.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_4.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_5.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_7.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        DoDefault()
        Return

        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
        Endwith
        * --- Area Manuale = Notify Event End
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

    Function SetControlsValue()
        If Not(This.oPgFrm.Page1.oPag.oDEVICE_1_13.Value==This.w_DEVICE)
            This.oPgFrm.Page1.oPag.oDEVICE_1_13.Value=This.w_DEVICE
        Endif
        If Not(This.oPgFrm.Page1.oPag.oOpt_File_1_15.RadioValue()==This.w_Opt_File)
            This.oPgFrm.Page1.oPag.oOpt_File_1_15.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTxt_File_1_17.Value==This.w_Txt_File)
            This.oPgFrm.Page1.oPag.oTxt_File_1_17.Value=This.w_Txt_File
        Endif
        If Not(This.oPgFrm.Page1.oPag.olingua_1_25.RadioValue()==This.w_lingua)
            This.oPgFrm.Page1.oPag.olingua_1_25.SetRadio()
        Endif
        cp_SetControlsValueExtFlds(This,'')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
            Else
                If Not(i_bnoChk)
                    cp_ErrorMsg(i_cErrorMsg)
                Endif
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

    * --- SaveDependsOn
    Proc SaveDependsOn()
        This.o_Opt_File = This.w_Opt_File
        This.o_Txt_File = This.w_Txt_File
        This.o_lingua = This.w_lingua
        Return

Enddefine

* --- Define pages as container
Define Class tcp_chprndlgPag1 As StdContainer
    Width  = 494
    Height = 198
    stdWidth  = 494
    stdheight = 198
    resizeXpos=205
    resizeYpos=188
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.


    Add Object oBtn_1_2 As StdButton With uid="VFOOFOVCCT",Left=18, Top=143, Width=48,Height=47,;
        Picture="PRVIDEO.bmp", Caption="", nPag=1;
        , ToolTipText = ""+MSG_PRINT_PREVIEW+"";
        , HelpContextID = 154039477;
        , Caption=MSG_PREVIEW;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_2.Click()
        With This.Parent.oContained
            cp_RunRep (This.Parent.oContained,cp_GetStdfile(.cNomeReport,"FRX"), "V", .w_Txt_File, .prCopie)
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mcalc(.T.)
        Endif
    Endproc

    Func oBtn_1_2.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (cp_IsStdFile(This.Parent.Parent.Parent.Parent.cNomeReport,'FRX'))
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_3 As StdButton With uid="SYUJSWCUEJ",Left=82, Top=143, Width=48,Height=47,;
        Picture="PRSTAMPA.bmp", Caption="", nPag=1;
        , ToolTipText = ""+MSG_SEND_TO_PRINTER+"";
        , HelpContextID = 74548296;
        , Caption=MSG_S_PRINT;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_3.Click()
        With This.Parent.oContained
            cp_PRNBOT (This.Parent.oContained,"StpClick")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mcalc(.T.)
        Endif
    Endproc

    Func oBtn_1_3.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (cp_IsStdFile(This.Parent.oContained.cNomeReport,'FRX'))
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_4 As StdButton With uid="YCIJBGHPJT",Left=155, Top=143, Width=48,Height=47,;
        Picture="PROPTION.bmp", Caption="", nPag=1;
        , ToolTipText = ""+MSG_SEND_TO_PRINTER_WITH_OPTIONS+"";
        , HelpContextID = 238449951;
        , Caption=MSG_OPTIONS_PRINTER;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_4.Click()
        With This.Parent.oContained
            cp_PRNBOT (This.Parent.oContained,"StpOptClick")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mcalc(.T.)
        Endif
    Endproc

    Func oBtn_1_4.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (cp_IsStdFile(This.Parent.oContained.cNomeReport,'FRX'))
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_5 As StdButton With uid="GBDTDRAQVH",Left=219, Top=143, Width=48,Height=47,;
        Picture="PRWORD.bmp", Caption="", nPag=1;
        , HelpContextID = 45872263;
        , Caption='\<Word';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_5.Click()
        With This.Parent.oContained
            cp_PRNBOT (This.Parent.oContained,"WordClick")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mcalc(.T.)
        Endif
    Endproc

    Func oBtn_1_5.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (cp_IsStdFile(This.Parent.oContained.cNomeReport,'DOC'))
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_6 As StdButton With uid="DZERHNUCVM",Left=287, Top=143, Width=48,Height=47,;
        Picture="PREXCEL.bmp", Caption="", nPag=1;
        , HelpContextID = 70907123;
        , Caption='\<Excel';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_6.Click()
        With This.Parent.oContained
            cp_PRNBOT (This.Parent.oContained,"ExcelClick")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mcalc(.T.)
        Endif
    Endproc


    Add Object oBtn_1_7 As StdButton With uid="LPRBZXUKYV",Left=359, Top=142, Width=48,Height=47,;
        Picture="prgraph.bmp", Caption="", nPag=1;
        , ToolTipText = ""+MSG_MSGRAPH_GRAPH+"";
        , HelpContextID = 184118200;
        , Caption=MSG_GRAPH ;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_7.Click()
        With This.Parent.oContained
            cp_PRNBOT (This.Parent.oContained,"GraphClick")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mcalc(.T.)
        Endif
    Endproc

    Func oBtn_1_7.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (cp_IsStdFile(This.Parent.oContained.cNomeReport,'VGR') Or cp_IsStdFile(This.Parent.oContained.cNomeReport,'VFC'))
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_8 As StdButton With uid="BDHEEBNWXU",Left=430, Top=140, Width=48,Height=47,;
        Picture="PRESCI.bmp", Caption="", nPag=1;
        , ToolTipText = ""+MSG_EXIT+"";
        , HelpContextID = 65664135;
        , Caption=MSG_S_EXIT;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_8.Click()
        =cp_StandardFunction(This,"Quit")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mcalc(.T.)
        Endif
    Endproc


    Add Object oBtn_1_9 As StdButton With uid="GHFIMVPFCJ",Left=434, Top=71, Width=48,Height=47,;
        Picture="PRFILE.bmp", Caption="", nPag=1;
        , ToolTipText = ""+MSG_PRINT_OR_EXPORT_ON_FILE+"";
        , HelpContextID = 258908845;
        , Caption=MSG_S_SAVE;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_9.Click()
        With This.Parent.oContained
            cp_PRNBOT (This.Parent.oContained,"StpFileClick")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mcalc(.T.)
        Endif
    Endproc


    Add Object oBtn_1_10 As StdButton With uid="TPMPZXYMVN",Left=434, Top=9, Width=48,Height=47,;
        Picture="PRASSOC.bmp", Caption="", nPag=1;
        , ToolTipText = ""+MSG_CONNECT_REPORT_TO_PRINTER+"";
        , HelpContextID = 113871591;
        , Caption=MSG_REPORT_BS_PRINTER;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_10.Click()
        With This.Parent.oContained
            cp_PRNBOT (This.Parent.oContained,"AssocClick")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mcalc(.T.)
        Endif
    Endproc

    Add Object oDEVICE_1_13 As StdField With uid="YJTBDAQENK",rtseq=1,rtrep=.F.,;
        cFormVar = "w_DEVICE", cQueryName = "DEVICE",Enabled=.F.,;
        bObbl = .F. , nPag = 1, Value=Space(200), bMultilanguage =  .F.,;
        ToolTipText = "Stampante selezionata",;
        HelpContextID = 67696922,;
        bGlobalFont=.T.,;
        Height=21, Width=294, Left=107, Top=24, InputMask=Replicate('X',200)


    Add Object oBtn_1_14 As StdButton With uid="BYWCYEOHKL",Left=404, Top=24, Width=24,Height=23,;
        caption="...", nPag=1;
        , ToolTipText = ""+MSG_CHANGE_PRINTER+"";
        , HelpContextID = 92081017;
        , bGlobalFont=.T.

    Proc oBtn_1_14.Click()
        With This.Parent.oContained
            cp_PRNBOT (This.Parent.oContained,"SStpClick")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mcalc(.T.)
        Endif
    Endproc


    Add Object oOpt_File_1_15 As StdCombo With uid="QDOWFWNDEA",rtseq=2,rtrep=.F.,Left=11,Top=91,Width=89,Height=20;
        , ToolTipText = "tipo file";
        , HelpContextID = 188041379;
        , cFormVar="w_Opt_File",RowSource=""+"TXT,"+"DBF,"+"SDF,"+"Delimited", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oOpt_File_1_15.RadioValue()
        Return(Iif(This.Value =1,'TXT',;
            iif(This.Value =2,'DBF',;
            iif(This.Value =3,'SDF',;
            iif(This.Value =4,'Delimited',;
            space(20))))))
    Endfunc
    Func oOpt_File_1_15.GetRadio()
        This.Parent.oContained.w_Opt_File = This.RadioValue()
        Return .T.
    Endfunc

    Func oOpt_File_1_15.SetRadio()
        This.Parent.oContained.w_Opt_File=Trim(This.Parent.oContained.w_Opt_File)
        This.Value = ;
            iif(This.Parent.oContained.w_Opt_File=='TXT',1,;
            iif(This.Parent.oContained.w_Opt_File=='DBF',2,;
            iif(This.Parent.oContained.w_Opt_File=='SDF',3,;
            iif(This.Parent.oContained.w_Opt_File=='Delimited',4,;
            0))))
    Endfunc

    Add Object oTxt_File_1_17 As StdField With uid="CIRLONFEHJ",rtseq=3,rtrep=.F.,;
        cFormVar = "w_Txt_File", cQueryName = "Txt_File",;
        bObbl = .F. , nPag = 1, Value=Space(200), bMultilanguage =  .F.,;
        ToolTipText = "Inserisci il path+nome file",;
        HelpContextID = 179325091,;
        bGlobalFont=.T.,;
        Height=20, Width=294, Left=107, Top=92, InputMask=Replicate('X',200)


    Add Object oBtn_1_18 As StdButton With uid="KFZKZIEDQW",Left=404, Top=91, Width=24,Height=23,;
        Picture="PROPEN.bmp", Caption="", nPag=1;
        , ToolTipText = ""+MSG_SELECT_FILE+"";
        , HelpContextID = 92081017;
        , bGlobalFont=.T.

    Proc oBtn_1_18.Click()
        With This.Parent.oContained
            cp_PRNBOT (This.Parent.oContained,"SFileClick")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mcalc(.T.)
        Endif
    Endproc


    Add Object oObj_1_21 As cp_setCtrlObjprop With uid="WTFHXDANKA",Left=561, Top=5, Width=78,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Stampante",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 165936170;
        , bGlobalFont=.T.



    Add Object oObj_1_22 As cp_setCtrlObjprop With uid="IXIWKTVXNX",Left=561, Top=31, Width=78,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Stampa su File",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 165936170;
        , bGlobalFont=.T.



    Add Object oObj_1_23 As cp_setCtrlObjprop With uid="XTOBMAAEXJ",Left=561, Top=57, Width=78,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Stampa su File",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 165936170;
        , bGlobalFont=.T.



    Add Object olingua_1_25 As StdTableCombo With uid="LSQYVBRSEJ",rtseq=4,rtrep=.F.,Left=11,Top=24,Width=89,Height=21;
        , ToolTipText = "tipo file";
        , HelpContextID = 242285324;
        , cFormVar="w_lingua",tablefilter="", bObbl = .F. , nPag = 1;
        , cTable='CP_CHPRNDLG.VQR',cKey='Code',cValue='Code',cOrderBy='Code',xDefault=Space(20);
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_1_12 As StdString With uid="SHCDKROQHP",Visible=.T., Left=107, Top=4,;
        Alignment=0, Width=108, Height=18,;
        Caption="Stampante"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_16 As StdString With uid="BQSULVPABR",Visible=.T., Left=9, Top=70,;
        Alignment=0, Width=120, Height=18,;
        Caption="Nome File"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_19 As StdString With uid="UOQLUQMRXZ",Visible=.T., Left=107, Top=71,;
        Alignment=0, Width=148, Height=18,;
        Caption="Stampa su File"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_24 As StdString With uid="UWHRVBTMKK",Visible=.T., Left=9, Top=4,;
        Alignment=0, Width=120, Height=18,;
        Caption="Lingua"  ;
        , bGlobalFont=.T.

    Add Object oBox_1_1 As StdBox With uid="QYMUVVOCEP",Left=9, Top=132, Width=473,Height=2

    Add Object oBox_1_11 As StdBox With uid="DAXSITZOYC",Left=9, Top=63, Width=473,Height=1
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_chprndlg','','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Dialog Window"
Endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
