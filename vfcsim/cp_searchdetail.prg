* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_searchdetail                                                 *
*              Gestione ricerca detail                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-12-20                                                      *
* Last revis.: 2011-03-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
Return(Createobject("tcp_searchdetail",oParentObject))

* --- Class definition
Define Class tcp_searchdetail As StdForm
    Top    = 10
    Left   = 10

    * --- Standard Properties
    Width  = 231
    Height = 180
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2011-03-28"
    HelpContextID=192233995
    max_rt_seq=10

    * --- Constant Properties
    _IDX = 0
    cPrg = "cp_searchdetail"
    cComment = "Gestione ricerca detail"
    oParentObject = .Null.
    Icon = "mask.ico"
    *closable = .f.

    * --- Local Variables
    w_CtrlType = Space(1)
    w_SearchValue = Space(100)
    o_SearchValue = Space(100)
    w_SearchValueN = 0
    o_SearchValueN = 0
    w_SearchValueD = Ctod('  /  /  ')
    o_SearchValueD = Ctod('  /  /  ')
    w_TypeFilter = Space(10)
    w_TypeFilter1 = Space(10)
    o_TypeFilter1 = Space(10)
    w_ExpandForm = 0
    w_SecondSearch = .F.
    w_CHECKED = .F.
    o_CHECKED = .F.
    w_RemPrevFilter = Space(1)
    w_ZoomValueN = .Null.
    w_ZoomValue = .Null.
    * --- Area Manuale = Declare Variables
    * --- cp_searchdetail
    Top    = -1000
    Left   = -1000
    *Left=_Screen.Width + 100
    ShowWindow=1
    AlwaysOnTop=.T.
    Closable=.F.
    TitleBar=0
    * --- disabilito il men� sulla barra
    ControlBox = .F.
    * ---- disabilito l'ingrandimento della maschera
    MinButton=.F.
    MaxButton=.F.
    BorderStyle=2

    *-- INTERNAL USE: Determines whether or not this form should be be released when the deactivate even fires.
    rundeactivaterelease = .T.
    *-- INTERNAL USE: Used to tell the class whether the mouse (cursor) is within the boundaries of the Search/Filter form or not.
    _inform = .T.
    *-- INTERNAL USE: Used to tell the class whether the form is fading or not.
    _infademode = .T.

    *-- INTERNAL USE: Holds the SECONDS since midnight when the last user interaction with this form occurred. Ensures that fading of the form will not occur while the user is actively using the form. See the FadeWait property of the GridExtra class.
    lastuseraction = 0

    *-- INTERNAL USE: Determines whether or not this form should be be released when the deactivate even fires.
    rundeactivaterelease = .T.

    Add Object oTimerInform As TimerInform With ;
        Top = 355, ;
        Left = 42, ;
        Name = "oTimerInform"
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=1, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tcp_searchdetailPag1","cp_searchdetail",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate("Pag.1")
            .Pages(1).oPag.oContained=Thisform
        Endwith
        This.Parent.oFirstControl = This.Page1.oPag.oSearchValue_1_3
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
        * --- Area Manuale = Init Page Frame
        * --- cp_searchdetail
        This.Pages(1).oPag.AddObject("oStr_StringAdvanced","cp_StringAdvanced")
        * --- Fine Area Manuale
    Endproc
    Proc Init(oParentObject)
        If Vartype(m.oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        This.w_ZoomValueN = This.oPgFrm.Pages(1).oPag.ZoomValueN
        This.w_ZoomValue = This.oPgFrm.Pages(1).oPag.ZoomValue
        DoDefault()
    Proc Destroy()
        This.w_ZoomValueN = .Null.
        This.w_ZoomValue = .Null.
        DoDefault()

        * --- Open tables
    Function OpenWorkTables()
        Return(This.OpenAllTables(0))

    Procedure SetPostItConn()
        Return


        * --- Read record and initialize Form variables
    Procedure LoadRec()
    Endproc

    * --- ecpQuery
    Procedure ecpQuery()
        This.BlankRec()
        This.cFunction="Edit"
        This.SetStatus()
        If This.cFunction="Edit"
            This.mEnableControls()
        Endif
    Endproc
    * --- Esc
    Proc ecpQuit()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            This.cFunction = "Edit"
            This.oFirstControl.SetFocus()
            Return
        Endif
        * ---
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Endproc
    Proc QueryUnload()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            Nodefault
            Return
        Endif
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Proc ecpSave()
        If This.TerminateEdit() And This.CheckForm()
            This.LockScreen=.T.
            This.mReplace(.T.)
            This.LockScreen=.F.
            This.NotifyEvent("Done")
            This.Release()
        Endif
    Endproc
    Func OkToQuit()
        Return .T.
    Endfunc

    * --- Blank Form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- cp_searchdetail
        This.Height=40
        This.oPgFrm.Height=40
        This.Left=This.oParentObject.Parent.oRefForm.Left+Objtoclient(This.oParentObject,2)+Iif(Type("_SCREEN.TBMDI.Visible")='L' And _Screen.TBMDI.Visible,0,Sysmetric(3))
        This.nOldLeft=This.Left
        This.Top=This.oParentObject.Parent.oRefForm.Top+Objtoclient(This.oParentObject.Parent,1)+Iif(Type("_SCREEN.TBMDI.Visible")='L' And _Screen.TBMDI.Visible,0,Sysmetric(4)+Sysmetric(9))+This.oParentObject.Parent.Height

        This.oTimerInform.Enabled=.T.
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        With This
            .w_CtrlType=Space(1)
            .w_SearchValue=Space(100)
            .w_SearchValueN=0
            .w_SearchValueD=Ctod("  /  /  ")
            .w_TypeFilter=Space(10)
            .w_TypeFilter1=Space(10)
            .w_ExpandForm=0
            .w_SecondSearch=.F.
            .w_CHECKED=.F.
            .w_RemPrevFilter=Space(1)
            .w_CtrlType = 'C'
            .DoRTCalc(2,4,.F.)
            .w_TypeFilter = Iif(.w_CHECKED, '=', Iif(Empty(.w_TypeFilter), Iif(g_RicPerCont='T', 'Like %', 'Like'), .w_TypeFilter))
            .w_TypeFilter1 = Iif(.w_CHECKED Or Empty(.w_TypeFilter1), '=', .w_TypeFilter1)
            .w_ExpandForm = 1
            .w_SecondSearch = .F.
            .DoRTCalc(9,9,.F.)
            .w_RemPrevFilter = 'S'
            .oPgFrm.Page1.oPag.ZoomValueN.Calculate()
            .oPgFrm.Page1.oPag.ZoomValue.Calculate()
        Endwith
        This.SaveDependsOn()
        This.SetControlsValue()
        This.mHideControls()
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- cp_searchdetail
    Endproc

    Procedure Deactivate
        If !This.rundeactivaterelease
            Return
        Endif
        This.Release()
    Endproc

    Procedure GotFocus
        This.oTimerInform.Enabled=.F.
        DoDefault()
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = Setting operation
    *     Allowed Parameters: Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        * --- Deactivating List page when <> da Query
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt
        i_cFlt =""
        Return (i_cFlt)
    Endfunc

    Proc QueryKeySet(i_cWhere,i_cOrderBy)
    Endproc

    Proc SelectCursor
    Proc GetXKey

        * --- Update Database
    Function mReplace(i_bEditing)
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        This.NotifyEvent('Update start')
        With This
        Endwith
        This.NotifyEvent('Update end')
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(.T.)

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        If i_bUpd
            With This
                .DoRTCalc(1,4,.T.)
                If .o_CHECKED<>.w_CHECKED
                    .w_TypeFilter = Iif(.w_CHECKED, '=', Iif(Empty(.w_TypeFilter), Iif(g_RicPerCont='T', 'Like %', 'Like'), .w_TypeFilter))
                Endif
                If .o_CHECKED<>.w_CHECKED
                    .w_TypeFilter1 = Iif(.w_CHECKED Or Empty(.w_TypeFilter1), '=', .w_TypeFilter1)
                Endif
                If .o_TypeFilter1<>.w_TypeFilter1
                    .Calculate_DQJXHCMTBC()
                Endif
                .DoRTCalc(7,7,.T.)
                If .o_SearchValueD<>.w_SearchValueD.Or. .o_SearchValueN<>.w_SearchValueN.Or. .o_SearchValue<>.w_SearchValue
                    .w_SecondSearch = .F.
                Endif
                .oPgFrm.Page1.oPag.ZoomValueN.Calculate()
                .oPgFrm.Page1.oPag.ZoomValue.Calculate()
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
            Endwith
            This.DoRTCalc(9,10,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
            .oPgFrm.Page1.oPag.ZoomValueN.Calculate()
            .oPgFrm.Page1.oPag.ZoomValue.Calculate()
        Endwith
        Return

    Proc Calculate_MEZADULZWN()
        With This
            * --- Assign type variabile filter/search
            .w_CtrlType = cp_SearchDetailR(This, "Type")
            .w_TypeFilter = Iif(.w_CtrlType='C', 'Like', '=')
        Endwith
    Endproc
    Proc Calculate_DQJXHCMTBC()
        With This
            * --- Assign type filter numeric/data
            .w_TypeFilter = .w_TypeFilter1
        Endwith
    Endproc
    Proc Calculate_CGMNTNQRZM()
        With This
            * --- Carica zoom
            cp_SearchDetailR(This;
                ,"LoadZoom";
                )
        Endwith
    Endproc
    Proc Calculate_LBXNOJPNRF()
        With This
            * --- Seleziona/Deseleziona record
            cp_SearchDetailR(This;
                ,"Checked";
                )
        Endwith
    Endproc

    * --- Enable controls under condition
    Procedure mEnableControls()
        This.oPgFrm.Page1.oPag.oSearchValue_1_3.Enabled = This.oPgFrm.Page1.oPag.oSearchValue_1_3.mCond()
        This.oPgFrm.Page1.oPag.oSearchValueN_1_4.Enabled = This.oPgFrm.Page1.oPag.oSearchValueN_1_4.mCond()
        This.oPgFrm.Page1.oPag.oSearchValueD_1_5.Enabled = This.oPgFrm.Page1.oPag.oSearchValueD_1_5.mCond()
        This.oPgFrm.Page1.oPag.oTypeFilter_1_10.Enabled = This.oPgFrm.Page1.oPag.oTypeFilter_1_10.mCond()
        This.oPgFrm.Page1.oPag.oTypeFilter1_1_11.Enabled = This.oPgFrm.Page1.oPag.oTypeFilter1_1_11.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_6.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_7.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
        This.oPgFrm.Page1.oPag.oBtn_1_8.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        This.oPgFrm.Page1.oPag.oSearchValue_1_3.Visible=!This.oPgFrm.Page1.oPag.oSearchValue_1_3.mHide()
        This.oPgFrm.Page1.oPag.oSearchValueN_1_4.Visible=!This.oPgFrm.Page1.oPag.oSearchValueN_1_4.mHide()
        This.oPgFrm.Page1.oPag.oSearchValueD_1_5.Visible=!This.oPgFrm.Page1.oPag.oSearchValueD_1_5.mHide()
        This.oPgFrm.Page1.oPag.oBtn_1_7.Visible=!This.oPgFrm.Page1.oPag.oBtn_1_7.mHide()
        This.oPgFrm.Page1.oPag.oBtn_1_8.Visible=!This.oPgFrm.Page1.oPag.oBtn_1_8.mHide()
        This.oPgFrm.Page1.oPag.oTypeFilter_1_10.Visible=!This.oPgFrm.Page1.oPag.oTypeFilter_1_10.mHide()
        This.oPgFrm.Page1.oPag.oTypeFilter1_1_11.Visible=!This.oPgFrm.Page1.oPag.oTypeFilter1_1_11.mHide()
        DoDefault()
        Return

        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            If Lower(cEvent)==Lower("Blank")
                .Calculate_MEZADULZWN()
                bRefresh=.T.
            Endif
            .oPgFrm.Page1.oPag.ZoomValueN.Event(cEvent)
            .oPgFrm.Page1.oPag.ZoomValue.Event(cEvent)
            If Lower(cEvent)==Lower("LoadZoom")
                .Calculate_CGMNTNQRZM()
                bRefresh=.T.
            Endif
            If Lower(cEvent)==Lower("w_ZoomValue row checked") Or Lower(cEvent)==Lower("w_ZoomValue row unchecked") Or Lower(cEvent)==Lower("ZoomValue menucheck") Or Lower(cEvent)==Lower("w_ZoomValueN row checked") Or Lower(cEvent)==Lower("w_ZoomValueN row unchecked") Or Lower(cEvent)==Lower("ZoomValueN menucheck")
                .Calculate_LBXNOJPNRF()
                bRefresh=.T.
            Endif
        Endwith
        * --- Area Manuale = Notify Event End
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

    Function SetControlsValue()
        If Not(This.oPgFrm.Page1.oPag.oSearchValue_1_3.Value==This.w_SearchValue)
            This.oPgFrm.Page1.oPag.oSearchValue_1_3.Value=This.w_SearchValue
        Endif
        If Not(This.oPgFrm.Page1.oPag.oSearchValueN_1_4.Value==This.w_SearchValueN)
            This.oPgFrm.Page1.oPag.oSearchValueN_1_4.Value=This.w_SearchValueN
        Endif
        If Not(This.oPgFrm.Page1.oPag.oSearchValueD_1_5.Value==This.w_SearchValueD)
            This.oPgFrm.Page1.oPag.oSearchValueD_1_5.Value=This.w_SearchValueD
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTypeFilter_1_10.RadioValue()==This.w_TypeFilter)
            This.oPgFrm.Page1.oPag.oTypeFilter_1_10.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTypeFilter1_1_11.RadioValue()==This.w_TypeFilter1)
            This.oPgFrm.Page1.oPag.oTypeFilter1_1_11.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oRemPrevFilter_1_16.RadioValue()==This.w_RemPrevFilter)
            This.oPgFrm.Page1.oPag.oRemPrevFilter_1_16.SetRadio()
        Endif
        cp_SetControlsValueExtFlds(This,'')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
            Else
                If Not(i_bnoChk)
                    cp_ErrorMsg(i_cErrorMsg)
                Endif
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

    * --- SaveDependsOn
    Proc SaveDependsOn()
        This.o_SearchValue = This.w_SearchValue
        This.o_SearchValueN = This.w_SearchValueN
        This.o_SearchValueD = This.w_SearchValueD
        This.o_TypeFilter1 = This.w_TypeFilter1
        This.o_CHECKED = This.w_CHECKED
        Return

Enddefine

* --- Define pages as container
Define Class tcp_searchdetailPag1 As StdContainer
    Width  = 229
    Height = 180
    stdWidth  = 229
    stdheight = 180
    resizeXpos=135
    resizeYpos=123
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.

    Add Object oSearchValue_1_3 As StdField With uid="DIMWKFNASX",rtseq=2,rtrep=.F.,;
        cFormVar = "w_SearchValue", cQueryName = "SearchValue",;
        bObbl = .F. , nPag = 1, Value=Space(100), bMultilanguage =  .F.,;
        HelpContextID = 140523114,;
        bGlobalFont=.T.,;
        Height=21, Width=146, Left=3, Top=1, InputMask=Replicate('X',100)

    Func oSearchValue_1_3.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (Not .w_CHECKED)
            Endwith
        Endif
    Endfunc

    Func oSearchValue_1_3.mHide()
        With This.Parent.oContained
            Return (.w_CtrlType<>'C')
        Endwith
    Endfunc

    Add Object oSearchValueN_1_4 As StdField With uid="ODBRSHKJJI",rtseq=3,rtrep=.F.,;
        cFormVar = "w_SearchValueN", cQueryName = "SearchValueN",;
        bObbl = .F. , nPag = 1, Value=0, bMultilanguage =  .F.,;
        HelpContextID = 140842602,;
        bGlobalFont=.T.,;
        Height=21, Width=146, Left=3, Top=1, cSayPict='"9999999999999.9999"', cGetPict='"9999999999999.9999"'

    Func oSearchValueN_1_4.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (Not .w_CHECKED)
            Endwith
        Endif
    Endfunc

    Func oSearchValueN_1_4.mHide()
        With This.Parent.oContained
            Return (.w_CtrlType<>'N')
        Endwith
    Endfunc

    Add Object oSearchValueD_1_5 As StdField With uid="YGGDOAVEMR",rtseq=4,rtrep=.F.,;
        cFormVar = "w_SearchValueD", cQueryName = "SearchValueD",;
        bObbl = .F. , nPag = 1, Value=Ctod("  /  /  "), bMultilanguage =  .F.,;
        HelpContextID = 140801642,;
        bGlobalFont=.T.,;
        Height=21, Width=90, Left=3, Top=1, bNoZoomDateType=.T.

    Func oSearchValueD_1_5.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (Not .w_CHECKED)
            Endwith
        Endif
    Endfunc

    Func oSearchValueD_1_5.mHide()
        With This.Parent.oContained
            Return (.w_CtrlType<>'D')
        Endwith
    Endfunc


    Add Object oBtn_1_6 As StdButton With uid="BDBFGXXSGH",Left=150, Top=1, Width=25,Height=25,;
        Picture="find.ico", Caption="", nPag=1;
        , ToolTipText = "Premere per eseguire la ricerca";
        , HelpContextID = 39831982;
        , ImgSize=16;
        , bGlobalFont=.T.

    Proc oBtn_1_6.Click()
        With This.Parent.oContained
            cp_SearchDetailR(This.Parent.oContained,"Search")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_6.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (.w_CtrlType='C' And Not Empty(.w_SearchValue) Or .w_CtrlType<>'C')
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_7 As StdButton With uid="NTXGYAVXWY",Left=177, Top=1, Width=25,Height=25,;
        Picture="filter.ico", Caption="", nPag=1;
        , ToolTipText = "Premere per eseguire il filtro";
        , HelpContextID = 48166113;
        , ImgSize=16;
        , bGlobalFont=.T.

    Proc oBtn_1_7.Click()
        With This.Parent.oContained
            cp_SearchDetailR(This.Parent.oContained,"Filter")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_7.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (Not .w_CHECKED)
            Endwith
        Endif
    Endfunc

    Func oBtn_1_7.mHide()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (.w_CHECKED)
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_8 As StdButton With uid="TVJQHCICBA",Left=177, Top=1, Width=25,Height=25,;
        Picture="filter.ico", Caption="", nPag=1;
        , HelpContextID = 48166113;
        , ImgSize=16;
        , bGlobalFont=.T.

    Proc oBtn_1_8.Click()
        With This.Parent.oContained
            cp_SearchDetailR(This.Parent.oContained,"Select")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_1_8.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (.w_CHECKED)
            Endwith
        Endif
    Endfunc

    Func oBtn_1_8.mHide()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (Not .w_CHECKED)
            Endwith
        Endif
    Endfunc


    Add Object oBtn_1_9 As StdButton With uid="BIRMKPXGII",Left=204, Top=1, Width=25,Height=25,;
        Picture="esc.ico", Caption="", nPag=1;
        , ToolTipText = "Premere per uscire";
        , HelpContextID = 192691328;
        , ImgSize=16;
        , bGlobalFont=.T.

    Proc oBtn_1_9.Click()
        =cp_StandardFunction(This,"Quit")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oTypeFilter_1_10 As StdCombo With uid="SJFGANFPZK",rtseq=5,rtrep=.F.,Left=3,Top=41,Width=111,Height=19;
        , ToolTipText = "Condizione di filtro o ricerca";
        , HelpContextID = 28176012;
        , cFormVar="w_TypeFilter",RowSource=""+"Comincia per,"+"Contiene,"+"Uguale,"+"Minore uguale,"+"Minore,"+"Maggiore uguale,"+"Maggiore,"+"Non comincia per,"+"Non contiene,"+"Diverso", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oTypeFilter_1_10.RadioValue()
        Return(Iif(This.Value =1,'Like',;
            iif(This.Value =2,'Like %',;
            iif(This.Value =3,'=',;
            iif(This.Value =4,'<=',;
            iif(This.Value =5,'<',;
            iif(This.Value =6,'>=',;
            iif(This.Value =7,'>',;
            iif(This.Value =8,'Not Like',;
            iif(This.Value =9,'Not Like %',;
            iif(This.Value =10,'<>',;
            space(10))))))))))))
    Endfunc
    Func oTypeFilter_1_10.GetRadio()
        This.Parent.oContained.w_TypeFilter = This.RadioValue()
        Return .T.
    Endfunc

    Func oTypeFilter_1_10.SetRadio()
        This.Parent.oContained.w_TypeFilter=Trim(This.Parent.oContained.w_TypeFilter)
        This.Value = ;
            iif(This.Parent.oContained.w_TypeFilter=='Like',1,;
            iif(This.Parent.oContained.w_TypeFilter=='Like %',2,;
            iif(This.Parent.oContained.w_TypeFilter=='=',3,;
            iif(This.Parent.oContained.w_TypeFilter=='<=',4,;
            iif(This.Parent.oContained.w_TypeFilter=='<',5,;
            iif(This.Parent.oContained.w_TypeFilter=='>=',6,;
            iif(This.Parent.oContained.w_TypeFilter=='>',7,;
            iif(This.Parent.oContained.w_TypeFilter=='Not Like',8,;
            iif(This.Parent.oContained.w_TypeFilter=='Not Like %',9,;
            iif(This.Parent.oContained.w_TypeFilter=='<>',10,;
            0))))))))))
    Endfunc

    Func oTypeFilter_1_10.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (Not .w_CHECKED)
            Endwith
        Endif
    Endfunc

    Func oTypeFilter_1_10.mHide()
        With This.Parent.oContained
            Return (.w_CtrlType<>'C')
        Endwith
    Endfunc


    Add Object oTypeFilter1_1_11 As StdCombo With uid="YSXFKELPRY",rtseq=6,rtrep=.F.,Left=3,Top=40,Width=111,Height=19;
        , ToolTipText = "Condizione di filtro o ricerca";
        , HelpContextID = 28163468;
        , cFormVar="w_TypeFilter1",RowSource=""+"Uguale,"+"Minore uguale,"+"Minore,"+"Maggiore uguale,"+"Maggiore,"+"Diverso", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oTypeFilter1_1_11.RadioValue()
        Return(Iif(This.Value =1,'=',;
            iif(This.Value =2,'<=',;
            iif(This.Value =3,'<',;
            iif(This.Value =4,'>=',;
            iif(This.Value =5,'>',;
            iif(This.Value =6,'<>',;
            space(10))))))))
    Endfunc
    Func oTypeFilter1_1_11.GetRadio()
        This.Parent.oContained.w_TypeFilter1 = This.RadioValue()
        Return .T.
    Endfunc

    Func oTypeFilter1_1_11.SetRadio()
        This.Parent.oContained.w_TypeFilter1=Trim(This.Parent.oContained.w_TypeFilter1)
        This.Value = ;
            iif(This.Parent.oContained.w_TypeFilter1=='=',1,;
            iif(This.Parent.oContained.w_TypeFilter1=='<=',2,;
            iif(This.Parent.oContained.w_TypeFilter1=='<',3,;
            iif(This.Parent.oContained.w_TypeFilter1=='>=',4,;
            iif(This.Parent.oContained.w_TypeFilter1=='>',5,;
            iif(This.Parent.oContained.w_TypeFilter1=='<>',6,;
            0))))))
    Endfunc

    Func oTypeFilter1_1_11.mCond()
        If(Vartype(This.Parent.oContained)<>'O')
            Return .T.
        Else
            With This.Parent.oContained
                Return (Not .w_CHECKED)
            Endwith
        Endif
    Endfunc

    Func oTypeFilter1_1_11.mHide()
        With This.Parent.oContained
            Return (.w_CtrlType='C')
        Endwith
    Endfunc

    Add Object oRemPrevFilter_1_16 As StdCheck With uid="NLDZBMOWAM",rtseq=10,rtrep=.F.,Left=116, Top=40, Caption="Annulla preced.", Enabled=.F.,;
        ToolTipText = "Annulla filtro precedentemente inserito",;
        HelpContextID = 122322730,;
        cFormVar="w_RemPrevFilter", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oRemPrevFilter_1_16.RadioValue()
        Return(Iif(This.Value =1,'S',;
            ' '))
    Endfunc
    Func oRemPrevFilter_1_16.GetRadio()
        This.Parent.oContained.w_RemPrevFilter = This.RadioValue()
        Return .T.
    Endfunc

    Func oRemPrevFilter_1_16.SetRadio()
        This.Parent.oContained.w_RemPrevFilter=Trim(This.Parent.oContained.w_RemPrevFilter)
        This.Value = ;
            iif(This.Parent.oContained.w_RemPrevFilter=='S',1,;
            0)
    Endfunc


    Add Object ZoomValueN As cp_szoombox With uid="XOFIVMTMNB",Left=-8, Top=59, Width=243,Height=121,;
        caption='Object',;
        cZoomFile="CP_ZOOMVALUEN",bOptions=.F.,bAdvOptions=.F.,cZoomOnZoom="",cTable="cpazi",bQueryOnLoad=.F.,bReadOnly=.T.,cMenuFile="",bQueryOnDblClick=.F.,bSearchFilterOnClick=.F.,;
        cEvent = "Init",;
        nPag=1;
        , ToolTipText = "Elenco valori presenti nello zoom";
        , HelpContextID = 52363898;
        , bGlobalFont=.T.



    Add Object ZoomValue As cp_szoombox With uid="BQESICZURG",Left=-8, Top=59, Width=243,Height=121,;
        caption='Object',;
        cZoomFile="CP_ZOOMVALUE",bOptions=.F.,bAdvOptions=.F.,cZoomOnZoom="",cTable="cpazi",bQueryOnLoad=.F.,bReadOnly=.T.,cMenuFile="",bQueryOnDblClick=.F.,bSearchFilterOnClick=.F.,;
        cEvent = "Init",;
        nPag=1;
        , ToolTipText = "Elenco valori presenti nello zoom";
        , HelpContextID = 52363898;
        , bGlobalFont=.T.

Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_searchdetail','','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Dialog Window"
Endproc

* --- Area Manuale = Functions & Procedures
* --- cp_searchdetail
**************************************************
* Definizione label per filtri avanzati
*
Define Class cp_StringAdvanced As StdString
    uid="YYABKDNGPX"
    Visible=.T.
    Left=6
    Top=24
    Alignment=0
    Width=130
    Height=18
    Caption="Visualizza filtri avanzati"
    bGlobalFont=.T.

    Procedure MouseEnter
        Lparameters nButton, nShift, nXCoord, nYCoord

        With This
            .FontUnderline = .T.
            .ForeColor = Rgb(0,0,255)
        Endwith
    Endproc

    Procedure MouseLeave
        Lparameters nButton, nShift, nXCoord, nYCoord

        With This
            .FontUnderline = .F.
            .ForeColor = Rgb(128,128,255)
        Endwith
    Endproc

    Procedure Click
        With This
            If .Caption = cp_Translate("Visualizza filtri avanzati")
                .Caption = cp_Translate("Nascondi filtri avanzati")
                .Parent.Parent.Parent.Parent.Height=180
                .Parent.Parent.Parent.Parent.oPgFrm.Height=180
                If .Parent.oContained.w_ExpandForm<>3
                    .Parent.oContained.w_ExpandForm=3
                    Do cp_SearchDetailR With .Parent.oContained, "LoadZoom"
                Endif
            Else
                .Caption = cp_Translate("Visualizza filtri avanzati")
                .Parent.Parent.Parent.Parent.Height=40
                .Parent.Parent.Parent.Parent.oPgFrm.Height=40
            Endif
        Endwith
    Endproc
Enddefine

**************************************************
*-- Class:        TimerInform
*-- ParentClass:  timer
*
Define Class TimerInform As Timer

    Height = 23
    Width = 23
    Name = "TimerInform"
    Interval=500

    Procedure Timer
        Try && Ignor errors for this feature (though there shouldn't be any errors thrown)
            If Mrow(This.Parent.Name,3) = -1 Or Mcol(This.Parent.Name,3) = -1
                If !This.Parent.rundeactivaterelease
                    Return
                Endif
                This.Parent.Release()
            Endif
        Catch
        Endtry
    Endproc
Enddefine
**************************************************
* --- Fine Area Manuale
