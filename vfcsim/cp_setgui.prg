* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_setgui                                                       *
*              Configurazione interfaccia                                      *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_132]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-04                                                      *
* Last revis.: 2012-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
Local i_Obj
If Vartype(m.oParentObject)<>'O'
    Wait Wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
    Return(.Null.)
Endif
If i_cType='edit'
    Return(.Null.)
Else
    If i_cType='paint'
        i_Obj=Createobject("tcp_setgui")
        i_Obj.Cnt.oParentObject=oParentObject
        i_Obj.LinkPCClick()
    Else
        i_Obj=Createobject('stdLazyChild',oParentObject,Program())
    Endif
Endif
Return(i_Obj)

Proc _CreateObject(retobj)
    retobj=Createobject("tccp_setgui")
    Return

Proc _AddObject(i_oCnt,i_name)
    i_oCnt.AddObject(i_name,"tccp_setgui")
    Return

    * --- Class definition
Define Class tcp_setgui As StdPCForm
    Width  = 535
    Height = 363+35
    Top    = 6
    Left   = 9
    cComment = "Configurazione interfaccia"
    cPrg = "cp_setgui"
    HelpContextID=22243636
    Add Object Cnt As tccp_setgui
Enddefine

Define Class tscp_setgui As PCContext
    w_usrcode = 0
    w_CIVTHEME = 0
    w_CIREPBEH = Space(1)
    w_CICPTBAR = Space(1)
    w_CIDSKBAR = Space(1)
    w_CISTABAR = Space(1)
    w_CISHWBTN = Space(1)
    w_CITBSIZE = 0
    w_CIMDIFRM = Space(1)
    w_CITABMEN = Space(1)
    w_CIXPTHEM = 0
    w_CIMENFIX = Space(1)
    w_CISEARMN = Space(1)
    w_CITOOLMN = Space(1)
    w_CIBCKGRD = Space(1)
    w_ciwaitwd = Space(1)
    w_CISELECL = 0
    w_CIEDTCOL = 0
    w_CIGRIDCL = 0
    w_CIDTLCLR = 0
    w_CIDSBLBC = 0
    w_CIDSBLFC = 0
    w_CISCRCOL = 0
    w_COLORCTSEL = Space(10)
    w_CIEVIZOM = 0
    w_COLORCTED = Space(10)
    w_COLORFCTDSBL = Space(10)
    w_COLORBCTDSBL = Space(10)
    w_COLORGRD = Space(10)
    w_COLORSCREEN = Space(10)
    w_COLORROWDTL = Space(10)
    w_CILBLFNM = Space(50)
    w_CITXTFNM = Space(50)
    w_CICBXFNM = Space(50)
    w_CIBTNFNM = Space(50)
    w_CIGRDFNM = Space(50)
    w_CIPAGFNM = Space(50)
    w_CILBLFSZ = 0
    w_CITXTFSZ = 0
    w_CICBXFSZ = 0
    w_CIBTNFSZ = 0
    w_CIGRDFSZ = 0
    w_CIPAGFSZ = 0
    w_CIDSKMEN = Space(1)
    w_CINAVSTA = Space(1)
    w_CINAVNUB = 0
    w_CINAVDIM = 0
    w_CIMNAFNM = Space(50)
    w_CIMNAFSZ = 0
    w_CIWMAFNM = Space(50)
    w_CIWMAFSZ = 0
    w_RET = Space(1)
    w_CIOBLCOL = 0
    w_COLORCTOB = Space(10)
    w_ciobl_on = Space(1)
    w_COLORZOM = Space(10)
    w_CICOLZOM = 0
    w_cilblfit = Space(1)
    w_citxtfit = Space(1)
    w_cicbxfit = Space(1)
    w_cibtnfit = Space(1)
    w_cigrdfit = Space(1)
    w_cipagfit = Space(1)
    w_cilblfbo = Space(1)
    w_citxtfbo = Space(1)
    w_cicbxfbo = Space(1)
    w_cibtnfbo = Space(1)
    w_cigrdfbo = Space(1)
    w_cipagfbo = Space(1)
    w_CICTRGRD = Space(1)
    w_CIMRKGRD = Space(1)
    w_CIAZOOML = Space(1)
    w_CIZOESPR = Space(1)
    w_CIHEHEZO = 0
    w_CIHEROZO = 0
    Proc Save(oFrom)
        This.w_usrcode = oFrom.w_usrcode
        This.w_CIVTHEME = oFrom.w_CIVTHEME
        This.w_CIREPBEH = oFrom.w_CIREPBEH
        This.w_CICPTBAR = oFrom.w_CICPTBAR
        This.w_CIDSKBAR = oFrom.w_CIDSKBAR
        This.w_CISTABAR = oFrom.w_CISTABAR
        This.w_CISHWBTN = oFrom.w_CISHWBTN
        This.w_CITBSIZE = oFrom.w_CITBSIZE
        This.w_CIMDIFRM = oFrom.w_CIMDIFRM
        This.w_CITABMEN = oFrom.w_CITABMEN
        This.w_CIXPTHEM = oFrom.w_CIXPTHEM
        This.w_CIMENFIX = oFrom.w_CIMENFIX
        This.w_CISEARMN = oFrom.w_CISEARMN
        This.w_CITOOLMN = oFrom.w_CITOOLMN
        This.w_CIBCKGRD = oFrom.w_CIBCKGRD
        This.w_ciwaitwd = oFrom.w_ciwaitwd
        This.w_CISELECL = oFrom.w_CISELECL
        This.w_CIEDTCOL = oFrom.w_CIEDTCOL
        This.w_CIGRIDCL = oFrom.w_CIGRIDCL
        This.w_CIDTLCLR = oFrom.w_CIDTLCLR
        This.w_CIDSBLBC = oFrom.w_CIDSBLBC
        This.w_CIDSBLFC = oFrom.w_CIDSBLFC
        This.w_CISCRCOL = oFrom.w_CISCRCOL
        This.w_COLORCTSEL = oFrom.w_COLORCTSEL
        This.w_CIEVIZOM = oFrom.w_CIEVIZOM
        This.w_COLORCTED = oFrom.w_COLORCTED
        This.w_COLORFCTDSBL = oFrom.w_COLORFCTDSBL
        This.w_COLORBCTDSBL = oFrom.w_COLORBCTDSBL
        This.w_COLORGRD = oFrom.w_COLORGRD
        This.w_COLORSCREEN = oFrom.w_COLORSCREEN
        This.w_COLORROWDTL = oFrom.w_COLORROWDTL
        This.w_CILBLFNM = oFrom.w_CILBLFNM
        This.w_CITXTFNM = oFrom.w_CITXTFNM
        This.w_CICBXFNM = oFrom.w_CICBXFNM
        This.w_CIBTNFNM = oFrom.w_CIBTNFNM
        This.w_CIGRDFNM = oFrom.w_CIGRDFNM
        This.w_CIPAGFNM = oFrom.w_CIPAGFNM
        This.w_CILBLFSZ = oFrom.w_CILBLFSZ
        This.w_CITXTFSZ = oFrom.w_CITXTFSZ
        This.w_CICBXFSZ = oFrom.w_CICBXFSZ
        This.w_CIBTNFSZ = oFrom.w_CIBTNFSZ
        This.w_CIGRDFSZ = oFrom.w_CIGRDFSZ
        This.w_CIPAGFSZ = oFrom.w_CIPAGFSZ
        This.w_CIDSKMEN = oFrom.w_CIDSKMEN
        This.w_CINAVSTA = oFrom.w_CINAVSTA
        This.w_CINAVNUB = oFrom.w_CINAVNUB
        This.w_CINAVDIM = oFrom.w_CINAVDIM
        This.w_CIMNAFNM = oFrom.w_CIMNAFNM
        This.w_CIMNAFSZ = oFrom.w_CIMNAFSZ
        This.w_CIWMAFNM = oFrom.w_CIWMAFNM
        This.w_CIWMAFSZ = oFrom.w_CIWMAFSZ
        This.w_RET = oFrom.w_RET
        This.w_CIOBLCOL = oFrom.w_CIOBLCOL
        This.w_COLORCTOB = oFrom.w_COLORCTOB
        This.w_ciobl_on = oFrom.w_ciobl_on
        This.w_COLORZOM = oFrom.w_COLORZOM
        This.w_CICOLZOM = oFrom.w_CICOLZOM
        This.w_cilblfit = oFrom.w_cilblfit
        This.w_citxtfit = oFrom.w_citxtfit
        This.w_cicbxfit = oFrom.w_cicbxfit
        This.w_cibtnfit = oFrom.w_cibtnfit
        This.w_cigrdfit = oFrom.w_cigrdfit
        This.w_cipagfit = oFrom.w_cipagfit
        This.w_cilblfbo = oFrom.w_cilblfbo
        This.w_citxtfbo = oFrom.w_citxtfbo
        This.w_cicbxfbo = oFrom.w_cicbxfbo
        This.w_cibtnfbo = oFrom.w_cibtnfbo
        This.w_cigrdfbo = oFrom.w_cigrdfbo
        This.w_cipagfbo = oFrom.w_cipagfbo
        This.w_CICTRGRD = oFrom.w_CICTRGRD
        This.w_CIMRKGRD = oFrom.w_CIMRKGRD
        This.w_CIAZOOML = oFrom.w_CIAZOOML
        This.w_CIZOESPR = oFrom.w_CIZOESPR
        This.w_CIHEHEZO = oFrom.w_CIHEHEZO
        This.w_CIHEROZO = oFrom.w_CIHEROZO
        PCContext::Save(oFrom)
    Proc Load(oTo)
        oTo.w_usrcode = This.w_usrcode
        oTo.w_CIVTHEME = This.w_CIVTHEME
        oTo.w_CIREPBEH = This.w_CIREPBEH
        oTo.w_CICPTBAR = This.w_CICPTBAR
        oTo.w_CIDSKBAR = This.w_CIDSKBAR
        oTo.w_CISTABAR = This.w_CISTABAR
        oTo.w_CISHWBTN = This.w_CISHWBTN
        oTo.w_CITBSIZE = This.w_CITBSIZE
        oTo.w_CIMDIFRM = This.w_CIMDIFRM
        oTo.w_CITABMEN = This.w_CITABMEN
        oTo.w_CIXPTHEM = This.w_CIXPTHEM
        oTo.w_CIMENFIX = This.w_CIMENFIX
        oTo.w_CISEARMN = This.w_CISEARMN
        oTo.w_CITOOLMN = This.w_CITOOLMN
        oTo.w_CIBCKGRD = This.w_CIBCKGRD
        oTo.w_ciwaitwd = This.w_ciwaitwd
        oTo.w_CISELECL = This.w_CISELECL
        oTo.w_CIEDTCOL = This.w_CIEDTCOL
        oTo.w_CIGRIDCL = This.w_CIGRIDCL
        oTo.w_CIDTLCLR = This.w_CIDTLCLR
        oTo.w_CIDSBLBC = This.w_CIDSBLBC
        oTo.w_CIDSBLFC = This.w_CIDSBLFC
        oTo.w_CISCRCOL = This.w_CISCRCOL
        oTo.w_COLORCTSEL = This.w_COLORCTSEL
        oTo.w_CIEVIZOM = This.w_CIEVIZOM
        oTo.w_COLORCTED = This.w_COLORCTED
        oTo.w_COLORFCTDSBL = This.w_COLORFCTDSBL
        oTo.w_COLORBCTDSBL = This.w_COLORBCTDSBL
        oTo.w_COLORGRD = This.w_COLORGRD
        oTo.w_COLORSCREEN = This.w_COLORSCREEN
        oTo.w_COLORROWDTL = This.w_COLORROWDTL
        oTo.w_CILBLFNM = This.w_CILBLFNM
        oTo.w_CITXTFNM = This.w_CITXTFNM
        oTo.w_CICBXFNM = This.w_CICBXFNM
        oTo.w_CIBTNFNM = This.w_CIBTNFNM
        oTo.w_CIGRDFNM = This.w_CIGRDFNM
        oTo.w_CIPAGFNM = This.w_CIPAGFNM
        oTo.w_CILBLFSZ = This.w_CILBLFSZ
        oTo.w_CITXTFSZ = This.w_CITXTFSZ
        oTo.w_CICBXFSZ = This.w_CICBXFSZ
        oTo.w_CIBTNFSZ = This.w_CIBTNFSZ
        oTo.w_CIGRDFSZ = This.w_CIGRDFSZ
        oTo.w_CIPAGFSZ = This.w_CIPAGFSZ
        oTo.w_CIDSKMEN = This.w_CIDSKMEN
        oTo.w_CINAVSTA = This.w_CINAVSTA
        oTo.w_CINAVNUB = This.w_CINAVNUB
        oTo.w_CINAVDIM = This.w_CINAVDIM
        oTo.w_CIMNAFNM = This.w_CIMNAFNM
        oTo.w_CIMNAFSZ = This.w_CIMNAFSZ
        oTo.w_CIWMAFNM = This.w_CIWMAFNM
        oTo.w_CIWMAFSZ = This.w_CIWMAFSZ
        oTo.w_RET = This.w_RET
        oTo.w_CIOBLCOL = This.w_CIOBLCOL
        oTo.w_COLORCTOB = This.w_COLORCTOB
        oTo.w_ciobl_on = This.w_ciobl_on
        oTo.w_COLORZOM = This.w_COLORZOM
        oTo.w_CICOLZOM = This.w_CICOLZOM
        oTo.w_cilblfit = This.w_cilblfit
        oTo.w_citxtfit = This.w_citxtfit
        oTo.w_cicbxfit = This.w_cicbxfit
        oTo.w_cibtnfit = This.w_cibtnfit
        oTo.w_cigrdfit = This.w_cigrdfit
        oTo.w_cipagfit = This.w_cipagfit
        oTo.w_cilblfbo = This.w_cilblfbo
        oTo.w_citxtfbo = This.w_citxtfbo
        oTo.w_cicbxfbo = This.w_cicbxfbo
        oTo.w_cibtnfbo = This.w_cibtnfbo
        oTo.w_cigrdfbo = This.w_cigrdfbo
        oTo.w_cipagfbo = This.w_cipagfbo
        oTo.w_CICTRGRD = This.w_CICTRGRD
        oTo.w_CIMRKGRD = This.w_CIMRKGRD
        oTo.w_CIAZOOML = This.w_CIAZOOML
        oTo.w_CIZOESPR = This.w_CIZOESPR
        oTo.w_CIHEHEZO = This.w_CIHEHEZO
        oTo.w_CIHEROZO = This.w_CIHEROZO
        PCContext::Load(oTo)
Enddefine

Define Class tccp_setgui As StdPCContainer
    Top    = 0
    Left   = 0

    * --- Standard Properties
    Width  = 535
    Height = 363+35
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2012-03-01"
    HelpContextID=22243636
    max_rt_seq=75

    * --- Constant Properties
    cpsetgui_IDX = 0
    CPUSERS_IDX = 0
    cFile = "cpsetgui"
    cKeySelect = "usrcode"
    cKeyWhere  = "usrcode=this.w_usrcode"
    cKeyWhereODBC = '"usrcode="+cp_ToStrODBC(this.w_usrcode)';

    cKeyWhereODBCqualified = '"cpsetgui.usrcode="+cp_ToStrODBC(this.w_usrcode)';

    cPrg = "cp_setgui"
    cComment = "Configurazione interfaccia"
    Icon = "anag.ico"
    * --- Area Manuale = Properties
    * --- Fine Area Manuale

    * --- Local Variables
    w_usrcode = 0
    w_CIVTHEME = 0
    o_CIVTHEME = 0
    w_CIREPBEH = Space(1)
    w_CICPTBAR = Space(1)
    w_CIDSKBAR = Space(1)
    w_CISTABAR = Space(1)
    w_CISHWBTN = Space(1)
    w_CITBSIZE = 0
    w_CIMDIFRM = Space(1)
    w_CITABMEN = Space(1)
    o_CITABMEN = Space(1)
    w_CIXPTHEM = 0
    w_CIMENFIX = Space(1)
    w_CISEARMN = Space(1)
    w_CITOOLMN = Space(1)
    w_CIBCKGRD = Space(1)
    w_ciwaitwd = Space(1)
    w_CISELECL = 0
    w_CIEDTCOL = 0
    w_CIGRIDCL = 0
    w_CIDTLCLR = 0
    w_CIDSBLBC = 0
    w_CIDSBLFC = 0
    w_CISCRCOL = 0
    w_COLORCTSEL = Space(10)
    w_CIEVIZOM = 0
    w_COLORCTED = Space(10)
    w_COLORFCTDSBL = Space(10)
    w_COLORBCTDSBL = Space(10)
    w_COLORGRD = Space(10)
    w_COLORSCREEN = Space(10)
    w_COLORROWDTL = Space(10)
    w_CILBLFNM = Space(50)
    w_CITXTFNM = Space(50)
    w_CICBXFNM = Space(50)
    w_CIBTNFNM = Space(50)
    w_CIGRDFNM = Space(50)
    w_CIPAGFNM = Space(50)
    w_CILBLFSZ = 0
    w_CITXTFSZ = 0
    w_CICBXFSZ = 0
    w_CIBTNFSZ = 0
    w_CIGRDFSZ = 0
    w_CIPAGFSZ = 0
    w_CIDSKMEN = Space(1)
    w_CINAVSTA = Space(1)
    w_CINAVNUB = 0
    w_CINAVDIM = 0
    w_CIMNAFNM = Space(50)
    w_CIMNAFSZ = 0
    w_CIWMAFNM = Space(50)
    w_CIWMAFSZ = 0
    w_RET = Space(1)
    w_CIOBLCOL = 0
    w_COLORCTOB = Space(10)
    w_ciobl_on = Space(1)
    w_COLORZOM = Space(10)
    w_CICOLZOM = 0
    w_cilblfit = Space(1)
    w_citxtfit = Space(1)
    w_cicbxfit = Space(1)
    w_cibtnfit = Space(1)
    w_cigrdfit = Space(1)
    w_cipagfit = Space(1)
    w_cilblfbo = Space(1)
    w_citxtfbo = Space(1)
    w_cicbxfbo = Space(1)
    w_cibtnfbo = Space(1)
    w_cigrdfbo = Space(1)
    w_cipagfbo = Space(1)
    w_CICTRGRD = Space(1)
    o_CICTRGRD = Space(1)
    w_CIMRKGRD = Space(1)
    w_CIAZOOML = Space(1)
    w_CIZOESPR = Space(1)
    w_CIHEHEZO = 0
    w_CIHEROZO = 0
    * --- Area Manuale = Declare Variables
    * --- cp_setgui
    Proc SetDefault
        Local l_usercode, l_bLoaded
        With This
            l_usercode = .w_usrcode
            l_bLoaded = .bLoaded
            .BlankRec()
            .w_usrcode = m.l_usercode
            .bUpdated = .T.
            .bLoaded = m.l_bLoaded
        Endwith
    Endproc

    Proc SetColor(pVar)
        Local L_ExecCMD, l_Color
        L_ExecCMD= 'this.w_'+pVar
        l_Color = Getcolor(&L_ExecCMD)
        If l_Color <> -1
            L_ExecCMD= L_ExecCMD+'= m.l_COLOR'
            &L_ExecCMD
        Endif
    Endproc
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=5, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tcp_setguiPag1","cp_setgui",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate("Impostazioni")
            .Pages(1).HelpContextID = 266791235
            .Pages(2).AddObject("oPag","tcp_setguiPag2","cp_setgui",2)
            .Pages(2).oPag.Visible=.T.
            .Pages(2).Caption=cp_Translate("Colori")
            .Pages(2).HelpContextID = 133181508
            .Pages(3).AddObject("oPag","tcp_setguiPag3","cp_setgui",3)
            .Pages(3).oPag.Visible=.T.
            .Pages(3).Caption=cp_Translate("Font")
            .Pages(3).HelpContextID = 161361220
            .Pages(4).AddObject("oPag","tcp_setguiPag4","cp_setgui",4)
            .Pages(4).oPag.Visible=.T.
            .Pages(4).Caption=cp_Translate("Desktop men�")
            .Pages(4).HelpContextID = 165343276
            .Pages(5).AddObject("oPag","tcp_setguiPag5","cp_setgui",5)
            .Pages(5).oPag.Visible=.T.
            .Pages(5).Caption=cp_Translate("Elenchi / zoom")
            .Pages(5).HelpContextID = 10169563
        Endwith
        This.Parent.oFirstControl = This.Page1.oPag.oCIVTHEME_1_2
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
        * --- Area Manuale = Init Page Frame
        * --- cp_setgui
        This.Pages(1).Caption=cp_Translate(MSG_SETTINGS_LABEL)
        This.Pages(2).Caption=cp_Translate(MSG_COLOR)
        This.Pages(3).Caption=cp_Translate(MSG_FONT)
        This.Pages(4).Caption=cp_Translate(MSG_DESKTOP_MENU)
        * --- Fine Area Manuale
    Endproc

    * --- Opening Tables
    Function OpenWorkTables()
        Dimension This.cWorkTables[2]
        This.cWorkTables[1]='CPUSERS'
        This.cWorkTables[2]='cpsetgui'
        * --- Area Manuale = Open Work Table
        * --- Fine Area Manuale
        Return(This.OpenAllTables(2))

    Procedure SetPostItConn()
        This.bPostIt=i_ServerConn[i_TableProp[this.cpsetgui_IDX,5],7]
        This.nPostItConn=i_TableProp[this.cpsetgui_IDX,3]
        Return

    Procedure NewContext()
        Return(Createobject('tscp_setgui'))


        * --- Read record and initialize Form variables
    Procedure LoadRec()
        Local i_cKey,i_nRes,i_cTable,i_nConn
        Local i_cSel,i_cDatabaseType,i_nFlds
        * --- Area Manuale = Load Record Init
        * --- Fine Area Manuale
        If Not(This.bOnScreen)
            This.nDeferredFillRec=1
            Return
        Endif
        If This.cFunction='Load'
            This.BlankRec()
            Return
        Endif
        This.nDeferredFillRec=0
        This.bF10=.F.
        This.bUpdated=.F.
        * --- The following Select reads the record
        *
        * select * from cpsetgui where usrcode=KeySet.usrcode
        *
        i_nConn = i_TableProp[this.cpsetgui_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.cpsetgui_IDX,2])
        If i_nConn<>0
            i_nFlds = i_dcx.getfieldscount('cpsetgui')
            i_cDatabaseType = cp_GetDatabaseType(i_nConn)
            i_cSel = "cpsetgui.*"
            i_cKey = This.cKeyWhereODBCqualified
            i_cTable = i_cTable+' cpsetgui '
            i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,This.cCursor)
            This.bLoaded = i_nRes<>-1 And Not(Eof())
        Else
            * i_cKey = this.cKeyWhere
            i_cKey = cp_PKFox(i_cTable  ,'usrcode',This.w_usrcode  )
            Select * From (i_cTable) Where &i_cKey Into Cursor (This.cCursor) nofilter
            This.bLoaded = Not(Eof())
        Endif
        * --- Copy values in work variables
        If This.bLoaded
            With This
                .w_COLORCTSEL = Space(10)
                .w_COLORCTED = Space(10)
                .w_COLORFCTDSBL = Space(10)
                .w_COLORBCTDSBL = Space(10)
                .w_COLORGRD = Space(10)
                .w_COLORSCREEN = Space(10)
                .w_COLORROWDTL = Space(10)
                .w_RET = Space(1)
                .w_COLORCTOB = Space(10)
                .w_COLORZOM = Space(10)
                .w_usrcode = Nvl(usrcode,0)
                .w_CIVTHEME = Nvl(CIVTHEME,0)
                .w_CIREPBEH = Nvl(CIREPBEH,Space(1))
                .w_CICPTBAR = Nvl(CICPTBAR,Space(1))
                .w_CIDSKBAR = Nvl(CIDSKBAR,Space(1))
                .w_CISTABAR = Nvl(CISTABAR,Space(1))
                .w_CISHWBTN = Nvl(CISHWBTN,Space(1))
                .w_CITBSIZE = Nvl(CITBSIZE,0)
                .w_CIMDIFRM = Nvl(CIMDIFRM,Space(1))
                .w_CITABMEN = Nvl(CITABMEN,Space(1))
                .w_CIXPTHEM = Nvl(CIXPTHEM,0)
                .w_CIMENFIX = Nvl(CIMENFIX,Space(1))
                .w_CISEARMN = Nvl(CISEARMN,Space(1))
                .w_CITOOLMN = Nvl(CITOOLMN,Space(1))
                .w_CIBCKGRD = Nvl(CIBCKGRD,Space(1))
                .w_ciwaitwd = Nvl(ciwaitwd,Space(1))
                .w_CISELECL = Nvl(CISELECL,0)
                .w_CIEDTCOL = Nvl(CIEDTCOL,0)
                .w_CIGRIDCL = Nvl(CIGRIDCL,0)
                .w_CIDTLCLR = Nvl(CIDTLCLR,0)
                .w_CIDSBLBC = Nvl(CIDSBLBC,0)
                .w_CIDSBLFC = Nvl(CIDSBLFC,0)
                .w_CISCRCOL = Nvl(CISCRCOL,0)
                .w_CIEVIZOM = Nvl(CIEVIZOM,0)
                .oPgFrm.Page2.oPag.oObj_2_19.Calculate(.w_CIEDTCOL)
                .oPgFrm.Page2.oPag.oObj_2_20.Calculate(.w_CISELECL)
                .oPgFrm.Page2.oPag.oObj_2_24.Calculate(.w_CIDSBLFC)
                .oPgFrm.Page2.oPag.oObj_2_25.Calculate(.w_CIDSBLBC)
                .oPgFrm.Page2.oPag.oObj_2_43.Calculate(.w_CISCRCOL)
                .oPgFrm.Page2.oPag.oObj_2_44.Calculate(.w_CIDTLCLR)
                .oPgFrm.Page2.oPag.oObj_2_45.Calculate(.w_CIGRIDCL)
                .w_CILBLFNM = Nvl(CILBLFNM,Space(50))
                .w_CITXTFNM = Nvl(CITXTFNM,Space(50))
                .w_CICBXFNM = Nvl(CICBXFNM,Space(50))
                .w_CIBTNFNM = Nvl(CIBTNFNM,Space(50))
                .w_CIGRDFNM = Nvl(CIGRDFNM,Space(50))
                .w_CIPAGFNM = Nvl(CIPAGFNM,Space(50))
                .w_CILBLFSZ = Nvl(CILBLFSZ,0)
                .w_CITXTFSZ = Nvl(CITXTFSZ,0)
                .w_CICBXFSZ = Nvl(CICBXFSZ,0)
                .w_CIBTNFSZ = Nvl(CIBTNFSZ,0)
                .w_CIGRDFSZ = Nvl(CIGRDFSZ,0)
                .w_CIPAGFSZ = Nvl(CIPAGFSZ,0)
                .w_CIDSKMEN = Nvl(CIDSKMEN,Space(1))
                .w_CINAVSTA = Nvl(CINAVSTA,Space(1))
                .w_CINAVNUB = Nvl(CINAVNUB,0)
                .w_CINAVDIM = Nvl(CINAVDIM,0)
                .w_CIMNAFNM = Nvl(CIMNAFNM,Space(50))
                .w_CIMNAFSZ = Nvl(CIMNAFSZ,0)
                .w_CIWMAFNM = Nvl(CIWMAFNM,Space(50))
                .w_CIWMAFSZ = Nvl(CIWMAFSZ,0)
                .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_THEME_APPLIC_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_THEME)
                .oPgFrm.Page1.oPag.oObj_1_32.Calculate(MSG_VIS_TOOL_BAR_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_33.Calculate(MSG_VIS_APP_BAR_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_34.Calculate(MSG_VIS_STATUS_BAR_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_35.Calculate(MSG_VIS_CONT_BUTTON_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_36.Calculate(MSG_DIM_IMM_TOOLBAR)
                .oPgFrm.Page1.oPag.oObj_1_37.Calculate(MSG_ICON_TOOLBAR)
                .oPgFrm.Page1.oPag.oObj_1_38.Calculate(MSG_VIS_MENUBAR_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_39.Calculate(MSG_VIS_CTRL_FIND_MENU_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_40.Calculate(MSG_VIS_TOOLMENU_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_41.Calculate(MSG_VIS_GRAD_BACKGOUND_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_42.Calculate(MSG_VIS_MODE)
                .oPgFrm.Page1.oPag.oObj_1_43.Calculate(MSG_TOOLBAR_STATUSBAR)
                .oPgFrm.Page1.oPag.oObj_1_44.Calculate(MSG_FORM_CONTROL)
                .oPgFrm.Page1.oPag.oObj_1_45.Calculate(MSG_TAB)
                .oPgFrm.Page1.oPag.oObj_1_46.Calculate(MSG_THEME_XP)
                .oPgFrm.Page1.oPag.oObj_1_47.Calculate(MSG_MENU)
                .oPgFrm.Page1.oPag.oObj_1_48.Calculate(MSG_VIS_MASK_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_49.Calculate(MSG_VIS_TOOL_BAR)
                .oPgFrm.Page1.oPag.oObj_1_50.Calculate(MSG_VIS_APP_BAR)
                .oPgFrm.Page1.oPag.oObj_1_51.Calculate(MSG_VIS_STATUS_BAR)
                .oPgFrm.Page1.oPag.oObj_1_52.Calculate(MSG_VIS_CONT_BUTTON)
                .oPgFrm.Page1.oPag.oObj_1_53.Calculate(MSG_VIS_MENUBAR)
                .oPgFrm.Page1.oPag.oObj_1_54.Calculate(MSG_VIS_CTRL_FIND_MENU)
                .oPgFrm.Page1.oPag.oObj_1_55.Calculate(MSG_VIS_TOOLMENU)
                .oPgFrm.Page1.oPag.oObj_1_56.Calculate(MSG_VIS_GRAD_BACKGOUND)
                .oPgFrm.Page1.oPag.oObj_1_57.Calculate(MSG_THEME_VALUE)
                .oPgFrm.Page1.oPag.oObj_1_58.Calculate(MSG_VIS_MASK_VALUE)
                .oPgFrm.Page1.oPag.oObj_1_59.Calculate(MSG_THEME_XP_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_60.Calculate(MSG_THEME_XP_VALUE)
                .oPgFrm.Page1.oPag.oObj_1_61.Calculate(MSG_TAB_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_62.Calculate(MSG_TAB_VALUE)
                .oPgFrm.Page2.oPag.oObj_2_48.Calculate(MSG_ENABLED)
                .oPgFrm.Page2.oPag.oObj_2_49.Calculate(MSG_DISABLED)
                .oPgFrm.Page2.oPag.oObj_2_50.Calculate(MSG_LISTS_ZOOM)
                .oPgFrm.Page2.oPag.oObj_2_51.Calculate(MSG_DESKTOP)
                .oPgFrm.Page2.oPag.oObj_2_52.Calculate(MSG_SELECTED+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_53.Calculate(MSG_EDITABLE_LABEL+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_54.Calculate(MSG_BACKGROUND+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_55.Calculate(MSG_CHARACTER+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_56.Calculate(MSG_GREED_LINES+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_57.Calculate(MSG_LINE_SELECTED+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_58.Calculate(MSG_GREED_DATA)
                .oPgFrm.Page2.oPag.oObj_2_59.Calculate(MSG_BACKGROUND_DESKTOP+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_60.Calculate(MSG_SELECTED_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_61.Calculate(MSG_EDITABLE_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_62.Calculate(MSG_GREED_LINES_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_63.Calculate(MSG_BACKGROUND_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_64.Calculate(MSG_CHARACTER_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_65.Calculate(MSG_BACKGROUND_DESKTOP_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_66.Calculate(MSG_LINE_SELECTED_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_20.Calculate(MSG_FONT_LABELS+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_21.Calculate(MSG_FONT_FIELDS+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_22.Calculate(MSG_FONT_COMBOBOX+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_23.Calculate(MSG_FONT_BUTTONS+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_24.Calculate(MSG_FONT_ZOOM+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_25.Calculate(MSG_FONT_TAB+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_26.Calculate(MSG_DIMENSION)
                .oPgFrm.Page3.oPag.oObj_3_27.Calculate(MSG_FONT_LABELS_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_28.Calculate(MSG_FONT_FIELDS_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_29.Calculate(MSG_FONT_BUTTONS_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_30.Calculate(MSG_FONT_ZOOM_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_31.Calculate(MSG_FONT_TAB_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_32.Calculate(MSG_FONT_COMBOBOX_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_33.Calculate(MSG_DIMENSION_LABELS)
                .oPgFrm.Page3.oPag.oObj_3_34.Calculate(MSG_DIMENSION_FIELDS)
                .oPgFrm.Page3.oPag.oObj_3_35.Calculate(MSG_DIMENSION_BUTTONS)
                .oPgFrm.Page3.oPag.oObj_3_36.Calculate(MSG_DIMENSION_ZOOM)
                .oPgFrm.Page3.oPag.oObj_3_37.Calculate(MSG_DIMENSION_TAB)
                .oPgFrm.Page3.oPag.oObj_3_38.Calculate(MSG_DIMENSION_COMBOBOX)
                .oPgFrm.Page4.oPag.oObj_4_17.Calculate(MSG_DESKTOP_MENU+MSG_FS)
                .oPgFrm.Page4.oPag.oObj_4_18.Calculate(MSG_DESKTOP_MENU_STATUS)
                .oPgFrm.Page4.oPag.oObj_4_19.Calculate(MSG_NUMBER_BUTTON_VIS)
                .oPgFrm.Page4.oPag.oObj_4_20.Calculate(MSG_INITIAL_DIMENSION_MENU )
                .oPgFrm.Page4.oPag.oObj_4_21.Calculate(MSG_FONT_MENU_NAV)
                .oPgFrm.Page4.oPag.oObj_4_22.Calculate(MSG_DIMENSION+MSG_FS)
                .oPgFrm.Page4.oPag.oObj_4_23.Calculate(MSG_DIMENSION+MSG_FS)
                .oPgFrm.Page4.oPag.oObj_4_24.Calculate(MSG_FONT_WIN_MAN)
                .oPgFrm.Page4.oPag.oObj_4_25.Calculate(MSG_DESKTOP_MENU_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_26.Calculate(MSG_DESKTOP_MENU_STATUS_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_27.Calculate(MSG_NUMBER_BUTTON_VIS_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_28.Calculate(MSG_INITIAL_DIMENSION_MENU_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_29.Calculate(MSG_FONT_MENU_NAV_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_30.Calculate(MSG_FONT_WIN_MAN_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_31.Calculate(MSG_FONT_MENU_NAV_DIM_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_32.Calculate(MSG_FONT_WIN_MAN_DIM_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_33.Calculate(MSG_DESKTOP_MENU_VALUE)
                .oPgFrm.Page4.oPag.oObj_4_34.Calculate(MSG_DESKTOP_MENU_STATUS_VALUE)
                .w_CIOBLCOL = Nvl(CIOBLCOL,0)
                .oPgFrm.Page2.oPag.oObj_2_69.Calculate(.w_CIOBLCOL)
                .oPgFrm.Page2.oPag.oObj_2_72.Calculate(MSG_HIGHLIGHT_REQUIRED+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_73.Calculate(MSG_REQUIRED_TOOLTIPTEXT)
                .w_ciobl_on = Nvl(ciobl_on,Space(1))
                .w_CICOLZOM = Nvl(CICOLZOM,0)
                .oPgFrm.Page2.oPag.oObj_2_78.Calculate(.w_CICOLZOM)
                .oPgFrm.Page2.oPag.oObj_2_79.Calculate(MSG_UNDERLINE_SELECTED_ROW_ZOOM_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_80.Calculate(MSG_UNDERLINE_SELECTED_ROW_ZOOM)
                .oPgFrm.Page2.oPag.oObj_2_81.Calculate(MSG_SELECTED_ROW_COLOUR+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_82.Calculate(MSG_SELECTED_ROW_COLOUR_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_68.Calculate(MSG_VIS_WAITWND_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_69.Calculate(MSG_VIS_WAITWND)
                .w_cilblfit = Nvl(cilblfit,Space(1))
                .w_citxtfit = Nvl(citxtfit,Space(1))
                .w_cicbxfit = Nvl(cicbxfit,Space(1))
                .w_cibtnfit = Nvl(cibtnfit,Space(1))
                .w_cigrdfit = Nvl(cigrdfit,Space(1))
                .w_cipagfit = Nvl(cipagfit,Space(1))
                .w_cilblfbo = Nvl(cilblfbo,Space(1))
                .w_citxtfbo = Nvl(citxtfbo,Space(1))
                .w_cicbxfbo = Nvl(cicbxfbo,Space(1))
                .w_cibtnfbo = Nvl(cibtnfbo,Space(1))
                .w_cigrdfbo = Nvl(cigrdfbo,Space(1))
                .w_cipagfbo = Nvl(cipagfbo,Space(1))
                .w_CICTRGRD = Nvl(CICTRGRD,Space(1))
                .w_CIMRKGRD = Nvl(CIMRKGRD,Space(1))
                .w_CIAZOOML = Nvl(CIAZOOML,Space(1))
                .w_CIZOESPR = Nvl(CIZOESPR,Space(1))
                .w_CIHEHEZO = Nvl(CIHEHEZO,0)
                .w_CIHEROZO = Nvl(CIHEROZO,0)
                .oPgFrm.Page5.oPag.oObj_5_11.Calculate(MSG_ADVGRIDHEADER)
                .oPgFrm.Page5.oPag.oObj_5_12.Calculate(MSG_ADVGRIDHEADER_TOOLTIP)
                .oPgFrm.Page5.oPag.oObj_5_13.Calculate(MSG_VIS_POINT_REC_TOOLTIP)
                .oPgFrm.Page5.oPag.oObj_5_14.Calculate(MSG_VIS_POINT_REC)
                .oPgFrm.Page5.oPag.oObj_5_15.Calculate(MSG_HEADERHEIGHT+MSG_FS)
                .oPgFrm.Page5.oPag.oObj_5_16.Calculate(MSG_ROWHEIGHT+MSG_FS)
                .oPgFrm.Page5.oPag.oObj_5_17.Calculate(MSG_AUTOLOADRECZOOM)
                .oPgFrm.Page5.oPag.oObj_5_18.Calculate(MSG_AUTOLOADRECZOOM_TOOLTIP)
                cp_LoadRecExtFlds(This,'cpsetgui')
            Endwith
            This.SaveDependsOn()
            This.SetControlsValue()
            This.mHideControls()
            This.ChildrenChangeRow()
            This.NotifyEvent('Load')
        Else
            This.BlankRec()
        Endif
        * --- Area Manuale = Load Record End
        * --- Fine Area Manuale
    Endproc

    * --- Blank form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        If Not(This.bOnScreen)
            This.nDeferredFillRec=2
            Return
        Endif
        This.nDeferredFillRec=0
        This.bUpdated=.F.
        This.bLoaded=.F.
        With This
            .w_usrcode = 0
            .w_CIVTHEME = 0
            .w_CIREPBEH = Space(1)
            .w_CICPTBAR = Space(1)
            .w_CIDSKBAR = Space(1)
            .w_CISTABAR = Space(1)
            .w_CISHWBTN = Space(1)
            .w_CITBSIZE = 0
            .w_CIMDIFRM = Space(1)
            .w_CITABMEN = Space(1)
            .w_CIXPTHEM = 0
            .w_CIMENFIX = Space(1)
            .w_CISEARMN = Space(1)
            .w_CITOOLMN = Space(1)
            .w_CIBCKGRD = Space(1)
            .w_ciwaitwd = Space(1)
            .w_CISELECL = 0
            .w_CIEDTCOL = 0
            .w_CIGRIDCL = 0
            .w_CIDTLCLR = 0
            .w_CIDSBLBC = 0
            .w_CIDSBLFC = 0
            .w_CISCRCOL = 0
            .w_COLORCTSEL = Space(10)
            .w_CIEVIZOM = 0
            .w_COLORCTED = Space(10)
            .w_COLORFCTDSBL = Space(10)
            .w_COLORBCTDSBL = Space(10)
            .w_COLORGRD = Space(10)
            .w_COLORSCREEN = Space(10)
            .w_COLORROWDTL = Space(10)
            .w_CILBLFNM = Space(50)
            .w_CITXTFNM = Space(50)
            .w_CICBXFNM = Space(50)
            .w_CIBTNFNM = Space(50)
            .w_CIGRDFNM = Space(50)
            .w_CIPAGFNM = Space(50)
            .w_CILBLFSZ = 0
            .w_CITXTFSZ = 0
            .w_CICBXFSZ = 0
            .w_CIBTNFSZ = 0
            .w_CIGRDFSZ = 0
            .w_CIPAGFSZ = 0
            .w_CIDSKMEN = Space(1)
            .w_CINAVSTA = Space(1)
            .w_CINAVNUB = 0
            .w_CINAVDIM = 0
            .w_CIMNAFNM = Space(50)
            .w_CIMNAFSZ = 0
            .w_CIWMAFNM = Space(50)
            .w_CIWMAFSZ = 0
            .w_RET = Space(1)
            .w_CIOBLCOL = 0
            .w_COLORCTOB = Space(10)
            .w_ciobl_on = Space(1)
            .w_COLORZOM = Space(10)
            .w_CICOLZOM = 0
            .w_cilblfit = Space(1)
            .w_citxtfit = Space(1)
            .w_cicbxfit = Space(1)
            .w_cibtnfit = Space(1)
            .w_cigrdfit = Space(1)
            .w_cipagfit = Space(1)
            .w_cilblfbo = Space(1)
            .w_citxtfbo = Space(1)
            .w_cicbxfbo = Space(1)
            .w_cibtnfbo = Space(1)
            .w_cigrdfbo = Space(1)
            .w_cipagfbo = Space(1)
            .w_CICTRGRD = Space(1)
            .w_CIMRKGRD = Space(1)
            .w_CIAZOOML = Space(1)
            .w_CIZOESPR = Space(1)
            .w_CIHEHEZO = 0
            .w_CIHEROZO = 0
            If .cFunction<>"Filter"
                .DoRTCalc(1,1,.F.)
                .w_CIVTHEME = -1
                .w_CIREPBEH = '8'
                .w_CICPTBAR = 'S'
                .w_CIDSKBAR = 'S'
                .w_CISTABAR = ' '
                .w_CISHWBTN = ' '
                .w_CITBSIZE = 24
                .w_CIMDIFRM = 'S'
                .w_CITABMEN = 'T'
                .w_CIXPTHEM = 1
                .w_CIMENFIX = 'S'
                .w_CISEARMN = 'S'
                .w_CITOOLMN = 'S'
                .w_CIBCKGRD = 'S'
                .w_ciwaitwd = 'N'
                .w_CISELECL = Rgb(255,231,162)
                .w_CIEDTCOL = Rgb(246,246,246)
                .w_CIGRIDCL = Rgb(208, 215, 229)
                .w_CIDTLCLR = .w_CISELECL
                .w_CIDSBLBC = Rgb(233,238,238)
                .w_CIDSBLFC = Rgb(0,41,91)
                .w_CISCRCOL = Rgb(255,255,255)
                .DoRTCalc(24,24,.F.)
                .w_CIEVIZOM = 0
                .oPgFrm.Page2.oPag.oObj_2_19.Calculate(.w_CIEDTCOL)
                .oPgFrm.Page2.oPag.oObj_2_20.Calculate(.w_CISELECL)
                .oPgFrm.Page2.oPag.oObj_2_24.Calculate(.w_CIDSBLFC)
                .oPgFrm.Page2.oPag.oObj_2_25.Calculate(.w_CIDSBLBC)
                .oPgFrm.Page2.oPag.oObj_2_43.Calculate(.w_CISCRCOL)
                .oPgFrm.Page2.oPag.oObj_2_44.Calculate(.w_CIDTLCLR)
                .oPgFrm.Page2.oPag.oObj_2_45.Calculate(.w_CIGRIDCL)
                .DoRTCalc(26,31,.F.)
                .w_CILBLFNM = i_cProjectFontName
                .w_CITXTFNM = i_cProjectFontName
                .w_CICBXFNM = i_cProjectFontName
                .w_CIBTNFNM = i_cProjectFontName
                .w_CIGRDFNM = i_cProjectFontName
                .w_CIPAGFNM = i_cProjectFontName
                .w_CILBLFSZ = i_nProjectFontSize
                .w_CITXTFSZ = i_nProjectFontSize
                .w_CICBXFSZ = i_nProjectFontSize
                .w_CIBTNFSZ = i_nProjectFontSize
                .w_CIGRDFSZ = i_nProjectFontSize
                .w_CIPAGFSZ = i_nProjectFontSize
                .w_CIDSKMEN = 'S'
                .w_CINAVSTA = 'A'
                .w_CINAVNUB = 7
                .w_CINAVDIM = 179
                .w_CIMNAFNM = i_cProjectFontName
                .w_CIMNAFSZ = i_nProjectFontSize
                .w_CIWMAFNM = i_cProjectFontName
                .w_CIWMAFSZ = i_nProjectFontSize
                .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_THEME_APPLIC_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_THEME)
                .oPgFrm.Page1.oPag.oObj_1_32.Calculate(MSG_VIS_TOOL_BAR_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_33.Calculate(MSG_VIS_APP_BAR_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_34.Calculate(MSG_VIS_STATUS_BAR_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_35.Calculate(MSG_VIS_CONT_BUTTON_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_36.Calculate(MSG_DIM_IMM_TOOLBAR)
                .oPgFrm.Page1.oPag.oObj_1_37.Calculate(MSG_ICON_TOOLBAR)
                .oPgFrm.Page1.oPag.oObj_1_38.Calculate(MSG_VIS_MENUBAR_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_39.Calculate(MSG_VIS_CTRL_FIND_MENU_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_40.Calculate(MSG_VIS_TOOLMENU_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_41.Calculate(MSG_VIS_GRAD_BACKGOUND_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_42.Calculate(MSG_VIS_MODE)
                .oPgFrm.Page1.oPag.oObj_1_43.Calculate(MSG_TOOLBAR_STATUSBAR)
                .oPgFrm.Page1.oPag.oObj_1_44.Calculate(MSG_FORM_CONTROL)
                .oPgFrm.Page1.oPag.oObj_1_45.Calculate(MSG_TAB)
                .oPgFrm.Page1.oPag.oObj_1_46.Calculate(MSG_THEME_XP)
                .oPgFrm.Page1.oPag.oObj_1_47.Calculate(MSG_MENU)
                .oPgFrm.Page1.oPag.oObj_1_48.Calculate(MSG_VIS_MASK_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_49.Calculate(MSG_VIS_TOOL_BAR)
                .oPgFrm.Page1.oPag.oObj_1_50.Calculate(MSG_VIS_APP_BAR)
                .oPgFrm.Page1.oPag.oObj_1_51.Calculate(MSG_VIS_STATUS_BAR)
                .oPgFrm.Page1.oPag.oObj_1_52.Calculate(MSG_VIS_CONT_BUTTON)
                .oPgFrm.Page1.oPag.oObj_1_53.Calculate(MSG_VIS_MENUBAR)
                .oPgFrm.Page1.oPag.oObj_1_54.Calculate(MSG_VIS_CTRL_FIND_MENU)
                .oPgFrm.Page1.oPag.oObj_1_55.Calculate(MSG_VIS_TOOLMENU)
                .oPgFrm.Page1.oPag.oObj_1_56.Calculate(MSG_VIS_GRAD_BACKGOUND)
                .oPgFrm.Page1.oPag.oObj_1_57.Calculate(MSG_THEME_VALUE)
                .oPgFrm.Page1.oPag.oObj_1_58.Calculate(MSG_VIS_MASK_VALUE)
                .oPgFrm.Page1.oPag.oObj_1_59.Calculate(MSG_THEME_XP_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_60.Calculate(MSG_THEME_XP_VALUE)
                .oPgFrm.Page1.oPag.oObj_1_61.Calculate(MSG_TAB_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_62.Calculate(MSG_TAB_VALUE)
                .oPgFrm.Page2.oPag.oObj_2_48.Calculate(MSG_ENABLED)
                .oPgFrm.Page2.oPag.oObj_2_49.Calculate(MSG_DISABLED)
                .oPgFrm.Page2.oPag.oObj_2_50.Calculate(MSG_LISTS_ZOOM)
                .oPgFrm.Page2.oPag.oObj_2_51.Calculate(MSG_DESKTOP)
                .oPgFrm.Page2.oPag.oObj_2_52.Calculate(MSG_SELECTED+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_53.Calculate(MSG_EDITABLE_LABEL+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_54.Calculate(MSG_BACKGROUND+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_55.Calculate(MSG_CHARACTER+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_56.Calculate(MSG_GREED_LINES+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_57.Calculate(MSG_LINE_SELECTED+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_58.Calculate(MSG_GREED_DATA)
                .oPgFrm.Page2.oPag.oObj_2_59.Calculate(MSG_BACKGROUND_DESKTOP+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_60.Calculate(MSG_SELECTED_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_61.Calculate(MSG_EDITABLE_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_62.Calculate(MSG_GREED_LINES_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_63.Calculate(MSG_BACKGROUND_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_64.Calculate(MSG_CHARACTER_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_65.Calculate(MSG_BACKGROUND_DESKTOP_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_66.Calculate(MSG_LINE_SELECTED_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_20.Calculate(MSG_FONT_LABELS+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_21.Calculate(MSG_FONT_FIELDS+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_22.Calculate(MSG_FONT_COMBOBOX+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_23.Calculate(MSG_FONT_BUTTONS+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_24.Calculate(MSG_FONT_ZOOM+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_25.Calculate(MSG_FONT_TAB+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_26.Calculate(MSG_DIMENSION)
                .oPgFrm.Page3.oPag.oObj_3_27.Calculate(MSG_FONT_LABELS_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_28.Calculate(MSG_FONT_FIELDS_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_29.Calculate(MSG_FONT_BUTTONS_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_30.Calculate(MSG_FONT_ZOOM_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_31.Calculate(MSG_FONT_TAB_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_32.Calculate(MSG_FONT_COMBOBOX_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_33.Calculate(MSG_DIMENSION_LABELS)
                .oPgFrm.Page3.oPag.oObj_3_34.Calculate(MSG_DIMENSION_FIELDS)
                .oPgFrm.Page3.oPag.oObj_3_35.Calculate(MSG_DIMENSION_BUTTONS)
                .oPgFrm.Page3.oPag.oObj_3_36.Calculate(MSG_DIMENSION_ZOOM)
                .oPgFrm.Page3.oPag.oObj_3_37.Calculate(MSG_DIMENSION_TAB)
                .oPgFrm.Page3.oPag.oObj_3_38.Calculate(MSG_DIMENSION_COMBOBOX)
                .oPgFrm.Page4.oPag.oObj_4_17.Calculate(MSG_DESKTOP_MENU+MSG_FS)
                .oPgFrm.Page4.oPag.oObj_4_18.Calculate(MSG_DESKTOP_MENU_STATUS)
                .oPgFrm.Page4.oPag.oObj_4_19.Calculate(MSG_NUMBER_BUTTON_VIS)
                .oPgFrm.Page4.oPag.oObj_4_20.Calculate(MSG_INITIAL_DIMENSION_MENU )
                .oPgFrm.Page4.oPag.oObj_4_21.Calculate(MSG_FONT_MENU_NAV)
                .oPgFrm.Page4.oPag.oObj_4_22.Calculate(MSG_DIMENSION+MSG_FS)
                .oPgFrm.Page4.oPag.oObj_4_23.Calculate(MSG_DIMENSION+MSG_FS)
                .oPgFrm.Page4.oPag.oObj_4_24.Calculate(MSG_FONT_WIN_MAN)
                .oPgFrm.Page4.oPag.oObj_4_25.Calculate(MSG_DESKTOP_MENU_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_26.Calculate(MSG_DESKTOP_MENU_STATUS_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_27.Calculate(MSG_NUMBER_BUTTON_VIS_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_28.Calculate(MSG_INITIAL_DIMENSION_MENU_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_29.Calculate(MSG_FONT_MENU_NAV_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_30.Calculate(MSG_FONT_WIN_MAN_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_31.Calculate(MSG_FONT_MENU_NAV_DIM_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_32.Calculate(MSG_FONT_WIN_MAN_DIM_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_33.Calculate(MSG_DESKTOP_MENU_VALUE)
                .oPgFrm.Page4.oPag.oObj_4_34.Calculate(MSG_DESKTOP_MENU_STATUS_VALUE)
                .oPgFrm.Page2.oPag.oObj_2_69.Calculate(.w_CIOBLCOL)
                .oPgFrm.Page2.oPag.oObj_2_72.Calculate(MSG_HIGHLIGHT_REQUIRED+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_73.Calculate(MSG_REQUIRED_TOOLTIPTEXT)
                .DoRTCalc(52,54,.F.)
                .w_ciobl_on = 'N'
                .DoRTCalc(56,56,.F.)
                .w_CICOLZOM = Rgb(255,231,162)
                .oPgFrm.Page2.oPag.oObj_2_78.Calculate(.w_CICOLZOM)
                .oPgFrm.Page2.oPag.oObj_2_79.Calculate(MSG_UNDERLINE_SELECTED_ROW_ZOOM_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_80.Calculate(MSG_UNDERLINE_SELECTED_ROW_ZOOM)
                .oPgFrm.Page2.oPag.oObj_2_81.Calculate(MSG_SELECTED_ROW_COLOUR+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_82.Calculate(MSG_SELECTED_ROW_COLOUR_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_68.Calculate(MSG_VIS_WAITWND_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_69.Calculate(MSG_VIS_WAITWND)
                .w_cilblfit = 'N'
                .w_citxtfit = 'N'
                .w_cicbxfit = 'N'
                .w_cibtnfit = 'N'
                .w_cigrdfit = 'N'
                .w_cipagfit = 'N'
                .w_cilblfbo = 'N'
                .w_citxtfbo = 'N'
                .w_cicbxfbo = 'N'
                .w_cibtnfbo = 'N'
                .w_cigrdfbo = 'N'
                .w_cipagfbo = 'N'
                .w_CICTRGRD = 'N'
                .w_CIMRKGRD = 'N'
                .w_CIAZOOML = 'S'
                .w_CIZOESPR = 'S'
                .w_CIHEHEZO = 19
                .w_CIHEROZO = 19
                .oPgFrm.Page5.oPag.oObj_5_11.Calculate(MSG_ADVGRIDHEADER)
                .oPgFrm.Page5.oPag.oObj_5_12.Calculate(MSG_ADVGRIDHEADER_TOOLTIP)
                .oPgFrm.Page5.oPag.oObj_5_13.Calculate(MSG_VIS_POINT_REC_TOOLTIP)
                .oPgFrm.Page5.oPag.oObj_5_14.Calculate(MSG_VIS_POINT_REC)
                .oPgFrm.Page5.oPag.oObj_5_15.Calculate(MSG_HEADERHEIGHT+MSG_FS)
                .oPgFrm.Page5.oPag.oObj_5_16.Calculate(MSG_ROWHEIGHT+MSG_FS)
                .oPgFrm.Page5.oPag.oObj_5_17.Calculate(MSG_AUTOLOADRECZOOM)
                .oPgFrm.Page5.oPag.oObj_5_18.Calculate(MSG_AUTOLOADRECZOOM_TOOLTIP)
            Endif
        Endwith
        cp_BlankRecExtFlds(This,'cpsetgui')
        This.SaveDependsOn()
        This.SetControlsValue()
        This.mHideControls()
        This.ChildrenChangeRow()
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = setting operation
    *     Allowed parameters  : Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        Local i_bVal
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
        * --- Disable List page when <> from Query
        With This.oPgFrm
            .Page1.oPag.oCIVTHEME_1_2.Enabled = i_bVal
            .Page1.oPag.oCIREPBEH_1_3.Enabled = i_bVal
            .Page1.oPag.oCICPTBAR_1_4.Enabled = i_bVal
            .Page1.oPag.oCIDSKBAR_1_5.Enabled = i_bVal
            .Page1.oPag.oCISTABAR_1_6.Enabled = i_bVal
            .Page1.oPag.oCISHWBTN_1_7.Enabled = i_bVal
            .Page1.oPag.oCITBSIZE_1_8.Enabled = i_bVal
            .Page1.oPag.oCIMDIFRM_1_9.Enabled = i_bVal
            .Page1.oPag.oCITABMEN_1_10.Enabled = i_bVal
            .Page1.oPag.oCIXPTHEM_1_11.Enabled = i_bVal
            .Page1.oPag.oCIMENFIX_1_12.Enabled = i_bVal
            .Page1.oPag.oCISEARMN_1_13.Enabled = i_bVal
            .Page1.oPag.oCITOOLMN_1_14.Enabled = i_bVal
            .Page1.oPag.oCIBCKGRD_1_15.Enabled = i_bVal
            .Page1.oPag.ociwaitwd_1_16.Enabled = i_bVal
            .Page2.oPag.oCIEVIZOM_2_16.Enabled = i_bVal
            .Page3.oPag.oCILBLFNM_3_1.Enabled = i_bVal
            .Page3.oPag.oCITXTFNM_3_2.Enabled = i_bVal
            .Page3.oPag.oCICBXFNM_3_3.Enabled = i_bVal
            .Page3.oPag.oCIBTNFNM_3_4.Enabled = i_bVal
            .Page3.oPag.oCIGRDFNM_3_5.Enabled = i_bVal
            .Page3.oPag.oCIPAGFNM_3_6.Enabled = i_bVal
            .Page3.oPag.oCILBLFSZ_3_13.Enabled = i_bVal
            .Page3.oPag.oCITXTFSZ_3_15.Enabled = i_bVal
            .Page3.oPag.oCICBXFSZ_3_16.Enabled = i_bVal
            .Page3.oPag.oCIBTNFSZ_3_17.Enabled = i_bVal
            .Page3.oPag.oCIGRDFSZ_3_18.Enabled = i_bVal
            .Page3.oPag.oCIPAGFSZ_3_19.Enabled = i_bVal
            .Page4.oPag.oCIDSKMEN_4_1.Enabled = i_bVal
            .Page4.oPag.oCINAVSTA_4_2.Enabled = i_bVal
            .Page4.oPag.oCINAVNUB_4_3.Enabled = i_bVal
            .Page4.oPag.oCINAVDIM_4_4.Enabled = i_bVal
            .Page4.oPag.oCIMNAFNM_4_5.Enabled = i_bVal
            .Page4.oPag.oCIMNAFSZ_4_6.Enabled = i_bVal
            .Page4.oPag.oCIWMAFNM_4_7.Enabled = i_bVal
            .Page4.oPag.oCIWMAFSZ_4_8.Enabled = i_bVal
            .Page2.oPag.ociobl_on_2_74.Enabled = i_bVal
            .Page3.oPag.ocilblfit_3_39.Enabled = i_bVal
            .Page3.oPag.ocitxtfit_3_40.Enabled = i_bVal
            .Page3.oPag.ocicbxfit_3_41.Enabled = i_bVal
            .Page3.oPag.ocibtnfit_3_42.Enabled = i_bVal
            .Page3.oPag.ocigrdfit_3_43.Enabled = i_bVal
            .Page3.oPag.ocipagfit_3_44.Enabled = i_bVal
            .Page3.oPag.ocilblfbo_3_45.Enabled = i_bVal
            .Page3.oPag.ocitxtfbo_3_46.Enabled = i_bVal
            .Page3.oPag.ocicbxfbo_3_47.Enabled = i_bVal
            .Page3.oPag.ocibtnfbo_3_48.Enabled = i_bVal
            .Page3.oPag.ocigrdfbo_3_49.Enabled = i_bVal
            .Page3.oPag.ocipagfbo_3_50.Enabled = i_bVal
            .Page5.oPag.oCICTRGRD_5_1.Enabled = i_bVal
            .Page5.oPag.oCIMRKGRD_5_2.Enabled = i_bVal
            .Page5.oPag.oCIAZOOML_5_3.Enabled = i_bVal
            .Page5.oPag.oCIZOESPR_5_4.Enabled = i_bVal
            .Page5.oPag.oCIHEHEZO_5_5.Enabled = i_bVal
            .Page5.oPag.oCIHEROZO_5_6.Enabled = i_bVal
            .Page2.oPag.oBtn_2_9.Enabled = i_bVal
            .Page2.oPag.oBtn_2_10.Enabled = i_bVal
            .Page2.oPag.oBtn_2_11.Enabled = i_bVal
            .Page2.oPag.oBtn_2_12.Enabled = i_bVal
            .Page2.oPag.oBtn_2_13.Enabled = i_bVal
            .Page2.oPag.oBtn_2_14.Enabled = i_bVal
            .Page2.oPag.oBtn_2_15.Enabled = i_bVal
            .Page2.oPag.oBtn_2_17.Enabled = i_bVal
            .Page2.oPag.oBtn_2_68.Enabled = i_bVal
            .Page2.oPag.oObj_2_19.Enabled = i_bVal
            .Page2.oPag.oObj_2_20.Enabled = i_bVal
            .Page2.oPag.oObj_2_24.Enabled = i_bVal
            .Page2.oPag.oObj_2_25.Enabled = i_bVal
            .Page2.oPag.oObj_2_43.Enabled = i_bVal
            .Page2.oPag.oObj_2_44.Enabled = i_bVal
            .Page2.oPag.oObj_2_45.Enabled = i_bVal
            .Page1.oPag.oObj_1_30.Enabled = i_bVal
            .Page1.oPag.oObj_1_31.Enabled = i_bVal
            .Page1.oPag.oObj_1_32.Enabled = i_bVal
            .Page1.oPag.oObj_1_33.Enabled = i_bVal
            .Page1.oPag.oObj_1_34.Enabled = i_bVal
            .Page1.oPag.oObj_1_35.Enabled = i_bVal
            .Page1.oPag.oObj_1_36.Enabled = i_bVal
            .Page1.oPag.oObj_1_37.Enabled = i_bVal
            .Page1.oPag.oObj_1_38.Enabled = i_bVal
            .Page1.oPag.oObj_1_39.Enabled = i_bVal
            .Page1.oPag.oObj_1_40.Enabled = i_bVal
            .Page1.oPag.oObj_1_41.Enabled = i_bVal
            .Page1.oPag.oObj_1_42.Enabled = i_bVal
            .Page1.oPag.oObj_1_43.Enabled = i_bVal
            .Page1.oPag.oObj_1_44.Enabled = i_bVal
            .Page1.oPag.oObj_1_45.Enabled = i_bVal
            .Page1.oPag.oObj_1_46.Enabled = i_bVal
            .Page1.oPag.oObj_1_47.Enabled = i_bVal
            .Page1.oPag.oObj_1_48.Enabled = i_bVal
            .Page1.oPag.oObj_1_49.Enabled = i_bVal
            .Page1.oPag.oObj_1_50.Enabled = i_bVal
            .Page1.oPag.oObj_1_51.Enabled = i_bVal
            .Page1.oPag.oObj_1_52.Enabled = i_bVal
            .Page1.oPag.oObj_1_53.Enabled = i_bVal
            .Page1.oPag.oObj_1_54.Enabled = i_bVal
            .Page1.oPag.oObj_1_55.Enabled = i_bVal
            .Page1.oPag.oObj_1_56.Enabled = i_bVal
            .Page1.oPag.oObj_1_57.Enabled = i_bVal
            .Page1.oPag.oObj_1_58.Enabled = i_bVal
            .Page1.oPag.oObj_1_59.Enabled = i_bVal
            .Page1.oPag.oObj_1_60.Enabled = i_bVal
            .Page1.oPag.oObj_1_61.Enabled = i_bVal
            .Page1.oPag.oObj_1_62.Enabled = i_bVal
            .Page2.oPag.oObj_2_48.Enabled = i_bVal
            .Page2.oPag.oObj_2_49.Enabled = i_bVal
            .Page2.oPag.oObj_2_50.Enabled = i_bVal
            .Page2.oPag.oObj_2_51.Enabled = i_bVal
            .Page2.oPag.oObj_2_52.Enabled = i_bVal
            .Page2.oPag.oObj_2_53.Enabled = i_bVal
            .Page2.oPag.oObj_2_54.Enabled = i_bVal
            .Page2.oPag.oObj_2_55.Enabled = i_bVal
            .Page2.oPag.oObj_2_56.Enabled = i_bVal
            .Page2.oPag.oObj_2_57.Enabled = i_bVal
            .Page2.oPag.oObj_2_58.Enabled = i_bVal
            .Page2.oPag.oObj_2_59.Enabled = i_bVal
            .Page2.oPag.oObj_2_60.Enabled = i_bVal
            .Page2.oPag.oObj_2_61.Enabled = i_bVal
            .Page2.oPag.oObj_2_62.Enabled = i_bVal
            .Page2.oPag.oObj_2_63.Enabled = i_bVal
            .Page2.oPag.oObj_2_64.Enabled = i_bVal
            .Page2.oPag.oObj_2_65.Enabled = i_bVal
            .Page2.oPag.oObj_2_66.Enabled = i_bVal
            .Page3.oPag.oObj_3_20.Enabled = i_bVal
            .Page3.oPag.oObj_3_21.Enabled = i_bVal
            .Page3.oPag.oObj_3_22.Enabled = i_bVal
            .Page3.oPag.oObj_3_23.Enabled = i_bVal
            .Page3.oPag.oObj_3_24.Enabled = i_bVal
            .Page3.oPag.oObj_3_25.Enabled = i_bVal
            .Page3.oPag.oObj_3_26.Enabled = i_bVal
            .Page3.oPag.oObj_3_27.Enabled = i_bVal
            .Page3.oPag.oObj_3_28.Enabled = i_bVal
            .Page3.oPag.oObj_3_29.Enabled = i_bVal
            .Page3.oPag.oObj_3_30.Enabled = i_bVal
            .Page3.oPag.oObj_3_31.Enabled = i_bVal
            .Page3.oPag.oObj_3_32.Enabled = i_bVal
            .Page3.oPag.oObj_3_33.Enabled = i_bVal
            .Page3.oPag.oObj_3_34.Enabled = i_bVal
            .Page3.oPag.oObj_3_35.Enabled = i_bVal
            .Page3.oPag.oObj_3_36.Enabled = i_bVal
            .Page3.oPag.oObj_3_37.Enabled = i_bVal
            .Page3.oPag.oObj_3_38.Enabled = i_bVal
            .Page4.oPag.oObj_4_17.Enabled = i_bVal
            .Page4.oPag.oObj_4_18.Enabled = i_bVal
            .Page4.oPag.oObj_4_19.Enabled = i_bVal
            .Page4.oPag.oObj_4_20.Enabled = i_bVal
            .Page4.oPag.oObj_4_21.Enabled = i_bVal
            .Page4.oPag.oObj_4_22.Enabled = i_bVal
            .Page4.oPag.oObj_4_23.Enabled = i_bVal
            .Page4.oPag.oObj_4_24.Enabled = i_bVal
            .Page4.oPag.oObj_4_25.Enabled = i_bVal
            .Page4.oPag.oObj_4_26.Enabled = i_bVal
            .Page4.oPag.oObj_4_27.Enabled = i_bVal
            .Page4.oPag.oObj_4_28.Enabled = i_bVal
            .Page4.oPag.oObj_4_29.Enabled = i_bVal
            .Page4.oPag.oObj_4_30.Enabled = i_bVal
            .Page4.oPag.oObj_4_31.Enabled = i_bVal
            .Page4.oPag.oObj_4_32.Enabled = i_bVal
            .Page4.oPag.oObj_4_33.Enabled = i_bVal
            .Page4.oPag.oObj_4_34.Enabled = i_bVal
            .Page2.oPag.oObj_2_69.Enabled = i_bVal
            .Page2.oPag.oObj_2_72.Enabled = i_bVal
            .Page2.oPag.oObj_2_73.Enabled = i_bVal
            .Page2.oPag.oObj_2_78.Enabled = i_bVal
            .Page2.oPag.oObj_2_79.Enabled = i_bVal
            .Page2.oPag.oObj_2_80.Enabled = i_bVal
            .Page2.oPag.oObj_2_81.Enabled = i_bVal
            .Page2.oPag.oObj_2_82.Enabled = i_bVal
            .Page1.oPag.oObj_1_68.Enabled = i_bVal
            .Page1.oPag.oObj_1_69.Enabled = i_bVal
            .Page5.oPag.oObj_5_11.Enabled = i_bVal
            .Page5.oPag.oObj_5_12.Enabled = i_bVal
            .Page5.oPag.oObj_5_13.Enabled = i_bVal
            .Page5.oPag.oObj_5_14.Enabled = i_bVal
            .Page5.oPag.oObj_5_15.Enabled = i_bVal
            .Page5.oPag.oObj_5_16.Enabled = i_bVal
            .Page5.oPag.oObj_5_17.Enabled = i_bVal
            .Page5.oPag.oObj_5_18.Enabled = i_bVal
        Endwith
        cp_SetEnabledExtFlds(This,'cpsetgui',i_cOp)
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt,i_nConn
        i_nConn = i_TableProp[this.cpsetgui_IDX,3]
        i_cFlt = This.cQueryFilter
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_usrcode,"usrcode",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIVTHEME,"CIVTHEME",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIREPBEH,"CIREPBEH",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CICPTBAR,"CICPTBAR",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIDSKBAR,"CIDSKBAR",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CISTABAR,"CISTABAR",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CISHWBTN,"CISHWBTN",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CITBSIZE,"CITBSIZE",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIMDIFRM,"CIMDIFRM",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CITABMEN,"CITABMEN",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIXPTHEM,"CIXPTHEM",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIMENFIX,"CIMENFIX",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CISEARMN,"CISEARMN",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CITOOLMN,"CITOOLMN",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIBCKGRD,"CIBCKGRD",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_ciwaitwd,"ciwaitwd",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CISELECL,"CISELECL",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIEDTCOL,"CIEDTCOL",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIGRIDCL,"CIGRIDCL",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIDTLCLR,"CIDTLCLR",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIDSBLBC,"CIDSBLBC",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIDSBLFC,"CIDSBLFC",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CISCRCOL,"CISCRCOL",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIEVIZOM,"CIEVIZOM",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CILBLFNM,"CILBLFNM",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CITXTFNM,"CITXTFNM",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CICBXFNM,"CICBXFNM",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIBTNFNM,"CIBTNFNM",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIGRDFNM,"CIGRDFNM",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIPAGFNM,"CIPAGFNM",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CILBLFSZ,"CILBLFSZ",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CITXTFSZ,"CITXTFSZ",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CICBXFSZ,"CICBXFSZ",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIBTNFSZ,"CIBTNFSZ",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIGRDFSZ,"CIGRDFSZ",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIPAGFSZ,"CIPAGFSZ",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIDSKMEN,"CIDSKMEN",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CINAVSTA,"CINAVSTA",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CINAVNUB,"CINAVNUB",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CINAVDIM,"CINAVDIM",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIMNAFNM,"CIMNAFNM",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIMNAFSZ,"CIMNAFSZ",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIWMAFNM,"CIWMAFNM",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIWMAFSZ,"CIWMAFSZ",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIOBLCOL,"CIOBLCOL",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_ciobl_on,"ciobl_on",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CICOLZOM,"CICOLZOM",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_cilblfit,"cilblfit",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_citxtfit,"citxtfit",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_cicbxfit,"cicbxfit",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_cibtnfit,"cibtnfit",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_cigrdfit,"cigrdfit",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_cipagfit,"cipagfit",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_cilblfbo,"cilblfbo",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_citxtfbo,"citxtfbo",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_cicbxfbo,"cicbxfbo",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_cibtnfbo,"cibtnfbo",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_cigrdfbo,"cigrdfbo",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_cipagfbo,"cipagfbo",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CICTRGRD,"CICTRGRD",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIMRKGRD,"CIMRKGRD",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIAZOOML,"CIAZOOML",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIZOESPR,"CIZOESPR",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIHEHEZO,"CIHEHEZO",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_CIHEROZO,"CIHEROZO",i_nConn)
        * --- Area Manuale = Build Filter
        * --- Fine Area Manuale
        Return (i_cFlt)
    Endfunc

    * --- Insert of new Record
    Function mInsert()
        Local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
        * --- Area Manuale = Insert Init
        * --- Fine Area Manuale
        If This.nDeferredFillRec<>0
            Return
        Endif
        If This.bUpdated .Or. This.IsAChildUpdated()
            i_nConn = i_TableProp[this.cpsetgui_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.cpsetgui_IDX,2])
            i_ccchkf=''
            i_ccchkv=''
            This.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,This.cpsetgui_IDX,i_nConn)
            * --- Area Manuale = Autonum Assigned
            * --- Fine Area Manuale
            *
            * insert into cpsetgui
            *
            This.NotifyEvent('Insert start')
            This.mUpdateTrs()
            If i_nConn<>0
                i_extfld=cp_InsertFldODBCExtFlds(This,'cpsetgui')
                i_extval=cp_InsertValODBCExtFlds(This,'cpsetgui')
                i_nnn="INSERT INTO "+i_cTable+" "+;
                    "(usrcode,CIVTHEME,CIREPBEH,CICPTBAR,CIDSKBAR"+;
                    ",CISTABAR,CISHWBTN,CITBSIZE,CIMDIFRM,CITABMEN"+;
                    ",CIXPTHEM,CIMENFIX,CISEARMN,CITOOLMN,CIBCKGRD"+;
                    ",ciwaitwd,CISELECL,CIEDTCOL,CIGRIDCL,CIDTLCLR"+;
                    ",CIDSBLBC,CIDSBLFC,CISCRCOL,CIEVIZOM,CILBLFNM"+;
                    ",CITXTFNM,CICBXFNM,CIBTNFNM,CIGRDFNM,CIPAGFNM"+;
                    ",CILBLFSZ,CITXTFSZ,CICBXFSZ,CIBTNFSZ,CIGRDFSZ"+;
                    ",CIPAGFSZ,CIDSKMEN,CINAVSTA,CINAVNUB,CINAVDIM"+;
                    ",CIMNAFNM,CIMNAFSZ,CIWMAFNM,CIWMAFSZ,CIOBLCOL"+;
                    ",ciobl_on,CICOLZOM,cilblfit,citxtfit,cicbxfit"+;
                    ",cibtnfit,cigrdfit,cipagfit,cilblfbo,citxtfbo"+;
                    ",cicbxfbo,cibtnfbo,cigrdfbo,cipagfbo,CICTRGRD"+;
                    ",CIMRKGRD,CIAZOOML,CIZOESPR,CIHEHEZO,CIHEROZO "+i_extfld+i_ccchkf+") VALUES ("+;
                    cp_ToStrODBC(This.w_usrcode)+;
                    ","+cp_ToStrODBC(This.w_CIVTHEME)+;
                    ","+cp_ToStrODBC(This.w_CIREPBEH)+;
                    ","+cp_ToStrODBC(This.w_CICPTBAR)+;
                    ","+cp_ToStrODBC(This.w_CIDSKBAR)+;
                    ","+cp_ToStrODBC(This.w_CISTABAR)+;
                    ","+cp_ToStrODBC(This.w_CISHWBTN)+;
                    ","+cp_ToStrODBC(This.w_CITBSIZE)+;
                    ","+cp_ToStrODBC(This.w_CIMDIFRM)+;
                    ","+cp_ToStrODBC(This.w_CITABMEN)+;
                    ","+cp_ToStrODBC(This.w_CIXPTHEM)+;
                    ","+cp_ToStrODBC(This.w_CIMENFIX)+;
                    ","+cp_ToStrODBC(This.w_CISEARMN)+;
                    ","+cp_ToStrODBC(This.w_CITOOLMN)+;
                    ","+cp_ToStrODBC(This.w_CIBCKGRD)+;
                    ","+cp_ToStrODBC(This.w_ciwaitwd)+;
                    ","+cp_ToStrODBC(This.w_CISELECL)+;
                    ","+cp_ToStrODBC(This.w_CIEDTCOL)+;
                    ","+cp_ToStrODBC(This.w_CIGRIDCL)+;
                    ","+cp_ToStrODBC(This.w_CIDTLCLR)+;
                    ","+cp_ToStrODBC(This.w_CIDSBLBC)+;
                    ","+cp_ToStrODBC(This.w_CIDSBLFC)+;
                    ","+cp_ToStrODBC(This.w_CISCRCOL)+;
                    ","+cp_ToStrODBC(This.w_CIEVIZOM)+;
                    ","+cp_ToStrODBC(This.w_CILBLFNM)+;
                    ","+cp_ToStrODBC(This.w_CITXTFNM)+;
                    ","+cp_ToStrODBC(This.w_CICBXFNM)+;
                    ","+cp_ToStrODBC(This.w_CIBTNFNM)+;
                    ","+cp_ToStrODBC(This.w_CIGRDFNM)+;
                    ","+cp_ToStrODBC(This.w_CIPAGFNM)+;
                    ","+cp_ToStrODBC(This.w_CILBLFSZ)+;
                    ","+cp_ToStrODBC(This.w_CITXTFSZ)+;
                    ","+cp_ToStrODBC(This.w_CICBXFSZ)+;
                    ","+cp_ToStrODBC(This.w_CIBTNFSZ)+;
                    ","+cp_ToStrODBC(This.w_CIGRDFSZ)+;
                    ","+cp_ToStrODBC(This.w_CIPAGFSZ)+;
                    ","+cp_ToStrODBC(This.w_CIDSKMEN)+;
                    ","+cp_ToStrODBC(This.w_CINAVSTA)+;
                    ","+cp_ToStrODBC(This.w_CINAVNUB)+;
                    ","+cp_ToStrODBC(This.w_CINAVDIM)+;
                    ","+cp_ToStrODBC(This.w_CIMNAFNM)+;
                    ","+cp_ToStrODBC(This.w_CIMNAFSZ)+;
                    ","+cp_ToStrODBC(This.w_CIWMAFNM)+;
                    ","+cp_ToStrODBC(This.w_CIWMAFSZ)+;
                    ","+cp_ToStrODBC(This.w_CIOBLCOL)+;
                    ","+cp_ToStrODBC(This.w_ciobl_on)+;
                    ","+cp_ToStrODBC(This.w_CICOLZOM)+;
                    ","+cp_ToStrODBC(This.w_cilblfit)+;
                    ","+cp_ToStrODBC(This.w_citxtfit)+;
                    ","+cp_ToStrODBC(This.w_cicbxfit)+;
                    ","+cp_ToStrODBC(This.w_cibtnfit)+;
                    ","+cp_ToStrODBC(This.w_cigrdfit)+;
                    ","+cp_ToStrODBC(This.w_cipagfit)+;
                    ","+cp_ToStrODBC(This.w_cilblfbo)+;
                    ","+cp_ToStrODBC(This.w_citxtfbo)+;
                    ","+cp_ToStrODBC(This.w_cicbxfbo)+;
                    ","+cp_ToStrODBC(This.w_cibtnfbo)+;
                    ","+cp_ToStrODBC(This.w_cigrdfbo)+;
                    ","+cp_ToStrODBC(This.w_cipagfbo)+;
                    ","+cp_ToStrODBC(This.w_CICTRGRD)+;
                    ","+cp_ToStrODBC(This.w_CIMRKGRD)+;
                    ","+cp_ToStrODBC(This.w_CIAZOOML)+;
                    ","+cp_ToStrODBC(This.w_CIZOESPR)+;
                    ","+cp_ToStrODBC(This.w_CIHEHEZO)+;
                    ","+cp_ToStrODBC(This.w_CIHEROZO)+;
                    i_extval+i_ccchkv+")"
                =cp_TrsSQL(i_nConn,i_nnn)
            Else
                i_extfld=cp_InsertFldVFPExtFlds(This,'cpsetgui')
                i_extval=cp_InsertValVFPExtFlds(This,'cpsetgui')
                cp_CheckDeletedKey(i_cTable,0,'usrcode',This.w_usrcode)
                Insert Into (i_cTable);
                    (usrcode,CIVTHEME,CIREPBEH,CICPTBAR,CIDSKBAR,CISTABAR,CISHWBTN,CITBSIZE,CIMDIFRM,CITABMEN,CIXPTHEM,CIMENFIX,CISEARMN,CITOOLMN,CIBCKGRD,ciwaitwd,CISELECL,CIEDTCOL,CIGRIDCL,CIDTLCLR,CIDSBLBC,CIDSBLFC,CISCRCOL,CIEVIZOM,CILBLFNM,CITXTFNM,CICBXFNM,CIBTNFNM,CIGRDFNM,CIPAGFNM,CILBLFSZ,CITXTFSZ,CICBXFSZ,CIBTNFSZ,CIGRDFSZ,CIPAGFSZ,CIDSKMEN,CINAVSTA,CINAVNUB,CINAVDIM,CIMNAFNM,CIMNAFSZ,CIWMAFNM,CIWMAFSZ,CIOBLCOL,ciobl_on,CICOLZOM,cilblfit,citxtfit,cicbxfit,cibtnfit,cigrdfit,cipagfit,cilblfbo,citxtfbo,cicbxfbo,cibtnfbo,cigrdfbo,cipagfbo,CICTRGRD,CIMRKGRD,CIAZOOML,CIZOESPR,CIHEHEZO,CIHEROZO  &i_extfld. &i_ccchkf.) Values (;
                    this.w_usrcode;
                    ,This.w_CIVTHEME;
                    ,This.w_CIREPBEH;
                    ,This.w_CICPTBAR;
                    ,This.w_CIDSKBAR;
                    ,This.w_CISTABAR;
                    ,This.w_CISHWBTN;
                    ,This.w_CITBSIZE;
                    ,This.w_CIMDIFRM;
                    ,This.w_CITABMEN;
                    ,This.w_CIXPTHEM;
                    ,This.w_CIMENFIX;
                    ,This.w_CISEARMN;
                    ,This.w_CITOOLMN;
                    ,This.w_CIBCKGRD;
                    ,This.w_ciwaitwd;
                    ,This.w_CISELECL;
                    ,This.w_CIEDTCOL;
                    ,This.w_CIGRIDCL;
                    ,This.w_CIDTLCLR;
                    ,This.w_CIDSBLBC;
                    ,This.w_CIDSBLFC;
                    ,This.w_CISCRCOL;
                    ,This.w_CIEVIZOM;
                    ,This.w_CILBLFNM;
                    ,This.w_CITXTFNM;
                    ,This.w_CICBXFNM;
                    ,This.w_CIBTNFNM;
                    ,This.w_CIGRDFNM;
                    ,This.w_CIPAGFNM;
                    ,This.w_CILBLFSZ;
                    ,This.w_CITXTFSZ;
                    ,This.w_CICBXFSZ;
                    ,This.w_CIBTNFSZ;
                    ,This.w_CIGRDFSZ;
                    ,This.w_CIPAGFSZ;
                    ,This.w_CIDSKMEN;
                    ,This.w_CINAVSTA;
                    ,This.w_CINAVNUB;
                    ,This.w_CINAVDIM;
                    ,This.w_CIMNAFNM;
                    ,This.w_CIMNAFSZ;
                    ,This.w_CIWMAFNM;
                    ,This.w_CIWMAFSZ;
                    ,This.w_CIOBLCOL;
                    ,This.w_ciobl_on;
                    ,This.w_CICOLZOM;
                    ,This.w_cilblfit;
                    ,This.w_citxtfit;
                    ,This.w_cicbxfit;
                    ,This.w_cibtnfit;
                    ,This.w_cigrdfit;
                    ,This.w_cipagfit;
                    ,This.w_cilblfbo;
                    ,This.w_citxtfbo;
                    ,This.w_cicbxfbo;
                    ,This.w_cibtnfbo;
                    ,This.w_cigrdfbo;
                    ,This.w_cipagfbo;
                    ,This.w_CICTRGRD;
                    ,This.w_CIMRKGRD;
                    ,This.w_CIAZOOML;
                    ,This.w_CIZOESPR;
                    ,This.w_CIHEHEZO;
                    ,This.w_CIHEROZO;
                    &i_extval. &i_ccchkv.)
            Endif
            This.NotifyEvent('Insert end')
        Endif
        * --- Area Manuale = Insert End
        * --- Fine Area Manuale
        Return(Not(bTrsErr))

        * --- Update Database
    Function mReplace(i_bEditing)
        Local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        If This.nDeferredFillRec<>0
            Return
        Endif
        If Not(This.bLoaded)
            This.mInsert()
            i_bEditing=.F.
        Else
            i_bEditing=.T.
        Endif
        If This.bUpdated And i_bEditing
            This.mRestoreTrs()
            This.mUpdateTrs()
        Endif
        If This.bUpdated And i_bEditing
            i_nConn = i_TableProp[this.cpsetgui_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.cpsetgui_IDX,2])
            i_ccchkf=''
            i_ccchkw=''
            This.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,This.cpsetgui_IDX,i_nConn)
            *
            * update cpsetgui
            *
            This.NotifyEvent('Update start')
            If i_nConn<>0
                i_cWhere = This.cKeyWhereODBC
                i_extfld=cp_ReplaceODBCExtFlds(This,'cpsetgui')
                i_nnn="UPDATE "+i_cTable+" SET"+;
                    " CIVTHEME="+cp_ToStrODBC(This.w_CIVTHEME)+;
                    ",CIREPBEH="+cp_ToStrODBC(This.w_CIREPBEH)+;
                    ",CICPTBAR="+cp_ToStrODBC(This.w_CICPTBAR)+;
                    ",CIDSKBAR="+cp_ToStrODBC(This.w_CIDSKBAR)+;
                    ",CISTABAR="+cp_ToStrODBC(This.w_CISTABAR)+;
                    ",CISHWBTN="+cp_ToStrODBC(This.w_CISHWBTN)+;
                    ",CITBSIZE="+cp_ToStrODBC(This.w_CITBSIZE)+;
                    ",CIMDIFRM="+cp_ToStrODBC(This.w_CIMDIFRM)+;
                    ",CITABMEN="+cp_ToStrODBC(This.w_CITABMEN)+;
                    ",CIXPTHEM="+cp_ToStrODBC(This.w_CIXPTHEM)+;
                    ",CIMENFIX="+cp_ToStrODBC(This.w_CIMENFIX)+;
                    ",CISEARMN="+cp_ToStrODBC(This.w_CISEARMN)+;
                    ",CITOOLMN="+cp_ToStrODBC(This.w_CITOOLMN)+;
                    ",CIBCKGRD="+cp_ToStrODBC(This.w_CIBCKGRD)+;
                    ",ciwaitwd="+cp_ToStrODBC(This.w_ciwaitwd)+;
                    ",CISELECL="+cp_ToStrODBC(This.w_CISELECL)+;
                    ",CIEDTCOL="+cp_ToStrODBC(This.w_CIEDTCOL)+;
                    ",CIGRIDCL="+cp_ToStrODBC(This.w_CIGRIDCL)+;
                    ",CIDTLCLR="+cp_ToStrODBC(This.w_CIDTLCLR)+;
                    ",CIDSBLBC="+cp_ToStrODBC(This.w_CIDSBLBC)+;
                    ",CIDSBLFC="+cp_ToStrODBC(This.w_CIDSBLFC)+;
                    ",CISCRCOL="+cp_ToStrODBC(This.w_CISCRCOL)+;
                    ",CIEVIZOM="+cp_ToStrODBC(This.w_CIEVIZOM)+;
                    ",CILBLFNM="+cp_ToStrODBC(This.w_CILBLFNM)+;
                    ",CITXTFNM="+cp_ToStrODBC(This.w_CITXTFNM)+;
                    ",CICBXFNM="+cp_ToStrODBC(This.w_CICBXFNM)+;
                    ",CIBTNFNM="+cp_ToStrODBC(This.w_CIBTNFNM)+;
                    ",CIGRDFNM="+cp_ToStrODBC(This.w_CIGRDFNM)+;
                    ",CIPAGFNM="+cp_ToStrODBC(This.w_CIPAGFNM)+;
                    ",CILBLFSZ="+cp_ToStrODBC(This.w_CILBLFSZ)+;
                    ",CITXTFSZ="+cp_ToStrODBC(This.w_CITXTFSZ)+;
                    ",CICBXFSZ="+cp_ToStrODBC(This.w_CICBXFSZ)+;
                    ",CIBTNFSZ="+cp_ToStrODBC(This.w_CIBTNFSZ)+;
                    ",CIGRDFSZ="+cp_ToStrODBC(This.w_CIGRDFSZ)+;
                    ",CIPAGFSZ="+cp_ToStrODBC(This.w_CIPAGFSZ)+;
                    ",CIDSKMEN="+cp_ToStrODBC(This.w_CIDSKMEN)+;
                    ",CINAVSTA="+cp_ToStrODBC(This.w_CINAVSTA)+;
                    ",CINAVNUB="+cp_ToStrODBC(This.w_CINAVNUB)+;
                    ",CINAVDIM="+cp_ToStrODBC(This.w_CINAVDIM)+;
                    ",CIMNAFNM="+cp_ToStrODBC(This.w_CIMNAFNM)+;
                    ",CIMNAFSZ="+cp_ToStrODBC(This.w_CIMNAFSZ)+;
                    ",CIWMAFNM="+cp_ToStrODBC(This.w_CIWMAFNM)+;
                    ",CIWMAFSZ="+cp_ToStrODBC(This.w_CIWMAFSZ)+;
                    ",CIOBLCOL="+cp_ToStrODBC(This.w_CIOBLCOL)+;
                    ",ciobl_on="+cp_ToStrODBC(This.w_ciobl_on)+;
                    ",CICOLZOM="+cp_ToStrODBC(This.w_CICOLZOM)+;
                    ",cilblfit="+cp_ToStrODBC(This.w_cilblfit)+;
                    ",citxtfit="+cp_ToStrODBC(This.w_citxtfit)+;
                    ",cicbxfit="+cp_ToStrODBC(This.w_cicbxfit)+;
                    ",cibtnfit="+cp_ToStrODBC(This.w_cibtnfit)+;
                    ",cigrdfit="+cp_ToStrODBC(This.w_cigrdfit)+;
                    ",cipagfit="+cp_ToStrODBC(This.w_cipagfit)+;
                    ",cilblfbo="+cp_ToStrODBC(This.w_cilblfbo)+;
                    ",citxtfbo="+cp_ToStrODBC(This.w_citxtfbo)+;
                    ",cicbxfbo="+cp_ToStrODBC(This.w_cicbxfbo)+;
                    ",cibtnfbo="+cp_ToStrODBC(This.w_cibtnfbo)+;
                    ",cigrdfbo="+cp_ToStrODBC(This.w_cigrdfbo)+;
                    ",cipagfbo="+cp_ToStrODBC(This.w_cipagfbo)+;
                    ",CICTRGRD="+cp_ToStrODBC(This.w_CICTRGRD)+;
                    ",CIMRKGRD="+cp_ToStrODBC(This.w_CIMRKGRD)+;
                    ",CIAZOOML="+cp_ToStrODBC(This.w_CIAZOOML)+;
                    ",CIZOESPR="+cp_ToStrODBC(This.w_CIZOESPR)+;
                    ",CIHEHEZO="+cp_ToStrODBC(This.w_CIHEHEZO)+;
                    ",CIHEROZO="+cp_ToStrODBC(This.w_CIHEROZO)+;
                    i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            Else
                i_extfld=cp_ReplaceVFPExtFlds(This,'cpsetgui')
                i_cWhere = cp_PKFox(i_cTable  ,'usrcode',This.w_usrcode  )
                Update (i_cTable) Set;
                    CIVTHEME=This.w_CIVTHEME;
                    ,CIREPBEH=This.w_CIREPBEH;
                    ,CICPTBAR=This.w_CICPTBAR;
                    ,CIDSKBAR=This.w_CIDSKBAR;
                    ,CISTABAR=This.w_CISTABAR;
                    ,CISHWBTN=This.w_CISHWBTN;
                    ,CITBSIZE=This.w_CITBSIZE;
                    ,CIMDIFRM=This.w_CIMDIFRM;
                    ,CITABMEN=This.w_CITABMEN;
                    ,CIXPTHEM=This.w_CIXPTHEM;
                    ,CIMENFIX=This.w_CIMENFIX;
                    ,CISEARMN=This.w_CISEARMN;
                    ,CITOOLMN=This.w_CITOOLMN;
                    ,CIBCKGRD=This.w_CIBCKGRD;
                    ,ciwaitwd=This.w_ciwaitwd;
                    ,CISELECL=This.w_CISELECL;
                    ,CIEDTCOL=This.w_CIEDTCOL;
                    ,CIGRIDCL=This.w_CIGRIDCL;
                    ,CIDTLCLR=This.w_CIDTLCLR;
                    ,CIDSBLBC=This.w_CIDSBLBC;
                    ,CIDSBLFC=This.w_CIDSBLFC;
                    ,CISCRCOL=This.w_CISCRCOL;
                    ,CIEVIZOM=This.w_CIEVIZOM;
                    ,CILBLFNM=This.w_CILBLFNM;
                    ,CITXTFNM=This.w_CITXTFNM;
                    ,CICBXFNM=This.w_CICBXFNM;
                    ,CIBTNFNM=This.w_CIBTNFNM;
                    ,CIGRDFNM=This.w_CIGRDFNM;
                    ,CIPAGFNM=This.w_CIPAGFNM;
                    ,CILBLFSZ=This.w_CILBLFSZ;
                    ,CITXTFSZ=This.w_CITXTFSZ;
                    ,CICBXFSZ=This.w_CICBXFSZ;
                    ,CIBTNFSZ=This.w_CIBTNFSZ;
                    ,CIGRDFSZ=This.w_CIGRDFSZ;
                    ,CIPAGFSZ=This.w_CIPAGFSZ;
                    ,CIDSKMEN=This.w_CIDSKMEN;
                    ,CINAVSTA=This.w_CINAVSTA;
                    ,CINAVNUB=This.w_CINAVNUB;
                    ,CINAVDIM=This.w_CINAVDIM;
                    ,CIMNAFNM=This.w_CIMNAFNM;
                    ,CIMNAFSZ=This.w_CIMNAFSZ;
                    ,CIWMAFNM=This.w_CIWMAFNM;
                    ,CIWMAFSZ=This.w_CIWMAFSZ;
                    ,CIOBLCOL=This.w_CIOBLCOL;
                    ,ciobl_on=This.w_ciobl_on;
                    ,CICOLZOM=This.w_CICOLZOM;
                    ,cilblfit=This.w_cilblfit;
                    ,citxtfit=This.w_citxtfit;
                    ,cicbxfit=This.w_cicbxfit;
                    ,cibtnfit=This.w_cibtnfit;
                    ,cigrdfit=This.w_cigrdfit;
                    ,cipagfit=This.w_cipagfit;
                    ,cilblfbo=This.w_cilblfbo;
                    ,citxtfbo=This.w_citxtfbo;
                    ,cicbxfbo=This.w_cicbxfbo;
                    ,cibtnfbo=This.w_cibtnfbo;
                    ,cigrdfbo=This.w_cigrdfbo;
                    ,cipagfbo=This.w_cipagfbo;
                    ,CICTRGRD=This.w_CICTRGRD;
                    ,CIMRKGRD=This.w_CIMRKGRD;
                    ,CIAZOOML=This.w_CIAZOOML;
                    ,CIZOESPR=This.w_CIZOESPR;
                    ,CIHEHEZO=This.w_CIHEHEZO;
                    ,CIHEROZO=This.w_CIHEROZO;
                    &i_extfld. &i_ccchkf. Where &i_cWhere. &i_ccchkw.
                i_nModRow=_Tally
            Endif
            =cp_CheckMultiuser(i_nModRow)
            This.NotifyEvent('Update end')
        Endif
        If Not(bTrsErr)
        Endif
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(bTrsErr)

        * --- Delete Records
    Function mDelete()
        Local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
        * --- Area Manuale = Delete Init
        * --- Fine Area Manuale
        Local i_bLoadNow
        If This.nDeferredFillRec<>0  && record is not loaded
            i_bLoadNow=.T.
            This.bOnScreen=.T.         && force loadrec in future
            This.LoadRec()             && record loaded, timestamp correct
        Endif
        If This.nDeferredFillRec=0 And This.bLoaded=.F.
            If i_bLoadNow
                This.bOnScreen=.F.
            Endif
            Return
        Endif
        *
        If Not(bTrsErr)
            i_nConn = i_TableProp[this.cpsetgui_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.cpsetgui_IDX,2])
            i_ccchkw=''
            This.SetCCCHKVarDelete(@i_ccchkw,This.cpsetgui_IDX,i_nConn)
            *
            * delete cpsetgui
            *
            This.NotifyEvent('Delete start')
            If i_nConn<>0
                i_cWhere = This.cKeyWhereODBC
                i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                    " WHERE "+&i_cWhere+i_ccchkw)
            Else
                * i_cWhere = this.cKeyWhere
                i_cWhere = cp_PKFox(i_cTable  ,'usrcode',This.w_usrcode  )
                Delete From (i_cTable) Where &i_cWhere. &i_ccchkw.
                i_nModRow=_Tally
            Endif
            =cp_CheckMultiuser(i_nModRow)
            If Not(bTrsErr)
                This.mRestoreTrs()
            Endif
            This.NotifyEvent('Delete end')
        Endif
        *
        If i_bLoadNow
            This.bOnScreen=.F.
        Endif
        *
        This.mDeleteWarnings()
        * --- Area Manuale = Delete End
        * --- Fine Area Manuale
        Return

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        Local i_cTable,i_nConn
        i_nConn = i_TableProp[this.cpsetgui_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.cpsetgui_IDX,2])
        If i_bUpd
            With This
                .oPgFrm.Page2.oPag.oObj_2_19.Calculate(.w_CIEDTCOL)
                .oPgFrm.Page2.oPag.oObj_2_20.Calculate(.w_CISELECL)
                .oPgFrm.Page2.oPag.oObj_2_24.Calculate(.w_CIDSBLFC)
                .oPgFrm.Page2.oPag.oObj_2_25.Calculate(.w_CIDSBLBC)
                .oPgFrm.Page2.oPag.oObj_2_43.Calculate(.w_CISCRCOL)
                .oPgFrm.Page2.oPag.oObj_2_44.Calculate(.w_CIDTLCLR)
                .oPgFrm.Page2.oPag.oObj_2_45.Calculate(.w_CIGRIDCL)
                .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_THEME_APPLIC_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_THEME)
                .oPgFrm.Page1.oPag.oObj_1_32.Calculate(MSG_VIS_TOOL_BAR_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_33.Calculate(MSG_VIS_APP_BAR_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_34.Calculate(MSG_VIS_STATUS_BAR_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_35.Calculate(MSG_VIS_CONT_BUTTON_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_36.Calculate(MSG_DIM_IMM_TOOLBAR)
                .oPgFrm.Page1.oPag.oObj_1_37.Calculate(MSG_ICON_TOOLBAR)
                .oPgFrm.Page1.oPag.oObj_1_38.Calculate(MSG_VIS_MENUBAR_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_39.Calculate(MSG_VIS_CTRL_FIND_MENU_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_40.Calculate(MSG_VIS_TOOLMENU_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_41.Calculate(MSG_VIS_GRAD_BACKGOUND_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_42.Calculate(MSG_VIS_MODE)
                .oPgFrm.Page1.oPag.oObj_1_43.Calculate(MSG_TOOLBAR_STATUSBAR)
                .oPgFrm.Page1.oPag.oObj_1_44.Calculate(MSG_FORM_CONTROL)
                .oPgFrm.Page1.oPag.oObj_1_45.Calculate(MSG_TAB)
                .oPgFrm.Page1.oPag.oObj_1_46.Calculate(MSG_THEME_XP)
                .oPgFrm.Page1.oPag.oObj_1_47.Calculate(MSG_MENU)
                .oPgFrm.Page1.oPag.oObj_1_48.Calculate(MSG_VIS_MASK_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_49.Calculate(MSG_VIS_TOOL_BAR)
                .oPgFrm.Page1.oPag.oObj_1_50.Calculate(MSG_VIS_APP_BAR)
                .oPgFrm.Page1.oPag.oObj_1_51.Calculate(MSG_VIS_STATUS_BAR)
                .oPgFrm.Page1.oPag.oObj_1_52.Calculate(MSG_VIS_CONT_BUTTON)
                .oPgFrm.Page1.oPag.oObj_1_53.Calculate(MSG_VIS_MENUBAR)
                .oPgFrm.Page1.oPag.oObj_1_54.Calculate(MSG_VIS_CTRL_FIND_MENU)
                .oPgFrm.Page1.oPag.oObj_1_55.Calculate(MSG_VIS_TOOLMENU)
                .oPgFrm.Page1.oPag.oObj_1_56.Calculate(MSG_VIS_GRAD_BACKGOUND)
                .oPgFrm.Page1.oPag.oObj_1_57.Calculate(MSG_THEME_VALUE)
                .oPgFrm.Page1.oPag.oObj_1_58.Calculate(MSG_VIS_MASK_VALUE)
                .oPgFrm.Page1.oPag.oObj_1_59.Calculate(MSG_THEME_XP_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_60.Calculate(MSG_THEME_XP_VALUE)
                .oPgFrm.Page1.oPag.oObj_1_61.Calculate(MSG_TAB_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_62.Calculate(MSG_TAB_VALUE)
                .oPgFrm.Page2.oPag.oObj_2_48.Calculate(MSG_ENABLED)
                .oPgFrm.Page2.oPag.oObj_2_49.Calculate(MSG_DISABLED)
                .oPgFrm.Page2.oPag.oObj_2_50.Calculate(MSG_LISTS_ZOOM)
                .oPgFrm.Page2.oPag.oObj_2_51.Calculate(MSG_DESKTOP)
                .oPgFrm.Page2.oPag.oObj_2_52.Calculate(MSG_SELECTED+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_53.Calculate(MSG_EDITABLE_LABEL+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_54.Calculate(MSG_BACKGROUND+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_55.Calculate(MSG_CHARACTER+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_56.Calculate(MSG_GREED_LINES+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_57.Calculate(MSG_LINE_SELECTED+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_58.Calculate(MSG_GREED_DATA)
                .oPgFrm.Page2.oPag.oObj_2_59.Calculate(MSG_BACKGROUND_DESKTOP+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_60.Calculate(MSG_SELECTED_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_61.Calculate(MSG_EDITABLE_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_62.Calculate(MSG_GREED_LINES_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_63.Calculate(MSG_BACKGROUND_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_64.Calculate(MSG_CHARACTER_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_65.Calculate(MSG_BACKGROUND_DESKTOP_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_66.Calculate(MSG_LINE_SELECTED_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_20.Calculate(MSG_FONT_LABELS+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_21.Calculate(MSG_FONT_FIELDS+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_22.Calculate(MSG_FONT_COMBOBOX+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_23.Calculate(MSG_FONT_BUTTONS+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_24.Calculate(MSG_FONT_ZOOM+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_25.Calculate(MSG_FONT_TAB+MSG_FS)
                .oPgFrm.Page3.oPag.oObj_3_26.Calculate(MSG_DIMENSION)
                .oPgFrm.Page3.oPag.oObj_3_27.Calculate(MSG_FONT_LABELS_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_28.Calculate(MSG_FONT_FIELDS_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_29.Calculate(MSG_FONT_BUTTONS_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_30.Calculate(MSG_FONT_ZOOM_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_31.Calculate(MSG_FONT_TAB_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_32.Calculate(MSG_FONT_COMBOBOX_TOOLTIP)
                .oPgFrm.Page3.oPag.oObj_3_33.Calculate(MSG_DIMENSION_LABELS)
                .oPgFrm.Page3.oPag.oObj_3_34.Calculate(MSG_DIMENSION_FIELDS)
                .oPgFrm.Page3.oPag.oObj_3_35.Calculate(MSG_DIMENSION_BUTTONS)
                .oPgFrm.Page3.oPag.oObj_3_36.Calculate(MSG_DIMENSION_ZOOM)
                .oPgFrm.Page3.oPag.oObj_3_37.Calculate(MSG_DIMENSION_TAB)
                .oPgFrm.Page3.oPag.oObj_3_38.Calculate(MSG_DIMENSION_COMBOBOX)
                .oPgFrm.Page4.oPag.oObj_4_17.Calculate(MSG_DESKTOP_MENU+MSG_FS)
                .oPgFrm.Page4.oPag.oObj_4_18.Calculate(MSG_DESKTOP_MENU_STATUS)
                .oPgFrm.Page4.oPag.oObj_4_19.Calculate(MSG_NUMBER_BUTTON_VIS)
                .oPgFrm.Page4.oPag.oObj_4_20.Calculate(MSG_INITIAL_DIMENSION_MENU )
                .oPgFrm.Page4.oPag.oObj_4_21.Calculate(MSG_FONT_MENU_NAV)
                .oPgFrm.Page4.oPag.oObj_4_22.Calculate(MSG_DIMENSION+MSG_FS)
                .oPgFrm.Page4.oPag.oObj_4_23.Calculate(MSG_DIMENSION+MSG_FS)
                .oPgFrm.Page4.oPag.oObj_4_24.Calculate(MSG_FONT_WIN_MAN)
                .oPgFrm.Page4.oPag.oObj_4_25.Calculate(MSG_DESKTOP_MENU_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_26.Calculate(MSG_DESKTOP_MENU_STATUS_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_27.Calculate(MSG_NUMBER_BUTTON_VIS_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_28.Calculate(MSG_INITIAL_DIMENSION_MENU_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_29.Calculate(MSG_FONT_MENU_NAV_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_30.Calculate(MSG_FONT_WIN_MAN_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_31.Calculate(MSG_FONT_MENU_NAV_DIM_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_32.Calculate(MSG_FONT_WIN_MAN_DIM_TOOLTIP)
                .oPgFrm.Page4.oPag.oObj_4_33.Calculate(MSG_DESKTOP_MENU_VALUE)
                .oPgFrm.Page4.oPag.oObj_4_34.Calculate(MSG_DESKTOP_MENU_STATUS_VALUE)
                .oPgFrm.Page2.oPag.oObj_2_69.Calculate(.w_CIOBLCOL)
                .oPgFrm.Page2.oPag.oObj_2_72.Calculate(MSG_HIGHLIGHT_REQUIRED+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_73.Calculate(MSG_REQUIRED_TOOLTIPTEXT)
                .oPgFrm.Page2.oPag.oObj_2_78.Calculate(.w_CICOLZOM)
                .oPgFrm.Page2.oPag.oObj_2_79.Calculate(MSG_UNDERLINE_SELECTED_ROW_ZOOM_TOOLTIP)
                .oPgFrm.Page2.oPag.oObj_2_80.Calculate(MSG_UNDERLINE_SELECTED_ROW_ZOOM)
                .oPgFrm.Page2.oPag.oObj_2_81.Calculate(MSG_SELECTED_ROW_COLOUR+MSG_FS)
                .oPgFrm.Page2.oPag.oObj_2_82.Calculate(MSG_SELECTED_ROW_COLOUR_TOOLTIP)
                If .o_CIVTHEME<>.w_CIVTHEME.Or. .o_CITABMEN<>.w_CITABMEN
                    .Calculate_DBYVFFNWVV()
                Endif
                .oPgFrm.Page1.oPag.oObj_1_68.Calculate(MSG_VIS_WAITWND_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_69.Calculate(MSG_VIS_WAITWND)
                .oPgFrm.Page5.oPag.oObj_5_11.Calculate(MSG_ADVGRIDHEADER)
                .oPgFrm.Page5.oPag.oObj_5_12.Calculate(MSG_ADVGRIDHEADER_TOOLTIP)
                .oPgFrm.Page5.oPag.oObj_5_13.Calculate(MSG_VIS_POINT_REC_TOOLTIP)
                .oPgFrm.Page5.oPag.oObj_5_14.Calculate(MSG_VIS_POINT_REC)
                .oPgFrm.Page5.oPag.oObj_5_15.Calculate(MSG_HEADERHEIGHT+MSG_FS)
                .oPgFrm.Page5.oPag.oObj_5_16.Calculate(MSG_ROWHEIGHT+MSG_FS)
                .oPgFrm.Page5.oPag.oObj_5_17.Calculate(MSG_AUTOLOADRECZOOM)
                .oPgFrm.Page5.oPag.oObj_5_18.Calculate(MSG_AUTOLOADRECZOOM_TOOLTIP)
                If .o_CICTRGRD<>.w_CICTRGRD
                    .Calculate_SXRQWFXDKC()
                Endif
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
            Endwith
            This.DoRTCalc(1,75,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
            .oPgFrm.Page2.oPag.oObj_2_19.Calculate(.w_CIEDTCOL)
            .oPgFrm.Page2.oPag.oObj_2_20.Calculate(.w_CISELECL)
            .oPgFrm.Page2.oPag.oObj_2_24.Calculate(.w_CIDSBLFC)
            .oPgFrm.Page2.oPag.oObj_2_25.Calculate(.w_CIDSBLBC)
            .oPgFrm.Page2.oPag.oObj_2_43.Calculate(.w_CISCRCOL)
            .oPgFrm.Page2.oPag.oObj_2_44.Calculate(.w_CIDTLCLR)
            .oPgFrm.Page2.oPag.oObj_2_45.Calculate(.w_CIGRIDCL)
            .oPgFrm.Page1.oPag.oObj_1_30.Calculate(MSG_THEME_APPLIC_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_31.Calculate(MSG_THEME)
            .oPgFrm.Page1.oPag.oObj_1_32.Calculate(MSG_VIS_TOOL_BAR_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_33.Calculate(MSG_VIS_APP_BAR_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_34.Calculate(MSG_VIS_STATUS_BAR_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_35.Calculate(MSG_VIS_CONT_BUTTON_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_36.Calculate(MSG_DIM_IMM_TOOLBAR)
            .oPgFrm.Page1.oPag.oObj_1_37.Calculate(MSG_ICON_TOOLBAR)
            .oPgFrm.Page1.oPag.oObj_1_38.Calculate(MSG_VIS_MENUBAR_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_39.Calculate(MSG_VIS_CTRL_FIND_MENU_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_40.Calculate(MSG_VIS_TOOLMENU_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_41.Calculate(MSG_VIS_GRAD_BACKGOUND_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_42.Calculate(MSG_VIS_MODE)
            .oPgFrm.Page1.oPag.oObj_1_43.Calculate(MSG_TOOLBAR_STATUSBAR)
            .oPgFrm.Page1.oPag.oObj_1_44.Calculate(MSG_FORM_CONTROL)
            .oPgFrm.Page1.oPag.oObj_1_45.Calculate(MSG_TAB)
            .oPgFrm.Page1.oPag.oObj_1_46.Calculate(MSG_THEME_XP)
            .oPgFrm.Page1.oPag.oObj_1_47.Calculate(MSG_MENU)
            .oPgFrm.Page1.oPag.oObj_1_48.Calculate(MSG_VIS_MASK_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_49.Calculate(MSG_VIS_TOOL_BAR)
            .oPgFrm.Page1.oPag.oObj_1_50.Calculate(MSG_VIS_APP_BAR)
            .oPgFrm.Page1.oPag.oObj_1_51.Calculate(MSG_VIS_STATUS_BAR)
            .oPgFrm.Page1.oPag.oObj_1_52.Calculate(MSG_VIS_CONT_BUTTON)
            .oPgFrm.Page1.oPag.oObj_1_53.Calculate(MSG_VIS_MENUBAR)
            .oPgFrm.Page1.oPag.oObj_1_54.Calculate(MSG_VIS_CTRL_FIND_MENU)
            .oPgFrm.Page1.oPag.oObj_1_55.Calculate(MSG_VIS_TOOLMENU)
            .oPgFrm.Page1.oPag.oObj_1_56.Calculate(MSG_VIS_GRAD_BACKGOUND)
            .oPgFrm.Page1.oPag.oObj_1_57.Calculate(MSG_THEME_VALUE)
            .oPgFrm.Page1.oPag.oObj_1_58.Calculate(MSG_VIS_MASK_VALUE)
            .oPgFrm.Page1.oPag.oObj_1_59.Calculate(MSG_THEME_XP_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_60.Calculate(MSG_THEME_XP_VALUE)
            .oPgFrm.Page1.oPag.oObj_1_61.Calculate(MSG_TAB_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_62.Calculate(MSG_TAB_VALUE)
            .oPgFrm.Page2.oPag.oObj_2_48.Calculate(MSG_ENABLED)
            .oPgFrm.Page2.oPag.oObj_2_49.Calculate(MSG_DISABLED)
            .oPgFrm.Page2.oPag.oObj_2_50.Calculate(MSG_LISTS_ZOOM)
            .oPgFrm.Page2.oPag.oObj_2_51.Calculate(MSG_DESKTOP)
            .oPgFrm.Page2.oPag.oObj_2_52.Calculate(MSG_SELECTED+MSG_FS)
            .oPgFrm.Page2.oPag.oObj_2_53.Calculate(MSG_EDITABLE_LABEL+MSG_FS)
            .oPgFrm.Page2.oPag.oObj_2_54.Calculate(MSG_BACKGROUND+MSG_FS)
            .oPgFrm.Page2.oPag.oObj_2_55.Calculate(MSG_CHARACTER+MSG_FS)
            .oPgFrm.Page2.oPag.oObj_2_56.Calculate(MSG_GREED_LINES+MSG_FS)
            .oPgFrm.Page2.oPag.oObj_2_57.Calculate(MSG_LINE_SELECTED+MSG_FS)
            .oPgFrm.Page2.oPag.oObj_2_58.Calculate(MSG_GREED_DATA)
            .oPgFrm.Page2.oPag.oObj_2_59.Calculate(MSG_BACKGROUND_DESKTOP+MSG_FS)
            .oPgFrm.Page2.oPag.oObj_2_60.Calculate(MSG_SELECTED_TOOLTIP)
            .oPgFrm.Page2.oPag.oObj_2_61.Calculate(MSG_EDITABLE_TOOLTIP)
            .oPgFrm.Page2.oPag.oObj_2_62.Calculate(MSG_GREED_LINES_TOOLTIP)
            .oPgFrm.Page2.oPag.oObj_2_63.Calculate(MSG_BACKGROUND_TOOLTIP)
            .oPgFrm.Page2.oPag.oObj_2_64.Calculate(MSG_CHARACTER_TOOLTIP)
            .oPgFrm.Page2.oPag.oObj_2_65.Calculate(MSG_BACKGROUND_DESKTOP_TOOLTIP)
            .oPgFrm.Page2.oPag.oObj_2_66.Calculate(MSG_LINE_SELECTED_TOOLTIP)
            .oPgFrm.Page3.oPag.oObj_3_20.Calculate(MSG_FONT_LABELS+MSG_FS)
            .oPgFrm.Page3.oPag.oObj_3_21.Calculate(MSG_FONT_FIELDS+MSG_FS)
            .oPgFrm.Page3.oPag.oObj_3_22.Calculate(MSG_FONT_COMBOBOX+MSG_FS)
            .oPgFrm.Page3.oPag.oObj_3_23.Calculate(MSG_FONT_BUTTONS+MSG_FS)
            .oPgFrm.Page3.oPag.oObj_3_24.Calculate(MSG_FONT_ZOOM+MSG_FS)
            .oPgFrm.Page3.oPag.oObj_3_25.Calculate(MSG_FONT_TAB+MSG_FS)
            .oPgFrm.Page3.oPag.oObj_3_26.Calculate(MSG_DIMENSION)
            .oPgFrm.Page3.oPag.oObj_3_27.Calculate(MSG_FONT_LABELS_TOOLTIP)
            .oPgFrm.Page3.oPag.oObj_3_28.Calculate(MSG_FONT_FIELDS_TOOLTIP)
            .oPgFrm.Page3.oPag.oObj_3_29.Calculate(MSG_FONT_BUTTONS_TOOLTIP)
            .oPgFrm.Page3.oPag.oObj_3_30.Calculate(MSG_FONT_ZOOM_TOOLTIP)
            .oPgFrm.Page3.oPag.oObj_3_31.Calculate(MSG_FONT_TAB_TOOLTIP)
            .oPgFrm.Page3.oPag.oObj_3_32.Calculate(MSG_FONT_COMBOBOX_TOOLTIP)
            .oPgFrm.Page3.oPag.oObj_3_33.Calculate(MSG_DIMENSION_LABELS)
            .oPgFrm.Page3.oPag.oObj_3_34.Calculate(MSG_DIMENSION_FIELDS)
            .oPgFrm.Page3.oPag.oObj_3_35.Calculate(MSG_DIMENSION_BUTTONS)
            .oPgFrm.Page3.oPag.oObj_3_36.Calculate(MSG_DIMENSION_ZOOM)
            .oPgFrm.Page3.oPag.oObj_3_37.Calculate(MSG_DIMENSION_TAB)
            .oPgFrm.Page3.oPag.oObj_3_38.Calculate(MSG_DIMENSION_COMBOBOX)
            .oPgFrm.Page4.oPag.oObj_4_17.Calculate(MSG_DESKTOP_MENU+MSG_FS)
            .oPgFrm.Page4.oPag.oObj_4_18.Calculate(MSG_DESKTOP_MENU_STATUS)
            .oPgFrm.Page4.oPag.oObj_4_19.Calculate(MSG_NUMBER_BUTTON_VIS)
            .oPgFrm.Page4.oPag.oObj_4_20.Calculate(MSG_INITIAL_DIMENSION_MENU )
            .oPgFrm.Page4.oPag.oObj_4_21.Calculate(MSG_FONT_MENU_NAV)
            .oPgFrm.Page4.oPag.oObj_4_22.Calculate(MSG_DIMENSION+MSG_FS)
            .oPgFrm.Page4.oPag.oObj_4_23.Calculate(MSG_DIMENSION+MSG_FS)
            .oPgFrm.Page4.oPag.oObj_4_24.Calculate(MSG_FONT_WIN_MAN)
            .oPgFrm.Page4.oPag.oObj_4_25.Calculate(MSG_DESKTOP_MENU_TOOLTIP)
            .oPgFrm.Page4.oPag.oObj_4_26.Calculate(MSG_DESKTOP_MENU_STATUS_TOOLTIP)
            .oPgFrm.Page4.oPag.oObj_4_27.Calculate(MSG_NUMBER_BUTTON_VIS_TOOLTIP)
            .oPgFrm.Page4.oPag.oObj_4_28.Calculate(MSG_INITIAL_DIMENSION_MENU_TOOLTIP)
            .oPgFrm.Page4.oPag.oObj_4_29.Calculate(MSG_FONT_MENU_NAV_TOOLTIP)
            .oPgFrm.Page4.oPag.oObj_4_30.Calculate(MSG_FONT_WIN_MAN_TOOLTIP)
            .oPgFrm.Page4.oPag.oObj_4_31.Calculate(MSG_FONT_MENU_NAV_DIM_TOOLTIP)
            .oPgFrm.Page4.oPag.oObj_4_32.Calculate(MSG_FONT_WIN_MAN_DIM_TOOLTIP)
            .oPgFrm.Page4.oPag.oObj_4_33.Calculate(MSG_DESKTOP_MENU_VALUE)
            .oPgFrm.Page4.oPag.oObj_4_34.Calculate(MSG_DESKTOP_MENU_STATUS_VALUE)
            .oPgFrm.Page2.oPag.oObj_2_69.Calculate(.w_CIOBLCOL)
            .oPgFrm.Page2.oPag.oObj_2_72.Calculate(MSG_HIGHLIGHT_REQUIRED+MSG_FS)
            .oPgFrm.Page2.oPag.oObj_2_73.Calculate(MSG_REQUIRED_TOOLTIPTEXT)
            .oPgFrm.Page2.oPag.oObj_2_78.Calculate(.w_CICOLZOM)
            .oPgFrm.Page2.oPag.oObj_2_79.Calculate(MSG_UNDERLINE_SELECTED_ROW_ZOOM_TOOLTIP)
            .oPgFrm.Page2.oPag.oObj_2_80.Calculate(MSG_UNDERLINE_SELECTED_ROW_ZOOM)
            .oPgFrm.Page2.oPag.oObj_2_81.Calculate(MSG_SELECTED_ROW_COLOUR+MSG_FS)
            .oPgFrm.Page2.oPag.oObj_2_82.Calculate(MSG_SELECTED_ROW_COLOUR_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_68.Calculate(MSG_VIS_WAITWND_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_69.Calculate(MSG_VIS_WAITWND)
            .oPgFrm.Page5.oPag.oObj_5_11.Calculate(MSG_ADVGRIDHEADER)
            .oPgFrm.Page5.oPag.oObj_5_12.Calculate(MSG_ADVGRIDHEADER_TOOLTIP)
            .oPgFrm.Page5.oPag.oObj_5_13.Calculate(MSG_VIS_POINT_REC_TOOLTIP)
            .oPgFrm.Page5.oPag.oObj_5_14.Calculate(MSG_VIS_POINT_REC)
            .oPgFrm.Page5.oPag.oObj_5_15.Calculate(MSG_HEADERHEIGHT+MSG_FS)
            .oPgFrm.Page5.oPag.oObj_5_16.Calculate(MSG_ROWHEIGHT+MSG_FS)
            .oPgFrm.Page5.oPag.oObj_5_17.Calculate(MSG_AUTOLOADRECZOOM)
            .oPgFrm.Page5.oPag.oObj_5_18.Calculate(MSG_AUTOLOADRECZOOM_TOOLTIP)
        Endwith
        Return

    Proc Calculate_ZSLVHQZHHK()
        With This
            * --- Set default
            .w_RET = .SetDefault()
        Endwith
    Endproc
    Proc Calculate_DDOFRCSJOB()
        With This
            * --- Inizializza combo...
            .w_CIVTHEME = -1
        Endwith
    Endproc
    Proc Calculate_DBYVFFNWVV()
        With This
            * --- Flag gradiente/Griglie avanzate
            .w_CIBCKGRD = Iif(.w_CIVTHEME <> -1 And .w_CITABMEN <> 'S', .w_CIBCKGRD, 'N')
            .w_CICTRGRD = Iif(.w_CIVTHEME <> -1, .w_CICTRGRD, 'N')
        Endwith
    Endproc
    Proc Calculate_SXRQWFXDKC()
        With This
            * --- Espandi gestione parametri zoom
            .w_CIZOESPR = Iif(.w_CICTRGRD='S', 'N', 'S')
        Endwith
    Endproc

    * --- Enable controls under condition
    Procedure mEnableControls()
        This.oPgFrm.Page1.oPag.oCIVTHEME_1_2.Enabled = This.oPgFrm.Page1.oPag.oCIVTHEME_1_2.mCond()
        This.oPgFrm.Page1.oPag.oCITABMEN_1_10.Enabled = This.oPgFrm.Page1.oPag.oCITABMEN_1_10.mCond()
        This.oPgFrm.Page1.oPag.oCIMENFIX_1_12.Enabled = This.oPgFrm.Page1.oPag.oCIMENFIX_1_12.mCond()
        This.oPgFrm.Page1.oPag.oCISEARMN_1_13.Enabled = This.oPgFrm.Page1.oPag.oCISEARMN_1_13.mCond()
        This.oPgFrm.Page1.oPag.oCIBCKGRD_1_15.Enabled = This.oPgFrm.Page1.oPag.oCIBCKGRD_1_15.mCond()
        This.oPgFrm.Page2.oPag.oCIEVIZOM_2_16.Enabled = This.oPgFrm.Page2.oPag.oCIEVIZOM_2_16.mCond()
        This.oPgFrm.Page4.oPag.oCIDSKMEN_4_1.Enabled = This.oPgFrm.Page4.oPag.oCIDSKMEN_4_1.mCond()
        This.oPgFrm.Page5.oPag.oCICTRGRD_5_1.Enabled = This.oPgFrm.Page5.oPag.oCICTRGRD_5_1.mCond()
        This.oPgFrm.Page5.oPag.oCIZOESPR_5_4.Enabled = This.oPgFrm.Page5.oPag.oCIZOESPR_5_4.mCond()
        This.oPgFrm.Page2.oPag.oBtn_2_15.Enabled = This.oPgFrm.Page2.oPag.oBtn_2_15.mCond()
        This.oPgFrm.Page2.oPag.oBtn_2_68.Enabled = This.oPgFrm.Page2.oPag.oBtn_2_68.mCond()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        This.oPgFrm.Pages(4).Enabled=Not(This.w_CIVTHEME=-1)
        This.oPgFrm.Pages(4).oPag.Visible=This.oPgFrm.Pages(4).Enabled
        This.oPgFrm.Page2.oPag.oBtn_2_15.Visible=!This.oPgFrm.Page2.oPag.oBtn_2_15.mHide()
        This.oPgFrm.Page2.oPag.oCOLORZOM_2_75.Visible=!This.oPgFrm.Page2.oPag.oCOLORZOM_2_75.mHide()
        This.oPgFrm.Page2.oPag.oStr_2_76.Visible=!This.oPgFrm.Page2.oPag.oStr_2_76.mHide()
        DoDefault()
        Return

        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            .oPgFrm.Page2.oPag.oObj_2_19.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_20.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_24.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_25.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_43.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_44.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_45.Event(cEvent)
            If Lower(cEvent)==Lower("SetDefault")
                .Calculate_ZSLVHQZHHK()
                bRefresh=.T.
            Endif
            .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_45.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_47.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_48.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_53.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_54.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_55.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_58.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_59.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_60.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_61.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_62.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_48.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_49.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_50.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_51.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_52.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_53.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_54.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_55.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_56.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_57.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_58.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_59.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_60.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_61.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_62.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_63.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_64.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_65.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_66.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_20.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_21.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_22.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_23.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_24.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_25.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_26.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_27.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_28.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_29.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_30.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_31.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_32.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_33.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_34.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_35.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_36.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_37.Event(cEvent)
            .oPgFrm.Page3.oPag.oObj_3_38.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_17.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_18.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_19.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_20.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_21.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_22.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_23.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_24.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_25.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_26.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_27.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_28.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_29.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_30.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_31.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_32.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_33.Event(cEvent)
            .oPgFrm.Page4.oPag.oObj_4_34.Event(cEvent)
            If Lower(cEvent)==Lower("Init")
                .Calculate_DDOFRCSJOB()
                bRefresh=.T.
            Endif
            .oPgFrm.Page2.oPag.oObj_2_69.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_72.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_73.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_78.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_79.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_80.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_81.Event(cEvent)
            .oPgFrm.Page2.oPag.oObj_2_82.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_68.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_69.Event(cEvent)
            .oPgFrm.Page5.oPag.oObj_5_11.Event(cEvent)
            .oPgFrm.Page5.oPag.oObj_5_12.Event(cEvent)
            .oPgFrm.Page5.oPag.oObj_5_13.Event(cEvent)
            .oPgFrm.Page5.oPag.oObj_5_14.Event(cEvent)
            .oPgFrm.Page5.oPag.oObj_5_15.Event(cEvent)
            .oPgFrm.Page5.oPag.oObj_5_16.Event(cEvent)
            .oPgFrm.Page5.oPag.oObj_5_17.Event(cEvent)
            .oPgFrm.Page5.oPag.oObj_5_18.Event(cEvent)
        Endwith
        * --- Area Manuale = Notify Event End
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

    Function SetControlsValue()
        If Not(This.oPgFrm.Page1.oPag.oCIVTHEME_1_2.RadioValue()==This.w_CIVTHEME)
            This.oPgFrm.Page1.oPag.oCIVTHEME_1_2.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oCIREPBEH_1_3.RadioValue()==This.w_CIREPBEH)
            This.oPgFrm.Page1.oPag.oCIREPBEH_1_3.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oCICPTBAR_1_4.RadioValue()==This.w_CICPTBAR)
            This.oPgFrm.Page1.oPag.oCICPTBAR_1_4.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oCIDSKBAR_1_5.RadioValue()==This.w_CIDSKBAR)
            This.oPgFrm.Page1.oPag.oCIDSKBAR_1_5.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oCISTABAR_1_6.RadioValue()==This.w_CISTABAR)
            This.oPgFrm.Page1.oPag.oCISTABAR_1_6.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oCISHWBTN_1_7.RadioValue()==This.w_CISHWBTN)
            This.oPgFrm.Page1.oPag.oCISHWBTN_1_7.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oCITBSIZE_1_8.RadioValue()==This.w_CITBSIZE)
            This.oPgFrm.Page1.oPag.oCITBSIZE_1_8.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oCIMDIFRM_1_9.RadioValue()==This.w_CIMDIFRM)
            This.oPgFrm.Page1.oPag.oCIMDIFRM_1_9.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oCITABMEN_1_10.RadioValue()==This.w_CITABMEN)
            This.oPgFrm.Page1.oPag.oCITABMEN_1_10.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oCIXPTHEM_1_11.RadioValue()==This.w_CIXPTHEM)
            This.oPgFrm.Page1.oPag.oCIXPTHEM_1_11.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oCIMENFIX_1_12.RadioValue()==This.w_CIMENFIX)
            This.oPgFrm.Page1.oPag.oCIMENFIX_1_12.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oCISEARMN_1_13.RadioValue()==This.w_CISEARMN)
            This.oPgFrm.Page1.oPag.oCISEARMN_1_13.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oCITOOLMN_1_14.RadioValue()==This.w_CITOOLMN)
            This.oPgFrm.Page1.oPag.oCITOOLMN_1_14.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.oCIBCKGRD_1_15.RadioValue()==This.w_CIBCKGRD)
            This.oPgFrm.Page1.oPag.oCIBCKGRD_1_15.SetRadio()
        Endif
        If Not(This.oPgFrm.Page1.oPag.ociwaitwd_1_16.RadioValue()==This.w_ciwaitwd)
            This.oPgFrm.Page1.oPag.ociwaitwd_1_16.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oCOLORCTSEL_2_8.Value==This.w_COLORCTSEL)
            This.oPgFrm.Page2.oPag.oCOLORCTSEL_2_8.Value=This.w_COLORCTSEL
        Endif
        If Not(This.oPgFrm.Page2.oPag.oCIEVIZOM_2_16.RadioValue()==This.w_CIEVIZOM)
            This.oPgFrm.Page2.oPag.oCIEVIZOM_2_16.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oCOLORCTED_2_21.Value==This.w_COLORCTED)
            This.oPgFrm.Page2.oPag.oCOLORCTED_2_21.Value=This.w_COLORCTED
        Endif
        If Not(This.oPgFrm.Page2.oPag.oCOLORFCTDSBL_2_26.Value==This.w_COLORFCTDSBL)
            This.oPgFrm.Page2.oPag.oCOLORFCTDSBL_2_26.Value=This.w_COLORFCTDSBL
        Endif
        If Not(This.oPgFrm.Page2.oPag.oCOLORBCTDSBL_2_27.Value==This.w_COLORBCTDSBL)
            This.oPgFrm.Page2.oPag.oCOLORBCTDSBL_2_27.Value=This.w_COLORBCTDSBL
        Endif
        If Not(This.oPgFrm.Page2.oPag.oCOLORGRD_2_34.Value==This.w_COLORGRD)
            This.oPgFrm.Page2.oPag.oCOLORGRD_2_34.Value=This.w_COLORGRD
        Endif
        If Not(This.oPgFrm.Page2.oPag.oCOLORSCREEN_2_37.Value==This.w_COLORSCREEN)
            This.oPgFrm.Page2.oPag.oCOLORSCREEN_2_37.Value=This.w_COLORSCREEN
        Endif
        If Not(This.oPgFrm.Page2.oPag.oCOLORROWDTL_2_42.Value==This.w_COLORROWDTL)
            This.oPgFrm.Page2.oPag.oCOLORROWDTL_2_42.Value=This.w_COLORROWDTL
        Endif
        If Not(This.oPgFrm.Page3.oPag.oCILBLFNM_3_1.RadioValue()==This.w_CILBLFNM)
            This.oPgFrm.Page3.oPag.oCILBLFNM_3_1.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.oCITXTFNM_3_2.RadioValue()==This.w_CITXTFNM)
            This.oPgFrm.Page3.oPag.oCITXTFNM_3_2.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.oCICBXFNM_3_3.RadioValue()==This.w_CICBXFNM)
            This.oPgFrm.Page3.oPag.oCICBXFNM_3_3.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.oCIBTNFNM_3_4.RadioValue()==This.w_CIBTNFNM)
            This.oPgFrm.Page3.oPag.oCIBTNFNM_3_4.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.oCIGRDFNM_3_5.RadioValue()==This.w_CIGRDFNM)
            This.oPgFrm.Page3.oPag.oCIGRDFNM_3_5.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.oCIPAGFNM_3_6.RadioValue()==This.w_CIPAGFNM)
            This.oPgFrm.Page3.oPag.oCIPAGFNM_3_6.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.oCILBLFSZ_3_13.Value==This.w_CILBLFSZ)
            This.oPgFrm.Page3.oPag.oCILBLFSZ_3_13.Value=This.w_CILBLFSZ
        Endif
        If Not(This.oPgFrm.Page3.oPag.oCITXTFSZ_3_15.Value==This.w_CITXTFSZ)
            This.oPgFrm.Page3.oPag.oCITXTFSZ_3_15.Value=This.w_CITXTFSZ
        Endif
        If Not(This.oPgFrm.Page3.oPag.oCICBXFSZ_3_16.Value==This.w_CICBXFSZ)
            This.oPgFrm.Page3.oPag.oCICBXFSZ_3_16.Value=This.w_CICBXFSZ
        Endif
        If Not(This.oPgFrm.Page3.oPag.oCIBTNFSZ_3_17.Value==This.w_CIBTNFSZ)
            This.oPgFrm.Page3.oPag.oCIBTNFSZ_3_17.Value=This.w_CIBTNFSZ
        Endif
        If Not(This.oPgFrm.Page3.oPag.oCIGRDFSZ_3_18.Value==This.w_CIGRDFSZ)
            This.oPgFrm.Page3.oPag.oCIGRDFSZ_3_18.Value=This.w_CIGRDFSZ
        Endif
        If Not(This.oPgFrm.Page3.oPag.oCIPAGFSZ_3_19.Value==This.w_CIPAGFSZ)
            This.oPgFrm.Page3.oPag.oCIPAGFSZ_3_19.Value=This.w_CIPAGFSZ
        Endif
        If Not(This.oPgFrm.Page4.oPag.oCIDSKMEN_4_1.RadioValue()==This.w_CIDSKMEN)
            This.oPgFrm.Page4.oPag.oCIDSKMEN_4_1.SetRadio()
        Endif
        If Not(This.oPgFrm.Page4.oPag.oCINAVSTA_4_2.RadioValue()==This.w_CINAVSTA)
            This.oPgFrm.Page4.oPag.oCINAVSTA_4_2.SetRadio()
        Endif
        If Not(This.oPgFrm.Page4.oPag.oCINAVNUB_4_3.RadioValue()==This.w_CINAVNUB)
            This.oPgFrm.Page4.oPag.oCINAVNUB_4_3.SetRadio()
        Endif
        If Not(This.oPgFrm.Page4.oPag.oCINAVDIM_4_4.Value==This.w_CINAVDIM)
            This.oPgFrm.Page4.oPag.oCINAVDIM_4_4.Value=This.w_CINAVDIM
        Endif
        If Not(This.oPgFrm.Page4.oPag.oCIMNAFNM_4_5.RadioValue()==This.w_CIMNAFNM)
            This.oPgFrm.Page4.oPag.oCIMNAFNM_4_5.SetRadio()
        Endif
        If Not(This.oPgFrm.Page4.oPag.oCIMNAFSZ_4_6.Value==This.w_CIMNAFSZ)
            This.oPgFrm.Page4.oPag.oCIMNAFSZ_4_6.Value=This.w_CIMNAFSZ
        Endif
        If Not(This.oPgFrm.Page4.oPag.oCIWMAFNM_4_7.RadioValue()==This.w_CIWMAFNM)
            This.oPgFrm.Page4.oPag.oCIWMAFNM_4_7.SetRadio()
        Endif
        If Not(This.oPgFrm.Page4.oPag.oCIWMAFSZ_4_8.Value==This.w_CIWMAFSZ)
            This.oPgFrm.Page4.oPag.oCIWMAFSZ_4_8.Value=This.w_CIWMAFSZ
        Endif
        If Not(This.oPgFrm.Page2.oPag.oCOLORCTOB_2_71.Value==This.w_COLORCTOB)
            This.oPgFrm.Page2.oPag.oCOLORCTOB_2_71.Value=This.w_COLORCTOB
        Endif
        If Not(This.oPgFrm.Page2.oPag.ociobl_on_2_74.RadioValue()==This.w_ciobl_on)
            This.oPgFrm.Page2.oPag.ociobl_on_2_74.SetRadio()
        Endif
        If Not(This.oPgFrm.Page2.oPag.oCOLORZOM_2_75.Value==This.w_COLORZOM)
            This.oPgFrm.Page2.oPag.oCOLORZOM_2_75.Value=This.w_COLORZOM
        Endif
        If Not(This.oPgFrm.Page3.oPag.ocilblfit_3_39.RadioValue()==This.w_cilblfit)
            This.oPgFrm.Page3.oPag.ocilblfit_3_39.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.ocitxtfit_3_40.RadioValue()==This.w_citxtfit)
            This.oPgFrm.Page3.oPag.ocitxtfit_3_40.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.ocicbxfit_3_41.RadioValue()==This.w_cicbxfit)
            This.oPgFrm.Page3.oPag.ocicbxfit_3_41.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.ocibtnfit_3_42.RadioValue()==This.w_cibtnfit)
            This.oPgFrm.Page3.oPag.ocibtnfit_3_42.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.ocigrdfit_3_43.RadioValue()==This.w_cigrdfit)
            This.oPgFrm.Page3.oPag.ocigrdfit_3_43.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.ocipagfit_3_44.RadioValue()==This.w_cipagfit)
            This.oPgFrm.Page3.oPag.ocipagfit_3_44.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.ocilblfbo_3_45.RadioValue()==This.w_cilblfbo)
            This.oPgFrm.Page3.oPag.ocilblfbo_3_45.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.ocitxtfbo_3_46.RadioValue()==This.w_citxtfbo)
            This.oPgFrm.Page3.oPag.ocitxtfbo_3_46.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.ocicbxfbo_3_47.RadioValue()==This.w_cicbxfbo)
            This.oPgFrm.Page3.oPag.ocicbxfbo_3_47.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.ocibtnfbo_3_48.RadioValue()==This.w_cibtnfbo)
            This.oPgFrm.Page3.oPag.ocibtnfbo_3_48.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.ocigrdfbo_3_49.RadioValue()==This.w_cigrdfbo)
            This.oPgFrm.Page3.oPag.ocigrdfbo_3_49.SetRadio()
        Endif
        If Not(This.oPgFrm.Page3.oPag.ocipagfbo_3_50.RadioValue()==This.w_cipagfbo)
            This.oPgFrm.Page3.oPag.ocipagfbo_3_50.SetRadio()
        Endif
        If Not(This.oPgFrm.Page5.oPag.oCICTRGRD_5_1.RadioValue()==This.w_CICTRGRD)
            This.oPgFrm.Page5.oPag.oCICTRGRD_5_1.SetRadio()
        Endif
        If Not(This.oPgFrm.Page5.oPag.oCIMRKGRD_5_2.RadioValue()==This.w_CIMRKGRD)
            This.oPgFrm.Page5.oPag.oCIMRKGRD_5_2.SetRadio()
        Endif
        If Not(This.oPgFrm.Page5.oPag.oCIAZOOML_5_3.RadioValue()==This.w_CIAZOOML)
            This.oPgFrm.Page5.oPag.oCIAZOOML_5_3.SetRadio()
        Endif
        If Not(This.oPgFrm.Page5.oPag.oCIZOESPR_5_4.RadioValue()==This.w_CIZOESPR)
            This.oPgFrm.Page5.oPag.oCIZOESPR_5_4.SetRadio()
        Endif
        If Not(This.oPgFrm.Page5.oPag.oCIHEHEZO_5_5.Value==This.w_CIHEHEZO)
            This.oPgFrm.Page5.oPag.oCIHEHEZO_5_5.Value=This.w_CIHEHEZO
        Endif
        If Not(This.oPgFrm.Page5.oPag.oCIHEROZO_5_6.Value==This.w_CIHEROZO)
            This.oPgFrm.Page5.oPag.oCIHEROZO_5_6.Value=This.w_CIHEROZO
        Endif
        cp_SetControlsValueExtFlds(This,'cpsetgui')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            If .bUpdated
                Do Case
                    Case   Not(!Empty(.w_CILBLFNM) And TestFont('L', .w_CILBLFNM, .w_CILBLFSZ)<>-1 And !Empty(.w_CILBLFSZ))
                        .oPgFrm.ActivePage = 3
                        .oPgFrm.Page3.oPag.oCILBLFSZ_3_13.SetFocus()
                        i_bnoChk = .F.
                        i_bRes = .F.
                        i_cErrorMsg = Thisform.msgFmt("Dimensione del font non supportata")
                    Case   Not(!Empty(.w_CITXTFNM) And TestFont('T', .w_CITXTFNM, .w_CITXTFSZ)<>-1 And !Empty(.w_CITXTFSZ))
                        .oPgFrm.ActivePage = 3
                        .oPgFrm.Page3.oPag.oCITXTFSZ_3_15.SetFocus()
                        i_bnoChk = .F.
                        i_bRes = .F.
                        i_cErrorMsg = Thisform.msgFmt("Dimensione del font non supportata")
                    Case   Not(!Empty(.w_CICBXFNM) And TestFont('C', .w_CICBXFNM, .w_CICBXFSZ)<>-1 And !Empty(.w_CICBXFSZ))
                        .oPgFrm.ActivePage = 3
                        .oPgFrm.Page3.oPag.oCICBXFSZ_3_16.SetFocus()
                        i_bnoChk = .F.
                        i_bRes = .F.
                        i_cErrorMsg = Thisform.msgFmt("Dimensione del font non supportata")
                    Case   Not(!Empty(.w_CIBTNFNM) And TestFont('B', .w_CIBTNFNM, .w_CIBTNFSZ)<>-1 And !Empty(.w_CIBTNFSZ))
                        .oPgFrm.ActivePage = 3
                        .oPgFrm.Page3.oPag.oCIBTNFSZ_3_17.SetFocus()
                        i_bnoChk = .F.
                        i_bRes = .F.
                        i_cErrorMsg = Thisform.msgFmt("Dimensione del font non supportata")
                    Case   Not(!Empty(.w_CIGRDFNM) And TestFont('G', .w_CIGRDFNM, .w_CIGRDFSZ)<>-1 And !Empty(.w_CIGRDFSZ))
                        .oPgFrm.ActivePage = 3
                        .oPgFrm.Page3.oPag.oCIGRDFSZ_3_18.SetFocus()
                        i_bnoChk = .F.
                        i_bRes = .F.
                        i_cErrorMsg = Thisform.msgFmt("Dimensione del font non supportata")
                    Case   Not(!Empty(.w_CIPAGFNM) And TestFont('P', .w_CIPAGFNM, .w_CIPAGFSZ)<>-1 And !Empty(.w_CIPAGFSZ))
                        .oPgFrm.ActivePage = 3
                        .oPgFrm.Page3.oPag.oCIPAGFSZ_3_19.SetFocus()
                        i_bnoChk = .F.
                        i_bRes = .F.
                        i_cErrorMsg = Thisform.msgFmt("Dimensione del font non supportata")
                    Case   Not(!Empty(.w_CIMNAFNM) And TestFont('L', .w_CIMNAFNM, .w_CIMNAFSZ)<>-1 And !Empty(.w_CIMNAFSZ))
                        .oPgFrm.ActivePage = 4
                        .oPgFrm.Page4.oPag.oCIMNAFSZ_4_6.SetFocus()
                        i_bnoChk = .F.
                        i_bRes = .F.
                        i_cErrorMsg = Thisform.msgFmt("Dimensione del font non supportata")
                    Case   Not(!Empty(.w_CIWMAFNM) And TestFont('L', .w_CIWMAFNM, .w_CIWMAFSZ)<>-1 And !Empty(.w_CIWMAFSZ))
                        .oPgFrm.ActivePage = 4
                        .oPgFrm.Page4.oPag.oCIWMAFSZ_4_8.SetFocus()
                        i_bnoChk = .F.
                        i_bRes = .F.
                        i_cErrorMsg = Thisform.msgFmt("Dimensione del font non supportata")
                    Case   Not(.w_CIHEHEZO > 0)
                        .oPgFrm.ActivePage = 5
                        .oPgFrm.Page5.oPag.oCIHEHEZO_5_5.SetFocus()
                        i_bnoChk = .F.
                        i_bRes = .F.
                        i_cErrorMsg = Thisform.msgFmt("Inserire l'altezza della testata dello zoom maggiore di zero")
                    Case   Not(.w_CIHEROZO > 0)
                        .oPgFrm.ActivePage = 5
                        .oPgFrm.Page5.oPag.oCIHEROZO_5_6.SetFocus()
                        i_bnoChk = .F.
                        i_bRes = .F.
                        i_cErrorMsg = Thisform.msgFmt("Inserire l'altezza della righe dello zoom maggiore di zero")
                Endcase
            Endif
            * --- Area Manuale = Check Form
            * --- cp_setgui
            If i_bRes
                cp_ErrorMsg(cp_Translate(MSG_SETTING_ON_NEXT_RESTART))
            Endif
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
            Else
                If Not(i_bnoChk)
                    cp_ErrorMsg(i_cErrorMsg)
                Endif
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

    * --- SaveDependsOn
    Proc SaveDependsOn()
        This.o_CIVTHEME = This.w_CIVTHEME
        This.o_CITABMEN = This.w_CITABMEN
        This.o_CICTRGRD = This.w_CICTRGRD
        Return

Enddefine

* --- Define pages as container
Define Class tcp_setguiPag1 As StdContainer
    Width  = 531
    Height = 363
    stdWidth  = 531
    stdheight = 363
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.


    Add Object oCIVTHEME_1_2 As StdCombo With uid="KKYNAIXTLY",Value=2,rtseq=2,rtrep=.F.,Left=32,Top=33,Width=187,Height=19;
        , RowSourceType = 1;
        , ToolTipText = "Tema applicato al gestionale";
        , HelpContextID = 206009433;
        , cFormVar="w_CIVTHEME",RowSource=""+"Standard,"+"Sistema Operativo,"+"2003 Blue,"+"2003 Oliva,"+"2003 Silver,"+"2007 Black,"+"2007 Blue,"+"2007 Silver", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oCIVTHEME_1_2.RadioValue()
        Return(Iif(This.Value =1,-1,;
            iif(This.Value =2,0,;
            iif(This.Value =3,1,;
            iif(This.Value =4,2,;
            iif(This.Value =5,3,;
            iif(This.Value =6,4,;
            iif(This.Value =7,5,;
            iif(This.Value =8,6,;
            0)))))))))
    Endfunc
    Func oCIVTHEME_1_2.GetRadio()
        This.Parent.oContained.w_CIVTHEME = This.RadioValue()
        Return .T.
    Endfunc

    Func oCIVTHEME_1_2.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_CIVTHEME==-1,1,;
            iif(This.Parent.oContained.w_CIVTHEME==0,2,;
            iif(This.Parent.oContained.w_CIVTHEME==1,3,;
            iif(This.Parent.oContained.w_CIVTHEME==2,4,;
            iif(This.Parent.oContained.w_CIVTHEME==3,5,;
            iif(This.Parent.oContained.w_CIVTHEME==4,6,;
            iif(This.Parent.oContained.w_CIVTHEME==5,7,;
            iif(This.Parent.oContained.w_CIVTHEME==6,8,;
            0))))))))
    Endfunc

    Func oCIVTHEME_1_2.mCond()
        With This.Parent.oContained
            Return (Version(5)>=900)
        Endwith
    Endfunc


    Add Object oCIREPBEH_1_3 As StdCombo With uid="PWIWMOGOMA",rtseq=3,rtrep=.F.,Left=286,Top=56,Width=187,Height=19;
        , HelpContextID = 55800961;
        , cFormVar="w_CIREPBEH",RowSource=""+"80 (Retrocompatibile),"+"90 (GDI+)", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oCIREPBEH_1_3.RadioValue()
        Return(Iif(This.Value =1,'8',;
            iif(This.Value =2,'9',;
            space(1))))
    Endfunc
    Func oCIREPBEH_1_3.GetRadio()
        This.Parent.oContained.w_CIREPBEH = This.RadioValue()
        Return .T.
    Endfunc

    Func oCIREPBEH_1_3.SetRadio()
        This.Parent.oContained.w_CIREPBEH=Trim(This.Parent.oContained.w_CIREPBEH)
        This.Value = ;
            iif(This.Parent.oContained.w_CIREPBEH=='8',1,;
            iif(This.Parent.oContained.w_CIREPBEH=='9',2,;
            0))
    Endfunc

    Add Object oCICPTBAR_1_4 As StdCheck With uid="OJRJRQJXNJ",rtseq=4,rtrep=.F.,Left=32, Top=119, Caption="Visualizza barra degli strumenti",;
        ToolTipText = "Se attivo visualizza la barra degli strumenti",;
        HelpContextID = 133461277,;
        cFormVar="w_CICPTBAR", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oCICPTBAR_1_4.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func oCICPTBAR_1_4.GetRadio()
        This.Parent.oContained.w_CICPTBAR = This.RadioValue()
        Return .T.
    Endfunc

    Func oCICPTBAR_1_4.SetRadio()
        This.Parent.oContained.w_CICPTBAR=Trim(This.Parent.oContained.w_CICPTBAR)
        This.Value = ;
            iif(This.Parent.oContained.w_CICPTBAR=='S',1,;
            0)
    Endfunc

    Add Object oCIDSKBAR_1_5 As StdCheck With uid="ZXPARNXJCJ",rtseq=5,rtrep=.F.,Left=32, Top=140, Caption="Visualizza barra delle applicazioni",;
        ToolTipText = "Se attivo visualizza la barra delle applicazioni",;
        HelpContextID = 254113053,;
        cFormVar="w_CIDSKBAR", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oCIDSKBAR_1_5.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func oCIDSKBAR_1_5.GetRadio()
        This.Parent.oContained.w_CIDSKBAR = This.RadioValue()
        Return .T.
    Endfunc

    Func oCIDSKBAR_1_5.SetRadio()
        This.Parent.oContained.w_CIDSKBAR=Trim(This.Parent.oContained.w_CIDSKBAR)
        This.Value = ;
            iif(This.Parent.oContained.w_CIDSKBAR=='S',1,;
            0)
    Endfunc

    Add Object oCISTABAR_1_6 As StdCheck With uid="ERRTOGTWXP",rtseq=6,rtrep=.F.,Left=32, Top=161, Caption="Visualizza barra di stato",;
        ToolTipText = "Se attivo visualizza la barra di stato (in basso)",;
        HelpContextID = 88372509,;
        cFormVar="w_CISTABAR", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oCISTABAR_1_6.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oCISTABAR_1_6.GetRadio()
        This.Parent.oContained.w_CISTABAR = This.RadioValue()
        Return .T.
    Endfunc

    Func oCISTABAR_1_6.SetRadio()
        This.Parent.oContained.w_CISTABAR=Trim(This.Parent.oContained.w_CISTABAR)
        This.Value = ;
            iif(This.Parent.oContained.w_CISTABAR=='S',1,;
            0)
    Endfunc

    Add Object oCISHWBTN_1_7 As StdCheck With uid="FQMHCYRRPS",rtseq=7,rtrep=.F.,Left=32, Top=182, Caption="Visualizza bottone contestuale",;
        ToolTipText = "Se attivo visualizza il bottone contestuale accanto ai campi con link",;
        HelpContextID = 176452848,;
        cFormVar="w_CISHWBTN", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oCISHWBTN_1_7.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oCISHWBTN_1_7.GetRadio()
        This.Parent.oContained.w_CISHWBTN = This.RadioValue()
        Return .T.
    Endfunc

    Func oCISHWBTN_1_7.SetRadio()
        This.Parent.oContained.w_CISHWBTN=Trim(This.Parent.oContained.w_CISHWBTN)
        This.Value = ;
            iif(This.Parent.oContained.w_CISHWBTN=='S',1,;
            0)
    Endfunc


    Add Object oCITBSIZE_1_8 As StdCombo With uid="TKEQGLEPQU",rtseq=8,rtrep=.F.,Left=32,Top=225,Width=76,Height=19;
        , ToolTipText = "Dimensione icone toolbar";
        , HelpContextID = 165317530;
        , cFormVar="w_CITBSIZE",RowSource=""+"16,"+"24,"+"32", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oCITBSIZE_1_8.RadioValue()
        Return(Iif(This.Value =1,16,;
            iif(This.Value =2,24,;
            iif(This.Value =3,32,;
            0))))
    Endfunc
    Func oCITBSIZE_1_8.GetRadio()
        This.Parent.oContained.w_CITBSIZE = This.RadioValue()
        Return .T.
    Endfunc

    Func oCITBSIZE_1_8.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_CITBSIZE==16,1,;
            iif(This.Parent.oContained.w_CITBSIZE==24,2,;
            iif(This.Parent.oContained.w_CITBSIZE==32,3,;
            0)))
    Endfunc


    Add Object oCIMDIFRM_1_9 As StdCombo With uid="BQFRHJEJMG",rtseq=9,rtrep=.F.,Left=286,Top=134,Width=166,Height=19;
        , RowSourceType = 1;
        , ToolTipText = "Modalit� di visualizzazione delle maschere";
        , HelpContextID = 205419742;
        , cFormVar="w_CIMDIFRM",RowSource=""+"Classica,"+"Integrata,"+"Classica / Integrata,"+"Integrata / Classica", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oCIMDIFRM_1_9.RadioValue()
        Return(Iif(This.Value =1,'S',;
            iif(This.Value =2,'I',;
            iif(This.Value =3,'A',;
            iif(This.Value =4,'M',;
            space(1))))))
    Endfunc
    Func oCIMDIFRM_1_9.GetRadio()
        This.Parent.oContained.w_CIMDIFRM = This.RadioValue()
        Return .T.
    Endfunc

    Func oCIMDIFRM_1_9.SetRadio()
        This.Parent.oContained.w_CIMDIFRM=Trim(This.Parent.oContained.w_CIMDIFRM)
        This.Value = ;
            iif(This.Parent.oContained.w_CIMDIFRM=='S',1,;
            iif(This.Parent.oContained.w_CIMDIFRM=='I',2,;
            iif(This.Parent.oContained.w_CIMDIFRM=='A',3,;
            iif(This.Parent.oContained.w_CIMDIFRM=='M',4,;
            0))))
    Endfunc


    Add Object oCITABMEN_1_10 As StdCombo With uid="UGMBXROSQM",rtseq=10,rtrep=.F.,Left=286,Top=177,Width=166,Height=19;
        , RowSourceType = 1;
        , ToolTipText = "Modalit� di visualizzazione delle tab";
        , HelpContextID = 183143199;
        , cFormVar="w_CITABMEN",RowSource=""+"Standard,"+"A tema", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oCITABMEN_1_10.RadioValue()
        Return(Iif(This.Value =1,'S',;
            iif(This.Value =2,'T',;
            space(1))))
    Endfunc
    Func oCITABMEN_1_10.GetRadio()
        This.Parent.oContained.w_CITABMEN = This.RadioValue()
        Return .T.
    Endfunc

    Func oCITABMEN_1_10.SetRadio()
        This.Parent.oContained.w_CITABMEN=Trim(This.Parent.oContained.w_CITABMEN)
        This.Value = ;
            iif(This.Parent.oContained.w_CITABMEN=='S',1,;
            iif(This.Parent.oContained.w_CITABMEN=='T',2,;
            0))
    Endfunc

    Func oCITABMEN_1_10.mCond()
        With This.Parent.oContained
            Return (.w_CIVTHEME <> -1)
        Endwith
    Endfunc


    Add Object oCIXPTHEM_1_11 As StdCombo With uid="AMVFHFREXI",Value=1,rtseq=11,rtrep=.F.,Left=286,Top=220,Width=108,Height=18;
        , RowSourceType = 1;
        , ToolTipText = "Tema XP attivo o meno";
        , HelpContextID = 133597999;
        , cFormVar="w_CIXPTHEM",RowSource=""+"Disattivo,"+"Attivo", bObbl = .F. , nPag = 1;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oCIXPTHEM_1_11.RadioValue()
        Return(Iif(This.Value =1,0,;
            iif(This.Value =2,1,;
            -1)))
    Endfunc
    Func oCIXPTHEM_1_11.GetRadio()
        This.Parent.oContained.w_CIXPTHEM = This.RadioValue()
        Return .T.
    Endfunc

    Func oCIXPTHEM_1_11.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_CIXPTHEM==0,1,;
            iif(This.Parent.oContained.w_CIXPTHEM==1,2,;
            0))
    Endfunc

    Add Object oCIMENFIX_1_12 As StdCheck With uid="QKUNFLMZFJ",rtseq=12,rtrep=.F.,Left=32, Top=295, Caption="Men� in posizione fissa",;
        ToolTipText = "Se attivo visualizza la barra del menu in posizione fissa",;
        HelpContextID = 21919109,;
        cFormVar="w_CIMENFIX", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oCIMENFIX_1_12.RadioValue()
        Return(Iif(This.Value =1,'S',;
            'N'))
    Endfunc
    Func oCIMENFIX_1_12.GetRadio()
        This.Parent.oContained.w_CIMENFIX = This.RadioValue()
        Return .T.
    Endfunc

    Func oCIMENFIX_1_12.SetRadio()
        This.Parent.oContained.w_CIMENFIX=Trim(This.Parent.oContained.w_CIMENFIX)
        This.Value = ;
            iif(This.Parent.oContained.w_CIMENFIX=='S',1,;
            0)
    Endfunc

    Func oCIMENFIX_1_12.mCond()
        With This.Parent.oContained
            Return (.w_CIVTHEME<>-1)
        Endwith
    Endfunc

    Add Object oCISEARMN_1_13 As StdCheck With uid="RWWFHXELEL",rtseq=13,rtrep=.F.,Left=32, Top=316, Caption="Abilita ricerca voci men�",;
        ToolTipText = "Se attivo visualizza il controllo per la ricerca di voci di men�",;
        HelpContextID = 72643817,;
        cFormVar="w_CISEARMN", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oCISEARMN_1_13.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func oCISEARMN_1_13.GetRadio()
        This.Parent.oContained.w_CISEARMN = This.RadioValue()
        Return .T.
    Endfunc

    Func oCISEARMN_1_13.SetRadio()
        This.Parent.oContained.w_CISEARMN=Trim(This.Parent.oContained.w_CISEARMN)
        This.Value = ;
            iif(This.Parent.oContained.w_CISEARMN=='S',1,;
            0)
    Endfunc

    Func oCISEARMN_1_13.mCond()
        With This.Parent.oContained
            Return (.w_CIVTHEME<>-1)
        Endwith
    Endfunc

    Add Object oCITOOLMN_1_14 As StdCheck With uid="RKRMTWUXJM",rtseq=14,rtrep=.F.,Left=32, Top=337, Caption="Abilita tool men�",;
        ToolTipText = "Se attivo abilita il tool menu (visualizzabile tramite ctrl+t)",;
        HelpContextID = 218794775,;
        cFormVar="w_CITOOLMN", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oCITOOLMN_1_14.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func oCITOOLMN_1_14.GetRadio()
        This.Parent.oContained.w_CITOOLMN = This.RadioValue()
        Return .T.
    Endfunc

    Func oCITOOLMN_1_14.SetRadio()
        This.Parent.oContained.w_CITOOLMN=Trim(This.Parent.oContained.w_CITOOLMN)
        This.Value = ;
            iif(This.Parent.oContained.w_CITOOLMN=='S',1,;
            0)
    Endfunc

    Add Object oCIBCKGRD_1_15 As StdCheck With uid="NNTWTSEYPQ",rtseq=15,rtrep=.F.,Left=286, Top=249, Caption="Gradiente di sfondo",;
        ToolTipText = "Se attivo mostra un gradiente di sfondo",;
        HelpContextID = 237204558,;
        cFormVar="w_CIBCKGRD", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oCIBCKGRD_1_15.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func oCIBCKGRD_1_15.GetRadio()
        This.Parent.oContained.w_CIBCKGRD = This.RadioValue()
        Return .T.
    Endfunc

    Func oCIBCKGRD_1_15.SetRadio()
        This.Parent.oContained.w_CIBCKGRD=Trim(This.Parent.oContained.w_CIBCKGRD)
        This.Value = ;
            iif(This.Parent.oContained.w_CIBCKGRD=='S',1,;
            0)
    Endfunc

    Func oCIBCKGRD_1_15.mCond()
        With This.Parent.oContained
            Return (.w_CIVTHEME <> -1 And .w_CITABMEN <> 'S')
        Endwith
    Endfunc

    Add Object ociwaitwd_1_16 As StdCheck With uid="VUZWGYUVMR",rtseq=16,rtrep=.F.,Left=286, Top=270, Caption="Abilita tema wait window",;
        HelpContextID = 238720627,;
        cFormVar="w_ciwaitwd", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func ociwaitwd_1_16.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func ociwaitwd_1_16.GetRadio()
        This.Parent.oContained.w_ciwaitwd = This.RadioValue()
        Return .T.
    Endfunc

    Func ociwaitwd_1_16.SetRadio()
        This.Parent.oContained.w_ciwaitwd=Trim(This.Parent.oContained.w_ciwaitwd)
        This.Value = ;
            iif(This.Parent.oContained.w_ciwaitwd=='S',1,;
            0)
    Endfunc


    Add Object oObj_1_30 As cp_setobjprop With uid="SKPUZOAYUU",Left=656, Top=25, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIVTHEME",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_31 As cp_setCtrlObjprop With uid="WTFHXDANKA",Left=656, Top=4, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Tema",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_32 As cp_setobjprop With uid="SEWJLTLJEG",Left=656, Top=93, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CICPTBAR",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_33 As cp_setobjprop With uid="TDZPUMQCSE",Left=656, Top=114, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIDSKBAR",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_34 As cp_setobjprop With uid="OKPCDXPXJV",Left=656, Top=135, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CISTABAR",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_35 As cp_setobjprop With uid="FPJAZRRGED",Left=656, Top=156, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CISHWBTN",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_36 As cp_setCtrlObjprop With uid="ICHWMLTXJJ",Left=656, Top=186, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Dimensione immagini toolbar",;
        cEvent = "Blank",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_37 As cp_setobjprop With uid="AZRSZKUMQF",Left=656, Top=199, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CITBSIZE",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_38 As cp_setobjprop With uid="GGASNOPFMI",Left=656, Top=264, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIMENFIX",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_39 As cp_setobjprop With uid="ZBBJCLDDXQ",Left=656, Top=285, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CISEARMN",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_40 As cp_setobjprop With uid="YZKVDVLWAI",Left=656, Top=306, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CITOOLMN",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_41 As cp_setobjprop With uid="NXFBOCAKBY",Left=985, Top=268, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIBCKGRD",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_42 As cp_setCtrlObjprop With uid="RLJPSBDGYN",Left=985, Top=92, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Modalit� visualizzazione",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_43 As cp_setCtrlObjprop With uid="GGTKXJGACH",Left=656, Top=69, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Tool bar / status bar",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_44 As cp_setCtrlObjprop With uid="CJDZYWXBKS",Left=985, Top=69, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Form / Control",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_45 As cp_setCtrlObjprop With uid="YXBVBJYCQP",Left=985, Top=136, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Tab",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_46 As cp_setCtrlObjprop With uid="XETOLEHYXC",Left=985, Top=183, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Tema XP",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_47 As cp_setCtrlObjprop With uid="GERYBVQHBS",Left=656, Top=240, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Men�",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_48 As cp_setobjprop With uid="OQZMUDFGMA",Left=985, Top=108, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIMDIFRM",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_49 As cp_setobjprop With uid="ZDVHIOYSCI",Left=772, Top=93, Width=107,Height=18,;
        caption='Object',;
        cProp="caption",cObj="w_CICPTBAR",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_50 As cp_setobjprop With uid="PQJGABBAFV",Left=772, Top=114, Width=107,Height=18,;
        caption='Object',;
        cProp="caption",cObj="w_CIDSKBAR",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_51 As cp_setobjprop With uid="RMZCHLBOAN",Left=772, Top=135, Width=107,Height=18,;
        caption='Object',;
        cProp="caption",cObj="w_CISTABAR",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_52 As cp_setobjprop With uid="ECIZDLTPHW",Left=772, Top=156, Width=107,Height=18,;
        caption='Object',;
        cProp="caption",cObj="w_CISHWBTN",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_53 As cp_setobjprop With uid="XKAOLCKGJH",Left=772, Top=264, Width=107,Height=18,;
        caption='Object',;
        cProp="caption",cObj="w_CIMENFIX",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_54 As cp_setobjprop With uid="NGHDEKYRRB",Left=772, Top=285, Width=107,Height=18,;
        caption='Object',;
        cProp="caption",cObj="w_CISEARMN",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_55 As cp_setobjprop With uid="CIFRAIAOPW",Left=772, Top=306, Width=107,Height=18,;
        caption='Object',;
        cProp="caption",cObj="w_CITOOLMN",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_56 As cp_setobjprop With uid="BCBWBMGOJK",Left=1109, Top=268, Width=107,Height=18,;
        caption='Object',;
        cProp="caption",cObj="w_CIBCKGRD",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_57 As cp_setobjprop With uid="XJQGLQKXUE",Left=772, Top=25, Width=107,Height=18,;
        caption='Object',;
        cProp="RowSource",cObj="w_CIVTHEME",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_58 As cp_setobjprop With uid="FVTYCQKXJF",Left=1109, Top=108, Width=107,Height=18,;
        caption='Object',;
        cProp="RowSource",cObj="w_CIMDIFRM",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_59 As cp_setobjprop With uid="LGHFTQEFAG",Left=985, Top=199, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIXPTHEM",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_60 As cp_setobjprop With uid="OIADGBQAUV",Left=1109, Top=199, Width=107,Height=18,;
        caption='Object',;
        cProp="RowSource",cObj="w_CIXPTHEM",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_61 As cp_setobjprop With uid="QHXRDJZXKH",Left=985, Top=152, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CITABMEN",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_62 As cp_setobjprop With uid="XQXNIRDDGA",Left=1109, Top=152, Width=107,Height=18,;
        caption='Object',;
        cProp="RowSource",cObj="w_CITABMEN",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_68 As cp_setobjprop With uid="GIUJXRAIJC",Left=986, Top=308, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_ciwaitwd",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_1_69 As cp_setobjprop With uid="QQOHFRKHXV",Left=1110, Top=308, Width=107,Height=18,;
        caption='Object',;
        cProp="caption",cObj="w_ciwaitwd",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.


    Add Object oStr_1_17 As StdString With uid="LMQOAPKKXG",Visible=.T., Left=285, Top=204,;
        Alignment=0, Width=110, Height=18,;
        Caption="Tema XP"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_18 As StdString With uid="EGTKSXWAQH",Visible=.T., Left=26, Top=95,;
        Alignment=0, Width=124, Height=18,;
        Caption="Tool bar / status bar"  ;
        , FontName = "Arial", FontSize = 9, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_1_20 As StdString With uid="TQMNZARRNJ",Visible=.T., Left=283, Top=95,;
        Alignment=0, Width=99, Height=18,;
        Caption="Form / Control"  ;
        , FontName = "Arial", FontSize = 9, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_1_22 As StdString With uid="ZPUXLISGUW",Visible=.T., Left=32, Top=13,;
        Alignment=0, Width=130, Height=18,;
        Caption="Tema"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_23 As StdString With uid="MDCYWKOLAP",Visible=.T., Left=286, Top=118,;
        Alignment=0, Width=165, Height=18,;
        Caption="Modalit� visualizzazione"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_24 As StdString With uid="BSYVOZDLMT",Visible=.T., Left=286, Top=161,;
        Alignment=0, Width=94, Height=18,;
        Caption="Tab"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_25 As StdString With uid="JOSOXDUYZH",Visible=.T., Left=32, Top=209,;
        Alignment=0, Width=227, Height=18,;
        Caption="Dimensione immagini toolbar"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_26 As StdString With uid="IFRGPEXQCQ",Visible=.T., Left=26, Top=271,;
        Alignment=0, Width=124, Height=18,;
        Caption="Men�"  ;
        , FontName = "Arial", FontSize = 9, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_1_65 As StdString With uid="GYRYYVDMTN",Visible=.T., Left=283, Top=17,;
        Alignment=0, Width=99, Height=18,;
        Caption="Report"  ;
        , FontName = "Arial", FontSize = 9, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_1_67 As StdString With uid="DRXCKNONKD",Visible=.T., Left=286, Top=39,;
        Alignment=0, Width=130, Height=18,;
        Caption="Motore report"  ;
        , bGlobalFont=.T.

    Add Object oBox_1_19 As StdBox With uid="TLVIIGADYM",Left=8, Top=112, Width=223,Height=1

    Add Object oBox_1_21 As StdBox With uid="IEBPSKRUQN",Left=264, Top=112, Width=240,Height=1

    Add Object oBox_1_27 As StdBox With uid="FXMQMUYWXW",Left=8, Top=288, Width=223,Height=1

    Add Object oBox_1_66 As StdBox With uid="BPIUPEJFUE",Left=264, Top=34, Width=240,Height=1
Enddefine
Define Class tcp_setguiPag2 As StdContainer
    Width  = 531
    Height = 363
    stdWidth  = 531
    stdheight = 363
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.

    Add Object oCOLORCTSEL_2_8 As StdField With uid="MOGOFUELKX",rtseq=24,rtrep=.F.,;
        cFormVar = "w_COLORCTSEL", cQueryName = "COLORCTSEL",Enabled=.F.,;
        bObbl = .F. , nPag = 2, Value=Space(10), bMultilanguage =  .F.,;
        ToolTipText = "Colore da utilizzare per lo sfondo dei campi selezionati",;
        HelpContextID = 99801664,;
        bGlobalFont=.T.,;
        Height=21, Width=83, Left=144, Top=56, InputMask=Replicate('X',10)


    Add Object oBtn_2_9 As StdButton With uid="PEBTFBBJMK",Left=234, Top=56, Width=22,Height=19,;
        caption="...", nPag=2;
        , ToolTipText = ""+MSG_SELECT_COLOR+"";
        , HelpContextID = 19027268;
        , bGlobalFont=.T.

    Proc oBtn_2_9.Click()
        With This.Parent.oContained
            .SetColor('CISELECL')
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_2_10 As StdButton With uid="NXWXMXHDJW",Left=234, Top=85, Width=22,Height=19,;
        caption="...", nPag=2;
        , ToolTipText = ""+MSG_SELECT_COLOR+"";
        , HelpContextID = 19027268;
        , bGlobalFont=.T.

    Proc oBtn_2_10.Click()
        With This.Parent.oContained
            .SetColor('CIEDTCOL')
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_2_11 As StdButton With uid="UNCGHMEXBP",Left=497, Top=56, Width=22,Height=19,;
        caption="...", nPag=2;
        , ToolTipText = ""+MSG_SELECT_COLOR+"";
        , HelpContextID = 19027268;
        , bGlobalFont=.T.

    Proc oBtn_2_11.Click()
        With This.Parent.oContained
            .SetColor('CIDSBLBC')
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_2_12 As StdButton With uid="ZFYVKJJAHZ",Left=497, Top=85, Width=22,Height=19,;
        caption="...", nPag=2;
        , ToolTipText = ""+MSG_SELECT_COLOR+"";
        , HelpContextID = 19027268;
        , bGlobalFont=.T.

    Proc oBtn_2_12.Click()
        With This.Parent.oContained
            .SetColor('CIDSBLFC')
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_2_13 As StdButton With uid="FTMRQULGFT",Left=234, Top=234, Width=22,Height=19,;
        caption="...", nPag=2;
        , ToolTipText = ""+MSG_SELECT_COLOR+"";
        , HelpContextID = 19027268;
        , bGlobalFont=.T.

    Proc oBtn_2_13.Click()
        With This.Parent.oContained
            .SetColor('CIGRIDCL')
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_2_14 As StdButton With uid="RBEAVUZWHM",Left=497, Top=234, Width=22,Height=19,;
        caption="...", nPag=2;
        , ToolTipText = ""+MSG_SELECT_COLOR+"";
        , HelpContextID = 19027268;
        , bGlobalFont=.T.

    Proc oBtn_2_14.Click()
        With This.Parent.oContained
            .SetColor('CISCRCOL')
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_2_15 As StdButton With uid="KIEABAIYOW",Left=234, Top=262, Width=22,Height=19,;
        caption="...", nPag=2;
        , ToolTipText = ""+MSG_SELECT_COLOR+"";
        , HelpContextID = 19027268;
        , bGlobalFont=.T.

    Proc oBtn_2_15.Click()
        With This.Parent.oContained
            .SetColor('CICOLZOM')
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_2_15.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (.w_CIEVIZOM = 2 And Version(5)>=900)
            Endwith
        Endif
    Endfunc

    Func oBtn_2_15.mHide()
        With This.Parent.oContained
            Return (.w_CIEVIZOM <> 2)
        Endwith
    Endfunc

    Add Object oCIEVIZOM_2_16 As StdCheck With uid="ORRAUIDZIS",rtseq=25,rtrep=.F.,Left=42, Top=303, Caption="Evidenzia la riga selezionata",;
        ToolTipText = "Se attivo, la riga selezionata negli oggetti di tipo elenco verr� evidenziata con un colore",;
        HelpContextID = 44665637,;
        cFormVar="w_CIEVIZOM", bObbl = .F. , nPag = 2;
        , bGlobalFont=.T.


    Func oCIEVIZOM_2_16.RadioValue()
        Return(Iif(This.Value =1,2,;
            0))
    Endfunc
    Func oCIEVIZOM_2_16.GetRadio()
        This.Parent.oContained.w_CIEVIZOM = This.RadioValue()
        Return .T.
    Endfunc

    Func oCIEVIZOM_2_16.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_CIEVIZOM==2,1,;
            0)
    Endfunc

    Func oCIEVIZOM_2_16.mCond()
        With This.Parent.oContained
            Return (Version(5)>=900)
        Endwith
    Endfunc


    Add Object oBtn_2_17 As StdButton With uid="FAQVHSXMZR",Left=497, Top=338, Width=22,Height=19,;
        caption="...", nPag=2;
        , ToolTipText = ""+MSG_SELECT_COLOR+"";
        , HelpContextID = 19027268;
        , bGlobalFont=.T.

    Proc oBtn_2_17.Click()
        With This.Parent.oContained
            .SetColor('CIDTLCLR')
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oObj_2_19 As cp_setobjprop With uid="EQRJTNAASA",Left=547, Top=377, Width=184,Height=24,;
        caption='Sfondo Control editabili',;
        cProp="disabledbackcolor",cObj="w_COLORCTED",;
        nPag=2;
        , ToolTipText = "Sfondo control editabili";
        , HelpContextID = 153658488;
        , bGlobalFont=.T.



    Add Object oObj_2_20 As cp_setobjprop With uid="QXICVSSUMR",Left=547, Top=354, Width=184,Height=24,;
        caption='Sfondo Control Selezionato',;
        cProp="disabledbackcolor",cObj="w_COLORCTSEL",;
        nPag=2;
        , ToolTipText = "Sfondo control selezionato";
        , HelpContextID = 240044070;
        , bGlobalFont=.T.


    Add Object oCOLORCTED_2_21 As StdField With uid="NPBORGGMIE",rtseq=26,rtrep=.F.,;
        cFormVar = "w_COLORCTED", cQueryName = "COLORCTED",Enabled=.F.,;
        bObbl = .F. , nPag = 2, Value=Space(10), bMultilanguage =  .F.,;
        ToolTipText = "Colore da utilizzare per lo sfondo dei campi editabili",;
        HelpContextID = 99489888,;
        bGlobalFont=.T.,;
        Height=21, Width=83, Left=144, Top=85, InputMask=Replicate('X',10)


    Add Object oObj_2_24 As cp_setobjprop With uid="UEBWXYJNIO",Left=547, Top=469, Width=184,Height=24,;
        caption='Colore Font control disabilitato',;
        cProp="disabledbackcolor",cObj="w_COLORFCTDSBL",;
        nPag=2;
        , ToolTipText = "Colore font control disabilitato";
        , HelpContextID = 251759825;
        , bGlobalFont=.T.



    Add Object oObj_2_25 As cp_setobjprop With uid="EKKBIWECLA",Left=547, Top=446, Width=184,Height=24,;
        caption='Colore sfondo control disabilitato',;
        cProp="disabledbackcolor",cObj="w_COLORBCTDSBL",;
        nPag=2;
        , ToolTipText = "Colore sfondo control disabilitato";
        , HelpContextID = 70178519;
        , bGlobalFont=.T.


    Add Object oCOLORFCTDSBL_2_26 As StdField With uid="KEPBBFTUIZ",rtseq=27,rtrep=.F.,;
        cFormVar = "w_COLORFCTDSBL", cQueryName = "COLORFCTDSBL",Enabled=.F.,;
        bObbl = .F. , nPag = 2, Value=Space(10), bMultilanguage =  .F.,;
        ToolTipText = "Colore da utilizzare per il carattere dei campi disabilitati",;
        HelpContextID = 183847231,;
        bGlobalFont=.T.,;
        Height=21, Width=83, Left=407, Top=85, InputMask=Replicate('X',10)

    Add Object oCOLORBCTDSBL_2_27 As StdField With uid="HAWELLGLKM",rtseq=28,rtrep=.F.,;
        cFormVar = "w_COLORBCTDSBL", cQueryName = "COLORBCTDSBL",Enabled=.F.,;
        bObbl = .F. , nPag = 2, Value=Space(10), bMultilanguage =  .F.,;
        ToolTipText = "Colore da utilizzare per lo sfondo dei campi disabilitati",;
        HelpContextID = 183847231,;
        bGlobalFont=.T.,;
        Height=21, Width=83, Left=407, Top=56, InputMask=Replicate('X',10)

    Add Object oCOLORGRD_2_34 As StdField With uid="KXBYKVRCAE",rtseq=29,rtrep=.F.,;
        cFormVar = "w_COLORGRD", cQueryName = "COLORGRD",Enabled=.F.,;
        bObbl = .F. , nPag = 2, Value=Space(10), bMultilanguage =  .F.,;
        ToolTipText = "Colore da utilizzare per le orizzontali e verticali",;
        HelpContextID = 168962994,;
        bGlobalFont=.T.,;
        Height=21, Width=83, Left=144, Top=234, InputMask=Replicate('X',10)

    Add Object oCOLORSCREEN_2_37 As StdField With uid="QHTHYIIPFX",rtseq=30,rtrep=.F.,;
        cFormVar = "w_COLORSCREEN", cQueryName = "COLORSCREEN",Enabled=.F.,;
        bObbl = .F. , nPag = 2, Value=Space(10), bMultilanguage =  .F.,;
        ToolTipText = "Colore da utilizzare per lo sfondo del desktop",;
        HelpContextID = 104884767,;
        bGlobalFont=.T.,;
        Height=21, Width=83, Left=407, Top=234, InputMask=Replicate('X',10)

    Add Object oCOLORROWDTL_2_42 As StdField With uid="ZTFNGYYBDO",rtseq=31,rtrep=.F.,;
        cFormVar = "w_COLORROWDTL", cQueryName = "COLORROWDTL",Enabled=.F.,;
        bObbl = .F. , nPag = 2, Value=Space(10), bMultilanguage =  .F.,;
        ToolTipText = "Colore da utilizzare per lo sfondo della riga selezionata in una griglia di dati",;
        HelpContextID = 104814971,;
        bGlobalFont=.T.,;
        Height=21, Width=83, Left=407, Top=338, InputMask=Replicate('X',10)


    Add Object oObj_2_43 As cp_setobjprop With uid="HTUAIRSZBU",Left=547, Top=492, Width=184,Height=24,;
        caption='Colore Screen',;
        cProp="disabledbackcolor",cObj="w_COLORSCREEN",;
        nPag=2;
        , ToolTipText = "Colore screen";
        , HelpContextID = 53716980;
        , bGlobalFont=.T.



    Add Object oObj_2_44 As cp_setobjprop With uid="HQPKMNSDWL",Left=547, Top=423, Width=184,Height=24,;
        caption='Colore Riga Selezionata Dtl',;
        cProp="disabledbackcolor",cObj="w_COLORROWDTL",;
        nPag=2;
        , ToolTipText = "Colore screen";
        , HelpContextID = 163494631;
        , bGlobalFont=.T.



    Add Object oObj_2_45 As cp_setobjprop With uid="SJXRMMHHHD",Left=547, Top=400, Width=184,Height=24,;
        caption='Colore linee griglie',;
        cProp="disabledbackcolor",cObj="w_COLORGRD",;
        nPag=2;
        , ToolTipText = "Setta colore linee griglia";
        , HelpContextID = 93080990;
        , bGlobalFont=.T.



    Add Object oObj_2_48 As cp_setCtrlObjprop With uid="LBGOBDWLGG",Left=563, Top=33, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Abilitati",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_49 As cp_setCtrlObjprop With uid="BRNYYLGECG",Left=919, Top=33, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Disabilitati",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_50 As cp_setCtrlObjprop With uid="FMTHXQBUNN",Left=563, Top=172, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Elenchi /zoom",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_51 As cp_setCtrlObjprop With uid="QHVGKEWOCK",Left=919, Top=172, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Desktop",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_52 As cp_setCtrlObjprop With uid="NVEMDKDGBL",Left=563, Top=95, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Selezionato:",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_53 As cp_setCtrlObjprop With uid="ZFKSCPIZYA",Left=563, Top=124, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Editabile:",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_54 As cp_setCtrlObjprop With uid="MQZMSZNUKL",Left=919, Top=95, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Sfondo:",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_55 As cp_setCtrlObjprop With uid="FXOZUDEZEV",Left=919, Top=124, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Carattere:",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_56 As cp_setCtrlObjprop With uid="PZYWEXURWC",Left=563, Top=202, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Linee griglie:",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_57 As cp_setCtrlObjprop With uid="FRYXCYQANL",Left=919, Top=295, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Riga selezionata:",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_58 As cp_setCtrlObjprop With uid="WAGTPYKNJL",Left=919, Top=265, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Griglie di dati",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_59 As cp_setCtrlObjprop With uid="JGSNFRQEXW",Left=919, Top=202, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Sfondo desktop:",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_60 As cp_setobjprop With uid="QDSZCFUUKG",Left=681, Top=95, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_COLORCTSEL",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_61 As cp_setobjprop With uid="IJQIHULLVL",Left=681, Top=124, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_COLORCTED",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_62 As cp_setobjprop With uid="BBABGKXHCK",Left=681, Top=202, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_COLORGRD",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_63 As cp_setobjprop With uid="CRGSPUXOWD",Left=1038, Top=95, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_COLORBCTDSBL",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_64 As cp_setobjprop With uid="TKDBGOMIXZ",Left=1038, Top=124, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_COLORFCTDSBL",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_65 As cp_setobjprop With uid="MXHBLEHCOL",Left=1038, Top=202, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_COLORSCREEN",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_66 As cp_setobjprop With uid="LLOTTPAQGN",Left=1038, Top=295, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_COLORROWDTL",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oBtn_2_68 As StdButton With uid="KJTASBKMYO",Left=283, Top=132, Width=22,Height=22,;
        caption="...", nPag=2;
        , ToolTipText = ""+MSG_SELECT_COLOR+"";
        , HelpContextID = 19027268;
        , bGlobalFont=.T.

    Proc oBtn_2_68.Click()
        With This.Parent.oContained
            .SetColor('CIOBLCOL')
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Func oBtn_2_68.mCond()
        If !Isnull(This.Parent.oContained)
            With This.Parent.oContained
                Return (.w_ciobl_on='S')
            Endwith
        Endif
    Endfunc


    Add Object oObj_2_69 As cp_setobjprop With uid="HKZXMTAGRT",Left=547, Top=514, Width=184,Height=24,;
        caption='Campi obbligatori',;
        cProp="disabledbackcolor",cObj="w_COLORCTOB",;
        nPag=2;
        , ToolTipText = "Campi obbligatori";
        , HelpContextID = 150959910;
        , bGlobalFont=.T.


    Add Object oCOLORCTOB_2_71 As StdField With uid="XURQDVYPNZ",rtseq=54,rtrep=.F.,;
        cFormVar = "w_COLORCTOB", cQueryName = "COLORCTOB",Enabled=.F.,;
        bObbl = .F. , nPag = 2, Value=Space(10), bMultilanguage =  .F.,;
        ToolTipText = "Colore da utilizzare per evidenziare i campi obbligatori",;
        HelpContextID = 99489536,;
        bGlobalFont=.T.,;
        Height=21, Width=83, Left=196, Top=132, InputMask=Replicate('X',10)


    Add Object oObj_2_72 As cp_setCtrlObjprop With uid="MUTAYCSXTC",Left=562, Top=144, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Evidenzia obbligatori:",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_73 As cp_setobjprop With uid="RFPQEWZFUF",Left=680, Top=144, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_COLORCTSEL",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object ociobl_on_2_74 As StdCombo With uid="YGCPCFYLBX",rtseq=55,rtrep=.F.,Left=144,Top=133,Width=47,Height=21;
        , HelpContextID = 21141259;
        , cFormVar="w_ciobl_on",RowSource=""+"Si,"+"No", bObbl = .F. , nPag = 2;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func ociobl_on_2_74.RadioValue()
        Return(Iif(This.Value =1,'S',;
            iif(This.Value =2,'N',;
            space(1))))
    Endfunc
    Func ociobl_on_2_74.GetRadio()
        This.Parent.oContained.w_ciobl_on = This.RadioValue()
        Return .T.
    Endfunc

    Func ociobl_on_2_74.SetRadio()
        This.Parent.oContained.w_ciobl_on=Trim(This.Parent.oContained.w_ciobl_on)
        This.Value = ;
            iif(This.Parent.oContained.w_ciobl_on=='S',1,;
            iif(This.Parent.oContained.w_ciobl_on=='N',2,;
            0))
    Endfunc

    Add Object oCOLORZOM_2_75 As StdField With uid="RIGFTLDQIM",rtseq=56,rtrep=.F.,;
        cFormVar = "w_COLORZOM", cQueryName = "COLORZOM",Enabled=.F.,;
        bObbl = .F. , nPag = 2, Value=Space(10), bMultilanguage =  .F.,;
        ToolTipText = "Colore da utilizzare per lo sfondo della riga selezionata in un elenco",;
        HelpContextID = 168962853,;
        bGlobalFont=.T.,;
        Height=21, Width=83, Left=144, Top=262, InputMask=Replicate('X',10)

    Func oCOLORZOM_2_75.mHide()
        With This.Parent.oContained
            Return (.w_CIEVIZOM <> 2)
        Endwith
    Endfunc


    Add Object oObj_2_78 As cp_setobjprop With uid="RAKYNHALNR",Left=547, Top=537, Width=184,Height=24,;
        caption='Evidenza riga selezionata negli zoom',;
        cProp="disabledbackcolor",cObj="w_COLORZOM",;
        nPag=2;
        , ToolTipText = "Evidenza riga selezionata negli zoom";
        , HelpContextID = 44752877;
        , bGlobalFont=.T.



    Add Object oObj_2_79 As cp_setobjprop With uid="ZCCGKRCAEV",Left=563, Top=288, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIEVIZOM",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_80 As cp_setobjprop With uid="QTQZNCCEFV",Left=679, Top=288, Width=107,Height=18,;
        caption='Object',;
        cProp="caption",cObj="w_CIEVIZOM",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_81 As cp_setCtrlObjprop With uid="CDNLIVIKKB",Left=563, Top=318, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Colore riga selezionata:",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_2_82 As cp_setobjprop With uid="JEDBAOICUP",Left=679, Top=318, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_COLORZOM",;
        cEvent = "Init",;
        nPag=2;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.


    Add Object oStr_2_18 As StdString With uid="CEKXRNXBRY",Visible=.T., Left=36, Top=33,;
        Alignment=0, Width=177, Height=18,;
        Caption="Abilitati"  ;
        , FontName = "Arial", FontSize = 9, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_2_22 As StdString With uid="DOZEKZBHRE",Visible=.T., Left=5, Top=85,;
        Alignment=1, Width=134, Height=18,;
        Caption="Editabile:"  ;
        , bGlobalFont=.T.

    Add Object oStr_2_23 As StdString With uid="SIJNQEVDJS",Visible=.T., Left=5, Top=56,;
        Alignment=1, Width=134, Height=18,;
        Caption="Selezionato:"  ;
        , bGlobalFont=.T.

    Add Object oStr_2_28 As StdString With uid="WBTKYRDXRM",Visible=.T., Left=271, Top=85,;
        Alignment=1, Width=134, Height=18,;
        Caption="Carattere:"  ;
        , bGlobalFont=.T.

    Add Object oStr_2_29 As StdString With uid="KZYMQJXXIO",Visible=.T., Left=271, Top=56,;
        Alignment=1, Width=134, Height=18,;
        Caption="Sfondo:"  ;
        , bGlobalFont=.T.

    Add Object oStr_2_30 As StdString With uid="KOJIFVHWEB",Visible=.T., Left=282, Top=33,;
        Alignment=0, Width=177, Height=18,;
        Caption="Disabilitati"  ;
        , FontName = "Arial", FontSize = 9, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_2_33 As StdString With uid="AHEXUSEGBY",Visible=.T., Left=5, Top=234,;
        Alignment=1, Width=134, Height=18,;
        Caption="Linee griglie:"  ;
        , bGlobalFont=.T.

    Add Object oStr_2_35 As StdString With uid="OYNODWFTEC",Visible=.T., Left=36, Top=204,;
        Alignment=0, Width=177, Height=18,;
        Caption="Elenchi /zoom"  ;
        , FontName = "Arial", FontSize = 9, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_2_38 As StdString With uid="WVSMJOVFHN",Visible=.T., Left=271, Top=234,;
        Alignment=1, Width=134, Height=18,;
        Caption="Sfondo desktop:"  ;
        , bGlobalFont=.T.

    Add Object oStr_2_39 As StdString With uid="MRONQBDVIX",Visible=.T., Left=282, Top=204,;
        Alignment=0, Width=177, Height=18,;
        Caption="Desktop"  ;
        , FontName = "Arial", FontSize = 9, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_2_41 As StdString With uid="KAFSEBDEFM",Visible=.T., Left=271, Top=338,;
        Alignment=1, Width=134, Height=18,;
        Caption="Riga selezionata:"  ;
        , bGlobalFont=.T.

    Add Object oStr_2_46 As StdString With uid="TZEEBVPTMD",Visible=.T., Left=282, Top=308,;
        Alignment=0, Width=177, Height=18,;
        Caption="Griglie di dati"  ;
        , FontName = "Arial", FontSize = 9, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_2_70 As StdString With uid="QZMKQOYXLQ",Visible=.T., Left=5, Top=135,;
        Alignment=1, Width=134, Height=18,;
        Caption="Evidenzia obbligatori:"  ;
        , bGlobalFont=.T.

    Add Object oStr_2_76 As StdString With uid="YGHAYYNNKF",Visible=.T., Left=1, Top=262,;
        Alignment=1, Width=138, Height=18,;
        Caption="Colore riga selezionata:"  ;
        , bGlobalFont=.T.

    Func oStr_2_76.mHide()
        With This.Parent.oContained
            Return (.w_CIEVIZOM <> 2)
        Endwith
    Endfunc

    Add Object oBox_2_31 As StdBox With uid="UVDBTBFTNC",Left=20, Top=50, Width=248,Height=1

    Add Object oBox_2_32 As StdBox With uid="FMNXZMMLUY",Left=278, Top=50, Width=248,Height=1

    Add Object oBox_2_36 As StdBox With uid="WBEAFAEBLG",Left=20, Top=221, Width=248,Height=1

    Add Object oBox_2_40 As StdBox With uid="TPWVWCSGLL",Left=278, Top=221, Width=248,Height=1

    Add Object oBox_2_47 As StdBox With uid="GKWHLPOCVK",Left=278, Top=325, Width=248,Height=1
Enddefine
Define Class tcp_setguiPag3 As StdContainer
    Width  = 531
    Height = 363
    stdWidth  = 531
    stdheight = 363
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.


    Add Object oCILBLFNM_3_1 As StdTableComboFont With uid="XEWFUYKPVZ",rtseq=32,rtrep=.F.,Left=127,Top=57,Width=164,Height=19;
        , ToolTipText = "Font da utilizzare per la etichette, checkbox, radiobox";
        , HelpContextID = 253588698;
        , cFormVar="w_CILBLFNM",tablefilter="", bObbl = .F. , nPag = 3;
        , cTable='',cKey='',cValue='',cOrderBy='',xDefault=Space(50);
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.


    Add Object oCITXTFNM_3_2 As StdTableComboFont With uid="AEBOTFGNNC",rtseq=33,rtrep=.F.,Left=127,Top=91,Width=164,Height=19;
        , ToolTipText = "Font da utilizzare per textbox, memo";
        , HelpContextID = 142963930;
        , cFormVar="w_CITXTFNM",tablefilter="", bObbl = .F. , nPag = 3;
        , cTable='',cKey='',cValue='',cOrderBy='',xDefault=Space(50);
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.


    Add Object oCICBXFNM_3_3 As StdTableComboFont With uid="NXNLBIHBGE",rtseq=34,rtrep=.F.,Left=127,Top=125,Width=164,Height=19;
        , ToolTipText = "Font da utilizzare per combobox";
        , HelpContextID = 185890010;
        , cFormVar="w_CICBXFNM",tablefilter="", bObbl = .F. , nPag = 3;
        , cTable='',cKey='',cValue='',cOrderBy='',xDefault=Space(50);
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.


    Add Object oCIBTNFNM_3_4 As StdTableComboFont With uid="FYKEPEUGQU",rtseq=35,rtrep=.F.,Left=127,Top=159,Width=164,Height=19;
        , ToolTipText = "Font da utilizzare per i bottoni";
        , HelpContextID = 36926682;
        , cFormVar="w_CIBTNFNM",tablefilter="", bObbl = .F. , nPag = 3;
        , cTable='',cKey='',cValue='',cOrderBy='',xDefault=Space(50);
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.


    Add Object oCIGRDFNM_3_5 As StdTableComboFont With uid="JJSHBWWONC",rtseq=36,rtrep=.F.,Left=127,Top=193,Width=164,Height=19;
        , ToolTipText = "Font da utilizzare per gli zoom";
        , HelpContextID = 135820506;
        , cFormVar="w_CIGRDFNM",tablefilter="", bObbl = .F. , nPag = 3;
        , cTable='',cKey='',cValue='',cOrderBy='',xDefault=Space(50);
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.


    Add Object oCIPAGFNM_3_6 As StdTableComboFont With uid="LCIYMQPBZY",rtseq=37,rtrep=.F.,Left=127,Top=227,Width=164,Height=19;
        , ToolTipText = "Font da utilizzare per le tab";
        , HelpContextID = 168916186;
        , cFormVar="w_CIPAGFNM",tablefilter="", bObbl = .F. , nPag = 3;
        , cTable='',cKey='',cValue='',cOrderBy='',xDefault=Space(50);
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Add Object oCILBLFSZ_3_13 As StdField With uid="KTPMHIBSVZ",rtseq=38,rtrep=.F.,;
        cFormVar = "w_CILBLFSZ", cQueryName = "CILBLFSZ",;
        bObbl = .F. , nPag = 3, Value=0, bMultilanguage =  .F.,;
        sErrorMsg = "Dimensione del font non supportata",;
        ToolTipText = "Dimensione font etichette",;
        HelpContextID = 253588911,;
        bGlobalFont=.T.,;
        Height=21, Width=76, Left=308, Top=57, cSayPict='"999"', cGetPict='"999"'

    Func oCILBLFSZ_3_13.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            bRes = (!Empty(.w_CILBLFNM) And TestFont('L', .w_CILBLFNM, .w_CILBLFSZ)<>-1 And !Empty(.w_CILBLFSZ))
            If bRes And !(TestFont('L', .w_CILBLFNM, .w_CILBLFSZ) = 1)
                bRes=(Messagebox(cp_Translate(Thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente"))+". "+cp_Translate(MSG_ACCEPT_ANYWAY),4+32,"")=6)
                This.Parent.oContained.bDontReportError=!bRes
            Endif
        Endwith
        Return bRes
    Endfunc

    Add Object oCITXTFSZ_3_15 As StdField With uid="MZYXYCYXKG",rtseq=39,rtrep=.F.,;
        cFormVar = "w_CITXTFSZ", cQueryName = "CITXTFSZ",;
        bObbl = .F. , nPag = 3, Value=0, bMultilanguage =  .F.,;
        sErrorMsg = "Dimensione del font non supportata",;
        ToolTipText = "Dimensione font campi",;
        HelpContextID = 142964143,;
        bGlobalFont=.T.,;
        Height=21, Width=76, Left=308, Top=91, cSayPict='"999"', cGetPict='"999"'

    Func oCITXTFSZ_3_15.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            bRes = (!Empty(.w_CITXTFNM) And TestFont('T', .w_CITXTFNM, .w_CITXTFSZ)<>-1 And !Empty(.w_CITXTFSZ))
            If bRes And !(TestFont('T', .w_CITXTFNM, .w_CITXTFSZ) = 1)
                bRes=(Messagebox(cp_Translate(Thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente"))+". "+cp_Translate(MSG_ACCEPT_ANYWAY),4+32,"")=6)
                This.Parent.oContained.bDontReportError=!bRes
            Endif
        Endwith
        Return bRes
    Endfunc

    Add Object oCICBXFSZ_3_16 As StdField With uid="ELFXAWJCPG",rtseq=40,rtrep=.F.,;
        cFormVar = "w_CICBXFSZ", cQueryName = "CICBXFSZ",;
        bObbl = .F. , nPag = 3, Value=0, bMultilanguage =  .F.,;
        sErrorMsg = "Dimensione del font non supportata",;
        ToolTipText = "Dimensione font combobox",;
        HelpContextID = 185890223,;
        bGlobalFont=.T.,;
        Height=21, Width=76, Left=308, Top=125, cSayPict='"999"', cGetPict='"999"'

    Func oCICBXFSZ_3_16.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            bRes = (!Empty(.w_CICBXFNM) And TestFont('C', .w_CICBXFNM, .w_CICBXFSZ)<>-1 And !Empty(.w_CICBXFSZ))
            If bRes And !(TestFont('C', .w_CICBXFNM, .w_CICBXFSZ) = 1)
                bRes=(Messagebox(cp_Translate(Thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente"))+". "+cp_Translate(MSG_ACCEPT_ANYWAY),4+32,"")=6)
                This.Parent.oContained.bDontReportError=!bRes
            Endif
        Endwith
        Return bRes
    Endfunc

    Add Object oCIBTNFSZ_3_17 As StdField With uid="CDYSADIXOB",rtseq=41,rtrep=.F.,;
        cFormVar = "w_CIBTNFSZ", cQueryName = "CIBTNFSZ",;
        bObbl = .F. , nPag = 3, Value=0, bMultilanguage =  .F.,;
        sErrorMsg = "Dimensione del font non supportata",;
        ToolTipText = "Dimensione font bottoni",;
        HelpContextID = 36926895,;
        bGlobalFont=.T.,;
        Height=21, Width=76, Left=308, Top=159, cSayPict='"999"', cGetPict='"999"'

    Func oCIBTNFSZ_3_17.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            bRes = (!Empty(.w_CIBTNFNM) And TestFont('B', .w_CIBTNFNM, .w_CIBTNFSZ)<>-1 And !Empty(.w_CIBTNFSZ))
            If bRes And !(TestFont('B', .w_CIBTNFNM, .w_CIBTNFSZ)=1)
                bRes=(Messagebox(cp_Translate(Thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente"))+". "+cp_Translate(MSG_ACCEPT_ANYWAY),4+32,"")=6)
                This.Parent.oContained.bDontReportError=!bRes
            Endif
        Endwith
        Return bRes
    Endfunc

    Add Object oCIGRDFSZ_3_18 As StdField With uid="HAYREGIFUQ",rtseq=42,rtrep=.F.,;
        cFormVar = "w_CIGRDFSZ", cQueryName = "CIGRDFSZ",;
        bObbl = .F. , nPag = 3, Value=0, bMultilanguage =  .F.,;
        sErrorMsg = "Dimensione del font non supportata",;
        ToolTipText = "Dimensione font zoom",;
        HelpContextID = 135820719,;
        bGlobalFont=.T.,;
        Height=21, Width=76, Left=308, Top=193, cSayPict='"999"', cGetPict='"999"'

    Func oCIGRDFSZ_3_18.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            bRes = (!Empty(.w_CIGRDFNM) And TestFont('G', .w_CIGRDFNM, .w_CIGRDFSZ)<>-1 And !Empty(.w_CIGRDFSZ))
            If bRes And !(TestFont('G', .w_CIGRDFNM, .w_CIGRDFSZ)=1)
                bRes=(Messagebox(cp_Translate(Thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente"))+". "+cp_Translate(MSG_ACCEPT_ANYWAY),4+32,"")=6)
                This.Parent.oContained.bDontReportError=!bRes
            Endif
        Endwith
        Return bRes
    Endfunc

    Add Object oCIPAGFSZ_3_19 As StdField With uid="TZTVIGUUPA",rtseq=43,rtrep=.F.,;
        cFormVar = "w_CIPAGFSZ", cQueryName = "CIPAGFSZ",;
        bObbl = .F. , nPag = 3, Value=0, bMultilanguage =  .F.,;
        sErrorMsg = "Dimensione del font non supportata",;
        ToolTipText = "Dimensione font tab",;
        HelpContextID = 168916399,;
        bGlobalFont=.T.,;
        Height=21, Width=76, Left=308, Top=227, cSayPict='"999"', cGetPict='"999"'

    Func oCIPAGFSZ_3_19.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            bRes = (!Empty(.w_CIPAGFNM) And TestFont('P', .w_CIPAGFNM, .w_CIPAGFSZ)<>-1 And !Empty(.w_CIPAGFSZ))
            If bRes And !(TestFont('P', .w_CIPAGFNM, .w_CIPAGFSZ)=1)
                bRes=(Messagebox(cp_Translate(Thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente"))+". "+cp_Translate(MSG_ACCEPT_ANYWAY),4+32,"")=6)
                This.Parent.oContained.bDontReportError=!bRes
            Endif
        Endwith
        Return bRes
    Endfunc


    Add Object oObj_3_20 As cp_setCtrlObjprop With uid="ZMYOFSIQGW",Left=543, Top=48, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Font etichette:",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_21 As cp_setCtrlObjprop With uid="AYHZAWCQDZ",Left=543, Top=82, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Font campi:",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_22 As cp_setCtrlObjprop With uid="IOTZXVKKVG",Left=543, Top=116, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Font combobox:",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_23 As cp_setCtrlObjprop With uid="FMFTEUHYJN",Left=543, Top=150, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Font bottoni:",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_24 As cp_setCtrlObjprop With uid="MDQQBYPDUK",Left=543, Top=184, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Font zoom:",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_25 As cp_setCtrlObjprop With uid="ZDTSAPRYON",Left=543, Top=218, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Font tab:",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_26 As cp_setCtrlObjprop With uid="XWXGWGZIMD",Left=846, Top=28, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Dimensione",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_27 As cp_setobjprop With uid="IQEMRMQANG",Left=657, Top=48, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CILBLFNM",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_28 As cp_setobjprop With uid="QEHANLBBKT",Left=657, Top=82, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CITXTFNM",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_29 As cp_setobjprop With uid="DZTCUEIASV",Left=657, Top=150, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIBTNFNM",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_30 As cp_setobjprop With uid="EMCYNRGSCM",Left=657, Top=184, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIGRDFNM",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_31 As cp_setobjprop With uid="HDTOVOLKON",Left=657, Top=218, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIPAGFNM",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_32 As cp_setobjprop With uid="BEJKRFLNNW",Left=657, Top=116, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CICBXFNM",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_33 As cp_setobjprop With uid="PNHQAUNKJM",Left=845, Top=51, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CILBLFSZ",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_34 As cp_setobjprop With uid="ITZDGQAKSA",Left=845, Top=85, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CITXTFSZ",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_35 As cp_setobjprop With uid="EFHWJCDOWQ",Left=845, Top=153, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIBTNFSZ",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_36 As cp_setobjprop With uid="DBJTCIMGIM",Left=845, Top=187, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIGRDFSZ",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_37 As cp_setobjprop With uid="GLONJNIPXT",Left=845, Top=221, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIPAGFSZ",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_3_38 As cp_setobjprop With uid="BDNVGTJPYL",Left=845, Top=119, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CICBXFSZ",;
        cEvent = "Init",;
        nPag=3;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.


    Add Object ocilblfit_3_39 As StdCheck With uid="ZVWDSPGSBR",rtseq=58,rtrep=.F.,Left=396, Top=57, Caption="Italic",;
        HelpContextID = 247490715,;
        cFormVar="w_cilblfit", bObbl = .F. , nPag = 3;
        , bGlobalFont=.T.


    Func ocilblfit_3_39.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func ocilblfit_3_39.GetRadio()
        This.Parent.oContained.w_cilblfit = This.RadioValue()
        Return .T.
    Endfunc

    Func ocilblfit_3_39.SetRadio()
        This.Parent.oContained.w_cilblfit=Trim(This.Parent.oContained.w_cilblfit)
        This.Value = ;
            iif(This.Parent.oContained.w_cilblfit=='S',1,;
            0)
    Endfunc

    Add Object ocitxtfit_3_40 As StdCheck With uid="PCDBEISXPW",rtseq=59,rtrep=.F.,Left=396, Top=91, Caption="Italic",;
        HelpContextID = 89680027,;
        cFormVar="w_citxtfit", bObbl = .F. , nPag = 3;
        , bGlobalFont=.T.


    Func ocitxtfit_3_40.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func ocitxtfit_3_40.GetRadio()
        This.Parent.oContained.w_citxtfit = This.RadioValue()
        Return .T.
    Endfunc

    Func ocitxtfit_3_40.SetRadio()
        This.Parent.oContained.w_citxtfit=Trim(This.Parent.oContained.w_citxtfit)
        This.Value = ;
            iif(This.Parent.oContained.w_citxtfit=='S',1,;
            0)
    Endfunc

    Add Object ocicbxfit_3_41 As StdCheck With uid="PUGVECLQRN",rtseq=60,rtrep=.F.,Left=396, Top=125, Caption="Italic",;
        HelpContextID = 46753947,;
        cFormVar="w_cicbxfit", bObbl = .F. , nPag = 3;
        , bGlobalFont=.T.


    Func ocicbxfit_3_41.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func ocicbxfit_3_41.GetRadio()
        This.Parent.oContained.w_cicbxfit = This.RadioValue()
        Return .T.
    Endfunc

    Func ocicbxfit_3_41.SetRadio()
        This.Parent.oContained.w_cicbxfit=Trim(This.Parent.oContained.w_cicbxfit)
        This.Value = ;
            iif(This.Parent.oContained.w_cicbxfit=='S',1,;
            0)
    Endfunc

    Add Object ocibtnfit_3_42 As StdCheck With uid="RSYBGVOCWW",rtseq=61,rtrep=.F.,Left=396, Top=159, Caption="Italic",;
        HelpContextID = 195717275,;
        cFormVar="w_cibtnfit", bObbl = .F. , nPag = 3;
        , bGlobalFont=.T.


    Func ocibtnfit_3_42.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func ocibtnfit_3_42.GetRadio()
        This.Parent.oContained.w_cibtnfit = This.RadioValue()
        Return .T.
    Endfunc

    Func ocibtnfit_3_42.SetRadio()
        This.Parent.oContained.w_cibtnfit=Trim(This.Parent.oContained.w_cibtnfit)
        This.Value = ;
            iif(This.Parent.oContained.w_cibtnfit=='S',1,;
            0)
    Endfunc

    Add Object ocigrdfit_3_43 As StdCheck With uid="XXQSRCESWM",rtseq=62,rtrep=.F.,Left=396, Top=193, Caption="Italic",;
        HelpContextID = 96823451,;
        cFormVar="w_cigrdfit", bObbl = .F. , nPag = 3;
        , bGlobalFont=.T.


    Func ocigrdfit_3_43.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func ocigrdfit_3_43.GetRadio()
        This.Parent.oContained.w_cigrdfit = This.RadioValue()
        Return .T.
    Endfunc

    Func ocigrdfit_3_43.SetRadio()
        This.Parent.oContained.w_cigrdfit=Trim(This.Parent.oContained.w_cigrdfit)
        This.Value = ;
            iif(This.Parent.oContained.w_cigrdfit=='S',1,;
            0)
    Endfunc

    Add Object ocipagfit_3_44 As StdCheck With uid="NWDWWQMMDF",rtseq=63,rtrep=.F.,Left=396, Top=227, Caption="Italic",;
        HelpContextID = 63727771,;
        cFormVar="w_cipagfit", bObbl = .F. , nPag = 3;
        , bGlobalFont=.T.


    Func ocipagfit_3_44.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func ocipagfit_3_44.GetRadio()
        This.Parent.oContained.w_cipagfit = This.RadioValue()
        Return .T.
    Endfunc

    Func ocipagfit_3_44.SetRadio()
        This.Parent.oContained.w_cipagfit=Trim(This.Parent.oContained.w_cipagfit)
        This.Value = ;
            iif(This.Parent.oContained.w_cipagfit=='S',1,;
            0)
    Endfunc

    Add Object ocilblfbo_3_45 As StdCheck With uid="YLOPRWXCNT",rtseq=64,rtrep=.F.,Left=456, Top=57, Caption="Bold",;
        HelpContextID = 247490802,;
        cFormVar="w_cilblfbo", bObbl = .F. , nPag = 3;
        , bGlobalFont=.T.


    Func ocilblfbo_3_45.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func ocilblfbo_3_45.GetRadio()
        This.Parent.oContained.w_cilblfbo = This.RadioValue()
        Return .T.
    Endfunc

    Func ocilblfbo_3_45.SetRadio()
        This.Parent.oContained.w_cilblfbo=Trim(This.Parent.oContained.w_cilblfbo)
        This.Value = ;
            iif(This.Parent.oContained.w_cilblfbo=='S',1,;
            0)
    Endfunc

    Add Object ocitxtfbo_3_46 As StdCheck With uid="JCOBAWCLQV",rtseq=65,rtrep=.F.,Left=456, Top=91, Caption="Bold",;
        HelpContextID = 89680114,;
        cFormVar="w_citxtfbo", bObbl = .F. , nPag = 3;
        , bGlobalFont=.T.


    Func ocitxtfbo_3_46.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func ocitxtfbo_3_46.GetRadio()
        This.Parent.oContained.w_citxtfbo = This.RadioValue()
        Return .T.
    Endfunc

    Func ocitxtfbo_3_46.SetRadio()
        This.Parent.oContained.w_citxtfbo=Trim(This.Parent.oContained.w_citxtfbo)
        This.Value = ;
            iif(This.Parent.oContained.w_citxtfbo=='S',1,;
            0)
    Endfunc

    Add Object ocicbxfbo_3_47 As StdCheck With uid="CAVJHIUWRT",rtseq=66,rtrep=.F.,Left=456, Top=125, Caption="Bold",;
        HelpContextID = 46754034,;
        cFormVar="w_cicbxfbo", bObbl = .F. , nPag = 3;
        , bGlobalFont=.T.


    Func ocicbxfbo_3_47.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func ocicbxfbo_3_47.GetRadio()
        This.Parent.oContained.w_cicbxfbo = This.RadioValue()
        Return .T.
    Endfunc

    Func ocicbxfbo_3_47.SetRadio()
        This.Parent.oContained.w_cicbxfbo=Trim(This.Parent.oContained.w_cicbxfbo)
        This.Value = ;
            iif(This.Parent.oContained.w_cicbxfbo=='S',1,;
            0)
    Endfunc

    Add Object ocibtnfbo_3_48 As StdCheck With uid="JZCIWULNQJ",rtseq=67,rtrep=.F.,Left=456, Top=159, Caption="Bold",;
        HelpContextID = 195717362,;
        cFormVar="w_cibtnfbo", bObbl = .F. , nPag = 3;
        , bGlobalFont=.T.


    Func ocibtnfbo_3_48.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func ocibtnfbo_3_48.GetRadio()
        This.Parent.oContained.w_cibtnfbo = This.RadioValue()
        Return .T.
    Endfunc

    Func ocibtnfbo_3_48.SetRadio()
        This.Parent.oContained.w_cibtnfbo=Trim(This.Parent.oContained.w_cibtnfbo)
        This.Value = ;
            iif(This.Parent.oContained.w_cibtnfbo=='S',1,;
            0)
    Endfunc

    Add Object ocigrdfbo_3_49 As StdCheck With uid="RIPYOINPFE",rtseq=68,rtrep=.F.,Left=456, Top=193, Caption="Bold",;
        HelpContextID = 96823538,;
        cFormVar="w_cigrdfbo", bObbl = .F. , nPag = 3;
        , bGlobalFont=.T.


    Func ocigrdfbo_3_49.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func ocigrdfbo_3_49.GetRadio()
        This.Parent.oContained.w_cigrdfbo = This.RadioValue()
        Return .T.
    Endfunc

    Func ocigrdfbo_3_49.SetRadio()
        This.Parent.oContained.w_cigrdfbo=Trim(This.Parent.oContained.w_cigrdfbo)
        This.Value = ;
            iif(This.Parent.oContained.w_cigrdfbo=='S',1,;
            0)
    Endfunc

    Add Object ocipagfbo_3_50 As StdCheck With uid="PCCERGZQZJ",rtseq=69,rtrep=.F.,Left=456, Top=227, Caption="Bold",;
        HelpContextID = 63727858,;
        cFormVar="w_cipagfbo", bObbl = .F. , nPag = 3;
        , bGlobalFont=.T.


    Func ocipagfbo_3_50.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func ocipagfbo_3_50.GetRadio()
        This.Parent.oContained.w_cipagfbo = This.RadioValue()
        Return .T.
    Endfunc

    Func ocipagfbo_3_50.SetRadio()
        This.Parent.oContained.w_cipagfbo=Trim(This.Parent.oContained.w_cipagfbo)
        This.Value = ;
            iif(This.Parent.oContained.w_cipagfbo=='S',1,;
            0)
    Endfunc

    Add Object oStr_3_7 As StdString With uid="ADMMUSPOPS",Visible=.T., Left=22, Top=57,;
        Alignment=1, Width=100, Height=18,;
        Caption="Font etichette:"  ;
        , bGlobalFont=.T.

    Add Object oStr_3_8 As StdString With uid="BNIFNBVKCO",Visible=.T., Left=22, Top=91,;
        Alignment=1, Width=100, Height=17,;
        Caption="Font campi:"  ;
        , bGlobalFont=.T.

    Add Object oStr_3_9 As StdString With uid="CQKRHDMKUH",Visible=.T., Left=22, Top=159,;
        Alignment=1, Width=100, Height=17,;
        Caption="Font bottoni:"  ;
        , bGlobalFont=.T.

    Add Object oStr_3_10 As StdString With uid="XKMJQKCKEI",Visible=.T., Left=10, Top=125,;
        Alignment=1, Width=112, Height=17,;
        Caption="Font combobox:"  ;
        , bGlobalFont=.T.

    Add Object oStr_3_11 As StdString With uid="UUIYYLBVDV",Visible=.T., Left=22, Top=193,;
        Alignment=1, Width=100, Height=17,;
        Caption="Font zoom:"  ;
        , bGlobalFont=.T.

    Add Object oStr_3_12 As StdString With uid="TNZYLLZWTX",Visible=.T., Left=22, Top=227,;
        Alignment=1, Width=100, Height=17,;
        Caption="Font tab:"  ;
        , bGlobalFont=.T.

    Add Object oStr_3_14 As StdString With uid="EYUYQDCQFF",Visible=.T., Left=280, Top=38,;
        Alignment=1, Width=101, Height=18,;
        Caption="Dimensione"  ;
        , bGlobalFont=.T.
Enddefine
Define Class tcp_setguiPag4 As StdContainer
    Width  = 531
    Height = 363
    stdWidth  = 531
    stdheight = 363
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.


    Add Object oCIDSKMEN_4_1 As StdCombo With uid="SLYGQQBJHE",rtseq=44,rtrep=.F.,Left=238,Top=39,Width=186,Height=19;
        , RowSourceType = 1;
        , ToolTipText = "Se attivo abilita il desktop menu (visualizzabile tramite ctrl+d)";
        , HelpContextID = 14322463;
        , cFormVar="w_CIDSKMEN",RowSource=""+"Abilita desktop menu,"+"Disabilita desktop menu,"+"Apri desktop menu all'ingresso", bObbl = .F. , nPag = 4;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oCIDSKMEN_4_1.RadioValue()
        Return(Iif(This.Value =1,'S',;
            iif(This.Value =2,'H',;
            iif(This.Value =3,'O',;
            space(1)))))
    Endfunc
    Func oCIDSKMEN_4_1.GetRadio()
        This.Parent.oContained.w_CIDSKMEN = This.RadioValue()
        Return .T.
    Endfunc

    Func oCIDSKMEN_4_1.SetRadio()
        This.Parent.oContained.w_CIDSKMEN=Trim(This.Parent.oContained.w_CIDSKMEN)
        This.Value = ;
            iif(This.Parent.oContained.w_CIDSKMEN=='S',1,;
            iif(This.Parent.oContained.w_CIDSKMEN=='H',2,;
            iif(This.Parent.oContained.w_CIDSKMEN=='O',3,;
            0)))
    Endfunc

    Func oCIDSKMEN_4_1.mCond()
        With This.Parent.oContained
            Return (.w_CIVTHEME <> -1)
        Endwith
    Endfunc


    Add Object oCINAVSTA_4_2 As StdCombo With uid="QUKGWVIOIL",rtseq=45,rtrep=.F.,Left=238,Top=68,Width=186,Height=19;
        , RowSourceType = 1;
        , ToolTipText = "Stato del DeskMenu all'avvio";
        , HelpContextID = 152007712;
        , cFormVar="w_CINAVSTA",RowSource=""+"Aperta,"+"Chiusa", bObbl = .F. , nPag = 4;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oCINAVSTA_4_2.RadioValue()
        Return(Iif(This.Value =1,'A',;
            iif(This.Value =2,'C',;
            space(1))))
    Endfunc
    Func oCINAVSTA_4_2.GetRadio()
        This.Parent.oContained.w_CINAVSTA = This.RadioValue()
        Return .T.
    Endfunc

    Func oCINAVSTA_4_2.SetRadio()
        This.Parent.oContained.w_CINAVSTA=Trim(This.Parent.oContained.w_CINAVSTA)
        This.Value = ;
            iif(This.Parent.oContained.w_CINAVSTA=='A',1,;
            iif(This.Parent.oContained.w_CINAVSTA=='C',2,;
            0))
    Endfunc


    Add Object oCINAVNUB_4_3 As StdCombo With uid="AJAKYKZJWK",rtseq=46,rtrep=.F.,Left=239,Top=97,Width=62,Height=19;
        , RowSourceType = 1;
        , ToolTipText = "Importa il numero massimo di pulsanti visualizzati dal desktop menu";
        , HelpContextID = 116427727;
        , cFormVar="w_CINAVNUB",RowSource=""+"1,"+"2,"+"3,"+"4,"+"5,"+"6,"+"7,"+"8,"+"9,"+"10,"+"11,"+"12", bObbl = .F. , nPag = 4;
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Func oCINAVNUB_4_3.RadioValue()
        Return(Iif(This.Value =1,1,;
            iif(This.Value =2,2,;
            iif(This.Value =3,3,;
            iif(This.Value =4,4,;
            iif(This.Value =5,5,;
            iif(This.Value =6,6,;
            iif(This.Value =7,7,;
            iif(This.Value =8,8,;
            iif(This.Value =9,9,;
            iif(This.Value =10,10,;
            iif(This.Value =11,11,;
            iif(This.Value =12,12,;
            0)))))))))))))
    Endfunc
    Func oCINAVNUB_4_3.GetRadio()
        This.Parent.oContained.w_CINAVNUB = This.RadioValue()
        Return .T.
    Endfunc

    Func oCINAVNUB_4_3.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_CINAVNUB==1,1,;
            iif(This.Parent.oContained.w_CINAVNUB==2,2,;
            iif(This.Parent.oContained.w_CINAVNUB==3,3,;
            iif(This.Parent.oContained.w_CINAVNUB==4,4,;
            iif(This.Parent.oContained.w_CINAVNUB==5,5,;
            iif(This.Parent.oContained.w_CINAVNUB==6,6,;
            iif(This.Parent.oContained.w_CINAVNUB==7,7,;
            iif(This.Parent.oContained.w_CINAVNUB==8,8,;
            iif(This.Parent.oContained.w_CINAVNUB==9,9,;
            iif(This.Parent.oContained.w_CINAVNUB==10,10,;
            iif(This.Parent.oContained.w_CINAVNUB==11,11,;
            iif(This.Parent.oContained.w_CINAVNUB==12,12,;
            0))))))))))))
    Endfunc

    Add Object oCINAVDIM_4_4 As StdField With uid="MUFZHXFLFE",rtseq=47,rtrep=.F.,;
        cFormVar = "w_CINAVDIM", cQueryName = "CINAVDIM",;
        bObbl = .F. , nPag = 4, Value=0, bMultilanguage =  .F.,;
        ToolTipText = "Dimensione iniziale in pixel del desktop menu",;
        HelpContextID = 152007893,;
        bGlobalFont=.T.,;
        Height=21, Width=83, Left=239, Top=127, cSayPict='"9999999999"', cGetPict='"9999999999"'


    Add Object oCIMNAFNM_4_5 As StdTableComboFont With uid="VDGVFJSWLZ",rtseq=48,rtrep=.F.,Left=168,Top=184,Width=164,Height=19;
        , ToolTipText = "Font da utilizzare per la sezione menu navigator";
        , HelpContextID = 81687770;
        , cFormVar="w_CIMNAFNM",tablefilter="", bObbl = .F. , nPag = 4;
        , cTable='',cKey='',cValue='',cOrderBy='',xDefault=Space(50);
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Add Object oCIMNAFSZ_4_6 As StdField With uid="BFGXYNJXAE",rtseq=49,rtrep=.F.,;
        cFormVar = "w_CIMNAFSZ", cQueryName = "CIMNAFSZ",;
        bObbl = .F. , nPag = 4, Value=0, bMultilanguage =  .F.,;
        sErrorMsg = "Dimensione del font non supportata",;
        ToolTipText = "Dimensione font per la sezione menu navigator",;
        HelpContextID = 81687983,;
        bGlobalFont=.T.,;
        Height=21, Width=76, Left=444, Top=184, cSayPict='"999"', cGetPict='"999"'

    Func oCIMNAFSZ_4_6.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            bRes = (!Empty(.w_CIMNAFNM) And TestFont('L', .w_CIMNAFNM, .w_CIMNAFSZ)<>-1 And !Empty(.w_CIMNAFSZ))
            If bRes And !(TestFont('L', .w_CIMNAFNM, .w_CIMNAFSZ) = 1)
                bRes=(Messagebox(cp_Translate(Thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente"))+". "+cp_Translate(MSG_ACCEPT_ANYWAY),4+32,"")=6)
                This.Parent.oContained.bDontReportError=!bRes
            Endif
        Endwith
        Return bRes
    Endfunc


    Add Object oCIWMAFNM_4_7 As StdTableComboFont With uid="CEJELALOQK",rtseq=50,rtrep=.F.,Left=168,Top=218,Width=164,Height=19;
        , ToolTipText = "Font da utilizzare per la sezione Windows manager";
        , HelpContextID = 81294554;
        , cFormVar="w_CIWMAFNM",tablefilter="", bObbl = .F. , nPag = 4;
        , cTable='',cKey='',cValue='',cOrderBy='',xDefault=Space(50);
        , FontName = "Arial", FontSize = 8, FontBold = .F., FontItalic=.T., FontUnderline=.F., FontStrikethru=.F.

    Add Object oCIWMAFSZ_4_8 As StdField With uid="LNZPGHJJVM",rtseq=51,rtrep=.F.,;
        cFormVar = "w_CIWMAFSZ", cQueryName = "CIWMAFSZ",;
        bObbl = .F. , nPag = 4, Value=0, bMultilanguage =  .F.,;
        sErrorMsg = "Dimensione del font non supportata",;
        ToolTipText = "Dimensione font per la sezione Windows manager",;
        HelpContextID = 81294767,;
        bGlobalFont=.T.,;
        Height=21, Width=76, Left=444, Top=218, cSayPict='"999"', cGetPict='"999"'

    Func oCIWMAFSZ_4_8.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            bRes = (!Empty(.w_CIWMAFNM) And TestFont('L', .w_CIWMAFNM, .w_CIWMAFSZ)<>-1 And !Empty(.w_CIWMAFSZ))
            If bRes And !(TestFont('L', .w_CIWMAFNM, .w_CIWMAFSZ) = 1)
                bRes=(Messagebox(cp_Translate(Thisform.msgFmt("Alcune informazioni potrebbero non essere visualizzate correttamente"))+". "+cp_Translate(MSG_ACCEPT_ANYWAY),4+32,"")=6)
                This.Parent.oContained.bDontReportError=!bRes
            Endif
        Endwith
        Return bRes
    Endfunc


    Add Object oObj_4_17 As cp_setCtrlObjprop With uid="JCLGXQXYZA",Left=553, Top=37, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Desktop men�:",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_18 As cp_setCtrlObjprop With uid="DWHFTOLGSG",Left=553, Top=69, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Stato Desktop menu all'avvio:",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_19 As cp_setCtrlObjprop With uid="QYHFNOKUDE",Left=553, Top=100, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Numero di bottoni visualizzabili:",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_20 As cp_setCtrlObjprop With uid="KZRRRZPBOU",Left=553, Top=129, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Dimensione iniziale desktop menu:",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_21 As cp_setCtrlObjprop With uid="EHWSUFKYBO",Left=553, Top=184, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Font menu navigator:",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_22 As cp_setCtrlObjprop With uid="HGYCWMGPUG",Left=906, Top=184, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Dimensione:",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_23 As cp_setCtrlObjprop With uid="AYTLKQLVQP",Left=906, Top=218, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Dimensione:",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_24 As cp_setCtrlObjprop With uid="WEXSRJXEXQ",Left=553, Top=217, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Font windows manager:",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_25 As cp_setobjprop With uid="JCDJAXNMHH",Left=667, Top=37, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIDSKMEN",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_26 As cp_setobjprop With uid="EHRUQXQPWU",Left=667, Top=68, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CINAVSTA",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_27 As cp_setobjprop With uid="UBOXHLHCUC",Left=667, Top=97, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CINAVNUB",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_28 As cp_setobjprop With uid="UJELONDHMM",Left=667, Top=127, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CINAVDIM",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_29 As cp_setobjprop With uid="TCTABLVLRN",Left=667, Top=184, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIMNAFNM",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_30 As cp_setobjprop With uid="SKQFBTJCXH",Left=667, Top=218, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIWMAFNM",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_31 As cp_setobjprop With uid="MWCNVEAGCR",Left=1021, Top=184, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIMNAFSZ",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_32 As cp_setobjprop With uid="MNPOQPMLDY",Left=1021, Top=218, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_CIWMAFSZ",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_33 As cp_setobjprop With uid="QCZBORRIKB",Left=786, Top=37, Width=107,Height=18,;
        caption='Object',;
        cProp="RowSource",cObj="w_CIDSKMEN",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.



    Add Object oObj_4_34 As cp_setobjprop With uid="QLGPJBERJD",Left=786, Top=68, Width=107,Height=18,;
        caption='Object',;
        cProp="RowSource",cObj="w_CINAVSTA",;
        cEvent = "Init",;
        nPag=4;
        , HelpContextID = 141364156;
        , bGlobalFont=.T.


    Add Object oStr_4_9 As StdString With uid="RBCVPKTCZB",Visible=.T., Left=7, Top=38,;
        Alignment=1, Width=226, Height=18,;
        Caption="Desktop men�:"  ;
        , bGlobalFont=.T.

    Add Object oStr_4_10 As StdString With uid="JMOSAGWQOA",Visible=.T., Left=4, Top=184,;
        Alignment=1, Width=159, Height=18,;
        Caption="Font menu navigator:"  ;
        , bGlobalFont=.T.

    Add Object oStr_4_11 As StdString With uid="UBKQDBPFVW",Visible=.T., Left=4, Top=217,;
        Alignment=1, Width=159, Height=18,;
        Caption="Font windows manager:"  ;
        , bGlobalFont=.T.

    Add Object oStr_4_12 As StdString With uid="YVNYKNFGIQ",Visible=.T., Left=336, Top=184,;
        Alignment=1, Width=101, Height=18,;
        Caption="Dimensione:"  ;
        , bGlobalFont=.T.

    Add Object oStr_4_13 As StdString With uid="ESWVGHVNDV",Visible=.T., Left=336, Top=218,;
        Alignment=1, Width=101, Height=18,;
        Caption="Dimensione:"  ;
        , bGlobalFont=.T.

    Add Object oStr_4_14 As StdString With uid="UZCLKWTABU",Visible=.T., Left=7, Top=100,;
        Alignment=1, Width=226, Height=18,;
        Caption="Numero di bottoni visualizzabili:"  ;
        , bGlobalFont=.T.

    Add Object oStr_4_15 As StdString With uid="VRGRVVMDTK",Visible=.T., Left=7, Top=69,;
        Alignment=1, Width=226, Height=18,;
        Caption="Stato Desktop menu all'avvio:"  ;
        , bGlobalFont=.T.

    Add Object oStr_4_16 As StdString With uid="HQOOUXWMFP",Visible=.T., Left=7, Top=129,;
        Alignment=1, Width=226, Height=18,;
        Caption="Dimensione iniziale desktop menu:"  ;
        , bGlobalFont=.T.
Enddefine
Define Class tcp_setguiPag5 As StdContainer
    Width  = 531
    Height = 363
    stdWidth  = 531
    stdheight = 363
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.

    Add Object oCICTRGRD_5_1 As StdCheck With uid="HWJBNMSVRX",rtseq=70,rtrep=.F.,Left=37, Top=49, Caption="Intestazione griglia avanzata",;
        ToolTipText = "Se attivo, visualizza il controllo avanzato nell'intestazione degli zoom",;
        HelpContextID = 164334514,;
        cFormVar="w_CICTRGRD", bObbl = .F. , nPag = 5;
        , bGlobalFont=.T.


    Func oCICTRGRD_5_1.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func oCICTRGRD_5_1.GetRadio()
        This.Parent.oContained.w_CICTRGRD = This.RadioValue()
        Return .T.
    Endfunc

    Func oCICTRGRD_5_1.SetRadio()
        This.Parent.oContained.w_CICTRGRD=Trim(This.Parent.oContained.w_CICTRGRD)
        This.Value = ;
            iif(This.Parent.oContained.w_CICTRGRD=='S',1,;
            0)
    Endfunc

    Func oCICTRGRD_5_1.mCond()
        With This.Parent.oContained
            Return (.w_CIVTHEME<>-1)
        Endwith
    Endfunc

    Add Object oCIMRKGRD_5_2 As StdCheck With uid="FPPQZCEDUQ",rtseq=71,rtrep=.F.,Left=37, Top=72, Caption="Puntatore del record",;
        ToolTipText = "Se attivo, visualizza il puntatore del record negli oggetti di tipo elenco e griglie di dati",;
        HelpContextID = 253654094,;
        cFormVar="w_CIMRKGRD", bObbl = .F. , nPag = 5;
        , bGlobalFont=.T.


    Func oCIMRKGRD_5_2.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func oCIMRKGRD_5_2.GetRadio()
        This.Parent.oContained.w_CIMRKGRD = This.RadioValue()
        Return .T.
    Endfunc

    Func oCIMRKGRD_5_2.SetRadio()
        This.Parent.oContained.w_CIMRKGRD=Trim(This.Parent.oContained.w_CIMRKGRD)
        This.Value = ;
            iif(This.Parent.oContained.w_CIMRKGRD=='S',1,;
            0)
    Endfunc

    Add Object oCIAZOOML_5_3 As StdCheck With uid="YDTKOZZYEN",rtseq=72,rtrep=.F.,Left=37, Top=95, Caption="Visualizza record selezionato",;
        ToolTipText = "Se attivo, il record selezionato viene visualizzato quando ci si sposta dalla pagina elenco",;
        HelpContextID = 59929801,;
        cFormVar="w_CIAZOOML", bObbl = .F. , nPag = 5;
        , bGlobalFont=.T.


    Func oCIAZOOML_5_3.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func oCIAZOOML_5_3.GetRadio()
        This.Parent.oContained.w_CIAZOOML = This.RadioValue()
        Return .T.
    Endfunc

    Func oCIAZOOML_5_3.SetRadio()
        This.Parent.oContained.w_CIAZOOML=Trim(This.Parent.oContained.w_CIAZOOML)
        This.Value = ;
            iif(This.Parent.oContained.w_CIAZOOML=='S',1,;
            0)
    Endfunc

    Add Object oCIZOESPR_5_4 As StdCheck With uid="UVIIICGFIG",rtseq=73,rtrep=.F.,Left=37, Top=118, Caption="Espandi gestione parametri zoom",;
        ToolTipText = "Espandi gestione parametri negli zoom",;
        HelpContextID = 150697260,;
        cFormVar="w_CIZOESPR", bObbl = .F. , nPag = 5;
        , bGlobalFont=.T.


    Func oCIZOESPR_5_4.RadioValue()
        Return(Iif(This.Value =1,'S',;
            space(1)))
    Endfunc
    Func oCIZOESPR_5_4.GetRadio()
        This.Parent.oContained.w_CIZOESPR = This.RadioValue()
        Return .T.
    Endfunc

    Func oCIZOESPR_5_4.SetRadio()
        This.Parent.oContained.w_CIZOESPR=Trim(This.Parent.oContained.w_CIZOESPR)
        This.Value = ;
            iif(This.Parent.oContained.w_CIZOESPR=='S',1,;
            0)
    Endfunc

    Func oCIZOESPR_5_4.mCond()
        With This.Parent.oContained
            Return (.w_CICTRGRD='S')
        Endwith
    Endfunc

    Add Object oCIHEHEZO_5_5 As StdField With uid="QDOCYNHTLQ",rtseq=74,rtrep=.F.,;
        cFormVar = "w_CIHEHEZO", cQueryName = "CIHEHEZO",;
        bObbl = .F. , nPag = 5, Value=0, bMultilanguage =  .F.,;
        sErrorMsg = "Inserire l'altezza della testata dello zoom maggiore di zero",;
        ToolTipText = "Altezza della testata degli zoom",;
        HelpContextID = 189363462,;
        bGlobalFont=.T.,;
        Height=21, Width=34, Left=431, Top=51

    Func oCIHEHEZO_5_5.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            bRes = (.w_CIHEHEZO > 0)
        Endwith
        Return bRes
    Endfunc

    Add Object oCIHEROZO_5_6 As StdField With uid="BMELVTMRQG",rtseq=75,rtrep=.F.,;
        cFormVar = "w_CIHEROZO", cQueryName = "CIHEROZO",;
        bObbl = .F. , nPag = 5, Value=0, bMultilanguage =  .F.,;
        sErrorMsg = "Inserire l'altezza della righe dello zoom maggiore di zero",;
        ToolTipText = "Altezza righe dello zoom",;
        HelpContextID = 88700166,;
        bGlobalFont=.T.,;
        Height=21, Width=34, Left=431, Top=75

    Func oCIHEROZO_5_6.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            bRes = (.w_CIHEROZO > 0)
        Endwith
        Return bRes
    Endfunc


    Add Object oObj_5_11 As cp_setobjprop With uid="KDVTBDTSZF",Left=553, Top=18, Width=189,Height=18,;
        caption='Intestazione griglia avanzata',;
        cProp="caption",cObj="w_CICTRGRD",;
        cEvent = "Init",;
        nPag=5;
        , HelpContextID = 26842811;
        , bGlobalFont=.T.



    Add Object oObj_5_12 As cp_setobjprop With uid="RXQFJVRUNI",Left=553, Top=35, Width=189,Height=18,;
        caption='Intestazione griglia avanzata',;
        cProp="tooltiptext",cObj="w_CICTRGRD",;
        cEvent = "Init",;
        nPag=5;
        , HelpContextID = 26842811;
        , bGlobalFont=.T.



    Add Object oObj_5_13 As cp_setobjprop With uid="GMXJXNFRNC",Left=553, Top=58, Width=189,Height=18,;
        caption='Puntatore del record',;
        cProp="tooltiptext",cObj="w_CIMRKGRD",;
        cEvent = "Init",;
        nPag=5;
        , HelpContextID = 254672115;
        , bGlobalFont=.T.



    Add Object oObj_5_14 As cp_setobjprop With uid="OMNSWJZCEO",Left=553, Top=75, Width=189,Height=18,;
        caption='Puntatore del record',;
        cProp="caption",cObj="w_CIMRKGRD",;
        cEvent = "Init",;
        nPag=5;
        , HelpContextID = 254672115;
        , bGlobalFont=.T.



    Add Object oObj_5_15 As cp_setCtrlObjprop With uid="CUUAPQQJEH",Left=553, Top=98, Width=189,Height=18,;
        caption='Altezza testata zoom',;
        cProp="Caption",cObj="Altezza testata zoom:",;
        cEvent = "Init",;
        nPag=5;
        , HelpContextID = 8925058;
        , bGlobalFont=.T.



    Add Object oObj_5_16 As cp_setCtrlObjprop With uid="RLYPIYICVJ",Left=553, Top=115, Width=189,Height=18,;
        caption='Altezza righe zoom',;
        cProp="Caption",cObj="Altezza righe zoom:",;
        cEvent = "Init",;
        nPag=5;
        , HelpContextID = 191686791;
        , bGlobalFont=.T.



    Add Object oObj_5_17 As cp_setobjprop With uid="CBCBTFWVRS",Left=553, Top=143, Width=189,Height=18,;
        caption='Visualizza record selezionato',;
        cProp="caption",cObj="w_CIAZOOML",;
        cEvent = "Init",;
        nPag=5;
        , HelpContextID = 47524258;
        , bGlobalFont=.T.



    Add Object oObj_5_18 As cp_setobjprop With uid="XKRSQROKSK",Left=553, Top=160, Width=189,Height=18,;
        caption='Visualizza record selezionato',;
        cProp="tooltiptext",cObj="w_CIAZOOML",;
        cEvent = "Init",;
        nPag=5;
        , HelpContextID = 47524258;
        , bGlobalFont=.T.


    Add Object oStr_5_7 As StdString With uid="HPDLTQKQXB",Visible=.T., Left=33, Top=24,;
        Alignment=0, Width=124, Height=18,;
        Caption="Elenchi / zoom"  ;
        , FontName = "Arial", FontSize = 9, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_5_9 As StdString With uid="VAAORWAKSA",Visible=.T., Left=276, Top=77,;
        Alignment=1, Width=152, Height=18,;
        Caption="Altezza righe zoom:"  ;
        , bGlobalFont=.T.

    Add Object oStr_5_10 As StdString With uid="FTZZWCWVAJ",Visible=.T., Left=276, Top=53,;
        Alignment=1, Width=152, Height=18,;
        Caption="Altezza testata zoom:"  ;
        , bGlobalFont=.T.

    Add Object oBox_5_8 As StdBox With uid="CCNQLMEQDJ",Left=14, Top=41, Width=223,Height=1
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_setgui','cpsetgui','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +" "+i_cAliasName2+".usrcode=cpsetgui.usrcode";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Master File"
Endproc

* --- Area Manuale = Functions & Procedures
* --- cp_setgui
Define Class StdTableComboFont As StdTableCombo
    Proc Init
        If Vartype(This.bNoBackColor)='U'
            This.BackColor = i_nEBackColor
        Endif
        This.ToolTipText=cp_Translate(This.ToolTipText)
        Local l_numf, l_i
        Afont(This.combovalues)
        This.nValues = Alen(This.combovalues)
        l_i = 1
        Do While l_i <= This.nValues
            This.AddItem(This.combovalues[l_i])
            l_i = l_i + 1
        Enddo
        This.SetFont()
    Endproc
Enddefine
* --- Fine Area Manuale
