* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_showdbf                                                      *
*              Mostra il risultato                                             *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-02                                                      *
* Last revis.: 2009-10-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
Return(Createobject("tcp_showdbf",oParentObject))

* --- Class definition
Define Class tcp_showdbf As StdForm
    Top    = 10
    Left   = 10

    * --- Standard Properties
    Width  = 711
    Height = 577
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2009-10-20"
    HelpContextID=27288283
    max_rt_seq=1

    * --- Constant Properties
    _IDX = 0
    cPrg = "cp_showdbf"
    cComment = "Mostra il risultato"
    oParentObject = .Null.
    Icon = "mask.ico"
    *closable = .f.

    * --- Local Variables
    w_ALCMDSQL = Space(0)
    w_BrowseOBJ = .Null.
    * --- Area Manuale = Declare Variables
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=1, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tcp_showdbfPag1","cp_showdbf",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate("Pag.1")
            .Pages(1).oPag.oContained=Thisform
        Endwith
        This.Parent.oFirstControl = .Null.
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
        * --- Area Manuale = Init Page Frame
        * --- Fine Area Manuale
    Endproc
    Proc Init(oParentObject)
        If Vartype(m.oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        This.w_BrowseOBJ = This.oPgFrm.Pages(1).oPag.BrowseOBJ
        DoDefault()
    Proc Destroy()
        This.w_BrowseOBJ = .Null.
        DoDefault()

        * --- Open tables
    Function OpenWorkTables()
        Return(This.OpenAllTables(0))

    Procedure SetPostItConn()
        Return


        * --- Read record and initialize Form variables
    Procedure LoadRec()
    Endproc

    * --- ecpQuery
    Procedure ecpQuery()
        This.BlankRec()
        This.cFunction="Edit"
        This.SetStatus()
        If This.cFunction="Edit"
            This.mEnableControls()
        Endif
    Endproc
    * --- Esc
    Proc ecpQuit()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            This.cFunction = "Edit"
            This.oFirstControl.SetFocus()
            Return
        Endif
        * ---
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Endproc
    Proc QueryUnload()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            Nodefault
            Return
        Endif
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Proc ecpSave()
        If This.TerminateEdit() And This.CheckForm()
            This.LockScreen=.T.
            This.mReplace(.T.)
            This.LockScreen=.F.
            This.NotifyEvent("Done")
            This.Release()
        Endif
    Endproc
    Func OkToQuit()
        Return .T.
    Endfunc

    * --- Blank Form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        With This
            .w_ALCMDSQL=Space(0)
            .oPgFrm.Page1.oPag.BrowseOBJ.Calculate()
        Endwith
        This.DoRTCalc(1,1,.F.)
        This.SaveDependsOn()
        This.SetControlsValue()
        This.mHideControls()
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = Setting operation
    *     Allowed Parameters: Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        * --- Deactivating List page when <> da Query
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt
        i_cFlt =""
        Return (i_cFlt)
    Endfunc

    Proc QueryKeySet(i_cWhere,i_cOrderBy)
    Endproc

    Proc SelectCursor
    Proc GetXKey

        * --- Update Database
    Function mReplace(i_bEditing)
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        This.NotifyEvent('Update start')
        With This
        Endwith
        This.NotifyEvent('Update end')
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(.T.)

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        If i_bUpd
            With This
                .oPgFrm.Page1.oPag.BrowseOBJ.Calculate()
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
            Endwith
            This.DoRTCalc(1,1,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
            .oPgFrm.Page1.oPag.BrowseOBJ.Calculate()
        Endwith
        Return


        * --- Enable controls under condition
    Procedure mEnableControls()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        DoDefault()
        Return

        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            .oPgFrm.Page1.oPag.BrowseOBJ.Event(cEvent)
        Endwith
        * --- Area Manuale = Notify Event End
        * --- cp_showdbf
        If Upper( cEvent ) = "INIT"
            This.w_ALCMDSQL = This.oParentObject.oParentObject.w_ALCMDSQL
            This.w_BrowseOBJ.cCURSOR = This.oParentObject.w_SHOWDBF_CURS
            This.w_BrowseOBJ.Browse()
            This.SetControlsValue()
        Endif

        If Upper( cEvent ) = "DONE" And Used( This.oParentObject.w_SHOWDBF_CURS )
            Select( This.oParentObject.w_SHOWDBF_CURS )
            Use
        Endif
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

    Function SetControlsValue()
        If Not(This.oPgFrm.Page1.oPag.oALCMDSQL_1_3.Value==This.w_ALCMDSQL)
            This.oPgFrm.Page1.oPag.oALCMDSQL_1_3.Value=This.w_ALCMDSQL
        Endif
        cp_SetControlsValueExtFlds(This,'')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
            Else
                If Not(i_bnoChk)
                    cp_ErrorMsg(i_cErrorMsg)
                Endif
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

Enddefine

* --- Define pages as container
Define Class tcp_showdbfPag1 As StdContainer
    Width  = 707
    Height = 577
    stdWidth  = 707
    stdheight = 577
    resizeXpos=315
    resizeYpos=330
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.


    Add Object BrowseOBJ As cp_CBrowse With uid="LYNWLXXRLE",Left=6, Top=100, Width=692,Height=472,;
        caption='BrowseOBJ',;
        nPag=1;
        , HelpContextID = 214631290;
        , bGlobalFont=.T.


    Add Object oALCMDSQL_1_3 As StdMemo With uid="ZWWLMXPBDG",rtseq=1,rtrep=.F.,;
        cFormVar = "w_ALCMDSQL", cQueryName = "ALCMDSQL",Enabled=.F.,;
        bObbl = .F. , nPag = 1, Value=Space(0), bMultilanguage =  .F.,;
        HelpContextID = 266362248,;
        bGlobalFont=.T.,;
        Height=75, Width=631, Left=4, Top=20, ReadOnly=.T., Enabled=.T.


    Add Object oBtn_1_4 As StdButton With uid="PTAFAFGHOT",Left=651, Top=21, Width=48,Height=45,;
        Picture="BMP\ESC.BMP", Caption="", nPag=1;
        , ToolTipText = "Chiude la maschera";
        , HelpContextID = 235359269;
        , Caption='\<Esci';
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_4.Click()
        =cp_StandardFunction(This,"Quit")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object oStr_1_2 As StdString With uid="EWTEYNTLSQ",Visible=.T., Left=12, Top=5,;
        Alignment=0, Width=133, Height=18,;
        Caption="Comando SQL"  ;
        , bGlobalFont=.T.
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_showdbf','','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Dialog Window"
Endproc

* --- Area Manuale = Functions & Procedures
* --- cp_showdbf
Define Class cp_CBrowse As Grid

    cCURSOR=""

    * Standard
    RecordSource=""
    RecordSourceType=1   && ALIAS
    ColumnCount=0
    ReadOnly=.T.
    DeleteMark=.F.

    Proc Calculate(xValue)
    Endproc

    Proc Event(cEvent)
        If cEvent="Browse"
            This.Browse()
        Endif
    Endproc

    Proc Browse
        Local nI,cI,oCol
        With This
            .RecordSource=''
            .ColumnCount=0
            If !Empty(.cCURSOR)
                .ColumnCount=Fcount(.cCURSOR)
                .RecordSource=.cCURSOR
                For nI=1 To .ColumnCount
                    cI=Alltrim(Str(nI))
                    oCol=.Columns(nI)
                    oCol.Header1.Caption=Field(nI,.cCURSOR)
                    oCol.Bound=.F.
                    oCol.ReadOnly=.ReadOnly
                    oCol.ControlSource=Alltrim(.cCURSOR)+'.'+Field(nI,.cCURSOR)
                    oCol.SelectOnEntry=.F.
                Next
            Endif
        Endwith
        Grid::Refresh()
    Endproc

Enddefine
* --- Fine Area Manuale
