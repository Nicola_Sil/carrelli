* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_suppmenu                                                     *
*              Gestione eventi treeview menu                                   *
*                                                                              *
*      Author: GiorGio Montali                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language: Visual Fox Pro                                                  *
*          OS: Windows                                                         *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-18                                                      *
* Last revis.: 2008-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject,Event,cText,tipo,bBtnClick,cPosizione
* --- Area Manuale = Header
* --- Fine Area Manuale
Private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
Createobject("tcp_suppmenu",oParentObject,m.Event,m.cText,m.tipo,m.bBtnClick,m.cPosizione)
Return(i_retval)

Define Class tcp_suppmenu As StdBatch
    * --- Local variables
    Event = Space(10)
    cText = Space(150)
    tipo = 0
    bBtnClick = .F.
    cPosizione = Space(1)
    indexTemp = 0
    w_SYSKEY = Space(254)
    w_sysIndex = 0
    oldLevel = 0
    w_SEQUENZA = Space(5)
    padreNodo = .Null.
    oldInsert = Space(254)
    w_OINDEX = 0
    w_BMPINDEX = 0
    SysType = 0
    w_Obj_Pos = .Null.
    bitCanc = Space(254)
    w_PADRE = .Null.
    w_CMENUFILE = Space(254)
    w_TempCursor = Space(10)
    w_OBJ = .Null.
    w_TOOLOBJ = .Null.
    w_MUTENTE = Space(25)
    w_FILENAME = Space(25)
    w_TREESX = Space(10)
    w_CURSAPP = Space(10)
    * --- WorkFile variables
    runtime_filters = 1

    Procedure Pag1
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        * --- Gestisce gli eventi che avvengono sulle due Treeview dei menu
        *     Tutte le variabili che hanno suffisso SYS si riferiscono alla Treeview del menu di sistema.
        *     Tutte le variabili che hanno suffisso USR si riferiscono alla TreeView del menu utente.
        * --- La prima pagina condiziona gli eventi e rimanda alle relative pagine di gestione
        * --- Variabile per determinare se il batch � stato lanciato premendo uno dei bottoncini del menu du destra
        * --- Variabile per determinare se la voce deve essere inserita Prima (P), Dopo (D) o occorre
        *     inserire il flag Cancellato (C)
        Do Case
            Case This.Event=="SYSCLICK"
                * --- Memorizzo l'indice del nodo selezionato nella TreeView di destra
                This.w_sysIndex = This.oParentObject.w_SYSTreeView.oTree.SelectedItem.Index
                * --- Controllo che il nodo selezionato a destra, se � un separatore, non venga
                *     direttamente attaccato al "Menu Principale"
                This.w_SYSKEY = This.oParentObject.w_SYSTreeView.oTree.nodes(This.w_sysIndex).Key
                Select (This.oParentObject.w_SYSCURSOR)
                Seek Alltrim(Substr(This.w_SYSKEY,2))
                If Found() And Directory=4 And This.oParentObject.w_directory=0
                    Return
                Endif
                * --- Metto in un cursore d'appoggio le voci che dovr� copiare a sinistra

                #If Version(5)>=700
                    Select * From ( This.oParentObject.w_SYSTreeView.cCursor ) Where 1=0 Into Cursor Appo noFilter Readwrite
                #Else
                    Local w_nome
                    w_nome = Sys(2015)
                    Select * From ( This.oParentObject.w_SYSTreeView.cCursor ) Into Cursor (w_nome) Where 1=0 noFilter
                    Use Dbf() Again In 0 Alias "Appo"
                    Use
                    Select ( "Appo" )
                #Endif
                This.oParentObject.w_SYSTreeView.oTree.nodes(This.w_sysIndex).expanded = .F.
                This.oParentObject.w_SYSTreeView.contatore = 1
                This.oParentObject.w_SYSTreeView.rifpadre = -1
                This.oParentObject.w_SYSTreeView.visita(This.oParentObject.w_SYSTreeView.oTree.nodes(This.w_sysIndex),"appo",0)
                * --- Mantengo l'indice originale del men� di sinistra per espanderlo al termine
                This.w_OINDEX = This.oParentObject.w_INDEX
                Select ("appo")
                Go Top
                Scan
                    * --- Doppio click sul menu principale, skippo la prima voce...
                    If Directory=0
                        Skip 1
                    Endif
                    This.w_BMPINDEX = This.oParentObject.w_USRTreeView.GetBitmapIndex(Appo.CPBmpName)
                    If Recno("Appo")=1
                        If This.oParentObject.w_directory=2 Or This.oParentObject.w_directory=0
                            * --- Inseriamo la nuova voce all'interno del submenu sul quale siamo posizionati
                            *     (il valore 4 indica "figlio di " w_INDEX)
                            This.oParentObject.w_USRTreeView.oTree.nodes.Add(This.oParentObject.w_INDEX,4,,Alltrim(Appo.VOCEMENU),This.w_BMPINDEX)
                        Else
                            * --- Inseriamo la nuova voce subito dopo il menu sul quale siamo posizionati
                            *     (il valore 2 indica "successivo a " w_INDEX)
                            This.oParentObject.w_USRTreeView.oTree.nodes.Add(This.oParentObject.w_INDEX,2,,Alltrim(Appo.VOCEMENU),This.w_BMPINDEX)
                        Endif
                    Else
                        If Level=This.oldLevel
                            * --- Se il livello del nodo che devo inserire � uguale al nodo precedente, inserisco il nodo subito dopo
                            This.oParentObject.w_USRTreeView.oTree.nodes.Add(This.oParentObject.w_INDEX,2,,Alltrim(Appo.VOCEMENU),This.w_BMPINDEX)
                        Else
                            If Level>This.oldLevel
                                * --- Se il livello del nodo che devo inserire � maggiore del nodo precedente, inserisco il nodo all'interno dello stesso
                                This.oParentObject.w_USRTreeView.oTree.nodes.Add(This.oParentObject.w_INDEX,4,,Alltrim(Appo.VOCEMENU),This.w_BMPINDEX)
                            Else
                                * --- Calcolo l'index del nodo all'interno del quale devo inserire la voce
                                Macro="this.oParentObject.w_USRTreeView.oTree.nodes(this.oParentObject.w_INDEX).parent"
                                This.padreNodo = &Macro
                                k=This.oldLevel-Level
                                Do While k>0
                                    This.padreNodo = This.padreNodo.Parent
                                    k=k-1
                                Enddo
                                This.oParentObject.w_INDEX = This.padreNodo.Index
                                This.oParentObject.w_USRTreeView.oTree.nodes.Add(This.oParentObject.w_INDEX,4,,Alltrim(Appo.VOCEMENU),This.w_BMPINDEX)
                            Endif
                        Endif
                    Endif
                    * --- tengo aggiornato oldLevel e la chiave
                    This.oldLevel = Level
                    * --- Portiamo l'indice sul nodo appena creato
                    This.oParentObject.w_INDEX = This.oParentObject.w_USRTreeView.oTree.nodes.Count()
                    * --- Salviamo il Tag della TreeView
                    This.oParentObject.w_USRTreeView.SetTag(This.oParentObject.w_USRTreeView.oTree.nodes( This.oParentObject.w_INDEX ) , Appo.NAMEPROC , Appo.Directory , Appo.Enabled , Appo.CPBmpName , Appo.MODULO , Appo.INS_POINT , Appo.POSITION , Appo.INSERTION , Appo.Deleted, Appo.Note, Appo.MRU,Appo.TEAROFF)
                Endscan
                Select ("appo")
                Use
                * --- Mettiamo a TRUE la variabile modified ogni volta che salviamo.
                This.oParentObject.oParentObject.bModified=.T.
                * --- Espando l'elemento passato..
                This.oParentObject.w_USRTreeView.oTree.nodes(This.w_OINDEX).expanded = .T.
                * --- Riposizioniamo il w_INDEX
                This.oParentObject.w_INDEX = This.w_OINDEX
                This.Pag3()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
            Case This.Event=="USRDEL"
                * --- Il "Menu Principale" non si pu� eliminare...
                If This.oParentObject.w_INDEX=1
                    i_retcode = 'stop'
                    Return
                Endif
                * --- Cancellazione dell'elemento
                This.oParentObject.w_USRTreeView.delElement(This.oParentObject.w_INDEX)
                This.oParentObject.w_INDEX = This.oParentObject.w_USRTreeView.oTree.SelectedItem.Index
                * --- Mettiamo a TRUE la variabile modificata ogni volta che salviamo.
                This.oParentObject.oParentObject.bModified=.T.
                This.Pag3()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
            Case This.Event=="ADD"
                If This.bBtnClick
                    * --- Quando bRigthClick �.T. obbiamo inizializzare w_index  per posizionarci sul primo nodo.
                    *     Inoltre dobbiamo mettere w_directory a 0 che � il valore necessario affinch� i nodi aggiunti
                    *     vengano correttamente inseriti in prima posizione
                    This.oParentObject.w_INDEX = 1
                    This.oParentObject.w_directory = 0
                Endif
                If This.oParentObject.w_directory = 2 Or This.oParentObject.w_directory = 0
                    * --- Inseriamo la nuova voce all'interno del submenu sul quale siamo posizionati
                    *     (il valore 4 indica "figlio di " w_INDEX)
                    This.oParentObject.w_USRTreeView.oTree.nodes.Add(This.oParentObject.w_INDEX,4,,This.cText,This.tipo)
                Else
                    * --- Inseriamo la nuova voce subito dopo il menu sul quale siamo posizionati
                    *     (il valore 2 indica "successivo a " w_INDEX)
                    This.oParentObject.w_USRTreeView.oTree.nodes.Add(This.oParentObject.w_INDEX,2,,This.cText,This.tipo)
                Endif
                * --- Inseriamo il fatto che sia una directory o un menu...
                This.oParentObject.w_directory = Iif(This.tipo=1,2,1)
                * --- ... o un separatore
                If This.cText="------------"
                    This.oParentObject.w_directory = 4
                Endif
                This.oParentObject.w_USRTreeView.expandElement(This.oParentObject.w_INDEX)
                * --- Portiamo l'indice sul nodo appena creato
                This.oParentObject.w_INDEX = This.oParentObject.w_USRTreeView.oTree.nodes.Count()
                * --- Condizioniamo tutto per vedere se la chiamata � arrivata da un tasto destro.
                * --- La variabile viene inizializzata in pag.2 con il valore di DIRECTORY del nodo della SysTreeView
                *     E' utilizzata per condizionare la bitmap da uitlizzare premendo il bottoncino Elimina
                If This.bBtnClick
                    * --- Se la chiamata � arrivata dal tasto destro, allora dobbiamo inserire il punto d'inserzione.
                    *     o mettere il flag cancellato
                    This.Pag2()
                    If i_retcode='stop' Or !Empty(i_Error)
                        Return
                    Endif
                    * --- Variabile per memorizzare la bitmap del menu cancellato da inserire
                    This.bitCanc = Iif(This.SysType=1,"MLEAFCanc","MNODECanc")+".bmp"
                    * --- Settiamo anche la combobox
                    If This.cPosizione="P" Or This.cPosizione="D"
                        This.oParentObject.w_POSITION = This.cPosizione
                    Endif
                Endif
                * --- Salviamo il Tag della TreeView
                This.oParentObject.w_USRTreeView.oTree.nodes(This.oParentObject.w_INDEX).Text = Iif(This.bBtnClick And This.cPosizione="C",Alltrim(Iif(This.SysType=1,cp_Translate(MSG_OPTION),cp_Translate(MSG_MENU))+" - " + cp_Translate(MSG_ERASE)+" "+This.oParentObject.w_insertPoint),Alltrim(This.cText))
                * --- w_USRTreeView.SetTag(oNodo,cNameProc,cDirectory,bAbilitato,cBitmap,cModulo,cInsertPoint,cPosition,cInsertion,cDeleted)
                This.oParentObject.w_USRTreeView.SetTag(This.oParentObject.w_USRTreeView.oTree.nodes( This.oParentObject.w_INDEX ) , "" , This.oParentObject.w_directory , .T. , Iif(Vartype(This.cPosizione)="C" And This.cPosizione="C",This.bitCanc,"") , "" , Iif(This.bBtnClick,This.oParentObject.w_insertPoint,"") ,Iif(This.bBtnClick, This.oParentObject.w_POSITION,"") , "" , Iif(Vartype(This.cPosizione)="C" And This.cPosizione="C","F","T"), This.oParentObject.w_NOTE, This.oParentObject.w_MRU, This.oParentObject.w_TEAROFF)
                This.Pag3()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                If This.bBtnClick And Vartype(This.cPosizione)="C" And This.cPosizione="C"
                    * --- Visualizziamo la bitmap solo se aggiungiamo un nodo coi bottoni di destra
                    This.oParentObject.w_USRTreeView.oTree.nodes(This.oParentObject.w_INDEX).Image = This.oParentObject.w_USRTreeView.GetBitmapIndex( This.bitCanc )
                Endif
                * --- Mettiamo a TRUE la variabile modificata ogni volta che salviamo.
                This.oParentObject.oParentObject.bModified=.T.
            Case This.Event=="LOST"
                * --- Ogni volta che lascio il focus da una qualche variabile della maschera, aggiorna la USRTreeView.
                This.oParentObject.w_USRTreeView.oTree.nodes(This.oParentObject.w_INDEX).Text = Alltrim(This.oParentObject.w_VOCEMENU)
                This.oParentObject.w_USRTreeView.SetTag(This.oParentObject.w_USRTreeView.oTree.nodes( This.oParentObject.w_INDEX ) , This.oParentObject.w_NAMEPROC , This.oParentObject.w_directory , This.oParentObject.w_ABILITATO , This.oParentObject.w_BITMAP , This.oParentObject.w_MODULO , This.oParentObject.w_insertPoint , This.oParentObject.w_POSITION , "" , Iif(This.oParentObject.w_DELETED,"F", "T"), This.oParentObject.w_NOTE, This.oParentObject.w_MRU, This.oParentObject.w_TEAROFF)
                * --- Aggiorniamo le voci del nodo
                This.Pag3()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                * --- Mettiamo a TRUE la variabile modificata ogni volta che salviamo.
                This.oParentObject.oParentObject.bModified=.T.
                * --- Se cambio la Bitmap aggiorno l'immagine del nodo...
                If (This.oParentObject.CurrentEvent="w_BITMAP LostFocus" Or This.oParentObject.CurrentEvent="w_DELETED LostFocus")
                    If This.oParentObject.w_DELETED
                        This.oParentObject.w_USRTreeView.oTree.nodes(This.oParentObject.w_INDEX).Image = This.oParentObject.w_USRTreeView.GetBitmapIndex( Iif(This.oParentObject.w_directory=1,"MLEAFCanc","MNODECanc")+".bmp")
                    Else
                        * --- Gestione Bitmap vuota
                        If Empty(This.oParentObject.w_BITMAP)
                            This.oParentObject.w_USRTreeView.oTree.nodes(This.oParentObject.w_INDEX).Image = This.oParentObject.w_USRTreeView.GetBitmapIndex( Iif(This.oParentObject.w_directory=1,g_MLEAFBMP,g_MNODEBMP))
                        Else
                            This.oParentObject.w_USRTreeView.oTree.nodes(This.oParentObject.w_INDEX).Image = This.oParentObject.w_USRTreeView.GetBitmapIndex( This.oParentObject.w_BITMAP )
                        Endif
                    Endif
                Endif
            Case This.Event=="INS"
                * --- Inseriamo il punto d'inserzione alla voce selezionata
                This.Pag2()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
                * --- Imposto a Prima la posizione se vuota..
                This.oParentObject.w_POSITION = Iif(Empty( This.oParentObject.w_POSITION ) , "P" , This.oParentObject.w_POSITION )
                * --- Salva il punto d'inserzione nella USRTreeView relativamente al nodo della USRTreeView
                *     precedentemente selezionato
                This.oParentObject.w_USRTreeView.SetTag(This.oParentObject.w_USRTreeView.oTree.nodes( This.oParentObject.w_INDEX ) , This.oParentObject.w_NAMEPROC , This.oParentObject.w_directory , This.oParentObject.w_ABILITATO , This.oParentObject.w_BITMAP , This.oParentObject.w_MODULO , This.oParentObject.w_insertPoint , This.oParentObject.w_POSITION , "" , Iif(This.oParentObject.w_DELETED,"F", "T"), This.oParentObject.w_NOTE, This.oParentObject.w_MRU, This.oParentObject.w_TEAROFF)
                * --- Mettiamo a TRUE la variabile modificata ogni volta che salviamo.
                This.oParentObject.oParentObject.bModified=.T.
            Case This.Event=="USRSEL"
                This.Pag3()
                If i_retcode='stop' Or !Empty(i_Error)
                    Return
                Endif
            Case This.Event=="INITMENU"
                * --- Popola all'avvio entrambe le tree view del disegnatore di men�...
                This.w_PADRE = This.oParentObject
                If Empty (This.oParentObject.w_MenuName)
                    * --- Se la Cp_FindDefault non trova niente significa che mi st� basando sul Plan:vmn
                    If Empty( cp_FindDefault("vmn") )
                        This.oParentObject.w_MenuName = Upper( i_CpDic )
                        This.w_CMENUFILE = Upper(i_CpDic)+".vmn"
                    Else
                        This.oParentObject.w_MenuName = "DEFAULT"
                        This.w_CMENUFILE = "DEFAULT.vmn"
                    Endif
                Else
                    This.w_CMENUFILE = This.oParentObject.w_MenuName+".vmn"
                    This.oParentObject.w_MenuName = Upper(Alltrim(Substr(This.oParentObject.w_MenuName,Rat("\",This.oParentObject.w_MenuName)+1)))
                Endif
                If This.w_PADRE.CurrentEvent="Avvio"
                    * --- controlliamo se il cursore � gi� stato creato
                    If Used (This.oParentObject.w_SYSCURSOR)

                        Select (This.oParentObject.w_SYSCURSOR)
                        Use
                    Endif
                    If Not Empty(i_cMenuLoaded)
                        * --- Per il caricamento della TreeView di destra del Disegnatore di Menu, non viene pi�
                        *     utilizzato il cursore i_CUR_MENU, ma viene utilizzato un nuovo cursore nel quale
                        This.w_TempCursor = Sys(2015)

                        Do Vmn_To_Cur In vu_exec With i_cMenuLoaded+".VMN", This.w_TempCursor
                        Do merge_all In vu_exec With This.w_TempCursor , i_cmodules, .T.

                        This.oParentObject.w_SYSCURSOR = This.w_TempCursor
                    Else
                        This.oParentObject.w_SYSCURSOR = Sys(2015)

                        Do cp_Create_Cur_Menu In vu_exec With This.oParentObject.w_SYSCURSOR
                        Select ( This.oParentObject.w_SYSCURSOR )
                        Index On LVLKEY Tag LVLKEY

                        New_Root( This.oParentObject.w_SYSCURSOR )
                    Endif
                    * --- Inizializzo anche il cursore del menu utente...
                    If Empty(This.oParentObject.w_USRCURSOR)
                        This.w_PADRE.oParentObject.NewDoc()
                    Endif
                Else
                    If Used ( This.oParentObject.w_SYSCURSOR )

                        Select ( This.oParentObject.w_SYSCURSOR )
                        Use
                    Endif
                    This.oParentObject.w_SYSCURSOR = Sys(2015)
                    * --- Carico il men� selezionato...
                    Do Vmn_To_Cur In vu_exec With This.w_CMENUFILE , This.oParentObject.w_SYSCURSOR
                Endif
                * --- Popolo la Tree view con il contenuto del cursore...
                This.oParentObject.w_SYSTreeView.cCursor = This.oParentObject.w_SYSCURSOR
                This.w_PADRE.NotifyEvent("SysMenu")
                If Type("this.oParentObject.w_SysTreeView.oTree.nodes(1)")="O"
                    This.oParentObject.w_SYSTreeView.oTree.nodes(1).expanded = .T.
                Endif
            Case This.Event=="TRY"
                * --- Gestione Prova Men� da Disegnatore di Men�.
                This.w_TOOLOBJ = This.oParentObject.oParentObject
                This.w_PADRE = This.oParentObject
                If Not This.w_PADRE.bTestMenu
                    This.oParentObject.w_OLD_MENU = Sys(5)+Sys(2003)+"\"+Sys(2015)+".VMN"
                    * --- Salvo Nome Menu Aperto
                    This.w_MUTENTE = This.oParentObject.w_MENUUTENTE
                    This.w_FILENAME = This.w_TOOLOBJ.cFileName
                    This.w_TOOLOBJ.cFileName = This.oParentObject.w_OLD_MENU

                    CURSORE=Sys(2015)
                    CUR_CURSOR=cp_GetGlobalVar("i_CUR_MENU")
                    Select * From (CUR_CURSOR) Into Cursor &CURSORE
                    Select (CURSORE)
                    Index On LVLKEY Tag LVLKEY
                    * --- Salvo Menu di Prova per aggiornare per poter creare DBF con le modifiche
                    This.w_TOOLOBJ.savedoc()
                    This.w_TOOLOBJ.cFileName = This.w_FILENAME
                    * --- Ripristino Nome Menu Aperto
                    This.oParentObject.w_MENUUTENTE = This.w_MUTENTE
                    This.w_MUTENTE = Alltrim(cp_FindDefault("VMN"))
                    If Empty( This.w_MUTENTE )
                        * --- Se non ho definito il file Default cerco il men� con il nome dell'analisi..
                        This.w_MUTENTE = cp_FindNamedDefaultFile(i_CpDic,"vmn")
                    Endif
                    * --- Creo il Men� di Default senza men� Custom in w_CUR_MENU
                    *     Mergio il DBF che contiene il men� da provare con il men� di default appena caricato
                    This.oParentObject.w_CUR_MENU = Sys(2015)
                    This.w_TREESX = Sys(2015)
                    * --- La domanda se caricare il men� di defaultl a faccio solo se il men� di base
                    *     � un visual men�, se men� vecchio stile non posso svoglere il merge
                    If Not Empty( This.w_MUTENTE ) And Cp_YesNo(MSG_LOADING_DEFAULT_MENU_QP)

                        Do Vmn_To_Cur In vu_exec With This.oParentObject.w_OLD_MENU , This.w_TREESX

                        Do Vmn_To_Cur In vu_exec With This.w_MUTENTE+".VMN" , This.oParentObject.w_CUR_MENU
                        Do merge_all In vu_exec With This.oParentObject.w_CUR_MENU,i_cmodules,.T.
                        Do Merge In vu_exec With This.oParentObject.w_CUR_MENU,This.w_TREESX

                    Else
                        Do Vmn_To_Cur In vu_exec With This.oParentObject.w_OLD_MENU , This.oParentObject.w_CUR_MENU
                    Endif
                    If Used ( This.w_TREESX )

                        Select ( This.w_TREESX )
                        Use
                    Endif
                    If Used ( CUR_CURSOR )

                        Select ( CUR_CURSOR )
                        Use
                    Endif
                    * --- Swapp tra i_CUR_MENU e w_CUR_MENU
                    This.w_CURSAPP = cp_GetGlobalVar("i_CUR_MENU")
                    * --- Valorizzo i_CUR_MENU per visualizzare il men� anche con il CTRL-T
                    cp_SetGlobalVar("i_CUR_MENU",This.oParentObject.w_CUR_MENU)
                    This.oParentObject.w_CUR_MENU = CURSORE
                    * --- Carico Menu di Prova

                    Do CaricaMenu In vu_exec With cp_GetGlobalVar("i_CUR_MENU"),.F.
                    If cp_fileexist(This.oParentObject.w_OLD_MENU)
                        * --- Elimino Menu di Prova
                        Delete File (This.oParentObject.w_OLD_MENU)
                    Endif
                    This.w_OBJ = This.w_PADRE.getctrl("TJEHKLWKFJ")
                    This.w_OBJ.Caption = MSG_RECOVER
                    This.w_OBJ.ToolTipText = MSG_SET_RECOVER_ITEM
                    This.w_PADRE.bTestMenu = .T.
                Else
                    * --- Eseguo Ripristina
                    * --- Swapp tra w_CUR_MENU e i_CUR_MENU
                    This.w_CURSAPP = cp_GetGlobalVar("i_CUR_MENU")
                    * --- Valorizzo i_CUR_MENU per visualizzare il men� anche con il CTRL-T
                    cp_SetGlobalVar("i_CUR_MENU",This.oParentObject.w_CUR_MENU)
                    This.oParentObject.w_CUR_MENU = This.w_CURSAPP
                    * --- Elimino Cursore Menu di Prova
                    If Used ( This.oParentObject.w_CUR_MENU )

                        Select ( This.oParentObject.w_CUR_MENU )
                        Use
                    Endif

                    Do CaricaMenu In vu_exec With cp_GetGlobalVar("i_CUR_MENU"),.F.
                    * --- Rinomino Caption Bottone
                    This.w_OBJ = This.w_PADRE.getctrl("TJEHKLWKFJ")
                    This.w_OBJ.Caption = MSG_TRY
                    This.w_OBJ.ToolTipText = MSG_SET_TRY_ITEM
                    This.w_PADRE.bTestMenu = .F.
                Endif
        Endcase
    Endproc


    Procedure Pag2
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        * --- Inserisce il punto d'inserzione in automatico
        This.oParentObject.w_SYSTreeView.SetFocus()
        This.w_sysIndex = This.oParentObject.w_SYSTreeView.oTree.SelectedItem.Index
        This.w_SYSKEY = This.oParentObject.w_SYSTreeView.oTree.nodes(This.w_sysIndex).Key
        * --- Se esiste la chiave, significa che la voce fa parte del menu base e NON � una voce agganciata;
        *     in questo caso, non serve calcolare il path, ma basta recuperarlo dal cursore contenente il menu.
        Select (This.oParentObject.w_SYSCURSOR)
        Seek Alltrim(Substr(This.w_SYSKEY,2))
        If Found()
            This.oParentObject.w_insertPoint = INSERTION
            This.SysType = Directory
        Else
            i_retcode = 'stop'
            Return
        Endif
    Endproc


    Procedure Pag3
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        * --- Al cambio di nodo nel men� utente aggiorno le informazioni ad esso legate..
        Local L_VOCEMENU,L_NAMEPROC,L_DIRECTORY,L_ABILITATO,L_BITMAP,L_MODULO,L_INSERTPOINT,L_POSITION,L_INSERTION,L_DELETED, L_NOTE, L_MRU, L_TEAROFF
        This.oParentObject.w_USRTreeView.GetValues(This.oParentObject.w_USRTreeView.oTree.nodes( This.oParentObject.w_INDEX ) , @L_VOCEMENU,@L_NAMEPROC,@L_DIRECTORY,@L_ABILITATO,@L_BITMAP,@L_MODULO,@L_INSERTPOINT,@L_POSITION,@L_INSERTION,@L_DELETED,@L_NOTE,@L_MRU,@L_TEAROFF)
        This.oParentObject.w_VOCEMENU = L_VOCEMENU
        This.oParentObject.w_NAMEPROC = L_NAMEPROC
        This.oParentObject.w_directory = L_DIRECTORY
        This.oParentObject.w_ABILITATO = L_ABILITATO
        This.oParentObject.w_BITMAP = L_BITMAP
        This.oParentObject.w_MODULO = L_MODULO
        This.oParentObject.w_insertPoint = L_INSERTPOINT
        This.oParentObject.w_POSITION = L_POSITION
        This.oParentObject.w_DELETED = L_DELETED="F"
        This.oParentObject.w_NOTE = L_NOTE
        This.oParentObject.w_MRU = L_MRU
        This.oParentObject.w_TEAROFF = L_TEAROFF
    Endproc


    Proc Init(oParentObject,Event,cText,tipo,bBtnClick,cPosizione)
        This.Event=Event
        This.cText=cText
        This.tipo=tipo
        This.bBtnClick=bBtnClick
        This.cPosizione=cPosizione
        DoDefault(oParentObject)
        Return
    Function OpenTables()
        Dimension This.cWorkTables[max(1,0)]
        Return(This.OpenAllTables(0))

        * --- Area Manuale = Functions & Procedures
        * --- Fine Area Manuale
Enddefine

Procedure getEntityType(result)
    result="Routine"
Endproc
Procedure getEntityParmList(result)
    result="Event,cText,tipo,bBtnClick,cPosizione"
Endproc
