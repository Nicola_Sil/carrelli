* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_loadgui                                                      *
*              cp_LoadGUI                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-25                                                      *
* Last revis.: 2012-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
Private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
Createobject("tcp_loadgui",oParentObject,m.pOper)
Return(i_retval)

Define Class tcp_loadgui As StdBatch
    * --- Local variables
    pOper = Space(1)
    w_CODUTE = 0
    w_TEST = 0
    w_OLDVISUALTHEME = 0
    w_STABAR = Space(1)
    w_DSKBAR = Space(1)
    w_CPTBAR = Space(1)
    w_TOOLMN = Space(1)
    w_CIBCKGRD = Space(1)
    w_CIMRKGRD = Space(1)
    w_CILBLFNM = Space(50)
    w_CITXTFNM = Space(50)
    w_CICBXFNM = Space(50)
    w_CIBTNFNM = Space(50)
    w_CIGRDFNM = Space(50)
    w_CIPAGFNM = Space(50)
    w_CITBSIZE = 0
    w_CISEARMN = Space(1)
    w_CIMENFIX = Space(1)
    w_WAITWND = Space(1)
    w_CILBLFIT = Space(1)
    w_CITXTFIT = Space(1)
    w_CICBXFIT = Space(1)
    w_CIBTNFIT = Space(1)
    w_CIGRDFIT = Space(1)
    w_CIPAGFIT = Space(1)
    w_CILBLFBO = Space(1)
    w_CITXTFBO = Space(1)
    w_CICBXFBO = Space(1)
    w_CIBTNFBO = Space(1)
    w_CIGRDFBO = Space(1)
    w_CIPAGFBO = Space(1)
    w_CICTRGRD = Space(1)
    w_CIAZOOML = Space(1)
    w_CIZOESPR = Space(1)
    * --- WorkFile variables
    cpsetgui_idx=0
    cpazi_idx=0
    runtime_filters = 2

    Procedure Pag1
        Local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
        * --- Load GUI
        *     pOper:
        *     'S' -> Start
        *     'U' -> User
        * --- Old value
        This.w_OLDVISUALTHEME = i_VisualTheme
        * --- Variabili di appoggio per valorizzazione variabili pubbliche
        * --- Inizio caricamento cfg
        This.w_TEST = -2
        If This.pOper = "S"
            * --- Start
            *     Da cp3start, utente = -1
            This.w_CODUTE = -1
        Else
            * --- User
            *     Da cp_login, utente = i_codute
            This.w_CODUTE = i_CODUTE
            * --- Read from cpsetgui
            i_nOldArea=Select()
            If Used('_read_')
                Select _read_
                Use
            Endif
            i_nConn=i_TableProp[this.cpsetgui_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.cpsetgui_idx,2])
            If i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                    "usrcode"+;
                    " from "+i_cTable+" cpsetgui where ";
                    +"usrcode = "+cp_ToStrODBC(This.w_CODUTE);
                    ,"_read_")
                i_Rows=Iif(Used('_read_'),Reccount(),0)
            Else
                Select;
                    usrcode;
                    from (i_cTable) Where;
                    usrcode = This.w_CODUTE;
                    into Cursor _read_
                i_Rows=_Tally
            Endif
            If Used('_read_')
                Locate For 1=1
                This.w_TEST = Nvl(cp_ToDate(_read_.usrcode),cp_NullValue(_read_.usrcode))
                Use
            Else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                Return
            Endif
            Select (i_nOldArea)
            If This.w_CODUTE <> This.w_TEST
                This.w_CODUTE = -1
            Endif
        Endif
        * --- Controllo in caso di installazione se ho una configurazione salvata
        If This.w_CODUTE = -1
            * --- Read from cpsetgui
            i_nOldArea=Select()
            If Used('_read_')
                Select _read_
                Use
            Endif
            i_nConn=i_TableProp[this.cpsetgui_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.cpsetgui_idx,2])
            If i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                    "usrcode"+;
                    " from "+i_cTable+" cpsetgui where ";
                    +"usrcode = "+cp_ToStrODBC(This.w_CODUTE);
                    ,"_read_")
                i_Rows=Iif(Used('_read_'),Reccount(),0)
            Else
                Select;
                    usrcode;
                    from (i_cTable) Where;
                    usrcode = This.w_CODUTE;
                    into Cursor _read_
                i_Rows=_Tally
            Endif
            If Used('_read_')
                Locate For 1=1
                This.w_TEST = Nvl(cp_ToDate(_read_.usrcode),cp_NullValue(_read_.usrcode))
                Use
            Else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                Return
            Endif
            Select (i_nOldArea)
            * --- Se non trovo una confgurazione uso quella di default
            If This.w_TEST <> -1
                * --- Scrivo cfg di default per installazione (-1)
                * --- Insert into cpsetgui
                i_nConn=i_TableProp[this.cpsetgui_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.cpsetgui_idx,2])
                i_commit = .F.
                If Vartype(nTrsConnCnt)="U" Or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .T.
                Endif
                i_ccchkf=''
                i_ccchkv=''
                This.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,This.cpsetgui_idx,i_nConn)
                If i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"usrcode"+",cigridcl"+",ciselecl"+",ciedtcol"+",cidsblfc"+",cidsblbc"+",cistabar"+",cixpthem"+",ciscrcol"+",cicptbar"+",cidskbar"+",cimdifrm"+",citoolmn"+",cidtlclr"+",civtheme"+",citabmen"+",cibckgrd"+",cimrkgrd"+",cidskmen"+",cilblfnm"+",citxtfnm"+",cicbxfnm"+",cibtnfnm"+",cigrdfnm"+",cipagfnm"+",cilblfsz"+",citxtfsz"+",cicbxfsz"+",cibtnfsz"+",cigrdfsz"+",cipagfsz"+",cimnafnm"+",cimnafsz"+",ciwmafnm"+",ciwmafsz"+",cilblfbo"+",citxtfbo"+",cicbxfbo"+",cibtnfbo"+",cigrdfbo"+",cipagfbo"+",cilblfit"+",citxtfit"+",cicbxfit"+",cibtnfit"+",cigrdfit"+",cipagfit"+",cinavnub"+",cinavsta"+",cinavdim"+",citbsize"+",cisearmn"+",cimenfix"+",cishwbtn"+",cioblcol"+",ciobl_on"+",cicolzom"+",cievizom"+",cirepbeh"+",ciwaitwd"+",cictrgrd"+",cihehezo"+",ciherozo"+",ciazooml"+",cizoespr"+i_ccchkf+") values ("+;
                        cp_NullLink(cp_ToStrODBC(-1),'cpsetgui','usrcode');
                        +","+cp_NullLink(cp_ToStrODBC(Rgb(208, 215, 229)),'cpsetgui','cigridcl');
                        +","+cp_NullLink(cp_ToStrODBC(Rgb(255,231,162)),'cpsetgui','ciselecl');
                        +","+cp_NullLink(cp_ToStrODBC(Rgb(246,246,246)),'cpsetgui','ciedtcol');
                        +","+cp_NullLink(cp_ToStrODBC(Rgb(0,41,91)),'cpsetgui','cidsblfc');
                        +","+cp_NullLink(cp_ToStrODBC(Rgb(233,238,238)),'cpsetgui','cidsblbc');
                        +","+cp_NullLink(cp_ToStrODBC(" "),'cpsetgui','cistabar');
                        +","+cp_NullLink(cp_ToStrODBC(1),'cpsetgui','cixpthem');
                        +","+cp_NullLink(cp_ToStrODBC(Rgb(255,255,255)),'cpsetgui','ciscrcol');
                        +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','cicptbar');
                        +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','cidskbar');
                        +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','cimdifrm');
                        +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','citoolmn');
                        +","+cp_NullLink(cp_ToStrODBC(Rgb(255,231,162)),'cpsetgui','cidtlclr');
                        +","+cp_NullLink(cp_ToStrODBC(-1),'cpsetgui','civtheme');
                        +","+cp_NullLink(cp_ToStrODBC("T"),'cpsetgui','citabmen');
                        +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','cibckgrd');
                        +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cimrkgrd');
                        +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','cidskmen');
                        +","+cp_NullLink(cp_ToStrODBC(i_cProjectFontName),'cpsetgui','cilblfnm');
                        +","+cp_NullLink(cp_ToStrODBC(i_cProjectFontName),'cpsetgui','citxtfnm');
                        +","+cp_NullLink(cp_ToStrODBC(i_cProjectFontName),'cpsetgui','cicbxfnm');
                        +","+cp_NullLink(cp_ToStrODBC(i_cProjectFontName),'cpsetgui','cibtnfnm');
                        +","+cp_NullLink(cp_ToStrODBC(i_cProjectFontName),'cpsetgui','cigrdfnm');
                        +","+cp_NullLink(cp_ToStrODBC(i_cProjectFontName),'cpsetgui','cipagfnm');
                        +","+cp_NullLink(cp_ToStrODBC(i_nProjectFontSize),'cpsetgui','cilblfsz');
                        +","+cp_NullLink(cp_ToStrODBC(i_nProjectFontSize),'cpsetgui','citxtfsz');
                        +","+cp_NullLink(cp_ToStrODBC(i_nProjectFontSize),'cpsetgui','cicbxfsz');
                        +","+cp_NullLink(cp_ToStrODBC(i_nProjectFontSize),'cpsetgui','cibtnfsz');
                        +","+cp_NullLink(cp_ToStrODBC(i_nProjectFontSize),'cpsetgui','cigrdfsz');
                        +","+cp_NullLink(cp_ToStrODBC(i_nProjectFontSize),'cpsetgui','cipagfsz');
                        +","+cp_NullLink(cp_ToStrODBC(i_cProjectFontName),'cpsetgui','cimnafnm');
                        +","+cp_NullLink(cp_ToStrODBC(i_nProjectFontSize),'cpsetgui','cimnafsz');
                        +","+cp_NullLink(cp_ToStrODBC(i_cProjectFontName),'cpsetgui','ciwmafnm');
                        +","+cp_NullLink(cp_ToStrODBC(i_nProjectFontSize),'cpsetgui','ciwmafsz');
                        +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cilblfbo');
                        +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','citxtfbo');
                        +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cicbxfbo');
                        +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cibtnfbo');
                        +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cigrdfbo');
                        +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cipagfbo');
                        +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cilblfit');
                        +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','citxtfit');
                        +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cicbxfit');
                        +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cibtnfit');
                        +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cigrdfit');
                        +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cipagfit');
                        +","+cp_NullLink(cp_ToStrODBC(7),'cpsetgui','cinavnub');
                        +","+cp_NullLink(cp_ToStrODBC("A"),'cpsetgui','cinavsta');
                        +","+cp_NullLink(cp_ToStrODBC(179),'cpsetgui','cinavdim');
                        +","+cp_NullLink(cp_ToStrODBC(24),'cpsetgui','citbsize');
                        +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','cisearmn');
                        +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','cimenfix');
                        +","+cp_NullLink(cp_ToStrODBC(" "),'cpsetgui','cishwbtn');
                        +","+cp_NullLink(cp_ToStrODBC(0),'cpsetgui','cioblcol');
                        +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','ciobl_on');
                        +","+cp_NullLink(cp_ToStrODBC(Rgb(255,231,162)),'cpsetgui','cicolzom');
                        +","+cp_NullLink(cp_ToStrODBC(0),'cpsetgui','cievizom');
                        +","+cp_NullLink(cp_ToStrODBC("8"),'cpsetgui','cirepbeh');
                        +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','ciwaitwd');
                        +","+cp_NullLink(cp_ToStrODBC("N"),'cpsetgui','cictrgrd');
                        +","+cp_NullLink(cp_ToStrODBC(19),'cpsetgui','cihehezo');
                        +","+cp_NullLink(cp_ToStrODBC(19),'cpsetgui','ciherozo');
                        +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','ciazooml');
                        +","+cp_NullLink(cp_ToStrODBC("S"),'cpsetgui','cizoespr');
                        +i_ccchkv+")")
                Else
                    cp_CheckDeletedKey(i_cTable,0,'usrcode',-1,'cigridcl',Rgb(208, 215, 229),'ciselecl',Rgb(255,231,162),'ciedtcol',Rgb(246,246,246),'cidsblfc',Rgb(0,41,91),'cidsblbc',Rgb(233,238,238),'cistabar'," ",'cixpthem',1,'ciscrcol',Rgb(255,255,255),'cicptbar',"S",'cidskbar',"S",'cimdifrm',"S")
                    Insert Into (i_cTable) (usrcode,cigridcl,ciselecl,ciedtcol,cidsblfc,cidsblbc,cistabar,cixpthem,ciscrcol,cicptbar,cidskbar,cimdifrm,citoolmn,cidtlclr,civtheme,citabmen,cibckgrd,cimrkgrd,cidskmen,cilblfnm,citxtfnm,cicbxfnm,cibtnfnm,cigrdfnm,cipagfnm,cilblfsz,citxtfsz,cicbxfsz,cibtnfsz,cigrdfsz,cipagfsz,cimnafnm,cimnafsz,ciwmafnm,ciwmafsz,cilblfbo,citxtfbo,cicbxfbo,cibtnfbo,cigrdfbo,cipagfbo,cilblfit,citxtfit,cicbxfit,cibtnfit,cigrdfit,cipagfit,cinavnub,cinavsta,cinavdim,citbsize,cisearmn,cimenfix,cishwbtn,cioblcol,ciobl_on,cicolzom,cievizom,cirepbeh,ciwaitwd,cictrgrd,cihehezo,ciherozo,ciazooml,cizoespr &i_ccchkf. );
                        values (;
                        -1;
                        ,Rgb(208, 215, 229);
                        ,Rgb(255,231,162);
                        ,Rgb(246,246,246);
                        ,Rgb(0,41,91);
                        ,Rgb(233,238,238);
                        ," ";
                        ,1;
                        ,Rgb(255,255,255);
                        ,"S";
                        ,"S";
                        ,"S";
                        ,"S";
                        ,Rgb(255,231,162);
                        ,-1;
                        ,"T";
                        ,"S";
                        ,"N";
                        ,"S";
                        ,i_cProjectFontName;
                        ,i_cProjectFontName;
                        ,i_cProjectFontName;
                        ,i_cProjectFontName;
                        ,i_cProjectFontName;
                        ,i_cProjectFontName;
                        ,i_nProjectFontSize;
                        ,i_nProjectFontSize;
                        ,i_nProjectFontSize;
                        ,i_nProjectFontSize;
                        ,i_nProjectFontSize;
                        ,i_nProjectFontSize;
                        ,i_cProjectFontName;
                        ,i_nProjectFontSize;
                        ,i_cProjectFontName;
                        ,i_nProjectFontSize;
                        ,"N";
                        ,"N";
                        ,"N";
                        ,"N";
                        ,"N";
                        ,"N";
                        ,"N";
                        ,"N";
                        ,"N";
                        ,"N";
                        ,"N";
                        ,"N";
                        ,7;
                        ,"A";
                        ,179;
                        ,24;
                        ,"S";
                        ,"S";
                        ," ";
                        ,0;
                        ,"N";
                        ,Rgb(255,231,162);
                        ,0;
                        ,"8";
                        ,"N";
                        ,"N";
                        ,19;
                        ,19;
                        ,"S";
                        ,"S";
                        &i_ccchkv. )
                    i_Rows=Iif(bTrsErr,0,1)
                Endif
                If i_commit
                    cp_EndTrs(.T.)
                Endif
                If i_Rows<0 Or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    Return
                Endif
            Endif
        Endif
        * --- Leggo la cfg Installazione/Utente
        * --- Read from cpsetgui
        i_nOldArea=Select()
        If Used('_read_')
            Select _read_
            Use
        Endif
        i_nConn=i_TableProp[this.cpsetgui_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.cpsetgui_idx,2])
        If i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" cpsetgui where ";
                +"usrcode = "+cp_ToStrODBC(This.w_CODUTE);
                ,"_read_")
            i_Rows=Iif(Used('_read_'),Reccount(),0)
        Else
            Select;
            *;
            from (i_cTable) where;
            usrcode = this.w_CODUTE;
            into cursor _read_
            i_Rows=_Tally
        Endif
        If Used('_read_')
            Locate For 1=1
            i_nGridLineColor = Nvl(cp_ToDate(_read_.cigridcl),cp_NullValue(_read_.cigridcl))
            i_nBackColor = Nvl(cp_ToDate(_read_.ciselecl),cp_NullValue(_read_.ciselecl))
            i_nEBackColor = Nvl(cp_ToDate(_read_.ciedtcol),cp_NullValue(_read_.ciedtcol))
            i_uDisabledForecolor = Nvl(cp_ToDate(_read_.cidsblfc),cp_NullValue(_read_.cidsblfc))
            i_uDisabledBackcolor = Nvl(cp_ToDate(_read_.cidsblbc),cp_NullValue(_read_.cidsblbc))
            This.w_STABAR = Nvl(cp_ToDate(_read_.cistabar),cp_NullValue(_read_.cistabar))
            i_nXPTheme = Nvl(cp_ToDate(_read_.cixpthem),cp_NullValue(_read_.cixpthem))
            i_nScreenColor = Nvl(cp_ToDate(_read_.ciscrcol),cp_NullValue(_read_.ciscrcol))
            This.w_CPTBAR = Nvl(cp_ToDate(_read_.cicptbar),cp_NullValue(_read_.cicptbar))
            This.w_DSKBAR = Nvl(cp_ToDate(_read_.cidskbar),cp_NullValue(_read_.cidskbar))
            i_cViewMode = Nvl(cp_ToDate(_read_.cimdifrm),cp_NullValue(_read_.cimdifrm))
            This.w_TOOLMN = Nvl(cp_ToDate(_read_.citoolmn),cp_NullValue(_read_.citoolmn))
            i_nDtlRowClr = Nvl(cp_ToDate(_read_.cidtlclr),cp_NullValue(_read_.cidtlclr))
            i_VisualTheme = Nvl(cp_ToDate(_read_.civtheme),cp_NullValue(_read_.civtheme))
            i_cMenuTab = Nvl(cp_ToDate(_read_.citabmen),cp_NullValue(_read_.citabmen))
            This.w_CIBCKGRD = Nvl(cp_ToDate(_read_.cibckgrd),cp_NullValue(_read_.cibckgrd))
            This.w_CIMRKGRD = Nvl(cp_ToDate(_read_.cimrkgrd),cp_NullValue(_read_.cimrkgrd))
            i_cDeskMenu = Nvl(cp_ToDate(_read_.cidskmen),cp_NullValue(_read_.cidskmen))
            This.w_CILBLFNM = Nvl(cp_ToDate(_read_.cilblfnm),cp_NullValue(_read_.cilblfnm))
            This.w_CITXTFNM = Nvl(cp_ToDate(_read_.citxtfnm),cp_NullValue(_read_.citxtfnm))
            This.w_CICBXFNM = Nvl(cp_ToDate(_read_.cicbxfnm),cp_NullValue(_read_.cicbxfnm))
            This.w_CIBTNFNM = Nvl(cp_ToDate(_read_.cibtnfnm),cp_NullValue(_read_.cibtnfnm))
            This.w_CIGRDFNM = Nvl(cp_ToDate(_read_.cigrdfnm),cp_NullValue(_read_.cigrdfnm))
            This.w_CIPAGFNM = Nvl(cp_ToDate(_read_.cipagfnm),cp_NullValue(_read_.cipagfnm))
            i_nLblFontSize = Nvl(cp_ToDate(_read_.cilblfsz),cp_NullValue(_read_.cilblfsz))
            i_nFontSize = Nvl(cp_ToDate(_read_.citxtfsz),cp_NullValue(_read_.citxtfsz))
            i_nCboxFontSize = Nvl(cp_ToDate(_read_.cicbxfsz),cp_NullValue(_read_.cicbxfsz))
            i_nBtnFontSize = Nvl(cp_ToDate(_read_.cibtnfsz),cp_NullValue(_read_.cibtnfsz))
            i_nGrdFontSize = Nvl(cp_ToDate(_read_.cigrdfsz),cp_NullValue(_read_.cigrdfsz))
            i_nPageFontSize = Nvl(cp_ToDate(_read_.cipagfsz),cp_NullValue(_read_.cipagfsz))
            i_cMenuNavFontName = Nvl(cp_ToDate(_read_.cimnafnm),cp_NullValue(_read_.cimnafnm))
            i_nMenuNavFontSize = Nvl(cp_ToDate(_read_.ciwmafsz),cp_NullValue(_read_.ciwmafsz))
            i_cWindowManagerFontName = Nvl(cp_ToDate(_read_.ciwmafnm),cp_NullValue(_read_.ciwmafnm))
            i_cWindowManagerFontSize = Nvl(cp_ToDate(_read_.ciwmafsz),cp_NullValue(_read_.ciwmafsz))
            i_nDeskMenuMaxButton = Nvl(cp_ToDate(_read_.cinavnub),cp_NullValue(_read_.cinavnub))
            i_cDeskMenuStatus = Nvl(cp_ToDate(_read_.cinavsta),cp_NullValue(_read_.cinavsta))
            i_nDeskMenuInitDim = Nvl(cp_ToDate(_read_.cinavdim),cp_NullValue(_read_.cinavdim))
            This.w_CITBSIZE = Nvl(cp_ToDate(_read_.citbsize),cp_NullValue(_read_.citbsize))
            This.w_CISEARMN = Nvl(cp_ToDate(_read_.cisearmn),cp_NullValue(_read_.cisearmn))
            This.w_CIMENFIX = Nvl(cp_ToDate(_read_.cimenfix),cp_NullValue(_read_.cimenfix))
            i_cZBtnShw = Nvl(cp_ToDate(_read_.cishwbtn),cp_NullValue(_read_.cishwbtn))
            i_nOblColor = Nvl(cp_ToDate(_read_.cioblcol),cp_NullValue(_read_.cioblcol))
            i_cHlOblColor = Nvl(cp_ToDate(_read_.ciobl_on),cp_NullValue(_read_.ciobl_on))
            i_nEviRigaZoom = Nvl(cp_ToDate(_read_.cievizom),cp_NullValue(_read_.cievizom))
            i_nZoomColor = Nvl(cp_ToDate(_read_.cicolzom),cp_NullValue(_read_.cicolzom))
            i_cRepBehavior = Nvl(cp_ToDate(_read_.cirepbeh),cp_NullValue(_read_.cirepbeh))
            This.w_WAITWND = Nvl(cp_ToDate(_read_.ciwaitwd),cp_NullValue(_read_.ciwaitwd))
            This.w_CILBLFIT = Nvl(cp_ToDate(_read_.cilblfit),cp_NullValue(_read_.cilblfit))
            This.w_CITXTFIT = Nvl(cp_ToDate(_read_.citxtfit),cp_NullValue(_read_.citxtfit))
            This.w_CICBXFIT = Nvl(cp_ToDate(_read_.cicbxfit),cp_NullValue(_read_.cicbxfit))
            This.w_CIBTNFIT = Nvl(cp_ToDate(_read_.cibtnfit),cp_NullValue(_read_.cibtnfit))
            This.w_CIGRDFIT = Nvl(cp_ToDate(_read_.cigrdfit),cp_NullValue(_read_.cigrdfit))
            This.w_CIPAGFIT = Nvl(cp_ToDate(_read_.cipagfit),cp_NullValue(_read_.cipagfit))
            This.w_CILBLFBO = Nvl(cp_ToDate(_read_.cilblfbo),cp_NullValue(_read_.cilblfbo))
            This.w_CITXTFBO = Nvl(cp_ToDate(_read_.citxtfbo),cp_NullValue(_read_.citxtfbo))
            This.w_CICBXFBO = Nvl(cp_ToDate(_read_.cicbxfbo),cp_NullValue(_read_.cicbxfbo))
            This.w_CIBTNFBO = Nvl(cp_ToDate(_read_.cibtnfbo),cp_NullValue(_read_.cibtnfbo))
            This.w_CIGRDFBO = Nvl(cp_ToDate(_read_.cigrdfbo),cp_NullValue(_read_.cigrdfbo))
            This.w_CIPAGFBO = Nvl(cp_ToDate(_read_.cipagfbo),cp_NullValue(_read_.cipagfbo))
            This.w_CICTRGRD = Nvl(cp_ToDate(_read_.cictrgrd),cp_NullValue(_read_.cictrgrd))
            This.w_CIAZOOML = Nvl(cp_ToDate(_read_.ciazooml),cp_NullValue(_read_.ciazooml))
            i_nHeaderHeight = Nvl(cp_ToDate(_read_.cihehezo),cp_NullValue(_read_.cihehezo))
            i_nRowHeight = Nvl(cp_ToDate(_read_.ciherozo),cp_NullValue(_read_.ciherozo))
            This.w_CIZOESPR = Nvl(cp_ToDate(_read_.cizoespr),cp_NullValue(_read_.cizoespr))
            Use
        Else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            Return
        Endif
        Select (i_nOldArea)
        * --- Applico la cfg
        * --- Se fox inferiore a 9.0 disabilito nuova interfaccia
        #If Version(5)<900
            i_VisualTheme = -1 && -1 Vista Classica, 5 Office 2007 Blue
            This.w_OLDVISUALTHEME = -1
        #Endif
        * --- Setto variabili pubbliche

        i_bShowCPToolBar = This.w_CPTBAR = "S"
        i_bShowDeskTopBar = This.w_DSKBAR = "S"
        i_bShowToolMenu = This.w_TOOLMN = "S"
        i_bGradientBck = This.w_CIBCKGRD = "S"
        i_bRecordMark = This.w_CIMRKGRD = "S"
        i_nTBTNW = This.w_CITBSIZE
        i_nTBTNH = This.w_CITBSIZE
        i_bSearchMenu = This.w_CISEARMN = "S"
        i_bMenuFix = This.w_CIMENFIX="S"
        i_bWaitWindowTheme = This.w_WAITWND="S"

        *-- Font
        i_cFontName = Alltrim(This.w_CITXTFNM)
        i_cCboxFontName = Alltrim(This.w_CICBXFNM)
        i_cBtnFontName =Alltrim(This.w_CIBTNFNM)
        i_cPageFontName = Alltrim(This.w_CIPAGFNM)
        i_cGrdFontName = Alltrim(This.w_CIGRDFNM)
        i_cLblFontName = Alltrim(This.w_CILBLFNM)

        *-- Font Italic
        i_bLblFontItalic = This.w_CILBLFIT="S"
        i_bFontItalic = This.w_CITXTFIT="S"
        i_bCboxFontItalic = This.w_CICBXFIT="S"
        i_bBtnFontItalic = This.w_CIBTNFIT="S"
        i_bGrdFontItalic = This.w_CIGRDFIT="S"
        i_bPageFontItalic = This.w_CIPAGFIT="S"

        *-- Font Bold
        i_bLblFontBold = This.w_CILBLFBO="S"
        i_bFontBold = This.w_CITXTFBO="S"
        i_bCboxFontBold = This.w_CICBXFBO="S"
        i_bBtnFontBold = This.w_CIBTNFBO="S"
        i_bGrdFontBold = This.w_CIGRDFBO="S"
        i_bPageFontBold = This.w_CIPAGFBO="S"

        *-- Zoom
        i_AdvancedHeaderZoom = This.w_CICTRGRD="S"
        i_bAutoZoomLoad = This.w_CIAZOOML="S"
        i_bExpandZoomParameter = This.w_CIZOESPR="S"
        * --- Controllo cambio tema
        *     Se passo da std a nuovo o voceversa devo ricreare le toolbar e menu
        *     oldVisualTheme contiene il vecchio valore del tema
        *     -1 = std
        If i_VisualTheme <> -1
            i_ThemesManager.ThemeNumber = i_VisualTheme
        Endif
        * --- Status Bar
        If This.w_STABAR="S"

            Set Status Bar On
            Set Status Off
            If i_VisualTheme <> -1

                i_oStatusBar = Createobject("cp_StatusBar")
                i_oStatusBar.addpanel(0,100,.T.)
                i_oStatusBar.addpanel(3) &&OVR
                i_oStatusBar.addpanel(1) &&NUM
                i_oStatusBar.addpanel(2) &&CAPS
            Endif
        Else

            Set Status Bar Off
        Endif
        If (This.w_OLDVISUALTHEME = -1 And i_VisualTheme <> - 1) Or (This.w_OLDVISUALTHEME <> -1 And i_VisualTheme = - 1)
            * --- Se cambio da nuova a standard devo sgangiare le toolbar in modo tale da non avere pi� visibile la dockarea (poich� resta colorata)
            oCpToolBar.Visible=.F.
            If Vartype(oDesktopBar)="O"
                oDesktopBar.Visible= .F.
            Endif
            If This.w_OLDVISUALTHEME <> -1 And Vartype(i_MenuToolbar) = "O"

                i_MenuToolbar.Visible=.F.
            Endif
            * --- ToolBar sistema

            oCpToolBar.Destroy()
            oCpToolBar = .Null.
            oCpToolBar=Createobject("CPToolBar", i_VisualTheme<>-1)
            oCpToolBar.Dock(0)
            * --- Application bar
            If Vartype(oDesktopBar)="O"

                oDesktopBar.Destroy()
                oDesktopBar = .Null.
                cp_desk()
            Endif
            * --- Menu
            If This.w_OLDVISUALTHEME <> -1 And Vartype(i_MenuToolbar) = "O"

                i_MenuToolbar.Visible=.F.
                cb_Destroy(i_MenuToolbar.HWnd)
                i_MenuToolbar.oPopupMenu.oParentObject=.Null.
                i_MenuToolbar.Destroy()
                i_MenuToolbar = .Null.
                Release i_MenuToolbar
            Else
                Hide Menu _Msysmenu
            Endif
            * --- Ricarico menu
            If This.pOper = "U" And Vartype(i_CUR_MENU)="C" And Used(i_CUR_MENU)
                CP_MENU()
            Endif
        Endif
        * --- Setto screen color
        _Screen.BackColor = i_nScreenColor
        * --- Toolbar
        If Vartype(oCpToolBar)="O"

            oCpToolBar.Visible = i_bShowCPToolBar
            oCpToolBar.Customize = i_bShowCPToolBar
            oCpToolBar.ChangeSettings()
        Endif
        * --- Application Bar
        If Vartype(oDesktopBar) = "O"

            oDesktopBar.Visible = i_bShowDeskTopBar
            oDesktopBar.Customize = oDesktopBar.Visible
            oDesktopBar.ChangeSettings()
        Endif
        * --- Desktop Menu
        If Vartype(i_oDeskmenu)="O"

            i_oDeskmenu.ChangeSettings()
        Endif
        * --- Cambio tema XP
        Sys(2700, i_nXPTheme)
        If i_nXPTheme = 1 And i_nZBtnWidth < 14
            * --- Se tema Xp Attivo il bottone contestuale al mimimo deve essere largo 14 pixel
            *     altrimenti risulta tagliato
            *     Se cambia impostazione tra installazioen e utente,
            *      non funziona da Xp attivo a disattivo, per farlo funzionare
            *     occorre memorizzare in un'ulteriore var. il valore prima della modifica..

            Public i_OldZBtnWidth
            i_OldZBtnWidth = i_nZBtnWidth
            i_nZBtnWidth = 14
            * --- Rimuovo la classe del bottoncino nel caso in cui le impostazioni
            *     relative al tema Xp differiscono tra Installazione e utente...
            Clear Class btnZoom
        Endif
        If i_nXPTheme = 0 And i_nZBtnWidth = 14 And Vartype(i_OldZBtnWidth)="N" And i_nZBtnWidth <> i_OldZBtnWidth
            i_nZBtnWidth = i_OldZBtnWidth
            Clear Class btnZoom
        Endif
        If This.pOper = "U" And i_VisualTheme <> -1
            If Vartype(i_MenuToolbar) = "O"
                * --- Menu in posizione fissa

                i_MenuToolbar.Dock(0,-1,0)
                i_MenuToolbar.Movable = Not i_bMenuFix
                i_MenuToolbar.Gripper = Not i_bMenuFix
                * --- Ricerca voci menu
                If i_bSearchMenu
                    If Type("i_MenuToolbar.oSearchMenu") <> "O"

                        i_MenuToolbar.AddObject("oSearchMenu", "SearchMenu", i_MenuToolbar.oPopupMenu.cCursorName)
                        i_MenuToolbar.oSearchMenu.Visible=.T.
                        i_MenuToolbar.Rearrange()
                    Endif
                Else
                    If Type("i_MenuToolbar.oSearchMenu") = "O"

                        i_MenuToolbar.RemoveObject("oSearchMenu")
                        i_MenuToolbar.Rearrange()
                    Endif
                Endif
            Endif
        Endif
        * --- DeskMenu
        If This.pOper = "U"
            * --- Rimovo l'oggetto
            If Vartype(i_oDeskmenu)="O"

                _Screen.RemoveObject("NavBar")
                Release i_oDeskmenu
                _Screen.RemoveObject("ImgBackground")
                _Screen.RemoveObject("TBMDI")
            Endif
            Do Case
                Case i_cDeskMenu = "O"
                    * --- Apro e visualizzo
                    cp_NavBar(.T.)
                Case i_cDeskMenu = "S"
                    * --- Carico ma non visualizzo
                    cp_NavBar(.F.)
            Endcase
        Endif
        * --- Report engine behavior
        If i_cRepBehavior = "8"

            Set REPORTBEHAVIOR 80
        Else

            Set REPORTBEHAVIOR 90
        Endif

        _ReportOutput = Fullpath("ReportOutput.app")
        _ReportPreview = Fullpath("ReportPreview.app")
        _ReportBuilder = Fullpath("ReportBuilder.app")
    Endproc


    Proc Init(oParentObject,pOper)
        This.pOper=pOper
        DoDefault(oParentObject)
        Return
    Function OpenTables()
        Dimension This.cWorkTables[max(1,2)]
        This.cWorkTables[1]='cpsetgui'
        This.cWorkTables[2]='cpazi'
        Return(This.OpenAllTables(2))

        * --- Area Manuale = Functions & Procedures
        * --- Fine Area Manuale
Enddefine

Procedure getEntityType(result)
    result="Routine"
Endproc
Procedure getEntityParmList(result)
    result="pOper"
Endproc
