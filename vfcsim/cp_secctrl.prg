* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_secctrl                                                      *
*              Richiesta sblocco procedura                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-06-12                                                      *
* Last revis.: 2008-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject
* --- Area Manuale = Header
* --- cp_secctrl
= cp_ReadXdc()
* --- Fine Area Manuale
Return(Createobject("tcp_secctrl",oParentObject))

* --- Class definition
Define Class tcp_secctrl As StdForm
    Top    = 10
    Left   = 10

    * --- Standard Properties
    Width  = 648
    Height = 132
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2008-10-08"
    HelpContextID=107176555
    max_rt_seq=3

    * --- Constant Properties
    _IDX = 0
    cpusrgrp_IDX = 0
    cPrg = "cp_secctrl"
    cComment = "Richiesta sblocco procedura"
    oParentObject = .Null.
    Icon = "mask.ico"
    WindowType = 1
    MinButton = .F.
    *closable = .f.

    * --- Local Variables
    w_tablename = Space(50)
    w_TBCOMMENT = Space(60)
    w_CURFORM = Space(50)
    * --- Area Manuale = Declare Variables
    * --- cp_secctrl
    * --- Centro la finestra
    AutoCenter=.T.
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=1, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tcp_secctrlPag1","cp_secctrl",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate("Pag.1")
            .Pages(1).oPag.oContained=Thisform
        Endwith
        This.Parent.oFirstControl = .Null.
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
        * --- Area Manuale = Init Page Frame
        * --- cp_secctrl
        * --- Translate interface...
        This.Parent.cComment=cp_Translate(MSG_REQUEST_UNBLOCK_PROCEDURE)
        * --- Fine Area Manuale
    Endproc
    Proc Init(oParentObject)
        If Vartype(m.oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        DoDefault()

        * --- Open tables
    Function OpenWorkTables()
        Dimension This.cWorkTables[1]
        This.cWorkTables[1]='cpusrgrp'
        Return(This.OpenAllTables(1))

    Procedure SetPostItConn()
        Return


        * --- Read record and initialize Form variables
    Procedure LoadRec()
    Endproc

    * --- ecpQuery
    Procedure ecpQuery()
        This.BlankRec()
        This.cFunction="Edit"
        This.SetStatus()
        If This.cFunction="Edit"
            This.mEnableControls()
        Endif
    Endproc
    * --- Esc
    Proc ecpQuit()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            This.cFunction = "Edit"
            This.oFirstControl.SetFocus()
            Return
        Endif
        * ---
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Endproc
    Proc QueryUnload()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            Nodefault
            Return
        Endif
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Proc ecpSave()
        If This.TerminateEdit() And This.CheckForm()
            This.LockScreen=.T.
            This.mReplace(.T.)
            This.LockScreen=.F.
            This.Hide()
            This.NotifyEvent("Done")
            This.Release()
        Endif
    Endproc
    Func OkToQuit()
        Return .T.
    Endfunc

    * --- Blank Form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        With This
            .w_tablename=Space(50)
            .w_TBCOMMENT=Space(60)
            .w_CURFORM=Space(50)
            .w_tablename = Iif( Vartype(oParentObject)='C', Left(oParentObject,At('@',oParentObject)-1) ,'')
            .w_TBCOMMENT = I_DCX.GETTABLEDEscr(I_DCX.gETTABLEIDX( Alltrim( .w_tablename ) ))
            .w_CURFORM = Iif( Vartype(oParentObject)='C' , Substr(oParentObject, At('@',oParentObject)+1, At('@',oParentObject,2)-At('@',oParentObject)-1), '' )
            .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_FEATURE_NOT_ACCESSIBLE_WITH_RESTRICTED_DATA)
            .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_TABLE_WITH_ACCESS_FILTER_UNABLE_TO_ACCESS)
            .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_TABLE_WITH_ACCESS_FILTER+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_MANAGEMENT_REQUEST+MSG_FS)
        Endwith
        This.SaveDependsOn()
        This.SetControlsValue()
        This.oPgFrm.Page1.oPag.oBtn_1_8.Enabled = This.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
        This.mHideControls()
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = Setting operation
    *     Allowed Parameters: Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        * --- Deactivating List page when <> da Query
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt
        i_cFlt =""
        Return (i_cFlt)
    Endfunc

    Proc QueryKeySet(i_cWhere,i_cOrderBy)
    Endproc

    Proc SelectCursor
    Proc GetXKey

        * --- Update Database
    Function mReplace(i_bEditing)
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        This.NotifyEvent('Update start')
        With This
        Endwith
        This.NotifyEvent('Update end')
        * --- Area Manuale = Replace End
        * --- cp_secctrl
        w_checkadmin=.T.
        * --- Fine Area Manuale
        Return(.T.)

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        If i_bUpd
            With This
                .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_FEATURE_NOT_ACCESSIBLE_WITH_RESTRICTED_DATA)
                .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_TABLE_WITH_ACCESS_FILTER_UNABLE_TO_ACCESS)
                .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_TABLE_WITH_ACCESS_FILTER+MSG_FS)
                .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_MANAGEMENT_REQUEST+MSG_FS)
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
            Endwith
            This.DoRTCalc(1,3,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
            .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_FEATURE_NOT_ACCESSIBLE_WITH_RESTRICTED_DATA)
            .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_TABLE_WITH_ACCESS_FILTER_UNABLE_TO_ACCESS)
            .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_TABLE_WITH_ACCESS_FILTER+MSG_FS)
            .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_MANAGEMENT_REQUEST+MSG_FS)
        Endwith
        Return


        * --- Enable controls under condition
    Procedure mEnableControls()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        DoDefault()
        Return

        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
        Endwith
        * --- Area Manuale = Notify Event End
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

    Function SetControlsValue()
        If Not(This.oPgFrm.Page1.oPag.otablename_1_3.Value==This.w_tablename)
            This.oPgFrm.Page1.oPag.otablename_1_3.Value=This.w_tablename
        Endif
        If Not(This.oPgFrm.Page1.oPag.oTBCOMMENT_1_4.Value==This.w_TBCOMMENT)
            This.oPgFrm.Page1.oPag.oTBCOMMENT_1_4.Value=This.w_TBCOMMENT
        Endif
        If Not(This.oPgFrm.Page1.oPag.oCURFORM_1_7.Value==This.w_CURFORM)
            This.oPgFrm.Page1.oPag.oCURFORM_1_7.Value=This.w_CURFORM
        Endif
        cp_SetControlsValueExtFlds(This,'')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
            Else
                If Not(i_bnoChk)
                    cp_ErrorMsg(i_cErrorMsg)
                Endif
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

Enddefine

* --- Define pages as container
Define Class tcp_secctrlPag1 As StdContainer
    Width  = 644
    Height = 132
    stdWidth  = 644
    stdheight = 132
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.

    Add Object otablename_1_3 As StdField With uid="NRBQWSBWLH",rtseq=1,rtrep=.F.,;
        cFormVar = "w_tablename", cQueryName = "",Enabled=.F.,;
        bObbl = .F. , nPag = 1, Value=Space(50), bMultilanguage =  .F.,;
        HelpContextID = 204210195,;
        bGlobalFont=.T.,;
        Height=21, Width=216, Left=200, Top=50, InputMask=Replicate('X',50)

    Add Object oTBCOMMENT_1_4 As StdField With uid="GWXCAUQGAN",rtseq=2,rtrep=.F.,;
        cFormVar = "w_TBCOMMENT", cQueryName = "TBCOMMENT",Enabled=.F.,;
        bObbl = .F. , nPag = 1, Value=Space(60), bMultilanguage =  .F.,;
        HelpContextID = 48640206,;
        bGlobalFont=.T.,;
        Height=21, Width=216, Left=421, Top=50, InputMask=Replicate('X',60)

    Add Object oCURFORM_1_7 As StdField With uid="PELIPIZVZE",rtseq=3,rtrep=.F.,;
        cFormVar = "w_CURFORM", cQueryName = "CURFORM",Enabled=.F.,;
        bObbl = .F. , nPag = 1, Value=Space(50), bMultilanguage =  .F.,;
        HelpContextID = 183094857,;
        bGlobalFont=.T.,;
        Height=21, Width=437, Left=200, Top=75, InputMask=Replicate('X',50)


    Add Object oBtn_1_8 As StdButton With uid="JWIBHYOYEX",Left=587, Top=103, Width=50,Height=20,;
        Picture="BMP\ESC.BMP", Caption="", nPag=1;
        , HelpContextID = 155470997;
        , Caption=MSG_S_EXIT;
        , FontName = "Arial", FontSize = 7, FontBold = .F., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.
    Proc oBtn_1_8.Click()
        =cp_StandardFunction(This,"Quit")
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oObj_1_9 As cp_setCtrlObjprop With uid="WTFHXDANKA",Left=7, Top=138, Width=284,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="La funzionalit� non � accessibile da utenti con restrizione nei dati.",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 94629129;
        , bGlobalFont=.T.



    Add Object oObj_1_10 As cp_setCtrlObjprop With uid="LIFPFMELWQ",Left=7, Top=164, Width=284,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Sulla tabella sono stati definiti accessi con filtro: impossibile accedere.",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 94629129;
        , bGlobalFont=.T.



    Add Object oObj_1_11 As cp_setCtrlObjprop With uid="DXBAZXZQNK",Left=7, Top=190, Width=284,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Tabella con accesso bloccato:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 94629129;
        , bGlobalFont=.T.



    Add Object oObj_1_12 As cp_setCtrlObjprop With uid="MPPQQAAGHO",Left=7, Top=216, Width=284,Height=26,;
        caption='Object',;
        cProp="Caption",cObj="Gestione richiesta:",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 94629129;
        , bGlobalFont=.T.


    Add Object oStr_1_1 As StdString With uid="LJAXTAYSPR",Visible=.T., Left=14, Top=7,;
        Alignment=2, Width=621, Height=19,;
        Caption="La funzionalit� non � accessibile da utenti con restrizione nei dati."  ;
        , FontName = "Arial", FontSize = 10, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_1_2 As StdString With uid="GLROYFNAJU",Visible=.T., Left=14, Top=25,;
        Alignment=2, Width=621, Height=19,;
        Caption="Sulla tabella sono stati definiti accessi con filtro: impossibile accedere."  ;
        , FontName = "Arial", FontSize = 10, FontBold = .T., FontItalic=.F., FontUnderline=.F., FontStrikethru=.F.

    Add Object oStr_1_5 As StdString With uid="SKIKRKJNVZ",Visible=.T., Left=4, Top=50,;
        Alignment=1, Width=189, Height=18,;
        Caption="Tabella con accesso bloccato:"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_6 As StdString With uid="BSYHUWLRWV",Visible=.T., Left=14, Top=75,;
        Alignment=1, Width=179, Height=18,;
        Caption="Gestione richiesta:"  ;
        , bGlobalFont=.T.
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_secctrl','','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Dialog Window"
Endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
