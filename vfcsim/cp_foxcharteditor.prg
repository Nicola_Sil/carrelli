* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_foxcharteditor                                               *
*              FoxChart editor                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-07-27                                                      *
* Last revis.: 2010-05-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
Return(Createobject("tcp_foxcharteditor",oParentObject))

* --- Class definition
Define Class tcp_foxcharteditor As StdForm
    Top    = 10
    Left   = 10

    * --- Standard Properties
    Width  = 629
    Height = 598
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2010-05-26"
    HelpContextID=99652182
    max_rt_seq=4

    * --- Constant Properties
    _IDX = 0
    cpfchart_IDX = 0
    cPrg = "cp_foxcharteditor"
    cComment = "FoxChart editor"
    oParentObject = .Null.
    Icon = "mask.ico"
    *closable = .f.

    * --- Local Variables
    w_FDSERIAL = Space(3)
    o_FDSERIAL = Space(3)
    w_CHANGEONFLY = .F.
    w_ISVQR = .F.
    w_ISCURS = .F.

    * --- Children pointers
    cp_FoxChartProperties = .Null.
    w_oChart = .Null.
    * --- Area Manuale = Declare Variables
    * --- cp_foxcharteditor
    * --- Disabilito la possibilitÓ di chiudere la maschera con la crocetta
    * --- in alto a destra
    Closable=.F.
    AutoCenter=.T.
    AlwaysOnTop=.T.
    * --- Disabilito la Toolbar all'attivarsi della maschera
    Proc SetStatus()
        DoDefault()
        oCpToolBar.Enable(.F.)
    Endproc

    * --- Disabilito la Toolbar all'attivarsi della maschera
    * --- inoltre valorizzo i_curform facendola puntare alla toolbar
    * --- che ha lanciato la maschera
    Proc Activate()
        DoDefault()
        oCpToolBar.Enable(.F.)
        *--- tasto destro non funziona, lo devo disabilitare
        On Key Label RIGHTMOUSE Wait Window ""
    Endproc

    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=1, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        *set procedure to cp_FoxChartProperties additive
        With This
            .Pages(1).AddObject("oPag","tcp_foxcharteditorPag1","cp_foxcharteditor",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate("Pag.1")
            .Pages(1).oPag.oContained=Thisform
        Endwith
        This.Parent.oFirstControl = This.Page1.oPag.oCHANGEONFLY_1_6
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
        *release procedure cp_FoxChartProperties
        * --- Area Manuale = Init Page Frame
        * --- cp_foxcharteditor
        * --- Translate interface...
        This.Parent.cComment=cp_Translate(MSG_FOXCHARTS)
        * --- Fine Area Manuale
    Endproc
    Proc Init(oParentObject)
        If Vartype(m.oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        This.w_oChart = This.oPgFrm.Pages(1).oPag.oChart
        DoDefault()
    Proc Destroy()
        This.w_oChart = .Null.
        DoDefault()

        * --- Open tables
    Function OpenWorkTables()
        Dimension This.cWorkTables[1]
        This.cWorkTables[1]='cpfchart'
        Return(This.OpenAllTables(1))

    Procedure SetPostItConn()
        Return

    Function CreateChildren()
        This.cp_FoxChartProperties = Createobject('stdDynamicChild',This,'cp_FoxChartProperties',This.oPgFrm.Page1.oPag.oLinkPC_1_3)
        This.cp_FoxChartProperties.createrealchild()
        Return

    Procedure DestroyChildren()
        If !Isnull(This.cp_FoxChartProperties)
            This.cp_FoxChartProperties.DestroyChildrenChain()
            This.cp_FoxChartProperties=.Null.
        Endif
        This.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_3')
        Return

    Function IsAChildUpdated()
        Local i_bRes
        i_bRes = This.bUpdated
        i_bRes = i_bRes .Or. This.cp_FoxChartProperties.IsAChildUpdated()
        Return(i_bRes)

    Procedure ChildrenNewDocument()
        This.cp_FoxChartProperties.NewDocument()
        Return

    Procedure SetChildrenKeys()
        With This
            This.cp_FoxChartProperties.SetKey(;
                .w_FDSERIAL,"code";
                )
        Endwith
        Return

    Procedure ChildrenChangeRow()
        With This
            .cp_FoxChartProperties.ChangeRow(This.cRowID+'      1',1;
                ,.w_FDSERIAL,"code";
                )
        Endwith
        Return

    Function AddSonsFilter(i_cFlt,i_oTopObject)
        Local i_f,i_fnidx,i_cDatabaseType
        With This
            If !Isnull(.cp_FoxChartProperties)
                i_f=.cp_FoxChartProperties.BuildFilter()
                If !(i_f==.cp_FoxChartProperties.cQueryFilter)
                    i_fnidx=.cp_FoxChartProperties.cFile+'_IDX'
                    i_cDatabaseType = i_ServerConn[i_TableProp[.cp_FoxChartProperties.&i_fnidx.,5],6]
                    i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.cp_FoxChartProperties.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.cp_FoxChartProperties.&i_fnidx.,2])+' where '+i_f+')'
                Else
                    i_f=''
                Endif
                i_f=.cp_FoxChartProperties.AddSonsFilter(i_f,i_oTopObject)
                If !Empty(i_f)
                    i_cFlt=i_cFlt+Iif(Empty(i_cFlt),'',' AND ')+i_f
                Endif
            Endif
        Endwith
        Return(i_cFlt)


        * --- Read record and initialize Form variables
    Procedure LoadRec()
    Endproc

    * --- ecpQuery
    Procedure ecpQuery()
        This.BlankRec()
        This.cFunction="Edit"
        This.SetStatus()
        If This.cFunction="Edit"
            This.mEnableControls()
        Endif
    Endproc
    * --- Esc
    Proc ecpQuit()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            This.cFunction = "Edit"
            This.oFirstControl.SetFocus()
            Return
        Endif
        * ---
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Endproc
    Proc QueryUnload()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        If !This.OkToQuit()
            This.__dummy__.Enabled=.F.
            Nodefault
            Return
        Endif
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Proc ecpSave()
        If This.TerminateEdit() And This.CheckForm()
            This.Save_cp_FoxChartProperties(.F.)
            This.LockScreen=.T.
            This.mReplace(.T.)
            This.LockScreen=.F.
            This.NotifyEvent("Done")
            This.Release()
        Endif
    Endproc
    Func OkToQuit()
        If  This.cp_FoxChartProperties.IsAChildUpdated()
            Return cp_YesNo(MSG_DISCARD_CHANGES_QP)
        Endif
        Return .T.
    Endfunc
    Proc Save_cp_FoxChartProperties(i_ask)
        If This.cp_FoxChartProperties.IsAChildUpdated() And (!i_ask Or cp_YesNo(MSG_SAVE_CHANGES_QP+' (LinkPC)'))
            cp_BeginTrs()
            This.cp_FoxChartProperties.mReplace(.T.)
            cp_EndTrs()
        Endif
    Endproc

    * --- Blank Form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        This.ChildrenNewDocument()
        With This
            .w_FDSERIAL=Space(3)
            .w_CHANGEONFLY=.F.
            .w_ISVQR=.F.
            .w_ISCURS=.F.
            .oPgFrm.Page1.oPag.oChart.Calculate()
            .cp_FoxChartProperties.NewDocument()
            .cp_FoxChartProperties.ChangeRow('1',1,.w_FDSERIAL,"code")
            If Not(.cp_FoxChartProperties.bLoaded)
                .cp_FoxChartProperties.SetKey(.w_FDSERIAL,"code")
            Endif
            .DoRTCalc(1,1,.F.)
            .w_CHANGEONFLY = .T.
        Endwith
        This.DoRTCalc(3,4,.F.)
        This.SaveDependsOn()
        This.SetControlsValue()
        This.mHideControls()
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- cp_foxcharteditor

    Endproc

    Proc ecpQuit1()
        * --- Move without activating controls
        This.cFunction='Filter'
        This.__dummy__.Enabled=.T.
        This.__dummy__.SetFocus()
        * ---
        This.Hide()
        This.NotifyEvent("Edit Aborted")
        This.NotifyEvent("Done")
        This.Release()
    Endproc

    Proc ecpQuit()
        i_curform.oParentObject.ecpQuit()
    Endproc

    Proc ecpSave()
        Return

        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = Setting operation
    *     Allowed Parameters: Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        * --- Deactivating List page when <> da Query
        This.cp_FoxChartProperties.SetStatus(i_cOp)
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *  this.cp_FoxChartProperties.SetChildrenStatus(i_cOp)
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate filter
    Func BuildFilter()
        Local i_cFlt
        i_cFlt =""
        Return (i_cFlt)
    Endfunc

    Proc QueryKeySet(i_cWhere,i_cOrderBy)
    Endproc

    Proc SelectCursor
    Proc GetXKey

        * --- Update Database
    Function mReplace(i_bEditing)
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        This.NotifyEvent('Update start')
        With This
        Endwith
        This.NotifyEvent('Update end')
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(.T.)

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        If i_bUpd
            With This
                .oPgFrm.Page1.oPag.oChart.Calculate()
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
                If .w_FDSERIAL<>.o_FDSERIAL
                    .Save_cp_FoxChartProperties(.T.)
                    .cp_FoxChartProperties.NewDocument()
                    .cp_FoxChartProperties.ChangeRow('1',1,.w_FDSERIAL,"code")
                    If Not(.cp_FoxChartProperties.bLoaded)
                        .cp_FoxChartProperties.SetKey(.w_FDSERIAL,"code")
                    Endif
                Endif
            Endwith
            This.DoRTCalc(1,4,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
            .oPgFrm.Page1.oPag.oChart.Calculate()
        Endwith
        Return

    Proc Calculate_BYKEQGZGCK()
        With This
            * --- AFTERCHART
            cp_setfoxchartproperties(This;
                ,"SETGRIDDATA";
                )
        Endwith
    Endproc

    * --- Enable controls under condition
    Procedure mEnableControls()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        DoDefault()
        Return

        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            .oPgFrm.Page1.oPag.oChart.Event(cEvent)
            If Lower(cEvent)==Lower("w_ochart afterchart")
                .Calculate_BYKEQGZGCK()
                bRefresh=.T.
            Endif
        Endwith
        * --- Area Manuale = Notify Event End
        * --- cp_foxcharteditor
        If Lower(cEvent)='getvaluefromchart'
            This.cp_FoxChartProperties.NotifyEvent("GetValueFromChart")
        Endif
        If Lower(cEvent)='w_ochart config loaded'
            This.cp_FoxChartProperties.NotifyEvent("CONFIGLOADED")
        Endif
        If Lower(cEvent)='redrawfoxcharts'
            This.cp_FoxChartProperties.NotifyEvent("REDRAWFOXCHARTS")
        Endif
        If Lower(cEvent)='setcursordata'
            This.cp_FoxChartProperties.NotifyEvent("SETCURSORDATA")
        Endif
        If Lower(cEvent)='gettfieldsfromchart'
            This.cp_FoxChartProperties.NotifyEvent("GETTFIELDSFROMCHART")
        Endif
        If Lower(cEvent)='getlegends'
            This.cp_FoxChartProperties.NotifyEvent("GETLEGENDS")
        Endif
        If Lower(cEvent)='runbyparam'
            This.cp_FoxChartProperties.NotifyEvent("RUNBYPARAM")
        Endif


        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

    Function SetControlsValue()
        If Not(This.oPgFrm.Page1.oPag.oCHANGEONFLY_1_6.RadioValue()==This.w_CHANGEONFLY)
            This.oPgFrm.Page1.oPag.oCHANGEONFLY_1_6.SetRadio()
        Endif
        cp_SetControlsValueExtFlds(This,'')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            *i_bRes = i_bRes .and. .cp_FoxChartProperties.CheckForm()
            If i_bRes
                i_bRes=  .cp_FoxChartProperties.CheckForm()
                If Not(i_bRes)
                    This.oPgFrm.ActivePage=1
                Endif
            Endif
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
            Else
                If Not(i_bnoChk)
                    cp_ErrorMsg(i_cErrorMsg)
                Endif
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

    * --- SaveDependsOn
    Proc SaveDependsOn()
        This.o_FDSERIAL = This.w_FDSERIAL
        * --- cp_FoxChartProperties : Depends On
        This.cp_FoxChartProperties.SaveDependsOn()
        Return

Enddefine

* --- Define pages as container
Define Class tcp_foxcharteditorPag1 As StdContainer
    Width  = 625
    Height = 598
    stdWidth  = 625
    stdheight = 598
    resizeXpos=480
    resizeYpos=224
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.


    Add Object oChart As cp_FoxCharts With uid="ZPUJQCCDHG",Left=5, Top=1, Width=616,Height=305,;
        caption='Object',;
        cLogoFileName="",cSource="",cFileCfg="",Anchor=0,bDisplayLogo=.F.,bQueryOnLoad=.T.,cMenuFile="",bReadOnly=.T.,;
        cEvent = "Redraw",;
        nPag=1;
        , HelpContextID = 5175482;
        , bGlobalFont=.T.



    Add Object oLinkPC_1_3 As stdDynamicChildContainer With uid="MPPJVAAMEA",Left=4, Top=337, Width=612, Height=255, bOnScreen=.T.;



    Add Object oBtn_1_4 As StdButton With uid="XEHWPCHNML",Left=10, Top=307, Width=77,Height=25,;
        caption="Redraw", nPag=1;
        , HelpContextID = 242956870;
        , bGlobalFont=.T.

    Proc oBtn_1_4.Click()
        With This.Parent.oContained
            .cp_FoxChartProperties.cp_foxchartfields.NotifyEvent("REDRAWFOXCHARTS")
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc


    Add Object oBtn_1_5 As StdButton With uid="PZEOMMSRXV",Left=91, Top=307, Width=77,Height=25,;
        caption="Update", nPag=1;
        , HelpContextID = 7073094;
        , bGlobalFont=.T.

    Proc oBtn_1_5.Click()
        With This.Parent.oContained
            .w_oChart.oChart.DrawChart()
        Endwith
        If !Isnull(This.Parent.oContained) And Inlist(This.Parent.oContained.cFunction,"Edit","Load")
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object oCHANGEONFLY_1_6 As StdCheck With uid="EAPFEMDGJM",rtseq=2,rtrep=.F.,Left=175, Top=311, Caption="Apply change on the fly",;
        HelpContextID = 227252619,;
        cFormVar="w_CHANGEONFLY", bObbl = .F. , nPag = 1;
        , bGlobalFont=.T.


    Func oCHANGEONFLY_1_6.RadioValue()
        Return(Iif(This.Value =1,.T.,;
            .F.))
    Endfunc
    Func oCHANGEONFLY_1_6.GetRadio()
        This.Parent.oContained.w_CHANGEONFLY = This.RadioValue()
        Return .T.
    Endfunc

    Func oCHANGEONFLY_1_6.SetRadio()

        This.Value = ;
            iif(This.Parent.oContained.w_CHANGEONFLY==.T.,1,;
            0)
    Endfunc
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_foxcharteditor','','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Dialog Window"
Endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
