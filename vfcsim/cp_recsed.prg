* ---------------------------------------------------------------------------- *
* #%&%#Build:  59
*                                                                              *
*   Procedure: cp_recsed                                                       *
*              Security record detail                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-27                                                      *
* Last revis.: 2008-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
Parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
Local i_Obj
If Vartype(m.oParentObject)<>'O'
    Wait Wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
    Return(.Null.)
Endif
If i_cType='edit'
    Return(.Null.)
Else
    If i_cType='paint'
        i_Obj=Createobject("tcp_recsed")
        i_Obj.Cnt.oParentObject=oParentObject
        i_Obj.LinkPCClick()
    Else
        i_Obj=Createobject('stdLazyChild',oParentObject,Program())
    Endif
Endif
Return(i_Obj)

Proc _CreateObject(retobj)
    retobj=Createobject("tccp_recsed")
    Return

Proc _AddObject(i_oCnt,i_name)
    i_oCnt.AddObject(i_name,"tccp_recsed")
    Return

    * --- Class definition
Define Class tcp_recsed As StdPCForm
    Width  = 151
    Height = 215
    Top    = 10
    Left   = 7
    cComment = "Security record detail"
    cPrg = "cp_recsed"
    HelpContextID=107182393
    Add Object Cnt As tccp_recsed
Enddefine

Define Class tscp_recsed As PCContext
    w_GRPCODE = 0
    w_USRCODE = 0
    w_XGRPCODE = 0
    w_XUSRCODE = 0
    w_namegrp = Space(20)
    w_nameusr = Space(20)
    w_tablename = Space(50)
    w_progname = Space(50)
    w_rfrownum = 0
    Proc Save(i_oFrom)
        This.w_GRPCODE = i_oFrom.w_GRPCODE
        This.w_USRCODE = i_oFrom.w_USRCODE
        This.w_XGRPCODE = i_oFrom.w_XGRPCODE
        This.w_XUSRCODE = i_oFrom.w_XUSRCODE
        This.w_namegrp = i_oFrom.w_namegrp
        This.w_nameusr = i_oFrom.w_nameusr
        This.w_tablename = i_oFrom.w_tablename
        This.w_progname = i_oFrom.w_progname
        This.w_rfrownum = i_oFrom.w_rfrownum
        PCContext::Save(i_oFrom)
    Proc Load(i_oTo)
        i_oTo.w_GRPCODE = This.w_GRPCODE
        i_oTo.w_USRCODE = This.w_USRCODE
        i_oTo.w_XGRPCODE = This.w_XGRPCODE
        i_oTo.w_XUSRCODE = This.w_XUSRCODE
        i_oTo.w_namegrp = This.w_namegrp
        i_oTo.w_nameusr = This.w_nameusr
        i_oTo.w_tablename = This.w_tablename
        i_oTo.w_progname = This.w_progname
        i_oTo.w_rfrownum = This.w_rfrownum
        i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
        PCContext::Load(i_oTo)
Enddefine

Define Class tccp_recsed As StdPCTrsContainer
    Top    = 0
    Left   = 0

    * --- Standard Properties
    Width  = 151
    Height = 215
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    infodaterev="2008-10-08"
    HelpContextID=107182393
    max_rt_seq=9

    * --- Detail File Properties
    cTrsName=''

    * --- Constant Properties
    cprecsed_IDX = 0
    cpgroups_IDX = 0
    cpusers_IDX = 0
    cFile = "cprecsed"
    cKeySelect = "tablename,progname,rfrownum"
    cKeyWhere  = "tablename=this.w_tablename and progname=this.w_progname and rfrownum=this.w_rfrownum"
    cKeyDetail  = "GRPCODE=this.w_GRPCODE and USRCODE=this.w_USRCODE and tablename=this.w_tablename and progname=this.w_progname and rfrownum=this.w_rfrownum"
    cKeyWhereODBC = '"tablename="+cp_ToStrODBC(this.w_tablename)';
        +'+" and progname="+cp_ToStrODBC(this.w_progname)';
        +'+" and rfrownum="+cp_ToStrODBC(this.w_rfrownum)';

    cKeyDetailWhereODBC = '"GRPCODE="+cp_ToStrODBC(this.w_GRPCODE)';
        +'+" and USRCODE="+cp_ToStrODBC(this.w_USRCODE)';
        +'+" and tablename="+cp_ToStrODBC(this.w_tablename)';
        +'+" and progname="+cp_ToStrODBC(this.w_progname)';
        +'+" and rfrownum="+cp_ToStrODBC(this.w_rfrownum)';

    cKeyWhereODBCqualified = '"cprecsed.tablename="+cp_ToStrODBC(this.w_tablename)';
        +'+" and cprecsed.progname="+cp_ToStrODBC(this.w_progname)';
        +'+" and cprecsed.rfrownum="+cp_ToStrODBC(this.w_rfrownum)';

    cOrderByDett = ''
    cOrderByDettCustom = ''
    bApplyOrderDetail=.T.
    bApplyOrderDetailCustom=.T.
    cOrderByDett = 'cprecsed.GRPCODE,cprecsed.USRCODE'
    cPrg = "cp_recsed"
    cComment = "Security record detail"
    i_nRowNum = 0
    i_nRowPerPage = 7
    Icon = "movi.ico"
    i_lastcheckrow = 0
    * --- Area Manuale = Properties
    * --- Fine Area Manuale

    * --- Local Variables
    w_GRPCODE = 0
    o_GRPCODE = 0
    w_USRCODE = 0
    o_USRCODE = 0
    w_XGRPCODE = 0
    o_XGRPCODE = 0
    w_XUSRCODE = 0
    o_XUSRCODE = 0
    w_namegrp = Space(20)
    w_nameusr = Space(20)
    w_tablename = Space(50)
    w_progname = Space(50)
    w_rfrownum = 0
    * --- Area Manuale = Declare Variables
    * --- Fine Area Manuale

    * --- Define Page Frame
    Add Object oPgFrm As StdPCPageFrame With PageCount=1, Width=This.Width, Height=This.Height
    Proc oPgFrm.Init
        StdPCPageFrame::Init()
        With This
            .Pages(1).AddObject("oPag","tcp_recsedPag1","cp_recsed",1)
            .Pages(1).oPag.Visible=.T.
            .Pages(1).Caption=cp_Translate("Pag.1")
        Endwith
        This.Parent.oFirstControl = .Null.
        This.Parent.cComment=cp_Translate(This.Parent.cComment)
        * --- Area Manuale = Init Page Frame
        * --- Fine Area Manuale
    Endproc
    Proc Init(oParentObject)
        If Vartype(m.oParentObject)<>'L'
            This.oParentObject=oParentObject
        Endif
        DoDefault()

        * --- Open tables
    Function OpenWorkTables()
        Dimension This.cWorkTables[3]
        This.cWorkTables[1]='cpgroups'
        This.cWorkTables[2]='cpusers'
        This.cWorkTables[3]='cprecsed'
        * --- Area Manuale = Open Work Table
        * --- Fine Area Manuale
        Return(This.OpenAllTables(3))

    Procedure SetPostItConn()
        This.bPostIt=i_ServerConn[i_TableProp[this.cprecsed_IDX,5],7]
        This.nPostItConn=i_TableProp[this.cprecsed_IDX,3]
        Return

    Procedure NewContext()
        Return(Createobject('tscp_recsed'))


        * --- Read record and initialize Form variables
    Procedure LoadRec()
        Local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
        Local i_cSel,i_cDatabaseType,i_nFlds
        * --- Area Manuale = Load Record Init
        * --- Fine Area Manuale
        If Not(This.bOnScreen)
            This.nDeferredFillRec=1
            Return
        Endif
        If This.cFunction='Load'
            This.BlankRec()
            Return
        Endif
        This.nDeferredFillRec=0
        This.bF10=.F.
        This.bUpdated=.F.
        This.bHeaderUpdated=.F.
        * --- Select reading the record
        *
        * select * from cprecsed where GRPCODE=KeySet.GRPCODE
        *                            and USRCODE=KeySet.USRCODE
        *                            and tablename=KeySet.tablename
        *                            and progname=KeySet.progname
        *                            and rfrownum=KeySet.rfrownum
        *
        i_cOrder = ''
        If This.bApplyOrderDetail
            If This.bApplyOrderDetailCustom And Not Empty(This.cOrderByDettCustom)
                i_cOrder = 'order by '+This.cOrderByDettCustom
            Else
                If Not Empty(This.cOrderByDett)
                    i_cOrder = 'order by '+This.cOrderByDett
                Endif
            Endif
        Endif
        * --- Area Manuale = Before Load Detail
        * --- Fine Area Manuale
        If Used(This.cTrsName+'_bis')
            Select (This.cTrsName+'_bis')
            Zap
        Endif
        i_nConn = i_TableProp[this.cprecsed_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.cprecsed_IDX,2],This.bLoadRecFilter,This.cprecsed_IDX,"cp_recsed")
        If i_nConn<>0
            i_nFlds = i_dcx.getfieldscount('cprecsed')
            i_cDatabaseType = cp_GetDatabaseType(i_nConn)
            i_cSel = "cprecsed.*"
            i_cKey = This.cKeyWhereODBCqualified
            i_cTable = i_cTable+' cprecsed '
            i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,This.cCursor)
            This.bLoaded = i_nRes<>-1 And Not(Eof())
        Else
            * i_cKey = this.cKeyWhere
            i_cKey = cp_PKFox(i_cTable  ,'tablename',This.w_tablename  ,'progname',This.w_progname  ,'rfrownum',This.w_rfrownum  )
            Select * From (i_cTable) cprecsed Where &i_cKey &i_cOrder Into Cursor (This.cCursor) nofilter
            This.bLoaded = Not(Eof())
        Endif
        i_cTF = This.cCursor
        * --- Copy values in work variables
        If This.bLoaded
            With This
                .w_tablename = Nvl(tablename,Space(50))
                .w_progname = Nvl(progname,Space(50))
                .w_rfrownum = Nvl(rfrownum,0)
                .oPgFrm.Page1.oPag.oObj_1_8.Calculate(MSG_GROUPS)
                .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_USERS)
                .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_GROUP_DESC)
                .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_USER_DESC)
                .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_GROUP_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_USER_TOOLTIP)
                cp_LoadRecExtFlds(This,'cprecsed')
            Endwith
            * === TEMPORARY
            Select (This.cTrsName)
            Zap
            Select (This.cCursor)
            This.i_nRowNum = 0
            Scan
                With This
                    .w_namegrp = Space(20)
                    .w_nameusr = Space(20)
                    .w_GRPCODE = Nvl(GRPCODE,0)
                    .w_USRCODE = Nvl(USRCODE,0)
                    .w_XGRPCODE = Iif(.w_GRPCODE=-1, 0, .w_GRPCODE)
                    .link_2_3('Load')
                    .w_XUSRCODE = Iif(.w_XGRPCODE<>0, 0, Iif(.w_USRCODE=-1,0,.w_USRCODE))
                    .link_2_4('Load')
                    Select (This.cTrsName)
                    Append Blank
                    Replace CPCCCHK With &i_cTF..CPCCCHK
                    .TrsFromWork()
                    Replace GRPCODE With .w_GRPCODE
                    Replace USRCODE With .w_USRCODE
                    Replace I_SRV With " "
                    .nLastRow = Recno()
                    Select (This.cCursor)
                Endwith
            Endscan
            This.nFirstrow=1
            Select (This.cCursor)
            Go Top
            Select (This.cTrsName)
            With This
                .oPgFrm.Page1.oPag.oObj_1_8.Calculate(MSG_GROUPS)
                .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_USERS)
                .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_GROUP_DESC)
                .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_USER_DESC)
                .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_GROUP_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_USER_TOOLTIP)
            Endwith
            Go Top
            With This
                .oPgFrm.Page1.oPag.oBody.Refresh()
                .WorkFromTrs()
                .mCalcRowObjs()
                .SaveDependsOn()
                .SetControlsValue()
                .mHideControls()
                .ChildrenChangeRow()
                .oPgFrm.Page1.oPag.oBody.nAbsRow=1
                .oPgFrm.Page1.oPag.oBody.nRelRow=1
                .NotifyEvent('Load')
            Endwith
        Else
            This.BlankRec()
        Endif
        * --- Area Manuale = Load Record End
        * --- Fine Area Manuale
    Endproc

    * --- Blanking Form variables
    Procedure BlankRec()
        * --- Area Manuale = Blank Record Init
        * --- Fine Area Manuale
        If Not(This.bOnScreen)
            This.nDeferredFillRec=2
            Return
        Endif
        This.nDeferredFillRec=0
        This.bUpdated=.F.
        This.bHeaderUpdated=.F.
        This.bLoaded=.F.
        If Used(This.cTrsName+'_bis')
            Select (This.cTrsName+'_bis')
            Zap
        Endif
        Select (This.cTrsName)
        Zap
        Append Blank
        This.nLastRow  = Recno()
        This.nFirstrow = 1
        Replace I_SRV    With "A"
        With This
            .w_GRPCODE=0
            .w_USRCODE=0
            .w_XGRPCODE=0
            .w_XUSRCODE=0
            .w_namegrp=Space(20)
            .w_nameusr=Space(20)
            .w_tablename=Space(50)
            .w_progname=Space(50)
            .w_rfrownum=0
            If .cFunction<>"Filter"
                .w_GRPCODE = Iif(.w_XGRPCODE=0,-1,.w_XGRPCODE)
                .w_USRCODE = Iif(.w_XUSRCODE=0 Or .w_XGRPCODE<>0,-1,.w_XUSRCODE)
                .w_XGRPCODE = Iif(.w_GRPCODE=-1, 0, .w_GRPCODE)
                .DoRTCalc(3,3,.F.)
                If Not(Empty(.w_XGRPCODE))
                    .link_2_3('Full')
                Endif
                .w_XUSRCODE = Iif(.w_XGRPCODE<>0, 0, Iif(.w_USRCODE=-1,0,.w_USRCODE))
                .DoRTCalc(4,4,.F.)
                If Not(Empty(.w_XUSRCODE))
                    .link_2_4('Full')
                Endif
                .oPgFrm.Page1.oPag.oObj_1_8.Calculate(MSG_GROUPS)
                .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_USERS)
                .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_GROUP_DESC)
                .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_USER_DESC)
                .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_GROUP_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_USER_TOOLTIP)
            Endif
        Endwith
        cp_BlankRecExtFlds(This,'cprecsed')
        This.DoRTCalc(5,9,.F.)
        This.SaveDependsOn()
        This.SetControlsValue()
        This.TrsFromWork()
        This.mHideControls()
        This.ChildrenChangeRow()
        This.oPgFrm.Page1.oPag.oBody.nAbsRow=0
        This.oPgFrm.Page1.oPag.oBody.nRelRow=1
        This.NotifyEvent('Blank')
        * --- Area Manuale = Blank Record End
        * --- Fine Area Manuale
    Endproc

    * --- Procedure for field enabling
    *     cOp = Setting operation
    *     Allowed Parameters: Load - Query - Edit - Filter
    *     Load and Filter set all fields
    *
    Procedure SetEnabled(i_cOp)
        Local i_bVal
        * --- Area Manuale = Enable Controls Init
        * --- Fine Area Manuale
        i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
        * --- Disabling List page when <> from Query
        With This.oPgFrm
            .Page1.oPag.oObj_1_8.Enabled = i_bVal
            .Page1.oPag.oObj_1_9.Enabled = i_bVal
            .Page1.oPag.oObj_1_10.Enabled = i_bVal
            .Page1.oPag.oObj_1_11.Enabled = i_bVal
            .Page1.oPag.oObj_1_12.Enabled = i_bVal
            .Page1.oPag.oObj_1_13.Enabled = i_bVal
            .Page1.oPag.oBody.Enabled = .T.
            .Page1.oPag.oBody.oBodyCol.Enabled = i_bVal
        Endwith
        cp_SetEnabledExtFlds(This,'cprecsed',i_cOp)
        * --- Area Manuale = Enable Controls End
        * --- Fine Area Manuale
    Endproc

    *procedure SetChildrenStatus
    *endproc

    * ------ Redefined procedures from Standard Form

    * --- Generate  filter
    Func BuildFilter()
        Local i_cFlt,i_nConn
        i_nConn = i_TableProp[this.cprecsed_IDX,3]
        i_cFlt = This.cQueryFilter
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_tablename,"tablename",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_progname,"progname",i_nConn)
        i_cFlt = cp_BuildWhere(i_cFlt,This.w_rfrownum,"rfrownum",i_nConn)
        * --- Area Manuale = Build Filter
        * --- Fine Area Manuale
        Return (i_cFlt)
    Endfunc

    *
    *  --- Transaction procedures
    *
    Proc CreateTrs
        This.cTrsName=Sys(2015)
        Create Cursor (This.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
            ,t_XGRPCODE N(5);
            ,t_XUSRCODE N(5);
            ,t_namegrp C(20);
            ,t_nameusr C(20);
            ,GRPCODE N(5);
            ,USRCODE N(5);
            ,t_GRPCODE N(5);
            ,t_USRCODE N(5);
            )
        This.oPgFrm.Page1.oPag.oBody.ColumnCount=0
        This.oPgFrm.Page1.oPag.oBody.RecordSource=This.cTrsName
        This.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tcp_recsedbodyrow')
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.Width=This.oPgFrm.Page1.oPag.oBody.Width
        This.oPgFrm.Page1.oPag.oBody.RowHeight=This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.Height
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.F.
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.Visible=.T.
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=This
        * --- Row charateristics
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXGRPCODE_2_3.ControlSource=This.cTrsName+'.t_XGRPCODE'
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXUSRCODE_2_4.ControlSource=This.cTrsName+'.t_XUSRCODE'
        This.oPgFrm.Page1.oPag.onamegrp_2_5.ControlSource=This.cTrsName+'.t_namegrp'
        This.oPgFrm.Page1.oPag.onameusr_2_6.ControlSource=This.cTrsName+'.t_nameusr'
        This.oPgFrm.Page1.oPag.oBody.ZOrder(1)
        This.oPgFrm.Page1.oPag.oBody3D.ZOrder(1)
        This.oPgFrm.Page1.oPag.pagebmp.ZOrder(1)
        This.oPgFrm.Page1.oPag.oBody.oFirstControl=This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXGRPCODE_2_3
        * --- New table already open in exclusive mode
        * --- Area Manuale = Create Trs
        * --- Fine Area Manuale

    Function mInsert()
        Local i_nConn,i_cTable,i_extfld,i_extval
        * --- Area Manuale = Insert Master Init
        * --- Fine Area Manuale
        i_nConn = i_TableProp[this.cprecsed_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.cprecsed_IDX,2])
        * --- Area Manuale = Autonum Assigned
        * --- Fine Area Manuale
        * --- Area Manuale = Insert Master End
        * --- Fine Area Manuale
        Return(Not(bTrsErr))

        * --- Insert new Record in Detail table
    Function mInsertDetail(i_nCntLine)
        Local i_cKey,i_nConn,i_cTable,i_TN
        * --- Area Manuale = Insert Detail Init
        * --- Fine Area Manuale
        If This.nDeferredFillRec<>0
            Return
        Endif
        If This.bUpdated .Or. This.IsAChildUpdated()
            i_nConn = i_TableProp[this.cprecsed_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.cprecsed_IDX,2])
            *
            * insert into cprecsed
            *
            i_TN = This.cTrsName
            This.NotifyEvent('Insert row start')
            Local i_cFldBody,i_cFldValBody
            If i_nConn<>0
                i_extfld=cp_InsertFldODBCExtFlds(This,'cprecsed')
                i_extval=cp_InsertValODBCExtFlds(This,'cprecsed')
                i_cFldBody=" "+;
                    "(GRPCODE,USRCODE,tablename,progname,rfrownum,CPCCCHK"+i_extfld+")"
                i_cFldValBody=" "+;
                    "("+cp_ToStrODBC(This.w_GRPCODE)+","+cp_ToStrODBC(This.w_USRCODE)+","+cp_ToStrODBC(This.w_tablename)+","+cp_ToStrODBC(This.w_progname)+","+cp_ToStrODBC(This.w_rfrownum)+;
                    ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
                =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
            Else
                i_extfld=cp_InsertFldVFPExtFlds(This,'cprecsed')
                i_extval=cp_InsertValVFPExtFlds(This,'cprecsed')
                cp_CheckDeletedKey(i_cTable,0,'GRPCODE',This.w_GRPCODE,'USRCODE',This.w_USRCODE,'tablename',This.w_tablename,'progname',This.w_progname,'rfrownum',This.w_rfrownum)
                Insert Into (i_cTable) (;
                    GRPCODE;
                    ,USRCODE;
                    ,tablename;
                    ,progname;
                    ,rfrownum;
                    ,CPCCCHK &i_extfld.) Values (;
                    this.w_GRPCODE;
                    ,This.w_USRCODE;
                    ,This.w_tablename;
                    ,This.w_progname;
                    ,This.w_rfrownum;
                    ,cp_NewCCChk() &i_extval. )
            Endif
            This.NotifyEvent('Insert row end')
        Endif
        * --- Area Manuale = Insert Detail End
        * --- Fine Area Manuale
        Return(Not(bTrsErr))

        * --- Update Database
    Function mReplace(i_bEditing)
        Local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
        Local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
        * --- Area Manuale = Replace Init
        * --- Fine Area Manuale
        If This.nDeferredFillRec<>0
            Return
        Endif
        If Not(This.bLoaded)
            This.mInsert()
            i_bEditing=.F.
        Else
            i_bEditing=.T.
        Endif
        If This.bUpdated
            i_nConn = i_TableProp[this.cprecsed_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.cprecsed_IDX,2])
            i_nModRow = 1
            If i_bEditing .And. This.bHeaderUpdated
                This.mRestoreTrs()
            Endif
            If This.bHeaderUpdated
                This.mUpdateTrs()
            Endif
            If This.bHeaderUpdated And i_bEditing
                Select (This.cTrsName)
                i_TN = This.cTrsName
                i_bUpdAll = .F.
                Go Top
                If i_bEditing And I_SRV<>'A'
                Endif
                If Not(i_bUpdAll)
                    Scan For (t_GRPCODE>0 Or t_USRCODE>0) And (I_SRV<>"U" And I_SRV<>"A")
                        i_OldCCCHK=Iif(i_bEditing,&i_TN..CPCCCHK,'')
                        * --- Updating Master table
                        This.NotifyEvent('Update start')
                        If i_nConn<>0
                            i_extfld=cp_ReplaceODBCExtFlds(This,'cprecsed')
                            i_cWhere = This.cKeyWhereODBC
                            i_nnn="UPDATE "+i_cTable+" SET "+;
                                "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                                " and GRPCODE="+cp_ToStrODBC(&i_TN.->GRPCODE)+;
                                " and USRCODE="+cp_ToStrODBC(&i_TN.->USRCODE)+;
                                " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                            i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
                        Else
                            i_extfld=cp_ReplaceVFPExtFlds(This,'cprecsed')
                            i_cWhere = This.cKeyWhere
                            Update (i_cTable) Set ;
                                CPCCCHK=cp_NewCCChk() &i_extfld. Where &i_cWhere;
                                and GRPCODE=&i_TN.->GRPCODE;
                                and USRCODE=&i_TN.->USRCODE;
                                and CPCCCHK==i_OldCCCHK
                            i_nModRow=_Tally
                        Endif
                        This.NotifyEvent('Update end')
                        If i_nModRow<1
                            Exit
                        Endif
                    Endscan
                    * --- Make the last record the actual position (mcalc problem with double transitory)
                    This.SetRow(Reccount(This.cTrsName), .F.)
                    This.SetControlsValue()
                Endif
            Endif

            * --- Update Detail table
            If i_nModRow>0   && Master table updated with success
                Set Delete Off
                Select (This.cTrsName)
                i_TN = This.cTrsName
                i_bUpdAll = .F.
                Scan For (t_GRPCODE>0 Or t_USRCODE>0) And ((I_SRV="U" Or i_bUpdAll) Or I_SRV="A" Or Deleted())
                    This.WorkFromTrs()
                    This.oPgFrm.Page1.oPag.oBody.nAbsRow=Recno()
                    i_OldCCCHK=Iif(i_bEditing.Or.I_SRV<>"A",&i_TN..CPCCCHK,'')
                    i_nRec = Recno()
                    Set Delete On
                    If Deleted()
                        If I_SRV<>"A"
                            * --- Delete from database an erased row
                            This.NotifyEvent('Delete row start')
                            This.mRestoreTrsDetail()
                            If i_nConn<>0
                                i_cWhere = This.cKeyWhereODBC
                                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                                    " WHERE "+&i_cWhere+;
                                    " and GRPCODE="+cp_ToStrODBC(&i_TN.->GRPCODE)+;
                                    " and USRCODE="+cp_ToStrODBC(&i_TN.->USRCODE)+;
                                    " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
                            Else
                                i_cWhere = This.cKeyWhere
                                Delete From (i_cTable) Where &i_cWhere;
                                    and GRPCODE=&i_TN.->GRPCODE;
                                    and USRCODE=&i_TN.->USRCODE;
                                    and CPCCCHK==i_OldCCCHK
                                i_nModRow=_Tally
                            Endif
                            This.NotifyEvent('Delete row end')
                        Endif
                    Else
                        If I_SRV="A"
                            * --- Insert new row in database
                            This.mUpdateTrsDetail()
                            i_NR = 0
                            Replace GRPCODE With This.w_GRPCODE
                            Replace USRCODE With This.w_USRCODE
                            =This.mInsertDetail(i_NR)
                        Else
                            * --- Area Manuale = Replace Loop
                            * --- Fine Area Manuale
                            *
                            * update cprecsed
                            *
                            This.NotifyEvent('Update row start')
                            This.mRestoreTrsDetail()
                            This.mUpdateTrsDetail()
                            If i_nConn<>0
                                i_extfld=cp_ReplaceODBCExtFlds(This,'cprecsed')
                                i_cWhere = This.cKeyWhereODBC
                                i_nnn="UPDATE "+i_cTable+" SET "+;
                                    "GRPCODE="+cp_ToStrODBC(This.w_GRPCODE)+;
                                    ",USRCODE="+cp_ToStrODBC(This.w_USRCODE)+;
                                    ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                                    " WHERE "+&i_cWhere+;
                                    " and GRPCODE="+cp_ToStrODBC(GRPCODE)+;
                                    " and USRCODE="+cp_ToStrODBC(USRCODE)+;
                                    " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
                            Else
                                i_extfld=cp_ReplaceVFPExtFlds(This,'cprecsed')
                                i_cWhere = This.cKeyWhere
                                Update (i_cTable) Set ;
                                    GRPCODE=This.w_GRPCODE;
                                    ,USRCODE=This.w_USRCODE;
                                    ,CPCCCHK=cp_NewCCChk() &i_extfld. Where &i_cWhere;
                                    and GRPCODE=&i_TN.->GRPCODE;
                                    and USRCODE=&i_TN.->USRCODE;
                                    and CPCCCHK==i_OldCCCHK
                                i_nModRow=_Tally
                            Endif
                            This.NotifyEvent('Update row end')
                        Endif
                    Endif
                    If i_nModRow<1
                        Exit
                    Endif
                    Set Delete Off
                Endscan
                Set Delete On
                * --- Make the last record the actual position (mcalc problem with double transitory)
                This.SetRow(Reccount(This.cTrsName), .F.)
                This.SetControlsValue()
            Endif
            =cp_CheckMultiuser(i_nModRow)
        Endif
        If Not(bTrsErr)
        Endif
        * --- Area Manuale = Replace End
        * --- Fine Area Manuale
        Return(bTrsErr)

        * --- Delete Records
    Function mDelete()
        Local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
        Local i_cDel,i_nRec
        * --- Area Manuale = Delete Init
        * --- Fine Area Manuale
        Local i_bLoadNow
        If This.nDeferredFillRec<>0  && Record is not loaded
            i_bLoadNow=.T.
            This.bOnScreen=.T.         && Force loadrec
            This.LoadRec()             && Record loaded, timestamp correct
        Endif
        If This.nDeferredFillRec=0 And This.bLoaded=.F.
            If i_bLoadNow
                This.bOnScreen=.F.
            Endif
            Return
        Endif
        *
        If Not(bTrsErr)
            i_nConn = i_TableProp[this.cprecsed_IDX,3]
            i_cTable = cp_SetAzi(i_TableProp[this.cprecsed_IDX,2])
            This.NotifyEvent("Delete Start")
            Select (This.cTrsName)
            i_TN = This.cTrsName
            i_nModRow = 1
            i_cDel = Set('DELETED')
            Set Delete Off
            Scan For (t_GRPCODE>0 Or t_USRCODE>0) And I_SRV<>'A'
                This.WorkFromTrs()
                i_OldCCCHK=&i_TN..CPCCCHK
                *
                * delete cprecsed
                *
                This.NotifyEvent('Delete row start')
                If i_nConn<>0
                    i_cWhere = This.cKeyWhereODBC
                    i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                        " WHERE "+&i_cWhere+;
                        " and GRPCODE="+cp_ToStrODBC(&i_TN.->GRPCODE)+;
                        " and USRCODE="+cp_ToStrODBC(&i_TN.->USRCODE)+;
                        " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
                Else
                    i_cWhere = This.cKeyWhere
                    Delete From (i_cTable) Where &i_cWhere;
                        and GRPCODE=&i_TN.->GRPCODE;
                        and USRCODE=&i_TN.->USRCODE;
                        and CPCCCHK==i_OldCCCHK
                    i_nModRow=_Tally
                Endif
                This.NotifyEvent('Delete row end')
                If i_nModRow<1
                    Exit
                Endif
            Endscan
            Set Delete &i_cDel
            * --- Make the last record the actual position (mcalc problem with double transitory)
            This.SetRow(Reccount(This.cTrsName), .F.)
            This.SetControlsValue()
            =cp_CheckMultiuser(i_nModRow)
            If Not(bTrsErr)
                This.mRestoreTrs()
                Select (This.cTrsName)
                i_TN = This.cTrsName
                i_cDel = Set('DELETED')
                Set Delete Off
                Scan For (t_GRPCODE>0 Or t_USRCODE>0) And I_SRV<>'A'
                    i_OldCCCHK=&i_TN..CPCCCHK
                    This.mRestoreTrsDetail()
                Endscan
                Set Delete &i_cDel
                * --- Make the last record the actual position (mcalc problem with double transitory)
                This.SetRow(Reccount(This.cTrsName), .F.)
                This.SetControlsValue()
            Endif
            This.NotifyEvent("Delete End")
        Endif
        *
        If i_bLoadNow
            This.bOnScreen=.F.
        Endif
        *
        This.mDeleteWarnings()
        * --- Area Manuale = Delete End
        * --- Fine Area Manuale
        Return

        * --- Calculations
    Function mCalc(i_bUpd)
        This.bCalculating=.T.
        Local i_cTable,i_nConn
        i_nConn = i_TableProp[this.cprecsed_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.cprecsed_IDX,2])
        If i_bUpd
            With This
                If .o_XGRPCODE<>.w_XGRPCODE.Or. .o_XUSRCODE<>.w_XUSRCODE
                    .w_GRPCODE = Iif(.w_XGRPCODE=0,-1,.w_XGRPCODE)
                Endif
                If .o_XUSRCODE<>.w_XUSRCODE.Or. .o_XGRPCODE<>.w_XGRPCODE
                    .w_USRCODE = Iif(.w_XUSRCODE=0 Or .w_XGRPCODE<>0,-1,.w_XUSRCODE)
                Endif
                If .o_GRPCODE<>.w_GRPCODE
                    .w_XGRPCODE = Iif(.w_GRPCODE=-1, 0, .w_GRPCODE)
                    .link_2_3('Full')
                Endif
                If .o_USRCODE<>.w_USRCODE.Or. .o_XGRPCODE<>.w_XGRPCODE
                    .w_XUSRCODE = Iif(.w_XGRPCODE<>0, 0, Iif(.w_USRCODE=-1,0,.w_USRCODE))
                    .link_2_4('Full')
                Endif
                .oPgFrm.Page1.oPag.oObj_1_8.Calculate(MSG_GROUPS)
                .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_USERS)
                .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_GROUP_DESC)
                .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_USER_DESC)
                .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_GROUP_TOOLTIP)
                .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_USER_TOOLTIP)
                * --- Area Manuale = Calculate
                * --- Fine Area Manuale
            Endwith
            This.DoRTCalc(5,9,.T.)
            This.SetControlsValue()
            If Inlist(This.cFunction,'Edit','Load')
                This.bUpdated=.T.
                This.mEnableControls()
            Endif
            Replace t_GRPCODE With This.w_GRPCODE
            Replace t_USRCODE With This.w_USRCODE
        Endif
        This.bCalculating=.F.
        Return

    Proc mCalcObjs()
        With This
            .oPgFrm.Page1.oPag.oObj_1_8.Calculate(MSG_GROUPS)
            .oPgFrm.Page1.oPag.oObj_1_9.Calculate(MSG_USERS)
            .oPgFrm.Page1.oPag.oObj_1_10.Calculate(MSG_GROUP_DESC)
            .oPgFrm.Page1.oPag.oObj_1_11.Calculate(MSG_USER_DESC)
            .oPgFrm.Page1.oPag.oObj_1_12.Calculate(MSG_GROUP_TOOLTIP)
            .oPgFrm.Page1.oPag.oObj_1_13.Calculate(MSG_USER_TOOLTIP)
        Endwith
        Return

    Proc mCalcRowObjs()
        With This
        Endwith
        Return

        * --- Enable controls under condition
    Procedure mEnableControls()
        This.mEnableControlsFixed()
        This.mHideControls()
        DoDefault()
        Return

        * --- Enable controls under condition for Grid and for FixedPos fields
    Procedure mEnableControlsFixed()
        This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXUSRCODE_2_4.Enabled = Inlist(This.cFunction,"Edit","Load") And This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXUSRCODE_2_4.mCond()
        This.mHideControls()
        DoDefault()
        Return

    Procedure mHideControls()
        This.mHideRowControls()
        DoDefault()
        Return

    Procedure mHideRowControls()
        DoDefault()
        Return


        * --- NotifyEvent
    Function NotifyEvent(cEvent)
        Local bRefresh,i_oldevt
        bRefresh=.F.
        i_oldevt=This.currentEvent
        This.currentEvent=cEvent
        * --- Area Manuale = Notify Event Init
        * --- Fine Area Manuale
        With This
            .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
            .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
        Endwith
        * --- Area Manuale = Notify Event End
        * --- Fine Area Manuale
        DoDefault(cEvent)
        If bRefresh
            This.SetControlsValue()
        Endif
        This.currentEvent=i_oldevt
        Return

        * --- Link procedure for entity name=XGRPCODE
    Func link_2_3(i_cCtrl,oSource)
        Local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
        i_nConn = i_TableProp[this.cpgroups_IDX,3]
        i_lTable = "cpgroups"
        If This.cFunction='Load'
            i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2], .T., This.cpgroups_IDX)
        Else
            i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])
        Endif
        i_nArea = Select()
        i_bEmpty = .F.
        If Used("_Link_")
            Select _Link_
            Use
        Endif
        Do Case
            Case Empty(This.w_XGRPCODE) And i_cCtrl<>"Drop"
                i_bEmpty = .T.
                i_reccount=0
            Case i_cCtrl='Part'
                i_cFlt=cp_QueryEntityFilter('',True,'cpgroups')
                If i_nConn<>0
                    i_cWhere = " code="+cp_ToStrODBC(This.w_XGRPCODE);

                    i_ret=cp_SQL(i_nConn,"select code,name";
                        +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
                    i_reccount = Iif(i_ret=-1,0,Reccount())
                Else
                    i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                        ,'code',This.w_XGRPCODE)
                    Select Code,Name;
                        from (i_cTable) As (i_lTable) Where &i_cWhere. Into Cursor _Link_
                    i_reccount = _Tally
                Endif
                If i_reccount>1
                    If !Empty(This.w_XGRPCODE) And !This.bDontReportError
                        deferred_cp_zoom('cpgroups','*','code',cp_AbsName(oSource.Parent,'oXGRPCODE_2_3'),i_cWhere,'',"",'',This)
                    Endif
                    This.bDontReportError = .T.
                    i_reccount = 0

                Endif
            Case i_cCtrl='Drop'
                If i_nConn<>0
                    i_ret=cp_SQL(i_nConn,"select code,name";
                        +" from "+i_cTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                        ,"_Link_")
                    i_reccount = Iif(i_ret=-1,0,Reccount())
                Else
                    i_cWhere = cp_PKFox(i_cTable;
                        ,'code',oSource.xKey(1))
                    Select Code,Name;
                        from (i_cTable) Where &i_cWhere. Into Cursor _Link_
                    i_reccount = _Tally
                Endif
            Case i_cCtrl='Full' Or i_cCtrl='Load' Or i_cCtrl='Extend'
                If .Not. Empty(This.w_XGRPCODE)
                    If i_nConn<>0
                        i_ret=cp_SQL(i_nConn,"select code,name";
                            +" from "+i_cTable+" where code="+cp_ToStrODBC(This.w_XGRPCODE);
                            ,"_Link_")
                        i_reccount = Iif(i_ret=-1,0,Reccount())
                    Else
                        i_cWhere = cp_PKFox(i_cTable;
                            ,'code',This.w_XGRPCODE)
                        Select Code,Name;
                            from (i_cTable) Where &i_cWhere. Into Cursor _Link_
                        i_reccount = _Tally
                    Endif
                Else
                    i_reccount = 0
                Endif
        Endcase
        If i_reccount>0 And Used("_Link_")
            This.w_XGRPCODE = Nvl(_Link_.Code,0)
            This.w_namegrp = Nvl(_Link_.Name,Space(20))
        Else
            If i_cCtrl<>'Load'
                This.w_XGRPCODE = 0
            Endif
            This.w_namegrp = Space(20)
        Endif
        i_bRes=i_reccount=1
        If i_bRes And i_cCtrl<>'Full' And i_cCtrl<>'Load' And Used("_Link_")
            i_cKey = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])+'\'+cp_ToStr(_Link_.Code,1)
            cp_ShowWarn(i_cKey,This.cpgroups_IDX)
        Endif
        If i_cCtrl='Drop'
            This.TrsFromWork()
            This.NotifyEvent('w_XGRPCODE Changed')
        Endif
        Select (i_nArea)
        Return(i_bRes Or i_bEmpty)
    Endfunc

    * --- Link procedure for entity name=XUSRCODE
    Func link_2_4(i_cCtrl,oSource)
        Local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
        i_nConn = i_TableProp[this.cpusers_IDX,3]
        i_lTable = "cpusers"
        If This.cFunction='Load'
            i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .T., This.cpusers_IDX)
        Else
            i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
        Endif
        i_nArea = Select()
        i_bEmpty = .F.
        If Used("_Link_")
            Select _Link_
            Use
        Endif
        Do Case
            Case Empty(This.w_XUSRCODE) And i_cCtrl<>"Drop"
                i_bEmpty = .T.
                i_reccount=0
            Case i_cCtrl='Part'
                i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
                If i_nConn<>0
                    i_cWhere = " code="+cp_ToStrODBC(This.w_XUSRCODE);

                    i_ret=cp_SQL(i_nConn,"select code,name";
                        +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
                    i_reccount = Iif(i_ret=-1,0,Reccount())
                Else
                    i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                        ,'code',This.w_XUSRCODE)
                    Select Code,Name;
                        from (i_cTable) As (i_lTable) Where &i_cWhere. Into Cursor _Link_
                    i_reccount = _Tally
                Endif
                If i_reccount>1
                    If !Empty(This.w_XUSRCODE) And !This.bDontReportError
                        deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.Parent,'oXUSRCODE_2_4'),i_cWhere,'',"",'',This)
                    Endif
                    This.bDontReportError = .T.
                    i_reccount = 0

                Endif
            Case i_cCtrl='Drop'
                If i_nConn<>0
                    i_ret=cp_SQL(i_nConn,"select code,name";
                        +" from "+i_cTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                        ,"_Link_")
                    i_reccount = Iif(i_ret=-1,0,Reccount())
                Else
                    i_cWhere = cp_PKFox(i_cTable;
                        ,'code',oSource.xKey(1))
                    Select Code,Name;
                        from (i_cTable) Where &i_cWhere. Into Cursor _Link_
                    i_reccount = _Tally
                Endif
            Case i_cCtrl='Full' Or i_cCtrl='Load' Or i_cCtrl='Extend'
                If .Not. Empty(This.w_XUSRCODE)
                    If i_nConn<>0
                        i_ret=cp_SQL(i_nConn,"select code,name";
                            +" from "+i_cTable+" where code="+cp_ToStrODBC(This.w_XUSRCODE);
                            ,"_Link_")
                        i_reccount = Iif(i_ret=-1,0,Reccount())
                    Else
                        i_cWhere = cp_PKFox(i_cTable;
                            ,'code',This.w_XUSRCODE)
                        Select Code,Name;
                            from (i_cTable) Where &i_cWhere. Into Cursor _Link_
                        i_reccount = _Tally
                    Endif
                Else
                    i_reccount = 0
                Endif
        Endcase
        If i_reccount>0 And Used("_Link_")
            This.w_XUSRCODE = Nvl(_Link_.Code,0)
            This.w_nameusr = Nvl(_Link_.Name,Space(20))
        Else
            If i_cCtrl<>'Load'
                This.w_XUSRCODE = 0
            Endif
            This.w_nameusr = Space(20)
        Endif
        i_bRes=i_reccount=1
        If i_bRes And i_cCtrl<>'Full' And i_cCtrl<>'Load' And Used("_Link_")
            i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.Code,1)
            cp_ShowWarn(i_cKey,This.cpusers_IDX)
        Endif
        If i_cCtrl='Drop'
            This.TrsFromWork()
            This.NotifyEvent('w_XUSRCODE Changed')
        Endif
        Select (i_nArea)
        Return(i_bRes Or i_bEmpty)
    Endfunc

    Function SetControlsValue()
        Select (This.cTrsName)
        If Not(This.oPgFrm.Page1.oPag.onamegrp_2_5.Value==This.w_namegrp)
            This.oPgFrm.Page1.oPag.onamegrp_2_5.Value=This.w_namegrp
            Replace t_namegrp With This.oPgFrm.Page1.oPag.onamegrp_2_5.Value
        Endif
        If Not(This.oPgFrm.Page1.oPag.onameusr_2_6.Value==This.w_nameusr)
            This.oPgFrm.Page1.oPag.onameusr_2_6.Value=This.w_nameusr
            Replace t_nameusr With This.oPgFrm.Page1.oPag.onameusr_2_6.Value
        Endif
        If Not(This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXGRPCODE_2_3.Value==This.w_XGRPCODE)
            This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXGRPCODE_2_3.Value=This.w_XGRPCODE
            Replace t_XGRPCODE With This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXGRPCODE_2_3.Value
        Endif
        If Not(This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXUSRCODE_2_4.Value==This.w_XUSRCODE)
            This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXUSRCODE_2_4.Value=This.w_XUSRCODE
            Replace t_XUSRCODE With This.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oXUSRCODE_2_4.Value
        Endif
        cp_SetControlsValueExtFlds(This,'cprecsed')
    Endfunc


    * --- CheckForm
    Func CheckForm()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            If .bUpdated
                Do Case
                    Otherwise
                        i_bRes = .CheckRow()
                        If .Not. i_bRes
                            If !Isnull(.oNewFocus)
                                If .oPgFrm.ActivePage>1
                                    .oPgFrm.ActivePage=1
                                Endif
                                .oNewFocus.SetFocus()
                                .oNewFocus=.Null.
                            Endif
                        Endif
                        * return(i_bRes)
                Endcase
            Endif
            * --- Area Manuale = Check Form
            * --- Fine Area Manuale
            If Not(i_bnoObbl)
                cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
                Return(i_bRes)
            Endif
            If Not(i_bnoChk)
                cp_ErrorMsg(i_cErrorMsg)
                Return(i_bRes)
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

    * --- CheckRow
    Func CheckRow()
        Local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
        i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
        i_bRes = .T.
        i_bnoChk = .T.
        i_bnoObbl = .T.
        With This
            If .w_GRPCODE>0 Or .w_USRCODE>0
                * --- Area Manuale = Check Row
                * --- Fine Area Manuale
            Else
                If This.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 And This.oPgFrm.Page1.oPag.oBody.nAbsRow<>This.nLastRow And (Type('i_bCheckEmptyRows')='U' Or i_bCheckEmptyRows)
                    i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
                    i_bRes=.F.
                    i_bnoChk=.F.
                Endif
            Endif
            If Not(i_bnoChk)
                Do cp_ErrorMsg With i_cErrorMsg
                Return(i_bRes)
            Endif
            If Not(i_bnoObbl)
                Do cp_ErrorMsg With MSG_FIELD_CANNOT_BE_NULL_QM
                Return(i_bRes)
            Endif
        Endwith
        If i_bRes
            i_bRes=DoDefault()
        Endif
        Return(i_bRes)
    Endfunc

    * --- SaveDependsOn
    Proc SaveDependsOn()
        This.o_GRPCODE = This.w_GRPCODE
        This.o_USRCODE = This.w_USRCODE
        This.o_XGRPCODE = This.w_XGRPCODE
        This.o_XUSRCODE = This.w_XUSRCODE
        Return


        * --- FullRow
    Func FullRow()
        Local i_bRes,i_nArea
        i_nArea=Select()
        Select (This.cTrsName)
        i_bRes=(t_GRPCODE>0 Or t_USRCODE>0)
        Select(i_nArea)
        Return(i_bRes)
    Endfunc

    * --- InitRow
    Proc InitRow()
        This.NotifyEvent("Before Init Row")
        Select (This.cTrsName)
        Append Blank
        This.nLastRow = Recno()
        Replace I_SRV    With "A"
        Replace I_RECNO  With Recno()
        With This
            .w_GRPCODE=0
            .w_USRCODE=0
            .w_XGRPCODE=0
            .w_XUSRCODE=0
            .w_namegrp=Space(20)
            .w_nameusr=Space(20)
            .w_GRPCODE = Iif(.w_XGRPCODE=0,-1,.w_XGRPCODE)
            .w_USRCODE = Iif(.w_XUSRCODE=0 Or .w_XGRPCODE<>0,-1,.w_XUSRCODE)
            .w_XGRPCODE = Iif(.w_GRPCODE=-1, 0, .w_GRPCODE)
            .DoRTCalc(3,3,.F.)
            If Not(Empty(.w_XGRPCODE))
                .link_2_3('Full')
            Endif
            .w_XUSRCODE = Iif(.w_XGRPCODE<>0, 0, Iif(.w_USRCODE=-1,0,.w_USRCODE))
            .DoRTCalc(4,4,.F.)
            If Not(Empty(.w_XUSRCODE))
                .link_2_4('Full')
            Endif
        Endwith
        This.DoRTCalc(5,9,.F.)
        This.oPgFrm.Page1.oPag.oBody.nAbsRow=Recno()
        This.TrsFromWork()
        This.ChildrenChangeRow()
        This.NotifyEvent("Init Row")
    Endproc

    * --- WorkFromTrs
    Proc WorkFromTrs()
        Select (This.cTrsName)
        This.w_GRPCODE = t_GRPCODE
        This.w_USRCODE = t_USRCODE
        This.w_XGRPCODE = t_XGRPCODE
        This.w_XUSRCODE = t_XUSRCODE
        This.w_namegrp = t_namegrp
        This.w_nameusr = t_nameusr
        This.oPgFrm.Page1.oPag.oBody.nAbsRow=Recno()
    Endproc

    * --- TrsFromWork
    Proc TrsFromWork()
        Select (This.cTrsName)
        Replace I_RECNO With Recno()
        Replace t_GRPCODE With This.w_GRPCODE
        Replace t_USRCODE With This.w_USRCODE
        Replace t_XGRPCODE With This.w_XGRPCODE
        Replace t_XUSRCODE With This.w_XUSRCODE
        Replace t_namegrp With This.w_namegrp
        Replace t_nameusr With This.w_nameusr
        If I_SRV='A'
            Replace GRPCODE With This.w_GRPCODE
            Replace USRCODE With This.w_USRCODE
        Endif
    Endproc

    * --- SubtractTotals
    Proc SubtractTotals()
    Endproc
Enddefine

* --- Define pages as container
Define Class tcp_recsedPag1 As StdContainer
    Width  = 147
    Height = 215
    stdWidth  = 147
    stdheight = 215
    resizeYpos=87
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.


    Add Object oObj_1_8 As cp_setCtrlObjprop With uid="WTFHXDANKA",Left=615, Top=-1, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Gruppi",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425399;
        , bGlobalFont=.T.



    Add Object oObj_1_9 As cp_setCtrlObjprop With uid="GGRDGFFQVZ",Left=750, Top=-1, Width=107,Height=18,;
        caption='Object',;
        cProp="Caption",cObj="Utenti",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425399;
        , bGlobalFont=.T.



    Add Object oObj_1_10 As cp_setobjprop With uid="SKPUZOAYUU",Left=615, Top=162, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_namegrp",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425399;
        , bGlobalFont=.T.



    Add Object oObj_1_11 As cp_setobjprop With uid="YIGARNUYFX",Left=615, Top=187, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_nameusr",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425399;
        , bGlobalFont=.T.



    Add Object oObj_1_12 As cp_setobjprop With uid="FLKYMPZHIR",Left=615, Top=26, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_XGRPCODE",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425399;
        , bGlobalFont=.T.



    Add Object oObj_1_13 As cp_setobjprop With uid="FWHFXVEOJG",Left=750, Top=26, Width=107,Height=18,;
        caption='Object',;
        cProp="tooltiptext",cObj="w_XUSRCODE",;
        cEvent = "Init",;
        nPag=1;
        , HelpContextID = 56425399;
        , bGlobalFont=.T.


    Add Object oStr_1_1 As StdString With uid="KVICLJFNZB",Visible=.T., Left=9, Top=6,;
        Alignment=2, Width=62, Height=18,;
        Caption="Gruppi"  ;
        , bGlobalFont=.T.

    Add Object oStr_1_2 As StdString With uid="LXZFTWRGXM",Visible=.T., Left=74, Top=6,;
        Alignment=2, Width=62, Height=18,;
        Caption="Utenti"  ;
        , bGlobalFont=.T.

    Add Object oBox_1_3 As StdBox With uid="LCFIHIVCPX",Left=6, Top=3, Width=133,Height=22

    Add Object oBox_1_4 As StdBox With uid="DAZKNDEIYN",Left=72, Top=3, Width=2,Height=156

    Add Object oEndHeader As BodyKeyMover With nDirection=1
    *
    * --- BODY transaction
    *
    Add Object oBody3D As Shape With Left=-1,Top=24,;
        width=140,Height=Int(Fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,SpecialEffect=0

    Add Object oBody As StdBody Noinit With ;
        left=0,Top=25,Width=139,Height=Int(Fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,ColumnCount=0,GridLines=1,;
        HeaderHeight=0,DeleteMark=.F.,ScrollBars=0,Enabled=.F.,;
        cLinkFile='cpgroups|cpusers|'

    Proc oBody.SetCurrentRow()
        Thisform.LockScreen=.T.
        Select (This.Parent.oContained.cTrsName)
        If Recno()<>This.nAbsRow
            If This.nAbsRow<>0 And Inlist(This.Parent.oContained.cFunction,'Edit','Load')
                If This.nAbsRow<>This.Parent.oContained.i_lastcheckrow And !This.Parent.oContained.CheckRow()
                    This.Parent.oContained.i_lastcheckrow=0
                    This.Parent.oContained.__dummy__.Enabled=.T.
                    This.Parent.oContained.__dummy__.SetFocus()
                    Select (This.Parent.oContained.cTrsName)
                    Go (This.nAbsRow)
                    This.SetFullFocus()
                    If !Isnull(This.Parent.oContained.oNewFocus)
                        This.Parent.oContained.oNewFocus.SetFocus()
                        This.Parent.oContained.oNewFocus=.Null.
                    Endif
                    This.Parent.oContained.__dummy__.Enabled=.F.
                    Thisform.LockScreen=.F.
                    Return
                Endif
            Endif
            This.nAbsRow=Recno()
            This.Parent.oContained.WorkFromTrs()
            This.Parent.oContained.mEnableControlsFixed()
            This.Parent.oContained.mCalcRowObjs()
            This.Parent.onamegrp_2_5.Refresh()
            This.Parent.onameusr_2_6.Refresh()
            * --- Area Manuale = Set Current Row
            * --- Fine Area Manuale
        Else
            This.Parent.oContained.mEnableControlsFixed()
        Endif
        If !Isnull(This.Parent.oContained.oNewFocus)
            This.Parent.oContained.oNewFocus.SetFocus()
            This.Parent.oContained.oNewFocus=.Null.
        Endif
        If This.RelativeRow<>0
            This.nRelRow=This.RelativeRow
        Endif
        If This.nBeforeAfter>0 && and Version(5)<700
            This.nBeforeAfter=This.nBeforeAfter-1
        Endif
        Thisform.LockScreen=.F.
        This.Parent.oContained.i_lastcheckrow=0
    Endproc
    Func oBody.GetDropTarget(cFile,nX,nY)
        Local oDropInto
        oDropInto=.Null.
        Do Case
            Case cFile='cpgroups'
                oDropInto=This.oBodyCol.oRow.oXGRPCODE_2_3
            Case cFile='cpusers'
                oDropInto=This.oBodyCol.oRow.oXUSRCODE_2_4
        Endcase
        Return(oDropInto)
    Endfunc


    Add Object onamegrp_2_5 As StdTrsField With uid="BACRHQMRWI",rtseq=5,rtrep=.T.,;
        cFormVar="w_namegrp",Value=Space(20),Enabled=.F.,;
        ToolTipText = "Descrizione gruppo",;
        HelpContextID = 123734823,;
        cTotal="", bFixedPos=.T., cQueryName = "namegrp",;
        bObbl = .F. , nPag = 2, bIsInHeader=.F., bMultilanguage =  .F.,;
        bGlobalFont=.T.,;
        Height=21, Width=133, Left=6, Top=162, InputMask=Replicate('X',20)

    Add Object onameusr_2_6 As StdTrsField With uid="KIKNHAOVJO",rtseq=6,rtrep=.T.,;
        cFormVar="w_nameusr",Value=Space(20),Enabled=.F.,;
        ToolTipText = "Nome utente",;
        HelpContextID = 90180393,;
        cTotal="", bFixedPos=.T., cQueryName = "nameusr",;
        bObbl = .F. , nPag = 2, bIsInHeader=.F., bMultilanguage =  .F.,;
        bGlobalFont=.T.,;
        Height=21, Width=133, Left=6, Top=187, InputMask=Replicate('X',20)

    Add Object oBeginFooter As BodyKeyMover With nDirection=-1
Enddefine

* --- Defining Body row
Define Class tcp_recsedBodyRow As Container
    Width=130
    Height=Int(Fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
    BackStyle=0                                                && 0=trasparente
    BorderWidth=0                                              && Spessore Bordo
    bGlobalFont=.T.
    FontName      = "Arial"
    FontSize      = 9
    FontBold      = .F.
    FontItalic    = .F.
    FontUnderline = .F.
    FontStrikethru= .F.
    oContained = .Null.

    Add Object oXGRPCODE_2_3 As StdTrsField With uid="BJTDZFWRTC",rtseq=3,rtrep=.T.,;
        cFormVar="w_XGRPCODE",Value=0,;
        ToolTipText = "Gruppo da controllare nell'applicazione delle politiche di sicurezza",;
        HelpContextID = 32725323,;
        cTotal = "", SpecialEffect=1, BorderStyle=0,;
        bObbl = .F. , nPag = 2, bIsInHeader=.F., bMultilanguage =  .F.,;
        bGlobalFont=.T.,;
        Height=17, Width=62, Left=-2, Top=0, bHasZoom = .T. , cLinkFile="cpgroups", oKey_1_1="code", oKey_1_2="this.w_XGRPCODE"

    Func oXGRPCODE_2_3.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            bRes=.link_2_3('Part',This)
        Endwith
        Return bRes
    Endfunc

    Proc oXGRPCODE_2_3.ecpDrop(oSource)
        This.Parent.oContained.link_2_3('Drop',oSource)
        This.Parent.oContained.mCalc(.T.)
        Select (This.Parent.oContained.cTrsName)
        If I_SRV<>'A'
            Replace I_SRV With 'U'
        Endif
        This.SetFocus()
    Endproc

    Proc oXGRPCODE_2_3.mZoom
        Do cp_grpzoom With This.Parent.oContained
        If !Isnull(This.Parent.oContained)
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc

    Add Object oXUSRCODE_2_4 As StdTrsField With uid="CUQZUDDGHO",rtseq=4,rtrep=.T.,;
        cFormVar="w_XUSRCODE",Value=0,;
        ToolTipText = "Utente da controllare nell'applicazione delle politiche di sicurezza",;
        HelpContextID = 34945355,;
        cTotal = "", SpecialEffect=1, BorderStyle=0,;
        bObbl = .F. , nPag = 2, bIsInHeader=.F., bMultilanguage =  .F.,;
        bGlobalFont=.T.,;
        Height=17, Width=62, Left=63, Top=0, bHasZoom = .T. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_XUSRCODE"

    Func oXUSRCODE_2_4.mCond()
        With This.Parent.oContained
            Return (.w_XGRPCODE=0)
        Endwith
    Endfunc

    Func oXUSRCODE_2_4.Check()
        Local bRes, bRes2
        bRes = .T.
        With This.Parent.oContained
            bRes=.link_2_4('Part',This)
        Endwith
        Return bRes
    Endfunc

    Proc oXUSRCODE_2_4.ecpDrop(oSource)
        This.Parent.oContained.link_2_4('Drop',oSource)
        This.Parent.oContained.mCalc(.T.)
        Select (This.Parent.oContained.cTrsName)
        If I_SRV<>'A'
            Replace I_SRV With 'U'
        Endif
        This.SetFocus()
    Endproc

    Proc oXUSRCODE_2_4.mZoom
        Do cp_usrzoom With This.Parent.oContained
        If !Isnull(This.Parent.oContained)
            This.Parent.oContained.mCalc(.T.)
        Endif
    Endproc
    Add Object oLast As LastKeyMover
    * ---
    Func oXGRPCODE_2_3.When()
        Return(.T.)
    Proc oXGRPCODE_2_3.GotFocus()
        If Inlist(This.Parent.oContained.cFunction,'Edit','Load')
            This.Parent.Parent.Parent.SetCurrentRow()
            This.Parent.oContained.SetControlsValue()
        Endif
        DoDefault()
    Proc oXGRPCODE_2_3.KeyPress(nKeyCode,nShift)
        DoDefault(nKeyCode,nShift)
        Do Case
            Case Inlist(nKeyCode,5,15)
                Nodefault
                If This.Valid()=0
                    Return
                Endif
                If This.Parent.oContained.CheckRow()
                    If Recno()>This.Parent.oContained.nFirstrow
                        This.Parent.oContained.i_lastcheckrow=This.Parent.Parent.Parent.nAbsRow
                        Thisform.LockScreen=.T.
                        Skip -1
                        This.Parent.Parent.Parent.SetFullFocus()
                    Else
                        This.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
                    Endif
                Else
                    If !Isnull(This.Parent.oContained.oNewFocus)
                        This.Parent.oContained.oNewFocus.SetFocus()
                        This.Parent.oContained.oNewFocus=.Null.
                    Endif
                Endif
            Case nKeyCode=24
                Nodefault
                If This.Valid()=0
                    Return
                Endif
                If This.Parent.oContained.CheckRow()
                    If Recno()=This.Parent.oContained.nLastRow
                        If This.Parent.oContained.FullRow() And This.Parent.oContained.CanAddRow()
                            Thisform.LockScreen=.T.
                            This.Parent.oContained.__dummy__.Enabled=.T.
                            This.Parent.oContained.__dummy__.SetFocus()
                            This.Parent.oContained.InitRow()
                            This.Parent.Parent.Parent.SetFocus()
                            This.Parent.oContained.__dummy__.Enabled=.F.
                        Else
                            This.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
                        Endif
                    Else
                        Thisform.LockScreen=.T.
                        This.Parent.oContained.i_lastcheckrow=This.Parent.Parent.Parent.nAbsRow
                        If This.Parent.Parent.Parent.RelativeRow=6
                            This.Parent.Parent.Parent.DoScroll(1)
                        Endif
                        Skip
                        This.Parent.Parent.Parent.SetFullFocus()
                    Endif
                Else
                    If !Isnull(This.Parent.oContained.oNewFocus)
                        This.Parent.oContained.oNewFocus.SetFocus()
                        This.Parent.oContained.oNewFocus=.Null.
                    Endif
                Endif
        Endcase
Enddefine

Procedure QueryFilter(result)
    Local i_res, i_cAliasName,i_cAliasName2
    result=''
    i_res=cp_AppQueryFilter('cp_recsed','cprecsed','')
    If !Empty(i_res)
        If Lower(Right(i_res,4))='.vqr'
            i_cAliasName2 = "cp"+Right(Sys(2015),8)
            i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.T.)+") "+i_cAliasName2+" where ";
                +" "+i_cAliasName2+".tablename=cprecsed.tablename";
                +" and "+i_cAliasName2+".progname=cprecsed.progname";
                +" and "+i_cAliasName2+".rfrownum=cprecsed.rfrownum";
                +")"
        Endif
        result=Iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
    Endif
Endproc

Procedure getEntityType(result)
    result="Detail File"
Endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
